MOVIES3D is maintained by laurent.berger[at]ifremer.fr and has benefited (so far) from codes from:

* naig.lebouffant[at]ifremer.fr
* mathieu.doray[at]ifremer.fr
* arthur.blanluet[at]ifremer.fr
* barbara.remond[at]gmail.com
