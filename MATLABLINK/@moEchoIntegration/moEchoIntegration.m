function p = moEchoIntegration(a)
if nargin == 0
   p.Enable=1;
   p = class(p,'moEchoIntegration');
   p.Enable=  moGetEchoIntegrationEnabled(p);
   %moSetEchoIntegrationEnabled(p);

elseif isa(a,'moEchoIntegration')
   p = a;
else
   p.Enable=1;
   p = class(p,'moEchoIntegration');
   p.Enable=  moGetEchoIntegrationEnabled(p);
   %moSetEchoIntegrationEnabled(p);
end
