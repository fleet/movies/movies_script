% moGetLayersDefinition Gets the layers definition
%   moGetLayersDefinition()
%     
%   return values : minDepth, maxDepth
%                   layerType : 0 = surface
%				1 = bottom
%
% IPSIS otonck 07/07/2008
% $Revision: 0.1 $
