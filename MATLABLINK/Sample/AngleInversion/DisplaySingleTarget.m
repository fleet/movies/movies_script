function varargout = DisplaySingleTarget(varargin)
% DISPLAYSINGLETARGET M-file for DisplaySingleTarget.fig
%      DISPLAYSINGLETARGET, by itself, creates a new DISPLAYSINGLETARGET or raises the existing
%      singleton*.
%
%      H = DISPLAYSINGLETARGET returns the handle to a new DISPLAYSINGLETARGET or the handle to
%      the existing singleton*.
%
%      DISPLAYSINGLETARGET('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in DISPLAYSINGLETARGET.M with the given input arguments.
%
%      DISPLAYSINGLETARGET('Property','Value',...) creates a new DISPLAYSINGLETARGET or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before DisplaySingleTarget_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to DisplaySingleTarget_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help DisplaySingleTarget

% Last Modified by GUIDE v2.5 05-Feb-2008 13:55:46

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @DisplaySingleTarget_OpeningFcn, ...
                   'gui_OutputFcn',  @DisplaySingleTarget_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before DisplaySingleTarget is made visible.
function DisplaySingleTarget_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to DisplaySingleTarget (see VARARGIN)
handles.Data=[];
handles.InitialData =[];
handles.CurrentDisplayList=[];
handles.ChannelNameList=[];
handles.ValuesChannelId=[];
handles.SounderAngles.ER60=[0 0 0 0 0 0 0 0 0 0 0 0 0 0 0] ;
handles.SounderAngles.ME70= [0 0 0]
% no populate everything

StringList=[];
ValuesChannelId=[];

optargin = size(varargin,2);
% Choose default command line output for DisplaySingleTarget
handles.output = hObject;
% Update handles structure
guidata(hObject, handles);


if optargin == 1
    handles.InitialData = MyData;
    handles.Data =handles.InitialData;

    NewFileOpened(hObject,handles);
end;

% UIWAIT makes DisplaySingleTarget wait for user response (see UIRESUME)
% uiwait(handles.figure1);



function NewFileOpened(hObject,handles)
handles.CurrentDisplayList=[];
handles.ChannelNameList=[];
handles.ValuesChannelId=[];
% no populate everything

StringList=[];
ValuesChannelId=[];
SounderList= handles.Data.SounderList;
for numSounder=1:size(SounderList,2)
    sounder=SounderList(1,numSounder);
    for numTrans=1:size(sounder.m_transducer,2);
        Transducer=sounder.m_transducer(1,numTrans);
        for numChan=1:size(Transducer.m_SoftChannel,2)
            SoftChannel=Transducer.m_SoftChannel(1,numChan);
            StringList=strvcat(StringList,SoftChannel.m_channelName);
            ValuesChannelId=[ValuesChannelId;SoftChannel.m_softChannelId];
        end;
    end;
end;

set(handles.ChannelList,'String',StringList);
handles.ValuesChannelId=ValuesChannelId;
handles.ChannelNameList=StringList;


PopulateDisplayList(hObject,handles);


% Update handles structure
guidata(hObject, handles);



% --- Outputs from this function are returned to the command line.
function varargout = DisplaySingleTarget_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in ChannelList.
function ChannelList_Callback(hObject, eventdata, handles)
% hObject    handle to ChannelList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns ChannelList contents as cell array
%        contents{get(hObject,'Value')} returns selected item from ChannelList


% --- Executes during object creation, after setting all properties.
function ChannelList_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ChannelList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Str=get(handles.ChannelList,'Value');
Name=handles.ChannelNameList(Str,:);
ChanId=handles.ValuesChannelId(Str,:);
ColorIdx=get(handles.popupmenu2,'Value');
T=get(handles.popupmenu2,'String');
Color=cell2mat(T(ColorIdx,:));
MyValue.ChanId=ChanId;
MyValue.Color=Color;
MyValue.ChanName=Name;
AddToDisplayList(hObject,MyValue,handles);


function handles=AddToDisplayList(hObject,MyValue,handles)
found=0;
for i=1:size(handles.CurrentDisplayList,1)
    Value=handles.CurrentDisplayList(i,1);
    if strcmp(MyValue.ChanName,Value.ChanName)==1
       handles.CurrentDisplayList(i,1)=MyValue;
       found=1;
       break;
    end
    
end;
if found ==0
    handles.CurrentDisplayList=[handles.CurrentDisplayList;MyValue];
end;
clear found
PopulateDisplayList(hObject,handles);
guidata(hObject, handles);


function RemoveFromDisplayList(hObject,ChannelName,handles)
Total=handles.CurrentDisplayList;
handles.CurrentDisplayList=[];
for i=1:size(Total,1)
    Value=Total(i,1);
    if strcmp(ChannelName,Value.ChanName)~=1
        handles.CurrentDisplayList=[handles.CurrentDisplayList;Value];
    end
end;
PopulateDisplayList(hObject,handles);
guidata(hObject, handles);


function PopulateDisplayList(hObject,handles)
StringList=[];
StringColor=[];
Str=get(handles.listbox3,'Value');

for i=1:size(handles.CurrentDisplayList,1)
    Value=handles.CurrentDisplayList(i,1);
    StringList=strvcat(StringList,Value.ChanName);
    StringColor=strvcat(StringColor,Value.Color);
end;
if Str >=size(StringList,1) && Str > 1
    set(handles.listbox3,'Value',Str-1);
end;
set(handles.listbox3,'String',StringList);









% --- Executes on button press in pushbutton1.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

Str=get(handles.listbox3,'Value');
T=get(handles.listbox3,'String');
Name=T(Str,:);
RemoveFromDisplayList(hObject,Name,handles);


% --- Executes on selection change in popupmenu2.
function popupmenu2_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu2


% --- Executes during object creation, after setting all properties.
function popupmenu2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in listbox3.
function listbox3_Callback(hObject, eventdata, handles)
% hObject    handle to listbox3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns listbox3 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox3
set(handles.edit1,'String','');
set(handles.edit2,'String','');

Str=get(handles.listbox3,'Value');
T=get(handles.listbox3,'String');
Name=T(Str,:);
ValueFound=[];
found=0;
for i=1:size(handles.CurrentDisplayList,1)
    Value=handles.CurrentDisplayList(i,1);
    if strcmp(Name,Value.ChanName)==1
       found=1;
       ValueFound=Value;
       break;
    end
    
end;
if found ==1
    set(handles.edit1,'String',ValueFound.Color);
    set(handles.edit2,'String',ValueFound.ChanId);
end;
clear found

% --- Executes during object creation, after setting all properties.
function listbox3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
DisplayData(hObject, handles,handles.Data);

function DisplayData(hObject, handles,Data)
Str=get(handles.popupmenu3,'Value');
T=get(handles.popupmenu3,'String');
CoordString=cell2mat(T(Str,:));
h=figure(2);
set(h,'Name',handles.CurrentFileName);
clf;
hold on;

for i=1:size(handles.CurrentDisplayList,1)
    Value=handles.CurrentDisplayList(i,1);
    ShowSingleTarget(Data,Value.ChanId, Value.Color,CoordString,handles.SounderAngles)
end;


function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in togglebutton2.
function togglebutton2_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of togglebutton2


% --- Executes on button press in togglebutton3.
function togglebutton3_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of togglebutton3


% --- Executes on button press in radiobutton2.
function radiobutton2_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton2


% --- Executes on button press in radiobutton3.
function radiobutton3_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton3


% --- Executes on selection change in popupmenu3.
function popupmenu3_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu3 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu3


% --- Executes during object creation, after setting all properties.
function popupmenu3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton6.
function pushbutton6_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
figure(2);
clf;

% --- Executes on button press in pushbutton5.
function pushbutton5_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
for Str=1:size(handles.ChannelNameList,1)
    Name=handles.ChannelNameList(Str,:);
    ChanId=handles.ValuesChannelId(Str,:);
    switch (deblank(Name))
        case '18000'
            Color='r';        
        case '38000'
            Color='y';
        case '70000'
            Color='g';
        case '120000'
            Color='c';
        case '200000'
            Color='b';
        otherwise 
            Color='k';
    end;
            
    MyValue.ChanId=ChanId;
    MyValue.Color=Color;
    MyValue.ChanName=Name;
    handles=AddToDisplayList(hObject,MyValue,handles);
end


% --- Executes on button press in pushbutton7.
function pushbutton7_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.Data=handles.InitialData ;
guidata(hObject, handles);

% --- Executes on button press in pushbutton8.
function pushbutton8_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
MyData=[];

Str=get(handles.popupmenu4,'Value');
T=get(handles.popupmenu4,'String');
CoordString=cell2mat(T(Str,:));
Str=get(handles.edit3,'String');
%T=get(handles.edit3,'String');
minValue=str2num(Str);
Str=get(handles.edit4,'String');
maxValue=str2num(Str);


OutPut=FilterSingleTarget(handles.Data.FanList,CoordString,minValue,maxValue,0);
MyData.FanList=OutPut;
MyData.SounderList=handles.Data.SounderList;
DisplayData(hObject, handles,MyData)



function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double


% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit4 as text
%        str2double(get(hObject,'String')) returns contents of edit4 as a double


% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton9.
function pushbutton9_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
MyData=[];

Str=get(handles.popupmenu4,'Value');
T=get(handles.popupmenu4,'String');
CoordString=cell2mat(T(Str,:));
Str=get(handles.edit3,'String');
%T=get(handles.edit3,'String');
minValue=str2num(Str);
Str=get(handles.edit4,'String');
maxValue=str2num(Str);


OutPut=FilterSingleTarget(handles.Data.FanList,CoordString,minValue,maxValue,0);
MyData.FanList=OutPut;
MyData.SounderList=handles.Data.SounderList;
DisplayData(hObject, handles,MyData);
handles.Data=MyData ;
guidata(hObject, handles);



% --- Executes on selection change in popupmenu4.
function popupmenu4_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu4 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu4


% --- Executes during object creation, after setting all properties.
function popupmenu4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit5_Callback(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit5 as text
%        str2double(get(hObject,'String')) returns contents of edit5 as a double


% --- Executes during object creation, after setting all properties.
function edit5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton11.
function pushbutton11_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

StringStat=ViewStatistics(handles.Data.FanList);
set(handles.listbox6,'Value',1);
set(handles.listbox6,'String',StringStat);


% --- Executes on selection change in listbox6.
function listbox6_Callback(hObject, eventdata, handles)
% hObject    handle to listbox6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns listbox6 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox6


% --- Executes during object creation, after setting all properties.
function listbox6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton12.
function pushbutton12_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.Data= OnlyKeepChannelInList(handles );
guidata(hObject, handles);


function BoolRet = IsInDisplayList(beamId, handles)
found = 0;
for i=1:size(handles.CurrentDisplayList,1)
    Value=handles.CurrentDisplayList(i,1);
    if Value.ChanId==beamId
        found=1;
        break;
    end;
end;
BoolRet=found;

function OutPutData = OnlyKeepChannelInList(handles)
% build rejected list
ChannelIdList = handles.ValuesChannelId;
for chanNum= 1:size(handles.ValuesChannelId,1)
    chanId=(handles.ValuesChannelId(chanNum,:));
    % search if it is in displayList
    if IsInDisplayList(chanId,handles)==0
        handles.Data.FanList=FilterSingleTarget(handles.Data.FanList,'Channel Filter',0,0,chanId);
    end;
end;
OutPutData = handles.Data;

function Sounder = GetSounderWithId(sounderId,handles)
found = 0;
for i = 1 : size(handles.Data.SounderList,2)
        if handles.Data.SounderList(1,i).m_SounderId == sounderId
            found=1;
            RetSounder= handles.Data.SounderList(1,sounderId);
            break;
        end;
end;
if found==0
    error('Sounder not found');
else
    Sounder = RetSounder;
end;

function Sounder = GetSounderWithChannId(chanId,handles)
found = 0;
for i = 1 : size(handles.Data.SounderList,2)
    tmp=handles.Data.SounderList(1,i);
    if found == 0
        for transNum=1:size(tmp.m_transducer,2)
            Trans=tmp.m_transducer(1,transNum);
            if found == 0;
                for chanNum=1:size(Trans.m_SoftChannel,2)
                    if Trans.m_SoftChannel(1,chanNum).m_softChannelId == chanId
                        found=1;
                        RetSounder= tmp;
                        break;
                    end;
                end;
            end;
        end;
    end;
end;
if found==0
    error('Sounder not found');
else
    Sounder = RetSounder;
end;

function CleanUpPerFan(hObject, eventdata, handles, save)
NumberOfTargetExpectedER60 = 0;
NumberOfTargetExpectedME70 = 0;

ChannelIdList = handles.ValuesChannelId;
for chanNum= 1:size(handles.ValuesChannelId,1)
    chanId=(handles.ValuesChannelId(chanNum,:));
    % search if it is in displayList
    if IsInDisplayList(chanId,handles)==1
        MySounder =GetSounderWithChannId(chanId,handles);
        if MySounder.m_isMultiBeam == 1
            NumberOfTargetExpectedME70=1;
        else
            NumberOfTargetExpectedER60=NumberOfTargetExpectedER60+1;
        end;
    end;
end;

FinalData =[];
for i = 1: size(handles.Data.FanList,2)
    MyFan= handles.Data.FanList(1,i);
    Sounder = GetSounderWithId(MyFan.m_sounderId ,handles);
    keep = 0;
    if Sounder.m_isMultiBeam == 1
        if NumberOfTargetExpectedME70>0
            if size(MyFan.singleTarget(1,1).m_splitBeamData,2) > 0
                keep=1;
            end;
        end;
    else
        if size(MyFan.singleTarget(1,1).m_splitBeamData,2)==NumberOfTargetExpectedER60
            keep=1;
            for numBeam= 1 :size(MyFan.singleTarget(1,1).m_splitBeamData,2)
                Target= MyFan.singleTarget(1,1).m_splitBeamData(1,numBeam);
                if size(Target.m_targetList,2) ~= 1
                    keep=0;
                    break;
                end;
            end;
        end;
    end;
    if keep == 1
        FinalData= [FinalData,MyFan];
    end;
end;
MyData.FanList=FinalData;
MyData.SounderList=handles.Data.SounderList;

DisplayData(hObject,handles,MyData );
if save==1
    handles.Data=MyData;
    guidata(hObject, handles);

end;


% --- Executes on button press in pushbutton14.
function pushbutton14_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
CleanUpPerFan(hObject, eventdata, handles,0);





% --- Executes on button press in pushbutton15.
function pushbutton15_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton15 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
CleanUpPerFan(hObject, eventdata, handles,1);


% --- Executes on button press in pushbutton16.
function pushbutton16_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton16 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

Delay = [];
Athwart = [];
Along = [];
PingId = [];
FanList=[];
SounderER60=[];
PackageId=[];
numPack=0;
X0=handles.SounderAngles.ME70;

% x=InverseAngleME70(X0,handles.SounderAngles.ER60,handles.Data.FanList,handles.Data.SounderList);
% AllAngles=handles.SounderAngles;
% AllAngles.ME70=x;
% InitialAngleER60(AllAngles);
% x
% h=figure(4);
% set(h,'Name',handles.CurrentFileName);
% 
% 
% hold on;
% 
% pushResult(0,x(1),x(2),x(3),'-g.');


x=InverseAngleME70_new(zeros(1,11),[-0.29;-0.17;-0.08;-0.03;-0.02]',handles.Data.FanList,handles.Data.SounderList);
AllAngles=handles.SounderAngles;
AllAngles.ER60=[[-0.29;-0.17;-0.08;-0.03;-0.02]' x(1:5) x(6:10)];
AllAngles.ME70=[x(11) 0 0 ];
InitialAngleER60(AllAngles);
x


 

% --- Executes on button press in pushbutton17.
function pushbutton17_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton17 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.SounderAngles=InitialAngleER60(handles.SounderAngles);
guidata(hObject, handles);

function CleanUpTimeFanME70(hObject, eventdata, handles,save)

FinalData =[];
for i = 1: size(handles.Data.FanList,2)
    MyFan= handles.Data.FanList(1,i);
    Sounder = GetSounderWithId(MyFan.m_sounderId ,handles);
    keep = 0;
    if Sounder.m_isMultiBeam == 1
        if i ~= 1 && i~= size(handles.Data.FanList,2)
            % check if last ping fan Id is mine minus one AND is an ER60
            % Sounder
            LastFan = handles.Data.FanList(1,i-1);
            LastSounder = GetSounderWithId(LastFan.m_sounderId ,handles);
            
            if LastSounder.m_isMultiBeam == 0 &&  LastFan.m_pingId == MyFan.m_pingId-1;
                % check if last ping fan Id is mine plus one AND is an ER60
                % Sounder
                NextFan = handles.Data.FanList(1,i+1);
                NextSounder = GetSounderWithId(NextFan.m_sounderId ,handles);
                if NextSounder.m_isMultiBeam == 0 &&  NextFan.m_pingId == MyFan.m_pingId+1;
                    keep = 1;
                end;
            end;
            
        end;
    else
        keep =1;
    end;
    if keep == 1
        FinalData= [FinalData,MyFan];
    end;
end;

MyData.FanList=FinalData;
MyData.SounderList=handles.Data.SounderList;

DisplayData(hObject,handles,MyData );
if save==1
    handles.Data=MyData;
    guidata(hObject, handles);
end;




% --- Executes on button press in pushbutton18.
function pushbutton18_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton18 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
CleanUpTimeFanME70(hObject, eventdata, handles,0);

% --- Executes on button press in pushbutton19.
function pushbutton19_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton19 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
CleanUpTimeFanME70(hObject, eventdata, handles,1);


function pushResult(PingId,Delay,AngleAthwart,AngleAlong,Color)
subplot(3,1,1);
hold on;
plot(PingId,Delay,Color,'MarkerSize',10);
title('Delay');

subplot(3,1,2);
hold on;
plot(PingId,AngleAthwart,Color,'MarkerSize',10);
title('Athwart Angle');

subplot(3,1,3);
hold on;
plot(PingId,AngleAlong,Color,'MarkerSize',10);
title('Along Angle');

% --- Executes on button press in pushbutton20.
function pushbutton20_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton20 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

Delay = [];
Athwart = [];
Along = [];
PingId = [];
FanList=[];
SounderER60=[];
PackageId=[];
numPack=0;
X0=zeros(1,10);

for i = 1: size(handles.Data.FanList,2)
    MyFan = handles.Data.FanList(1,i);
    Sounder = GetSounderWithId(MyFan.m_sounderId ,handles);
    if Sounder.m_isMultiBeam == 0
        FanList=[FanList, MyFan];
        SounderER60=Sounder;
    end;
    if size(FanList,2) == 100
        numPack= numPack+1;
        x=InverseAngleER60_new(X0,[-0.29;-0.17;-0.08;-0.03;-0.02]',FanList,SounderER60);
        FanList=[];
        PackageId=[PackageId,numPack];
        Delay= [Delay,[-0.29;-0.17;-0.08;-0.03;-0.02]];
        Athwart= [Athwart,(x(1:5))'];
        Along= [Along,(x(6:10))'];
    end;
end;
 
if size(FanList,2) > size(X0,2)
    numPack= numPack+1;
    x=InverseAngleER60_new(X0,[-0.29;-0.17;-0.08;-0.03;-0.02]',FanList,SounderER60);
    FanList=[];
    PackageId=[PackageId,numPack];
    Delay= [Delay,[-0.29;-0.17;-0.08;-0.03;-0.02]];
    Athwart= [Athwart,(x(1:5))'];
    Along= [Along,(x(6:10))'];
end;


% X0=zeros(1,15);
% 
% for i = 1: size(handles.Data.FanList,2)
%     MyFan = handles.Data.FanList(1,i);
%     Sounder = GetSounderWithId(MyFan.m_sounderId ,handles);
%     if Sounder.m_isMultiBeam == 0
%         FanList=[FanList, MyFan];
%         SounderER60=Sounder;
%     end;
%     if size(FanList,2) == 100
%         numPack= numPack+1;
%         x=InverseAngleER60(X0,FanList,SounderER60);
%         FanList=[];
%         PackageId=[PackageId,numPack];
%         Delay= [Delay,x(1:5)'];
%         Athwart= [Athwart,(x(6:10))'];
%         Along= [Along,(x(11:15))'];
%     end;
% end;
%  
% if size(FanList,2) > size(X0,2)
%     numPack= numPack+1;
%     x=InverseAngleER60(X0,FanList,SounderER60);
%     FanList=[];
%     PackageId=[PackageId,numPack];
%     Delay= [Delay,(x(1:5))'];
%     Athwart= [Athwart,(x(6:10))'];
%     Along= [Along,(x(11:15))'];
% end;

DelayResult= mean(Delay');
AthwartResult= mean(Athwart');
AlongResult= mean(Along');

XResult=[DelayResult,AthwartResult,AlongResult];
AllAngles=handles.SounderAngles;
AllAngles.ER60=XResult;
InitialAngleER60(AllAngles);
 
h=figure(3);
set(h,'Name',handles.CurrentFileName);

clf;
hold on;

pushResult(PackageId,Delay(1,:),Athwart(1,:),Along(1,:),'-r.');
pushResult(PackageId,Delay(2,:),Athwart(2,:),Along(2,:),'-y.');
pushResult(PackageId,Delay(3,:),Athwart(3,:),Along(3,:),'-g.');
pushResult(PackageId,Delay(4,:),Athwart(4,:),Along(4,:),'-c.');
pushResult(PackageId,Delay(5,:),Athwart(5,:),Along(5,:),'-b.');
legend('18','38','70','120','200');


% --- Executes on button press in pushbutton20.
function pushbutton22_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton20 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[FileName,PathName,FilterIndex]=uigetfile('*.hac','Load HAC File');
if ~isequal(FileName,0)
    Name=strcat(PathName,FileName);
    handles.InitialData=ParseData(Name);
    handles.Data =handles.InitialData;
    handles.CurrentFileName=Name;
    set(handles.figure1,'Name',handles.CurrentFileName);
   
    NewFileOpened(hObject,handles);
else
    msgbox('File does not exist');
end;


