function M = EnergieFunction(X, FanVector,Sounder)

M= [];
% X is a vector containing X = [internalDelay angle1 angle2;internalDelay angle1 angle2;....] 
for k = 1 : size(FanVector,2)
    fan=FanVector(k);
   


    
    MatrixPosition = [];
    for trans=1:size(fan.fanBeam,2)
        
        % filter  120 and 200
%         if trans == 0 || trans == 5
%             fan.fanBeam(1,trans).Empty = 1;
%         end;
        
        Delay=X(trans,1);
        transFaceAlongAngleOffsetRad=X(trans,2);
        m_transFaceAthwarAngleOffsetRad=X(trans,3);
    
        Transducer=Sounder.m_transducer(1,trans);
        SoftChannel=Transducer.m_SoftChannel;
     
        Position= [0;0;0];
        if fan.fanBeam(1,trans).Empty == 0
           if SoftChannel.m_softChannelId ~= fan.fanBeam(1,trans).beam
             errordlg('Incoherent data','Error','modal');
           end
           Range=fan.fanBeam(1,trans).m_targetRange;
           AlongTSAngle=fan.fanBeam(1,trans).m_AlongShipAngleRad;
           AthwartTSAngle=fan.fanBeam(1,trans).m_AthwartShipAngleRad;
           Position=ComputeWorldPosition(Delay,transFaceAlongAngleOffsetRad,m_transFaceAthwarAngleOffsetRad,fan.m_PingFan, Transducer, SoftChannel , Range, AlongTSAngle, AthwartTSAngle);
%            if fan.fanBeam(1,trans).Position ~= Position
%              fprintf('===> Position Is Different data\n')
%            end
        end;
        
        MatrixPosition=[MatrixPosition,Position];
        %little check activate it if given X is the original one
        
        
        
    end
    
    
    Distance= [];
    NumberOfPoint= 0;
	for trans=1:size(fan.fanBeam,2)

        DistanceResp=[];
        if fan.fanBeam(1,trans).Empty == 0 
            for trans2=1:size(fan.fanBeam,2)
                if fan.fanBeam(1,trans2).Empty == 0
                    DistanceResp=[DistanceResp;norm(MatrixPosition(:,trans2)-MatrixPosition(:,trans))];
                end;
            end

            Distance = [Distance;mean(DistanceResp)];
           NumberOfPoint=NumberOfPoint+1; 
        end;

    end;
    Mymean = 0;
    if size(Distance,2) > 0 &&  NumberOfPoint==5 
        Mymean=mean(Distance);
    end;
    M = [M;Mymean];
end
% hold on;
% figure(1);
% plot(M,'b');
% figure(2);
% color=['y';'m';'c';'r';'g';'b';'w';'k'];
% hold on;
% for t= 1 : 5
%     scatter(180*X(t,2)/pi,180*X(t,3)/pi,20,color(t,1));
% end;
% figure(3)
% hold on;
% for t= 1 : 5
%     scatter(X(t,1),t,20,color(t,1));
% end;
% drawnow;
fprintf ('Mean =[%f] var[%f] Max[%f]\n',mean(M),std(M),max(M));
% save interMediaire M;