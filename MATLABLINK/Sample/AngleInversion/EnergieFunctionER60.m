function M = EnergieFunctionER60(XInit,  FanList,Sounder)
DELAYINIT =   (XInit(1:5))';
ATHWART= (XInit(6:10))';
ALONG= (XInit(11:15))';

M= [];
% X is a vector containing X = [internalDelay angle1 angle2;internalDelay angle1 angle2;....]
for fanNum=1:size(FanList,2)
    fan=FanList(1,fanNum);
    MatrixPosition = [];
    for trans=1:5
        Delay=DELAYINIT(trans,1);
        transFaceAlongAngleOffsetRad=ALONG(trans,1)*pi/180;
        m_transFaceAthwarAngleOffsetRad=ATHWART(trans,1)*pi/180;

        Transducer=Sounder.m_transducer(1,trans);
        SoftChannel=Transducer.m_SoftChannel;

        Position= [0;0;0];

        TargetStruct = FindTarget(SoftChannel.m_softChannelId,fan);

        if TargetStruct.found == 1
            if size(TargetStruct.Target.m_targetList,2) ~= 1
                errordlg('Incoherent data','Error','modal');
            end;
            Range=TargetStruct.Target.m_targetList(1,1).m_targetRange;
            AlongTSAngle=TargetStruct.Target.m_targetList(1,1).m_AlongShipAngleRad;
            AthwartTSAngle=TargetStruct.Target.m_targetList(1,1).m_AthwartShipAngleRad;
            Position=ComputeWorldPositionER60(Delay,transFaceAlongAngleOffsetRad,m_transFaceAthwarAngleOffsetRad,fan, Transducer, SoftChannel , Range, AlongTSAngle, AthwartTSAngle);

            MatrixPosition=[MatrixPosition,Position];
        end;
        %little check activate it if given X is the original one
    end

    Distance= [];
    for numPoint=1:size(MatrixPosition,2)
        DistanceResp=[];
        for numPoint2=1:size(MatrixPosition,2)
            DistanceResp=[DistanceResp;norm(MatrixPosition(:,numPoint)-MatrixPosition(:,numPoint2))];
        end
        Distance = [Distance;mean(DistanceResp)];
    end;
    Mymean = 0;
    if size(Distance,2) > 0
        Mymean=mean(Distance);
    end;
    M = [M,Mymean];
end;

figure(6);
hold on;
plot(M);

% figure(4);
% hold on;
% IDX=[1:size(M,2)];
% plot(M);
% drawnow;
% figure(5);
% hold on;
% Delay= DELAYINIT;
% Athwart= ATHWART;
% Along= ALONG;
% pushResult(0,Delay(1,:),Athwart(1,:),Along(1,:),'r');
% pushResult(0,Delay(2,:),Athwart(2,:),Along(2,:),'y');
% pushResult(0,Delay(3,:),Athwart(3,:),Along(3,:),'g');
% pushResult(0,Delay(4,:),Athwart(4,:),Along(4,:),'c');
% pushResult(0,Delay(5,:),Athwart(5,:),Along(5,:),'b');
% 
% drawnow;

% function pushResult(PingId,Delay,AngleAthwart,AngleAlong,Color)
% 
% 
% subplot(3,1,1);
% hold on;
% scatter(PingId,Delay,10,Color);
% plot(PingId,Delay,Color);
% title('Delay');
% 
% subplot(3,1,2);
% hold on;
% scatter(PingId,AngleAthwart,10,Color);
% plot(PingId,AngleAthwart,Color);
% title('Athwart Angle');
% 
% subplot(3,1,3);
% hold on;
% scatter(PingId,AngleAlong,10,Color);
% plot(PingId,AngleAlong,Color);
% title('Along Angle');


function TargetStruct = FindTarget(beamId,fan)
found = 0;

for i = 1 :size(fan.singleTarget(1,1).m_splitBeamData,2)
    if fan.singleTarget(1,1).m_splitBeamData(1,i).m_parentSTId == beamId  
        Target= fan.singleTarget(1,1).m_splitBeamData(1,i);
        Ret.Target=Target;
        found= 1;
        break;
    end;
end;
Ret.found=found;

TargetStruct=Ret;