function M = EnergieFunctionME70(XInit, XER60,FanList,SounderList)
DELAYINIT =   XInit(1);
ATHWART= XInit(2);
ALONG= XInit(3);

DELAY_ER60 =   (XER60(1:5))';
ATHWART_ER60= (XER60(6:10))';
ALONG_ER60= (XER60(10:15))';

FanPosition = [];


% X is a vector containing X = [internalDelay angle1 angle2;internalDelay angle1 angle2;....]
for fanNum=1:size(FanList,2)
    fan=FanList(1,fanNum);

    Delay=DELAYINIT(1);
    transFaceAlongAngleOffsetRad=ALONG*pi/180;
    m_transFaceAthwarAngleOffsetRad=ATHWART*pi/180;

    Sounder= GetSounderWithId(fan.m_sounderId,SounderList);

    % A chaque pingFan on va associer un ensemble de cibles
    % correspondantes, pour ces cibles on va calculer le barycentre de
    % celles ci. Ainsi a chaque pingFan (ER60 ou ME70) on obtient un vector
    % 3*1 associe qui contient les positions du barycentre des cibles.
    %
    MatrixPosition = [];
    for transNum=1:size(Sounder.m_transducer,2)
        Transducer=Sounder.m_transducer(1,transNum);

        for chanNum=1:size(Transducer.m_SoftChannel,2)
            SoftChannel=Transducer.m_SoftChannel(1,chanNum);
            TargetStruct = FindTarget(SoftChannel.m_softChannelId,fan);

            if TargetStruct.found == 1
                if size(TargetStruct.Target.m_targetList,2) ~= 1
                    errordlg('Incoherent data','Error','modal');
                end;
                Range=TargetStruct.Target.m_targetList(1,1).m_targetRange;
                AlongTSAngle=TargetStruct.Target.m_targetList(1,1).m_AlongShipAngleRad;
                AthwartTSAngle=TargetStruct.Target.m_targetList(1,1).m_AthwartShipAngleRad;

                if Sounder.m_isMultiBeam==1
                    Position=ComputeWorldPositionME70(Delay,transFaceAlongAngleOffsetRad,m_transFaceAthwarAngleOffsetRad,fan, Transducer, SoftChannel , Range, AlongTSAngle, AthwartTSAngle);
                else
                    DelayER60=DELAY_ER60(transNum,1);
                    AlongER60Rad=ATHWART_ER60(transNum,1)*pi/180;
                    AthwER60Rad=ATHWART_ER60(transNum,1)*pi/180;

                    Position=ComputeWorldPositionER60(DelayER60,AlongER60Rad,AthwER60Rad,fan, Transducer, SoftChannel , Range, AlongTSAngle, AthwartTSAngle);
                end;
                MatrixPosition=[MatrixPosition,Position];
            end;
            %little check activate it if given X is the original one
        end
    end;
    if size(MatrixPosition,2)>1
         Barycenter=mean(MatrixPosition')';
    else
        Barycenter=MatrixPosition;
    end;
    FanPosition= [FanPosition, Barycenter];
end;
M= [];
for fanNum=2:size(FanList,2)-1
    fan=FanList(1,fanNum);
    Sounder= GetSounderWithId(fan.m_sounderId,SounderList);
	if Sounder.m_isMultiBeam==1
        LastPos=FanPosition(:,fanNum-1);
        ThisPos=FanPosition(:,fanNum);
        NextPos=FanPosition(:,fanNum+1);
        M=[M,norm(LastPos-ThisPos)];
        M=[M,norm(NextPos-ThisPos)];
    end;
end;
figure(6);
hold on;
plot(M);

function Sounder = GetSounderWithId(sounderId,SounderList)
found = 0;
for i = 1 : size(SounderList,2)
    if SounderList(1,i).m_SounderId == sounderId
        found=1;
        RetSounder= SounderList(1,sounderId);
        break;
    end;
end;
if found==0
    error('Sounder not found');
else
    Sounder = RetSounder;
end;

function TargetStruct = FindTarget(beamId,fan)
found = 0;

for i = 1 :size(fan.singleTarget(1,1).m_splitBeamData,2)
    if fan.singleTarget(1,1).m_splitBeamData(1,i).m_parentSTId == beamId
        Target= fan.singleTarget(1,1).m_splitBeamData(1,i);
        Ret.Target=Target;
        found= 1;
        break;
    end;
end;
Ret.found=found;

TargetStruct=Ret;