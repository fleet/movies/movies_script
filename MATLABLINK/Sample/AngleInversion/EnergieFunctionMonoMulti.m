
function M = EnergieFunctionMonoMulti(X,SounderMulti, FanVector,Sounder, OffsetMono)

    M= [];
    for k = 2 : size(FanVector,2)
        fan=FanVector(k-1);
        LastPos=GetMonoMatrixPosition(fan,Sounder,OffsetMono);

        fan=FanVector(k);
        CurrentPos=GetMonoMatrixPosition(fan,Sounder,OffsetMono);

        fan=fan.Multi;
        MyPos=  GetMultiMatrixPosition(X,SounderMulti,fan);

        Distance= [];
        NumberOfPoint= 0;
        fan=FanVector(k);
        lastFan=FanVector(k-1);
        fan.m_PingFan.m_pingId;
        for point = 1 : size(LastPos,2)
            Distance=[Distance,norm(MyPos-LastPos(:,point))];
        end;
        if lastFan.m_PingFan.m_pingId == fan.m_PingFan.m_pingId-1
            for point = 1 : size(CurrentPos,2)
                Distance=[Distance,norm(MyPos-CurrentPos(:,point))];
            end;
        end;
        Mymean = 0;
        if size(Distance,2) > 0 
            Mymean=mean(Distance);
        end;
        M = [M;Mymean];
    end;
    fprintf ('Mean =[%f] var[%f] Max[%f]\n',mean(M),std(M),max(M));
    hold on;
    plot(M)

function M=GetMonoMatrixPosition(fan, Sounder, OffsetMono)

    MatrixPosition = [];
    for trans=2:size(fan.fanBeam,2)
        Delay=OffsetMono(trans,1);
        transFaceAlongAngleOffsetRad=OffsetMono(trans,2);
        m_transFaceAthwarAngleOffsetRad=OffsetMono(trans,3);

        Transducer=Sounder.m_transducer(1,trans);
        SoftChannel=Transducer.m_SoftChannel;

        Position= [0;0;0];
        if fan.fanBeam(1,trans).Empty == 0
            if SoftChannel.m_softChannelId ~= fan.fanBeam(1,trans).beam 
                errordlg('Incoherent data','Error','modal');
            end
            Range=fan.fanBeam(1,trans).m_targetRange;
            AlongTSAngle=fan.fanBeam(1,trans).m_AlongShipAngleRad;
            AthwartTSAngle=fan.fanBeam(1,trans).m_AthwartShipAngleRad;
            Position=ComputeWorldPosition(Delay,transFaceAlongAngleOffsetRad,m_transFaceAthwarAngleOffsetRad,fan.m_PingFan, Transducer, SoftChannel , Range, AlongTSAngle, AthwartTSAngle);
            MatrixPosition=[MatrixPosition,Position];
        end;

    end;
    M=MatrixPosition;


function M = GetMultiMatrixPosition(X,SounderMulti,fan)
    Delay=X(1,1);
    transFaceAlongAngleOffsetRad=X(1,2);
    m_transFaceAthwarAngleOffsetRad=X(1,3);
    Transducer=SounderMulti.m_transducer(1,1);
    BeamId= fan.fanBeam.beam;
    %find channel
    for i=1: size(Transducer.m_SoftChannel,2)
        if Transducer.m_SoftChannel(1,i).m_softChannelId == BeamId
            SoftChannel=Transducer.m_SoftChannel(1,i);
        end;
    end;
    Position= [0;0;0];
    if fan.fanBeam(1,1).Empty == 0
        if SoftChannel.m_softChannelId ~= fan.fanBeam(1,1).beam
            errordlg('Incoherent data','Error','modal');
        end
        Range=fan.fanBeam(1,1).m_targetRange;
        AlongTSAngle=fan.fanBeam(1,1).m_AlongShipAngleRad;
        AthwartTSAngle=fan.fanBeam(1,1).m_AthwartShipAngleRad;
        Position=ComputeWorldPositionMulti(Delay,transFaceAlongAngleOffsetRad,m_transFaceAthwarAngleOffsetRad,fan.m_PingFan, Transducer, SoftChannel , Range, AlongTSAngle, AthwartTSAngle);
    end;
    M=Position;

