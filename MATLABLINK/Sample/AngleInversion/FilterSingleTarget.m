function Output = FilterSingleTarget(InputData, FilterType, minValue, maxValue,beamIdValue)
OutPutData=[];

minPingId=InputData(1,1).m_pingId;
Leng=size(InputData,2);
maxPingId=InputData(1,Leng).m_pingId;
clear Leng;
filterWithAxisX=0;
filterWithAxisY=0;
filterWithAxisZ=0;

beamIdToFilter=0;


switch lower(FilterType)
    case lower('Position X')
        filterWithAxisX=1;
    case lower('Position Y')
        filterWithAxisY=1;
    case lower('Position Z')
        filterWithAxisZ=1;
    case lower('PingFan Id')
        minPingId=minValue;
        maxPingId=maxValue;
    case lower('Channel Filter')
        beamIdToFilter=1;
    otherwise
         error('Unkwown filter type');
end;        



for i=1:size(InputData,2)
    Fan=InputData(1,i);
    if size(Fan.singleTarget,2)>0 && Fan.m_pingId>=minPingId && Fan.m_pingId<= maxPingId
        singleTargetList=[];

        for numBeamData=1:size(Fan.singleTarget(1,1).m_splitBeamData,2)
            BeamData=Fan.singleTarget(1,1).m_splitBeamData(1,numBeamData);
            if beamIdToFilter == 0 || BeamData.m_parentSTId ~= beamIdValue
                ListTarget=[];
                for targetNum=1:size(BeamData.m_targetList,2)
                    Target=BeamData.m_targetList(1,targetNum);
                    Filtered=0;
                    X = Target.m_positionWorldCoord(1,1);
                    Y=Target.m_positionWorldCoord(2,1);
                    Z= Target.m_positionWorldCoord(3,1);
                    if filterWithAxisY==1
                        if X <minValue || X > maxValue
                            Filtered=1;
                        end;
                    end;
                    if filterWithAxisY==1
                        if Y <minValue ||  Y > maxValue
                            Filtered=1;
                        end;
                    end;
                    if filterWithAxisZ==1
                        if Z <minValue ||   Z > maxValue
                            Filtered=1;
                        end;
                    end;
                    if Filtered== 0
                        ListTarget=[ListTarget;Target];
                    end;
                end;
                if size(ListTarget,2)>0
                    BeamData.m_targetList=ListTarget;
                    singleTargetList=[singleTargetList,BeamData];
                end;
            end;
        end;
        
       if size(singleTargetList,2)>0
            Fan.singleTarget.m_splitBeamData=singleTargetList;
            OutPutData=[OutPutData,Fan];
       end;
    end;
end;

Output=OutPutData;

