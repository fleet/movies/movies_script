function varargout = InitialAngleER60(varargin)
% INITIALANGLEER60 M-file for InitialAngleER60.fig
%      INITIALANGLEER60, by itself, creates a new INITIALANGLEER60 or raises the existing
%      singleton*.
%
%      H = INITIALANGLEER60 returns the handle to a new INITIALANGLEER60 or the handle to
%      the existing singleton*.
%
%      INITIALANGLEER60('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in INITIALANGLEER60.M with the given input arguments.
%
%      INITIALANGLEER60('Property','Value',...) creates a new INITIALANGLEER60 or raises
%      the existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before InitialAngleER60_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to InitialAngleER60_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help InitialAngleER60

% Last Modified by GUIDE v2.5 13-Feb-2008 10:55:20

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @InitialAngleER60_OpeningFcn, ...
                   'gui_OutputFcn',  @InitialAngleER60_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end
handles.Data.ER60 =  [0 0 0 0 0 0 0 0 0 0 0 0 0 0 0] ;
handles.Data.ME70 =  [0 0 0] ;

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before InitialAngleER60 is made visible.
function InitialAngleER60_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to InitialAngleER60 (see VARARGIN)

% Choose default command line output for InitialAngleER60
handles.output = hObject;
handles.Data.ER60 =  [0 0 0 0 0 0 0 0 0 0 0 0 0 0 0] ;
handles.Data.ME70 =  [0 0 0] ;

% Update handles structure
guidata(hObject, handles);

initialize_gui(hObject, handles, false);

% UIWAIT makes InitialAngleER60 wait for user response (see UIRESUME)
% uiwait(handles.figure1);

optargin = size(varargin,2);

if optargin == 1
	handles.Data =cell2mat(varargin);
    guidata(hObject, handles);

   
end;
PopulateEditText(hObject,handles);
uiwait(hObject);

% --- Outputs from this function are returned to the command line.
function varargout = InitialAngleER60_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.Data;
delete(hObject);






% --- Executes during object creation, after setting all properties.
function volume_CreateFcn(hObject, eventdata, handles)
% hObject    handle to volume (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end





% --- Executes when selected object changed in unitgroup.
function unitgroup_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in unitgroup 
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function initialize_gui(fig_handle, handles, isreset)
% If the metricdata field is present and the reset flag is false, it means
% we are we are just re-initializing a GUI by calling it from the cmd line
% while it is up. So, bail out as we dont want to reset the data.


% Update handles structure
guidata(handles.figure1, handles);

function PopulateEditText(hObject, handles)
     set(handles.Delay1,'String',handles.Data.ER60(1));
     set(handles.Delay2,'String',handles.Data.ER60(2));
     set(handles.Delay3,'String',handles.Data.ER60(3));
     set(handles.Delay4,'String',handles.Data.ER60(4));
     set(handles.Delay5,'String',handles.Data.ER60(5));
     
     set(handles.Atw1,'String',handles.Data.ER60(6));
     set(handles.Atw2,'String',handles.Data.ER60(7));
     set(handles.Atw3,'String',handles.Data.ER60(8));
     set(handles.Atw4,'String',handles.Data.ER60(9));
     set(handles.Atw5,'String',handles.Data.ER60(10));
    
     set(handles.Alon1,'String',handles.Data.ER60(11));
     set(handles.Alon2,'String',handles.Data.ER60(12));
     set(handles.Alon3,'String',handles.Data.ER60(13));
     set(handles.Alon4,'String',handles.Data.ER60(14));
     set(handles.Alon5,'String',handles.Data.ER60(15));
 
     set(handles.Delay81,'String',handles.Data.ME70(1));
     set(handles.Atw79,'String',handles.Data.ME70(2));
     set(handles.Alon80,'String',handles.Data.ME70(3));

function PopulateValues(hObject, handles)
    handles.Data.ER60(1) = str2double(get(handles.Delay1,'String')) ;
    handles.Data.ER60(2) = str2double(get(handles.Delay2,'String')) ;
    handles.Data.ER60(3) = str2double(get(handles.Delay3,'String')) ;
    handles.Data.ER60(4) = str2double(get(handles.Delay4,'String')) ;
    handles.Data.ER60(5) = str2double(get(handles.Delay5,'String')) ;
 
    handles.Data.ER60(6) = str2double(get(handles.Atw1,'String')) ;
    handles.Data.ER60(7) = str2double(get(handles.Atw2,'String')) ;
    handles.Data.ER60(8) = str2double(get(handles.Atw3,'String')) ;
    handles.Data.ER60(9) = str2double(get(handles.Atw4,'String')) ;
    handles.Data.ER60(10) = str2double(get(handles.Atw5,'String')) ;


    handles.Data.ER60(11) = str2double(get(handles.Alon1,'String')) ;
    handles.Data.ER60(12) = str2double(get(handles.Alon2,'String')) ;
    handles.Data.ER60(13) = str2double(get(handles.Alon3,'String')) ;
    handles.Data.ER60(14) = str2double(get(handles.Alon4,'String')) ;
    handles.Data.ER60(15) = str2double(get(handles.Alon5,'String')) ;

    
	handles.Data.ME70(1) = str2double(get(handles.Delay81,'String')) ;
	handles.Data.ME70(2) = str2double(get(handles.Atw79,'String')) ;
	handles.Data.ME70(3) = str2double(get(handles.Alon80,'String')) ;

    guidata(hObject, handles);

function roll1_Callback(hObject, eventdata, handles)
% hObject    handle to roll1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
PopulateValues(hObject,handles);

% Hints: get(hObject,'String') returns contents of roll1 as text
%        str2double(get(hObject,'String')) returns contents of roll1 as a double


% --- Executes during object creation, after setting all properties.
function roll1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to roll1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Alon1_Callback(hObject, eventdata, handles)
% hObject    handle to Alon1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Alon1 as text
%        str2double(get(hObject,'String')) returns contents of Alon1 as a double
PopulateValues(hObject,handles);


% --- Executes during object creation, after setting all properties.
function Alon1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Alon1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Delay1_Callback(hObject, eventdata, handles)
% hObject    handle to Delay1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Delay1 as text
%        str2double(get(hObject,'String')) returns contents of Delay1 as a double
PopulateValues(hObject,handles);


% --- Executes during object creation, after setting all properties.
function Delay1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Delay1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function roll2_Callback(hObject, eventdata, handles)
% hObject    handle to roll2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of roll2 as text
%        str2double(get(hObject,'String')) returns contents of roll2 as a double
PopulateValues(hObject,handles);


% --- Executes during object creation, after setting all properties.
function roll2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to roll2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Alon2_Callback(hObject, eventdata, handles)
% hObject    handle to Alon2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Alon2 as text
%        str2double(get(hObject,'String')) returns contents of Alon2 as a double
PopulateValues(hObject,handles);


% --- Executes during object creation, after setting all properties.
function Alon2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Alon2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Delay2_Callback(hObject, eventdata, handles)
% hObject    handle to Delay2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Delay2 as text
%        str2double(get(hObject,'String')) returns contents of Delay2 as a double
PopulateValues(hObject,handles);


% --- Executes during object creation, after setting all properties.
function Delay2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Delay2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function roll3_Callback(hObject, eventdata, handles)
% hObject    handle to roll3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of roll3 as text
%        str2double(get(hObject,'String')) returns contents of roll3 as a double
PopulateValues(hObject,handles);


% --- Executes during object creation, after setting all properties.
function roll3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to roll3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Alon3_Callback(hObject, eventdata, handles)
% hObject    handle to Alon3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Alon3 as text
%        str2double(get(hObject,'String')) returns contents of Alon3 as a double
PopulateValues(hObject,handles);


% --- Executes during object creation, after setting all properties.
function Alon3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Alon3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Delay3_Callback(hObject, eventdata, handles)
% hObject    handle to Delay3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Delay3 as text
%        str2double(get(hObject,'String')) returns contents of Delay3 as a double
PopulateValues(hObject,handles);


% --- Executes during object creation, after setting all properties.
function Delay3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Delay3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function roll4_Callback(hObject, eventdata, handles)
% hObject    handle to roll4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of roll4 as text
%        str2double(get(hObject,'String')) returns contents of roll4 as a double
PopulateValues(hObject,handles);


% --- Executes during object creation, after setting all properties.
function roll4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to roll4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Alon4_Callback(hObject, eventdata, handles)
% hObject    handle to Alon4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Alon4 as text
%        str2double(get(hObject,'String')) returns contents of Alon4 as a double
PopulateValues(hObject,handles);

% --- Executes during object creation, after setting all properties.
function Alon4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Alon4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Delay4_Callback(hObject, eventdata, handles)
% hObject    handle to Delay4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Delay4 as text
%        str2double(get(hObject,'String')) returns contents of Delay4 as a double
PopulateValues(hObject,handles);


% --- Executes during object creation, after setting all properties.
function Delay4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Delay4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function roll5_Callback(hObject, eventdata, handles)
% hObject    handle to roll5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of roll5 as text
%        str2double(get(hObject,'String')) returns contents of roll5 as a double
PopulateValues(hObject,handles);


% --- Executes during object creation, after setting all properties.
function roll5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to roll5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Alon5_Callback(hObject, eventdata, handles)
% hObject    handle to Alon5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Alon5 as text
%        str2double(get(hObject,'String')) returns contents of Alon5 as a double
PopulateValues(hObject,handles);


% --- Executes during object creation, after setting all properties.
function Alon5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Alon5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Delay5_Callback(hObject, eventdata, handles)
% hObject    handle to Delay5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Delay5 as text
%        str2double(get(hObject,'String')) returns contents of Delay5 as a double
PopulateValues(hObject,handles);


% --- Executes during object creation, after setting all properties.
function Delay5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Delay5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Atw1_Callback(hObject, eventdata, handles)
% hObject    handle to Atw1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Atw1 as text
%        str2double(get(hObject,'String')) returns contents of Atw1 as a double


% --- Executes during object creation, after setting all properties.
function Atw1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Atw1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Atw2_Callback(hObject, eventdata, handles)
% hObject    handle to Atw2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Atw2 as text
%        str2double(get(hObject,'String')) returns contents of Atw2 as a double


% --- Executes during object creation, after setting all properties.
function Atw2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Atw2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Atw3_Callback(hObject, eventdata, handles)
% hObject    handle to Atw3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Atw3 as text
%        str2double(get(hObject,'String')) returns contents of Atw3 as a double


% --- Executes during object creation, after setting all properties.
function Atw3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Atw3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Atw4_Callback(hObject, eventdata, handles)
% hObject    handle to Atw4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Atw4 as text
%        str2double(get(hObject,'String')) returns contents of Atw4 as a double


% --- Executes during object creation, after setting all properties.
function Atw4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Atw4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Atw5_Callback(hObject, eventdata, handles)
% hObject    handle to Atw5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Atw5 as text
%        str2double(get(hObject,'String')) returns contents of Atw5 as a double


% --- Executes during object creation, after setting all properties.
function Atw5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Atw5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton9.
function pushbutton9_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
PopulateValues(hObject,handles);
uiresume(handles.figure1);


% --- Executes on button press in pushbutton10.
function pushbutton10_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[FileName,PathName,FilterIndex]=uigetfile('*.mat','Load Angle mat File');
if  ~isequal(FileName,0)
    Name=strcat(PathName,FileName);
    load(Name,'Data');
    handles.Data =Data;
    PopulateEditText(hObject,handles);
    guidata(hObject, handles);
else
    msgbox('File does not exist');
end;


% --- Executes on button press in pushbutton11.
function pushbutton11_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[FileName,PathName,FilterIndex]=uiputfile('*.mat','Save Angle mat File');
if  ~isequal(FileName,0)
    Name=strcat(PathName,FileName);
    Data = handles.Data;
	save(Name,'Data');
    
else
    msgbox('File does not exist');
end;



function Atw79_Callback(hObject, eventdata, handles)
% hObject    handle to Atw79 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Atw79 as text
%        str2double(get(hObject,'String')) returns contents of Atw79 as a double
PopulateValues(hObject,handles);


% --- Executes during object creation, after setting all properties.
function Atw79_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Atw79 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Alon80_Callback(hObject, eventdata, handles)
% hObject    handle to Alon80 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Alon80 as text
%        str2double(get(hObject,'String')) returns contents of Alon80 as a double
PopulateValues(hObject,handles);


% --- Executes during object creation, after setting all properties.
function Alon80_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Alon80 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Delay81_Callback(hObject, eventdata, handles)
% hObject    handle to Delay81 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Delay81 as text
%        str2double(get(hObject,'String')) returns contents of Delay81 as a double
PopulateValues(hObject,handles);


% --- Executes during object creation, after setting all properties.
function Delay81_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Delay81 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


