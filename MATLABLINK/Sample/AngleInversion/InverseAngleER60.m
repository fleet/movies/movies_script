
function x= InverseAngleER60( X0,fanList,SounderList)

format long;

% we limit to a one deg 
LimitOneDeg= 2;
LimitDelay = 2;
Lb = [ -LimitDelay -LimitDelay -LimitDelay -LimitDelay -LimitDelay -LimitOneDeg -LimitOneDeg -LimitOneDeg -LimitOneDeg -LimitOneDeg -LimitOneDeg -LimitOneDeg -LimitOneDeg -LimitOneDeg -LimitOneDeg];

Lu= - Lb;
% Set an options file for LSQNONLIN to use the
% medium-scale algorithm 
options = optimset('Largescale','on','TolX',0.0002);

% Calculate the new coefficients using LSQNONLIN.

x=lsqnonlin(@EnergieFunctionER60,X0,Lb,Lu,options, fanList,SounderList);
%x=lsqnonlin(@EnergieFunctionPerFanER60,X0,fan,Sounder);
