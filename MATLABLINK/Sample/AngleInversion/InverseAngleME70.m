
function x= InverseAngleME70( X0ME70,XER60,FanList,SounderList)

format long;

% we limit to a one deg 
LimitOneDeg= 2;
LimitDelay = 2;
Lb = [-LimitDelay -LimitOneDeg -LimitOneDeg ];

Lu= - Lb;
% Set an options file for LSQNONLIN to use the
% medium-scale algorithm 
options = optimset('Largescale','on','TolX',0.0002);

% Calculate the new coefficients using LSQNONLIN.

x=lsqnonlin(@EnergieFunctionME70,X0ME70,Lb,Lu,options, XER60,FanList,SounderList);

