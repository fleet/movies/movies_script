function Data = ParseData(FileName)

if exist(FileName,'file') ~= 2
    error('File does not exis');
end;

moReadWholeFile(FileName);
FanList=[];
for i= 0 : moGetNumberOfPingFan()-1
   FanList=[FanList,moGetPingFan(i)];
end
Data.FanList=FanList;

Data.SounderList=moGetSounderList();

end