function ShowSingleTarget(Data,BeamId, Color,CoordString ,SounderAngles)
 %   ShowSingleTargetBeam(Data,BeamId, Color, 20,CoordString);
    ShowAndRecomputeSingleTargetBeam(Data,BeamId, Color, 20,CoordString,SounderAngles.ME70,SounderAngles.ER60);
end



function Sounder = GetSounderWithId(sounderId,SounderList)
found = 0;
for i = 1 : size(SounderList,2)
    if SounderList(1,i).m_SounderId == sounderId
        found=1;
        RetSounder= SounderList(1,sounderId);
        break;
    end;
end;
if found==0
    error('Sounder not found');
else
    Sounder = RetSounder;
end;
end

function TargetStruct = FindTarget(beamId,fan)
found = 0;
if size(fan.singleTarget,2)> 0
for i = 1 :size(fan.singleTarget(1,1).m_splitBeamData,2)
    if fan.singleTarget(1,1).m_splitBeamData(1,i).m_parentSTId == beamId
        Target= fan.singleTarget(1,1).m_splitBeamData(1,i);
        Ret.Target=Target;
        found= 1;
        break;
    end;
end;
end;
Ret.found=found;

TargetStruct=Ret;
end

function ShowAndRecomputeSingleTargetBeam(Data, BeamId, Color, Size,CoordSystem,ME70, XER60)

useWorldCoord=0;
switch lower(CoordSystem)
    case 'world coordinates'
        useWorldCoord=1;
    case 'beam coordinates'
        useWorldCoord=0;
    case 'angle and range'
        useWorldCoord=2;

end
FanList=Data.FanList;
SounderList=Data.SounderList;
DataToDisplayX=[];
DataToDisplayY=[];
DataToDisplayZ=[];
MatrixRange = [];
Heave = [];

DELAYINIT =   ME70(1);
ATHWART= ME70(2);
ALONG= ME70(3);

DELAY_ER60 =   (XER60(1:5))';
ATHWART_ER60= (XER60(6:10))';
ALONG_ER60= (XER60(11:15))';

FanPosition = [];


% X is a vector containing X = [internalDelay angle1 angle2;internalDelay angle1 angle2;....]
for fanNum=1:size(FanList,2)
    fan=FanList(1,fanNum);

    Delay=DELAYINIT(1);
    transFaceAlongAngleOffsetRad=ALONG*pi/180;
    m_transFaceAthwarAngleOffsetRad=ATHWART*pi/180;

    Sounder= GetSounderWithId(fan.m_sounderId,SounderList);

    % A chaque pingFan on va associer un ensemble de cibles
    % correspondantes, pour ces cibles on va calculer le barycentre de
    % celles ci. Ainsi a chaque pingFan (ER60 ou ME70) on obtient un vector
    % 3*1 associe qui contient les positions du barycentre des cibles.
    %
    MatrixPosition = [];
    for transNum=1:size(Sounder.m_transducer,2)
        Transducer=Sounder.m_transducer(1,transNum);

        for chanNum=1:size(Transducer.m_SoftChannel,2)
            SoftChannel=Transducer.m_SoftChannel(1,chanNum);
            if BeamId==SoftChannel.m_softChannelId
                TargetStruct = FindTarget(SoftChannel.m_softChannelId,fan);

                if TargetStruct.found == 1
                    for targetNumList=1:size(TargetStruct.Target.m_targetList,2) 
                        MyTarget= TargetStruct.Target.m_targetList(1,targetNumList);

                        Range=MyTarget.m_targetRange;
                        AlongTSAngle=MyTarget.m_AlongShipAngleRad;
                        AthwartTSAngle=MyTarget.m_AthwartShipAngleRad;

                        if useWorldCoord==1
                            if Sounder.m_isMultiBeam==1
                                Position=ComputeWorldPositionME70(Delay,transFaceAlongAngleOffsetRad,m_transFaceAthwarAngleOffsetRad,fan, Transducer, SoftChannel , Range, AlongTSAngle, AthwartTSAngle);
                        
                            else
                                DelayER60=DELAY_ER60(transNum,1);
                                AlongER60Rad=ALONG_ER60(transNum,1)*pi/180;
                                AthwER60Rad=ATHWART_ER60(transNum,1)*pi/180;

                                Position=ComputeWorldPositionER60(DelayER60,AlongER60Rad,AthwER60Rad,fan, Transducer, SoftChannel , Range, AlongTSAngle, AthwartTSAngle);
                            end;
                            DataToDisplayX=[DataToDisplayX;Position(1,1)];
                            DataToDisplayY=[DataToDisplayY;Position(2,1)];
                            DataToDisplayZ=[DataToDisplayZ;Position(3,1)];
                        else
                            if useWorldCoord== 0
                                DataToDisplayX=[DataToDisplayX;MyTarget.m_positionBeamCoord(1,1)];
                                DataToDisplayY=[DataToDisplayY;MyTarget.m_positionBeamCoord(2,1)];
                                DataToDisplayZ=[DataToDisplayZ;MyTarget.m_positionBeamCoord(3,1)];
                            else
                                DataToDisplayX=[DataToDisplayX;MyTarget.m_AlongShipAngleRad*180/pi];
                                DataToDisplayY=[DataToDisplayY;MyTarget.m_AthwartShipAngleRad*180/pi];
                                DataToDisplayZ=[DataToDisplayZ;MyTarget.m_targetRange];
                                
                            end
                        end

                    end;
                end;
            end;
            %little check activate it if given X is the original one
        end
    end;

end;
if size(DataToDisplayX,2)>0
    scatter3(DataToDisplayX,DataToDisplayY,DataToDisplayZ,Size, Color);
end
% figure(12);
% hold on;
% plot(MatrixRange);
% plot(Heave+14.2,'r');
end

    function ShowSingleTargetBeam(Data, BeamId, Color, Size,CoordSystem)

        useWorldCoord=0;
        switch lower(CoordSystem)
            case 'world coordinates'
                useWorldCoord=1;
            case 'beam coordinates'
                useWorldCoord=0;
            case 'angle and range'
                useWorldCoord=2;

        end
        FanList=Data.FanList;
        SounderList=Data.SounderList;
        DataToDisplayX=[];
        DataToDisplayY=[];
        DataToDisplayZ=[];
        MatrixRange = [];
        Heave = [];
        for i=1:size(FanList,2)
            Fan=FanList(1,i);
            if size(Fan.singleTarget,2)>0
                for numBeamData=1:size(Fan.singleTarget(1,1).m_splitBeamData,2)
                    BeamData=Fan.singleTarget(1,1).m_splitBeamData(1,numBeamData);
                    if BeamData.m_parentSTId == BeamId
                        for targetNum=1:size(BeamData.m_targetList,2)
                            Target=BeamData.m_targetList(1,targetNum);
                            if useWorldCoord==1
                                DataToDisplayX=[DataToDisplayX;Target.m_positionWorldCoord(1,1)];
                                DataToDisplayY=[DataToDisplayY;Target.m_positionWorldCoord(2,1)];
                                DataToDisplayZ=[DataToDisplayZ;Target.m_positionWorldCoord(3,1)];
                            else
                                if useWorldCoord== 0
                                    DataToDisplayX=[DataToDisplayX;Target.m_positionBeamCoord(1,1)];
                                    DataToDisplayY=[DataToDisplayY;Target.m_positionBeamCoord(2,1)];
                                    DataToDisplayZ=[DataToDisplayZ;Target.m_positionBeamCoord(3,1)];
                                else
                                    DataToDisplayX=[DataToDisplayX;Target.m_AlongShipAngleRad*180/pi];
                                    DataToDisplayY=[DataToDisplayY;Target.m_AthwartShipAngleRad*180/pi];
                                    DataToDisplayZ=[DataToDisplayZ;Target.m_targetRange];
                                    MatrixRange = [MatrixRange;Target.m_targetRange];
                                    Heave= [Heave;Fan.navigationAttitude.sensorHeaveMeter];
                                end
                            end

                        end;
                    end;

                end;
            end;

        end;
        if size(DataToDisplayX,2)>0
            scatter3(DataToDisplayX,DataToDisplayY,DataToDisplayZ,Size, Color);
        end
        % figure(12);
        % hold on;
        % plot(MatrixRange);
        % plot(Heave+14.2,'r');
    end
