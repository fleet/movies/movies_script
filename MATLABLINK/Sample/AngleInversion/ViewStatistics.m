function OutPutStr=ViewStatistics(InputData)
    minPingId=InputData(1,1).m_pingId;
    Leng=size(InputData,2);
    maxPingId=InputData(1,Leng).m_pingId;
    numberOfNullPingFan=0;
    AllStat=[];
    minNumberOfTargetPerFan=0;
    maxNumberOfTargetPerFan=0;
    
    for i=1:size(InputData,2)
        Fan=InputData(1,i);
        if size(Fan.singleTarget,1) > 0
            if minNumberOfTargetPerFan==0
                minNumberOfTargetPerFan=size(Fan.singleTarget(1,1).m_splitBeamData,2);
            else
                minNumberOfTargetPerFan=min(minNumberOfTargetPerFan,size(Fan.singleTarget(1,1).m_splitBeamData,2));
            end;
            maxNumberOfTargetPerFan=max(maxNumberOfTargetPerFan,size(Fan.singleTarget(1,1).m_splitBeamData,2));
            for numBeamData=1:size(Fan.singleTarget(1,1).m_splitBeamData,2)
                BeamData=Fan.singleTarget(1,1).m_splitBeamData(1,numBeamData);
                AllStat=PushStat(AllStat,BeamData.m_parentSTId,size(BeamData.m_targetList,2));
            end;
        else
            numberOfNullPingFan=numberOfNullPingFan+1;
        end;
    end;
    
StrOut = '';
StringOutput='';
StrOut=sprintf('Min target Per Fan %d\n',minNumberOfTargetPerFan);
StringOutput=strvcat(StringOutput,StrOut);
StrOut=sprintf('Max target Per Fan %d\n',maxNumberOfTargetPerFan);
StringOutput=strvcat(StringOutput,StrOut);

StrOut=sprintf('Min ping Id %d\n',minPingId);
StringOutput=strvcat(StringOutput,StrOut);
StrOut=sprintf('Max ping Id %d\n',maxPingId);
StringOutput=strvcat(StringOutput,StrOut);
StrOut=sprintf('Number of Null PingFan %d\n',numberOfNullPingFan);
StringOutput=strvcat(StringOutput,StrOut);

for i=1:size(AllStat,2)
    BeamEntry= AllStat(1,i);
    if BeamEntry.maxTargetCount > 0
        StrOut=sprintf('\t channel[%d] Max target [%d]\n',BeamEntry.BeamId,BeamEntry.maxTargetCount);
        StringOutput=strvcat(StringOutput,StrOut);
    end;
end;

OutPutStr=StringOutput;


function Output=PushStat(AllStat, BeamId, targetCount)
found=0;

for i=1:size(AllStat,2)
    BeamEntry= AllStat(1,i);
    if BeamEntry.BeamId== BeamId
        found=1;
        BeamEntry.maxTargetCount=max(BeamEntry.maxTargetCount,targetCount);
        AllStat(1,i)=BeamEntry;
        break;
    end;
end;
if found == 0
    BeamEntry.BeamId=BeamId;
    BeamEntry.minTargetCount=1;
    BeamEntry.maxTargetCount=targetCount;
    AllStat=[AllStat,BeamEntry];
end;

Output=AllStat;
