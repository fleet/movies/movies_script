close all
clear all

%% EI shoal

% path_hac_survey='I:\Pelgas14\Hac-Hermes\ANEshl\'
path_hac_survey='I:\Pelgas14\Hac-Hermes\PILshl\'
% path_conf='I:\Pelgas14\Hac-Hermes\ANEshl\EIshl\config\'
% path_result='I:\Pelgas14\Hac-Hermes\ANEshl\EIshl\results\'
path_conf='I:\Pelgas14\Hac-Hermes\PILshl\EIshl\config\'
path_result='I:\Pelgas14\Hac-Hermes\PILshl\EIshl\results\'
thresholds=[-70:-50];
runs=12;
esu=1;
%ithri=4

batch_school_multiTHR(path_hac_survey,path_conf,path_result,thresholds,runs,esu)

%% Import EI shoal results

%chemin='/export/home/Mathieu/Projects/BoB/Isobay/proc/'
chemin=path_result

filedir = dirr([chemin,'\*.mat']);  % ensemble des fichiers
nb_seuil = size(filedir,1);  % nombre de seuils

% figure;
% hold on;
nblME70=0;
nblER60=0;

for numdir = 1:size(thresholds,2)

    %seuil(numdir)=-str2num(filedir(numdir).name(size(filedir(numdir).name,2)-2:size(filedir(numdir).name,2)));
    seuil(numdir)=str2num(filedir(numdir).name)
    
    %filelist=ls([chemin,filedir(numdir).name,'/*school.mat']);
    %nb_files = size(filelist,1);

    filelist = squeeze(dir(fullfile([chemin,filedir(numdir).name,'\1'],'*.mat')));  % all files
    nb_files = size(filelist,1);  % number of files
    
    for numfile = 1:nb_files

        %matfilename = filelist(numfile,:);
        matfilename = filelist(numfile,:).name
        
%         year = str2num(matfilename(14:17));
%         month =  str2num(matfilename(18:19));
%         day = str2num(matfilename(20:21));
%         hour = str2num(matfilename(23:24));
%         mn = str2num(matfilename(25:26));

%         datefile=datenum(year, month, day, hour, mn, 0);
%         if (datefile>=debut && datefile<=fin)

        FileName = [chemin,filedir(numdir).name,'/1/',matfilename(1:findstr('.',matfilename)-1),'.mat'];
        load(FileName);

            if  size(schools_ER60,2)>0

                for i=1:size(schools_ER60,2)
                    time_ER60(nblER60+i) = schools_ER60(1,i).time;
                    length_ER60(nblER60+i) = schools_ER60(1,i).length;
                    width_ER60(nblER60+i) = schools_ER60(1,i).width;
                    height_ER60(nblER60+i) = schools_ER60(1,i).height;
                    volume_ER60(nblER60+i) = schools_ER60(1,i).volume;
                    MVBS_ER60(nblER60+i) = schools_ER60(1,i).MVBS_weighted;
                    sigma_ag_ER60(nblER60+i) = schools_ER60(1,i).sigma_ag;
                    position_ER60(nblER60+i,:) = schools_ER60(1,i).position;
                    seuil_ER60(nblER60+i,:) = seuil(numdir);

                end


                nblER60=nblER60+size(schools_ER60,2);
            end

            if  size(schools_ME70,1)>0
                for i=1:size(schools_ME70,2)
                    time_ME70(nblME70+i) = schools_ME70(1,i).time;
                    length_ME70(nblME70+i) = schools_ME70(1,i).length;
                    width_ME70(nblME70+i) = schools_ME70(1,i).width;
                    height_ME70(nblME70+i) = schools_ME70(1,i).height;
                    volume_ME70(nblME70+i) = schools_ME70(1,i).volume;
                    MVBS_ME70(nblME70+i) = schools_ME70(1,i).MVBS_weighted;
                    sigma_ag_ME70(nblME70+i) = schools_ME70(1,i).sigma_ag;
                    position_ME70(nblME70+i,:) = schools_ME70(1,i).position;
                    seuil_ME70(nblME70+i,:) = seuil(numdir);

                end

                nblME70=nblME70+size(schools_ME70,2);
            end

            clear  schools_ER60  schools_ME70;

%         end

    end


end

% load([chemin,'Navigation.mat'])

% timenum=zeros(size(Time,1),1);
% for i=1:size(Time,1)
%     timenum(i)=datenum(Time(i,:),'dd/mm/yyyy HH:MM:SS.FFF');
% end
% indexNav = find(timenum>debut & timenum<fin);
% 
% MaxX=max( reshape(Longitude(:,indexNav),1,[]));
% MinX=min( reshape(Longitude(:,indexNav),1,[]));
% 
% MaxY=max(reshape(Latitude(:,indexNav),1,[]));
% MinY=min(reshape(Latitude(:,indexNav),1,[]));
% 
% nbPoints = 1000;
% Precision = max((MaxX-MinX)/nbPoints,(MaxY-MinY)/nbPoints)
% XI=MinX:Precision:MaxX;
% YI=MinY:Precision:MaxY;
%     figure;
% ZI = griddata( reshape(Longitude(:,indexNav),1,[]),reshape(Latitude(:,indexNav),1,[]),-reshape(Depth(:,indexNav),1,[]),XI,YI');
% hold on;
% surf(XI,YI,ZI,'LineStyle','none');
% view(0,70);
% light
% lighting gouraud

chemin_save=[path_result,'shlmat/']

   if ~exist(chemin_save,'dir')
        mkdir(chemin_save)
    end

save([chemin_save,'shlAllThr.mat'],'MVBS_ER60','MVBS_ME70','height_ER60','height_ME70','length_ER60','length_ME70','position_ER60','position_ME70','seuil_ER60','seuil_ME70','sigma_ag_ER60','sigma_ag_ME70','time_ER60','time_ME70','volume_ER60','volume_ME70','width_ER60','width_ME70')


%% Exploratory analysis

% eventually, data import

pathPIL='I:\Pelgas14\Hac-Hermes\PILshl\EIshl\results\shlmat/shlAllThr.mat';
pathANE='I:\Pelgas14\Hac-Hermes\ANEshl\EIshl\results\shlmat/shlAllThr.mat';

%whof = whos('-file', '/export/home/Mathieu/Projects/BoB/Isobay/proc/shlmat/shl-55-60.mat');
load(pathPIL)

% data selection
seuil_sigma_ag=0;
seuil_volume = 0;
debut=datenum('30/05/2009 04:00');
fin=datenum('30/05/2009 23:00');
debut=min(time_ME70);
fin=max(time_ME70);

tabulate((time_ME70)>debut)
tabulate((time_ME70)<fin)

index= find((time_ME70)>debut & (time_ME70)<fin  & volume_ME70>seuil_volume);

plot(time_ME70,-position_ME70(:,3),'o')
% hold on
% plot(time_ME70_55,-position_ME70_55(:,3),'or')
% datetick('x',15,'keepticks')
% 
% position_ME70_60s=position_ME70_60((time_ME70_60)>debut & (time_ME70_60)<fin,:);
% 
% index2= find(((time_ME70)>debut & (time_ME70)<fin)  & seuil_ME70==-60);

% Data exploration

% long vs lat vs depth
plot3(position_ME70(seuil_ME70==-60,1),position_ME70(seuil_ME70==-60,2),position_ME70(seuil_ME70==-60,3))

% depth histogram
hist(position_ME70(seuil_ME70==-60,3))

%position_ME70_60_S=position_ME70_60(position_ME70_60(:,:,3)>=60,:,:);

% school parameters~threshold
seuils=unique(seuil_ME70);
nb_seuil=size(unique(seuil_ME70),1);

for i=1:nb_seuil
    percentilelength(i,:)=prctile(length_ME70(find(seuil_ME70(index)==seuil(i))),[25 50 75]);
    occurence(i)=size(length_ME70(find(seuil_ME70(index)==seuil(i))),2);
end
figure;
[AX,H1,H2] = plotyy(seuil,percentilelength,seuil,occurence);
set(get(AX(1),'Ylabel'),'String','Length (m)') 
set(get(AX(2),'Ylabel'),'String','Occurence')
xlabel('Threshold (dB)');

legend('10% percentile', '50% percentile', '90% percentile','Occurence');
title('Length distribution of schools at various threshold')
saveas(gcf,[chemin,sprintf('Length')],'fig')
saveas(gcf,[chemin,sprintf('Length')],'jpg')

for i=1:nb_seuil
    percentileWidth(i,:)=prctile(width_ME70(find(seuil_ME70(index)==seuil(i))),[25 50 75]);
    occurence(i)=size(width_ME70(find(seuil_ME70(index)==seuil(i))),2);
end
figure;
[AX,H1,H2] = plotyy(seuil,percentileWidth,seuil,occurence);
set(get(AX(1),'Ylabel'),'String','Width (m)') 
set(get(AX(2),'Ylabel'),'String','Occurence')
xlabel('Threshold (dB)');

legend('10% percentile', '50% percentile', '90% percentile','Occurence');
title('Width distribution of schools at various threshold')
saveas(gcf,[chemin,sprintf('Width')],'fig')
saveas(gcf,[chemin,sprintf('Width')],'jpg')

for i=1:nb_seuil
    percentileHeight(i,:)=prctile(height_ME70(find(seuil_ME70(index)==seuil(i))),[25 50 75]);
    occurence(i)=size(height_ME70(find(seuil_ME70(index)==seuil(i))),2);
end
figure;
[AX,H1,H2] = plotyy(seuil,percentileHeight,seuil,occurence);
set(get(AX(1),'Ylabel'),'String','Height (m)') 
set(get(AX(2),'Ylabel'),'String','Occurence')
xlabel('Threshold (dB)');

legend('10% percentile', '50% percentile', '90% percentile','Occurence');
title('Height distribution of schools at various threshold')
saveas(gcf,[chemin,sprintf('Height')],'fig')
saveas(gcf,[chemin,sprintf('Height')],'jpg')

for i=1:nb_seuil
    percentileVolume(i,:)=prctile(volume_ME70(find(seuil_ME70(index)==seuil(i))),[25 50 75]);
    occurence(i)=size(volume_ME70(find(seuil_ME70(index)==seuil(i))),2);
end
figure;
[AX,H1,H2] = plotyy(seuil,percentileVolume,seuil,occurence);
set(get(AX(1),'Ylabel'),'String','Volume (m3)') 
set(get(AX(2),'Ylabel'),'String','Occurence')
xlabel('Threshold (dB)');

legend('10% percentile', '50% percentile', '90% percentile','Occurence');
title('Volume distribution of schools at various threshold')
saveas(gcf,[chemin,sprintf('Volume')],'fig')
saveas(gcf,[chemin,sprintf('Volume')],'jpg')

for i=1:nb_seuil
    percentileAspect(i,:)=prctile(volume_ME70(find(seuil_ME70(index)==seuil(i)))./(height_ME70(find(seuil_ME70(index)==seuil(i))).*width_ME70(find(seuil_ME70(index)==seuil(i))).*length_ME70(find(seuil_ME70(index)==seuil(i)))),[25 50 75]);
    occurence(i)=size(length_ME70(find(seuil_ME70(index)==seuil(i))),2);
end
figure;
[AX,H1,H2] = plotyy(seuil,percentileAspect,seuil,occurence);
set(get(AX(1),'Ylabel'),'String','Aspect (unitless)') 
set(get(AX(2),'Ylabel'),'String','Occurence')
xlabel('Threshold (dB)');

legend('10% percentile', '50% percentile', '90% percentile','Occurence');
title('Aspect distribution of schools at various threshold (Volume ratio of school relative to bounding box)')
saveas(gcf,[chemin,sprintf('Aspect')],'fig')
saveas(gcf,[chemin,sprintf('Aspect')],'jpg')

for i=1:nb_seuil
    percentileMVBS(i,:)=prctile(MVBS_ME70(find(seuil_ME70(index)==seuil(i))),[25 50 75]);
    occurence(i)=size(MVBS_ME70(find(seuil_ME70(index)==seuil(i))),2);
end
figure;
[AX,H1,H2] = plotyy(seuil,percentileMVBS,seuil,occurence);
set(get(AX(1),'Ylabel'),'String','MVBS (dB)') 
set(get(AX(2),'Ylabel'),'String','Occurence')
xlabel('Threshold (dB)');

legend('10% percentile', '50% percentile', '90% percentile','Occurence');
title('MVBS distribution of schools at various threshold')
saveas(gcf,[chemin,sprintf('MVBS')],'fig')
saveas(gcf,[chemin,sprintf('MVBS')],'jpg')

close all

% figure;
% boxplot(length_ME70(index),seuil_ME70(index))
% xlabel('Threshold (dB)');
% ylabel('Length (m)');
% title('Length of school distribution for various threshold')
% saveas(gcf,[chemin,sprintf('Length ')],'fig')
% saveas(gcf,[chemin,sprintf('Length ')],'jpg')
% 
% figure;
% boxplot(width_ME70(index),seuil_ME70(index))
% xlabel('Threshold (dB)');
% ylabel('Width (m)');
% title('Width of school distribution for various threshold')
% saveas(gcf,[chemin,sprintf('Width ')],'fig')
% saveas(gcf,[chemin,sprintf('Width ')],'jpg')
% 
% figure;
% boxplot(height_ME70(index),seuil_ME70(index))
% xlabel('Threshold (dB)');
% ylabel('Height (m)');
% title('Height of school distribution for various threshold')
% saveas(gcf,[chemin,sprintf('Height ')],'fig')
% saveas(gcf,[chemin,sprintf('Height ')],'jpg')
% 
% figure;
% boxplot(volume_ME70(index),seuil_ME70(index))
% xlabel('Threshold (dB)');
% ylabel('Volume (m3)');
% title('Volume of school distribution for various threshold')
% saveas(gcf,[chemin,sprintf('Volume ')],'fig')
% saveas(gcf,[chemin,sprintf('Volume ')],'jpg')
% 
% figure;
% boxplot(height_ME70(index).*width_ME70(index).*length_ME70(index)./volume_ME70(index),seuil_ME70(index))
% xlabel('Threshold (dB)');
% ylabel('Volume ratio (unitless)');
% title('Volume ratio of school distribution relative to bounding box for various threshold')
% saveas(gcf,[chemin,sprintf('Aspect ratio')],'fig')
% saveas(gcf,[chemin,sprintf('Aspect ratio')],'jpg')
% 
% figure;
% boxplot(MVBS_ME70(index),seuil_ME70(index))
% xlabel('Threshold (dB)');
% ylabel('MVBS (dB)');
% title('MVBS of school distribution for various threshold')
% saveas(gcf,[chemin,sprintf('MVBS ')],'fig')
% saveas(gcf,[chemin,sprintf('MVBS ')],'jpg')


D=zeros(size(position_ME70(index),2),21);
for i=1:size(position_ME70(index),2)
    k=1;
    for j=max(1,i-10):min(i+10,size(position_ME70(index),2))
        D(i,k)=distance3D(position_ME70(index(i),:),position_ME70(index(j),:));
        k=k+1;
    end
end

for i=1:size(position_ME70(index),2)
    Dmin(i)=min(D(i,find(D(i,:)>0)));
end

for i=1:nb_seuil
    percentileDistance(i,:)=prctile(Dmin(find(seuil_ME70(index)==seuil(i))),[25 50 75]);
    occurence(i)=size(Dmin(find(seuil_ME70(index)==seuil(i))),2);
end
figure;
[AX,H1,H2] = plotyy(seuil,percentileDistance,seuil,occurence);
set(get(AX(1),'Ylabel'),'String','Distance (m)') 
set(get(AX(2),'Ylabel'),'String','Occurence')
xlabel('Threshold (dB)');

legend('Occurence','10% percentile', '50% percentile', '90% percentile');
title('Distance distribution of schools various threshold')
saveas(gcf,[chemin,sprintf('Distance')],'fig')
saveas(gcf,[chemin,sprintf('Distance')],'jpg')

% figure;
% boxplot(Dmin,seuil_ME70(index))
% xlabel('Threshold (dB)');
% ylabel('Distance (dB)');
% title('Distance between schools for various threshold')
% saveas(gcf,[chemin,sprintf('Distance ')],'fig')
% saveas(gcf,[chemin,sprintf('Distance ')],'jpg')


volume_ratio=zeros(nb_seuil,1);
for i=1:nb_seuil
    volume_ratio(i)=sum(volume_ME70(seuil_ME70(index)==seuil(i)))/sum(volume_ME70(seuil_ME70(index)==min(seuil)));
end

figure;
plot(seuil,volume_ratio);
xlabel('Threshold (dB)');
ylabel('Volume ratio (unitless)');
title('Volume ratio relative to lowest threshold')
saveas(gcf,[chemin,sprintf('Volume ratio')],'fig')
saveas(gcf,[chemin,sprintf('Volume ratio')],'jpg')

sigma_ag_ratio=zeros(nb_seuil,1);
for i=1:nb_seuil
    sigma_ag_ratio(i)=sum(sigma_ag_ME70(seuil_ME70(index)==seuil(i)))/sum(sigma_ag_ME70(seuil_ME70(index)==min(seuil)));
end

figure;
plot(seuil,sigma_ag_ratio);
xlabel('Threshold (dB)');
ylabel('Backscatter ratio (m)');
title('Backscatter ratio relative to lowest threshold')
saveas(gcf,[chemin,sprintf('Backscatter ratio ')],'fig')
saveas(gcf,[chemin,sprintf('Backscatter ratio ')],'jpg')




% for i=1:nb_seuil
% 
%     figure
%     scatter3(position_ME70(find(seuil_ME70()==seuil(i)),1),position_ME70(find(seuil_ME70()==seuil(i)),2),-position_ME70(find(seuil_ME70()==seuil(i)),3),sqrt(length_ME70(find(seuil_ME70()==seuil(i))).*width_ME70(find(seuil_ME70()==seuil(i)))),MVBS_ME70(find(seuil_ME70()==seuil(i))));
%     hold on;
% %     scatter3(reshape(Latitude(:,indexNav(1:10:end)),1,[]),reshape(Longitude(:,indexNav(1:10:end)),1,[]),-reshape(Depth(:,indexNav(1:10:end)),1,[]),'k');
%     xlabel('Latitude (�)');
%     ylabel('Longitude (�)');
%     ylabel('Depth (m)');
%     title(['Spatial distribution of schools at ',num2str(seuil(i)),'dB threshold, size represents length color represents density'])
%     colorbar;
%     axis([min(position_ME70(:,1)) max(position_ME70(:,1)) min(position_ME70(:,2)) max(position_ME70(:,2)) min(-position_ME70(:,3)) max(-position_ME70(:,3)) min(MVBS_ME70(:)) max(MVBS_ME70(:))]);
%     view(-40,20);
%     saveas(gcf,[chemin,'r�partition spatiale seuil ',num2str(seuil(i)),'dB'],'fig')
%     saveas(gcf,[chemin,'r�partition spatiale seuil ',num2str(seuil(i)),'dB'],'jpg')
%     
% 
% end

for i=1:nb_seuil

    figure
    scatter(position_ME70(find(seuil_ME70()==seuil(i)),1),position_ME70(find(seuil_ME70()==seuil(i)),2),sqrt(length_ME70(find(seuil_ME70()==seuil(i))).*width_ME70(find(seuil_ME70()==seuil(i)))),height_ME70(find(seuil_ME70()==seuil(i))));
    hold on;
%     scatter3(reshape(Latitude(:,indexNav(1:10:end)),1,[]),reshape(Longitude(:,indexNav(1:10:end)),1,[]),-reshape(Depth(:,indexNav(1:10:end)),1,[]),'k');
    xlabel('Latitude (�)');
    ylabel('Longitude (�)');

    title(['Spatial distribution of schools (vertical projection) at ',num2str(seuil(i)),'dB threshold, size represents length, color represents density'])
    colorbar;
    axis([min(position_ME70(:,1)) max(position_ME70(:,1)) min(position_ME70(:,2)) max(position_ME70(:,2))]);
    caxis( [min(height_ME70(:)) max(height_ME70(:))]);
%     view(-40,20);
    saveas(gcf,[chemin,'r�partition spatiale seuil ',num2str(seuil(i)),'dB'],'fig')
    saveas(gcf,[chemin,'r�partition spatiale seuil ',num2str(seuil(i)),'dB'],'jpg')
    

end

% figure
% for i=1:nb_seuil
% 
%     %             nbpoints=size(schools_ME70(1,i).contours,2)
%     %             for j=1:nbpoints
%     %                 lat(j)=schools_ME70(1,i).contours(j).m_latitude;
%     %                 long(j)=schools_ME70(1,i).contours(j).m_longitude;
%     %                 depth(j)=schools_ME70(1,i).contours(j).m_depth;
%     %             end
%     %
%     %             if nbpoints>50
%     %
%     %                 scatter3(lat,long,depth,10,nblME70*ones(1,nbpoints));
%     %             end
%     %             clear lat;
%     %             clear long
%     %             clear depth;
% end

%% shoal typology: PCA on selected ME70 shoal descriptors

nvar=strvcat('length','width','height','volume','MVBS','sigma_ag','depth')

X=multishl60s;

Xstd=(X-repmat(mean(X,1),size(X,1),1))./repmat(std(X,1),size(X,1),1);

boxplot(Xstd,'orientation','horizontal','labels',nvar)

%PCA on shoal descriptors

[coefs,scores,variances,t2] = princomp(Xstd);

percent_explained = 100*variances/sum(variances)
pareto(percent_explained)
xlabel('Principal Component')
ylabel('Variance Explained (%)')

biplot(coefs(:,1:2), 'scores',scores(:,1:2),'varlabels',nvar);

%% PCA on ME70 shoal descriptors w/o extreme shoal

Xs=multishl60s(time_ME70_60sf~=time_ME70_60sf(317),:);

Xsstd=(Xs-repmat(mean(Xs,1),size(Xs,1),1))./repmat(std(Xs,1),size(Xs,1),1);

[coefs2,scores2,variances2,t2] = princomp(Xsstd);

percent_explained2 = 100*variances2/sum(variances2)
pareto(percent_explained2)
xlabel('Principal Component')
ylabel('Variance Explained (%)')

biplot(coefs2(:,1:3), 'scores',scores2(:,1:3),'varlabels',nvar)

Xpc=scores2(:,1:3);

% kmeans clustering

[cidx2,cmeans2] = kmeans(Xpc,2,'dist','sqeuclidean')
[silh2,h] = silhouette(Xpc,cidx2,'sqeuclidean');

[cidx3,cmeans3,sumd3] = kmeans(Xpc,2,'replicates',5,'display','final');
sum(sumd3)

% HAC clustering

eucD = pdist(Xpc,'euclidean');
clustTreeEuc = linkage(eucD,'average');
cophenet(clustTreeEuc,eucD)
[h,nodes] = dendrogram(clustTreeEuc,0);

% Biplots

vmult=10;
ptsymb = {'bs','r^','md','go','c+'};

%subplot(2,2,1)
for i = 1:size(unique(cidx2),1)
    clust = find(cidx2==i);
    plot(Xpc(clust,1),Xpc(clust,2),ptsymb{i});
    hold on
end
hold on
text(coefs2(:,1)*vmult,coefs2(:,2)*vmult,nvar)
h=compass(coefs2(:,1)*(vmult-1),coefs2(:,2)*(vmult-1))
set(h,'Color',[.8 .8 .8],'MarkerSize',6);
line([min(scores2(:,1))-1 max(scores2(:,1))+1],[0 0],'Color',[.8 .8 .8])
line([0 0],[min(scores2(:,2))-1 max(scores2(:,2))+1],'Color',[.8 .8 .8])
title({'PCA biplot + kmeans clustering of ME70 shoal descriptors at -60dB'});
legend('Cluster1','Cluster2');
xlabel('PC1');
ylabel('PC2');

saveas(gcf,[chemin_plots,sprintf('biplotPC12_PCA+km_shl_ME70-60dB')],'fig')
saveas(gcf,[chemin_plots,sprintf('biplotPC12_PCA+km_shl_ME70-60dB')],'jpg')

vmult=4;
figure;
for i = 1:size(unique(cidx2),1)
    clust = find(cidx2==i);
    plot(Xpc(clust,1),Xpc(clust,3),ptsymb{i});
    hold on
end
hold on
text(coefs2(:,1)*vmult,coefs2(:,3)*vmult,nvar)
h=compass(coefs2(:,1)*(vmult-1),coefs2(:,3)*(vmult-1))
set(h,'Color',[.8 .8 .8],'MarkerSize',6);
line([min(scores2(:,1))-1 max(scores2(:,1))+1],[0 0],'Color',[.8 .8 .8])
line([0 0],[min(scores2(:,3))-1 max(scores2(:,3))+1],'Color',[.8 .8 .8])
title({'Biplot of PCA + clustering of shoal ME70 -60dB descriptors'});
legend('Cluster 1','Cluster 2');
xlabel('PC1');
ylabel('PC3');

saveas(gcf,[chemin_plots,sprintf('biplotPC13_PCA+km_shl_ME70-60dB')],'fig')
saveas(gcf,[chemin_plots,sprintf('biplotPC13_PCA+km_shl_ME70-60dB')],'jpg')

vmult=5;
figure;
for i = 1:size(unique(cidx2),1)
    clust = find(cidx2==i);
    plot(Xpc(clust,2),Xpc(clust,3),ptsymb{i});
    hold on
end
hold on
text(coefs2(:,2)*vmult,coefs2(:,3)*vmult,nvar)
h=compass(coefs2(:,2)*(vmult-1),coefs2(:,3)*(vmult-1))
set(h,'Color',[.8 .8 .8],'MarkerSize',6);
line([min(scores2(:,2))-1 max(scores2(:,2))+1],[0 0],'Color',[.8 .8 .8])
line([0 0],[min(scores2(:,3))-1 max(scores2(:,3))+1],'Color',[.8 .8 .8])
title({'Biplot of PCA + clustering of shoal ME70 -60dB descriptors'});
legend('Cluster 1','Cluster 2');
xlabel('PC2');
ylabel('PC3');

saveas(gcf,[chemin_plots,sprintf('biplotPC23_PCA+km_shl_ME70-60dB')],'fig')
saveas(gcf,[chemin_plots,sprintf('biplotPC23_PCA+km_shl_ME70-60dB')],'jpg')

%3d plots

% with a loop
for i = 1:3
    clust = find(cidx2==i);
    plot3(Xpc(clust,1),Xpc(clust,2),Xpc(clust,3),ptsymb{i});
    hold on
end
plot3(cmeans2(:,1),cmeans2(:,2),cmeans2(:,3),'ko');
plot3(cmeans2(:,1),cmeans2(:,2),cmeans2(:,3),'kx');
plot3(coefs2(:,1),coefs2(:,2),coefs2(:,3),'-')

for i = 1:size(unique(cidx2),1)
    clust = find(cidx2==i);
    plot3(position_ME70_60sf(clust,2),position_ME70_60sf(clust,1),-position_ME70_60sf(clust,3),ptsymb{i});
    hold on
end

% with 'scatter' function

clust=vertcat(cidx2(1:316),3,cidx2(317:size(cidx2,1)));
scatter(time_ME70_60sf,-position_ME70_60sf(:,3),log(multishl60s(:,4)+1)*50,clust,'filled');
datetick('x',15,'keepticks');

scatter3(position_ME70_60sf(:,2),position_ME70_60sf(:,1),-position_ME70_60sf(:,3),log(multishl60s(:,4)+1)*10,clust,'filled');
xlabel('Longitude');
ylabel('Latitude');
zlabel('Depth (m)');

%% PCA on ER60 shoal descriptors w/o extreme shoal

Y=monoshl60s;

Ystd=Y./repmat(std(Y,1),size(Y,1),1);

[coefs3,scores3,variances3,t2] = princomp(Ystd);

percent_explained3 = 100*variances3/sum(variances3)
pareto(percent_explained3);
xlabel('Principal Component');
ylabel('Variance Explained (%)');

biplot(coefs3(:,1:3), 'scores',scores3(:,1:3),'varlabels',nvar)

Ypc=scores3(:,1:3);

% kmeans clustering

[cidx3,cmeans3] = kmeans(Ypc,2,'dist','sqeuclidean')
[silh3,h] = silhouette(Ypc,cidx3,'sqeuclidean');

[cidx4,cmeans4,sumd4] = kmeans(Ypc,2,'replicates',5,'display','final');
sum(sumd4)

% HAC clustering

eucD = pdist(Xpc,'euclidean');
clustTreeEuc = linkage(eucD,'average');
cophenet(clustTreeEuc,eucD)
[h,nodes] = dendrogram(clustTreeEuc,0);

% Biplots

vmult=6;
ptsymb = {'bs','r^','md','go','c+'};

%subplot(2,2,1)
for i = 1:size(unique(cidx3),1)
    clust = find(cidx3==i);
    plot(Ypc(clust,1),Ypc(clust,2),ptsymb{i});
    hold on
end
hold on
text(coefs3(:,1)*vmult,coefs3(:,2)*vmult,nvar)
line([min(scores3(:,1))-1 max(scores3(:,1))+1],[0 0],'Color',[.8 .8 .8])
line([0 0],[min(scores3(:,2))-1 max(scores3(:,2))+1],'Color',[.8 .8 .8])
title({'Biplot of PCA + clustering of shoal ER60 -60dB descriptors','kmeans clustering'});
legend('Cluster1','Cluster2');
xlabel('PC1')
ylabel('PC2')

%subplot(2,2,2)


clust2=cidx3;
scatter(time_ER60_60sf,-position_ER60_60sf(:,3),log(monoshl60s(:,4)+1)*10,clust2,'filled');
datetick('x',15,'keepticks');




% Remove offshore areas

Xs=X(time_ME70_60<tmax & time_ME70_60>tmin,:);

sel=time_ME70_60<tmax & time_ME70_60>tmin;

plot(time_ME70_60(:,time_ME70_60<tmax & time_ME70_60>tmin),-position_ME70_60(time_ME70_60<tmax & time_ME70_60>tmin,3),'o')
datetick('x',15,'keepticks')

time_ME70_60s=time_ME70_60(:,sel);
position_ME70_60s=position_ME70_60(sel,:);

Xsstd=(Xs-repmat(mean(Xs,1),size(Xs,1),1))./repmat(std(Xs,1),size(Xs,1),1);

boxplot(Xsstd,'orientation','horizontal','labels',nvar)

%PCA on shoal descriptors

[coefs,scores,variances,t2] = princomp(Xsstd);

percent_explained = 100*variances/sum(variances)
pareto(percent_explained)
xlabel('Principal Component')
ylabel('Variance Explained (%)')

plot(scores(:,1),scores(:,2),'+')
xlabel('1st Principal Component')
ylabel('2nd Principal Component')

%names=num2str([1:size(Xs,1)]);
%gname(names)

datestr(datevec(time_ME70_60s(:,86192)))

plot(time_ME70_60s,-position_ME70_60s(:,3),'o')
datetick('x',15,'keepticks')
hold on
plot(time_ME70_60s(:,scores(:,2)<-50),-position_ME70_60s(scores(:,2)<-50,3),'or')

biplot(coefs(:,1:2), 'scores',scores(:,1:2),'varlabels',nvar);

%surface layer
sel2=position_ME70_60s(:,3)<60;
tabulate(sel2)
size(time_ME70_60s2)
Xs2=Xs(sel2,:);

time_ME70_60s2=time_ME70_60s(:,sel2);
position_ME70_60s2=position_ME70_60s(sel2,:);

hist(position_ME70_60s2(:,3))

plot(time_ME70_60s2,-position_ME70_60s2(:,3),'o')
datetick('x',15,'keepticks')

Xs2std=(Xs2-repmat(mean(Xs2,1),size(Xs2,1),1))./repmat(std(Xs2,1),size(Xs2,1),1);

boxplot(Xs2std,'orientation','horizontal','labels',nvar)

%PCA on shoal descriptors

[coefs2,scores2,variances2,t2] = princomp(Xs2std);

percent_explained2 = 100*variances2/sum(variances2)
pareto(percent_explained2)
xlabel('Principal Component')
ylabel('Variance Explained (%)')

plot(scores2(:,1),scores2(:,2),'+')
xlabel('1st Principal Component')
ylabel('2nd Principal Component')

biplot(coefs2(:,1:2), 'scores',scores2(:,1:2),'varlabels',nvar);

size(scores2)

Xpc=scores2(:,1:5);

IDX=kmeans(Xpc,2);
size(IDX)
size(time_ME70_60s2)

plot(time_ME70_60s2,-position_ME70_60s2(:,3),'o')
datetick('x',15,'keepticks')
hold on
%plot(time_ME70_60s2(:,IDX==2),-position_ME70_60s2(IDX==2,3),'or')
%datetick('x',15,'keepticks')

saveas(gcf,[chemin_plots,sprintf('z~t2_SURFshl_ME70ER60-60dB')],'fig')
saveas(gcf,[chemin_plots,sprintf('z~t2_SURFshl_ME70ER60-60dB')],'jpg')

%new selection

tmin2=datenum('29-May-2009 12:17:00');

sel3=(position_ME70_60s2(:,3)<33)'&time_ME70_60s2 > tmin2;

tabulate(sel3)
size(time_ME70_60s2)
Xs3=Xs(sel3,:);

time_ME70_60s3=time_ME70_60s2(:,sel3);
position_ME70_60s3=position_ME70_60s2(sel3,:);

hist(position_ME70_60s3(:,3))

plot(time_ME70_60s3,-position_ME70_60s3(:,3),'o')
datetick('x',15,'keepticks')

Xs3std=(Xs3-repmat(mean(Xs3,1),size(Xs3,1),1))./repmat(std(Xs3,1),size(Xs3,1),1);

%PCA on shoal descriptors

[coefs3,scores3,variances3,t2] = princomp(Xs3std);

percent_explained3 = 100*variances3/sum(variances3)
pareto(percent_explained3)
xlabel('Principal Component')
ylabel('Variance Explained (%)')

plot(scores3(:,1),scores3(:,2),'+')
xlabel('1st Principal Component')
ylabel('2nd Principal Component')

%names=num2str([1:size(Xs3,1)]);
%gname(names)
tr=time_ME70_60s3(:,376);
datestr(datevec(time_ME70_60s3(:,376)))
position_ME70_60s3(376,3)
biplot(coefs3(:,1:2), 'scores',scores3(:,1:2),'varlabels',nvar);


time_ME70_60s4=time_ME70_60s3(:,time_ME70_60s3~=tr);
position_ME70_60s4=position_ME70_60s3(time_ME70_60s3~=tr,:);

Xs4=Xs3(time_ME70_60s3~=tr,:);

Xs4std=(Xs4-repmat(mean(Xs4,1),size(Xs4,1),1))./repmat(std(Xs4,1),size(Xs4,1),1);

%PCA on shoal descriptors

[coefs4,scores4,variances4,t2] = princomp(Xs4std);

percent_explained4 = 100*variances4/sum(variances4)
pareto(percent_explained4)
xlabel('Principal Component')
ylabel('Variance Explained (%)')

plot(scores4(:,1),scores4(:,2),'+')
xlabel('1st Principal Component')
ylabel('2nd Principal Component')

biplot(coefs4(:,1:2), 'scores',scores4(:,1:2),'varlabels',nvar);

Xpc=scores4(:,1:4);

IDX=kmeans(Xpc,2);
size(IDX)
size(time_ME70_60s2)

plot(time_ME70_60s4,-position_ME70_60s4(:,3),'o')
datetick('x',15,'keepticks')
hold on
plot(time_ME70_60s4(:,IDX==2),-position_ME70_60s4(IDX==2,3),'or')
datetick('x',15,'keepticks')

plot(scores4(:,1),scores4(:,2),'+')
xlabel('1st Principal Component')
ylabel('2nd Principal Component')
hold on
plot(scores4(IDX==2,1),scores4(IDX==2,2),'+r')

%N=412

%ER60


time_ER60_60s=time_ER60_60(:,seltot);
position_ER60_60s=position_ER60_60(seltot,:);

size(time_ER60_60s)
size(position_ER60_60s)

plot(time_ER60_60s,-position_ER60_60(:,3),'o')
datetick('x',15,'keepticks')

Ysstd=(Ys-repmat(mean(Ys,1),size(Ys,1),1))./repmat(std(Ys,1),size(Ys,1),1);

boxplot(Ysstd,'orientation','horizontal','labels',nvar)

%PCA on shoal descriptors

[coefsy,scoresy,variancesy,t2] = princomp(Ysstd);

percent_explainedy = 100*variancesy/sum(variancesy)
pareto(percent_explainedy)
xlabel('Principal Component')
ylabel('Variance Explained (%)')

plot(scoresy(:,1),scoresy(:,2),'+')
xlabel('1st Principal Component')
ylabel('2nd Principal Component')

biplot(coefsy(:,1:2), 'scores',scoresy(:,1:2),'varlabels',nvar);

