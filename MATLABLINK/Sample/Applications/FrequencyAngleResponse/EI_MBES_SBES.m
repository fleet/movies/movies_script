%LB 02/04/2013 ajout r�cup�ration sondeur horizontal

%Clean workspace
clear all;
close all;

%% Set paths
path_hac_survey='C:\Users\lberger\Desktop\FAST 2018\PELGAS\'
path_config = [path_hac_survey,'/Config_M3D/EI_lay']


%% Echo-integration
thresholds=[-60];
% runs=[4];
% datestart='03/05/2016';
% dateend='03/05/2016';
% heurestart='08:38:00';
% heureend='9:27:00';
% survey_name='PELGAS16';
% runs=[4];
% datestart='03/05/2016';
% dateend='03/05/2016';
% heurestart='09:30:00';
% heureend='10:30:00';
% survey_name='PELGAS16_chalut';
% runs=[4];
% datestart='03/05/2016';
% dateend='03/05/2016';
% heurestart='10:40:00';
% heureend='11:10:00';
% survey_name='PELGAS16_bis';
% runs=[4];
datestart='04/01/2009';
dateend='04/07/2017';
heurestart='04:40:00';
heureend='06:00:00';
survey_name='PELGAS2';
runs=[ 3]; 
% datestart='03/05/2016';
% dateend='03/05/2016';
% heurestart='14:50:00';
% heureend='15:40:00';
% survey_name='PELGAS16_chalut2';
% runs=[4];
% datestart='03/05/2016';
% dateend='03/05/2016';
% heurestart='15:50:00';
% heureend='17:30:00';
% survey_name='PELGAS16_qua';
% runs=[5];
% datestart='04/05/2016';
% dateend='04/05/2016';
% heurestart='08:00:00';
% heureend='9:30:00';
% survey_name='PELGAS16_qua';
% runs=[12];
% datestart='05/05/2014';
% dateend='05/05/2014';
% heurestart='08:00:00';
% heureend='08:30:00';
% survey_name='PELGAS14';

% runs=[13];
% datestart='12/05/2015';
% dateend='12/05/2015';
% heurestart='08:36:00';
% heureend='12:00:00';
% survey_name='PELGAS15bis';

% runs=[9];
% datestart='08/05/2015';
% dateend='08/05/2015';
% heurestart='08:00:00';
% heureend='14:30:00';
% survey_name='PELGAS15';


path_save=[path_hac_survey,'/Result/',survey_name]
%If save path does not exist, create it
if ~exist(path_save,'dir')
    mkdir(path_save)
end

%  batch_EI_multiTHR(path_hac_survey,path_config,path_save,thresholds,runs,'',datestart,heurestart,dateend,heureend);
%     datestart=datenum(2010,1,18,23,50,0);
%     dateend=datenum(2010,1,19,00,45,00);


%% Bind EIlay results matrices

for ir=1:length(runs)
    for t=1:size(thresholds,2)
        str_run(ir,:)='000';        str_run(ir,end-length(num2str(runs(ir)))+1:end)=num2str(runs(ir));
        filename_ME70=[path_save,'/',survey_name,'-RUN' str_run(ir,:),'-TH' num2str(thresholds(t)),'-ME70-EIlay.mat'];
        filename_ER60=[path_save,'/',survey_name,'-RUN' str_run(ir,:),'-TH' num2str(thresholds(t)),'-ER60-EIlay.mat'];
        filename_ER60h=[path_save,'/',survey_name,'-RUN' str_run(ir,:),'-TH' num2str(thresholds(t)),'-ER60h-EIlay.mat'];
        path_results=[path_save,'/RUN' str_run(ir,:) '/',num2str(thresholds(:,t)),'/'];
%     EIlayRes_bind_mfr(path_results,filename_ME70,filename_ER60,filename_ER60h,datestart,heurestart,dateend,heureend);
    end
end

%% Load bound EIlay results
str_run='000'; str_run(end-length(num2str(runs))+1:end)=num2str(runs);
filename_ME70=[path_save,'/',survey_name,'-RUN' str_run,'-TH' num2str(thresholds),'-ME70-EIlay.mat'];
filename_ER60=[path_save,'/',survey_name,'-RUN' str_run,'-TH' num2str(thresholds),'-ER60-EIlay.mat'];
filename_ER60h=[path_save,'/',survey_name,'-RUN' str_run,'-TH' num2str(thresholds),'-ER60h-EIlay.mat'];
load(filename_ER60);
% load(filename_ER60h);
load(filename_ME70);

%% Exploratory plots
%choice for frequency
freqME70=11;
freqER60=2;


%on corrige l'�talonnage du 120kHz EK60 attention on fait que sur le Sv pour
%l'instant
% ind=find(str2num(datestr(time_ER60_db,'yyyy'))==2009);
% Sv_surfER60_db(ind,:,4)=Sv_surfER60_db(ind,:,4)-2;
% 
% ind=find(str2num(datestr(time_ER60_db,'yyyy'))==2010);
% Sv_surfER60_db(ind,:,4)=Sv_surfER60_db(ind,:,4)-1;
% 
% ind=find(str2num(datestr(time_ER60_db,'yyyy'))==2011);
% Sv_surfER60_db(ind,:,4)=Sv_surfER60_db(ind,:,4)-1;
% 
% ind=find(str2num(datestr(time_ER60_db,'yyyy'))==2015);
% Sv_surfER60_db(ind,:,4)=Sv_surfER60_db(ind,:,4)-1;
% 
% ind=find(str2num(datestr(time_ER60_db,'yyyy'))==2016);
% Sv_surfER60_db(ind,:,4)=Sv_surfER60_db(ind,:,4)-1;
% 
% ind=find(str2num(datestr(time_ER60_db,'yyyy'))==2015);
% Sv_surfER60_db(ind,:,3)=Sv_surfER60_db(ind,:,3)-1;


% choice for surface layer
startlayer=3;
if runs==4
endlayer=8;
elseif runs==3
    endlayer=14
end

isregular=1;


thresholdminSv=-100;
thresholdmaxSv=-45;

thresholdminSa=10;
thresholdmaxSa=6000;
if( runs==3)
%     %mauvaise classif PELGAS?
%     %     indnonAnchois=find( time_ER60_db>datenum('07/05/2015 08:35','dd/mm/yyyy HH:MM'));
%     indnonAnchois=1204;
%     Sv_surfER60_db(indnonAnchois,:,:)=nan;
%     Sa_surfER60_db(indnonAnchois,:,:)=nan;
%     Ni_surfER60_db(indnonAnchois,:,:)=nan;
%     Nt_surfER60_db(indnonAnchois,:,:)=nan;
%     indnonAnchois=1237;
%     Sv_surfME70_db(indnonAnchois,:,:)=nan;
%     Sa_surfME70_db(indnonAnchois,:,:)=nan;
%     Ni_surfME70_db(indnonAnchois,:,:)=nan;
%     Nt_surfME70_db(indnonAnchois,:,:)=nan;
%     

end


steering=[-42.20577 -35.92659 -30.43624 -25.51571 -21.02889 -16.88417 -13.01624 -9.376566 -5.927816 -2.640473 0.495642 3.601663 6.827438 10.21141 13.78197 17.57491 21.63671 26.02967 30.84084 36.19885 42.30876];
% steering=[  -38.6736 -29.9841 -22.5463 -15.9467 -9.9476 -4.3973 0.7788 5.8878 11.3002 17.1513 23.5864 30.8312 39.2774 ] ;
% frequency=[18 38 70 120 200 333];
frequency=[18 38 70 120 200];

depth_bottom_ME70_db=squeeze(depth_bottom_ME70_db);
depth_bottom_ER60_db=squeeze(depth_bottom_ER60_db);

% filtre les valeurs ne correspondant pas aux poissons
indnonfishME70=find(Sv_surfME70_db<=thresholdminSv);
Sv_surfME70_db(indnonfishME70)=nan;
Sa_surfME70_db(indnonfishME70)=nan;
Ni_surfME70_db(indnonfishME70)=nan;
Nt_surfME70_db(indnonfishME70)=nan;
indnonfishME70=find(Sv_surfME70_db>=thresholdmaxSv);
Sv_surfME70_db(indnonfishME70)=nan;
Sa_surfME70_db(indnonfishME70)=nan;
Ni_surfME70_db(indnonfishME70)=nan;
Nt_surfME70_db(indnonfishME70)=nan;

indnonfishER60=find(Sv_surfER60_db<=thresholdminSv);
Sv_surfER60_db(indnonfishER60)=nan;
Sa_surfER60_db(indnonfishER60)=nan;
Ni_surfER60_db(indnonfishER60)=nan;
Nt_surfER60_db(indnonfishER60)=nan;
indnonfishER60=find(Sv_surfER60_db>=thresholdmaxSv);
Sv_surfER60_db(indnonfishER60)=nan;
Sa_surfER60_db(indnonfishER60)=nan;
Ni_surfER60_db(indnonfishER60)=nan;
Nt_surfER60_db(indnonfishER60)=nan;

indnonfishME70=find(Sa_surfME70_db<=thresholdminSa);
Sv_surfME70_db(indnonfishME70)=nan;
Sa_surfME70_db(indnonfishME70)=nan;
Ni_surfME70_db(indnonfishME70)=nan;
Nt_surfME70_db(indnonfishME70)=nan;
indnonfishME70=find(Sa_surfME70_db>=thresholdmaxSa);
Sv_surfME70_db(indnonfishME70)=nan;
Sa_surfME70_db(indnonfishME70)=nan;
Ni_surfME70_db(indnonfishME70)=nan;
Nt_surfME70_db(indnonfishME70)=nan;

indnonfishER60=find(Sa_surfER60_db<=thresholdminSa);
Sv_surfER60_db(indnonfishER60)=nan;
Sa_surfER60_db(indnonfishER60)=nan;
Ni_surfER60_db(indnonfishER60)=nan;
Nt_surfER60_db(indnonfishER60)=nan;
indnonfishER60=find(Sa_surfER60_db>=thresholdmaxSa);
Sv_surfER60_db(indnonfishER60)=nan;
Sa_surfER60_db(indnonfishER60)=nan;
Ni_surfER60_db(indnonfishER60)=nan;
Nt_surfER60_db(indnonfishER60)=nan;


% filtre les valeurs en dehors du plateau

for f=1:size(depth_bottom_ME70_db,2)
    indnonfishME70=find(depth_bottom_ME70_db(:,f)>150/cosd(steering(f)));
    depth_bottom_ME70_db(indnonfishME70,:)=nan;
    Sv_surfME70_db(indnonfishME70,:,:)=nan;
    Sa_surfME70_db(indnonfishME70,:,:)=nan;
Ni_surfME70_db(indnonfishME70,:,:)=nan;
Nt_surfME70_db(indnonfishME70,:,:)=nan;
    
    indnonfishME70=find(depth_bottom_ME70_db(:,f)<15/cosd(steering(f)));
    depth_bottom_ME70_db(indnonfishME70,:)=nan;
    Sv_surfME70_db(indnonfishME70,:,:)=nan;
    Sa_surfME70_db(indnonfishME70,:,:)=nan;
Ni_surfME70_db(indnonfishME70,:,:)=nan;
Nt_surfME70_db(indnonfishME70,:,:)=nan;
end


for f=1:size(depth_bottom_ER60_db,2)
    indnonfishER60=find(abs(depth_bottom_ER60_db(:,f))>150);
    depth_bottom_ER60_db(indnonfishER60,:)=nan;
    Sv_surfER60_db(indnonfishER60,:,:)=nan;
    Sa_surfER60_db(indnonfishER60,:,:)=nan;
Ni_surfER60_db(indnonfishER60,:,:)=nan;
Nt_surfER60_db(indnonfishER60,:,:)=nan;
    
    indnonfishER60=find(abs(depth_bottom_ER60_db(:,f))<15);
    depth_bottom_ER60_db(indnonfishER60,:)=nan;
    Sv_surfER60_db(indnonfishER60,:,:)=nan;
    Sa_surfER60_db(indnonfishER60,:,:)=nan;
Ni_surfER60_db(indnonfishER60,:,:)=nan;
Nt_surfER60_db(indnonfishER60,:,:)=nan;
    
    
end

figure;
imagesc([1:size(Sa_surfER60_db,1)],depth_surface_ER60_db(1,:,4),max(Ni_surfER60_db./Nt_surfER60_db,[],3)')

figure;
imagesc([1:size(Sa_surfME70_db,1)],depth_surface_ME70_db(1,:,freqME70),nanmean(Sv_surfER60_db(:,:,:),3)')

% filtre les valeurs dans la zone aveugle du fond
for i=1:size(depth_bottom_ME70_db,1)
    if (~isnan(depth_bottom_ME70_db(i,1)) && ~isnan(depth_bottom_ME70_db(i,freqME70)))
        for j=round(depth_bottom_ME70_db(i,freqME70)/5):-1:round(depth_bottom_ME70_db(i,freqME70)/5)-round(depth_bottom_ME70_db(i,1)*tand(5)/5)
            Sv_surfME70_db(i,j-1,:)=nan;
            Sa_surfME70_db(i,j-1,:)=nan;
Ni_surfME70_db(i,j-1,:)=nan;
Nt_surfME70_db(i,j-1,:)=nan;
        end
    end
end

for i=1:size(depth_bottom_ER60_db,1)
    if (~isnan(depth_bottom_ER60_db(i,freqER60)))
        for j=round(depth_bottom_ER60_db(i,freqER60)/5):-1:round(depth_bottom_ER60_db(i,freqER60)/5)-round(depth_bottom_ER60_db(i,freqER60)/cosd(45)*tand(5)/5)
            Sv_surfER60_db(i,j-1,:)=nan;
            Sa_surfER60_db(i,j-1,:)=nan;
Ni_surfER60_db(i,j-1,:)=nan;
Nt_surfER60_db(i,j-1,:)=nan;
        end
    end
end

% on supprime la premi�re couche surface EK60 comme pour le ME70
Sv_surfER60_db(:,1,:)=nan;
Sa_surfER60_db(:,1,:)=nan;
Ni_surfER60_db(:,1,:)=nan;
Nt_surfER60_db(:,1,:)=nan;

figure;
imagesc([1:size(Sa_surfER60_db,1)],depth_surface_ER60_db(1,:,4),max(Ni_surfER60_db./Nt_surfER60_db,[],3)')

figure;
imagesc([1:size(Sa_surfME70_db,1)],depth_surface_ME70_db(1,:,freqME70),nanmean(Sv_surfER60_db(:,:,:),3)')

% filtre des couches diffusantes
for f=1:size(depth_surface_ME70_db,3)
    for d=1:size(depth_surface_ME70_db,2)
        indnonfishME70=find(Ni_surfME70_db(:,d,f)./Nt_surfME70_db(:,d,f)>0.2 );
         if ~isempty(indnonfishER60)
        Sv_surfME70_db(indnonfishME70,d,:)=nan;
        Sa_surfME70_db(indnonfishME70,d,:)=nan;
        Ni_surfME70_db(indnonfishME70,d,:)=nan;
         end
    end
end

for f=1:size(depth_surface_ER60_db,3)
    for d=1:size(depth_surface_ER60_db,2)
        indnonfishER60=find(Ni_surfER60_db(:,d,f)./Nt_surfER60_db(:,d,f)>0.2 );
        if ~isempty(indnonfishER60)
        Sv_surfER60_db(indnonfishER60,d,:)=nan;
        Sa_surfER60_db(indnonfishER60,d,:)=nan;
        Ni_surfER60_db(indnonfishER60,d,:)=nan;
        end
    end
end


figure;
imagesc([1:size(Sa_surfER60_db,1)],depth_surface_ER60_db(1,:,4),max(Ni_surfER60_db./Nt_surfER60_db,[],3)')

figure;
imagesc([1:size(Sa_surfME70_db,1)],depth_surface_ME70_db(1,:,freqME70),nanmean(Sv_surfER60_db(:,:,:),3)')

%l�gende
for d=1:endlayer-startlayer+1
    if depth_surface_ME70_db(1,d+startlayer-1,freqME70)<100
        strdepth(d,:)=['0' num2str(depth_surface_ME70_db(1,d+startlayer-1,freqME70)) 'm'];
    else
        strdepth(d,:)=[num2str(depth_surface_ME70_db(1,d+startlayer-1,freqME70)) 'm'];
    end
end

%Ship track and mean Sa

figure;
% coastline display with Matlab mapping toolbox
landareas = shaperead('landareas.shp','UseGeoCoords', true);
geoshow(landareas);
hold on
for freq=1:size(Sa_surfME70_db,3)
    scatter(mean(lon_surfME70_db(:,startlayer:endlayer,freqME70),2),mean(lat_surfME70_db(:,startlayer:endlayer,freqME70),2),(nanmean(Sa_surfME70_db(:,startlayer:endlayer,freqME70),2)),(nanmean(Sa_surfME70_db(:,startlayer:endlayer,freqME70),2)));
    
end
% text(mean(lon_surfME70_db((1:10:end),startlayer:endlayer,freqME70),2),mean(lat_surfME70_db((1:10:end),startlayer:endlayer,freqME70),2),datestr(time_ME70_db((1:10:end)),'dd/mm HH:MM'),'FontSize',6,'FontWeight','bold');
axis([min(lon_surfME70_db(:,1)) max(lon_surfME70_db(:,1)) min(lat_surfME70_db(:,1)) max(lat_surfME70_db(:,1))]);
axis([-7 0 43 49]);
title('Mean NASC for surface layers','FontSize',16);
colorbar;
xlabel('Longitude [degree]','FontSize',15);
ylabel('Latitude [degree]','FontSize',15);

%Bottom detection (errors?)
figure;
plot(time_ME70_db,depth_bottom_ME70_db(:,freqME70),'*')
hold on;
plot(time_ER60_db,depth_bottom_ER60_db(:,freqER60),'s')
datetickzoom
title('Bottom depth ME70/ER60','FontSize',16);
xlabel('Time','FontSize',15);
ylabel('Bottom depth','FontSize',15);

%max Sv 
figure;
plot(time_ME70_db,(squeeze(max(Sv_surfME70_db(1:end,startlayer:endlayer,:),[],3))'),'*');
hold on;
plot(time_ER60_db,(squeeze(max(Sv_surfER60_db(1:end,startlayer:endlayer,:),[],3))'),'s');
datetickzoom
title('Sv max ME70/ER60','FontSize',16);
xlabel('Time','FontSize',15);
ylabel('mean NASC','FontSize',15);

%max Sa 
figure;
plot(time_ME70_db,(squeeze(max(Sa_surfME70_db(1:end,startlayer:endlayer,:),[],3))'),'*');
hold on;
plot(time_ER60_db,(squeeze(max(Sa_surfER60_db(1:end,startlayer:endlayer,:),[],3))'),'s');
datetickzoom
title('Sa max ME70/ER60','FontSize',16);
xlabel('Time','FontSize',15);
ylabel('max NASC','FontSize',15);

%max Sv 
figure;
plot(time_ER60_db,(squeeze(nanmean(Sa_surfER60_db(1:end,startlayer:endlayer,3),2))'),'*');
hold on;
plot(time_ER60_db,(squeeze(nanmean(Sa_surfER60_db(1:end,startlayer:endlayer,4),2))'),'s');
datetickzoom
title('Sv max ME70/ER60','FontSize',16);
xlabel('Time','FontSize',15);
ylabel('mean NASC','FontSize',15);

 %Synthetic echogram
 if isregular
     figure;imagesc([1:size(Sa_surfME70_db,1)],depth_surface_ME70_db(1,startlayer:endlayer,freqME70),(squeeze(nanmean(Sa_surfME70_db(1:end,startlayer:endlayer,:),3))'));colorbar;grid on
     hold on;
     plot([1:size(Sa_surfME70_db,1)],depth_bottom_ME70_db(:,freqME70),'*w','linewidth',3)
     title('Synthetic echogram mean NASC','FontSize',16)
     xlabel('ESU','FontSize',15);
     ylabel('Depth','FontSize',15);
          saveas(gcf,[path_save,'MeanNASCME70along_','_',datestr([datestart ' ' heurestart],'dd-mmm-yyyy_HHMMSS')],'fig')
     saveas(gcf,[path_save,'MeanNASCME70along_','_',datestr([datestart ' ' heurestart],'dd-mmm-yyyy_HHMMSS')],'png')
 else   
     figure;
     h0=pcolor(Sa_surfME70_db(:,startlayer:endlayer,freq)');colorbar;grid on
     set(h0,'EdgeColor','none','FaceColor','flat')
     title('Synthetic echogram','FontSize',16)
     xlabel('ESU','FontSize',15);
     ylabel('Layer','FontSize',15);
 end
 
     figure;imagesc([1:size(Sa_surfME70_db,3)],depth_surface_ME70_db(1,startlayer:endlayer,freqME70),(squeeze(nanmean(Sa_surfME70_db(1:end,startlayer:endlayer,:),1))));colorbar;grid on
     hold on;
     title('Synthetic echogram mean NASC','FontSize',16)
     xlabel('','FontSize',15);
     ylabel('Depth','FontSize',15);
     saveas(gcf,[path_save,'MeanNASCME70athwart_','_',datestr([datestart ' ' heurestart],'dd-mmm-yyyy_HHMMSS')],'fig')
     saveas(gcf,[path_save,'MeanNASCME70athwart_','_',datestr([datestart ' ' heurestart],'dd-mmm-yyyy_HHMMSS')],'png')
     

      %Synthetic echogram
 if isregular
     figure;imagesc([1:size(Sa_surfER60_db,1)],depth_surface_ER60_db(1,startlayer:endlayer,freqER60),(squeeze(nanmean(Sa_surfER60_db(1:end,startlayer:endlayer,:),3))'));colorbar;grid on
     hold on;
     plot([1:size(Sa_surfER60_db,1)],depth_bottom_ER60_db(:,freqER60),'*w','linewidth',3)
     title('Synthetic echogram mean NASC','FontSize',16)
     xlabel('ESU','FontSize',15);
     ylabel('Depth','FontSize',15);
          saveas(gcf,[path_save,'MeanNASCER60along_','_',datestr([datestart ' ' heurestart],'dd-mmm-yyyy_HHMMSS')],'fig')
     saveas(gcf,[path_save,'MeanNASCER60along_','_',datestr([datestart ' ' heurestart],'dd-mmm-yyyy_HHMMSS')],'png')
 else   
     figure;
     h0=pcolor(Sa_surfER60_db(:,startlayer:endlayer,freq)');colorbar;grid on
     set(h0,'EdgeColor','none','FaceColor','flat')
     title('Synthetic echogram','FontSize',16)
     xlabel('ESU','FontSize',15);
     ylabel('Layer','FontSize',15);
 end
 
     figure;imagesc([1:size(Sa_surfER60_db,3)],depth_surface_ER60_db(1,startlayer:endlayer,freqER60),(squeeze(nanmean(Sa_surfER60_db(1:end,startlayer:endlayer,:),1))));colorbar;grid on
     hold on;
     title('Synthetic echogram mean NASC','FontSize',16)
     xlabel('','FontSize',15);
     ylabel('Depth','FontSize',15);
     saveas(gcf,[path_save,'MeanNASCER60athwart_','_',datestr([datestart ' ' heurestart],'dd-mmm-yyyy_HHMMSS')],'fig')
     saveas(gcf,[path_save,'MeanNASCER60athwart_','_',datestr([datestart ' ' heurestart],'dd-mmm-yyyy_HHMMSS')],'png')
   
     %normalisation
     normalisation=1;

   count=[]
     for d=1:size(Sv_surfME70_db,2)-1
         for f=1:size(Sv_surfME70_db,3)
             count(d,f)=sum(~isnan(Sv_surfME70_db(:,d,f)));
         end
     end
     figure;
     imagesc(steering,depth_surface_ME70_db(1,:,freqME70),count);
      title('Number of ESU for each depth for ME70','FontSize',16)
     xlabel('Beam steering angle (�)','FontSize',15);
     ylabel('Depth','FontSize',15);
     colorbar;
     saveas(gcf,[path_save,'NumberME70_','_',datestr([datestart ' ' heurestart],'dd-mmm-yyyy_HHMMSS')],'fig')
     saveas(gcf,[path_save,'NumberME70_','_',datestr([datestart ' ' heurestart],'dd-mmm-yyyy_HHMMSS')],'png')
   
        count=[]
          for d=1:size(Sv_surfER60_db,2)-1
         for f=1:size(Sv_surfER60_db,3)
             count(d,f)=sum(~isnan(Sv_surfER60_db(:,d,f)));
         end
     end
     figure;
     imagesc(frequency,depth_surface_ER60_db(1,:,freqER60),count);
       title('Number of ESU for each depth for EK60','FontSize',16)
     xlabel('Beam frequency (kHz)','FontSize',15);
     ylabel('Depth','FontSize',15);
     colorbar
     saveas(gcf,[path_save,'NumberEK60_','_',datestr([datestart ' ' heurestart],'dd-mmm-yyyy_HHMMSS')],'fig')
     saveas(gcf,[path_save,'NumberEK60_','_',datestr([datestart ' ' heurestart],'dd-mmm-yyyy_HHMMSS')],'png')


Svmean=nan(size(Sv_surfME70_db,3),endlayer-startlayer);
Svmedian=nan(size(Sv_surfME70_db,3),endlayer-startlayer);
Svpct=nan(size(Sv_surfME70_db,3),endlayer-startlayer,2);

for d=1:endlayer-startlayer+1
    
    for i=1:size(Sv_surfME70_db,3);
        Sv_angle(i,d,:)=reshape(Sv_surfME70_db(1:end,d+startlayer-1,i),[],1);
    end
    
    for i = 1:size(Sv_angle,1);
        index=~isnan(Sv_angle(i,d,:));
        temp = 10.^(Sv_angle(i,d,:)/10);           % Get sigma for lengths and phi
        Svmean(i,d) = 10*log10(mean(temp(index)))  ;          % Calculate mean
        Svmedian(i,d) = 10*log10(median(temp(index)))  ;          % Calculate mean
        Svpct(i,d,:) = 10*log10(prctile(temp(index), [5 95]));  % Calculate 95%
    end
    if (normalisation==1)
    Svmean(:,d)=Svmean(:,d)-Svmean(freqME70,d);
    end
end

if (runs==3)
Svmean_new=[10*log10(mean(10.^(Svmean(:,1:2)./10),2)) 10*log10(mean(10.^(Svmean(:,3:4)./10),2)) 10*log10(mean(10.^(Svmean(:,5:6)./10),2)) 10*log10(mean(10.^(Svmean(:,7:8)./10),2)) 10*log10(mean(10.^(Svmean(:,9:10)./10),2)) 10*log10(mean(10.^(Svmean(:,11:12)./10),2)) ];
else
    Svmean_new=Svmean
end


col='rmgcbky';

figure;
hold on;
for i=1:size(Svmean_new,2)
    
%     errorbar(steering,Svmedian(:,i),Svmedian(:,i)-Svpct(:,i,1),Svpct(:,i,2)-Svmedian(:,i),[col(1+mod(i,6))],'LineWidth',3);
end
for i=1:size(Svmean_new,2)
    
    plot(steering,Svmean_new(:,i),'Color',[col(1+mod(i,7))],'LineStyle','-', 'LineWidth',3);
end
    toto=xlabel('Steering (�)');
    set(toto,'FontSize',16)
    toto=ylabel('Sv_{norm} (dB)');
    set(toto,'FontSize',16)
    
    set(gca,'FontSize',14)
%     title(['Mean normalized Sv versus beam axis steering '])
    hold on;
    grid on;
    if (normalisation)
        axis([steering(1) steering(end) -5 3])
    end
    
     legend(strdepth);
          saveas(gcf,[path_save,'SvAngle_','_',datestr([datestart ' ' heurestart],'dd-mmm-yyyy_HHMMSS')],'fig')
     saveas(gcf,[path_save,'SvAngle_','_',datestr([datestart ' ' heurestart],'dd-mmm-yyyy_HHMMSS')],'png')


Svfmean=nan(size(Sv_surfER60_db,3),endlayer-startlayer);
Svfmedian=nan(size(Sv_surfER60_db,3),endlayer-startlayer);
Svfpct=nan(size(Sv_surfER60_db,3),endlayer-startlayer,2);

for d=1:endlayer-startlayer+1
    for i=1:size(Sv_surfER60_db,3)
        Sv_freq(i,d,:)=reshape(Sv_surfER60_db(1:end,d+startlayer-1,i),[],1);
    end
    
    for i = 1:size(Sv_freq,1)
        index=~isnan(Sv_freq(i,d,:));
        temp = 10.^(Sv_freq(i,d,:)/10);           % Get sigma for lengths and phi
         Svfmean(i,d) = 10*log10(mean(temp(index)))  ;          % Calculate mean
        Svfmedian(i,d) = 10*log10(median(temp(index)))  ;           % Calculate mean
        Svfpct(i,d,:) =10*log10(prctile(temp(index), [5 95]));  % Calculate 95%
    end
        if (normalisation==1)
    Svfmean(:,d)=Svfmean(:,d)-Svfmean(freqER60,d);
    end
end

if (runs==3)
Svfmean_new=[10*log10(mean(10.^(Svfmean(:,1:2)./10),2)) 10*log10(mean(10.^(Svfmean(:,3:4)./10),2)) 10*log10(mean(10.^(Svfmean(:,5:6)./10),2)) 10*log10(mean(10.^(Svfmean(:,7:8)./10),2)) 10*log10(mean(10.^(Svfmean(:,9:10)./10),2)) 10*log10(mean(10.^(Svfmean(:,11:12)./10),2)) ];
else
    Svfmean_new=Svfmean;
end


figure;
hold on;
  for i=1:size(Svfmean_new,2)

%       errorbar(frequency,Svfmedian(:,i),Svfmedian(:,i)-Svfpct(:,i,1),Svfpct(:,i,2)-Svfmedian(:,i),[col(1+mod(i,6))],'LineWidth',3);
      
  end
    for i=1:size(Svfmean_new,2)

     
       plot(frequency,Svfmean_new(:,i),'Color',[col(1+mod(i,7))],'LineStyle','-', 'LineWidth',3);
  end
    toto=xlabel('Frequency (kHz)');
    set(toto,'FontSize',16)
    toto=ylabel('Sv_{norm} (dB)');
    set(toto,'FontSize',16)
    
    set(gca,'FontSize',14)
%     title(['Mean Sv versus frequency '])
    hold on;
    grid on;
    if (normalisation)
        axis([frequency(1) frequency(end) -2 4])
    end
       legend(strdepth);
         saveas(gcf,[path_save,'SvFreq_','_',datestr([datestart ' ' heurestart],'dd-mmm-yyyy_HHMMSS')],'fig')
     saveas(gcf,[path_save,'SvFreq_','_',datestr([datestart ' ' heurestart],'dd-mmm-yyyy_HHMMSS')],'png')
     

     Sv_surfER60_db_match=NaN(endlayer-startlayer+1,size(Sv_surfER60_db,1));
     Sv_surfME70_db_match=NaN(endlayer-startlayer+1,size(Sv_surfER60_db,1));
   diffER_ME=NaN(endlayer-startlayer+1,size(Sv_surfER60_db,1));
     for i=1:endlayer-startlayer+1
         k=1;
        for j=1:size(Sv_surfER60_db,1)
            ind=find(abs(time_ER60_db(j)-time_ME70_db(:))<10/86400);
            if ~isempty(ind)
                Sv_surfER60_db_match(i,k)=10*log10(nanmean(10.^(Sv_surfER60_db(j,i+startlayer-1,3:4)/10)));
                Sv_surfME70_db_match(i,k)=10*log10(nanmean(10.^(Sv_surfME70_db(ind(1),i+startlayer-1,:)/10)));
                diffER_ME(i,k)=10*log10(nanmean(10.^(Sv_surfER60_db(j,i+startlayer-1,4)/10)))-10*log10(nanmean(10.^(Sv_surfME70_db(ind(1),i+startlayer-1,:)/10)));
                k=k+1;
            end
        end
     end

      for i=1:size(diffER_ME,1)
          diffER_ME_mean(i)=10*log10(nanmean(10.^(Sv_surfER60_db_match(i,:)/10)))-10*log10(nanmean(10.^(Sv_surfME70_db_match(i,:)/10)));
      end
      
      if (runs==3)
diffER_ME_mean=[10*log10(mean(10.^(diffER_ME_mean(1:2)./10))) 10*log10(mean(10.^(diffER_ME_mean(3:4)./10))) 10*log10(mean(10.^(diffER_ME_mean(5:6)./10))) 10*log10(mean(10.^(diffER_ME_mean(7:8)./10))) 10*log10(mean(10.^(diffER_ME_mean(9:10)./10))) 10*log10(mean(10.^(diffER_ME_mean(11:12)./10))) ];
diffER_ME=[[diffER_ME(1,:) diffER_ME(2,:)]' [diffER_ME(3,:) diffER_ME(4,:)]' [diffER_ME(5,:) diffER_ME(6,:)]' [diffER_ME(7,:) diffER_ME(8,:)]' [diffER_ME(9,:) diffER_ME(10,:)]' [diffER_ME(11,:) diffER_ME(12,:)]']';
      else
    diffER_ME_mean=diffER_ME_mean;
      end

      diffER_ME_median=nan(size(diffER_ME,1),1);
      diffER_ME_pct=nan(size(diffER_ME,1),2);
            for i=1:size(diffER_ME,1)
                index=~isnan(diffER_ME(i,:));
                 diffER_ME_median(i) =nanmedian(diffER_ME(i,index));  % Calculate 95%
                diffER_ME_pct(i,:) =prctile(diffER_ME(i,index), [25 75]);  % Calculate 95%
            end

      
     figure;
    
       if (runs==3)
            plot([25 35 45 55 65 75],diffER_ME_pct(:,1), 'b','LineWidth',1);
     hold on;
          plot([25 35 45 55 65 75],diffER_ME_pct(:,2), 'b','LineWidth',1);
     plot([25 35 45 55 65 75],diffER_ME_mean, 'b','LineWidth',3);
     
       else
            plot(depth_surface_ME70_db(1,startlayer:endlayer,freqME70),diffER_ME_pct(:,1), 'b','LineWidth',1);
     hold on;
      plot(depth_surface_ME70_db(1,startlayer:endlayer,freqME70),diffER_ME_pct(:,2), 'b','LineWidth',1);
           plot(depth_surface_ME70_db(1,startlayer:endlayer,freqME70),diffER_ME_mean,'b', 'LineWidth',3);
       end
     xlabel('Depth','FontSize',16);
     ylabel('Sv difference between EK and ME(dB)','FontSize',16);
     grid on;
         set(gca,'FontSize',14)
         ylim([-12 12])
     
         saveas(gcf,[path_save,'SvDiffEKME_','_',datestr([datestart ' ' heurestart],'dd-mmm-yyyy_HHMMSS')],'fig')
     saveas(gcf,[path_save,'SvDiffEKME_','_',datestr([datestart ' ' heurestart],'dd-mmm-yyyy_HHMMSS')],'png')

% diffER_ME=NaN(endlayer-startlayer+1,size(Sv_surfER60_db,1));
%      for i=1:endlayer-startlayer+1
%          k=1;
%         for j=1:size(Sv_surfER60_db,1)
% %             if ~isnan(Sv_surfER60_db(j,i+startlayer-1,4)) & ~isnan(Sv_surfER60_db(j,i+startlayer-1,3))
%                 diffER_ME(i,k)=Sv_surfER60_db(j,i+startlayer-1,4)-Sv_surfER60_db(j,i+startlayer-1,3);
%                 k=k+1;
% %             end
%         end
%      end
% 
%      figure;
%      plot(depth_surface_ME70_db(1,startlayer:endlayer,freqME70),diffER_ME,'o');
%      hold on;
%      plot(depth_surface_ME70_db(1,startlayer:endlayer,freqME70),Svfmean(:,4)-Svfmean(:,3));
%      xlabel('Depth','FontSize',15);
%      ylabel('Difference between EK and ME(dB)','FontSize',15);
%      grid on;
     


%      
% 
% Samean=nan(size(Sa_surfME70_db,3),endlayer-startlayer);
% Samedian=nan(size(Sa_surfME70_db,3),endlayer-startlayer);
% Sapct=nan(size(Sa_surfME70_db,3),endlayer-startlayer,2);
% 
% for d=1:endlayer-startlayer+1
%     
%     for i=1:size(Sa_surfME70_db,3);
%         Sa_angle(i,d,:)=reshape(Sa_surfME70_db(1:end,d+startlayer-1,i),[],1);
%     end
%     
%     for i = 1:size(Sa_angle,1);
%         index=~isnan(Sa_angle(i,d,:));
%         temp = Sa_angle(i,d,:);           % Get sigma for lengths and phi
%         Samean(i,d) = (mean(temp(index)))  ;          % Calculate mean
%         Samedian(i,d) =(median(temp(index)))  ;          % Calculate mean
%         Sapct(i,d,:) = (prctile(temp(index), [5 95]));  % Calculate 95%
%     end
% end
% 
% col='rmgcbky';
% 
% figure;
% hold on;
% for i=1:size(Samedian,2)
%     
% %     errorbar(steering,10*log10(Samedian(:,i)),10*log10(Samedian(:,i))-10*log10(Sapct(:,i,1)),10*log10(Sapct(:,i,2))-10*log10(Samedian(:,i))),[col(1+mod(i,7))],'LineWidth',3);
% end
% for i=1:size(Samedian,2)
%     
%     plot(steering,(10*log10(Samean(:,i))),'Color',[col(1+mod(i,7))],'LineStyle','--', 'LineWidth',3);
% end
%     toto=xlabel('Steering (�)');
%     set(toto,'FontSize',16)
%     toto=ylabel('Sa (dB)');
%     set(toto,'FontSize',16)
%     
%     set(gca,'FontSize',14)
%     title(['Mean Sa versus beam axis steering '])
%     hold on;
%     grid on;
%      if (normalisation)
%     axis([steering(1) steering(end) -40 40])
%      end
%      legend(strdepth);
%           saveas(gcf,[path_save,'SaAngle_','_',datestr([datestart ' ' heurestart],'dd-mmm-yyyy_HHMMSS')],'fig')
%      saveas(gcf,[path_save,'SaAngle_','_',datestr([datestart ' ' heurestart],'dd-mmm-yyyy_HHMMSS')],'png')
% % 
% % indnonfishER60=find(Sa_surfER60_db<thresholdmin | Sa_surfER60_db>thresholdmax);
% % Sa_surfER60_db(indnonfishER60)=nan;
% 
% Safmean=nan(size(Sa_surfER60_db,3),endlayer-startlayer);
% Safmedian=nan(size(Sa_surfER60_db,3),endlayer-startlayer);
% Safpct=nan(size(Sa_surfER60_db,3),endlayer-startlayer,2);
% 
% for d=1:endlayer-startlayer+1
%     for i=1:size(Sa_surfER60_db,3)
%         Sa_freq(i,d,:)=reshape(Sa_surfER60_db(1:end,d+startlayer-1,i),[],1);
%     end
%     
%     for i = 1:size(Sa_freq,1)
%         index=~isnan(Sa_freq(i,d,:));
%         temp = Sa_freq(i,d,:);           % Get sigma for lengths and phi
%         Safmean(i,d) = (mean(temp(index)))  ;          % Calculate mean
%         Safmedian(i,d) = (median(temp(index)))  ;          % Calculate median
%         Safpct(i,d,:) = (prctile(temp(index), [5 95]));  % Calculate 95%
%     end
% end
% 
% 
% frequency=[18 38 70 120 200];
% % frequency=[38 70 120 200 333];
% figure;
% hold on;
%   for i=1:size(Safmedian,2)
% 
% %       errorbar(frequency,10*log10(Safmedian(:,i)),10*log10(Safmedian(:,i)-Safpct(:,i,1)),10*log10(Safpct(:,i,2)-Safmedian(:,i)),[col(1+mod(i,7))],'LineWidth',3);
%       
%   end
%   for i=1:size(Safmedian,2)
%       
%       
%       plot(frequency,10*log10(Safmean(:,i)),'Color',[col(1+mod(i,7))],'LineStyle','--', 'LineWidth',3);
%   end
%   toto=xlabel('Frequency (kHz)');
%     set(toto,'FontSize',16)
%     toto=ylabel('Sa (dB)');
%     set(toto,'FontSize',16)
%     
%     set(gca,'FontSize',14)
% %     title(['Mean Sa versus frequency '])
%     hold on;
%     grid on;
%        legend(strdepth);
%         if (normalisation)
%          axis([frequency(1) frequency(end) -40 40])
%         end
%          saveas(gcf,[path_save,'SaFreq_','_',datestr([datestart ' ' heurestart],'dd-mmm-yyyy_HHMMSS')],'fig')
%      saveas(gcf,[path_save,'SaFreq_','_',datestr([datestart ' ' heurestart],'dd-mmm-yyyy_HHMMSS')],'png')
%      
%    diffER_ME=NaN(endlayer-startlayer+1,size(Sa_surfER60_db,1));
%      for i=1:endlayer-startlayer+1
%          k=1;
%         for j=1:size(Sa_surfER60_db,1)
%             if ~isnan(Sa_surfER60_db(j,i+startlayer-1,4)) & ~isnan(Sa_surfME70_db(j,i+startlayer-1,freqME70))
%                 diffER_ME(i,k)=Sa_surfER60_db(j,i+startlayer-1,4)-Sa_surfME70_db(j,i+startlayer-1,freqME70);
%                 k=k+1;
%             end
%         end
%      end
% 
%      figure;
%      plot(depth_surface_ME70_db(1,startlayer:endlayer,freqME70),diffER_ME,'o');
%      hold on;
%      plot(depth_surface_ME70_db(1,startlayer:endlayer,freqME70),nanmean(diffER_ME,2));
%      xlabel('Depth','FontSize',15);
%      ylabel('Difference between EK and ME(dB)','FontSize',15);
%      
%          saveas(gcf,[path_save,'SaDiffEKME_','_',datestr([datestart ' ' heurestart],'dd-mmm-yyyy_HHMMSS')],'fig')
%      saveas(gcf,[path_save,'SaDiffEKME_','_',datestr([datestart ' ' heurestart],'dd-mmm-yyyy_HHMMSS')],'png')