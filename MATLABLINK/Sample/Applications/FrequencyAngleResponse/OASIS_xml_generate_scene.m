clear all
close all


% <o>< ><o> <o>< ><o> <o><    Configurations    ><o> <o>< ><o> <o>< ><o> %

% Cette partie du code regroupe toute les variables du programme � d�finir
% et � v�rifier avant l'�x�cution.

%%%%%%%%%%%%%%%%%%%%%%% Mouvement bateau %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
chemin = 'L:\PELGAS\DataEspecesPuresFiltre\Simu';
FileSardine='L:\PELGAS\DataEspecesPuresFiltre\Simu\Sardine.txt';

vbat=10;               %vitesse du bateau en noeud
D=500;                 %distance parcouru par le navire


RollAmplitude=0;
RollPeriod=0;
PitchAmplitude=0;
PitchPeriod=0;
RollInit=0;
PitchInit=0;
FechCentraleNav=10;
HeaveAmplitude=0;
HeavePeriod=0;
HeaveInit=0;
DepthAttitude=0;

%Latitude initial
latDeg=48 ;  %degr�s
latMin=0 ;   %minutes
latSec=0 ;   %seconde
latO=0 ;

%longitude initial
lonDeg=3 ; %degr�s
lonMin=0 ;  %minutes
lonSec=0 ;  %seconde
lonO=0 ;

%position initial des sondeurs
L=0; %position sur x
l=0; %position sur y
H=7;  %position sur z

%cap du bateau
heading=0; %en degr�s

%%%%%%%%%%%%%%%%%%%%%%% Fonf marin %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%simulation du fond marrin
AvecSansFond =0;   % 0: sans, 1: avec


%%%%%%%%%%%%%%%%%%%%%%% Bancs de poissons et biomasse %%%%%%%%%%%%%%%%%%%%%


%esp�ces de poissons : joue sur le TS
Espece=1;    % 1:sardine  0:anchois

if Espece==1
    taillecible=6;                %taille moyenne des poissons en cm
    tailleciblesd=1;
else
    taillecible=4;                %taille moyenne des poissons en cm
    tailleciblesd=1;
end

TS=-45;

%%%%%%%%%%%%%%%%%%%%%%%% Propri�t�s des sondeurs %%%%%%%%%%%%%%%%%%%%%%%%%%

depointageMax=45; %d�pointage maximum en multifaisceau

Rmin =5;  %range minimum

%dimension dans les 3 dimensions du volume fixe
D1=130;      %dimension en X
D2=130;     %dimension en Y
D3=10;       %dimension en Z

Xp=1.368;
Yp=0.885;
Zp=0.855;

Xpstd=0.1368;
Ypstd=0.0855;
Zpstd=0.0855;

%forme bancs
forme=1;    % 0 ellisoide, 1 parall�lip�dique

% orientation des bancs autour des trois axes en "degr�s*pi/180=>radians"
roll=0;      % rotation autour de X
pitch=-30;     % rotation autour de Y
NbSchool=100;
yaw=180*(rand(1,NbSchool)-0.5);       % rotation autour de Z

Rollstd=15;      % rotation autour de X
Pitchstd=15;     % rotation autour de Y
Yawstd=15;       % rotation autour de Z

%%%%%%position des bancs%%%%%
PositionCentreX=150;
PositionCentreY=0;   % profondeur du centre des bancs
PositionCentreZ=40;      %0:le navire est centr� sur le banc du centre

%l'offset en X sert � d�caler l'ensemble des bancs de poissons dans la
%direction X. Si les premier bancs sont hortogonalement sous la position
%initiale du navire ils ne seront pas vu.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% <o>< ><o> <o>< ><o> <o>< ><o> <o>< ><o> <o>< ><o> <o>< ><o> <o>< ><o> %

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Programme %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


    %recherche du banc le plus profond afin de calculer la profondeur
    %du fond
    ProfondeurFond = 100
   
    TetaMono=7;
    TetaMultiext=6;  %ouverture � -3dB pour multifaisceau
    TetaMulticentre=3;
    AngleMono=TetaMono ;
    AngleMulti=2*(depointageMax+TetaMultiext/2);
        AngleMultiCentre=TetaMulticentre;
    
    % Calcul de la demie base du c�ne d'insonification et du range max pour
    % chaque sondeur
    kMono=tan(((AngleMono*pi)/180)/2)*ProfondeurFond;
    kMulti=tan(((AngleMulti*pi)/180)/2)*ProfondeurFond;
    kMultiCentre=tan(((AngleMultiCentre*pi)/180)/2)*ProfondeurFond;
    RmaxMono=sqrt(ProfondeurFond^2+kMono^2);
    RmaxMulti=sqrt(ProfondeurFond^2+kMulti^2);


    % table de r�currence des pings
    % Pronfondeur  temps interPingds
    % 20-30             0,35
    % 30-50             0,4
    % 50-70             0,45
    % 70-90             0,5
    % 90-115            0,6
    % 115-140           0,7
    % 140-165           0,8
    % 165-190           0,9
    % 190-215            1
    % 215-240           1,1
    % 240-250           1,2


    if ProfondeurFond>=20 && ProfondeurFond<30
        InterPing=0.35;
    elseif ProfondeurFond>=30 && ProfondeurFond<50
        InterPing=0.4;
    elseif ProfondeurFond>=50 && ProfondeurFond<70
        InterPing=0.45;
    elseif ProfondeurFond>=70 && ProfondeurFond<90
        InterPing=0.5;
    elseif ProfondeurFond>=90 && ProfondeurFond<115
        InterPing=0.6;
    elseif ProfondeurFond>=115 && ProfondeurFond<140
        InterPing=0.7;
    elseif ProfondeurFond>=140 && ProfondeurFond<165
        InterPing=0.8;
    elseif ProfondeurFond>=165 && ProfondeurFond<190
        InterPing=0.9;
    elseif ProfondeurFond>=190 && ProfondeurFond<215
        InterPing=1;
    elseif ProfondeurFond>=215 && ProfondeurFond<240
        InterPing=1.1;
    elseif ProfondeurFond>=240 && ProfondeurFond<250
        InterPing=1.2;
    end

    % Calculde la distance entre deux pings et du nombre de pings
    dinterPing=vbat*InterPing; %distance entre deux pings en m�tre
    NbPing=round(D/dinterPing);%nombre total de ping

    dirScene=sprintf('scene_%g_%g_%g',Espece,abs(pitch),Pitchstd);
    mkdir([chemin,'\', dirScene,'\mono']);
        mkdir([chemin,'\', dirScene,'\multi']);
        

    %%%%%%%%%%%%%%%Ecriture du fichier bancsfin_mono.xml%%%%%%%%%%%%%%%%%

    %Nom du fichier XML qui va �tre g�n�r�
    for i=1:NbSchool

        if Espece==0
            Oasisfilenamemono = sprintf('scene_mono_anchois_%g_%g_%03g_%g.oasis',Espece,abs(pitch), round(abs(10*yaw(i))),pitchstd);
        elseif Espece==1
            Oasisfilenamemono = sprintf('scene_mono_sardine_%g_%g_%03g_%g.oasis',Espece,abs(pitch), round(abs(10*yaw(i))),Pitchstd);
        end

        fid = fopen([chemin,'\', dirScene  '\mono\' Oasisfilenamemono], 'w+');
        fprintf(fid,'<?xml version="1.0" encoding="UTF-8" standalone="no" ?>');
        fprintf(fid,'\n<APPLICATION APPNAME="OASIS" APPVERSION_MAJ="3" APPVERSION_MIN="01">');
        fprintf(fid,'\n <Configuration TYPE="Scenario">');

        %param�tres concernant l'environnement:
        fprintf(fid,'\n  <Configuration TYPE="ConfigEnv">');
        fprintf(fid,'\n   <PARAMETER NAME="SeaTemperature" VALUE="12.6" />');
        fprintf(fid,'\n   <PARAMETER NAME="Salinity" VALUE="35.4" />');
        fprintf(fid,'\n   <PARAMETER NAME="PH" VALUE="8" />');
        fprintf(fid,'\n   <PARAMETER NAME="WindSpeed" VALUE="15" />');
        fprintf(fid,'\n   <PARAMETER NAME="SelfNoiseLevel1Hz" VALUE="173" />');
        fprintf(fid,'\n   <PARAMETER NAME="PropellerPosition" VALUE-X="-40" VALUE-Y="0" VALUE-Z="0" />');
        fprintf(fid,'\n  </Configuration>');

        %param�tres concernant la sc�ne:
        fprintf(fid,'\n  <Configuration TYPE="ConfigScene">');
        fprintf(fid,'\n   <PARAMETER NAME="HasSeaFloor" VALUE="%d" />',AvecSansFond);
        fprintf(fid,'\n   <PARAMETER NAME="SeaFloorName" VALUE="Seafloor Name" />');
        fprintf(fid,'\n   <PARAMETER NAME="H" VALUE="%d" />',ProfondeurFond);
        fprintf(fid,'\n   <PARAMETER NAME="PhiBottom" VALUE="0" />');
        fprintf(fid,'\n   <PARAMETER NAME="PsiBottom" VALUE="0" />');
        fprintf(fid,'\n   <PARAMETER NAME="m10loga1" VALUE="-5" />');
        fprintf(fid,'\n   <PARAMETER NAME="a2" VALUE="180" />');
        fprintf(fid,'\n   <PARAMETER NAME="m10loga3" VALUE="-25" />');
        fprintf(fid,'\n   <PARAMETER NAME="Prof" VALUE="50" />');
        fprintf(fid,'\n   <PARAMETER NAME="BottomAngleSampling" VALUE="10" />');
        fprintf(fid,'\n   <PARAMETER NAME="NivMinIntercor" VALUE="-60" />');
        fprintf(fid,'\n   <PARAMETER NAME="NivMinGabarit" VALUE="-60" />');
        fprintf(fid,'\n   <PARAMETER NAME="Seed" VALUE="1" />');

        %param�tre concernant les bancs:
        fprintf(fid,'\n  <Configuration TYPE="Schools">');

     


        
        fprintf(fid,'\n  <School NAME="a">');
        fprintf(fid,'\n   <PARAMETER NAME="TSMode" VALUE="2" />');
        fprintf(fid,'\n   <PARAMETER NAME="TS" VALUE="%f" /> ',TS);
        fprintf(fid,'\n   <PARAMETER NAME="FreqList" VALUE="1;" /> ');
        fprintf(fid,'\n   <PARAMETER NAME="TSList" VALUE="%f" />',TS);
        fprintf(fid,'\n   <PARAMETER NAME="FishDirFile" VALUE="%s" />',FileSardine);
        fprintf(fid,'\n   <PARAMETER NAME="Position" VALUE-X="%f" VALUE-Y="%f" VALUE-Z="%f"/>',PositionCentreX,PositionCentreY,PositionCentreZ);
        fprintf(fid,'\n   <PARAMETER NAME="Orientation" VALUE-X="%f" VALUE-Y="%f" VALUE-Z="%f"/>',roll,pitch,yaw(i));
        fprintf(fid,'\n   <PARAMETER NAME="Dimension" VALUE-X="%f" VALUE-Y="%f" VALUE-Z="%f"/>',D1,D2,D3);
        fprintf(fid,'\n   <PARAMETER NAME="Spacing" VALUE-X="%f" VALUE-Y="%f" VALUE-Z="%f"/>',Xp,Yp,Zp);
        fprintf(fid,'\n   <PARAMETER NAME="Deviation" VALUE-X="%f" VALUE-Y="%f" VALUE-Z="%f"/>',Xpstd,Ypstd,Zpstd);
        fprintf(fid,'\n   <PARAMETER NAME="Seed" VALUE="1"/>');
        fprintf(fid,'\n   <PARAMETER NAME="OrientationDeviation" VALUE-X="%f" VALUE-Y="%f" VALUE-Z="%f"/>',Rollstd,Pitchstd,Yawstd);
        fprintf(fid,'\n   <PARAMETER NAME="OrientationSeed" VALUE="1"/>');
        fprintf(fid,'\n   <PARAMETER NAME="FishLength" VALUE="%f"/>',taillecible/100);
        fprintf(fid,'\n   <PARAMETER NAME="FishLengthSeed" VALUE="1"/>');
        fprintf(fid,'\n   <PARAMETER NAME="FishLengthDeviation" VALUE="%f"/>',tailleciblesd/100);
        fprintf(fid,'\n   <PARAMETER NAME="SchoolType" VALUE="%d"/>',forme);
        fprintf(fid,'\n   <PARAMETER NAME="DiffuserModelIndex" VALUE="7"/>');
        fprintf(fid,'\n  </School>');
                    


        fprintf(fid,'\n  </Configuration>');
        
        fprintf(fid,'\n   <Configuration TYPE="DiffuserModels">');
        fprintf(fid,'\n   <DiffuserModel NAME="TFS Holliday">');
        fprintf(fid,'\n   <PARAMETER NAME="g" VALUE="1.12"/>');
        fprintf(fid,'\n   <PARAMETER NAME="h" VALUE="1.09"/>');
        fprintf(fid,'\n   </DiffuserModel>');
        fprintf(fid,'\n   <DiffuserModel NAME="Fluid Sphere Stanton">');
        fprintf(fid,'\n   <PARAMETER NAME="g" VALUE="1.043"/>');
        fprintf(fid,'\n   <PARAMETER NAME="h" VALUE="1.05"/>');
        fprintf(fid,'\n   </DiffuserModel>');
        fprintf(fid,'\n   <DiffuserModel NAME="Fluid Prolate Spheroid Stanton">');
        fprintf(fid,'\n   <PARAMETER NAME="g" VALUE="1.043"/>');
        fprintf(fid,'\n   <PARAMETER NAME="h" VALUE="1.05"/>');
        fprintf(fid,'\n   <PARAMETER NAME="La" VALUE="5"/>');
        fprintf(fid,'\n   </DiffuserModel>');
        fprintf(fid,'\n   <DiffuserModel NAME="Fluid Bent Cylinder 93">');
        fprintf(fid,'\n   <PARAMETER NAME="g" VALUE="1.0357"/>');
        fprintf(fid,'\n   <PARAMETER NAME="h" VALUE="1.0279"/>');
        fprintf(fid,'\n   <PARAMETER NAME="La" VALUE="16"/>');
        fprintf(fid,'\n   <PARAMETER NAME="phoL" VALUE="3"/>');
        fprintf(fid,'\n   <PARAMETER NAME="s" VALUE="0.06"/>');
        fprintf(fid,'\n   <PARAMETER NAME="Incident Angle" VALUE="20"/>');
        fprintf(fid,'\n   <PARAMETER NAME="Angle Std Dev" VALUE="20"/>');
        fprintf(fid,'\n   </DiffuserModel>');
        fprintf(fid,'\n   <DiffuserModel NAME="Fluid Bent Cylinder 94">');
        fprintf(fid,'\n   <PARAMETER NAME="s" VALUE="0.06"/>');
        fprintf(fid,'\n   <PARAMETER NAME="R" VALUE="0.058"/>');
        fprintf(fid,'\n   <PARAMETER NAME="La" VALUE="16"/>');
        fprintf(fid,'\n   </DiffuserModel>');
        fprintf(fid,'\n   <DiffuserModel NAME="Elastic Sphere">');
        fprintf(fid,'\n   <PARAMETER NAME="R" VALUE="0.5"/>');
        fprintf(fid,'\n   </DiffuserModel>');
        fprintf(fid,'\n   <DiffuserModel NAME="Gaseous Prolate Spheroid">');
        fprintf(fid,'\n   <PARAMETER NAME="gsurf" VALUE="0.0012"/>');
        fprintf(fid,'\n   <PARAMETER NAME="h" VALUE="0.22"/>');
        fprintf(fid,'\n   <PARAMETER NAME="La" VALUE="5"/>');
        fprintf(fid,'\n   </DiffuserModel>');
        fprintf(fid,'\n   <DiffuserModel NAME="Gaseous Sphere">');
        fprintf(fid,'\n   <PARAMETER NAME="gsurf" VALUE="0.0012"/>');
        fprintf(fid,'\n   <PARAMETER NAME="h" VALUE="0.22"/>');
        fprintf(fid,'\n   </DiffuserModel>');
        fprintf(fid,'\n   </Configuration>');
      
            fprintf(fid,'\n  </Configuration>');

        fprintf(fid,'\n  <Configuration TYPE="ConfigSignals">');
        fprintf(fid,'\n   <PARAMETER NAME="PulseLength" VALUE="0.001" />');
        fprintf(fid,'\n   <PARAMETER NAME="ModulationType" VALUE="1" /> ');
        fprintf(fid,'\n   <PARAMETER NAME="BetaFactor" VALUE="-20" /> ');
        fprintf(fid,'\n   <PARAMETER NAME="Fdemod" VALUE="109000" /> ');
        fprintf(fid,'\n   <PARAMETER NAME="Fech" VALUE="10000" /> ');
        fprintf(fid,'\n   <PARAMETER NAME="TypeSelection" VALUE="0" />');
        fprintf(fid,'\n   <PARAMETER NAME="Tsig" VALUE="0.003" /> ');
        fprintf(fid,'\n   <PARAMETER NAME="EchoesWindowLength" VALUE="0.005" />');
        fprintf(fid,'\n  </Configuration>');

        fprintf(fid,'\n  <Configuration TYPE="ConfigSounder">');
        fprintf(fid,'\n   <PARAMETER NAME="Nping" VALUE="%d" />',NbPing);
        fprintf(fid,'\n   <PARAMETER NAME="Trec" VALUE="%d" />',InterPing);
        fprintf(fid,'\n   <PARAMETER NAME="Rmin" VALUE="%d" />',Rmin);
        fprintf(fid,'\n   <PARAMETER NAME="Rmax" VALUE="%d" />',RmaxMono);
        fprintf(fid,'\n   <PARAMETER NAME="SL" VALUE="230" />');
        fprintf(fid,'\n   <PARAMETER NAME="TVG" VALUE="0" />');
        fprintf(fid,'\n   <PARAMETER NAME="TransducerType" VALUE="1">');
        fprintf(fid,'\n    <Transducer TYPE="Mono">');
        fprintf(fid,'\n     <PARAMETER NAME="TransducerGeometry" VALUE="0" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="Frequency" VALUE="18000" />');
        fprintf(fid,'\n     <PARAMETER NAME="Psi" VALUE="0" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="Phi" VALUE="0" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="Position" VALUE-X="27.7" VALUE-Y="-0.4" VALUE-Z="7.11" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="TypeBeam" VALUE="1" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="BeamPsiFile" VALUE="" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="BeamPhiFile" VALUE="" />');
        fprintf(fid,'\n     <PARAMETER NAME="BeamWidthPhi3dB" VALUE="10" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="BeamWidthPsi3dB" VALUE="10" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="BeamWidth3dB" VALUE="10" />');
        fprintf(fid,'\n     <PARAMETER NAME="NumberAngleSamples" VALUE="361" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="MaxAngleBeam" VALUE="90" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="MinAngleBeam" VALUE="-90" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="BeamAngleSampling" VALUE="0.5" /> ');
        fprintf(fid,'\n    </Transducer>');
        fprintf(fid,'\n    <Transducer TYPE="Mono">');
        fprintf(fid,'\n     <PARAMETER NAME="TransducerGeometry" VALUE="0" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="Frequency" VALUE="38000" />');
        fprintf(fid,'\n     <PARAMETER NAME="Psi" VALUE="0" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="Phi" VALUE="0" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="Position" VALUE-X="27.69" VALUE-Y="0.4" VALUE-Z="7.11" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="TypeBeam" VALUE="1" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="BeamPsiFile" VALUE="" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="BeamPhiFile" VALUE="" />');
        fprintf(fid,'\n     <PARAMETER NAME="BeamWidthPhi3dB" VALUE="7" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="BeamWidthPsi3dB" VALUE="7" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="BeamWidth3dB" VALUE="7" />');
        fprintf(fid,'\n     <PARAMETER NAME="NumberAngleSamples" VALUE="361" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="MaxAngleBeam" VALUE="90" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="MinAngleBeam" VALUE="-90" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="BeamAngleSampling" VALUE="0.5" /> ');
        fprintf(fid,'\n    </Transducer>');
        fprintf(fid,'\n    <Transducer TYPE="Mono">');
        fprintf(fid,'\n     <PARAMETER NAME="TransducerGeometry" VALUE="0" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="Frequency" VALUE="70000" />');
        fprintf(fid,'\n     <PARAMETER NAME="Psi" VALUE="0" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="Phi" VALUE="0" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="Position" VALUE-X="27.02" VALUE-Y="-0.56" VALUE-Z="7.11" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="TypeBeam" VALUE="1" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="BeamPsiFile" VALUE="" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="BeamPhiFile" VALUE="" />');
        fprintf(fid,'\n     <PARAMETER NAME="BeamWidthPhi3dB" VALUE="7" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="BeamWidthPsi3dB" VALUE="7" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="BeamWidth3dB" VALUE="7" />');
        fprintf(fid,'\n     <PARAMETER NAME="NumberAngleSamples" VALUE="361" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="MaxAngleBeam" VALUE="90" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="MinAngleBeam" VALUE="-90" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="BeamAngleSampling" VALUE="0.5" /> ');
        fprintf(fid,'\n    </Transducer>');
        fprintf(fid,'\n    <Transducer TYPE="Mono">');
        fprintf(fid,'\n     <PARAMETER NAME="TransducerGeometry" VALUE="0" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="Frequency" VALUE="120000" />');
        fprintf(fid,'\n     <PARAMETER NAME="Psi" VALUE="0" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="Phi" VALUE="0" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="Position" VALUE-X="26.52" VALUE-Y="0.52" VALUE-Z="7.11" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="TypeBeam" VALUE="1" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="BeamPsiFile" VALUE="" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="BeamPhiFile" VALUE="" />');
        fprintf(fid,'\n     <PARAMETER NAME="BeamWidthPhi3dB" VALUE="7" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="BeamWidthPsi3dB" VALUE="7" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="BeamWidth3dB" VALUE="7" />');
        fprintf(fid,'\n     <PARAMETER NAME="NumberAngleSamples" VALUE="361" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="MaxAngleBeam" VALUE="90" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="MinAngleBeam" VALUE="-90" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="BeamAngleSampling" VALUE="0.5" /> ');
        fprintf(fid,'\n    </Transducer>');
        fprintf(fid,'\n     <Transducer TYPE="Mono">');
        fprintf(fid,'\n     <PARAMETER NAME="TransducerGeometry" VALUE="0" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="Frequency" VALUE="200000" />');
        fprintf(fid,'\n     <PARAMETER NAME="Psi" VALUE="0" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="Phi" VALUE="0" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="Position" VALUE-X="27.07" VALUE-Y="0.49" VALUE-Z="7.11" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="TypeBeam" VALUE="1" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="BeamPsiFile" VALUE="" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="BeamPhiFile" VALUE="" />');
        fprintf(fid,'\n     <PARAMETER NAME="BeamWidthPhi3dB" VALUE="7" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="BeamWidthPsi3dB" VALUE="7" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="BeamWidth3dB" VALUE="7" />');
        fprintf(fid,'\n     <PARAMETER NAME="NumberAngleSamples" VALUE="361" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="MaxAngleBeam" VALUE="90" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="MinAngleBeam" VALUE="-90" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="BeamAngleSampling" VALUE="0.5" /> ');
        fprintf(fid,'\n    </Transducer>');
        fprintf(fid,'\n     <Transducer TYPE="Mono">');
        fprintf(fid,'\n     <PARAMETER NAME="TransducerGeometry" VALUE="0" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="Frequency" VALUE="333000" />');
        fprintf(fid,'\n     <PARAMETER NAME="Psi" VALUE="0" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="Phi" VALUE="0" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="Position" VALUE-X="26.42" VALUE-Y="-0.49" VALUE-Z="7.11" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="TypeBeam" VALUE="1" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="BeamPsiFile" VALUE="" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="BeamPhiFile" VALUE="" />');
        fprintf(fid,'\n     <PARAMETER NAME="BeamWidthPhi3dB" VALUE="7" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="BeamWidthPsi3dB" VALUE="7" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="BeamWidth3dB" VALUE="7" />');
        fprintf(fid,'\n     <PARAMETER NAME="NumberAngleSamples" VALUE="361" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="MaxAngleBeam" VALUE="90" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="MinAngleBeam" VALUE="-90" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="BeamAngleSampling" VALUE="0.5" /> ');
        fprintf(fid,'\n    </Transducer>');

        fprintf(fid,'\n   </PARAMETER>');
        fprintf(fid,'\n  </Configuration>');

        if Espece ==0
            filenamemono = sprintf('scene_mono_anchois_%g_%g_%03g_%g',Espece,abs(pitch),round(abs(10*yaw(i))),Pitchstd);
            FileOasisResMono=[chemin,'\','simulation\anchois\', dirScene,'\mono\',filenamemono];
        elseif Espece ==1
            filenamemono = sprintf('scene_mono_sardine_%g_%g_%03g_%g',Espece,abs(pitch), round(abs(10*yaw(i))),Pitchstd);
            FileOasisResMono=[chemin,'\','simulation\sardine\', dirScene,'\mono\',filenamemono];
        end

        fprintf(fid,'\n  <Configuration TYPE="ConfigStorage">');
        fprintf(fid,'\n   <PARAMETER NAME="ResultFileName" VALUE="%s" />',FileOasisResMono);
        fprintf(fid,'\n   <PARAMETER NAME="FileTypeRESON" VALUE="0" />');
        fprintf(fid,'\n   <PARAMETER NAME="FileTypeSIMRAD" VALUE="0" /> ');
        fprintf(fid,'\n   <PARAMETER NAME="FileTypeMATLAB" VALUE="0" /> ');
        fprintf(fid,'\n   <PARAMETER NAME="FileTypeHAC" VALUE="1" /> ');
        fprintf(fid,'\n   <PARAMETER NAME="DecimationRatio" VALUE="10" /> ');
        fprintf(fid,'\n   <PARAMETER NAME="StorageThreshold" VALUE="-120" /> ');
        fprintf(fid,'\n  </Configuration>');




        fprintf(fid,'\n  <Configuration TYPE="ConfigVesselMotion">');
        fprintf(fid,'\n   <PARAMETER NAME="Vbat" VALUE="%d"/>',vbat );
        fprintf(fid,'\n   <PARAMETER NAME="RollAmplitude" VALUE="%d" />',RollAmplitude);
        fprintf(fid,'\n   <PARAMETER NAME="RollPeriod" VALUE="%d" />',RollPeriod);
        fprintf(fid,'\n   <PARAMETER NAME="PitchAmplitude" VALUE="%d" />',PitchAmplitude);
        fprintf(fid,'\n   <PARAMETER NAME="PitchPeriod" VALUE="%d" />',PitchPeriod);
        fprintf(fid,'\n   <PARAMETER NAME="RollInit" VALUE="%d" />',RollInit);
        fprintf(fid,'\n   <PARAMETER NAME="PitchInit" VALUE="%d" />',PitchInit);
        fprintf(fid,'\n   <PARAMETER NAME="FechCentraleNav" VALUE="%d" />',FechCentraleNav);
        fprintf(fid,'\n   <PARAMETER NAME="InitialLatitude" VALUE-Deg="%d" VALUE-Min="%d" VALUE-Sec="%d" VALUE-Orientation="%d" />',latDeg,latMin,latSec,latO);
        fprintf(fid,'\n   <PARAMETER NAME="InitialLongitude" VALUE-Deg="%d" VALUE-Min="%d" VALUE-Sec="%d" VALUE-Orientation="%d" />',lonDeg,lonMin,lonSec,lonO);
        fprintf(fid,'\n   <PARAMETER NAME="Heading" VALUE="%d" />',heading);
        fprintf(fid,'\n   <PARAMETER NAME="HeaveAmplitude" VALUE="%d" />',HeaveAmplitude);
        fprintf(fid,'\n   <PARAMETER NAME="HeavePeriod" VALUE="%d" />',HeavePeriod);
        fprintf(fid,'\n   <PARAMETER NAME="HeaveInit" VALUE="%d" />',HeaveInit);
        fprintf(fid,'\n   <PARAMETER NAME="DepthAttitude" VALUE="%d" />',DepthAttitude);
        fprintf(fid,'\n  </Configuration>');

        fprintf(fid,'\n </Configuration>');
        fprintf(fid,'\n</APPLICATION>');

        fclose(fid);

        %%%%%%%%%%%%%Ecriture du fichier bancsfin_multi.xml%%%%%%%%%%%%%%%%% %%




        ArrayPsiFile='L:\PELGAS\DataEspecesPuresFiltre\Simu\weighting_athwart_HR21.txt';
        ArrayPhiFile='L:\PELGAS\DataEspecesPuresFiltre\Simu\weighting_along_HR21.txt';
        FanBeamDistributionFile='L:\PELGAS\DataEspecesPuresFiltre\Simu\distrib_spatio_freq.txt';



        if Espece==0
            Oasisfilenamemulti = sprintf('scene_multi_anchois_%g_%g_%03g_%g.oasis',Espece,abs(pitch), round(abs(10*yaw(i))),Pitchstd);
        elseif Espece==1
            Oasisfilenamemulti = sprintf('scene_multi_sardine_%g_%g_%03g_%g.oasis',Espece,abs(pitch), round(abs(10*yaw(i))),Pitchstd);
        end

        fid = fopen([chemin,'\', dirScene '\multi\' Oasisfilenamemulti], 'wt');
        fprintf(fid,'<?xml version="1.0" encoding="UTF-8" standalone="no" ?>');
        fprintf(fid,'\n<APPLICATION APPNAME="OASIS" APPVERSION_MAJ="2" APPVERSION_MIN="9">');
        fprintf(fid,'\n');
        fprintf(fid,'\n <Configuration TYPE="Scenario">');
       
      
        %param�tres concernant l'environnement:
        fprintf(fid,'\n  <Configuration TYPE="ConfigEnv">');
        fprintf(fid,'\n   <PARAMETER NAME="SeaTemperature" VALUE="12.6" />');
        fprintf(fid,'\n   <PARAMETER NAME="Salinity" VALUE="35.4" />');
        fprintf(fid,'\n   <PARAMETER NAME="PH" VALUE="8" />');
        fprintf(fid,'\n   <PARAMETER NAME="WindSpeed" VALUE="15" />');
        fprintf(fid,'\n   <PARAMETER NAME="SelfNoiseLevel1Hz" VALUE="173" />');
        fprintf(fid,'\n   <PARAMETER NAME="PropellerPosition" VALUE-X="-40" VALUE-Y="0" VALUE-Z="0" />');
        fprintf(fid,'\n  </Configuration>');

        %param�tres concernant la sc�ne:
        fprintf(fid,'\n  <Configuration TYPE="ConfigScene">');
        fprintf(fid,'\n   <PARAMETER NAME="HasSeaFloor" VALUE="%d" />',AvecSansFond);
        fprintf(fid,'\n   <PARAMETER NAME="SeaFloorName" VALUE="Seafloor Name" />');
        fprintf(fid,'\n   <PARAMETER NAME="H" VALUE="%d" />',ProfondeurFond);
        fprintf(fid,'\n   <PARAMETER NAME="PhiBottom" VALUE="0" />');
        fprintf(fid,'\n   <PARAMETER NAME="PsiBottom" VALUE="0" />');
        fprintf(fid,'\n   <PARAMETER NAME="m10loga1" VALUE="-5" />');
        fprintf(fid,'\n   <PARAMETER NAME="a2" VALUE="180" />');
        fprintf(fid,'\n   <PARAMETER NAME="m10loga3" VALUE="-25" />');
        fprintf(fid,'\n   <PARAMETER NAME="Prof" VALUE="50" />');
        fprintf(fid,'\n   <PARAMETER NAME="BottomAngleSampling" VALUE="10" />');
        fprintf(fid,'\n   <PARAMETER NAME="NivMinIntercor" VALUE="-60" />');
        fprintf(fid,'\n   <PARAMETER NAME="NivMinGabarit" VALUE="-60" />');
        fprintf(fid,'\n   <PARAMETER NAME="Seed" VALUE="1" />');

        %param�tre concernant les bancs:
        fprintf(fid,'\n  <Configuration TYPE="Schools">');

     


        
        fprintf(fid,'\n  <School NAME="a">');
        fprintf(fid,'\n   <PARAMETER NAME="TSMode" VALUE="2" />');
        fprintf(fid,'\n   <PARAMETER NAME="TS" VALUE="%f" /> ',TS);
        fprintf(fid,'\n   <PARAMETER NAME="FreqList" VALUE="1;" /> ');
        fprintf(fid,'\n   <PARAMETER NAME="TSList" VALUE="%f" />',TS);
        fprintf(fid,'\n   <PARAMETER NAME="FishDirFile" VALUE="%s" />',FileSardine);
        fprintf(fid,'\n   <PARAMETER NAME="Position" VALUE-X="%f" VALUE-Y="%f" VALUE-Z="%f"/>',PositionCentreX,PositionCentreY,PositionCentreZ);
        fprintf(fid,'\n   <PARAMETER NAME="Orientation" VALUE-X="%f" VALUE-Y="%f" VALUE-Z="%f"/>',roll,pitch,yaw(i));
        fprintf(fid,'\n   <PARAMETER NAME="Dimension" VALUE-X="%f" VALUE-Y="%f" VALUE-Z="%f"/>',D1,D2,D3);
        fprintf(fid,'\n   <PARAMETER NAME="Spacing" VALUE-X="%f" VALUE-Y="%f" VALUE-Z="%f"/>',Xp,Yp,Zp);
        fprintf(fid,'\n   <PARAMETER NAME="Deviation" VALUE-X="%f" VALUE-Y="%f" VALUE-Z="%f"/>',Xpstd,Ypstd,Zpstd);
        fprintf(fid,'\n   <PARAMETER NAME="Seed" VALUE="1"/>');
        fprintf(fid,'\n   <PARAMETER NAME="OrientationDeviation" VALUE-X="%f" VALUE-Y="%f" VALUE-Z="%f"/>',Rollstd,Pitchstd,Yawstd);
        fprintf(fid,'\n   <PARAMETER NAME="OrientationSeed" VALUE="1"/>');
        fprintf(fid,'\n   <PARAMETER NAME="FishLength" VALUE="%f"/>',taillecible/100);
        fprintf(fid,'\n   <PARAMETER NAME="FishLengthSeed" VALUE="1"/>');
        fprintf(fid,'\n   <PARAMETER NAME="FishLengthDeviation" VALUE="%f"/>',tailleciblesd/100);
        fprintf(fid,'\n   <PARAMETER NAME="SchoolType" VALUE="%d"/>',forme);
        fprintf(fid,'\n   <PARAMETER NAME="DiffuserModelIndex" VALUE="7"/>');
        fprintf(fid,'\n  </School>');
                    


        fprintf(fid,'\n  </Configuration>');
        
        fprintf(fid,'\n   <Configuration TYPE="DiffuserModels">');
        fprintf(fid,'\n   <DiffuserModel NAME="TFS Holliday">');
        fprintf(fid,'\n   <PARAMETER NAME="g" VALUE="1.12"/>');
        fprintf(fid,'\n   <PARAMETER NAME="h" VALUE="1.09"/>');
        fprintf(fid,'\n   </DiffuserModel>');
        fprintf(fid,'\n   <DiffuserModel NAME="Fluid Sphere Stanton">');
        fprintf(fid,'\n   <PARAMETER NAME="g" VALUE="1.043"/>');
        fprintf(fid,'\n   <PARAMETER NAME="h" VALUE="1.05"/>');
        fprintf(fid,'\n   </DiffuserModel>');
        fprintf(fid,'\n   <DiffuserModel NAME="Fluid Prolate Spheroid Stanton">');
        fprintf(fid,'\n   <PARAMETER NAME="g" VALUE="1.043"/>');
        fprintf(fid,'\n   <PARAMETER NAME="h" VALUE="1.05"/>');
        fprintf(fid,'\n   <PARAMETER NAME="La" VALUE="5"/>');
        fprintf(fid,'\n   </DiffuserModel>');
        fprintf(fid,'\n   <DiffuserModel NAME="Fluid Bent Cylinder 93">');
        fprintf(fid,'\n   <PARAMETER NAME="g" VALUE="1.0357"/>');
        fprintf(fid,'\n   <PARAMETER NAME="h" VALUE="1.0279"/>');
        fprintf(fid,'\n   <PARAMETER NAME="La" VALUE="16"/>');
        fprintf(fid,'\n   <PARAMETER NAME="phoL" VALUE="3"/>');
        fprintf(fid,'\n   <PARAMETER NAME="s" VALUE="0.06"/>');
        fprintf(fid,'\n   <PARAMETER NAME="Incident Angle" VALUE="20"/>');
        fprintf(fid,'\n   <PARAMETER NAME="Angle Std Dev" VALUE="20"/>');
        fprintf(fid,'\n   </DiffuserModel>');
        fprintf(fid,'\n   <DiffuserModel NAME="Fluid Bent Cylinder 94">');
        fprintf(fid,'\n   <PARAMETER NAME="s" VALUE="0.06"/>');
        fprintf(fid,'\n   <PARAMETER NAME="R" VALUE="0.058"/>');
        fprintf(fid,'\n   <PARAMETER NAME="La" VALUE="16"/>');
        fprintf(fid,'\n   </DiffuserModel>');
        fprintf(fid,'\n   <DiffuserModel NAME="Elastic Sphere">');
        fprintf(fid,'\n   <PARAMETER NAME="R" VALUE="0.5"/>');
        fprintf(fid,'\n   </DiffuserModel>');
        fprintf(fid,'\n   <DiffuserModel NAME="Gaseous Prolate Spheroid">');
        fprintf(fid,'\n   <PARAMETER NAME="gsurf" VALUE="0.0012"/>');
        fprintf(fid,'\n   <PARAMETER NAME="h" VALUE="0.22"/>');
        fprintf(fid,'\n   <PARAMETER NAME="La" VALUE="5"/>');
        fprintf(fid,'\n   </DiffuserModel>');
        fprintf(fid,'\n   <DiffuserModel NAME="Gaseous Sphere">');
        fprintf(fid,'\n   <PARAMETER NAME="gsurf" VALUE="0.0012"/>');
        fprintf(fid,'\n   <PARAMETER NAME="h" VALUE="0.22"/>');
        fprintf(fid,'\n   </DiffuserModel>');
        fprintf(fid,'\n   </Configuration>');

        fprintf(fid,'\n  </Configuration>');
        
        fprintf(fid,'\n  <Configuration TYPE="ConfigSignals">');
        fprintf(fid,'\n   <PARAMETER NAME="PulseLength" VALUE="0.001" />');
        fprintf(fid,'\n   <PARAMETER NAME="ModulationType" VALUE="1" /> ');
        fprintf(fid,'\n   <PARAMETER NAME="BetaFactor" VALUE="-20" /> ');
        fprintf(fid,'\n   <PARAMETER NAME="Fdemod" VALUE="109000" /> ');
        fprintf(fid,'\n   <PARAMETER NAME="Fech" VALUE="10000" /> ');
        fprintf(fid,'\n   <PARAMETER NAME="TypeSelection" VALUE="0" />');
        fprintf(fid,'\n   <PARAMETER NAME="Tsig" VALUE="0.003" /> ');
        fprintf(fid,'\n   <PARAMETER NAME="EchoesWindowLength" VALUE="0.005" />');
        fprintf(fid,'\n  </Configuration>');

        fprintf(fid,'\n  <Configuration TYPE="ConfigSounder">');
        fprintf(fid,'\n   <PARAMETER NAME="Nping" VALUE="%d" />',NbPing);
        fprintf(fid,'\n   <PARAMETER NAME="Trec" VALUE="%d" />',InterPing);
        fprintf(fid,'\n   <PARAMETER NAME="Rmin" VALUE="%d" />',Rmin);
        fprintf(fid,'\n   <PARAMETER NAME="Rmax" VALUE="%d" />',RmaxMulti);
        fprintf(fid,'\n   <PARAMETER NAME="SL" VALUE="230" />');
        fprintf(fid,'\n   <PARAMETER NAME="TVG" VALUE="0" />');
        fprintf(fid,'\n   <PARAMETER NAME="TransducerType" VALUE="0">');
        fprintf(fid,'\n    <PARAMETER NAME="TransmitterStabilisation" VALUE="3" />');
        fprintf(fid,'\n    <PARAMETER NAME="ReceiverStabilisation" VALUE="3" />');
        fprintf(fid,'\n    <PARAMETER NAME="Position" VALUE-X="26.73" VALUE-Y="0" VALUE-Z="7.12" />');
        fprintf(fid,'\n    <PARAMETER NAME="SensorLength" VALUE="14.2" />');
        fprintf(fid,'\n    <PARAMETER NAME="SensorWidth" VALUE="6.7" />');
        fprintf(fid,'\n    <PARAMETER NAME="LongitudinalSampling" VALUE="15" />');
        fprintf(fid,'\n    <PARAMETER NAME="AthwartshipSampling" VALUE="7.5" />');
        fprintf(fid,'\n    <PARAMETER NAME="NbAlongShipSensors" VALUE="20" />');
        fprintf(fid,'\n    <PARAMETER NAME="NbAthwartShipSensors" VALUE="40" />');
        fprintf(fid,'\n    <PARAMETER NAME="ArrayPsiFile" VALUE="%s" />',ArrayPsiFile);
        fprintf(fid,'\n    <PARAMETER NAME="ArrayPhiFile" VALUE="%s" />',ArrayPhiFile);
        fprintf(fid,'\n    <PARAMETER NAME="FanBeamDistributionFile" VALUE="%s" />',FanBeamDistributionFile);
        fprintf(fid,'\n    <PARAMETER NAME="AngDPsiMax" VALUE="10" />');
        fprintf(fid,'\n    <PARAMETER NAME="AngDPhiMax" VALUE="5" />');
        fprintf(fid,'\n    <PARAMETER NAME="DepNpsi" VALUE="21" />');
        fprintf(fid,'\n    <PARAMETER NAME="DepNphi" VALUE="11" />');
        fprintf(fid,'\n    <PARAMETER NAME="TypeBeam" VALUE="1" /> ');
        fprintf(fid,'\n    <PARAMETER NAME="NumberAngleSamples" VALUE="361" />');
        fprintf(fid,'\n    <PARAMETER NAME="MaxAngleBeam" VALUE="90" />');
        fprintf(fid,'\n    <PARAMETER NAME="MinAngleBeam" VALUE="-90" />');
        fprintf(fid,'\n    <PARAMETER NAME="BeamPsiFile" VALUE="" />');
        fprintf(fid,'\n    <PARAMETER NAME="BeamPhiFile" VALUE="" />');
        fprintf(fid,'\n   </PARAMETER>');
        fprintf(fid,'\n  </Configuration>');

        if Espece ==0
            filenamemulti = sprintf('scene_multi_anchois_%g_%g_%03g_%g',Espece,abs(pitch), round(abs(10*yaw(i))),Pitchstd);
            FileOasisResMulti=[chemin,'\','simulation\anchois\', dirScene,'\multi\',filenamemulti];
        elseif Espece ==1
            filenamemulti = sprintf('scene_multi_sardine_%g_%g_%03g_%g',Espece,abs(pitch), round(abs(10*yaw(i))),Pitchstd);
            FileOasisResMulti=[chemin,'\','simulation\sardine\', dirScene,'\multi\',filenamemulti];
        end


        fprintf(fid,'\n  <Configuration TYPE="ConfigStorage">');
        fprintf(fid,'\n   <PARAMETER NAME="ResultFileName" VALUE="%s" />',FileOasisResMulti);
        fprintf(fid,'\n   <PARAMETER NAME="FileTypeRESON" VALUE="0" />');
        fprintf(fid,'\n   <PARAMETER NAME="FileTypeSIMRAD" VALUE="0" /> ');
        fprintf(fid,'\n   <PARAMETER NAME="FileTypeMATLAB" VALUE="0" /> ');
        fprintf(fid,'\n   <PARAMETER NAME="FileTypeHAC" VALUE="1" /> ');
        fprintf(fid,'\n   <PARAMETER NAME="DecimationRatio" VALUE="10" /> ');
        fprintf(fid,'\n   <PARAMETER NAME="StorageThreshold" VALUE="-120" /> ');
        fprintf(fid,'\n  </Configuration>');




        fprintf(fid,'\n  <Configuration TYPE="ConfigVesselMotion">');
        fprintf(fid,'\n   <PARAMETER NAME="Vbat" VALUE="%d"/>',vbat );
        fprintf(fid,'\n   <PARAMETER NAME="RollAmplitude" VALUE="%d" />',RollAmplitude);
        fprintf(fid,'\n   <PARAMETER NAME="RollPeriod" VALUE="%d" />',RollPeriod);
        fprintf(fid,'\n   <PARAMETER NAME="PitchAmplitude" VALUE="%d" />',PitchAmplitude);
        fprintf(fid,'\n   <PARAMETER NAME="PitchPeriod" VALUE="%d" />',PitchPeriod);
        fprintf(fid,'\n   <PARAMETER NAME="RollInit" VALUE="%d" />',RollInit);
        fprintf(fid,'\n   <PARAMETER NAME="PitchInit" VALUE="%d" />',PitchInit);
        fprintf(fid,'\n   <PARAMETER NAME="FechCentraleNav" VALUE="%d" />',FechCentraleNav);
        fprintf(fid,'\n   <PARAMETER NAME="InitialLatitude" VALUE-Deg="%d" VALUE-Min="%d" VALUE-Sec="%d" VALUE-Orientation="%d" />',latDeg,latMin,latSec,latO);
        fprintf(fid,'\n   <PARAMETER NAME="InitialLongitude" VALUE-Deg="%d" VALUE-Min="%d" VALUE-Sec="%d" VALUE-Orientation="%d" />',lonDeg,lonMin,lonSec,lonO);
        fprintf(fid,'\n   <PARAMETER NAME="Heading" VALUE="%d" />',heading);
        fprintf(fid,'\n   <PARAMETER NAME="HeaveAmplitude" VALUE="%d" />',HeaveAmplitude);
        fprintf(fid,'\n   <PARAMETER NAME="HeavePeriod" VALUE="%d" />',HeavePeriod);
        fprintf(fid,'\n   <PARAMETER NAME="HeaveInit" VALUE="%d" />',HeaveInit);
        fprintf(fid,'\n   <PARAMETER NAME="DepthAttitude" VALUE="%d" />',DepthAttitude);
        fprintf(fid,'\n  </Configuration>');

        fprintf(fid,'\n </Configuration>');
        fprintf(fid,'\n</APPLICATION>');

        fclose(fid);

        

    end
    
