%close all;
 clear all;

       
%Setting information about data and expected species percentage
threshold = 0.8
year = '2010'
pathData = 'R:\PELGAS2010\HacBrutPel10_Hermes';
% specie = 'ENGR-ENC';
% pathSave='L:\PELGAS\DataEspecesPuresFiltre\RUN001\';
specie = 'SARD-PIL';
pathSave='L:\PELGAS\DataEspecesPuresFiltre\RUN002\';


dirbiom='L:\PELGAS\DataEspecesPuresFiltre\';
filebiom = [dirbiom,'SpeciesBiomass2.csv'];
[survey,datetimeall,latESU,lonESU,percentageall,biomall] = chargebiom (filebiom,specie);

datetime=datetimeall(biomall >5 &  biomall <10);

filelist=rdir([pathSave]);

for i=1:length(filelist)
    fileDirName=char(filelist(i,:));
    if length(fileDirName)>31
        fileName=fileDirName(length(fileDirName)-31:end);
    end
    for k=1:length(datetime)
%         datetime(k)
        dateFish=datenum(datetime(k),'dd/mm/yyyy HH:MM');
        if (strcmp(fileName(length(fileName)-3:end),'.hac') && ~strcmp(fileName(length(fileName)-9:length(fileName)-7),'RUN'))
            dateHac=datenum(fileName(length(fileName)-18:length(fileName)-4),'yyyymmdd_HHMMSS');
            if (dateFish-7/(24*60)>dateHac && dateFish<dateHac+10/(24*60) )
                fileName
               delete([pathSave,fileName]);
            end
        end
    end
end