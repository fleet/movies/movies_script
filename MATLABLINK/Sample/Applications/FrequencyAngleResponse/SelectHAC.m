%close all;
 clear all;

       
%Setting information about data and expected species percentage
threshold = 0.8
year = '2015'
pathData = 'R:\PELGAS2010\HacBrutPel10_Hermes';
% specie = 'ENGR-ENC';
% pathSave='L:\PELGAS\DataEspecesPuresFiltre\RUN003\';
specie = 'SARD-PIL';
pathSave='L:\PELGAS\DataEspecesPuresFiltre\RUN004\';


dirbiom='L:\PELGAS\DataEspecesPuresFiltre\';
filebiom = [dirbiom,'SpeciesBiomass2.csv'];
[survey,datetimeall,latESU,lonESU,percentageall,biomall,depthall] = chargebiom (filebiom,specie);

% datetime=datetimeall(percentageall>=threshold & datenum(datetimeall,'dd/mm/yyyy HH:MM')>datenum('01/01/2009 00:00:00') & biomall>10  );
% biom=biomall(percentageall>=threshold & datenum(datetimeall,'dd/mm/yyyy HH:MM')>datenum('01/01/2009 00:00:00')  & biomall>10 );
% depth=depthall(percentageall>=threshold & datenum(datetimeall,'dd/mm/yyyy HH:MM')>datenum('01/01/2009 00:00:00') & biomall>10 );
% 
datetime=datetimeall(percentageall>=threshold & strcmp(survey,['PELGAS',year]) & biomall >10 );
biom=biomall(percentageall>=threshold & strcmp(survey,['PELGAS',year])& biomall >10 );
percentage=percentageall(percentageall>=threshold & strcmp(survey,['PELGAS',year])& biomall >10);
filelist=rdir([pathData]);

for k=1:length(datetime)

    dateFish=datenum(datetime(k),'dd/mm/yyyy HH:MM');
    for i=1:length(filelist)
        fileDirName=char(filelist(i,:));
        if length(fileDirName)>31
            fileName=fileDirName(length(fileDirName)-31:end);
        end
     
        if (strcmp(fileName(length(fileName)-3:end),'.hac') && ~strcmp(fileName(length(fileName)-9:length(fileName)-7),'RUN'))
            dateHac=datenum(fileName(length(fileName)-18:length(fileName)-4),'yyyymmdd_HHMMSS');
            if (dateFish>dateHac-10/(24*60) && dateFish<dateHac+10/(24*60) && exist([pathSave,fileName])==0)
                fileName
                    datetime(k)
                 copyfile(fileDirName,[pathSave,fileName]);
            end
        end
    end
end