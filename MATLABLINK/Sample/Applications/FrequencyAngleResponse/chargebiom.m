% Laurent BERGER- IFREMER
% 24.03.2011

% function chargecas charges data from ctd files 


function [survey,datetime,lat,lon,perc,biom,depth] = chargebiom (file,specie)

%% creating a string which specifies how to read the values of the ctd
%% file
n = 24;     % number of strings(fields) in the file type 'Biomres.csv'
str = '';
for k = 1:n
    if ( k==3)          % date et heure
        str = [str '%s'];
    elseif    ( k==2)          % date et nom campagne
        str = [str '%s'];
    elseif (k == 11 && strcmp(specie,'ENGR-ENC'))            % biomasse anchois
        str = [str '%n'];
    elseif (k == 16 && strcmp(specie,'SARD-PIL'))                  % biomasse sardine
        str = [str '%n'];
    elseif (k == 23 && strcmp(specie,'SARD-PIL'))                % pourcentage biomasse sardine
        str = [str '%n'];
    elseif (k == 24 && strcmp(specie,'ENGR-ENC'))             % pourcentage biomasse anchois
        str = [str '%n'];
    elseif (k == 4  || k == 5)      % latitude et longitude
        str = [str '%n'];
           elseif (k == 7 )      % profondeur
        str = [str '%n']; 
    else                                                    % nothhing
        str = [str '%*s'];
    end
end 

%% opening the file with the pre-defined string str
fid = fopen(file);
[data] = textscan(fid,str,'delimiter',';','Headerlines',1);
fclose(fid);


%% saving the data
survey=data{1,1};
datetime= data{1,2};
biom = data{1,6};
perc=data{1,7};
lat = data{1,3};
lon = data{1,4};
depth= data{1,5};


