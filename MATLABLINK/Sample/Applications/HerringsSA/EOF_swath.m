%% Load results


    clear all;

    %spath='Y:\IBTS09\proc\lay1p\sequences\herring_shoals_ME70_22'
    %    spath='/home/acoustica/partages/acoustique/Data_Sondeurs/IBTS09/HAC/herring/ALL/SA_shoals/herring_shoals_ME70.mat';    
    spath='V:/Data_Sondeurs/IBTS09/HAC/herring/ALL/SA_shoals/herring_shoals_ME70.mat';    
 
    %spath='G:/IBTS09s/test/shoal_test.mat'
 
    %load selected shoals in spath
    %--------------------------------------------
    load (spath)
    
%mean swaths in 3D array
%%

lss=unique(nss);

mswaths_db=[]
swaths_db=[]

size(Sa_surfME70_dbss)

for count=1:size(lss,2)

    seqi=Sa_surfME70_dbss(nss==lss(:,count),:,:);
    myzi=squeeze(mean(seqi,1));
    
    if count==1
        mswaths_db=myzi;

    else
        mswaths_db=cat(4,mswaths_db,myzi);

    end
end 

mswaths_db=squeeze(mswaths_db);

size(mswaths_db)

%plot mean swaths

for count=1:size(lss,2)
    contourf(flipdim(mswaths_db(:,:,count),1))
    set(gca,'YTickLabel',[-9*3:3:-3])
    ylabel('Depth (m)')
    xlabel('Beam number')
    %contour(mswaths_db(:,:,count))
    %title(nss(count))  
    pause
    close all;
end

%% mean swaths as matrix rows for EOF analysis
%---------------------------------
lss=unique(nss);

mswaths_mat=[]

for count=1:size(lss,2)

    seqi=Sa_surfME70_dbss(nss==lss(:,count),:,:);
    myzi=squeeze(mean(seqi,1));
    myzi1row = reshape(myzi,1,[]); 
    size(myzi1row)
    
    if count==1
        mswaths_mat=myzi1row;
    else
        mswaths_mat=cat(1,mswaths_mat,myzi1row);
    end
end    

size(mswaths_mat)

%% PCA ('EOF') on mswaths_mat matrix
%----------------------------------
boxplot(mswaths_mat)

lmswaths_mat=log(mswaths_mat+1);

[coefs,scores,variances,t2] = princomp(lmswaths_mat);

% variance explained per PC
%---------------------------
percent_explained = 100*variances/sum(variances)
size(percent_explained)
pareto(percent_explained)
xlabel('Principal Component')
ylabel('Variance Explained (%)')

% Biplot
%---------------------------

biplot(coefs(:,1:2), 'scores',scores(:,1:2));

size(mswaths_mat)
size(scores)

%axis([-.26 1 -.51 .51]);

%Hierarchical clustering
%----------------------------
X=scores(:,1:3)

Y = pdist(X);
Z = linkage(Y);
dendrogram(Z);
[H,T] = dendrogram(Z,'colorthreshold','default');

% Max. Number of Cluster selection 
Ncluster=4

T = clusterdata(X,'maxclust',Ncluster); 
find(T==2)

scatter(X(:,1),X(:,2),100,T,'filled')

%Mean swath per cluster
%-------------------------

tabulate(T)

tabulate(T)
Nc=unique(T);
size(T)

mswaths_cl=[]
size(seqi)

for count=1:size(Nc,1)

    seqi=mswaths_db(:,:,T==Nc(count,:));
    myzi=squeeze(mean(seqi,3));
    
    if count==1
        mswaths_cl=myzi;
    else
        mswaths_cl=cat(4,mswaths_cl,myzi);
    end
end 

mswaths_cl=squeeze(mswaths_cl);

size(mswaths_cl)

%plot mean swaths per cluster
%-------------------------

for count=1:size(Nc,1)
    contourf(flipdim(mswaths_cl(:,:,count),1))
    set(gca,'YTickLabel',[-9*3:3:-3])
    ylabel('Depth (m)')
    xlabel('Beam number')
    title(['Cluster' num2str(count)])
    pause
    close all;
end

cs=1

%plot mean swaths for selected cluster

csel=2

lss=unique(nss);
size(lss)
lss2=lss(T==csel);
size(mswaths_db)
size(lss)
size(T)
mswaths_dbs=mswaths_db(:,:,T==csel);
size(mswaths_dbs)

for count=1:size(lss2,2)
    contourf(flipdim(mswaths_dbs(:,:,count),1))
    set(gca,'YTickLabel',[-9*3:3:-3])
    ylabel('Depth (m)')
    xlabel('Beam number')
    %contour(mswaths_db(:,:,count))
    %title(nss(count))  
    pause
    close all;
end

size(depth_bottom_ME70_dbss)
size(nss)

    selns=lss(T==csel);

    nsel=ismember(nss,selns);
    nsss=str2num(char(nss(nsel)));
  
    
    size(nsss)
    size(nsss2)
    
    depth_bottom_ME70_dbsss=depth_bottom_ME70_dbss(nsel,:,:);
    Sa_surfME70_dbsss=Sa_surfME70_dbss(nsel,:,:);
    lat_surfME70_dbsss=lat_surfME70_dbss(nsel,:,:);
    lon_surfME70_dbsss=lon_surfME70_dbss(nsel,:,:);
    vol_surfME70_dbsss=vol_surfME70_dbss(nsel,:,:);


%% Save results

    %spath='Y:\IBTS09\proc\lay1p\sequences\herring_shoals_ME70_22'
    %spath='/home/acoustica/partages/acoustique/Data_Sondeurs/IBTS09/HAC/herring/ALL/SA_shoals/herring_shoals_ME70_EOF.mat';    
   % spath='/home/acoustica/partages/acoustique/Data_Sondeurs/IBTS09/HAC/herring/ALL/SA_shoals/herring_shoals_ME70_logEOF.mat';    
    spath='V:/Data_Sondeurs/IBTS09/HAC/herring/ALL/SA_shoals/herring_shoals_ME70_EOF.mat';    
    spath_Sa='V:/Data_Sondeurs/IBTS09/HAC/herring/ALL/SA_shoals/herring_shoals_ME70_EOF_Sa.mat';    
    spath_nsss='V:/Data_Sondeurs/IBTS09/HAC/herring/ALL/SA_shoals/herring_shoals_ME70_EOF_nsss.txt';    
    %save selected shoals in spath
    %--------------------------------------------
    save (spath,'mswaths_db','mswaths_cl','mswaths_mat','T','csel');
    save (spath_Sa,'Sa_surfME70_dbsss','lat_surfME70_dbsss','lon_surfME70_dbsss','vol_surfME70_dbsss');
    save (spath_nsss,'nsss','-ASCII','-tabs');
     

    

