    
function [Nseqsl_db] = lmin_sensitivity(lmin_range,ns)
       
    Nl=size(lmin_range,2);
    
    Nseqsl_db=[];
    
    for li=1:Nl
        lmin=lmin_range(li);
        tns=tabulate(ns);
    
        quantile(cell2mat(tns(:,2,:)),[.025 .25 .50 .75 .975])

        lns=tns(:,1,:);
        Nesus=cell2mat(tns(:,2,:));
        lnss=lns(Nesus>lmin);
        
        nss_sel=ismember(ns,lnss);
        nss=ns(nss_sel);
        tnseq=tabulate(nss_sel);

        tnss=tabulate(nss);
        lseq=cell2mat(tnss(:,2))  
        lseqs=lseq(lseq>0);
        %mean duration of selected sequences
        mean(lseqs)/(2*60)
        medlseqs=quantile(lseqs,.5)/(2*60)
        
        Nseqsl_db(li,1)=lmin;
        Nseqsl_db(li,2)=cell2mat(tnseq(2,3)); 
        Nseqsl_db(li,3)=medlseqs;        
    end   
            
end    
