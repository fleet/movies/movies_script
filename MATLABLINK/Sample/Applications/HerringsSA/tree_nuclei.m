%% Load results

clear all

    %spath='Y:\IBTS09\proc\lay1p\sequences\herring_shoals_ME70_22'
    spath1='/home/acoustica/partages/acoustique/Data_Sondeurs/IBTS09/HAC/herring/ALL/SA_shoals/herring_shoals_ME70.mat';
    spath2='/home/acoustica/partages/acoustique/Data_Sondeurs/IBTS09/HAC/herring/ALL/SA_shoals/herring_shoals_ME70_EOF.mat';    
   
    %load selected shoals in spath
    %--------------------------------------------
    load (spath1)
    load (spath2)
    
%% Tree regressions per sequence

%reshape Sa matrices to long format and perform for tree regression with manual pruning for
%each sequence

%cluster selection

csel

tabulate(T)
lss=unique(nss);
size(lss)
lss2=lss(T==csel);
size(lss2)


for k=1:size(lss2,2)

    sw1=Sa_surfME70_dbss(nss==lss2(:,k),:,:);
    size(sw1)
    hist(msw1)
    %msw1=log(flipdim(squeeze(mean(sw1,1)),1)+1);
    %
    msw1=flipdim(squeeze(mean(sw1,1)),1);
    contourf(msw1)
    set(gca,'YTickLabel',[-9*3-10:3:-3-10])
    ylabel('Depth (m)')
    xlabel('Beam number')

    sw1_long = reshape(sw1,[],1,1); 
    size(sw1_long)

    x=[1:1:size(sw1,1)]
    size(x)
    z=[1:1:size(sw1,2)]
    size(z)
    y=[1:1:size(sw1,3)]
    size(y)

    xlab= repmat(x,1,size(sw1,2)*size(sw1,3));
    size(xlab)

    zlab1=repmat(z,size(sw1,1),size(sw1,3));
    zlab=reshape(zlab1,1,[])';
    size(zlab1)
    size(zlab)

    ylab1=repmat(y,size(sw1,1),1);
    ylab2=repmat(ylab1,size(sw1,2),1);
    ylab=reshape(ylab2,1,[])';

    size(ylab1)
    size(ylab2)
    size(ylab)

    yz=horzcat(ylab,zlab);
    
    t = classregtree(yz,sw1_long,'categorical',1:2,'names',{'y' 'z'});
    %
    view(t);
    title('original tree');
    [c,s,n,best] = test(t,'cross',yz,sw1_long);
    tmin = prune(t,'level',best)
    view(tmin)
        title('best tree');
    pause

    close all hidden; 
       
end;


 [mincost,minloc] = min(c);
    plot(n,c,'b-o',...
     n(best+1),c(best+1),'bs',...
     n,(mincost+s(minloc))*ones(size(n)),'k--')
    xlabel('Tree size (number of terminal nodes)')