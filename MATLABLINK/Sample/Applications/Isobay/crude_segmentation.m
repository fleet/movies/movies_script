%% High density area selection

clear all;

%fpath='/home/acoustica/partages/acoustique/Data_Sondeurs/IBTS09/HAC/herring/ALL/FlatBottom/lay_1p/';
%fpath='Y:\IBTS09\proc\lay1p\3\';
%fpath='F:/Projects/BoB/Isobay/proc/lay/anchois_libere/RUN030/'
fpath='F:/Partage/CLASS08/Acoustique/Result/EIlay/'

%% Load EI lay results and bind matrices ...

%EIlay ESDUs = 1 ping ~ .5 sec ~ 1 m

filename_ME70 = 'CLASS08-RUN008-TH-80-ER60-EIlay.mat'
filename_ER60 = 'CLASS08-RUN008-TH-80-ME70-EIlay.mat'
%lay
%
%EIlayRes_bind(fpath,filename_ME70,filename_ER60)

%% ... or load .mat result file

%path_res='F:/Projects/Hareng/proc/lay/res/'

%path_res=[fpath,'res/']

pathres_ME70 = [fpath,filename_ME70]
pathres_ER60 = [fpath,filename_ER60]

load(pathres_ME70);
load(pathres_ER60); 
       
%% select highest density area

    %suggested settings:
    % bw=400, thr=1000, a=100

    %(julian) times in days since origin
    t = squeeze(time_ME70_db/86400+719529);
    size(t)
    %Sa moving average along x 
    %---------------
    size(mean(Sa_surfME70_db,2))
    
    Sa_surfME70_mx=squeeze(mean(squeeze(mean(Sa_surfME70_db,2)),2));

    plot(t,Sa_surfME70_mx)
    datetick('x',0)
        
    Sa_surfME70_mxS=squeeze(mean(squeeze(mean(Sa_surfME70_db(:,1:2,:),2)),2));
    Sa_surfME70_mxD=squeeze(mean(squeeze(mean(Sa_surfME70_db(:,3:9,:),2)),2));
    plot(t,Sa_surfME70_mxS)
    datetick('x',0)
    
    plotyy(t,Sa_surfME70_mxS,t,-depth_bottom_ME70_db(:,1,1))
    datetick('x',1)
    hold on
    plot(t,Sa_surfME70_mxD,'r')
   
    plot3(lon_surfME70_db(:,1,1),lat_surfME70_db(:,1,1),-depth_bottom_ME70_db(:,1,1))
    plot3(lon_surfER60_db(:,1,1),lat_surfER60_db(:,1,1),-depth_bottom_ER60_db(:,1,1))
    stem3(lon_surfER60_db(:,1,1),lat_surfER60_db(:,1,1),-depth_bottom_ER60_db(:,1,1))
            
    %check that times are in increasing order
    %plot(1:size(t,2),t)
    %datetick('x',0)
    
	%--------------------
	%school selection
	%--------------------

	%low pass filter: selection of ESUs based on:
	%1. Sa series smoothed with a moving average of bandwidth bw
	%2. a Sa threshold thr applied on the smoothed series
    
%% sensitivity analysis: bw and thr
    % Criterion: % of mean total Sa selected
    %--------------------------
    size(t)
    bw_range=[1:10:50];
    max(Sa_surfME70_mx)
    thr_range=[10 50 100 500 1000];
    size(bw_range,2)*size(thr_range,2)
    
    Nesus_db=xsmooth_sensitivity(bw_range,thr_range,t,Sa_surfME70_mx);
    size(Nesus_db)
    
    [X,Y] = meshgrid(bw_range,thr_range)
    Z = reshape(Nesus_db(:,3),size(thr_range,2),size(bw_range,2))
    Z2 = reshape(Nesus_db(:,4),size(thr_range,2),size(bw_range,2))

    surf(X,Y,Z,'FaceColor','interp')
    ylabel('thr');
    xlabel('bw');
    zlabel('% selected esus');

    surf(X,Y,Z2,'FaceColor','interp')
    ylabel('thr');
    xlabel('bw');
    zlabel('% (total mSax');
    colorbar
    
    figure;
    pcolor(X,Y,Z2)
    ylabel('thr');
    xlabel('bw');
    title('% (mean total Sa along x)');
    colorbar
       
%% bw and thr choice
%-----------------------------------
    bw=20;
    
    %-----------------------------------
       
    Sa_surfME70_smx = filter(ones(1,bw)/bw,1,Sa_surfME70_mx);
    
    t = squeeze(time_ME70_db/86400+719529);
    
    st=filter(ones(1,bw)/bw,1,t);
    %plot(t,st)
    %datetick('x',15)
    
    plot(t,Sa_surfME70_mx,'-.',t,Sa_surfME70_smx,'-'), grid on
    datetick('x',15)
    legend('Original Data','Smoothed Data',2)
    title('Average Sa along x')
    
   %select pings with smoothed Sa higher than 'thr' 
	%---------------
    hist(Sa_surfME70_smx)
    q75_Sa=quantile(Sa_surfME70_smx,0.75)
    q80_Sa=quantile(Sa_surfME70_smx,0.80)
    q95_Sa=quantile(Sa_surfME70_smx,0.95)
    q99_Sa=quantile(Sa_surfME70_smx,0.99)
   
   %----------------------------------- 
    thr=200;
   %-----------------------------------
   
    shoali=(Sa_surfME70_smx>=thr);
      
    ts=t(shoali);
    
    %check that times are in increasing order... often, they are not...
    plot(diff(ts))
    title('Time differences between sequences')
    ylim([-0.02 0.02]);
    
    %if times are not in increasing order, re-order times
    [tso,IXs] = sort(ts);
    
    Sa_surfME70_mxs=Sa_surfME70_mx(shoali);
    Sa_surfME70_smxs=Sa_surfME70_smx(shoali);
    depth_bottom_ME70_dbs=depth_bottom_ME70_db(shoali,:,:);
    Sa_surfME70_dbs=Sa_surfME70_db(shoali,:,:);
    lat_surfME70_dbs=lat_surfME70_db(shoali,:,:);
    lon_surfME70_dbs=lon_surfME70_db(shoali,:,:);
    vol_surfME70_dbs=vol_surfME70_db(shoali,:,:);
 
    %sort esus in chronological order
    Sa_surfME70_mxs=Sa_surfME70_mxs(IXs);
    Sa_surfME70_smxs=Sa_surfME70_smxs(IXs);
    depth_bottom_ME70_dbs=depth_bottom_ME70_dbs(IXs,:,:);
    Sa_surfME70_dbs=Sa_surfME70_dbs(IXs,:,:);
    lat_surfME70_dbs=lat_surfME70_dbs(IXs,:,:);
    lon_surfME70_dbs=lon_surfME70_dbs(IXs,:,:);
    vol_surfME70_dbs=vol_surfME70_dbs(IXs,:,:);
    
    %plot of selected ESUs (slow)
    %-------------------------------
    %hold on
    %plot(t,Sa_surfME70_mx,'-.'), grid on
    %datetick('x',15)
    %plot(t,Sa_surfME70_smx,'g-')
    %plot(t,thr,'r--')
    %plot(t(shoali),Sa_surfME70_mx(shoali),'r-')
    %legend('Original Data','Smoothed data','Selected pings',2)
 
    %plot of selected ESUs (fast)
    %-------------------------------
    figure;
    hold on
    plot(t,Sa_surfME70_mx,'b-')  
    datetick('x',15)
    plot(tso,Sa_surfME70_mxs,'r-')    
    plot(tso,Sa_surfME70_smxs,'g-') 
    legend('Original data','Selected esus','Smoothed data',2)    
    title('Average Sa along x')
    
    size(Sa_surfME70_mx)
    size(Sa_surfME70_mxs)
    
    %plot of selected ESUs duration in time domain
    %-------------------------------
    figure;
    hold on
    plot(t,t)
    plot(ts,ts,'--rs','linewidth',2)
    title('Squares = selected ESUs')
    datetick('x',15)
    datetick('y',15)
    
    %Total Sa ratio after/before selection
    %-----------------------------------------
    sum(squeeze(mean(squeeze(sum(Sa_surfME70_dbs,2)),2)),1)/sum(squeeze(mean(squeeze(sum(Sa_surfME70_db,2)),2)),1)

%% sequence labelling ESDU aggregation   
    %------------------
    % define time sequences: 
    % a = scaling factor for ESU sequences definition (time threshold for sequence edge definition between
    %successive ESUs = a * quantile(dt,0.975))
    %Objective: minimum number of sequences
    
%% sensitivity test for a in a_range
  
    a_range=[1:1:20];
    Nseq_db=a_sensitivity(a_range,t,tso);
    
%% a choice

    a=2 
      
    %if times are not in increasing order, re-order times
    [to,IX] = sort(t);
    %mean time difference between successive esus
    dt=diff(to);
    mdt=mean(dt);
    %mean time difference between successive esus in seconds
    mdt*3600*24
    
    dt=diff(to);
    hist(dt)
    medt=median(dt)
    sumdt=quantile(dt,[.025 .25 .50 .75 .975])
    
    dts=diff(tso);
    plot(dts)
    
    dts_sup=logical([1 dts>(sumdt(5)*a)]);
    tabulate(dts_sup)    
    ts_sup=[0 tso(dts_sup) max(tso)+1];
    
    %time threshold (in seconds) for sequences breaks
    sumdt(5)*a*3600*24
    
    N1=size(ts_sup,2);
    ns_lab= num2str((1:(N1-1))');
    size(ts_sup)
    size(ns_lab)
    
    %sequence labels
    ns= ordinal(tso,ns_lab,[],ts_sup);
    
    plot(tso,ns)
    datetick('x',15)
    ylabel('Sequence ID')
    xlabel('Time')
    
    
%% select sequences longer than lmin (e.g. sequence duration in nb of pings)
    %---------------
    
%% lmin sensitivity analysis
    
    lmin_range=[100:120:2000];
    
    Nseqsl_db = lmin_sensitivity(lmin_range,ns);
    
    plot(Nseqsl_db(:,1),Nseqsl_db(:,2))
    xlabel('Sequence min. length (nb. pings)')
    ylabel('% of selected ESUs')
    figure;
    plot(Nseqsl_db(:,1),Nseqsl_db(:,3))
    xlabel('Sequence min. length (nb. pings)')
    ylabel('Median sequence duration (minutes)')
    
%% lmin choice    

    lmin=1200
    
    tns=tabulate(ns);
    
    quantile(cell2mat(tns(:,2,:)),[.025 .25 .50 .75 .975])
    
    size(tns)
    lns=tns(:,1,:);
    Nesus=cell2mat(tns(:,2,:));
    lnss=lns(Nesus>lmin);
        
    nss_sel=ismember(ns,lnss);
    nss=ns(nss_sel);
    tabulate(nss_sel)

    ismember(unique(nss),lnss)
    
    tss=tso(nss_sel);
    Sa_surfME70_mxss=Sa_surfME70_mxs(nss_sel);
    Sa_surfME70_smxss=Sa_surfME70_smxs(nss_sel);
    
    size(Sa_surfME70_mxss)
  
    %select sequences longer than threshold lmin
    %--------------------------------------------
    depth_bottom_ME70_dbss=depth_bottom_ME70_dbs(nss_sel,:,:);
    Sa_surfME70_dbss=Sa_surfME70_dbs(nss_sel,:,:);
    lat_surfME70_dbss=lat_surfME70_dbs(nss_sel,:,:);
    lon_surfME70_dbss=lon_surfME70_dbs(nss_sel,:,:);
    %vol_surfME70_dbss=vol_surfME70_dbs(nss_sel,:,:);
    
    %initial array size
    size(Sa_surfME70_db2)   
    %array size and ratio of total Sa after/before moving average based selection
    %-----------------------------------------
    size(Sa_surfME70_dbs)
    sum(squeeze(mean(squeeze(sum(Sa_surfME70_dbs,2)),2)),1)/sum(squeeze(mean(squeeze(sum(Sa_surfME70_db2,2)),2)),1)        
    %array size and ratio of total Sa after/before selection on sequence duration 
    %-----------------------------------------
    sum(squeeze(mean(squeeze(sum(Sa_surfME70_dbss,2)),2)),1)/sum(squeeze(mean(squeeze(sum(Sa_surfME70_db2,2)),2)),1)
    size(Sa_surfME70_dbss) 

    %Number of sequences after moving filter selection
    size(lns)
    %Number of sequences after sequence duration based selection
    size(lnss)
    
%% selection of 1 sequence 
    %-----------------------------

    %tssi=tss(nss=='100')
    %dtsi=diff(tssi)
    %plot(dtsi)
  
    %plot(tssi(2:size(tssi,2)),dtsi)
    %datetick('x',15)    
     
    
%% Check sequence selection    

    size(lnss)

    %Global plot of selected sequences
    %---------------------------------
    %mean Sa values
    
    hold on;
    plot(t,Sa_surfME70_mx)
    plot(tss,Sa_surfME70_mxss,'r')
    datetick('x',15)
    legend('Original Data','Selected esus',2)    
    
    %smoothed Sa values
    figure;
    hold on;
    plot(t,Sa_surfME70_smx)
    plot(tss,Sa_surfME70_smxss,'r')
    datetick('x',15)   
    legend('Smoothed original Data','Smoothed selected esus',2)    

    %Plot of each selected sequence
    %---------------------------------    
    N=size(lnss,1);

    for n=1:N
        nseq=lnss(n)
        nssi_sel=ismember(nss,nseq);
        
        if n==1
            nssi_db=nssi_sel;
        else
            nssi_db=cat(1,nssi_db,nssi_sel);
        end;
        
        tabulate(nssi_sel)
        tssi=tss(nssi_sel); 
        Sa_surfME70_mxssi=Sa_surfME70_mxss(nssi_sel);
        Sa_surfME70_smxssi=Sa_surfME70_smxss(nssi_sel);
        Sa_surfME70_dbssi=Sa_surfME70_dbss(nssi_sel,:,:);
       
        hold on;
        figure;
        Sa_surfME70_dbssi_mz=squeeze(mean(Sa_surfME70_dbssi,2));
        %size(vol_surfME70_dbss)
        surf(Sa_surfME70_dbssi_mz)
        title(['Sequence' nseq])
        
        plot(tssi,Sa_surfME70_mxssi)
        plot(tssi,Sa_surfME70_smxssi,'r')
        datetick('x',15)
        title(['Sequence' nseq])  
        
        pause
        
        close all;
    end;
    
%% Save results

    %spath='Y:\IBTS09\proc\lay1p\sequences\herring_shoals_ME70_22'
    spath='/home/acoustica/partages/acoustique/Data_Sondeurs/IBTS09/HAC/herring/ALL/SA_shoals/herring_shoals_ME70.mat';    
    
    %save selected shoals in spath
    %--------------------------------------------
    save (spath,'tss','nss','depth_bottom_ME70_dbss','Sa_surfME70_dbss','lat_surfME70_dbss','lon_surfME70_dbss','vol_surfME70_dbss');
    

    
 
    
 
    
    
    