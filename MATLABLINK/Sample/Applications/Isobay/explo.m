%% High density area selection

clear all;

%fpath='/home/acoustica/partages/acoustique/Data_Sondeurs/IBTS09/HAC/herring/ALL/FlatBottom/lay_1p/';
%fpath='Y:\IBTS09\proc\lay1p\3\';
fpath='F:/Projects/BoB/Isobay/proc/lay/anchois_libere/RUN031/'

%% Load EI lay results and bind matrices ...

%EIlay ESDUs = 1 ping ~ .5 sec ~ 1 m

filename_ME70 = 'PEL09_R031_lay1NM_ME70.mat'
filename_ER60 = 'PEL09_R031_lay1NM_ER60.mat'
%lay
%
EIlayRes_bind(fpath,filename_ME70,filename_ER60)

%% ... or load .mat result file

%path_res='F:/Projects/Hareng/proc/lay/res/'

%
path_res=[fpath,'res/']

pathres_ME70 = [path_res,filename_ME70]
pathres_ER60 = [path_res,filename_ER60]

load(pathres_ME70);
load(pathres_ER60); 
       
%% exploration

    %navigation/depth
    %----------------------
    size(lon_surfER60_db)
    
    %plot3(lon_surfER60_db(:,1,1),lat_surfER60_db(:,1,1),-depth_bottom_ER60_db(:,1,1))
    stem3(lon_surfER60_db(:,1,1),lat_surfER60_db(:,1,1),-depth_bottom_ER60_db(:,1,1))

    %(julian) times in days since origin
    t = squeeze(time_ME70_db/86400+719529);
    size(t)
    %Sa moving average along x 
    %---------------
    size(mean(Sa_surfME70_db,2))
    
    Sa_surfME70_mx=squeeze(mean(squeeze(mean(Sa_surfME70_db,2)),2));

    plot(t,Sa_surfME70_mx)
    datetick('x',0)
        
    Sa_surfME70_mxS=squeeze(mean(squeeze(mean(Sa_surfME70_db(:,1:2,:),2)),2));
    Sa_surfME70_mxD=squeeze(mean(squeeze(mean(Sa_surfME70_db(:,3:9,:),2)),2));

    size(t)
    size(Sa_surfME70_mxS)
    size(depth_bottom_ER60_db(:,1,1))
    
    plotyy(t,Sa_surfME70_mxS,t,-depth_bottom_ME70_db(:,1,10))
    datetickzoom('x',1)
    hold on
    plot(t,Sa_surfME70_mxD,'r')
    legend('Bottom depth','Mean surface SA','Mean bottom SA')
    
    hl1 = line(t,Sa_surfME70_mxS,'Color','r');
    ax1 = gca;
    set(ax1,'XColor','r','YColor','r')
    datetickzoom('x',1)    
    ax2 = axes('Position',get(ax1,'Position'),...
           'XAxisLocation','top',...
           'YAxisLocation','right',...
           'Color','none',...
           'XColor','k','YColor','k');
    hl2 = line(t,-depth_bottom_ER60_db(:,1,1),'Color','k','Parent',ax2);
    hold on
    plot(t,Sa_surfME70_mxD,'r')
    