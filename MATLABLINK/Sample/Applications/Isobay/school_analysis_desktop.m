close all
clear all

%% EI shoal

chemin_ini='G:\PELGAS09\HAC-Hermes\RUN035\day\'

EI_thr=[-65:-55];

%ithri=4

for ithri=1:size(EI_thr,2)
    
    %
    pathi=[chemin_ini,'EIshl',num2str(EI_thr(ithri)),'dB\']
    %pathi=[chemin_save,'shoals',num2str(EI_thr(ithri)),'\']
    
    if ~exist(pathi,'dir')
        mkdir(pathi)
    end
%
    SampleShoalExtraction(chemin_ini,pathi);
%
clear functions;

end;

%% Import EI shoal results

%chemin='/export/home/Mathieu/Projects/BoB/Isobay/proc/'
chemin='G:/PELGAS09/HAC-Hermes/RUN035/day/'

filedir = dirr([chemin,'\*school.mat']);  % ensemble des fichiers
nb_seuil = size(filedir,1);  % nombre de seuils

% figure;
% hold on;
nblME70=0;
nblER60=0;

for numdir = 1:nb_seuil

    %seuil(numdir)=-str2num(filedir(numdir).name(size(filedir(numdir).name,2)-2:size(filedir(numdir).name,2)));
    seuil(numdir)=str2num(filedir(numdir).name(6:8))
    
    %filelist=ls([chemin,filedir(numdir).name,'/*school.mat']);
    %nb_files = size(filelist,1);

    filelist = squeeze(dir(fullfile([chemin,filedir(numdir).name],'*school.mat')));  % all files
    nb_files = size(filelist,1);  % number of files
    
    for numfile = 1:nb_files

        %matfilename = filelist(numfile,:);
        matfilename = filelist(numfile,:).name
        
        year = str2num(matfilename(14:17));
        month =  str2num(matfilename(18:19));
        day = str2num(matfilename(20:21));
        hour = str2num(matfilename(23:24));
        mn = str2num(matfilename(25:26));

%         datefile=datenum(year, month, day, hour, mn, 0);
%         if (datefile>=debut && datefile<=fin)

        FileName = [chemin,filedir(numdir).name,'/',matfilename(1:findstr('.',matfilename)-1),'.mat'];
        load(FileName);

            if  size(schools_ER60,2)>0

                for i=1:size(schools_ER60,2)
                    time_ER60(nblER60+i) = schools_ER60(1,i).time;
                    length_ER60(nblER60+i) = schools_ER60(1,i).length;
                    width_ER60(nblER60+i) = schools_ER60(1,i).width;
                    height_ER60(nblER60+i) = schools_ER60(1,i).height;
                    volume_ER60(nblER60+i) = schools_ER60(1,i).volume;
                    MVBS_ER60(nblER60+i) = schools_ER60(1,i).MVBS_weighted;
                    sigma_ag_ER60(nblER60+i) = schools_ER60(1,i).sigma_ag;
                    position_ER60(nblER60+i,:) = schools_ER60(1,i).position;
                    seuil_ER60(nblER60+i,:) = seuil(numdir);

                end


                nblER60=nblER60+size(schools_ER60,2);
            end

            if  size(schools_ME70,1)>0
                for i=1:size(schools_ME70,2)
                    time_ME70(nblME70+i) = schools_ME70(1,i).time;
                    length_ME70(nblME70+i) = schools_ME70(1,i).length;
                    width_ME70(nblME70+i) = schools_ME70(1,i).width;
                    height_ME70(nblME70+i) = schools_ME70(1,i).height;
                    volume_ME70(nblME70+i) = schools_ME70(1,i).volume;
                    MVBS_ME70(nblME70+i) = schools_ME70(1,i).MVBS_weighted;
                    sigma_ag_ME70(nblME70+i) = schools_ME70(1,i).sigma_ag;
                    position_ME70(nblME70+i,:) = schools_ME70(1,i).position;
                    seuil_ME70(nblME70+i,:) = seuil(numdir);

                end

                nblME70=nblME70+size(schools_ME70,2);
            end

            clear  schools_ER60  schools_ME70;

%         end

    end


end

% load([chemin,'Navigation.mat'])

% timenum=zeros(size(Time,1),1);
% for i=1:size(Time,1)
%     timenum(i)=datenum(Time(i,:),'dd/mm/yyyy HH:MM:SS.FFF');
% end
% indexNav = find(timenum>debut & timenum<fin);
% 
% MaxX=max( reshape(Longitude(:,indexNav),1,[]));
% MinX=min( reshape(Longitude(:,indexNav),1,[]));
% 
% MaxY=max(reshape(Latitude(:,indexNav),1,[]));
% MinY=min(reshape(Latitude(:,indexNav),1,[]));
% 
% nbPoints = 1000;
% Precision = max((MaxX-MinX)/nbPoints,(MaxY-MinY)/nbPoints)
% XI=MinX:Precision:MaxX;
% YI=MinY:Precision:MaxY;
%     figure;
% ZI = griddata( reshape(Longitude(:,indexNav),1,[]),reshape(Latitude(:,indexNav),1,[]),-reshape(Depth(:,indexNav),1,[]),XI,YI');
% hold on;
% surf(XI,YI,ZI,'LineStyle','none');
% view(0,70);
% light
% lighting gouraud

chemin_save=[chemin,'shlmat/']

   if ~exist(chemin_save,'dir')
        mkdir(chemin_save)
    end

save([chemin_save,'shl-5559606165.mat'],'MVBS_ER60','MVBS_ME70','height_ER60','height_ME70','length_ER60','length_ME70','position_ER60','position_ME70','seuil_ER60','seuil_ME70','sigma_ag_ER60','sigma_ag_ME70','time_ER60','time_ME70','volume_ER60','volume_ME70','width_ER60','width_ME70')


%% Exploratory analysis

% eventually, data import

whof = whos('-file', '/export/home/Mathieu/Projects/BoB/Isobay/proc/shlmat/shl-55-60.mat');
%load /export/home/Mathieu/Projects/BoB/Isobay/proc/shlmat/shl-55-60.mat

% data selection
seuil_sigma_ag=0;
seuil_volume = 0;
debut=datenum('30/05/2009 04:00');
fin=datenum('30/05/2009 23:00');

tabulate((time_ME70)>debut)
tabulate((time_ME70)<fin)

index= find((time_ME70)>debut & (time_ME70)<fin  & volume_ME70>seuil_volume);



plot(time_ME70_60,-position_ME70_60(:,3),'o')
hold on
plot(time_ME70_55,-position_ME70_55(:,3),'or')
datetick('x',15,'keepticks')

position_ME70_60s=position_ME70_60((time_ME70_60)>debut & (time_ME70_60)<fin,:);

index2= find(((time_ME70)>debut & (time_ME70)<fin)  & seuil_ME70==-60);

% Data exploration

plot3(position_ME70(seuil_ME70==-60,1),position_ME70(seuil_ME70==-60,2),position_ME70(seuil_ME70==-60,3))


hist(position_ME70(seuil_ME70==-60,3))


position_ME70_60_S=position_ME70_60(position_ME70_60(:,:,3)>=60,:,:);


for i=1:nb_seuil
    percentilelength(i,:)=prctile(length_ME70(find(seuil_ME70(index)==seuil(i))),[25 50 75]);
    occurence(i)=size(length_ME70(find(seuil_ME70(index)==seuil(i))),2);
end
figure;
[AX,H1,H2] = plotyy(seuil,percentilelength,seuil,occurence);
set(get(AX(1),'Ylabel'),'String','Length (m)') 
set(get(AX(2),'Ylabel'),'String','Occurence')
xlabel('Threshold (dB)');

legend('Occurence','10% percentile', '50% percentile', '90% percentile');
title('Length distribution of schools at various threshold')
saveas(gcf,[chemin,sprintf('Length')],'fig')
saveas(gcf,[chemin,sprintf('Length')],'jpg')

for i=1:nb_seuil
    percentileWidth(i,:)=prctile(width_ME70(find(seuil_ME70(index)==seuil(i))),[25 50 75]);
    occurence(i)=size(width_ME70(find(seuil_ME70(index)==seuil(i))),2);
end
figure;
[AX,H1,H2] = plotyy(seuil,percentileWidth,seuil,occurence);
set(get(AX(1),'Ylabel'),'String','Width (m)') 
set(get(AX(2),'Ylabel'),'String','Occurence')
xlabel('Threshold (dB)');

legend('Occurence','10% percentile', '50% percentile', '90% percentile');
title('Width distribution of schools at various threshold')
saveas(gcf,[chemin,sprintf('Width')],'fig')
saveas(gcf,[chemin,sprintf('Width')],'jpg')

for i=1:nb_seuil
    percentileHeight(i,:)=prctile(height_ME70(find(seuil_ME70(index)==seuil(i))),[25 50 75]);
    occurence(i)=size(height_ME70(find(seuil_ME70(index)==seuil(i))),2);
end
figure;
[AX,H1,H2] = plotyy(seuil,percentileHeight,seuil,occurence);
set(get(AX(1),'Ylabel'),'String','Height (m)') 
set(get(AX(2),'Ylabel'),'String','Occurence')
xlabel('Threshold (dB)');

legend('Occurence','10% percentile', '50% percentile', '90% percentile');
title('Height distribution of schools at various threshold')
saveas(gcf,[chemin,sprintf('Height')],'fig')
saveas(gcf,[chemin,sprintf('Height')],'jpg')

for i=1:nb_seuil
    percentileVolume(i,:)=prctile(volume_ME70(find(seuil_ME70(index)==seuil(i))),[25 50 75]);
    occurence(i)=size(volume_ME70(find(seuil_ME70(index)==seuil(i))),2);
end
figure;
[AX,H1,H2] = plotyy(seuil,percentileVolume,seuil,occurence);
set(get(AX(1),'Ylabel'),'String','Volume (m3)') 
set(get(AX(2),'Ylabel'),'String','Occurence')
xlabel('Threshold (dB)');

legend('Occurence','10% percentile', '50% percentile', '90% percentile');
title('Volume distribution of schools at various threshold')
saveas(gcf,[chemin,sprintf('Volume')],'fig')
saveas(gcf,[chemin,sprintf('Volume')],'jpg')

for i=1:nb_seuil
    percentileAspect(i,:)=prctile(volume_ME70(find(seuil_ME70(index)==seuil(i)))./(height_ME70(find(seuil_ME70(index)==seuil(i))).*width_ME70(find(seuil_ME70(index)==seuil(i))).*length_ME70(find(seuil_ME70(index)==seuil(i)))),[25 50 75]);
    occurence(i)=size(length_ME70(find(seuil_ME70(index)==seuil(i))),2);
end
figure;
[AX,H1,H2] = plotyy(seuil,percentileAspect,seuil,occurence);
set(get(AX(1),'Ylabel'),'String','Aspect (unitless)') 
set(get(AX(2),'Ylabel'),'String','Occurence')
xlabel('Threshold (dB)');

legend('Occurence','10% percentile', '50% percentile', '90% percentile');
title('Aspect distribution of schools at various threshold (Volume ratio of school relative to bounding box)')
saveas(gcf,[chemin,sprintf('Aspect')],'fig')
saveas(gcf,[chemin,sprintf('Aspect')],'jpg')

for i=1:nb_seuil
    percentileMVBS(i,:)=prctile(MVBS_ME70(find(seuil_ME70(index)==seuil(i))),[25 50 75]);
    occurence(i)=size(MVBS_ME70(find(seuil_ME70(index)==seuil(i))),2);
end
figure;
[AX,H1,H2] = plotyy(seuil,percentileMVBS,seuil,occurence);
set(get(AX(1),'Ylabel'),'String','MVBS (dB)') 
set(get(AX(2),'Ylabel'),'String','Occurence')
xlabel('Threshold (dB)');

legend('Occurence','10% percentile', '50% percentile', '90% percentile');
title('MVBS distribution of schools at various threshold')
saveas(gcf,[chemin,sprintf('MVBS')],'fig')
saveas(gcf,[chemin,sprintf('MVBS')],'jpg')

% figure;
% boxplot(length_ME70(index),seuil_ME70(index))
% xlabel('Threshold (dB)');
% ylabel('Length (m)');
% title('Length of school distribution for various threshold')
% saveas(gcf,[chemin,sprintf('Length ')],'fig')
% saveas(gcf,[chemin,sprintf('Length ')],'jpg')
% 
% figure;
% boxplot(width_ME70(index),seuil_ME70(index))
% xlabel('Threshold (dB)');
% ylabel('Width (m)');
% title('Width of school distribution for various threshold')
% saveas(gcf,[chemin,sprintf('Width ')],'fig')
% saveas(gcf,[chemin,sprintf('Width ')],'jpg')
% 
% figure;
% boxplot(height_ME70(index),seuil_ME70(index))
% xlabel('Threshold (dB)');
% ylabel('Height (m)');
% title('Height of school distribution for various threshold')
% saveas(gcf,[chemin,sprintf('Height ')],'fig')
% saveas(gcf,[chemin,sprintf('Height ')],'jpg')
% 
% figure;
% boxplot(volume_ME70(index),seuil_ME70(index))
% xlabel('Threshold (dB)');
% ylabel('Volume (m3)');
% title('Volume of school distribution for various threshold')
% saveas(gcf,[chemin,sprintf('Volume ')],'fig')
% saveas(gcf,[chemin,sprintf('Volume ')],'jpg')
% 
% figure;
% boxplot(height_ME70(index).*width_ME70(index).*length_ME70(index)./volume_ME70(index),seuil_ME70(index))
% xlabel('Threshold (dB)');
% ylabel('Volume ratio (unitless)');
% title('Volume ratio of school distribution relative to bounding box for various threshold')
% saveas(gcf,[chemin,sprintf('Aspect ratio')],'fig')
% saveas(gcf,[chemin,sprintf('Aspect ratio')],'jpg')
% 
% figure;
% boxplot(MVBS_ME70(index),seuil_ME70(index))
% xlabel('Threshold (dB)');
% ylabel('MVBS (dB)');
% title('MVBS of school distribution for various threshold')
% saveas(gcf,[chemin,sprintf('MVBS ')],'fig')
% saveas(gcf,[chemin,sprintf('MVBS ')],'jpg')


D=zeros(size(position_ME70(index),2),21);
for i=1:size(position_ME70(index),2)
    k=1;
    for j=max(1,i-10):min(i+10,size(position_ME70(index),2))
        D(i,k)=distance3D(position_ME70(index(i),:),position_ME70(index(j),:));
        k=k+1;
    end
end

for i=1:size(position_ME70(index),2)
    Dmin(i)=min(D(i,find(D(i,:)>0)));
end

for i=1:nb_seuil
    percentileDistance(i,:)=prctile(Dmin(find(seuil_ME70(index)==seuil(i))),[25 50 75]);
    occurence(i)=size(Dmin(find(seuil_ME70(index)==seuil(i))),2);
end
figure;
[AX,H1,H2] = plotyy(seuil,percentileDistance,seuil,occurence);
set(get(AX(1),'Ylabel'),'String','Distance (m)') 
set(get(AX(2),'Ylabel'),'String','Occurence')
xlabel('Threshold (dB)');

legend('Occurence','10% percentile', '50% percentile', '90% percentile');
title('Distance distribution of schools various threshold')
saveas(gcf,[chemin,sprintf('Distance')],'fig')
saveas(gcf,[chemin,sprintf('Distance')],'jpg')

% figure;
% boxplot(Dmin,seuil_ME70(index))
% xlabel('Threshold (dB)');
% ylabel('Distance (dB)');
% title('Distance between schools for various threshold')
% saveas(gcf,[chemin,sprintf('Distance ')],'fig')
% saveas(gcf,[chemin,sprintf('Distance ')],'jpg')


volume_ratio=zeros(nb_seuil,1);
for i=1:nb_seuil
    volume_ratio(i)=sum(volume_ME70(seuil_ME70(index)==seuil(i)))/sum(volume_ME70(seuil_ME70(index)==min(seuil)));
end

figure;
plot(seuil,volume_ratio);
xlabel('Threshold (dB)');
ylabel('Volume ratio (unitless)');
title('Volume ratio relative to lowest threshold')
saveas(gcf,[chemin,sprintf('Volume ratio')],'fig')
saveas(gcf,[chemin,sprintf('Volume ratio')],'jpg')

sigma_ag_ratio=zeros(nb_seuil,1);
for i=1:nb_seuil
    sigma_ag_ratio(i)=sum(sigma_ag_ME70(seuil_ME70(index)==seuil(i)))/sum(sigma_ag_ME70(seuil_ME70(index)==min(seuil)));
end

figure;
plot(seuil,sigma_ag_ratio);
xlabel('Threshold (dB)');
ylabel('Backscatter ratio (m)');
title('Backscatter ratio relative to lowest threshold')
saveas(gcf,[chemin,sprintf('Backscatter ratio ')],'fig')
saveas(gcf,[chemin,sprintf('Backscatter ratio ')],'jpg')




% for i=1:nb_seuil
% 
%     figure
%     scatter3(position_ME70(find(seuil_ME70()==seuil(i)),1),position_ME70(find(seuil_ME70()==seuil(i)),2),-position_ME70(find(seuil_ME70()==seuil(i)),3),sqrt(length_ME70(find(seuil_ME70()==seuil(i))).*width_ME70(find(seuil_ME70()==seuil(i)))),MVBS_ME70(find(seuil_ME70()==seuil(i))));
%     hold on;
% %     scatter3(reshape(Latitude(:,indexNav(1:10:end)),1,[]),reshape(Longitude(:,indexNav(1:10:end)),1,[]),-reshape(Depth(:,indexNav(1:10:end)),1,[]),'k');
%     xlabel('Latitude (�)');
%     ylabel('Longitude (�)');
%     ylabel('Depth (m)');
%     title(['Spatial distribution of schools at ',num2str(seuil(i)),'dB threshold, size represents length color represents density'])
%     colorbar;
%     axis([min(position_ME70(:,1)) max(position_ME70(:,1)) min(position_ME70(:,2)) max(position_ME70(:,2)) min(-position_ME70(:,3)) max(-position_ME70(:,3)) min(MVBS_ME70(:)) max(MVBS_ME70(:))]);
%     view(-40,20);
%     saveas(gcf,[chemin,'r�partition spatiale seuil ',num2str(seuil(i)),'dB'],'fig')
%     saveas(gcf,[chemin,'r�partition spatiale seuil ',num2str(seuil(i)),'dB'],'jpg')
%     
% 
% end

for i=1:nb_seuil

    figure
    scatter(position_ME70(find(seuil_ME70()==seuil(i)),1),position_ME70(find(seuil_ME70()==seuil(i)),2),sqrt(length_ME70(find(seuil_ME70()==seuil(i))).*width_ME70(find(seuil_ME70()==seuil(i)))),height_ME70(find(seuil_ME70()==seuil(i))));
    hold on;
%     scatter3(reshape(Latitude(:,indexNav(1:10:end)),1,[]),reshape(Longitude(:,indexNav(1:10:end)),1,[]),-reshape(Depth(:,indexNav(1:10:end)),1,[]),'k');
    xlabel('Latitude (�)');
    ylabel('Longitude (�)');

    title(['Spatial distribution of schools (vertical projection) at ',num2str(seuil(i)),'dB threshold, size represents length, color represents density'])
    colorbar;
    axis([min(position_ME70(:,1)) max(position_ME70(:,1)) min(position_ME70(:,2)) max(position_ME70(:,2))]);
    caxis( [min(height_ME70(:)) max(height_ME70(:))]);
%     view(-40,20);
    saveas(gcf,[chemin,'r�partition spatiale seuil ',num2str(seuil(i)),'dB'],'fig')
    saveas(gcf,[chemin,'r�partition spatiale seuil ',num2str(seuil(i)),'dB'],'jpg')
    

end

% figure
% for i=1:nb_seuil
% 
%     %             nbpoints=size(schools_ME70(1,i).contours,2)
%     %             for j=1:nbpoints
%     %                 lat(j)=schools_ME70(1,i).contours(j).m_latitude;
%     %                 long(j)=schools_ME70(1,i).contours(j).m_longitude;
%     %                 depth(j)=schools_ME70(1,i).contours(j).m_depth;
%     %             end
%     %
%     %             if nbpoints>50
%     %
%     %                 scatter3(lat,long,depth,10,nblME70*ones(1,nbpoints));
%     %             end
%     %             clear lat;
%     %             clear long
%     %             clear depth;
% end


