close all;
clear all;

%% eventually, data import

%chemin='G:\PELGAS09\HAC-Hermes\RUN034\day\'
%chemin='/media/HAC09/pelgas09/HAC-Hermes/run034/day/'
chemin='F:\Partage\M3D_examples\ShoalExtraction\'

filedir = dirr([chemin,'/*school.mat']);  % ensemble des fichiers
%nb_seuil = size(filedir,1);  % nombre de seuils
nb_seuil=3

chemin_save=[chemin,'shlmat/']
path=[chemin_save,'shl-556065.mat']

load (path);

chemin_plots=[chemin,'results/']
   if ~exist(chemin_plots,'dir');
        mkdir(chemin_plots);
   end;

seuil_sigma_ag=0;
seuil_volume = 0;

%whof = whos('-file', '/export/home/Mathieu/Projects/BoB/Isobay/proc/shlmat/shl-55-60.mat');
%load /export/home/Mathieu/Projects/BoB/Isobay/proc/shlmat/shl-55-60.mat

debut=datenum('29-May-2009 07:59:00');
fin=datenum('29-May-2009 20:00:00');

%% Threshold selection

tabulate((time_ME70)>debut);
tabulate((time_ME70)<fin);

index= find((time_ME70)>debut & (time_ME70)<fin  & volume_ME70>seuil_volume);

position_ME70_60=position_ME70(seuil_ME70==-60,:);
time_ME70_60=time_ME70(1,seuil_ME70==-60);

position_ME70_55=position_ME70(seuil_ME70==-55,:);
time_ME70_55=time_ME70(1,seuil_ME70==-55);

position_ME70_65=position_ME70(seuil_ME70==-65,:);
time_ME70_65=time_ME70(1,seuil_ME70==-65);

position_ER60_55=position_ER60(seuil_ER60==-55,:);
time_ER60_55=time_ER60(1,seuil_ER60==-55);

position_ER60_60=position_ER60(seuil_ER60==-60,:);
time_ER60_60=time_ER60(1,seuil_ER60==-60);

position_ER60_65=position_ER60(seuil_ER60==-65,:);
time_ER60_65=time_ER60(1,seuil_ER60==-65);

%% shoal depth distribution

subplot(2,2,1);
hist(position_ME70(seuil_ME70==-60,3));
title('Acoustic shoals depth distribution of , ME70 -60dB');
subplot(2,2,2);
hist(position_ME70(seuil_ME70==-55,3));
title('Depth distribution of acoustic shoals, ME70 -55dB');
subplot(2,2,3);
hist(position_ER60(seuil_ER60==-60,3));
title('Depth distribution of acoustic shoals, ER60 -60dB');
h = findobj(gca,'Type','patch');
set(h,'FaceColor','r','EdgeColor','w');
subplot(2,2,4);
hist(position_ER60(seuil_ER60==-55,3));
title('Depth distribution of acoustic shoals, ER60 -55dB');
h = findobj(gca,'Type','patch');
set(h,'FaceColor','r','EdgeColor','w');

saveas(gcf,[chemin_plots,sprintf('zshl_hist')],'fig');
saveas(gcf,[chemin_plots,sprintf('zshl_hist')],'jpg');

%% Shoal position: depth:time, all shoals on same graph

plot(time_ME70_60,-position_ME70_60(:,3),'o');
datetick('x',15,'keepticks');
hold on;
plot(time_ME70_55,-position_ME70_55(:,3),'or');
datetick('x',15,'keepticks');
legend('ME70 shoals, -60dB threshold','ME70 shoals, -55dB threshold','Location','SouthEast');
xlabel('Time');
ylabel('Depth (m)');

saveas(gcf,[chemin_plots,sprintf('z~t_shl_ME70')],'fig');
saveas(gcf,[chemin_plots,sprintf('z~t_shl_ME70')],'jpg');

plot(time_ER60_60,-position_ER60_60(:,3),'s');
datetick('x',15,'keepticks');
hold on;
plot(time_ER60_55,-position_ER60_55(:,3),'sr');
datetick('x',15,'keepticks');
legend('ER60 shoals, -60dB threshold','ER60 shoals, -55dB threshold');
xlabel('Time');
ylabel('Depth (m)');

saveas(gcf,[chemin_plots,sprintf('z~t_shl_ER60')],'fig');
saveas(gcf,[chemin_plots,sprintf('z~t_shl_ER60')],'jpg');

plot(time_ME70_60,-position_ME70_60(:,3),'o');
datetick('x',15,'keepticks');
hold on;
plot(time_ER60_60,-position_ER60_60(:,3),'sr');
datetick('x',15,'keepticks');
legend('ME70 shoals, -60dB threshold','ER60 shoals, -60dB threshold');
xlabel('Time');
ylabel('Depth (m)');

saveas(gcf,[chemin_plots,sprintf('z~t_shl_ME70ER60-60dB')],'fig');
saveas(gcf,[chemin_plots,sprintf('z~t_shl_ME70ER60-60dB')],'jpg');

plot(time_ME70_55,-position_ME70_55(:,3),'o');
datetick('x',15,'keepticks');
hold on;
plot(time_ER60_55,-position_ER60_55(:,3),'sr');
datetick('x',15,'keepticks');
legend('ME70 shoals, -55dB threshold','ER60 shoals, -55dB threshold');
xlabel('Time');
ylabel('Depth (m)');

saveas(gcf,[chemin_plots,sprintf('z~t_shl_ME70ER60-55dB')],'fig');
saveas(gcf,[chemin_plots,sprintf('z~t_shl_ME70ER60-55dB')],'jpg');

%% Segmentation: time:depth sequence selection

multishl=cat(1,length_ME70,width_ME70,height_ME70,volume_ME70,MVBS_ME70,sigma_ag_ME70)';
multishl=cat(2,multishl,position_ME70(:,3));

multishl65=multishl(seuil_ME70==-65,:);
multishl60=multishl(seuil_ME70==-60,:);
multishl55=multishl(seuil_ME70==-55,:);

monoshl=cat(1,length_ER60,width_ER60,height_ER60,volume_ER60,MVBS_ER60,sigma_ag_ER60)';
monoshl=cat(2,monoshl,position_ER60(:,3));

monoshl65=monoshl(seuil_ER60==-65,:);
monoshl60=monoshl(seuil_ER60==-60,:);
monoshl55=monoshl(seuil_ER60==-55,:);

unique(seuil_ER60)

tmin=datenum('29-May-2009 12:17:00');
tmax=datenum('29-May-2009 17:30:00');

nvar=strvcat('length','width','height','volume','MVBS','sigma_ag','depth')

seltotME70_55=time_ME70_55<tmax & time_ME70_55>tmin & (position_ME70_55(:,3)<33)';
seltotME70_60=time_ME70_60<tmax & time_ME70_60>tmin & (position_ME70_60(:,3)<33)';
seltotME70_65=time_ME70_65<tmax & time_ME70_65>tmin & (position_ME70_65(:,3)<33)';
seltotER60_60=time_ER60_60<tmax & time_ER60_60>tmin & (position_ER60_60(:,3)<33)' & (position_ER60_60(:,3)>7)';
seltotER60_55=time_ER60_55<tmax & time_ER60_55>tmin & (position_ER60_55(:,3)<33)' & (position_ER60_55(:,3)>7)';
seltotER60_65=time_ER60_65<tmax & time_ER60_65>tmin & (position_ER60_65(:,3)<33)' & (position_ER60_65(:,3)>7)';

size(time_ME70_65(:,seltotME70_65))
size(time_ME70_60(:,seltotME70_60))
size(time_ME70_55(:,seltotME70_55))
size(time_ER60_65(:,seltotER60_65))
size(time_ER60_60(:,seltotER60_60))
size(time_ER60_55(:,seltotER60_55))

time_ME70_65sf=time_ME70_65(:,seltotME70_65);
position_ME70_65sf=position_ME70_65(seltotME70_65,:);
multishl65s=multishl65(seltotME70_65,:);

time_ME70_60sf=time_ME70_60(:,seltotME70_60);
position_ME70_60sf=position_ME70_60(seltotME70_60,:);
multishl60s=multishl60(seltotME70_60,:);

time_ME70_55sf=time_ME70_55(:,seltotME70_55);
position_ME70_55sf=position_ME70_55(seltotME70_55,:);
multishl55s=multishl55(seltotME70_55,:);

time_ER60_65sf=time_ER60_65(:,seltotER60_65);
position_ER60_65sf=position_ER60_65(seltotER60_65,:);
monoshl65s=monoshl65(seltotER60_65,:);

time_ER60_60sf=time_ER60_60(:,seltotER60_60);
position_ER60_60sf=position_ER60_60(seltotER60_60,:);
monoshl60s=monoshl60(seltotER60_60,:);

time_ER60_55sf=time_ER60_55(:,seltotER60_55);
position_ER60_55sf=position_ER60_55(seltotER60_55,:);
monoshl55s=monoshl55(seltotER60_55,:);

%% Distribution of shoal descriptors

subplot(3,1,1)
boxplot(multishl65s,'orientation','horizontal','labels',nvar)
xlim([-60 50]) 
title('Shoal descriptors, ME70 -65dB')
text(repmat(51,7,1),[1:7],num2str(mean(multishl65s,1)','%6.2g'))
text(55,7.5,'Mean')
subplot(3,1,2)
boxplot(multishl60s,'orientation','horizontal','labels',nvar)
xlim([-60 50]) 
title('Shoal descriptors, ME70 -60dB')
text(repmat(51,7,1),[1:7],num2str(mean(multishl60s,1)','%6.2g'))
text(55,7.5,'Mean')
subplot(3,1,3)
boxplot(multishl55s,'orientation','horizontal','labels',nvar)
xlim([-60 50]) 
title('Shoal descriptors, ME70 -55dB')
text(repmat(51,7,1),[1:7],num2str(mean(multishl55s,1)','%6.2g'))
text(55,7.5,'Mean')

saveas(gcf,[chemin_plots,sprintf('shl_descriptors_ME70-556065dB')],'fig')
saveas(gcf,[chemin_plots,sprintf('shl_descriptors_ME70-556065dB')],'jpg')

subplot(3,1,1)
boxplot(monoshl65s,'orientation','horizontal','labels',nvar)
xlim([-60 50]) 
title('Shoal descriptors, ER60 -65dB')
text(repmat(51,7,1),[1:7],num2str(mean(monoshl65s,1)','%6.2g'))
text(55,7.5,'Mean')
subplot(3,1,2)
boxplot(monoshl60s,'orientation','horizontal','labels',nvar)
xlim([-60 50]) 
title('Shoal descriptors, ER60 -60dB')
text(repmat(51,7,1),[1:7],num2str(mean(monoshl60s,1)','%6.2g'))
text(55,7.5,'Mean')
subplot(3,1,3)
boxplot(monoshl55s,'orientation','horizontal','labels',nvar)
xlim([-60 50]) 
title('Shoal descriptors, ER60 -55dB')
text(repmat(51,7,1),[1:7],num2str(mean(monoshl55s,1)','%6.2g'))
text(55,7.5,'Mean')

saveas(gcf,[chemin_plots,sprintf('shl_descriptors_ER60-556065dB')],'fig')
saveas(gcf,[chemin_plots,sprintf('shl_descriptors_ER60-556065dB')],'jpg')

subplot(2,2,1)
boxplot(multishl60s,'orientation','horizontal','labels',nvar)
xlim([-60 50]) 
title('Shoal descriptors, ME70 -60dB')
text(repmat(51,7,1),[1:7],num2str(mean(multishl60s,1)','%6.2g'))
text(55,7.5,'Mean')
subplot(2,2,2)
boxplot(multishl55s,'orientation','horizontal','labels',nvar)
xlim([-60 50]) 
title('Shoal descriptors, ME70 -55dB')
text(repmat(51,7,1),[1:7],num2str(mean(multishl55s,1)','%6.2g'))
text(55,7.5,'Mean')
subplot(2,2,3)
boxplot(monoshl60s,'orientation','horizontal','labels',nvar)
xlim([-60 50]) 
title('Shoal descriptors, ER60 -60dB')
text(repmat(51,7,1),[1:7],num2str(mean(monoshl60s,1)','%6.2g'))
text(55,7.5,'Mean')
subplot(2,2,4)
boxplot(monoshl55s,'orientation','horizontal','labels',nvar)
xlim([-60 50]) 
title('Shoal descriptors, ER60 -55dB')
text(repmat(51,7,1),[1:7],num2str(mean(monoshl55s,1)','%6.2g'))
text(55,7.5,'Mean')

saveas(gcf,[chemin_plots,sprintf('shl_descriptors_ME70ER60-5560dB')],'fig')
saveas(gcf,[chemin_plots,sprintf('shl_descriptors_ME70ER60-5560dB')],'jpg')

%% Selected shoal position: depth~time

subplot(3,1,1)
plot(time_ME70_65,-position_ME70_65(:,3),'o')
datetick('x',15,'keepticks')
hold on
plot(time_ME70_65sf,-position_ME70_65sf(:,3)','+r')
datetick('x',15,'keepticks')
xlabel='Time'
ylabel='Depth (m)'
legend('All shoals','Selected shoals','Location','SouthEast')
title('ME70 shoals, -65dB threshold')
subplot(3,1,2)
plot(time_ME70_60,-position_ME70_60(:,3),'o')
datetick('x',15,'keepticks')
hold on
plot(time_ME70_60sf,-position_ME70_60sf(:,3)','+r')
datetick('x',15,'keepticks')
xlabel='Time'
ylabel='Depth (m)'
legend('All shoals','Selected shoals','Location','SouthEast')
title('ME70 shoals, -60dB threshold')
subplot(3,1,3)
plot(time_ME70_55,-position_ME70_55(:,3),'o')
datetick('x',15,'keepticks')
hold on
plot(time_ME70_55sf,-position_ME70_55sf(:,3)','+r')
datetick('x',15,'keepticks')
xlabel='Time'
ylabel='Depth (m)'
legend('All shoals','Selected shoals','Location','SouthEast')
title('ME70 shoals, -55dB threshold')

saveas(gcf,[chemin_plots,sprintf('z~t2_SURFshl_ME70-656055dB')],'fig')
saveas(gcf,[chemin_plots,sprintf('z~t2_SURFshl_ME70-656055dB')],'jpg')

subplot(2,1,1)
plot(time_ME70_60,-position_ME70_60(:,3),'o')
datetick('x',15,'keepticks')
hold on
plot(time_ME70_60sf,-position_ME70_60sf(:,3)','+r')
datetick('x',15,'keepticks')
xlabel='Time'
ylabel='Depth (m)'
legend('All shoals','Selected shoals','Location','SouthEast')
title('ME70 shoals, -60dB threshold')
subplot(2,1,2)
plot(time_ER60_60,-position_ER60_60(:,3),'o')
datetick('x',15,'keepticks')
hold on
plot(time_ER60_60sf,-position_ER60_60sf(:,3)','+r')
datetick('x',15,'keepticks')
xlabel='Time'
ylabel='Depth (m)'
legend('All shoals','Selected shoals','Location','SouthEast')
title('ER60 shoals, -60dB threshold')

saveas(gcf,[chemin_plots,sprintf('z~t2_SURFshl_ME70ER60-60dB')],'fig')
saveas(gcf,[chemin_plots,sprintf('z~t2_SURFshl_ME70ER60-60dB')],'jpg')

%% shoal typology: PCA on selected ME70 shoal descriptors

nvar=strvcat('length','width','height','volume','MVBS','sigma_ag','depth')

X=multishl60s;

Xstd=(X-repmat(mean(X,1),size(X,1),1))./repmat(std(X,1),size(X,1),1);

boxplot(Xstd,'orientation','horizontal','labels',nvar)

%PCA on shoal descriptors

[coefs,scores,variances,t2] = princomp(Xstd);

percent_explained = 100*variances/sum(variances)
pareto(percent_explained)
xlabel('Principal Component')
ylabel('Variance Explained (%)')

biplot(coefs(:,1:2), 'scores',scores(:,1:2),'varlabels',nvar);

%% PCA on ME70 shoal descriptors w/o extreme shoal

Xs=multishl60s(time_ME70_60sf~=time_ME70_60sf(317),:);

Xsstd=(Xs-repmat(mean(Xs,1),size(Xs,1),1))./repmat(std(Xs,1),size(Xs,1),1);

[coefs2,scores2,variances2,t2] = princomp(Xsstd);

percent_explained2 = 100*variances2/sum(variances2)
pareto(percent_explained2)
xlabel('Principal Component')
ylabel('Variance Explained (%)')

biplot(coefs2(:,1:3), 'scores',scores2(:,1:3),'varlabels',nvar)

Xpc=scores2(:,1:3);

% kmeans clustering

[cidx2,cmeans2] = kmeans(Xpc,2,'dist','sqeuclidean')
[silh2,h] = silhouette(Xpc,cidx2,'sqeuclidean');

[cidx3,cmeans3,sumd3] = kmeans(Xpc,2,'replicates',5,'display','final');
sum(sumd3)

% HAC clustering

eucD = pdist(Xpc,'euclidean');
clustTreeEuc = linkage(eucD,'average');
cophenet(clustTreeEuc,eucD)
[h,nodes] = dendrogram(clustTreeEuc,0);

% Biplots

vmult=10;
ptsymb = {'bs','r^','md','go','c+'};

%subplot(2,2,1)
for i = 1:size(unique(cidx2),1)
    clust = find(cidx2==i);
    plot(Xpc(clust,1),Xpc(clust,2),ptsymb{i});
    hold on
end
hold on
text(coefs2(:,1)*vmult,coefs2(:,2)*vmult,nvar)
h=compass(coefs2(:,1)*(vmult-1),coefs2(:,2)*(vmult-1))
set(h,'Color',[.8 .8 .8],'MarkerSize',6);
line([min(scores2(:,1))-1 max(scores2(:,1))+1],[0 0],'Color',[.8 .8 .8])
line([0 0],[min(scores2(:,2))-1 max(scores2(:,2))+1],'Color',[.8 .8 .8])
title({'PCA biplot + kmeans clustering of ME70 shoal descriptors at -60dB'});
legend('Cluster1','Cluster2');
xlabel('PC1');
ylabel('PC2');

saveas(gcf,[chemin_plots,sprintf('biplotPC12_PCA+km_shl_ME70-60dB')],'fig')
saveas(gcf,[chemin_plots,sprintf('biplotPC12_PCA+km_shl_ME70-60dB')],'jpg')

vmult=4;
figure;
for i = 1:size(unique(cidx2),1)
    clust = find(cidx2==i);
    plot(Xpc(clust,1),Xpc(clust,3),ptsymb{i});
    hold on
end
hold on
text(coefs2(:,1)*vmult,coefs2(:,3)*vmult,nvar)
h=compass(coefs2(:,1)*(vmult-1),coefs2(:,3)*(vmult-1))
set(h,'Color',[.8 .8 .8],'MarkerSize',6);
line([min(scores2(:,1))-1 max(scores2(:,1))+1],[0 0],'Color',[.8 .8 .8])
line([0 0],[min(scores2(:,3))-1 max(scores2(:,3))+1],'Color',[.8 .8 .8])
title({'Biplot of PCA + clustering of shoal ME70 -60dB descriptors'});
legend('Cluster 1','Cluster 2');
xlabel('PC1');
ylabel('PC3');

saveas(gcf,[chemin_plots,sprintf('biplotPC13_PCA+km_shl_ME70-60dB')],'fig')
saveas(gcf,[chemin_plots,sprintf('biplotPC13_PCA+km_shl_ME70-60dB')],'jpg')

vmult=5;
figure;
for i = 1:size(unique(cidx2),1)
    clust = find(cidx2==i);
    plot(Xpc(clust,2),Xpc(clust,3),ptsymb{i});
    hold on
end
hold on
text(coefs2(:,2)*vmult,coefs2(:,3)*vmult,nvar)
h=compass(coefs2(:,2)*(vmult-1),coefs2(:,3)*(vmult-1))
set(h,'Color',[.8 .8 .8],'MarkerSize',6);
line([min(scores2(:,2))-1 max(scores2(:,2))+1],[0 0],'Color',[.8 .8 .8])
line([0 0],[min(scores2(:,3))-1 max(scores2(:,3))+1],'Color',[.8 .8 .8])
title({'Biplot of PCA + clustering of shoal ME70 -60dB descriptors'});
legend('Cluster 1','Cluster 2');
xlabel('PC2');
ylabel('PC3');

saveas(gcf,[chemin_plots,sprintf('biplotPC23_PCA+km_shl_ME70-60dB')],'fig')
saveas(gcf,[chemin_plots,sprintf('biplotPC23_PCA+km_shl_ME70-60dB')],'jpg')

%3d plots

% with a loop
for i = 1:3
    clust = find(cidx2==i);
    plot3(Xpc(clust,1),Xpc(clust,2),Xpc(clust,3),ptsymb{i});
    hold on
end
plot3(cmeans2(:,1),cmeans2(:,2),cmeans2(:,3),'ko');
plot3(cmeans2(:,1),cmeans2(:,2),cmeans2(:,3),'kx');
plot3(coefs2(:,1),coefs2(:,2),coefs2(:,3),'-')

for i = 1:size(unique(cidx2),1)
    clust = find(cidx2==i);
    plot3(position_ME70_60sf(clust,2),position_ME70_60sf(clust,1),-position_ME70_60sf(clust,3),ptsymb{i});
    hold on
end

% with 'scatter' function

clust=vertcat(cidx2(1:316),3,cidx2(317:size(cidx2,1)));
scatter(time_ME70_60sf,-position_ME70_60sf(:,3),log(multishl60s(:,4)+1)*50,clust,'filled');
datetick('x',15,'keepticks');

scatter3(position_ME70_60sf(:,2),position_ME70_60sf(:,1),-position_ME70_60sf(:,3),log(multishl60s(:,4)+1)*10,clust,'filled');
xlabel('Longitude');
ylabel('Latitude');
zlabel('Depth (m)');

%% PCA on ER60 shoal descriptors w/o extreme shoal

Y=monoshl60s;

Ystd=Y./repmat(std(Y,1),size(Y,1),1);

[coefs3,scores3,variances3,t2] = princomp(Ystd);

percent_explained3 = 100*variances3/sum(variances3)
pareto(percent_explained3);
xlabel('Principal Component');
ylabel('Variance Explained (%)');

biplot(coefs3(:,1:3), 'scores',scores3(:,1:3),'varlabels',nvar)

Ypc=scores3(:,1:3);

% kmeans clustering

[cidx3,cmeans3] = kmeans(Ypc,2,'dist','sqeuclidean')
[silh3,h] = silhouette(Ypc,cidx3,'sqeuclidean');

[cidx4,cmeans4,sumd4] = kmeans(Ypc,2,'replicates',5,'display','final');
sum(sumd4)

% HAC clustering

eucD = pdist(Xpc,'euclidean');
clustTreeEuc = linkage(eucD,'average');
cophenet(clustTreeEuc,eucD)
[h,nodes] = dendrogram(clustTreeEuc,0);

% Biplots

vmult=6;
ptsymb = {'bs','r^','md','go','c+'};

%subplot(2,2,1)
for i = 1:size(unique(cidx3),1)
    clust = find(cidx3==i);
    plot(Ypc(clust,1),Ypc(clust,2),ptsymb{i});
    hold on
end
hold on
text(coefs3(:,1)*vmult,coefs3(:,2)*vmult,nvar)
line([min(scores3(:,1))-1 max(scores3(:,1))+1],[0 0],'Color',[.8 .8 .8])
line([0 0],[min(scores3(:,2))-1 max(scores3(:,2))+1],'Color',[.8 .8 .8])
title({'Biplot of PCA + clustering of shoal ER60 -60dB descriptors','kmeans clustering'});
legend('Cluster1','Cluster2');
xlabel('PC1')
ylabel('PC2')

%subplot(2,2,2)


clust2=cidx3;
scatter(time_ER60_60sf,-position_ER60_60sf(:,3),log(monoshl60s(:,4)+1)*10,clust2,'filled');
datetick('x',15,'keepticks');




% Remove offshore areas

Xs=X(time_ME70_60<tmax & time_ME70_60>tmin,:);

sel=time_ME70_60<tmax & time_ME70_60>tmin;

plot(time_ME70_60(:,time_ME70_60<tmax & time_ME70_60>tmin),-position_ME70_60(time_ME70_60<tmax & time_ME70_60>tmin,3),'o')
datetick('x',15,'keepticks')

time_ME70_60s=time_ME70_60(:,sel);
position_ME70_60s=position_ME70_60(sel,:);

Xsstd=(Xs-repmat(mean(Xs,1),size(Xs,1),1))./repmat(std(Xs,1),size(Xs,1),1);

boxplot(Xsstd,'orientation','horizontal','labels',nvar)

%PCA on shoal descriptors

[coefs,scores,variances,t2] = princomp(Xsstd);

percent_explained = 100*variances/sum(variances)
pareto(percent_explained)
xlabel('Principal Component')
ylabel('Variance Explained (%)')

plot(scores(:,1),scores(:,2),'+')
xlabel('1st Principal Component')
ylabel('2nd Principal Component')

%names=num2str([1:size(Xs,1)]);
%gname(names)

datestr(datevec(time_ME70_60s(:,86192)))

plot(time_ME70_60s,-position_ME70_60s(:,3),'o')
datetick('x',15,'keepticks')
hold on
plot(time_ME70_60s(:,scores(:,2)<-50),-position_ME70_60s(scores(:,2)<-50,3),'or')

biplot(coefs(:,1:2), 'scores',scores(:,1:2),'varlabels',nvar);

%surface layer
sel2=position_ME70_60s(:,3)<60;
tabulate(sel2)
size(time_ME70_60s2)
Xs2=Xs(sel2,:);

time_ME70_60s2=time_ME70_60s(:,sel2);
position_ME70_60s2=position_ME70_60s(sel2,:);

hist(position_ME70_60s2(:,3))

plot(time_ME70_60s2,-position_ME70_60s2(:,3),'o')
datetick('x',15,'keepticks')

Xs2std=(Xs2-repmat(mean(Xs2,1),size(Xs2,1),1))./repmat(std(Xs2,1),size(Xs2,1),1);

boxplot(Xs2std,'orientation','horizontal','labels',nvar)

%PCA on shoal descriptors

[coefs2,scores2,variances2,t2] = princomp(Xs2std);

percent_explained2 = 100*variances2/sum(variances2)
pareto(percent_explained2)
xlabel('Principal Component')
ylabel('Variance Explained (%)')

plot(scores2(:,1),scores2(:,2),'+')
xlabel('1st Principal Component')
ylabel('2nd Principal Component')

biplot(coefs2(:,1:2), 'scores',scores2(:,1:2),'varlabels',nvar);

size(scores2)

Xpc=scores2(:,1:5);

IDX=kmeans(Xpc,2);
size(IDX)
size(time_ME70_60s2)

plot(time_ME70_60s2,-position_ME70_60s2(:,3),'o')
datetick('x',15,'keepticks')
hold on
%plot(time_ME70_60s2(:,IDX==2),-position_ME70_60s2(IDX==2,3),'or')
%datetick('x',15,'keepticks')

saveas(gcf,[chemin_plots,sprintf('z~t2_SURFshl_ME70ER60-60dB')],'fig')
saveas(gcf,[chemin_plots,sprintf('z~t2_SURFshl_ME70ER60-60dB')],'jpg')

%new selection

tmin2=datenum('29-May-2009 12:17:00');

sel3=(position_ME70_60s2(:,3)<33)'&time_ME70_60s2 > tmin2;

tabulate(sel3)
size(time_ME70_60s2)
Xs3=Xs(sel3,:);

time_ME70_60s3=time_ME70_60s2(:,sel3);
position_ME70_60s3=position_ME70_60s2(sel3,:);

hist(position_ME70_60s3(:,3))

plot(time_ME70_60s3,-position_ME70_60s3(:,3),'o')
datetick('x',15,'keepticks')

Xs3std=(Xs3-repmat(mean(Xs3,1),size(Xs3,1),1))./repmat(std(Xs3,1),size(Xs3,1),1);

%PCA on shoal descriptors

[coefs3,scores3,variances3,t2] = princomp(Xs3std);

percent_explained3 = 100*variances3/sum(variances3)
pareto(percent_explained3)
xlabel('Principal Component')
ylabel('Variance Explained (%)')

plot(scores3(:,1),scores3(:,2),'+')
xlabel('1st Principal Component')
ylabel('2nd Principal Component')

%names=num2str([1:size(Xs3,1)]);
%gname(names)
tr=time_ME70_60s3(:,376);
datestr(datevec(time_ME70_60s3(:,376)))
position_ME70_60s3(376,3)
biplot(coefs3(:,1:2), 'scores',scores3(:,1:2),'varlabels',nvar);


time_ME70_60s4=time_ME70_60s3(:,time_ME70_60s3~=tr);
position_ME70_60s4=position_ME70_60s3(time_ME70_60s3~=tr,:);

Xs4=Xs3(time_ME70_60s3~=tr,:);

Xs4std=(Xs4-repmat(mean(Xs4,1),size(Xs4,1),1))./repmat(std(Xs4,1),size(Xs4,1),1);

%PCA on shoal descriptors

[coefs4,scores4,variances4,t2] = princomp(Xs4std);

percent_explained4 = 100*variances4/sum(variances4)
pareto(percent_explained4)
xlabel('Principal Component')
ylabel('Variance Explained (%)')

plot(scores4(:,1),scores4(:,2),'+')
xlabel('1st Principal Component')
ylabel('2nd Principal Component')

biplot(coefs4(:,1:2), 'scores',scores4(:,1:2),'varlabels',nvar);

Xpc=scores4(:,1:4);

IDX=kmeans(Xpc,2);
size(IDX)
size(time_ME70_60s2)

plot(time_ME70_60s4,-position_ME70_60s4(:,3),'o')
datetick('x',15,'keepticks')
hold on
plot(time_ME70_60s4(:,IDX==2),-position_ME70_60s4(IDX==2,3),'or')
datetick('x',15,'keepticks')

plot(scores4(:,1),scores4(:,2),'+')
xlabel('1st Principal Component')
ylabel('2nd Principal Component')
hold on
plot(scores4(IDX==2,1),scores4(IDX==2,2),'+r')

%N=412

%ER60


time_ER60_60s=time_ER60_60(:,seltot);
position_ER60_60s=position_ER60_60(seltot,:);

size(time_ER60_60s)
size(position_ER60_60s)

plot(time_ER60_60s,-position_ER60_60(:,3),'o')
datetick('x',15,'keepticks')

Ysstd=(Ys-repmat(mean(Ys,1),size(Ys,1),1))./repmat(std(Ys,1),size(Ys,1),1);

boxplot(Ysstd,'orientation','horizontal','labels',nvar)

%PCA on shoal descriptors

[coefsy,scoresy,variancesy,t2] = princomp(Ysstd);

percent_explainedy = 100*variancesy/sum(variancesy)
pareto(percent_explainedy)
xlabel('Principal Component')
ylabel('Variance Explained (%)')

plot(scoresy(:,1),scoresy(:,2),'+')
xlabel('1st Principal Component')
ylabel('2nd Principal Component')

biplot(coefsy(:,1:2), 'scores',scoresy(:,1:2),'varlabels',nvar);



%% Threshold sensitivity analysis
seuil_sigma_ag=0;
seuil_volume = 0;

debut=datenum('29/05/2009 07:59');
fin=datenum('29/05/2009 20:00');

debut=min(time_ME70)-1;
fin=max(time_ME70)+1;

%% Quantile plots

seuil=[-65 -60 -55]
nb_seuil=3

tabulate((time_ME70)>debut)
tabulate((time_ME70)<fin)

seuil_volume=0

sel=(time_ME70)>debut & (time_ME70)<fin  & volume_ME70>seuil_volume;
tabulate(sel)

% chemin='G:\PELGAS09\HAC-Hermes\RUN034\day\'
% 
chemin='F:\Partage\M3D_examples\ShoalExtraction\'

% filedir = dirr([chemin,'/*school.mat']);  % ensemble des fichiers
% nb_seuil = size(filedir,1);  % nombre de seuils
nb_seuil =3
% 
% chemin_save=[chemin,'shlmat\']
% 
path=[chemin_save,'shl-556065.mat']
% 
% 
load (path)
% 
% chemin_plots=[chemin,'results\']
%    if ~exist(chemin_plots,'dir')
%         mkdir(chemin_plots)
%    end


index= find(sel);

% Shoal length
%----------------------
   
for i=1:nb_seuil
    percentilelength(i,:)=prctile(length_ME70(find(seuil_ME70(index)==seuil(i))),[10 50 90]);
    occurence(i)=size(length_ME70(find(seuil_ME70(index)==seuil(i))),2);
end

figure;
[AX,H1,H2] = plotyy(seuil,percentilelength,seuil,occurence);
set(AX(1),'YColor','r') 
set(AX(2),'YColor','k') 
set(get(AX(1),'Ylabel'),'String','Length (m)','Color','r') 
set(get(AX(2),'Ylabel'),'String','Number','Color','k')
%xlabel('Threshold (dB)');
set(H1([1,3]),'LineStyle','--','LineWidth',3,'Color','r')
set(H1(2),'LineWidth',3,'Color','r')
set(H2,'LineStyle',':','LineWidth',3,'Color','k')

legend('Number of shoals','90% percentile', '50% percentile', '10% percentile');
title('Length distribution and number of acoustic shoals at various thresholds')

saveas(gcf,[chemin_plots,sprintf('Length')],'fig')
saveas(gcf,[chemin_plots,sprintf('Length')],'jpg')

% Shoal width
%----------------------

for i=1:nb_seuil
    percentileWidth(i,:)=prctile(width_ME70(find(seuil_ME70(index)==seuil(i))),[10 50 90]);
    occurence(i)=size(width_ME70(find(seuil_ME70(index)==seuil(i))),2);
end

figure;
[AX,H1,H2] = plotyy(seuil,percentileWidth,seuil,occurence);
set(AX(1),'YColor','r') 
set(AX(2),'YColor','k') 
set(get(AX(1),'Ylabel'),'String','Width (m)','Color','r') 
set(get(AX(2),'Ylabel'),'String','Number','Color','k')
%xlabel('Threshold (dB)');
set(H1([1,3]),'LineStyle','--','LineWidth',3,'Color','r')
set(H1(2),'LineWidth',3,'Color','r')
set(H2,'LineStyle',':','LineWidth',3,'Color','k')

legend('Number of shoals','90% percentile', '50% percentile', '10% percentile');
title('Width distribution and number of acoustic shoals at various thresholds')

saveas(gcf,[chemin_plots,sprintf('Width')],'fig')
saveas(gcf,[chemin_plots,sprintf('Width')],'jpg')

% Shoal height
%----------------------

for i=1:nb_seuil
    percentileHeight(i,:)=prctile(height_ME70(find(seuil_ME70(index)==seuil(i))),[10 50 90]);
    occurence(i)=size(height_ME70(find(seuil_ME70(index)==seuil(i))),2);
end

figure;
[AX,H1,H2] = plotyy(seuil,percentileHeight,seuil,occurence);
set(AX(1),'YColor','r') 
set(AX(2),'YColor','k') 
set(get(AX(1),'Ylabel'),'String','Height (m)','Color','r') 
set(get(AX(2),'Ylabel'),'String','Number','Color','k')
%xlabel('Threshold (dB)');
set(H1([1,3]),'LineStyle','--','LineWidth',3,'Color','r')
set(H1(2),'LineWidth',3,'Color','r')
set(H2,'LineStyle',':','LineWidth',3,'Color','k')

legend('Number of shoals','90% percentile', '50% percentile', '10% percentile');
title('Height distribution and number of acoustic shoals at various thresholds')

saveas(gcf,[chemin,sprintf('Height')],'fig')
saveas(gcf,[chemin,sprintf('Height')],'jpg')

% Shoal volume
%----------------------

for i=1:nb_seuil
    percentileVolume(i,:)=prctile(volume_ME70(find(seuil_ME70(index)==seuil(i))),[25 50 75]);
    occurence(i)=size(volume_ME70(find(seuil_ME70(index)==seuil(i))),2);
end

figure;
[AX,H1,H2] = plotyy(seuil,percentileVolume,seuil,occurence);
set(AX(1),'YColor','r') 
set(AX(2),'YColor','k') 
set(get(AX(1),'Ylabel'),'String','Volume (m3)','Color','r') 
set(get(AX(2),'Ylabel'),'String','Number','Color','k')
%xlabel('Threshold (dB)');
set(H1([1,3]),'LineStyle','--','LineWidth',3,'Color','r')
set(H1(2),'LineWidth',3,'Color','r')
set(H2,'LineStyle',':','LineWidth',3,'Color','k')

legend('Number of shoals','90% percentile', '50% percentile', '10% percentile');
title('Height distribution and number of acoustic shoals at various thresholds')

saveas(gcf,[chemin,sprintf('Volume')],'fig')
saveas(gcf,[chemin,sprintf('Volume')],'jpg')

% Aspect ratio

for i=1:nb_seuil
    percentileAspect(i,:)=prctile(volume_ME70(find(seuil_ME70(index)==seuil(i)))./(height_ME70(find(seuil_ME70(index)==seuil(i))).*width_ME70(find(seuil_ME70(index)==seuil(i))).*length_ME70(find(seuil_ME70(index)==seuil(i)))),[10 50 90]);
    occurence(i)=size(length_ME70(find(seuil_ME70(index)==seuil(i))),2);
end

figure;
[AX,H1,H2] = plotyy(seuil,percentileAspect,seuil,occurence);
set(AX(1),'YColor','r') 
set(AX(2),'YColor','k') 
set(get(AX(1),'Ylabel'),'String','Aspect','Color','r') 
set(get(AX(2),'Ylabel'),'String','Number','Color','k')
%xlabel('Threshold (dB)');
set(H1([1,3]),'LineStyle','--','LineWidth',3,'Color','r')
set(H1(2),'LineWidth',3,'Color','r')
set(H2,'LineStyle',':','LineWidth',3,'Color','k')

legend('Number of shoals','90% percentile', '50% percentile', '10% percentile');
title('Ratio of shoal volume over bounding box volume ("aspect") distribution and number of acoustic shoals at various threshold')

saveas(gcf,[chemin,sprintf('Aspect')],'fig')
saveas(gcf,[chemin,sprintf('Aspect')],'jpg')

% MVBS ratio

for i=1:nb_seuil
    percentileMVBS(i,:)=prctile(MVBS_ME70(find(seuil_ME70(index)==seuil(i))),[10 50 90]);
    occurence(i)=size(MVBS_ME70(find(seuil_ME70(index)==seuil(i))),2);
end
figure;
[AX,H1,H2] = plotyy(seuil,percentileMVBS,seuil,occurence);
set(AX(1),'YColor','r') 
set(AX(2),'YColor','k') 
set(get(AX(1),'Ylabel'),'String','Acoustic density (MVBS)','Color','r') 
set(get(AX(2),'Ylabel'),'String','Number','Color','k')
%xlabel('Threshold (dB)');
set(H1([1,3]),'LineStyle','--','LineWidth',3,'Color','r')
set(H1(2),'LineWidth',3,'Color','r')
set(H2,'LineStyle',':','LineWidth',3,'Color','k')

legend('Number of shoals','90% percentile', '50% percentile', '10% percentile');
title('Acoustic density (MVBS) distribution of acoustic shoals at various threshold')

saveas(gcf,[chemin,sprintf('MVBS')],'fig')
saveas(gcf,[chemin,sprintf('MVBS')],'jpg')

% figure;
% boxplot(length_ME70(index),seuil_ME70(index))
% xlabel('Threshold (dB)');
% ylabel('Length (m)');
% title('Length of school distribution for various threshold')
% saveas(gcf,[chemin,sprintf('Length ')],'fig')
% saveas(gcf,[chemin,sprintf('Length ')],'jpg')
% 
% figure;
% boxplot(width_ME70(index),seuil_ME70(index))
% xlabel('Threshold (dB)');
% ylabel('Width (m)');
% title('Width of school distribution for various threshold')
% saveas(gcf,[chemin,sprintf('Width ')],'fig')
% saveas(gcf,[chemin,sprintf('Width ')],'jpg')
% 
% figure;
% boxplot(height_ME70(index),seuil_ME70(index))
% xlabel('Threshold (dB)');
% ylabel('Height (m)');
% title('Height of school distribution for various threshold')
% saveas(gcf,[chemin,sprintf('Height ')],'fig')
% saveas(gcf,[chemin,sprintf('Height ')],'jpg')
% 
% figure;
% boxplot(volume_ME70(index),seuil_ME70(index))
% xlabel('Threshold (dB)');
% ylabel('Volume (m3)');
% title('Volume of school distribution for various threshold')
% saveas(gcf,[chemin,sprintf('Volume ')],'fig')
% saveas(gcf,[chemin,sprintf('Volume ')],'jpg')
% 
% figure;
% boxplot(height_ME70(index).*width_ME70(index).*length_ME70(index)./volume_ME70(index),seuil_ME70(index))
% xlabel('Threshold (dB)');
% ylabel('Volume ratio (unitless)');
% title('Volume ratio of school distribution relative to bounding box for various threshold')
% saveas(gcf,[chemin,sprintf('Aspect ratio')],'fig')
% saveas(gcf,[chemin,sprintf('Aspect ratio')],'jpg')
% 
% figure;
% boxplot(MVBS_ME70(index),seuil_ME70(index))
% xlabel('Threshold (dB)');
% ylabel('MVBS (dB)');
% title('MVBS of school distribution for various threshold')
% saveas(gcf,[chemin,sprintf('MVBS ')],'fig')
% saveas(gcf,[chemin,sprintf('MVBS ')],'jpg')


D=zeros(size(position_ME70(index),2),21);
for i=1:size(position_ME70(index),2)
    k=1;
    for j=max(1,i-10):min(i+10,size(position_ME70(index),2))
        D(i,k)=distance3D(position_ME70(index(i),:),position_ME70(index(j),:));
        k=k+1;
    end
end

for i=1:size(position_ME70(index),2)
    Dmin(i)=min(D(i,find(D(i,:)>0)));
end

for i=1:nb_seuil
    percentileDistance(i,:)=prctile(Dmin(find(seuil_ME70(index)==seuil(i))),[25 50 75]);
    occurence(i)=size(Dmin(find(seuil_ME70(index)==seuil(i))),2);
end
figure;
[AX,H1,H2] = plotyy(seuil,percentileDistance,seuil,occurence);
set(get(AX(1),'Ylabel'),'String','Distance (m)') 
set(get(AX(2),'Ylabel'),'String','Occurence')
xlabel('Threshold (dB)');

legend('Occurence','10% percentile', '50% percentile', '90% percentile');
title('Distance distribution of schools various threshold')
saveas(gcf,[chemin,sprintf('Distance')],'fig')
saveas(gcf,[chemin,sprintf('Distance')],'jpg')

% figure;
% boxplot(Dmin,seuil_ME70(index))
% xlabel('Threshold (dB)');
% ylabel('Distance (dB)');
% title('Distance between schools for various threshold')
% saveas(gcf,[chemin,sprintf('Distance ')],'fig')
% saveas(gcf,[chemin,sprintf('Distance ')],'jpg')


volume_ratio=zeros(nb_seuil,1);
for i=1:nb_seuil
    volume_ratio(i)=sum(volume_ME70(seuil_ME70(index)==seuil(i)))/sum(volume_ME70(seuil_ME70(index)==min(seuil)));
end

figure;
plot(seuil,volume_ratio);
xlabel('Threshold (dB)');
ylabel('Volume ratio (unitless)');
title('Volume ratio relative to lowest threshold')

saveas(gcf,[chemin,sprintf('Volume ratio')],'fig')
saveas(gcf,[chemin,sprintf('Volume ratio')],'jpg')

sigma_ag_ratio=zeros(nb_seuil,1);
for i=1:nb_seuil
    sigma_ag_ratio(i)=sum(sigma_ag_ME70(seuil_ME70(index)==seuil(i)))/sum(sigma_ag_ME70(seuil_ME70(index)==min(seuil)));
end

figure;
plot(seuil,sigma_ag_ratio);
xlabel('Threshold (dB)');
ylabel('Backscatter ratio (m)');
title('Backscatter ratio relative to lowest threshold')
saveas(gcf,[chemin,sprintf('Backscatter ratio ')],'fig')
saveas(gcf,[chemin,sprintf('Backscatter ratio ')],'jpg')




% for i=1:nb_seuil
% 
%     figure
%     scatter3(position_ME70(find(seuil_ME70()==seuil(i)),1),position_ME70(find(seuil_ME70()==seuil(i)),2),-position_ME70(find(seuil_ME70()==seuil(i)),3),sqrt(length_ME70(find(seuil_ME70()==seuil(i))).*width_ME70(find(seuil_ME70()==seuil(i)))),MVBS_ME70(find(seuil_ME70()==seuil(i))));
%     hold on;
% %     scatter3(reshape(Latitude(:,indexNav(1:10:end)),1,[]),reshape(Longitude(:,indexNav(1:10:end)),1,[]),-reshape(Depth(:,indexNav(1:10:end)),1,[]),'k');
%     xlabel('Latitude (�)');
%     ylabel('Longitude (�)');
%     ylabel('Depth (m)');
%     title(['Spatial distribution of schools at ',num2str(seuil(i)),'dB threshold, size represents length color represents density'])
%     colorbar;
%     axis([min(position_ME70(:,1)) max(position_ME70(:,1)) min(position_ME70(:,2)) max(position_ME70(:,2)) min(-position_ME70(:,3)) max(-position_ME70(:,3)) min(MVBS_ME70(:)) max(MVBS_ME70(:))]);
%     view(-40,20);
%     saveas(gcf,[chemin,'r�partition spatiale seuil ',num2str(seuil(i)),'dB'],'fig')
%     saveas(gcf,[chemin,'r�partition spatiale seuil ',num2str(seuil(i)),'dB'],'jpg')
%     
% 
% end

for i=1:nb_seuil

    figure
    scatter(position_ME70(find(seuil_ME70()==seuil(i)),1),position_ME70(find(seuil_ME70()==seuil(i)),2),sqrt(length_ME70(find(seuil_ME70()==seuil(i))).*width_ME70(find(seuil_ME70()==seuil(i)))),height_ME70(find(seuil_ME70()==seuil(i))));
    hold on;
%     scatter3(reshape(Latitude(:,indexNav(1:10:end)),1,[]),reshape(Longitude(:,indexNav(1:10:end)),1,[]),-reshape(Depth(:,indexNav(1:10:end)),1,[]),'k');
    xlabel('Latitude (�)');
    ylabel('Longitude (�)');

    title(['Spatial distribution of schools (vertical projection) at ',num2str(seuil(i)),'dB threshold, size represents length, color represents density'])
    colorbar;
    axis([min(position_ME70(:,1)) max(position_ME70(:,1)) min(position_ME70(:,2)) max(position_ME70(:,2))]);
    caxis( [min(height_ME70(:)) max(height_ME70(:))]);
%     view(-40,20);
    saveas(gcf,[chemin,'r�partition spatiale seuil ',num2str(seuil(i)),'dB'],'fig')
    saveas(gcf,[chemin,'r�partition spatiale seuil ',num2str(seuil(i)),'dB'],'jpg')
    

end

% figure
% for i=1:nb_seuil
% 
%     %             nbpoints=size(schools_ME70(1,i).contours,2)
%     %             for j=1:nbpoints
%     %                 lat(j)=schools_ME70(1,i).contours(j).m_latitude;
%     %                 long(j)=schools_ME70(1,i).contours(j).m_longitude;
%     %                 depth(j)=schools_ME70(1,i).contours(j).m_depth;
%     %             end
%     %
%     %             if nbpoints>50
%     %
%     %                 scatter3(lat,long,depth,10,nblME70*ones(1,nbpoints));
%     %             end
%     %             clear lat;
%     %             clear long
%     %             clear depth;
% end


