close all
clear all

%% Echo-int�gration (peut �tre comment� si elle a d�j� �t� ex�cut�e)

%chemin_ini='G:\PELGAS09\HAC-Hermes\RUN036\day\'
chemin_ini='G:\PELGAS09\HAC-Hermes\RUN034\day\'

%EI_thr=[-70 -65 -60 -55 -50]

%
EI_thr=[-59 -61]

%EI_thr=[-65 -50]

%ithri=3

for ithri=1:size(EI_thr,2)
    
    pathi=[chemin_ini,'EIshl',num2str(EI_thr(ithri)),'dB\']
    
    if ~exist(pathi,'dir')
        mkdir(pathi)
    end
%
    SampleShoalExtraction(chemin_ini,pathi);
%
    clear functions;

end


%% Import EI shoal results

chemin='G:\PELGAS09\HAC-Hermes\RUN034\day\'

filedir = dirr([chemin,'/*school.mat']);  % ensemble des fichiers
nb_seuil = size(filedir,1);  % nombre de seuils

% figure;
% hold on;
nblME70=0;
nblER60=0;

for numdir = 1:nb_seuil

    %seuil(numdir)=-str2num(filedir(numdir).name(size(filedir(numdir).name,2)-2:size(filedir(numdir).name,2)));
    seuil(numdir)=str2num(filedir(numdir).name(6:8))
    
    %filelist=ls([chemin,filedir(numdir).name,'/*school.mat']);
    %nb_files = size(filelist,1);

    filelist = squeeze(dir(fullfile([chemin,filedir(numdir).name],'*school.mat')));  % all files
    nb_files = size(filelist,1);  % number of files
    
    for numfile = 1:nb_files

        %matfilename = filelist(numfile,:);
        matfilename = filelist(numfile,:).name
        
        year = str2num(matfilename(14:17));
        month =  str2num(matfilename(18:19));
        day = str2num(matfilename(20:21));
        hour = str2num(matfilename(23:24));
        mn = str2num(matfilename(25:26));

%         datefile=datenum(year, month, day, hour, mn, 0);
%         if (datefile>=debut && datefile<=fin)

        FileName = [chemin,filedir(numdir).name,'/',matfilename(1:findstr('.',matfilename)-1),'.mat'];
        load(FileName);

            if  size(schools_ER60,2)>0

                for i=1:size(schools_ER60,2)
                    time_ER60(nblER60+i) = schools_ER60(1,i).time;
                    length_ER60(nblER60+i) = schools_ER60(1,i).length;
                    width_ER60(nblER60+i) = schools_ER60(1,i).width;
                    height_ER60(nblER60+i) = schools_ER60(1,i).height;
                    volume_ER60(nblER60+i) = schools_ER60(1,i).volume;
                    MVBS_ER60(nblER60+i) = schools_ER60(1,i).MVBS_weighted;
                    sigma_ag_ER60(nblER60+i) = schools_ER60(1,i).sigma_ag;
                    position_ER60(nblER60+i,:) = schools_ER60(1,i).position;
                    seuil_ER60(nblER60+i,:) = seuil(numdir);

                end


                nblER60=nblER60+size(schools_ER60,2);
            end

            if  size(schools_ME70,1)>0
                for i=1:size(schools_ME70,2)
                    time_ME70(nblME70+i) = schools_ME70(1,i).time;
                    length_ME70(nblME70+i) = schools_ME70(1,i).length;
                    width_ME70(nblME70+i) = schools_ME70(1,i).width;
                    height_ME70(nblME70+i) = schools_ME70(1,i).height;
                    volume_ME70(nblME70+i) = schools_ME70(1,i).volume;
                    MVBS_ME70(nblME70+i) = schools_ME70(1,i).MVBS_weighted;
                    sigma_ag_ME70(nblME70+i) = schools_ME70(1,i).sigma_ag;
                    position_ME70(nblME70+i,:) = schools_ME70(1,i).position;
                    seuil_ME70(nblME70+i,:) = seuil(numdir);

                end

                nblME70=nblME70+size(schools_ME70,2);
            end

            clear  schools_ER60  schools_ME70;

%         end

    end


end

% load([chemin,'Navigation.mat'])

% timenum=zeros(size(Time,1),1);
% for i=1:size(Time,1)
%     timenum(i)=datenum(Time(i,:),'dd/mm/yyyy HH:MM:SS.FFF');
% end
% indexNav = find(timenum>debut & timenum<fin);
% 
% MaxX=max( reshape(Longitude(:,indexNav),1,[]));
% MinX=min( reshape(Longitude(:,indexNav),1,[]));
% 
% MaxY=max(reshape(Latitude(:,indexNav),1,[]));
% MinY=min(reshape(Latitude(:,indexNav),1,[]));
% 
% nbPoints = 1000;
% Precision = max((MaxX-MinX)/nbPoints,(MaxY-MinY)/nbPoints)
% XI=MinX:Precision:MaxX;
% YI=MinY:Precision:MaxY;
%     figure;
% ZI = griddata( reshape(Longitude(:,indexNav),1,[]),reshape(Latitude(:,indexNav),1,[]),-reshape(Depth(:,indexNav),1,[]),XI,YI');
% hold on;
% surf(XI,YI,ZI,'LineStyle','none');
% view(0,70);
% light
% lighting gouraud

chemin_save=[chemin,'shlmat\']

   if ~exist(chemin_save,'dir')
        mkdir(chemin_save)
    end

save([chemin_save,'shl-556065.mat'],'MVBS_ER60','MVBS_ME70','height_ER60','height_ME70','length_ER60','length_ME70','position_ER60','position_ME70','seuil_ER60','seuil_ME70','sigma_ag_ER60','sigma_ag_ME70','time_ER60','time_ME70','volume_ER60','volume_ME70','width_ER60','width_ME70')


%% Analysis

clear all

% eventually, data import

chemin='G:\PELGAS09\HAC-Hermes\RUN034\day\'

filedir = dirr([chemin,'/*school.mat']);  % ensemble des fichiers
nb_seuil = size(filedir,1);  % nombre de seuils

chemin_save=[chemin,'shlmat\']
path=[chemin_save,'shl-556065.mat']

load (path)

chemin_plots=[chemin,'results\']
   if ~exist(chemin_plots,'dir')
        mkdir(chemin_plots)
   end

seuil_sigma_ag=0;
seuil_volume = 0;

%whof = whos('-file', '/export/home/Mathieu/Projects/BoB/Isobay/proc/shlmat/shl-55-60.mat');
%load /export/home/Mathieu/Projects/BoB/Isobay/proc/shlmat/shl-55-60.mat

debut=datenum('29/05/2009 07:59');
fin=datenum('29/05/2009 20:00');

% data selection

tabulate((time_ME70)>debut)
tabulate((time_ME70)<fin)

index= find((time_ME70)>debut & (time_ME70)<fin  & volume_ME70>seuil_volume);

position_ME70_60=position_ME70(seuil_ME70==-60,:);
time_ME70_60=time_ME70(1,seuil_ME70==-60);

position_ME70_55=position_ME70(seuil_ME70==-55,:);
time_ME70_55=time_ME70(1,seuil_ME70==-55);

position_ER60_55=position_ER60(seuil_ER60==-55,:);
time_ER60_55=time_ER60(1,seuil_ER60==-55);

position_ER60_60=position_ER60(seuil_ER60==-60,:);
time_ER60_60=time_ER60(1,seuil_ER60==-60);

%shoal depth distribution

subplot(2,2,1)
hist(position_ME70(seuil_ME70==-60,3))
title('Acoustic shoals depth distribution of , ME70 -60dB')
subplot(2,2,2)
hist(position_ME70(seuil_ME70==-55,3))
title('Depth distribution of acoustic shoals, ME70 -55dB')
subplot(2,2,3)
hist(position_ER60(seuil_ER60==-60,3))
title('Depth distribution of acoustic shoals, ER60 -60dB')
h = findobj(gca,'Type','patch');
set(h,'FaceColor','r','EdgeColor','w')
subplot(2,2,4)
hist(position_ER60(seuil_ER60==-55,3))
title('Depth distribution of acoustic shoals, ER60 -55dB')
h = findobj(gca,'Type','patch');
set(h,'FaceColor','r','EdgeColor','w')

saveas(gcf,[chemin_plots,sprintf('zshl_hist')],'fig')
saveas(gcf,[chemin_plots,sprintf('zshl_hist')],'jpg')

%All shoals on same graph

plot(time_ME70_60,-position_ME70_60(:,3),'o')
datetick('x',15,'keepticks')
hold on
plot(time_ME70_55,-position_ME70_55(:,3),'or')
datetick('x',15,'keepticks')
legend('ME70 shoals, -60dB threshold','ME70 shoals, -55dB threshold','Location','SouthEast')
xlabel('Time')
ylabel('Depth (m)')

saveas(gcf,[chemin_plots,sprintf('z~t_shl_ME70')],'fig')
saveas(gcf,[chemin_plots,sprintf('z~t_shl_ME70')],'jpg')

plot(time_ER60_60,-position_ER60_60(:,3),'s')
datetick('x',15,'keepticks')
hold on
plot(time_ER60_55,-position_ER60_55(:,3),'sr')
datetick('x',15,'keepticks')
legend('ER60 shoals, -60dB threshold','ER60 shoals, -55dB threshold')
xlabel('Time')
ylabel('Depth (m)')

saveas(gcf,[chemin_plots,sprintf('z~t_shl_ER60')],'fig')
saveas(gcf,[chemin_plots,sprintf('z~t_shl_ER60')],'jpg')

plot(time_ME70_60,-position_ME70_60(:,3),'o')
datetick('x',15,'keepticks')
hold on
plot(time_ER60_60,-position_ER60_60(:,3),'sr')
datetick('x',15,'keepticks')
legend('ME70 shoals, -60dB threshold','ER60 shoals, -60dB threshold')
xlabel('Time')
ylabel('Depth (m)')

saveas(gcf,[chemin_plots,sprintf('z~t_shl_ME70ER60-60dB')],'fig')
saveas(gcf,[chemin_plots,sprintf('z~t_shl_ME70ER60-60dB')],'jpg')

plot(time_ME70_55,-position_ME70_55(:,3),'o')
datetick('x',15,'keepticks')
hold on
plot(time_ER60_55,-position_ER60_55(:,3),'sr')
datetick('x',15,'keepticks')
legend('ME70 shoals, -55dB threshold','ER60 shoals, -55dB threshold')
xlabel('Time')
ylabel('Depth (m)')

saveas(gcf,[chemin_plots,sprintf('z~t_shl_ME70ER60-55dB')],'fig')
saveas(gcf,[chemin_plots,sprintf('z~t_shl_ME70ER60-55dB')],'jpg')

%% shoal typology

size(squeeze(length_ME70(:,seuil_ME70==-60)))

X=cat(1,squeeze(length_ME70(:,seuil_ME70==-60)),width_ME70(:,seuil_ME70==-60),height_ME70(:,seuil_ME70==-60),volume_ME70(:,seuil_ME70==-60),MVBS_ME70(:,seuil_ME70==-60),sigma_ag_ME70(:,seuil_ME70==-60))';
X=cat(2,X,position_ME70(seuil_ME70==-60,3));

nvar=strvcat('length','width','height','volume','MVBS','sigma_ag','depth')

boxplot(X,'orientation','horizontal','labels',nvar)

Xstd=(X-repmat(mean(X,1),size(X,1),1))./repmat(std(X,1),size(X,1),1);

boxplot(Xstd,'orientation','horizontal','labels',nvar)

%PCA on shoal descriptors

[coefs,scores,variances,t2] = princomp(Xstd);

percent_explained = 100*variances/sum(variances)
pareto(percent_explained)
xlabel('Principal Component')
ylabel('Variance Explained (%)')

plot(scores(:,1),scores(:,2),'+')
xlabel('1st Principal Component')
ylabel('2nd Principal Component')

plot(time_ME70_60,-position_ME70_60(:,3),'o')
datetick('x',15,'keepticks')
hold on
plot(time_ME70_60(:,scores(:,2)>20),-position_ME70_60(scores(:,2)>20,3),'or')

tmax=datenum('29-May-2009 17:30:00');
tmin=datenum('29-May-2009 09:00:00');

datevec(min(time_ME70_60))
datestr(datevec(min(time_ME70_60)))

%names=num2str([1:size(X,1)]);
%gname(names)

biplot(coefs(:,1:2), 'scores',scores(:,1:2),... 
'varlabels',nvar);

Xpc=scores(:,1:2);

% Remove offshore areas

Xs=X(time_ME70_60<tmax & time_ME70_60>tmin,:);

sel=time_ME70_60<tmax & time_ME70_60>tmin;

plot(time_ME70_60(:,time_ME70_60<tmax & time_ME70_60>tmin),-position_ME70_60(time_ME70_60<tmax & time_ME70_60>tmin,3),'o')
datetick('x',15,'keepticks')

time_ME70_60s=time_ME70_60(:,sel);
position_ME70_60s=position_ME70_60(sel,:);

Xsstd=(Xs-repmat(mean(Xs,1),size(Xs,1),1))./repmat(std(Xs,1),size(Xs,1),1);

boxplot(Xsstd,'orientation','horizontal','labels',nvar)

%PCA on shoal descriptors

[coefs,scores,variances,t2] = princomp(Xsstd);

percent_explained = 100*variances/sum(variances)
pareto(percent_explained)
xlabel('Principal Component')
ylabel('Variance Explained (%)')

plot(scores(:,1),scores(:,2),'+')
xlabel('1st Principal Component')
ylabel('2nd Principal Component')

%names=num2str([1:size(Xs,1)]);
%gname(names)

datestr(datevec(time_ME70_60s(:,86192)))

plot(time_ME70_60s,-position_ME70_60s(:,3),'o')
datetick('x',15,'keepticks')
hold on
plot(time_ME70_60s(:,scores(:,2)<-50),-position_ME70_60s(scores(:,2)<-50,3),'or')

biplot(coefs(:,1:2), 'scores',scores(:,1:2),'varlabels',nvar);

%surface layer
sel2=position_ME70_60s(:,3)<60;
tabulate(sel2)
size(time_ME70_60s2)
Xs2=Xs(sel2,:);

time_ME70_60s2=time_ME70_60s(:,sel2);
position_ME70_60s2=position_ME70_60s(sel2,:);

hist(position_ME70_60s2(:,3))

plot(time_ME70_60s2,-position_ME70_60s2(:,3),'o')
datetick('x',15,'keepticks')

Xs2std=(Xs2-repmat(mean(Xs2,1),size(Xs2,1),1))./repmat(std(Xs2,1),size(Xs2,1),1);

boxplot(Xs2std,'orientation','horizontal','labels',nvar)

%PCA on shoal descriptors

[coefs2,scores2,variances2,t2] = princomp(Xs2std);

percent_explained2 = 100*variances2/sum(variances2)
pareto(percent_explained2)
xlabel('Principal Component')
ylabel('Variance Explained (%)')

plot(scores2(:,1),scores2(:,2),'+')
xlabel('1st Principal Component')
ylabel('2nd Principal Component')

biplot(coefs2(:,1:2), 'scores',scores2(:,1:2),'varlabels',nvar);

size(scores2)

Xpc=scores2(:,1:5);

IDX=kmeans(Xpc,2);
size(IDX)
size(time_ME70_60s2)

plot(time_ME70_60s2,-position_ME70_60s2(:,3),'o')
datetick('x',15,'keepticks')
hold on
%plot(time_ME70_60s2(:,IDX==2),-position_ME70_60s2(IDX==2,3),'or')
%datetick('x',15,'keepticks')

saveas(gcf,[chemin_plots,sprintf('z~t2_SURFshl_ME70ER60-60dB')],'fig')
saveas(gcf,[chemin_plots,sprintf('z~t2_SURFshl_ME70ER60-60dB')],'jpg')

%new selection

tmin2=datenum('29-May-2009 12:17:00');

sel3=(position_ME70_60s2(:,3)<33)'&time_ME70_60s2 > tmin2;

tabulate(sel3)
size(time_ME70_60s2)
Xs3=Xs(sel3,:);

time_ME70_60s3=time_ME70_60s2(:,sel3);
position_ME70_60s3=position_ME70_60s2(sel3,:);

hist(position_ME70_60s3(:,3))

plot(time_ME70_60s3,-position_ME70_60s3(:,3),'o')
datetick('x',15,'keepticks')

Xs3std=(Xs3-repmat(mean(Xs3,1),size(Xs3,1),1))./repmat(std(Xs3,1),size(Xs3,1),1);

%PCA on shoal descriptors

[coefs3,scores3,variances3,t2] = princomp(Xs3std);

percent_explained3 = 100*variances3/sum(variances3)
pareto(percent_explained3)
xlabel('Principal Component')
ylabel('Variance Explained (%)')

plot(scores3(:,1),scores3(:,2),'+')
xlabel('1st Principal Component')
ylabel('2nd Principal Component')

%names=num2str([1:size(Xs3,1)]);
%gname(names)
tr=time_ME70_60s3(:,376);
datestr(datevec(time_ME70_60s3(:,376)))
position_ME70_60s3(376,3)
biplot(coefs3(:,1:2), 'scores',scores3(:,1:2),'varlabels',nvar);


time_ME70_60s4=time_ME70_60s3(:,time_ME70_60s3~=tr);
position_ME70_60s4=position_ME70_60s3(time_ME70_60s3~=tr,:);

Xs4=Xs3(time_ME70_60s3~=tr,:);

Xs4std=(Xs4-repmat(mean(Xs4,1),size(Xs4,1),1))./repmat(std(Xs4,1),size(Xs4,1),1);

%PCA on shoal descriptors

[coefs4,scores4,variances4,t2] = princomp(Xs4std);

percent_explained4 = 100*variances4/sum(variances4)
pareto(percent_explained4)
xlabel('Principal Component')
ylabel('Variance Explained (%)')

plot(scores4(:,1),scores4(:,2),'+')
xlabel('1st Principal Component')
ylabel('2nd Principal Component')

biplot(coefs4(:,1:2), 'scores',scores4(:,1:2),'varlabels',nvar);

Xpc=scores4(:,1:4);

IDX=kmeans(Xpc,2);
size(IDX)
size(time_ME70_60s2)

plot(time_ME70_60s4,-position_ME70_60s4(:,3),'o')
datetick('x',15,'keepticks')
hold on
plot(time_ME70_60s4(:,IDX==2),-position_ME70_60s4(IDX==2,3),'or')
datetick('x',15,'keepticks')

plot(scores4(:,1),scores4(:,2),'+')
xlabel('1st Principal Component')
ylabel('2nd Principal Component')
hold on
plot(scores4(IDX==2,1),scores4(IDX==2,2),'+r')

%N=412

tmax=datenum('29-May-2009 17:30:00');
tmin=datenum('29-May-2009 12:17:00');

seltotME70_55=time_ME70_55<tmax & time_ME70_55>tmin & (position_ME70_55(:,3)<33)';
seltotME70_60=time_ME70_60<tmax & time_ME70_60>tmin & (position_ME70_60(:,3)<33)';

size(time_ME70_55(:,seltotME70_55))
size(time_ME70_60(:,seltotME70_60))
size(time_ER60_60(:,seltotER60_60))

time_ME70_60sf=time_ME70_60(:,seltotME70_60);
position_ME70_60sf=position_ME70_60(seltotME70_60,:);
time_ME70_55sf=time_ME70_55(:,seltotME70_55);
position_ME70_55sf=position_ME70_55(seltotME70_55,:);
time_ER60_60sf=time_ER60_60(:,seltotER60_60);
position_ER60_60sf=position_ER60_60(seltotER60_60,:);

subplot(2,1,1)
plot(time_ME70_60,-position_ME70_60(:,3),'o')
datetick('x',15,'keepticks')
hold on
plot(time_ME70_60sf,-position_ME70_60sf(:,3)','+r')
datetick('x',15,'keepticks')
xlabel='Time'
ylabel='Depth (m)'
legend('All shoals','Selected shoals','Location','SouthEast')
title('ME70 shoals, -60dB threshold')
subplot(2,1,2)
plot(time_ME70_55,-position_ME70_55(:,3),'o')
datetick('x',15,'keepticks')
hold on
plot(time_ME70_55sf,-position_ME70_55sf(:,3)','+r')
datetick('x',15,'keepticks')
xlabel='Time'
ylabel='Depth (m)'
legend('All shoals','Selected shoals','Location','SouthEast')
title('ME70 shoals, -55dB threshold')

subplot(2,1,1)
plot(time_ME70_60,-position_ME70_60(:,3),'o')
datetick('x',15,'keepticks')
hold on
plot(time_ME70_60sf,-position_ME70_60sf(:,3)','+r')
datetick('x',15,'keepticks')
xlabel='Time'
ylabel='Depth (m)'
legend('All shoals','Selected shoals','Location','SouthEast')
title('ME70 shoals, -60dB threshold')
subplot(2,1,2)
plot(time_ER60_60,-position_ER60_60(:,3),'o')
datetick('x',15,'keepticks')
hold on
plot(time_ER60_60sf,-position_ER60_60sf(:,3)','+r')
datetick('x',15,'keepticks')
xlabel='Time'
ylabel='Depth (m)'
legend('All shoals','Selected shoals','Location','SouthEast')
title('ER60 shoals, -60dB threshold')




%ER60

Y=cat(1,squeeze(length_ER60(:,seuil_ER60==-60)),width_ER60(:,seuil_ER60==-60),height_ER60(:,seuil_ER60==-60),volume_ER60(:,seuil_ER60==-60),MVBS_ER60(:,seuil_ER60==-60),sigma_ag_ER60(:,seuil_ER60==-60))';
Y=cat(2,Y,position_ER60(seuil_ER60==-60,3));



seltotER60_60=time_ER60_60<tmax & time_ER60_60>tmin & (position_ER60_60(:,3)<33)' & (position_ER60_60(:,3)>7)';

Ys=Y(seltot,:);

time_ER60_60s=time_ER60_60(:,seltot);
position_ER60_60s=position_ER60_60(seltot,:);

size(time_ER60_60s)
size(position_ER60_60s)

plot(time_ER60_60s,-position_ER60_60(:,3),'o')
datetick('x',15,'keepticks')

Ysstd=(Ys-repmat(mean(Ys,1),size(Ys,1),1))./repmat(std(Ys,1),size(Ys,1),1);

boxplot(Ysstd,'orientation','horizontal','labels',nvar)

%PCA on shoal descriptors

[coefsy,scoresy,variancesy,t2] = princomp(Ysstd);

percent_explainedy = 100*variancesy/sum(variancesy)
pareto(percent_explainedy)
xlabel('Principal Component')
ylabel('Variance Explained (%)')

plot(scoresy(:,1),scoresy(:,2),'+')
xlabel('1st Principal Component')
ylabel('2nd Principal Component')

biplot(coefsy(:,1:2), 'scores',scoresy(:,1:2),'varlabels',nvar);



%% Threshold sensitivity analysis
seuil_sigma_ag=0;
seuil_volume = 0;

debut=datenum('29/05/2009 07:59');
fin=datenum('29/05/2009 20:00');

debut=min(time_ME70)-1;
fin=max(time_ME70)+1;

%% Quantile plots

seuil=[-65 -60 -55]
nb_seuil=3

tabulate((time_ME70)>debut)
tabulate((time_ME70)<fin)

seuil_volume=0

sel=(time_ME70)>debut & (time_ME70)<fin  & volume_ME70>seuil_volume;
tabulate(sel)

chemin='G:\PELGAS09\HAC-Hermes\RUN034\day\'

filedir = dirr([chemin,'/*school.mat']);  % ensemble des fichiers
nb_seuil = size(filedir,1);  % nombre de seuils

chemin_save=[chemin,'shlmat\']
path=[chemin_save,'shl-556065.mat']

load (path)

chemin_plots=[chemin,'results\']
   if ~exist(chemin_plots,'dir')
        mkdir(chemin_plots)
   end


index= find(sel);

% Shoal length
%----------------------
   
for i=1:nb_seuil
    percentilelength(i,:)=prctile(length_ME70(find(seuil_ME70(index)==seuil(i))),[10 50 90]);
    occurence(i)=size(length_ME70(find(seuil_ME70(index)==seuil(i))),2);
end

figure;
[AX,H1,H2] = plotyy(seuil,percentilelength,seuil,occurence);
set(AX(1),'YColor','r') 
set(AX(2),'YColor','k') 
set(get(AX(1),'Ylabel'),'String','Length (m)','Color','r') 
set(get(AX(2),'Ylabel'),'String','Number','Color','k')
xlabel('Threshold (dB)');
set(H1([1,3]),'LineStyle','--','LineWidth',3,'Color','r')
set(H1(2),'LineWidth',3,'Color','r')
set(H2,'LineStyle',':','LineWidth',3,'Color','k')

legend('Number of shoals','90% percentile', '50% percentile', '10% percentile');
title('Length distribution and number of acoustic shoals at various thresholds')

saveas(gcf,[chemin_plots,sprintf('Length')],'fig')
saveas(gcf,[chemin_plots,sprintf('Length')],'jpg')

% Shoal width
%----------------------

for i=1:nb_seuil
    percentileWidth(i,:)=prctile(width_ME70(find(seuil_ME70(index)==seuil(i))),[10 50 90]);
    occurence(i)=size(width_ME70(find(seuil_ME70(index)==seuil(i))),2);
end

figure;
[AX,H1,H2] = plotyy(seuil,percentileWidth,seuil,occurence);
set(AX(1),'YColor','r') 
set(AX(2),'YColor','k') 
set(get(AX(1),'Ylabel'),'String','Width (m)','Color','r') 
set(get(AX(2),'Ylabel'),'String','Number','Color','k')
xlabel('Threshold (dB)');
set(H1([1,3]),'LineStyle','--','LineWidth',3,'Color','r')
set(H1(2),'LineWidth',3,'Color','r')
set(H2,'LineStyle',':','LineWidth',3,'Color','k')

legend('Number of shoals','90% percentile', '50% percentile', '10% percentile');
title('Width distribution and number of acoustic shoals at various thresholds')

saveas(gcf,[chemin_plots,sprintf('Width')],'fig')
saveas(gcf,[chemin_plots,sprintf('Width')],'jpg')

% Shoal height
%----------------------

for i=1:nb_seuil
    percentileHeight(i,:)=prctile(height_ME70(find(seuil_ME70(index)==seuil(i))),[10 50 90]);
    occurence(i)=size(height_ME70(find(seuil_ME70(index)==seuil(i))),2);
end

figure;
[AX,H1,H2] = plotyy(seuil,percentileHeight,seuil,occurence);
set(AX(1),'YColor','r') 
set(AX(2),'YColor','k') 
set(get(AX(1),'Ylabel'),'String','Height (m)','Color','r') 
set(get(AX(2),'Ylabel'),'String','Number','Color','k')
xlabel('Threshold (dB)');
set(H1([1,3]),'LineStyle','--','LineWidth',3,'Color','r')
set(H1(2),'LineWidth',3,'Color','r')
set(H2,'LineStyle',':','LineWidth',3,'Color','k')

legend('Number of shoals','90% percentile', '50% percentile', '10% percentile');
title('Height distribution and number of acoustic shoals at various thresholds')

saveas(gcf,[chemin,sprintf('Height')],'fig')
saveas(gcf,[chemin,sprintf('Height')],'jpg')

% Shoal volume
%----------------------

for i=1:nb_seuil
    percentileVolume(i,:)=prctile(volume_ME70(find(seuil_ME70(index)==seuil(i))),[25 50 75]);
    occurence(i)=size(volume_ME70(find(seuil_ME70(index)==seuil(i))),2);
end

figure;
[AX,H1,H2] = plotyy(seuil,percentileVolume,seuil,occurence);
set(AX(1),'YColor','r') 
set(AX(2),'YColor','k') 
set(get(AX(1),'Ylabel'),'String','Volume (m3)','Color','r') 
set(get(AX(2),'Ylabel'),'String','Number','Color','k')
xlabel('Threshold (dB)');
set(H1([1,3]),'LineStyle','--','LineWidth',3,'Color','r')
set(H1(2),'LineWidth',3,'Color','r')
set(H2,'LineStyle',':','LineWidth',3,'Color','k')

legend('Number of shoals','90% percentile', '50% percentile', '10% percentile');
title('Height distribution and number of acoustic shoals at various thresholds')

saveas(gcf,[chemin,sprintf('Volume')],'fig')
saveas(gcf,[chemin,sprintf('Volume')],'jpg')

% Aspect ratio

for i=1:nb_seuil
    percentileAspect(i,:)=prctile(volume_ME70(find(seuil_ME70(index)==seuil(i)))./(height_ME70(find(seuil_ME70(index)==seuil(i))).*width_ME70(find(seuil_ME70(index)==seuil(i))).*length_ME70(find(seuil_ME70(index)==seuil(i)))),[10 50 90]);
    occurence(i)=size(length_ME70(find(seuil_ME70(index)==seuil(i))),2);
end

figure;
[AX,H1,H2] = plotyy(seuil,percentileAspect,seuil,occurence);
set(AX(1),'YColor','r') 
set(AX(2),'YColor','k') 
set(get(AX(1),'Ylabel'),'String','Aspect','Color','r') 
set(get(AX(2),'Ylabel'),'String','Number','Color','k')
xlabel('Threshold (dB)');
set(H1([1,3]),'LineStyle','--','LineWidth',3,'Color','r')
set(H1(2),'LineWidth',3,'Color','r')
set(H2,'LineStyle',':','LineWidth',3,'Color','k')

legend('Number of shoals','90% percentile', '50% percentile', '10% percentile');
title('Ratio of shoal volume over bounding box volume ("aspect") distribution and number of acoustic shoals at various threshold')

saveas(gcf,[chemin,sprintf('Aspect')],'fig')
saveas(gcf,[chemin,sprintf('Aspect')],'jpg')

% MVBS ratio

for i=1:nb_seuil
    percentileMVBS(i,:)=prctile(MVBS_ME70(find(seuil_ME70(index)==seuil(i))),[10 50 90]);
    occurence(i)=size(MVBS_ME70(find(seuil_ME70(index)==seuil(i))),2);
end
figure;
[AX,H1,H2] = plotyy(seuil,percentileMVBS,seuil,occurence);
set(AX(1),'YColor','r') 
set(AX(2),'YColor','k') 
set(get(AX(1),'Ylabel'),'String','Acoustic density (MVBS)','Color','r') 
set(get(AX(2),'Ylabel'),'String','Number','Color','k')
xlabel('Threshold (dB)');
set(H1([1,3]),'LineStyle','--','LineWidth',3,'Color','r')
set(H1(2),'LineWidth',3,'Color','r')
set(H2,'LineStyle',':','LineWidth',3,'Color','k')

legend('Number of shoals','90% percentile', '50% percentile', '10% percentile');
title('Acoustic density (MVBS) distribution of acoustic shoals at various threshold')

saveas(gcf,[chemin,sprintf('MVBS')],'fig')
saveas(gcf,[chemin,sprintf('MVBS')],'jpg')

% figure;
% boxplot(length_ME70(index),seuil_ME70(index))
% xlabel('Threshold (dB)');
% ylabel('Length (m)');
% title('Length of school distribution for various threshold')
% saveas(gcf,[chemin,sprintf('Length ')],'fig')
% saveas(gcf,[chemin,sprintf('Length ')],'jpg')
% 
% figure;
% boxplot(width_ME70(index),seuil_ME70(index))
% xlabel('Threshold (dB)');
% ylabel('Width (m)');
% title('Width of school distribution for various threshold')
% saveas(gcf,[chemin,sprintf('Width ')],'fig')
% saveas(gcf,[chemin,sprintf('Width ')],'jpg')
% 
% figure;
% boxplot(height_ME70(index),seuil_ME70(index))
% xlabel('Threshold (dB)');
% ylabel('Height (m)');
% title('Height of school distribution for various threshold')
% saveas(gcf,[chemin,sprintf('Height ')],'fig')
% saveas(gcf,[chemin,sprintf('Height ')],'jpg')
% 
% figure;
% boxplot(volume_ME70(index),seuil_ME70(index))
% xlabel('Threshold (dB)');
% ylabel('Volume (m3)');
% title('Volume of school distribution for various threshold')
% saveas(gcf,[chemin,sprintf('Volume ')],'fig')
% saveas(gcf,[chemin,sprintf('Volume ')],'jpg')
% 
% figure;
% boxplot(height_ME70(index).*width_ME70(index).*length_ME70(index)./volume_ME70(index),seuil_ME70(index))
% xlabel('Threshold (dB)');
% ylabel('Volume ratio (unitless)');
% title('Volume ratio of school distribution relative to bounding box for various threshold')
% saveas(gcf,[chemin,sprintf('Aspect ratio')],'fig')
% saveas(gcf,[chemin,sprintf('Aspect ratio')],'jpg')
% 
% figure;
% boxplot(MVBS_ME70(index),seuil_ME70(index))
% xlabel('Threshold (dB)');
% ylabel('MVBS (dB)');
% title('MVBS of school distribution for various threshold')
% saveas(gcf,[chemin,sprintf('MVBS ')],'fig')
% saveas(gcf,[chemin,sprintf('MVBS ')],'jpg')


D=zeros(size(position_ME70(index),2),21);
for i=1:size(position_ME70(index),2)
    k=1;
    for j=max(1,i-10):min(i+10,size(position_ME70(index),2))
        D(i,k)=distance3D(position_ME70(index(i),:),position_ME70(index(j),:));
        k=k+1;
    end
end

for i=1:size(position_ME70(index),2)
    Dmin(i)=min(D(i,find(D(i,:)>0)));
end

for i=1:nb_seuil
    percentileDistance(i,:)=prctile(Dmin(find(seuil_ME70(index)==seuil(i))),[25 50 75]);
    occurence(i)=size(Dmin(find(seuil_ME70(index)==seuil(i))),2);
end
figure;
[AX,H1,H2] = plotyy(seuil,percentileDistance,seuil,occurence);
set(get(AX(1),'Ylabel'),'String','Distance (m)') 
set(get(AX(2),'Ylabel'),'String','Occurence')
xlabel('Threshold (dB)');

legend('Occurence','10% percentile', '50% percentile', '90% percentile');
title('Distance distribution of schools various threshold')
saveas(gcf,[chemin,sprintf('Distance')],'fig')
saveas(gcf,[chemin,sprintf('Distance')],'jpg')

% figure;
% boxplot(Dmin,seuil_ME70(index))
% xlabel('Threshold (dB)');
% ylabel('Distance (dB)');
% title('Distance between schools for various threshold')
% saveas(gcf,[chemin,sprintf('Distance ')],'fig')
% saveas(gcf,[chemin,sprintf('Distance ')],'jpg')


volume_ratio=zeros(nb_seuil,1);
for i=1:nb_seuil
    volume_ratio(i)=sum(volume_ME70(seuil_ME70(index)==seuil(i)))/sum(volume_ME70(seuil_ME70(index)==min(seuil)));
end

figure;
plot(seuil,volume_ratio);
xlabel('Threshold (dB)');
ylabel('Volume ratio (unitless)');
title('Volume ratio relative to lowest threshold')

saveas(gcf,[chemin,sprintf('Volume ratio')],'fig')
saveas(gcf,[chemin,sprintf('Volume ratio')],'jpg')

sigma_ag_ratio=zeros(nb_seuil,1);
for i=1:nb_seuil
    sigma_ag_ratio(i)=sum(sigma_ag_ME70(seuil_ME70(index)==seuil(i)))/sum(sigma_ag_ME70(seuil_ME70(index)==min(seuil)));
end

figure;
plot(seuil,sigma_ag_ratio);
xlabel('Threshold (dB)');
ylabel('Backscatter ratio (m)');
title('Backscatter ratio relative to lowest threshold')
saveas(gcf,[chemin,sprintf('Backscatter ratio ')],'fig')
saveas(gcf,[chemin,sprintf('Backscatter ratio ')],'jpg')




% for i=1:nb_seuil
% 
%     figure
%     scatter3(position_ME70(find(seuil_ME70()==seuil(i)),1),position_ME70(find(seuil_ME70()==seuil(i)),2),-position_ME70(find(seuil_ME70()==seuil(i)),3),sqrt(length_ME70(find(seuil_ME70()==seuil(i))).*width_ME70(find(seuil_ME70()==seuil(i)))),MVBS_ME70(find(seuil_ME70()==seuil(i))));
%     hold on;
% %     scatter3(reshape(Latitude(:,indexNav(1:10:end)),1,[]),reshape(Longitude(:,indexNav(1:10:end)),1,[]),-reshape(Depth(:,indexNav(1:10:end)),1,[]),'k');
%     xlabel('Latitude (�)');
%     ylabel('Longitude (�)');
%     ylabel('Depth (m)');
%     title(['Spatial distribution of schools at ',num2str(seuil(i)),'dB threshold, size represents length color represents density'])
%     colorbar;
%     axis([min(position_ME70(:,1)) max(position_ME70(:,1)) min(position_ME70(:,2)) max(position_ME70(:,2)) min(-position_ME70(:,3)) max(-position_ME70(:,3)) min(MVBS_ME70(:)) max(MVBS_ME70(:))]);
%     view(-40,20);
%     saveas(gcf,[chemin,'r�partition spatiale seuil ',num2str(seuil(i)),'dB'],'fig')
%     saveas(gcf,[chemin,'r�partition spatiale seuil ',num2str(seuil(i)),'dB'],'jpg')
%     
% 
% end

for i=1:nb_seuil

    figure
    scatter(position_ME70(find(seuil_ME70()==seuil(i)),1),position_ME70(find(seuil_ME70()==seuil(i)),2),sqrt(length_ME70(find(seuil_ME70()==seuil(i))).*width_ME70(find(seuil_ME70()==seuil(i)))),height_ME70(find(seuil_ME70()==seuil(i))));
    hold on;
%     scatter3(reshape(Latitude(:,indexNav(1:10:end)),1,[]),reshape(Longitude(:,indexNav(1:10:end)),1,[]),-reshape(Depth(:,indexNav(1:10:end)),1,[]),'k');
    xlabel('Latitude (�)');
    ylabel('Longitude (�)');

    title(['Spatial distribution of schools (vertical projection) at ',num2str(seuil(i)),'dB threshold, size represents length, color represents density'])
    colorbar;
    axis([min(position_ME70(:,1)) max(position_ME70(:,1)) min(position_ME70(:,2)) max(position_ME70(:,2))]);
    caxis( [min(height_ME70(:)) max(height_ME70(:))]);
%     view(-40,20);
    saveas(gcf,[chemin,'r�partition spatiale seuil ',num2str(seuil(i)),'dB'],'fig')
    saveas(gcf,[chemin,'r�partition spatiale seuil ',num2str(seuil(i)),'dB'],'jpg')
    

end

% figure
% for i=1:nb_seuil
% 
%     %             nbpoints=size(schools_ME70(1,i).contours,2)
%     %             for j=1:nbpoints
%     %                 lat(j)=schools_ME70(1,i).contours(j).m_latitude;
%     %                 long(j)=schools_ME70(1,i).contours(j).m_longitude;
%     %                 depth(j)=schools_ME70(1,i).contours(j).m_depth;
%     %             end
%     %
%     %             if nbpoints>50
%     %
%     %                 scatter3(lat,long,depth,10,nblME70*ones(1,nbpoints));
%     %             end
%     %             clear lat;
%     %             clear long
%     %             clear depth;
% end


