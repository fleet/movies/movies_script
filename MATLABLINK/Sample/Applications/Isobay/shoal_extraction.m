close all
clear all

%% EI shoal

chemin_ini='G:\PELGAS09\HAC-Hermes\RUN034\day\selection\'

%EI_thr=[-61 -59];
EI_thr=[-65 -60 -55];

%ithri=4

for ithri=1:size(EI_thr,2)
    
    %
    pathi=[chemin_ini,'EIshl',num2str(EI_thr(ithri)),'dB\']
    %pathi=[chemin_save,'shoals',num2str(EI_thr(ithri)),'\']
    
    if ~exist(pathi,'dir')
        mkdir(pathi)
    end
%
    SampleShoalExtraction(chemin_ini,pathi);
%
clear functions;

end;

%% Import EI shoal results

%chemin='/export/home/Mathieu/Projects/BoB/Isobay/proc/'
chemin='G:/PELGAS09/HAC-Hermes/RUN035/day/'

filedir = dirr([chemin,'\*school.mat']);  % ensemble des fichiers
nb_seuil = size(filedir,1);  % nombre de seuils

% figure;
% hold on;
nblME70=0;
nblER60=0;

for numdir = 1:nb_seuil

    %seuil(numdir)=-str2num(filedir(numdir).name(size(filedir(numdir).name,2)-2:size(filedir(numdir).name,2)));
    seuil(numdir)=str2num(filedir(numdir).name(6:8))
    
    %filelist=ls([chemin,filedir(numdir).name,'/*school.mat']);
    %nb_files = size(filelist,1);

    filelist = squeeze(dir(fullfile([chemin,filedir(numdir).name],'*school.mat')));  % all files
    nb_files = size(filelist,1);  % number of files
    
    for numfile = 1:nb_files

        %matfilename = filelist(numfile,:);
        matfilename = filelist(numfile,:).name
        
        year = str2num(matfilename(14:17));
        month =  str2num(matfilename(18:19));
        day = str2num(matfilename(20:21));
        hour = str2num(matfilename(23:24));
        mn = str2num(matfilename(25:26));

%         datefile=datenum(year, month, day, hour, mn, 0);
%         if (datefile>=debut && datefile<=fin)

        FileName = [chemin,filedir(numdir).name,'/',matfilename(1:findstr('.',matfilename)-1),'.mat'];
        load(FileName);

            if  size(schools_ER60,2)>0

                for i=1:size(schools_ER60,2)
                    time_ER60(nblER60+i) = schools_ER60(1,i).time;
                    length_ER60(nblER60+i) = schools_ER60(1,i).length;
                    width_ER60(nblER60+i) = schools_ER60(1,i).width;
                    height_ER60(nblER60+i) = schools_ER60(1,i).height;
                    volume_ER60(nblER60+i) = schools_ER60(1,i).volume;
                    MVBS_ER60(nblER60+i) = schools_ER60(1,i).MVBS_weighted;
                    sigma_ag_ER60(nblER60+i) = schools_ER60(1,i).sigma_ag;
                    position_ER60(nblER60+i,:) = schools_ER60(1,i).position;
                    seuil_ER60(nblER60+i,:) = seuil(numdir);

                end


                nblER60=nblER60+size(schools_ER60,2);
            end

            if  size(schools_ME70,1)>0
                for i=1:size(schools_ME70,2)
                    time_ME70(nblME70+i) = schools_ME70(1,i).time;
                    length_ME70(nblME70+i) = schools_ME70(1,i).length;
                    width_ME70(nblME70+i) = schools_ME70(1,i).width;
                    height_ME70(nblME70+i) = schools_ME70(1,i).height;
                    volume_ME70(nblME70+i) = schools_ME70(1,i).volume;
                    MVBS_ME70(nblME70+i) = schools_ME70(1,i).MVBS_weighted;
                    sigma_ag_ME70(nblME70+i) = schools_ME70(1,i).sigma_ag;
                    position_ME70(nblME70+i,:) = schools_ME70(1,i).position;
                    seuil_ME70(nblME70+i,:) = seuil(numdir);

                end

                nblME70=nblME70+size(schools_ME70,2);
            end

            clear  schools_ER60  schools_ME70;

%         end

    end


end

% load([chemin,'Navigation.mat'])

% timenum=zeros(size(Time,1),1);
% for i=1:size(Time,1)
%     timenum(i)=datenum(Time(i,:),'dd/mm/yyyy HH:MM:SS.FFF');
% end
% indexNav = find(timenum>debut & timenum<fin);
% 
% MaxX=max( reshape(Longitude(:,indexNav),1,[]));
% MinX=min( reshape(Longitude(:,indexNav),1,[]));
% 
% MaxY=max(reshape(Latitude(:,indexNav),1,[]));
% MinY=min(reshape(Latitude(:,indexNav),1,[]));
% 
% nbPoints = 1000;
% Precision = max((MaxX-MinX)/nbPoints,(MaxY-MinY)/nbPoints)
% XI=MinX:Precision:MaxX;
% YI=MinY:Precision:MaxY;
%     figure;
% ZI = griddata( reshape(Longitude(:,indexNav),1,[]),reshape(Latitude(:,indexNav),1,[]),-reshape(Depth(:,indexNav),1,[]),XI,YI');
% hold on;
% surf(XI,YI,ZI,'LineStyle','none');
% view(0,70);
% light
% lighting gouraud

chemin_save=[chemin,'shlmat/']

   if ~exist(chemin_save,'dir')
        mkdir(chemin_save)
    end

save([chemin_save,'shl-5559606165.mat'],'MVBS_ER60','MVBS_ME70','height_ER60','height_ME70','length_ER60','length_ME70','position_ER60','position_ME70','seuil_ER60','seuil_ME70','sigma_ag_ER60','sigma_ag_ME70','time_ER60','time_ME70','volume_ER60','volume_ME70','width_ER60','width_ME70')


