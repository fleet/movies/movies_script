
%% Clear everything
clear all

%% Define paths

% path to the concatenated EI matlab files
% file name must be : SURVEYNAME-RUN000-TH-00-ER60-EIlay.mat
path_mtx = 'C:\Documents and Settings\bremond\Mes documents\test_data\CLASS08\result_EI_totCLASS\conf_PELGAS';

path_mtx_reverse = strrep(path_mtx,'\','/');
filelist = ls([path_mtx_reverse,'/*.mat']);             % all matlab files
nb_file = size(filelist,1);                             % number of files

% path to saving folder
path_save = [path_mtx_reverse,'/result_sum/'];
if ~exist(path_save,'dir')
    mkdir(path_save)
end

% Set path for standardization results
path_save_std = [path_save,'res_std'];
if ~exist(path_save_std,'dir')
    mkdir(path_save_std)
end

% set path for PCA results
path_save_PCA = [path_save,'res_PCA'];
if ~exist(path_save_PCA,'dir')
    mkdir(path_save_PCA)
end

% set path for kmean-clustering results
path_save_kmean = [path_save,'res_kmean'];
if ~exist(path_save_kmean,'dir')
    mkdir(path_save_kmean)
end

% set path for HAC-clustering results
path_save_HAC = [path_save,'res_HAC'];
if ~exist(path_save_HAC,'dir')
    mkdir(path_save_HAC)
end

% set path for maps
path_save_map = [path_save,'map'];
if ~exist(path_save_map,'dir')
    mkdir(path_save_map)
end

%% Define data

survey_name = 'CLASS08';
run = [5];
threshold = [-90:5:-60];

%% Loop to sum the depth layers

for ir = 1:length(run)
    for t = 1:size(threshold,2)
        
        % Build dedicated string
        str_run(ir,:)='000';
        str_run(ir,end-length(num2str(run(ir)))+1:end)=num2str(run(ir));
                 
        % load ER60 data
        load([path_mtx,'/',survey_name,'-RUN',str_run(ir,:),'-TH',num2str(threshold(t)),'-ER60-EIlay.mat']);
            
        % sizes
            size_bot = size(Sa_botER60_db);
            size_surf = size(Sa_surfER60_db);
            nb_freq = size_bot(3);
            nb_ping = size_bot(1);
            l_bot = size_bot(2);
            l_surf = size_surf(2);
            
            % Initial matrices
            Sa_botER60_db_sum = [];
            Sa_surfER60_db_sum = [];
            Sv_botER60_db_sum = [];
            Sv_surfER60_db_sum = [];
            depth_bottom_ER60_db_sum = [];
            depth_surface_ER60_db_sum = [];
            lat_botER60_db_sum = [];
            lat_surfER60_db_sum = [];
            lon_botER60_db_sum = [];
            lon_surfER60_db_sum = [];
            vol_surfER60_db_sum = [];
                     
            for c = 1:nb_ping
                % initial matrices
                Sa_botER60_db_sum_d=[];
                Sa_surfER60_db_sum_d = [];
                Sv_botER60_db_sum_d = [];
                Sv_surfER60_db_sum_d = [];
                depth_bottom_ER60_db_sum_d = [];
                depth_surface_ER60_db_sum_d = [];
                lat_botER60_db_sum_d = [];
                lat_surfER60_db_sum_d = [];
                lon_botER60_db_sum_d = [];
                lon_surfER60_db_sum_d = [];
                vol_surfER60_db_sum_d = [];
                
                for d = 1:nb_freq
            % result per frequency
                    Sa_botER60_db_sum_d = [Sa_botER60_db_sum_d,sum((Sa_botER60_db(c,1,d)):(Sa_botER60_db(c,l_bot,d)))];
                    Sa_surfER60_db_sum_d = [Sa_surfER60_db_sum_d,sum((Sa_surfER60_db(c,1,d)):(Sa_surfER60_db(c,l_bot,d)))];
                    Sv_botER60_db_sum_d = [Sv_botER60_db_sum_d,sum(Sv_botER60_db(c,1,d):Sv_botER60_db(c,l_bot,d))];
                    Sv_surfER60_db_sum_d = [Sv_surfER60_db_sum_d,sum(Sv_surfER60_db(c,1,d):Sv_surfER60_db(c,l_bot,d))];
                    depth_bottom_ER60_db_sum_d = [depth_bottom_ER60_db_sum_d,sum(depth_bottom_ER60_db(c,1,d):depth_bottom_ER60_db(c,l_bot,d))];
                    depth_surface_ER60_db_sum_d = [depth_surface_ER60_db_sum_d,sum(depth_surface_ER60_db(c,1,d):depth_surface_ER60_db(c,l_bot,d))];
                    lat_botER60_db_sum_d = [lat_botER60_db_sum_d,sum(lat_botER60_db(c,1,d):lat_botER60_db(c,l_bot,d))];
                    lat_surfER60_db_sum_d = [lat_surfER60_db_sum_d,sum(lat_surfER60_db(c,1,d):lat_surfER60_db(c,l_bot,d))];
                    lon_botER60_db_sum_d = [lon_botER60_db_sum_d,sum(lon_botER60_db(c,1,d):lon_botER60_db(c,l_bot,d))];
                    lon_surfER60_db_sum_d = [lon_surfER60_db_sum_d,sum(lon_surfER60_db(c,1,d):lon_surfER60_db(c,l_bot,d))];
                    vol_surfER60_db_sum_d = [vol_surfER60_db_sum_d,sum(vol_surfER60_db(c,1,d):vol_surfER60_db(c,l_bot,d))];
                    
                    
                end
            % result per ping (and frequency)
                Sa_botER60_db_sum = [Sa_botER60_db_sum;Sa_botER60_db_sum_d];
                Sa_surfER60_db_sum = [Sa_surfER60_db_sum;Sa_surfER60_db_sum_d];
                Sv_botER60_db_sum = [Sv_botER60_db_sum;Sv_botER60_db_sum_d];
                Sv_surfER60_db_sum = [Sv_surfER60_db_sum;Sv_surfER60_db_sum_d];
                depth_bottom_ER60_db_sum = [depth_bottom_ER60_db_sum;depth_bottom_ER60_db_sum_d];
                depth_surface_ER60_db_sum = [depth_surface_ER60_db_sum;depth_surface_ER60_db_sum_d];
                lat_botER60_db_sum = [lat_botER60_db_sum;lat_botER60_db_sum_d];
                lat_surfER60_db_sum = [lat_surfER60_db_sum;lat_surfER60_db_sum_d];
                lon_botER60_db_sum = [lon_botER60_db_sum;lon_botER60_db_sum_d];
                lon_surfER60_db_sum = [lon_botER60_db_sum;lon_surfER60_db_sum_d];
                vol_surfER60_db_sum = [vol_surfER60_db_sum;vol_surfER60_db_sum_d];
            end

        % Save
save([path_save,'/',survey_name,'-RUN',str_run(ir,:),'-TH',num2str(threshold(t)),'-ER60-EIlay-sum.mat'],'Sa_botER60_db_sum','Sa_botER60_db_sum','Sa_surfER60_db_sum','Sv_botER60_db_sum','Sv_surfER60_db_sum','depth_bottom_ER60_db_sum','depth_surface_ER60_db_sum','lat_botER60_db_sum','lat_surfER60_db_sum','lon_botER60_db_sum','lon_surfER60_db_sum','vol_surfER60_db_sum');
        
    end
end



%% Data standardization
% INPUT : echo-integrated data
% OUPUT : matlab file containing standardized echo-integrated data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% the " S " indice refers to the surface layers.
% the " B " indice refers to the bottom layers.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for t = 1:size(threshold,2)      
     for ir = 1:length(run)

        %%%%%%%%%%%%%%%%%%  SURFACE LAYERS   %%%%%%%%%%%%%%%%%%%%%%%%%
        % Build dedicated string
        str_run(ir,:)='000';
        str_run(ir,end-length(num2str(run(ir)))+1:end)=num2str(run(ir));
        
        % load result file from precedent step (sum)
        load([path_save,survey_name,'-RUN',str_run(ir,:),'-TH',num2str(threshold(t)),'-ER60-EIlay-sum.mat']);
       
        % standardization
        S = Sa_surfER60_db_sum;      
        Sstd = (S-repmat(mean(S,1),size(S,1),1))./repmat(std(S,1),size(S,1),1);    
        
        %%%%%%%%%%%%%%%%%%  BOTTOM LAYERS   %%%%%%%%%%%%%%%%%%%%%%%%%
        
        % Saving standardization for surface layers
        save([path_save_std,'/',survey_name,'-RUN',str_run(ir,:),'-TH',num2str(threshold(t)),'_surf_std'],'Sstd');

     end 
end

%% PCA 
% INPUT : standardized echo-integrated data (for precedent cell)
% OUPUT (for each threshold, each RUN) : 
%   - figure showing the percentage of variance explained by each PC
%   - 3D-biplot showing the points and the variables dispersion in the PC
%   - matlab file containing the percentage of variance explained by each PC
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for t = 1:size(threshold,2)     
     for ir = 1:length(run)
         
        % Build dedicated string
        str_run(ir,:)='000';
        str_run(ir,end-length(num2str(run(ir)))+1:end)=num2str(run(ir));
        
        %%%%%%%%%%%%%%%%%%  SURFACE LAYERS   %%%%%%%%%%%%%%%%%%%%%%%%%
        % load standardized data
        load ([path_save_std,'/',survey_name,'-RUN',str_run(ir,:),'-TH',num2str(threshold(t)),'_surf_std.mat']);
        % Analysis
        [coeffS,scoresS,variancesS,t2S]=princomp(Sstd);    
        
        % Variance percentage explained      
        percent_explained_S = 100*variancesS/sum(variancesS);      

        % graphs
        figure(1)
        pareto(percent_explained_S)
        xlabel('Principal Component')
        ylabel('Variance Explained for SURFACE layers (%)')
        
        % 3-PC biplot
        figure (2)
        nvar = {'18';'38';'70';'120';'200'};
        biplot(coeffS(:,1:3), 'scores',scoresS(:,1:3),'varlabels',nvar);     
        
        % points cordinates in the first 3 PC
        Spc=scoresS(:,1:3);
        
        %%%%%%%%%%%%%%%%%%  BOTTOM LAYERS   %%%%%%%%%%%%%%%%%%%%%%%%%
         
        
        % Saving PCA and scores for surface layers
        save([path_save_PCA,'/',survey_name,'-RUN',str_run(ir,:),'-TH',num2str(threshold(t)),'_surf_PCA'],'percent_explained_S');
        save([path_save_PCA,'/',survey_name,'-RUN',str_run(ir,:),'-TH',num2str(threshold(t)),'_surf_scores'],'Spc');
    
        % Saving the images
        savename1=([path_save_PCA,'/var_explained_',survey_name,'-RUN',str_run(ir,:),'-TH',num2str(threshold(t))]);
        savename2=([path_save_PCA,'/biplot_',survey_name,'-RUN',str_run(ir,:),'-TH',num2str(threshold(t))]);
        saveas(figure(1),savename1,'jpg');
        saveas(figure(2),savename2,'jpg');
        
     end
end
     
%% Kmeans clustering
% INPUT : 
%   - number of clusters wanted
%   - PCA scores (points coordinates in the first 3 PCs)
% OUPUT : 
%   - matlab file containing cluster numbers to which points belong
%   - figure showing the clusters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Number of clusters
n_cluster = 3;

for t = 1:size(threshold,2)     
     for ir = 1:length(run)
         
        % Build dedicated string
        str_run(ir,:)='000';
        str_run(ir,end-length(num2str(run(ir)))+1:end)=num2str(run(ir));
        
        %%%%%%%%%%%%%%%%%%  SURFACE LAYERS   %%%%%%%%%%%%%%%%%%%%%%%%%
        % loading
        load([path_save_PCA,'/',survey_name,'-RUN',str_run(ir,:),'-TH',num2str(threshold(t)),'_surf_scores.mat']);
        
        % analysis
        [IDX_S] = kmeans(Spc,n_cluster,'dist','sqeuclidean','emptyaction','singleton');
        
        % graph
        figure(3)
        silhouette(Spc,IDX_S,'sqeuclidean');
        
        % Saving kmean clustering (for surface layers)
        save([path_save_kmean,'/',survey_name,'-RUN',str_run(ir,:),'-TH',num2str(threshold(t)),'_surf_kmean'],'IDX_S');

        % Saving the figure
        savename3=([path_save_kmean,'/silhouette_',survey_name,'-RUN',str_run(ir,:),'-TH',num2str(threshold(t))]);
        saveas(figure(3),savename3,'jpg');
        
     end    
end
     
%% HAC clustering
% INPUT : 
%   - PCA scores (points coordinates in the first 3 PCs)
%   - number of clusters wanted
% OUTPUT :
%   - dendrogram
%   - 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



% set the number of clusters
% a first run should be made with the value '0' to get an overview of all
% the distances
n_cluster = 0;

for t = 1:size(threshold,2)     
     for ir = 1:length(run)
         
        % Build dedicated string
        str_run(ir,:)='000';
        str_run(ir,end-length(num2str(run(ir)))+1:end)=num2str(run(ir));
        
        %%%%%%%%%%%%%%%%%%  SURFACE LAYERS   %%%%%%%%%%%%%%%%%%%%%%%%%
        % loading
        load([path_save_PCA,'/',survey_name,'-RUN',str_run(ir,:),'-TH',num2str(threshold(t)),'_surf_scores.mat']);
        
        % Analysis
        euclid_dist = pdist(Spc,'euclidean');

        % Hierarchical cluster tree
        clustTreeEuc = linkage(euclid_dist,'average');
        cophenet(clustTreeEuc,euclid_dist)
        
        figure(4)
        [h,nodes] = dendrogram(clustTreeEuc,n_cluster);

        % Saving HAC clustering (surface layers)
        %save([path_save_HAC,'/',survey_name,'-RUN',str_run(ir,:),'-TH',num2str(threshold(t)),'_surf_HAC'],'');

        % saving the figure
        savename4=([path_save_HAC,'/dendrogram_',survey_name,'-RUN',str_run(ir,:),'-TH',num2str(threshold(t))]);
        saveas(figure(4),savename4,'jpg');
        
     end
end


%% Mapping
% INPUT : 
%   - ESU coordinates (lat, lon)
%   - cluster belonging
% OUTPUT :
%   - figures showing the clusters distribution along the RUN track for
%   each threshold
%   - figure showing the clusters distribution for the whole survey track 
%   for each threshold
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%  run = 8
% threshold = -90

% number of clusters selected
n_cluster = 3;

% initial matrix
coord_all = []; 
size_run = [];

%for t = 1:size(threshold,2)
for t = -60    
     for ir = 1:length(run)
         
        % Build dedicated string
        str_run(ir,:)='000';
        str_run(ir,end-length(num2str(run(ir)))+1:end)=num2str(run(ir));

        
                % coordinates and cluster belonging for surface layers
                load([path_mtx,'/',survey_name,'-RUN',str_run(ir,:),'-TH',num2str(threshold(t)),'-ER60-EIlay.mat']);
                load([path_save_kmean,'/',survey_name,'-RUN',str_run(ir,:),'-TH',num2str(threshold(t)),'_surf_kmean.mat']);


                lat_S = lat_surfER60_db(:,1,1);
                lon_S = lon_surfER60_db(:,1,1);
                coord_cluster_S = [lat_S,lon_S,IDX_S(:,1)];

                  %  for n= 1:n_cluster
                  
                % line rak of each cluster (surface layers)
                i_1 = find(coord_cluster_S(:,3)==1);
                i_2 = find(coord_cluster_S(:,3)==2);
                i_3 = find(coord_cluster_S(:,3)==3);
                
                % coordinates for points of each cluster (surface layers)
                coord_clust1 = coord_cluster_S(i_1,:);
                coord_clust2 = coord_cluster_S(i_2,:);
                coord_clust3 = coord_cluster_S(i_3,:);
                
               

                
                % cluster distribution for 1 run (per threshold)
                figure(5)
                plot(coord_clust1(:,1),coord_clust1(:,2),'x');
                hold on
                plot(coord_clust2(:,1),coord_clust2(:,2),'or');
                hold on
                plot(coord_clust3(:,1),coord_clust3(:,2),'sg');
                xlabel('latitude');
                ylabel('longitude');
                legend('cluster 1','cluster 2','cluster 3');
                title_name = ([survey_name,'-RUN',str_run(ir,:),'-TH',num2str(threshold(t))]);
                title(title_name);
                
                figure(6)
                
                
                % saving
                savename5=([path_save_map,'/clust_distri_',survey_name,'-RUN',str_run(ir,:),'-TH',num2str(threshold(t))]);
                saveas(figure(5),savename5,'jpg');
               
                close 
                %end

     end 
end




% map for all run - 1 threshold
% threshold = -60     
% run = 2:8
        for run = 2:8
        % Build dedicated string
        str_run(ir,:)='000';
        str_run(ir,end-length(num2str(run(ir)))+1:end)=num2str(run(ir));

        
                % coordinates and cluster belonging for surface layers
                
%                 load([path_mtx,'/',survey_name,'-RUN001-TH',num2str(threshold),'-ER60-EIlay.mat']);
%                 load([path_save_kmean,'/',survey_name,'-RUN001-TH',num2str(threshold),'_surf_kmean.mat']);
%                 lat_R1 = lat_surfER60_db(:,1,1);
%                 lon_R1 = lon_surfER60_db(:,1,1);
%                 coord_cluster_R1 = [lat_R1,lon_R1,IDX_S(:,1)];
                
%                 
%                 load([path_mtx,'/',survey_name,'-RUN002-TH',num2str(threshold),'-ER60-EIlay.mat']);
%                 load([path_save_kmean,'/',survey_name,'-RUN002-TH',num2str(threshold),'_surf_kmean.mat']);
%                 lat_R2 = lat_surfER60_db(:,1,1);
%                 lon_R2 = lon_surfER60_db(:,1,1);
%                 coord_cluster_R2 = [lat_R2,lon_R2,IDX_S(:,1)];
%                 
%                 
%                 load([path_mtx,'/',survey_name,'-RUN003-TH',num2str(threshold),'-ER60-EIlay.mat']);
%                 load([path_save_kmean,'/',survey_name,'-RUN003-TH',num2str(threshold),'_surf_kmean.mat']);
% 
%                 load([path_mtx,'/',survey_name,'-RUN004-TH',num2str(threshold),'-ER60-EIlay.mat']);
%                 load([path_save_kmean,'/',survey_name,'-RUN004-TH',num2str(threshold),'_surf_kmean.mat']);
% 
%                 load([path_mtx,'/',survey_name,'-RUN005-TH',num2str(threshold),'-ER60-EIlay.mat']);
%                 load([path_save_kmean,'/',survey_name,'-RUN005-TH',num2str(threshold),'_surf_kmean.mat']);
% 
%                 load([path_mtx,'/',survey_name,'-RUN006-TH',num2str(threshold),'-ER60-EIlay.mat']);
%                 load([path_save_kmean,'/',survey_name,'-RUN006-TH',num2str(threshold),'_surf_kmean.mat']);
% 
%                 load([path_mtx,'/',survey_name,'-RUN007-TH',num2str(threshold),'-ER60-EIlay.mat']);
%                 load([path_save_kmean,'/',survey_name,'-RUN007-TH',num2str(threshold),'_surf_kmean.mat']);
% 
%                 load([path_mtx,'/',survey_name,'-RUN008-TH',num2str(threshold),'-ER60-EIlay.mat']);
%                 load([path_save_kmean,'/',survey_name,'-RUN008-TH',num2str(threshold),'_surf_kmean.mat']);
%                                 
 
                load([path_mtx,'/',survey_name,'-RUN',str_run(ir,:),'-TH',num2str(threshold),'-ER60-EIlay.mat']);
                load([path_save_kmean,'/',survey_name,'-RUN',str_run(ir,:),'-TH',num2str(threshold),'_surf_kmean.mat']);
                     
                
                lat_S = lat_surfER60_db(:,1,1);
                lon_S = lon_surfER60_db(:,1,1);
                coord_cluster_S = [lat_S,lon_S,IDX_S(:,1)];
                Sz = size(coord_cluster_S);
                size1run = Sz(1);
                
                % coordinates and cluster belonging for all RUNs
                coord_all = [coord_all; coord_cluster_S];
                size_run = [size_run; size1run];
                
        end

% Separate the clusters
% line rak of each cluster (surface layers)
                i_all_1 = find(coord_all(:,3)==1);
                i_all_2 = find(coord_all(:,3)==2);
                i_all_3 = find(coord_all(:,3)==3);
                
                % coordinates for points of each cluster (surface layers)
                coord_all_1 = coord_all(i_all_1,:);
                coord_all_2 = coord_all(i_all_2,:);
                coord_all_3 = coord_all(i_all_3,:);


% graph
figure(6)
%  plot(coord_all_1(:,1),coord_all_1(:,2),'x');
%  hold on
plot(coord_all_2(:,1),coord_all_2(:,2),'xr');
hold on
plot(coord_all_3(:,1),coord_all_3(:,2),'xg');
xlabel('latitude');
ylabel('longitude');
legend('cluster 1','cluster 2','cluster 3');
%legend('cluster 2','cluster 3');


%%

% coordinates for points of each cluster (surface layers)
coord_clust1 = coord_cluster_S(i_1,:);
coord_clust2 = coord_cluster_S(i_2,:);
coord_clust3 = coord_cluster_S(i_3,:);

% saving

path_save_cluster = [path_save,'/cluster_res'];
if ~exist(path_save_cluster,'dir')
    mkdir(path_save_cluster)
end

save([path_save_cluster,'/',survey_name,'-RUN',str_run(ir,:),'-TH',num2str(threshold(t)),'-cluster_coord_',num2str(n_cluster),'.mat'],'coord_clust1','coord_clust2','coord_clust3');


%% Biplots

vmult=10;
ptsymb = {'bs','r^','md','go','c+'};

%subplot(2,2,1)
for i = 1:size(unique(IDX),1)
    clust = find(IDX==i);
    plot(Xpc(clust,1),Xpc(clust,2),ptsymb{i});
    
    hold on
end

hold on

text(coeff(:,1)*vmult,coeff(:,2)*vmult,nvar)
h=compass(coeff(:,1)*(vmult-1),coeff(:,2)*(vmult-1))
set(h,'Color',[.8 .8 .8],'MarkerSize',6);
line([min(scores(:,1))-1 max(scores(:,1))+1],[0 0],'Color',[.8 .8 .8])
line([0 0],[min(scores(:,2))-1 max(scores(:,2))+1],'Color',[.8 .8 .8])
title({'PCA biplot + kmeans clustering of ME70 shoal descriptors at -60dB'});
legend('Cluster1','Cluster2');
xlabel('PC1');
ylabel('PC2');

%% CODE MATHIEU
% % %Xs=multishl60s(time_ME70_60sf~=time_ME70_60sf(317),:);
% % Xs=multishl60(1:50,:);
% % Xsstd=(Xs-repmat(mean(Xs,1),size(Xs,1),1))./repmat(std(Xs,1),size(Xs,1),1); 
% % [coefs2,scores2,variances2,t2] = princomp(Xsstd);
% % percent_explained2 = 100*variances2/sum(variances2)
% % pareto(percent_explained2)
% % xlabel('Principal Component')
% % ylabel('Variance Explained (%)')
% % biplot(coefs2(:,1:3), 'scores',scores2(:,1:3),'varlabels',nvar)
% % Xpc=scores2(:,1:3);
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% kmeans clustering
% % [cidx2,cmeans2] = kmeans(Xpc,4,'dist','sqeuclidean');
% % [silh2,h] = silhouette(Xpc,cidx2,'sqeuclidean');
% % [cidx3,cmeans3,sumd3] = kmeans(Xpc,2,'replicates',5,'display','final');
% % sum(sumd3)
% 
% % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%HAC clustering
% % eucD = pdist(Xpc,'euclidean');
% % clustTreeEuc = linkage(eucD,'average');
% % cophenet(clustTreeEuc,eucD)
% % [h,nodes] = dendrogram(clustTreeEuc,0);
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Biplots
% 
% vmult=10;
% ptsymb = {'bs','r^','md','go','c+'};
% 
% %subplot(2,2,1)
% for i = 1:size(unique(cidx2),1)
%     clust = find(cidx2==i);
%     plot(Xpc(clust,1),Xpc(clust,2),ptsymb{i});
%     hold on
% end
% hold on
% text(coefs2(:,1)*vmult,coefs2(:,2)*vmult,nvar)
% h=compass(coefs2(:,1)*(vmult-1),coefs2(:,2)*(vmult-1))
% set(h,'Color',[.8 .8 .8],'MarkerSize',6);
% line([min(scores2(:,1))-1 max(scores2(:,1))+1],[0 0],'Color',[.8 .8 .8])
% line([0 0],[min(scores2(:,2))-1 max(scores2(:,2))+1],'Color',[.8 .8 .8])
% title({'PCA biplot + kmeans clustering of ME70 shoal descriptors at -60dB'});
% legend('Cluster1','Cluster2');
% xlabel('PC1');
% ylabel('PC2');