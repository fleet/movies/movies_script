    %Clean workspace
    clear all;
    
%% Set paths

      %path_hac_survey='X:/CLASS08/Acoustique/ME70-HAC/'
    %path_hac_survey='H:/HacCorPG12/'
    %path_hac_survey='J:/HacPG12_Hermes/stationsLeg3/ER60only/'
    %path_hac_survey='D:/PELGAS12/stationsLeg3/ER60only/'
    %path_hac_survey='M:/PELGAS12/LEG3/ER60/laySmallScale/stationsLeg3/ER60only/'
    path_hac_survey='J:/HacPG12_Hermes/stationsLeg3/ER60only/'
    %path_hac_survey='E:/CLASS08/Acoustique/ME70-HAC/'    
    path_config = [path_hac_survey,'Configs/EIlay-HighResolution']
    path_save=[path_hac_survey,'Results/EIlay-HighResolution']
    %path_save=['M:/PELGAS2012/result_EI']
    %If save path does not exist, create it
      if ~exist(path_save,'dir')
                mkdir(path_save)
      end
    
%% Echo-integration
 thresholds=[-80 -70 -60 -50];
 %thresholds=[-80];
 runs=[13:27];
 
 batch_EI_multiTHR(path_hac_survey,path_config,path_save,thresholds,runs);

%imagesc(Sv_surfER60(:,:,1)')


%% Bind EIlay results matrices
thresholds=[-80 -70 -60 -50];
runs=[13:27];
%runs=10
survey_name='PELGAS2012';
nfile1=repmat(1,1,size(runs,2));

for ir=1:length(runs)
    runs(ir)
    for t=1:size(thresholds,2)
        thresholds(t)
        str_run(ir,:)='000';
        str_run(ir,end-length(num2str(runs(ir)))+1:end)=num2str(runs(ir));
        filename_ME70=[path_save,'/',survey_name,'-RUN' str_run(ir,:),'-TH' num2str(thresholds(t)),'-ME70-EIlay.mat'];
        filename_ER60=[path_save,'/',survey_name,'-RUN' str_run(ir,:),'-TH' num2str(thresholds(t)),'-ER60-EIlay.mat'];
        path_results=[path_save,'/RUN' str_run(ir,:) '/',num2str(thresholds(:,t)),'/'];
        EIlayRes_bind(path_results,filename_ME70,filename_ER60,nfile1(ir));
    end
end

%fpath=path_results

%% Load bound EIlay results and creates a n*p*t arrray with n esdus, p depth layers and t EI thresholds, at 1 frequency

survey_name='PELGAS2012';
runs=[13:27];
thresholds=[-80 -70 -60 -50];
frequencies=[18 38 70 120 200 333];
fsel='ALL';
Nlayers='ALL'; 
   
for ir=1:length(runs)
%ir=1;
   str_run(ir,:)='000';
   str_run(ir,end-length(num2str(runs(ir)))+1:end)=num2str(runs(ir));

   
    
    for j=1:size(thresholds,2)

        %filename_ME70=[path_save,'/',survey_name,'-RUN' str_run,'-TH' num2str(thresholds),'-ME70-EIlay.mat'];
        %filename_ER60=[path_save,'/','RUN',str_run(ir,:),'/',survey_name,'-RUN' str_run(ir,:),'-TH' num2str(thresholds(j)),'-ER60-EIlay.mat'];
        filename_ER60=[path_save,'/',survey_name,'-RUN' str_run(ir,:),'-TH' num2str(thresholds(j)),'-ER60-EIlay.mat'];
        load(filename_ER60);

        if (strcmp('ALL',Nlayers))    
            Nlayers=size(Sv_surfER60_db,2);    
        end
        
        if ~strcmp('ALL',fsel)
            svsj=Sv_surfER60_db(:,1:Nlayers,frequencies==fsel);
            sasj=Sa_surfER60_db(:,1:Nlayers,frequencies==fsel);
        else
            svsj=Sv_surfER60_db(:,1:Nlayers,:);
            sasj=Sa_surfER60_db(:,1:Nlayers,:);

        end
 
        if j==1
            sv=svsj;
            f=frequencies;
            thr=repmat(thresholds(j),1,size(frequencies,2));
            sa=sasj;
        else
            sv=cat(3,sv,svsj);
            sa=cat(3,sa,sasj);
            f=cat(2,f,frequencies);
            thr=cat(2,thr,repmat(thresholds(j),1,size(frequencies,2)));
        end
     clear Sa_surfER60_db Sv_surfER60_db
    end
   
    filename_ER60=[path_save,'/',['RUN',str_run(ir,:)],'/',survey_name,'-RUN',str_run(ir,:),'-ER60-EIlay',num2str(fsel),'MultiThr',[num2str(thresholds(1)),num2str(thresholds(size(thresholds,2)))],'.mat'];
    
    save(filename_ER60,'time_ER60_db','lat_surfER60_db','lon_surfER60_db','depth_bottom_ER60_db','depth_surface_ER60_db','sv','sa','thr','f');
end 

%% Load 1 bound file

survey_name='PELGAS2012';
runs=1;
thresholds=[-80 -70 -60 -50];
fsel='ALL';

    ir=1;
    str_run='000';
    str_run(end-length(num2str(runs(ir)))+1:end)=num2str(runs);

filename_ER60=[path_save,'/',['RUN',str_run],'/',survey_name,'-RUN',str_run,'-ER60-EIlay',num2str(fsel),'MultiThr',[num2str(thresholds(1)),num2str(thresholds(size(thresholds,2)))],'.mat'];
load(filename_ER60)

%% casino import

path_evt='M:\PELGAS12\casino\csv\pelgas12_cor.csv';
allstring=1;

[C,Cs,evts,datetime,juliantime]=import_casino(path_evt,allstring);

stationsel=strcmp(evts,'DPFIL');
evts_depth=char(Cs{1,1});

%% Load all bound files and compute sa profiles

survey_name='PELGAS2012';
runs=[1:12];
thresholds=[-80 -70 -60 -50];
frequencies=[18 38 70 120 200 333];
fsel='ALL';

for ir=1:length(runs)
    %load EI result file
   str_run(ir,:)='000';
   str_run(ir,end-length(num2str(runs(ir)))+1:end)=num2str(runs(ir));
    ['station',str_run(ir,:)]
   fsel='ALL'; 
   filename_ER60=[path_save,'/',['RUN',str_run(ir,:)],'/',survey_name,'-RUN',str_run(ir,:),'-ER60-EIlay',num2str(fsel),'MultiThr',[num2str(thresholds(1)),num2str(thresholds(size(thresholds,2)))],'.mat'];
   load(filename_ER60)

  % Time span: converts string times to numeric time (nb of days since 0000)
    tmin=datestr(min(time_ER60_db))
    tmax=datestr(max(time_ER60_db))
   
    %watercolumn selection based on bottom depth   
   %hist(depth_bottom_ER60_db)
   zminbot=min(min(depth_bottom_ER60_db));
   Nlayers=floor(zminbot)-10;
    % Select cells in watercolumn
    svs=sv(:,1:Nlayers,:);
    sas=sa(:,1:Nlayers,:);
    
    % plot echograms
    figure
    nfig=[1 2 3 5 6 7];
    for fi=1:(size(frequencies,2))
        ncols=size(frequencies,2)*([1:size(thresholds,2)]-1);
        svsi=svs(:,1:Nlayers,ncols+fi);
        sas_longs=sas_long(:,ncols+fi);
        fsel=frequencies(fi);
        [num2str(fsel) 'kHz']
        thresholds
        subplot(2,4,nfig(fi))
        imagesc(svsi(:,1:Nlayers,1)')
        title([num2str(frequencies(fi)) 'kHz',num2str(thresholds(1)),'dB'])
    end

   % Compute mean and median NASC per depth layer at -80dB threshold
    msas80=squeeze(mean(sas(:,:,1:6),1));
    medsas80=squeeze(median(sas(:,:,1:6),1));
    qsas80 = quantile(sas(:,:,1:6),[.025 .25 .50 .75 .975]); 
    
    % plots
    z=1:Nlayers;
    z=z+10;
%     boxplot(sas(:,:,1))
%     hold on
%     plot(medsas80)
    %set(gca,'XTickLabel',{num2str(z)})
    %plot(msas80,-z, 'LineWidth',2)
    subplot(2,4,[4 8])
    plot(medsas80,-z, 'LineWidth',2)
    title({['Station',num2str(ir)],[tmin,' - ',tmax],[num2str(round(zminbot)),'m depth',]})
    legend('18kHz','38kHz','70kHz','120kHz','200kHz','333kHz')
    ylabel('Depth (m)')
    xlabel({'Median acoustic density of SSLs','(NASC -80dB thr.)'})
    xlim([0 60]) 
    
    plotname=[path_save,'/',['RUN',str_run(ir,:)],'/',survey_name,'-RUN',str_run(ir,:),'-ER60-EIlay',[num2str(frequencies(1)),'-',num2str(frequencies(size(frequencies,2))),'kHz'],'_MedianProfile',[num2str(thresholds(1)),'dB']];
    hgsave(plotname)
    saveas(gcf,plotname, 'jpg')
    
    medsa_filename=[path_save,'/',['RUN',str_run(ir,:)],'/',survey_name,'-RUN',str_run(ir,:),'-ER60-EIlay',num2str(fsel),'qsa-80_zf',[num2str(thresholds(1)),num2str(thresholds(size(thresholds,2)))],'.mat'];
    save(medsa_filename,'qsas80')

end    

%% Exploratory plots

% Time span: converts string times to numeric time (nb of days since 0000)
tmin=datestr(min(time_ER60_db))
tmax=datestr(max(time_ER60_db))

%Ship track
figure;
plot(lon_surfER60_db(:,1,1),lat_surfER60_db(:,1,1))

%Echogram plots
%-------------------
%Laurent's style
figure
subplot(2,3,1)
DrawEchoGram_brut(sv(:,:,1)',-100,0)
title('38 kHz -70dB')
subplot(2,3,2)
DrawEchoGram_brut(sv(:,:,2)',-100,0)
title('38 kHz -65dB')
subplot(2,3,3)
DrawEchoGram_brut(sv(:,:,3)',-100,0)
title('38 kHz -60dB')
subplot(2,3,4)
DrawEchoGram_brut(sv(:,:,4)',-100,0)
title('38 kHz -55dB')
subplot(2,3,5)
DrawEchoGram_brut(sv(:,:,5)',-100,0)
title('38 kHz -50dB')

%Matlab's style
%all frequencies/threshold combinations
%No. of layers to be plotted
Nlayers_db=[25]
Nlayers=25;

for thri=0:(size(thresholds,2)-1)
    svs=sv(:,:,(thri*6)+1:(thri+1)*6);
    %(thri*6)+1:(thri+1)*6
    figure
    subplot(2,3,1)
    imagesc(svs(:,1:Nlayers,1)')
    title([num2str(frequencies(1)) 'kHz',num2str(thresholds(thri+1)),'dB'])
    subplot(2,3,2)
    imagesc(svs(:,1:Nlayers,2)')
    title([num2str(frequencies(2)) 'kHz',num2str(thresholds(thri+1)),'dB'])
    subplot(2,3,3)
    imagesc(svs(:,1:Nlayers,3)')
    title([num2str(frequencies(3)) 'kHz',num2str(thresholds(thri+1)),'dB'])
    subplot(2,3,4)
    imagesc(svs(:,1:Nlayers,4)')
    title([num2str(frequencies(4)) 'kHz',num2str(thresholds(thri+1)),'dB'])
    subplot(2,3,5)
    imagesc(svs(:,1:Nlayers,5)')
    title([num2str(frequencies(5)) 'kHz',num2str(thresholds(thri+1)),'dB'])
    subplot(2,3,6)
    imagesc(svs(:,1:Nlayers,6)')
    title([num2str(frequencies(6)) 'kHz',num2str(thresholds(thri+1)),'dB'])
end    

%f/thr combinations
%---------------
vertcat(f,thr)

%% Select cells in watercolumn

svs=sv(:,1:Nlayers,:);
sas=sa(:,1:Nlayers,:);

%% Reshape Sv matrix: rows=echogram cells, columns=Sv@thresholds 
    size(svs)
    svs_long=squeeze(reshape(svs,[],size(svs,3)));
    size(svs_long)
    sas_long=squeeze(reshape(sas,[],size(sas,3)));
    size(sas_long)
    svs_wide=squeeze(reshape(svs_long,size(svs,1),[],size(svs,3)));
    size(svs_wide)
    
    imagesc(svs_wide(:,:,1)')

%% Mean thresholding response

Nlayers=25;

for fi=1:(size(frequencies,2))
    ncols=size(frequencies,2)*([1:size(thresholds,2)]-1);
    svsi=svs(:,1:Nlayers,ncols+fi);
    sas_longs=sas_long(:,ncols+fi);
    msa=mean(sas_longs,1);
    sdsa=std(sas_longs,1);
    fsel=frequencies(fi);
    [num2str(fsel) 'kHz']
    thresholds
    if fi==1
        msa_db=msa;
        sdsa_db=sdsa;
    else    
        msa_db=vertcat(msa_db,msa);
        sdsa_db=vertcat(sdsa_db,sdsa);
    end    
    figure
    subplot(2,3,1)
    imagesc(svsi(:,1:Nlayers,1)')
    title([num2str(frequencies(fi)) 'kHz',num2str(thresholds(1)),'dB'])
    subplot(2,3,2)
    imagesc(svsi(:,1:Nlayers,2)')
    title([num2str(frequencies(fi)) 'kHz',num2str(thresholds(2)),'dB'])
    subplot(2,3,3)
    imagesc(svsi(:,1:Nlayers,3)')
    title([num2str(frequencies(fi)) 'kHz',num2str(thresholds(3)),'dB'])
    subplot(2,3,4)
    imagesc(svsi(:,1:Nlayers,4)')
    title([num2str(frequencies(fi)) 'kHz',num2str(thresholds(4)),'dB'])
    subplot(2,3,5)
    plot(thresholds,msa)
    title(['Thresholding response,' num2str(frequencies(fi)) 'kHz'])
%     hold on
%     errorbar(thresholds,msa,sdsa,'r')
    plotname=[path_save,'/',['RUN',str_run],'/',survey_name,'-RUN',str_run,'-ER60-EIlay',[num2str(frequencies(fi)),'kHz'],'_MultiThr',[num2str(thresholds(1)),num2str(thresholds(size(thresholds,2))),'dB']];
    hgsave(plotname)
    saveas(gcf,plotname, 'jpg')
    
    filename_msa=[path_save,'/',['RUN',str_run],'/',survey_name,'-RUN',str_run,'-ER60-EIlay',[num2str(frequencies(fi)),'kHz'],'_MeanNASCPerThr',[num2str(thresholds(1)),num2str(thresholds(size(thresholds,2))),'dB'],'.mat'];
    save(filename_msa,'msa','sdsa','fsel','thresholds');
    
    filename_msa=[path_save,'/',['RUN',str_run],'/',survey_name,'-RUN',str_run,'-ER60-EIlay',[num2str(frequencies(1)),'-',num2str(frequencies(size(frequencies,2))),'kHz'],'_MeanNASCPerThr',[num2str(thresholds(1)),num2str(thresholds(size(thresholds,2))),'dB'],'.mat'];
    save(filename_msa,'msa_db','sdsa_db','fsel','thresholds');
end    

close all

plot(thresholds,msa_db)
hleg1 = legend(num2str(frequencies(1)),num2str(frequencies(2)),num2str(frequencies(3)),num2str(frequencies(4)),num2str(frequencies(5)),num2str(frequencies(6)),...
              'Location','NorthEastOutside');
xlabel('Echo-integration threshold'); ylabel('Mean NASC')
title('Station 1 Gironde plume')
plotname=[path_save,'/',['RUN',str_run],'/',survey_name,'-RUN',str_run,'-ER60-EIlay',[num2str(frequencies(1)),'-',num2str(frequencies(size(frequencies,2))),'kHz'],'_MeanNASCPerThr',[num2str(thresholds(1)),num2str(thresholds(size(thresholds,2))),'dB']];
hgsave(plotname)
saveas(gcf,plotname, 'jpg')   


%% PCA on Sv
% PCA    
   [coeff,scores,variances,t2]=princomp(svs_long);    
        
        % Variance percentage explained      
        percent_explained = 100*variances/sum(variances);      

        % graphs
        figure(1)
        pareto(percent_explained)
        xlabel('Principal Components')
        ylabel('Variance Explained for SURFACE layers (%)')
        
        % Biplot
        
      ft=num2str(vertcat(f,thr))
      
      for ni=1:size(ft,2)
          ft(3,ni)=strcat(num2str(ft(1,ni)),num2str(ft(2,ni)))
      end
   
      biplot(coeff(:,1:2), 'scores',scores(:,1:2));
    
%% kmeans clustering

% PCs selection

   Xpc=scores(:,1:10);
   coefs=coeff(:,1:10);

% kmeans   
 
for nc=2:10
    nc
    [cidx3,cmeans3,sumd3] = kmeans(Xpc,nc,'replicates',5,'display','final');
    clustsumi=[nc sum(sumd3) mean(sumd3) std(sumd3)]
    if nc==2
        clustsum=clustsumi;
        cidx=cidx3;
    else
        clustsum=vertcat(clustsum,clustsumi);
        cidx=horzcat(cidx,cidx3);
    end
end

plot(clustsum(:,1),clustsum(:,2:4))
legend('Sum of distance to centroids','Mean distance to centroids','SD of distance to centroids')

% HAC clustering: does not work

eucD = pdist(Xpc,'euclidean');
clustTreeEuc = linkage(eucD,'average');
cophenet(clustTreeEuc,eucD)
[h,nodes] = dendrogram(clustTreeEuc,0);

% Biplots

figure
for nc=2:5
    cidx3=cidx(:,nc);
    vmult=10;
    ptsymb = {'bs','r^','md','go','c+','bd'};
    subplot(2,3,nc-1)
    for i = 1:size(unique(cidx3),1)
        clust = find(cidx3==i);
        plot(Xpc(clust,1),Xpc(clust,2),ptsymb{i});
        hold on
    end
    hold on
    %text(coefs(:,1)*vmult,coefs(:,2)*vmult,nvar)
    h=compass(coefs(:,1)*(vmult-1),coefs(:,2)*(vmult-1));
    set(h,'Color',[.8 .8 .8],'MarkerSize',6);
    line([min(scores(:,1))-1 max(scores(:,1))+1],[0 0],'Color',[.8 .8 .8])
    line([0 0],[min(scores(:,2))-1 max(scores(:,2))+1],'Color',[.8 .8 .8])
    title([num2str(nc) 'clusters']);
    %legend(matstr(repmat('Cluster',nc,1),reshape(1:nc,[],1));
    xlabel('PC1');
    ylabel('PC2');
end    

plotname=[path_save,'/',['RUN',str_run],'/',survey_name,'-RUN',str_run,'-ER60-EIlay',[num2str(frequencies(1)),'-',num2str(frequencies(size(frequencies,2))),'kHz'],'_ClusterBiplots',[num2str(thresholds(1)),num2str(thresholds(size(thresholds,2))),'dB']];
hgsave(plotname)
saveas(gcf,plotname, 'jpg')

for nc=2:5
    cidx3=cidx(:,nc);
    cidx3_wide=squeeze(reshape(cidx3,size(sv,1),[],1));
    size(cidx3_wide)
    if nc==2 
        figure
    end    
    subplot(2,3,nc-1)
    imagesc(cidx3_wide(:,:,1)')
    title([num2str(nc) 'clusters'])
end    
    subplot(2,3,nc)
    imagesc(svs_wide(:,:,1)')
    title('Original echogram')
    
plotname=[path_save,'/',['RUN',str_run],'/',survey_name,'-RUN',str_run,'-ER60-EIlay',[num2str(frequencies(1)),'-',num2str(frequencies(size(frequencies,2))),'kHz'],'_EchogramSegmentation',[num2str(thresholds(1)),num2str(thresholds(size(thresholds,2))),'dB']];
hgsave(plotname)
saveas(gcf,plotname, 'jpg')

%PCs echograms
  size(Xpc)

  figure
  for npc=1:5
    PCi_wide=squeeze(reshape(Xpc(:,npc),size(sv,1),[],1));
    subplot(2,3,npc)
    imagesc(PCi_wide(:,:,1)')
    title(['Principal component ',num2str(npc),' echogram'])
  end
  subplot(2,3,6)
  mean_echogram=mean(svs_wide,3);
  imagesc(mean_echogram')
  title('Original echogram averaged over frequencies')

%% manual clustering    
    size(scores)
    
    scores_std=(scores-repmat(mean(scores,1),size(scores,1),1))./repmat(std(scores,1),size(scores,1),1);

    gcells=repmat(0,1,size(scores,1));
    gcells(scores(:,1)<0)=1;
    gcells((scores(:,1)<0.2)&(scores(:,2)<-0.1))=2;
    gcells((scores(:,1)<0.5)&(scores(:,2)<0))=3;    
    gcells(((scores(:,1)<0.6)&(scores(:,1)>0.4))&(scores(:,2)<0.25))=4; 
    gcells((scores(:,1)>0.45)&(scores(:,2)>0.25))=5; 
    
    size(coeff)
    size(scores)
    hist(scores(:,1))
    hist(scores(:,2))
    hist(scores(:,1))
    unique(gcells)
    tabulate(gcells)
    
    gcells_wide=squeeze(reshape(gcells,size(sv38,1),size(sv38,2)));
   
    gmsa38=grpstats(sa38_long,gcells);
    
    
    figure
subplot(2,3,1)
imagesc(sv38(:,:,1)')
title('38 kHz -70dB')
subplot(2,3,2)
imagesc(sv38(:,:,2)')
title('38 kHz -65dB')
subplot(2,3,3)
imagesc(sv38(:,:,3)')
title('38 kHz -60dB')
subplot(2,3,4)
imagesc(sv38(:,:,4)')
title('38 kHz -55dB')
subplot(2,3,5)
imagesc(sv38(:,:,5)')
title('38 kHz -50dB')
subplot(2,3,6)
imagesc(gcells_wide')
title('Cell classification based on thresholding response')

figure
imagesc(gcells_wide')

%% import log file (Casino)

%Path
path_evt='X:/CLASS08/casino/CLASS08_evts.csv';

C=import_casino(path_evt);

%% Merge EI and log data with cutlay_casino function
%Inputs:
%-------
%   C:          a casino output file
%   timeER60:   ESU time stamps (e.g. from EI results)
%   bps:        casino codes of sequences breakpoints to be selected
%   istarts     casino codes of sequences starts to be selected
%
%       e.g. to select only on-transect and inter-transect sequences (i.e. to exclude trawl hauls), use:
%           
        bps=char('DEBURAD','REPRAD','INTERAD','STOPRAD','FINRAD')
%           
        istarts=char('DEBURAD','REPRAD','INTERAD')
%       OR to select only on-transect sequences (excluding trawl hauls and inter-transects), use:
%           bps=char('DEBURAD','REPRAD','STOPRAD','FINRAD')
%           istarts=char('DEBURAD','REPRAD')
%       OR to select only trawl hauls sequences, use:
%           bps=char('DFIL','CULAB')
%           istarts=char('DFIL')
%       OR to select everything:
%           
bps=char('ALL')
%           
istarts=char('ALL')

[sdates3,stimes3,sevts3,rad_self] = cutlay_casino(C,time_ER60_db,bps,istarts)

%If the above does not work, try running it several times...





