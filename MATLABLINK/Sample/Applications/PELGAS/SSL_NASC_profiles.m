    %Clean workspace
    clear all;
    
%% Set paths

    path_hac_survey='J:/HacPG12_Hermes/stationsLeg3/ER60only/'
    path_taps='M:/PELGAS12/LEG3/TAPS/matrices_matlab/'
    path_save2='M:/PELGAS12/LEG3/ER60/NASCProfiles/'
    path_save=[path_hac_survey,'Results/EIlay-HighResolution']
    %If save path does not exist, create it
      if ~exist(path_save,'dir')
                mkdir(path_save)
      end
%% casino import

path_evt='M:\PELGAS12\casino\csv\pelgas12.csv';
allstring=1;

[C,Cs,evts,datetime,juliantime]=import_casino(path_evt,allstring);

codeaction=C{1,11};
stationLeg3sel=strcmp('DPFIL',codeaction);
nope=char(C{1,14});
nosta=char(C{1,15});
nopeLeg3=deblank(nope(stationLeg3sel,:));
nostaLeg3=deblank(nosta(stationLeg3sel,:));
%nostaLeg3(isspace(nostaLeg3))='0'

%% Load all bound files and compute sa profiles

survey_name='PELGAS2012';
runs=[20:27];
thresholds=[-80 -70 -60 -50];
ER60frequencies=[18 38 70 120 200 333];
TAPSfrequencies=[250 420 700 1100 1850 3040];
fsel='ALL';

for ir=1:length(runs)
    %load EI result file
   str_run(ir,:)='000';
   str_run(ir,end-length(num2str(runs(ir)))+1:end)=num2str(runs(ir));
    ['run ',str_run(ir,:)]
    ['station ',num2str(nostaLeg3(ir,:))]
    ['operation ',num2str(nopeLeg3(ir,:))]
   fsel='ALL'; 
   filename_ER60=[path_save,'/',['RUN',str_run(ir,:)],'/',survey_name,'-RUN',str_run(ir,:),'-ER60-EIlay',num2str(fsel),'MultiThr',[num2str(thresholds(1)),num2str(thresholds(size(thresholds,2)))],'.mat'];
   load(filename_ER60)

   %load and display TAPS profile
   filename_TAPS=[path_taps,'Mtaps_',nopeLeg3(ir,:),'.mat'];
   load(filename_TAPS)
    
  % Time span: converts string times to numeric time (nb of days since 0000)
    tmin=datestr(min(time_ER60_db))
    tmax=datestr(max(time_ER60_db))
   
    %watercolumn selection based on bottom depth   
   %hist(depth_bottom_ER60_db)
   zminbot=min(min(depth_bottom_ER60_db));
   if zminbot>200
       Nlayers=100
   else    
    Nlayers=floor(zminbot)-10;
   end 
    % Select cells in watercolumn
    Svs=sv(:,1:Nlayers,:);
    svs=10.^(Svs./10);
    sas=sa(:,1:Nlayers,:);
    
    z=1:Nlayers;
    z=z+9;
    
    % Compute mean, median NASC and quantiles per depth layer at -80dB threshold
    msas80=squeeze(mean(sas(:,:,1:6),1));
    medsas80=squeeze(median(sas(:,:,1:6),1));
    qsas80 = quantile(sas(:,:,1:6),[.025 .25 .50 .75 .975]); 
    medsvs80=squeeze(median(svs(:,:,1:6),1));
    medSvs80=10*log10(medsvs80);
    meansvs80=squeeze(mean(svs(:,:,1:6),1));
    qsvs80 = quantile(svs(:,:,1:6),[.025 .25 .50 .75 .975]);
    qSvs80=10*log10(qsvs80);
    svmedsas80=10*log10(medsas80/23270);
    svmedsas80(isinf(svmedsas80))=-80;
    
   
%      qlist=[.025 .25 .50 .75 .975];
%      figure
%      subplot(2,2,1)
%      for q=1:5
%          qi=squeeze(qsvs80(q,:,:));
%          %svqi=10*log10(qi/23270);
%          svqi=qi;
%          cols=get(0,'DefaultAxesColorOrder');
%          set(0,'DefaultAxesColorOrder',cols(1:6,:),...
%              'DefaultAxesLineStyleOrder','-|--|:')
%          subplot(2,3,q)
%          plot(svqi,-z,'-','LineWidth',2)
%          hold on
%          plot(svtaps,-ztaps,'--','LineWidth',2)
%          title(num2str(qlist(q)))
%      end       
        
    %Extract TAPS sv and depths and sort taps values in increasing depth
    [zs,IX] = sort(abs(taps1m(:,1)));
    taps1ms=taps1m(IX,:);
    ztaps=abs(taps1ms(:,1));
    %correct TAPS depth
    ztaps=ztaps+3;
    svtaps=taps1ms(:,2:7);  
    
    % Merge TAPS and ER60 sv at depths
    zboth=intersect(ztaps,reshape(z,[],1));
    medSvs80s=medSvs80(ismember(z,zboth),:);
    svtapss=svtaps(ismember(ztaps,zboth),:);
    Sv_ER60TAPS=horzcat(medSvs80s,svtapss);
    frequencies=horzcat(ER60frequencies,TAPSfrequencies);
    
    %qsas=squeeze(qsas80(3,:,:));
    %qsvs=10*log10(qsas/23270);
    %select Sv index to plot
    qsvs=medSvs80;
    
   % plot median ER60 NASC profiles and TAPS profiles
    subplot(1,2,1)
    cols=get(0,'DefaultAxesColorOrder');
    set(0,'DefaultAxesColorOrder',cols(1:6,:),...
       'DefaultAxesLineStyleOrder','-|--|:')
    plot( qsvs,-z,'-','LineWidth',2)
    hold on
    plot(svtaps,-ztaps,'--','LineWidth',2)
    title({['Station ',nostaLeg3(runs(ir),:),', operation ',nopeLeg3(runs(ir),:)],[tmin,' - ',tmax],[num2str(round(zminbot)),'m depth',]})
   legend('18kHz','38kHz','70kHz','120kHz','200kHz','333kHz','250kHz','420kHz','700kHz','1100kHz','1850kHz','3040kHz')
   ylabel('Depth (m)')
   xlabel({'SSLs acoustic density from ER60 (solid) and TAPS (dashed)','(Sv>-80dB)'})
   xlim([-80 -20]) 
   
   % plot median ER60 NASC profiles and TAPS on common range   
%    cols=get(0,'DefaultAxesColorOrder');
%    set(0,'DefaultAxesColorOrder',cols(1:6,:),...
%        'DefaultAxesLineStyleOrder','-|--|:')
%    figure
%    plot(Sv_ER60TAPS,-zboth, 'LineWidth',2)
%    title({['Station ',nostaLeg3(ir,:),', operation ',nopeLeg3(ir,:)],[tmin,' - ',tmax],[num2str(round(zminbot)),'m depth',]})
%    legend('18kHz','38kHz','70kHz','120kHz','200kHz','333kHz','250kHz','420kHz','700kHz','1100kHz','1850kHz','3040kHz')
%    ylabel('Depth (m)')
%    xlabel({'SSLs acoustic density from ER60 (solid) and TAPS (dashed)','(Sv)'})
%    xlim([-80 -20]) 

   % plot median ER60 NASC profiles and TAPS frequency response at depth over common range
    subplot(1,2,2)
    plot(log(frequencies),Sv_ER60TAPS, 'LineWidth',2)
    legend(num2str(zboth))
    xlabel({'Log(frequency)'})
 
    plotname=[path_save,'/',['RUN',str_run(ir,:)],'/',survey_name,nopeLeg3(runs(ir),:),'-station',nostaLeg3(runs(ir),:),'-','-ER60TAPS-EIlay',[num2str(ER60frequencies(1)),'-',num2str(ER60frequencies(size(ER60frequencies,2))),'kHz'],'_MedianNASCProfile',[num2str(thresholds(1)),'dB']];
    plotname2=[path_save2,'/',survey_name,nopeLeg3(runs(ir),:),'-station',nostaLeg3(runs(ir),:),'-','-ER60TAPS-EIlay',[num2str(ER60frequencies(1)),'-',num2str(ER60frequencies(size(ER60frequencies,2))),'kHz'],'_MedianNASCProfile',[num2str(thresholds(1)),'dB']];
    hgsave(plotname)
    hgsave(plotname2)
    
    % plot echograms with median NASC profile
    figure
    nfig=[1 2 3 5 6 7];
    for fi=1:(size(ER60frequencies,2))
        ncols=size(ER60frequencies,2)*([1:size(thresholds,2)]-1);
        svsi=Svs(:,1:Nlayers,ncols+fi);
        fsel=ER60frequencies(fi);
        [num2str(fsel) 'kHz']
        thresholds
        subplot(2,4,nfig(fi))
        DrawEchoGram_brut(svsi(:,1:Nlayers,1)',-80,-45)
        colorbar
        colorbar('location','southoutside')
        %set(gcf, 'PaperPositionMode', 'manual');
        %set(gcf, 'PaperUnits', 'inches');
        %set(gcf, 'PaperPosition', [2 1 4 2]);
        %imagesc(svsi(:,1:Nlayers,1)')
        title([num2str(ER60frequencies(fi)) 'kHz',num2str(thresholds(1)),'dB'])
    end
    
    subplot(2,4,[4 8])
    plot(medSvs80,-z, 'LineWidth',2)
    title({['Station ',nostaLeg3(runs(ir),:),', operation ',nopeLeg3(runs(ir),:)],[tmin,' - ',tmax],[num2str(round(zminbot)),'m depth',]})
    legend('18kHz','38kHz','70kHz','120kHz','200kHz','333kHz')
    ylabel('Depth (m)')
    xlabel({'Median acoustic density of SSLs','(sV -80dB thr.)'})
    xlim([-90 -40]) 
    
    plotname=[path_save,'/',['RUN',str_run(ir,:)],'/',survey_name,nopeLeg3(runs(ir),:),'-station',nostaLeg3(runs(ir),:),'-','-ER60-EIlay',[num2str(ER60frequencies(1)),'-',num2str(ER60frequencies(size(ER60frequencies,2))),'kHz'],'_MedianNASCProfile',[num2str(thresholds(1)),'dB']];
    plotname2=[path_save2,'/',survey_name,nopeLeg3(runs(ir),:),'-station',nostaLeg3(runs(ir),:),'-ER60-EIlay',[num2str(ER60frequencies(1)),'-',num2str(ER60frequencies(size(ER60frequencies,2))),'kHz'],'_MedianNASCProfile',[num2str(thresholds(1)),'dB']];
    hgsave(plotname)
    %saveas(gcf,plotname, 'jpg')
    hgsave(plotname2)
    %saveas(gcf,plotname2, 'jpg')
%     
    medsa_filename=[path_save,'/',['RUN',str_run(ir,:)],'/',survey_name,nopeLeg3(runs(ir),:),'-station',nostaLeg3(runs(ir),:),'-','_qNASC-80dB-zf_',[num2str(ER60frequencies(1)),'-',num2str(ER60frequencies(size(ER60frequencies,2))),'kHz'],'.mat'];
    medsa_filename2=[path_save2,survey_name,nopeLeg3(runs(ir),:),'-station',nostaLeg3(runs(ir),:),'_qNASC-80dB-zf_',[num2str(ER60frequencies(1)),'-',num2str(ER60frequencies(size(ER60frequencies,2))),'kHz'],'.mat'];
    save(medsa_filename,'qSvs80','qsas80')
    save(medsa_filename2,'qSvs80','qsas80')

end

%%

%close all
