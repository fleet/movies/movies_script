%% function to import first fields of .csv casino files located at "path_evt"
%----------------------------------------
% WARNING1! .csv file separator: ';'
% WARNING2! Remove all extra "'" in strings before importing

path_lay=

function [C] = import_layCsv(path_lay)
%% creating a string which specifies how to read the values of the casino file
%16 1st columns converted to string, 87 others are double
str = [repmat('%q ',1,16) repmat('%f ',1,87) '%*[^\n]'];
%str = [repmat('%s ',1,103)];

size(str)

%% opening the file with the pre-defined string str
fid = fopen(path_evt);
%
[C] = textscan(fid,str,'delimiter',';','HeaderLines',1);
%[C] = textscan(fid,str,'HeaderLines',1);
fclose(fid);
end


