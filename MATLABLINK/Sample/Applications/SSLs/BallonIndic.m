function [MDCLU1,BallonIndic]=BallonIndicator(CellulesER60,frequences,prefix,maxDepth,path_save)

%on calcule l'indicateur a partir du sv en dB
        %freq=[18 38 70 120 200];
        nbesuER60=size(CellulesER60,1);
        nblayerER60=size(CellulesER60,2);
        nbfreqER60=size(CellulesER60,3);
        
        BallonIndic=zeros(nbesuER60,nblayerER60);
        
        %1=fish, 2='other', 3=fluid like
        
        for esu=1:nbesuER60
            for layer=1:nblayerER60
              if (CellulesER60(esu,layer,2)+CellulesER60(esu,layer,4))>-138
                if (CellulesER60(esu,layer,4)-CellulesER60(esu,layer,2))<3
                   BallonIndic(esu,layer)=1;
                else
                   if (CellulesER60(esu,layer,4)-CellulesER60(esu,layer,2))>0
                       BallonIndic(esu,layer)=3;
                   else
                       BallonIndic(esu,layer)=2;
                   end    
                end
               else
                   if (CellulesER60(esu,layer,4)-CellulesER60(esu,layer,2))>0
                       BallonIndic(esu,layer)=3;
                   else
                       BallonIndic(esu,layer)=2;
                   end
              end
            end
        end

        %on sauvegarde les r�sultats
        save ([path_save,prefix,'_BallonIndic'],'BallonIndic');
    % changement de structure des facteurs 
    idx1_wide = squeeze(reshape(BallonIndic,size(CellulesER60,1),[],size(CellulesER60,2)));
        
    SVrawLong = squeeze(reshape(CellulesER60,[],size(CellulesER60,3)));
    size(SVrawLong)
 
    clustersNames={'Fish','Other','Fluid-like'};
    
figure(1);
for clu = 1:3
    SVrawLongs=SVrawLong(BallonIndic== clu,:);
    subplot(3,2,clu);
    boxplot(SVrawLongs)
    ylim([-110,-30])
    set(gca,'XTickLabel',{'18','38','70','120','200'},'XTick',1:5,'FontSize',11)
    xlabel('Frequences [kHz]'); %ylabel('Intensite acoustique [dB]');
    title(['Ballon clusters',clustersNames{clu}])
end

        saveas(figure(1),[path_anal,prefix,'_Ballon-fClustBoxplots'],'jpeg');
        saveas(figure(1),[path_anal,prefix,'_Ballon-fClustBoxplots'],'fig');

MDCLU1 = [];
for clu = 1:3
    MDF1 = [];
    for f = 1:length(frequences)
        svrawf = 10.^(CellulesER60(:,:,f)./10); 
        mdf1 = 10*log10(median(svrawf(BallonIndic == clu)));    
        MDF1 = [MDF1,mdf1];
    end
    MDCLU1 = [MDCLU1;MDF1]; 
end

% save
save([path_save,prefix,'_BallonRepFreqMedianeByClust_',num2str(k1),'clust'],'MDCLU1');

%% 8. Repr�sentation graphique de la classification des cellules
% --
    idxrad = idx1_wide;
    svrawrad = 10.^(CellulesER60./10);
    SVrawrad = CellulesER60;
          
    % figure
    f4 = figure(4); set(f4,'Units','Normalized','OuterPosition',[0 0 1 1]); 
    % echogrammes aux 5 frequences
    for freq = 1:5
        subplot(3,3,freq); DrawEchoGram_brut(SVrawrad(:,1:(round(maxDepth)),freq)',-45,-90);
        title([num2str(frequences(freq)),' kHz'],'FontSize',11);
        freezeColors
    end

    % reponses frequentielles medianes par cluster
    c = colormap(hsv(3)); % jeu des couleurs pour le clustering
    txtleg = []; txtcolbar = []; subplot(3,3,7); hold on
    for j = 1:3
        plot(MDCLU1(j,1:size(MDCLU1,2))','Color',c(j,:),'LineWidth',2)
        txtleg = [txtleg {['cluster ',num2str(j)]}];    % texte pour la l�gende
        txtcolbar = [txtcolbar {num2str(j)}];       % texte pour la barre des couleurs
    end
    set(gca,'XTickLabel',{'18','38','70','120','200'},'XTick',1:5,'FontSize',10)
    title('Median frequency responses','FontSize',10);
    xlabel('Frequences [kHz]','FontSize',10); %ylabel('Intensite acoustique [dB]');
    subplot(3,3,7)
    hold on
    for j = 1:5
        plot(MDCLU1(j,1:size(MDCLU1,2))','Color',c(j,:),'LineWidth',2)
    end
    legend(txtleg)

    % classification des cellules
    %
    subplot(3,3,8:9); 
    %figure()
    imagesc(BallonIndic(:,1:round(maxDepth))'); 
    colorbar ('location','eastoutside','XTickLabel',txtcolbar,'XTick',[1:3]) 
    title(['Cells classification'],'FontSize',10);
    c = colormap(hsv(3)); % jeu des couleurs pour le clustering

    saveas(figure(4),[path_save,prefix,'_Ballon-Classif_'],'jpeg');
    saveas(figure(4),[path_save,prefix,'_Ballon-Classif_'],'fig');
    


end 
