function [coeff,score,latent,IDX,idx_wide,MDCLU]=CMA_PCA_kmeanClass(SVraw,path_anal,k,silhou,frequences,maxDepth,prefix)

%% 3. Standardisation des donn�es 
% par division de la valeur de chaque cellule � une fr�quence donn�e par 
% la valeur moyenne de la fr�quence
SVstd=[]; standbasedmean=[]; 
for f = 1:5
    meanf = mean(mean(10.^(SVraw(:,:,f)./10)));
    SVstdf = 10*log10((10.^(SVraw(:,:,f)./10))/meanf);
    SVstd(:,:,f) = SVstdf;
    standbasedmean = [standbasedmean;10*log10(meanf)];
end
% sauvegarde des objets
save([path_anal,prefix,'_StandBasedMean.mat'],'standbasedmean'); % les moyennes globales par fr�quence
save([path_anal,prefix,'_SVstd.mat'],'SVstd'); 

% changement de structure des donn�es standardis�es
SVlong = squeeze(reshape(SVstd,[],size(SVstd,3)));
% et non standardis�es
SVrawLong = squeeze(reshape(SVraw,[],size(SVraw,3)));

%% 4. Analyse des correspondances multiples
% sur les donn�es standardis�es
% Large scale
%-------------
SVmax = max(max(SVlong));
SVmin = min(min(SVlong));
lag=(SVmax-SVmin)/5;
bks=[];
bks(1)=SVmin;
for i=1:4
    bks(i+1)=SVmin+lag*(i+1);
end

% ---------- Auto --------------
icfq = [];
for f = 1:5
    i5fq = SVlong(:,f) < ceil(SVmax) & SVlong(:,f) >= bks(4);
    i4fq = SVlong(:,f) < bks(4) & SVlong(:,f) >= bks(3);
    i3fq = SVlong(:,f) < bks(3) & SVlong(:,f) >= bks(2);
    i2fq = SVlong(:,f) < bks(2) & SVlong(:,f) >= bks(1);
    i1fq = SVlong(:,f) < bks(1) & SVlong(:,f) >= floor(SVmin);
    icfq = [i5fq i4fq i3fq i2fq i1fq icfq];
end

save([path_anal,prefix,'_TablDisjComp.mat'],'icfq');
%load([path_anal,prefix,'_TablDisjComp.mat']);

% nom des variables
vlab = {'18_5','18_4','18_3','18_2','18_1','38_5','38_4','38_3','38_2','38_1',...
    '70_5','70_4','70_3','70_2','70_1','120_5','120_4','120_3','120_2','120_1',...
    '200_5','200_4','200_3','200_2','200_1'};

%% 5. Analyse en Composantes Principales 
% sur le tableau disjonctif complet
[coeff,score,latent] = princomp(icfq);
save([path_anal,prefix,'_resPCA.mat'],'coeff','score','latent');
%load([path_anal,'1ping_resPCA.mat']);

% Variance
Var = 100*latent ./ sum(latent); VarCum = cumsum(Var);
figure(1)
hold on; bar(Var); plot(VarCum, '-or','LineWidth',2)
legend({'Var. par PC' 'Var. cumulee'},4); legend boxoff
xlabel('Composantes Principales'); ylabel('Variance expliquee [%]')
saveas(figure(1),[path_anal,prefix,'_variancePCA'],'jpeg'); close(1)

% biplot (projection des variables et des individus dans l'espace des
% composantes principales)

% Le nombre de cellules (= d'individus) �tant trop important pour la visualisation
% graphique, s�lection al�atoire de 10000 individus projet�s dans l'espace
if size(score,1)>10000
    x = score(:,1); n = 10000;
    nx = numel(x); inx = randperm(nx);
    pc1 = score(inx(1:n),1);pc2 = score(inx(1:n),2);
    pc3 = score(inx(1:n),3);pc4 = score(inx(1:n),4);
else
    pc1 = score(:,1);pc2 = score(:,2);
    pc3 = score(:,3);pc4 = score(:,4);
end    
% pour une EI � grande �chelle, visualisation de tous les individus
% pc1 = score(:,1);pc2 = score(:,2);
% pc3 = score(:,3);pc4 = score(:,4);

f2 = figure(2); set(f2,'Units','Normalized','OuterPosition',[0 0 1 1]);
subplot(1,2,1); biplot(coeff(:,1:2),'Scores',[pc1,pc2],...
    'VarLabels',vlab)
xlabel (['PC1 - ',num2str(round(Var(1))),'%'])
ylabel (['PC2 - ',num2str(round(Var(2))),'%'])
title('10000 random scores')
subplot(1,2,2); biplot(coeff(:,3:4),'Scores',[pc3,pc4],...
    'VarLabels',vlab)
xlabel (['PC3 - ',num2str(round(Var(3))),'%'])
ylabel (['PC4 - ',num2str(round(Var(4))),'%'])                    
saveas(figure(2),[path_anal,prefix,'_PC1-2_PC3-4'],'jpeg'); 

%% 6. Kmeans clustering
% ---
X = score(:,1:7); % on ne prend que les 7 premi�res CP pour le clustering
%k = 4;              % nombre de clusters
size(X)
% algorithme Kmeans (�a peut �tre long...)
opt = statset('Display','final','MaxIter',100); 
[IDX,C,sumd] = kmeans(X,k,'Replicates',40,'Options',opt,'start','cluster','emptyaction','singleton'); 

save([path_anal,prefix,'_ACM-KM_',num2str(k),'clusters'],'IDX','C','sumd');
% load([path_anal,'1pingFactorisedACP-KM_',num2str(k),'clusters']);

if silhou==true
    x1 = X1(:,1); n = 10000;
    nx1 = numel(x1); inx1 = randperm(nx1);
    x1r=x1(inx1(1:n),:);
    idx1r=IDX1(inx1(1:n),:);

    figure(3);
    [silh1,h1] = silhouette(x1r,idx1r);
    h = gca;
    h.Children.EdgeColor = [.8 .8 1];
    xlabel 'Silhouette Value';
    ylabel 'Cluster';

    save([path_anal,prefix,'_rawKMsilh_',num2str(k),'clusters'],'h1');
    %load([path_anal,'rawKMsilh_',num2str(k),'clusters']);
end


% changement de structure des facteurs 
idx_wide = squeeze(reshape(IDX,size(SVraw,1),[],size(SVraw,2)));

%% 7. Calcul des r�ponses fr�quentielles m�dianes par cluster
% ---

figure(3);

if k<5
    nrmax=2;
    ncmax=2;
elseif k<7
    nrmax=3;    
    ncmax=2;
else
    nrmax=3;    
    ncmax=3;    
end

for clu = 1:k
    MDF = [];
    SVrawLongs=SVrawLong(IDX == clu,:);
    subplot(nrmax,ncmax,clu);
    boxplot(SVrawLongs)
    ylim([-100,-30])
    title(['Cluster',num2str(clu)])

end
    saveas(figure(3),[path_anal,prefix,'_MCA-KM-fClustBoxplots_',num2str(k),'clust'],'jpeg');
    saveas(figure(3),[path_anal,prefix,'_MCA-KM-fClustBoxplots_',num2str(k),'clust'],'fig');

MDCLU = [];

for clu = 1:k
    MDF = [];
    for f = 1:length(frequences)
        svrawf = 10.^(SVraw(:,:,f)./10); 
        mdf = 10*log10(median(svrawf(IDX == clu)));    
        MDF = [MDF,mdf];
    end
    MDCLU = [MDCLU;MDF]; 
end

% save
save([path_anal,prefix,'_RepFreqMedianeByClust_',num2str(k),'clust'],'MDCLU');
%load([path_anal,'1ping_RepFreqMedianeByClust_',num2str(k),'clust']);

%% 8. Repr�sentation graphique de la classification des cellules
% --
    idxrad = idx_wide;
    svrawrad = 10.^(SVraw./10);
    SVrawrad = SVraw;
          
    % figure
    f4 = figure(4); set(f4,'Units','Normalized','OuterPosition',[0 0 1 1]); 
    % echogrammes aux 5 frequences
    for freq = 1:5
        subplot(3,3,freq); DrawEchoGram_brut(SVrawrad(:,1:(round(maxDepth)),freq)',-45,-90);
        title([num2str(frequences(f)),' kHz'],'FontSize',11);
        freezeColors
    end

    % reponses frequentielles medianes par cluster
    c = colormap(hsv(k)); % jeu des couleurs pour le clustering
    txtleg = []; txtcolbar = []; subplot(3,3,6); hold on
    for j = 1:k
        plot(MDCLU(j,1:size(MDCLU,2))','Color',c(j,:),'LineWidth',2)
        txtleg = [txtleg {['cluster ',num2str(j)]}];    % texte pour la l�gende
        txtcolbar = [txtcolbar {num2str(j)}];       % texte pour la barre des couleurs
    end
    set(gca,'XTickLabel',{'18','38','70','120','200'},'XTick',1:5,'FontSize',10)
    %title('Reponses frequentielles medianes par cluster','FontSize',11);
    xlabel('Frequences [kHz]'); %ylabel('Intensite acoustique [dB]');
    
    % classification des cellules
    %
    subplot(3,3,7:9); 
    %figure()
    imagesc(idxrad(:,1:round(maxDepth))'); 
    colorbar ('location','eastoutside','XTickLabel',txtleg,'XTick',[1:k]) 
    title(['Cells classification'],'FontSize',11);
    c = colormap(hsv(k)); % jeu des couleurs pour le clustering

    saveas(figure(4),[path_anal,prefix,'_ACM-KM-Classif_',num2str(k),'clust'],'jpeg');
    saveas(figure(4),[path_anal,prefix,'_ACM-KM-Classif_',num2str(k),'clust'],'fig');
    
    figure(5)
    imagesc(idxrad(:,1:round(maxDepth))'); 
    colorbar ('location','eastoutside','XTickLabel',txtleg,'XTick',[1:k]) 
    title(['Cells classification'],'FontSize',11);
    c = colormap(hsv(k)); % jeu des couleurs pour le clustering

    saveas(figure(5),[path_anal,prefix,'_ACM-KM-ImageClassif_',num2str(k),'clust'],'jpeg');
    saveas(figure(5),[path_anal,prefix,'_ACM-KM-ImageClassif_',num2str(k),'clust'],'fig');
    
    % figure des r�ponses fr�quentielles

    c = colormap(hsv(k)); % jeu des couleurs pour le clustering
    figure(6); hold on
    for j = 1:k
        plot(MDCLU(j,1:size(MDCLU,2))','Color',c(j,:),'LineWidth',2)
    end
    set(gca,'XTickLabel',{'18','38','70','120','200'},'XTick',1:5,'FontSize',11)
    title('Median frequency response per cluster','FontSize',11);
    xlabel('Frequencies [kHz]'); %ylabel('Intensite acoustique [dB]');
    legend(txtleg,4); legend boxoff
    
    saveas(figure(6),[path_anal,prefix,'_ACM-KM_ReponseFrequentielleClusters_',num2str(k),'clust'],'jpeg');
    saveas(figure(6),[path_anal,prefix,'_ACM-KM_ReponseFrequentielleClusters_',num2str(k),'clust'],'fig');


end
