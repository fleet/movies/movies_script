%LB 02/04/2013 ajout r�cup�ration sondeur horizontal

%Clean workspace
clear all;

%% Set paths
% path_hac_survey='K:/PELGAS13/HacBrut2PG13'
% path_config = [path_hac_survey,'/Config/EIFineEchelle']
% path_save=[path_hac_survey,'/Result/']
% %If save path does not exist, create it
% if ~exist(path_save,'dir')
%     mkdir(path_save)
% end

% ------------------------------------------------------------
% Analyse 1 fichier particulier
% path_hac_survey = 'C:/Users/bremond/Documents/data/Analyse_Pelgas13/acoustique/EK60';

% ------------------------------------------------------------
% Analyse par station
% path_hac_survey='K:/PELGAS13/HacBrut2PG13/RUN_de_nuit'
% path_config = ['C:/Users/bremond/Documents/data/Analyse_Pelgas12/EK60/EI/config/HighResolution']
% path_save=[path_hac_survey,'/Result/']
% %If save path does not exist, create it
% if ~exist(path_save,'dir')
%     mkdir(path_save)
% end

% ------------------------------------------------------------
% Methode Ballon
path_hac_survey='N:/PELGAS13/HacPG13_Hermes/EK60_only/';
path_config = 'M:/PELGAS13/Acoustique/ComparaisonMethodes/Config/Ballon';
path_save='M:/PELGAS13/Acoustique/ComparaisonMethodes/Result_EI/Ballon'

path_hac_survey='//tl-nas2/me70/PELGAS13/HacPG13_Hermes/EK60_only/';
path_config = '//tl-nas2/mission/PELGAS13/Acoustique/ComparaisonMethodes/Config/Ballon';
path_save='/media/Mathieu2/Data/EI_Ballon'

%If save path does not exist, create it
if ~exist(path_save,'dir')
    mkdir(path_save)
end
% ------------------------------------------------------------
% Methode Trenkel
% path_hac_survey='N:/PELGAS13/HacPG13_Hermes/EK60_only/';
% path_config = 'M:/PELGAS13/Acoustique/ComparaisonMethodes/Config/Trenkel';
% path_save='M:/PELGAS13/Acoustique/ComparaisonMethodes/Result_EI/Trenkel'
% %If save path does not exist, create it
% if ~exist(path_save,'dir')
%     mkdir(path_save)
% end
% ------------------------------------------------------------
% Methode LargeScale
path_hac_survey='N:/PELGAS13/HacPG13_Hermes/EK60_only/';
path_config = 'M:/PELGAS13/Acoustique/ComparaisonMethodes/Config/LargeScale';
path_save='M:/PELGAS13/AcoustiqueComparaisonMethodes/Result_EI/LargeScale'
%If save path does not exist, create it
if ~exist(path_save,'dir')
    mkdir(path_save)
end

 %% tests goto

clear all

path_hac_survey='N:/PELGAS13/HacPG13_Hermes/EK60_only/test';
path_conf = 'M:/PELGAS13/Acoustique/ComparaisonMethodes/Config/Ballon';
%path_conf = 'M:/PELGAS13/Acoustique/ComparaisonMethodes/Config/LargeScale';
path_result='N:/PELGAS13/HacPG13_Hermes/EK60_only/test/-80'
%If save path does not exist, create it
if ~exist(path_result,'dir')
    mkdir(path_result)
end
thresholds=[-80];
runs=[4];

chemin_ini=path_hac_survey;
chemin_save=path_result
path_save=path_result
path_config=path_conf;

%      %load configuration
%         path_config_reverse = strrep(path_conf, '\', '/');
%         moLoadConfig(path_config_reverse);
%    
%         %Activate 'EchoIntegrModule'
%         EchoIntegrModule = moEchoIntegration();
%         moSetEchoIntegrationEnabled(EchoIntegrModule);

   date='29-Apr-2013';
        hh=15;
        mm=19;
        ss=00;
        


%% Echo-integration
thresholds=[-80];
runs=[4];
batch_EI_multiTHR(path_hac_survey,path_config,path_save,thresholds,runs);

%% Bind EIlay results matrices
thresholds=[-80];
runs=[4];
survey_name='PELGAS13';
for ir=1:length(runs)
    for t=1:size(thresholds,2)
        str_run(ir,:)='000';
        str_run(ir,end-length(num2str(runs(ir)))+1:end)=num2str(runs(ir));
        filename_ME70=[path_save,'/',survey_name,'-RUN' str_run(ir,:),'-TH' num2str(thresholds(t)),'-ME70-EIlay.mat'];
        filename_ER60=[path_save,'/',survey_name,'-RUN' str_run(ir,:),'-TH' num2str(thresholds(t)),'-ER60-EIlay.mat'];
        %filename_ER60h=[path_save,'/',survey_name,'-RUN' str_run(ir,:),'-TH' num2str(thresholds(t)),'-ER60h-EIlay.mat'];
        path_results=[path_save,'/RUN' str_run(ir,:) '/',num2str(thresholds(:,t)),'/'];
        %EIlayRes_bind(path_results,filename_ME70,filename_ER60,filename_ER60h);
        EIlayRes_bind(path_results,filename_ME70,filename_ER60);
    end
end


%% Load bound EIlay results
thresholds=[-80];
survey_name='PELGAS13';
runs=[4];

fpath=path_save;

sels=[1];
str_run='000'; str_run(end-length(num2str(runs))+1:end)=num2str(runs);
% filename_ME70=[path_save,'/',survey_name,'-RUN' str_run,'-TH' num2str(thresholds),'-ME70-EIlay.mat'];
filename_ER60=[path_save,'/',survey_name,'-RUN' str_run,'-TH' num2str(thresholds),'-ER60-EIlay_SEL00',num2str(sels),'.mat'];
% filename_ER60h=[path_save,'/',survey_name,'-RUN' str_run,'-TH' num2str(thresholds),'-ER60h-EIlay.mat'];
load(filename_ER60);
% load(filename_ER60h);
   

load([path_save '/results_0001_EI.mat'])



end

%% Exploratory plots

% check EI start time

%datestr(time_ER60(1,1)/86400+719529,'mmmm dd, yyyy HH:MM:SS.FFF')
time4human(time_ER60(1,1))

depth_bottom_ER60_db=Depth_botER60;
time_ER60_db=time_ER60;
Sv_surfER60_db=Sv_surfER60;
depth_surface_ER60_db=Depth_surfER60;

plot(time_ER60_db,Sv_surfER60_db(:,1,1))
datetick('x',0)

%choice for frequency
freq=1;

%Bottom detection (errors?)
figure;
plot(time_ER60_db,depth_bottom_ER60_db(:,1,freq))
datetick('x',15)
title('Bottom depth','FontSize',16);
xlabel('Time','FontSize',15);
ylabel('Bottom depth','FontSize',15);

% choice for surface layer
startlayer=1;
endlayer=150;
isregular=1;

%Synthetic echogram
if isregular
    figure;imagesc(1:size(Sv_surfER60_db,1),depth_surface_ER60_db(1,startlayer:endlayer,freq),Sv_surfER60_db(:,startlayer:endlayer,freq)');colorbar;grid on
    hold on;
    plot(1:size(Sv_surfER60_db,1),depth_bottom_ER60_db(:,1,freq))
    title('Synthetic echogram','FontSize',16)
    xlabel('ESU','FontSize',15);
    ylabel('Depth','FontSize',15);
else   
    figure;
    h0=pcolor(Sv_surfER60_db(:,startlayer:endlayer,freq)');colorbar;grid on
    set(h0,'EdgeColor','none','FaceColor','flat')
    title('Synthetic echogram','FontSize',16)
    xlabel('ESU','FontSize',15);
    ylabel('Layer','FontSize',15);
end



%Ship track and mean Sa
figure;
% coastline display with Matlab mapping toolbox
    landareas = shaperead('landareas.shp','UseGeoCoords', true);
 geoshow(landareas);
hold on;
scatter(mean(lon_surfER60_db(:,startlayer:endlayer,freq),2),mean(lat_surfER60_db(:,startlayer:endlayer,freq),2),20,mean(Sa_surfER60_db(:,startlayer:endlayer,freq),2));
text(mean(lon_surfER60_db((1:10:end),startlayer:endlayer,freq),2),mean(lat_surfER60_db((1:10:end),startlayer:endlayer,freq),2),datestr(time_ER60_db((1:10:end)),'dd/mm HH:MM'),'FontSize',6,'FontWeight','bold');
axis([min(lon_surfER60_db(:,1)) max(lon_surfER60_db(:,1)) min(lat_surfER60_db(:,1)) max(lat_surfER60_db(:,1))]);
title('Mean Sa for surface layers','FontSize',16);
colorbar;
xlabel('Longitude [degree]','FontSize',15);
ylabel('Latitude [degree]','FontSize',15);



%% import log file (Casino)
%Path

path_evt='//tl-nas2/mission/PELGAS13/Casino/csv/pelgas13_evts_Matlab.csv';
path_evt='M://PELGAS13/Casino/csv/pelgas13_evts_Matlab.csv';

C=import_casino(path_evt,1);

%use allstring=1 to avoid problems with casino changing headers

%% Merge EI and log data with cutlay_casino function
%Inputs:
%-------
%   C:          a casino output file
%   timeER60:   ESU time stamps (e.g. from EI results)
%   bps:        casino codes of sequences breakpoints to be selected
%   istarts     casino codes of sequences starts to be selected
%
%       e.g. to select only on-transect and inter-transect sequences (i.e. to exclude trawl hauls), use:
%
% bps=char('DEBURAD','REPRAD','INTERAD','STOPRAD','FINRAD');
% istarts=char('DEBURAD','REPRAD','INTERAD');

%       OR to select only on-transect sequences (excluding trawl hauls and inter-transects), use:
          bps=char('DEBURAD','REPRAD','STOPRAD','FINRAD');
          istarts=char('DEBURAD','REPRAD');

%       OR to select only trawl hauls sequences, use:
%           bps=char('DFIL','CULAB');
%           istarts=char('DFIL');
%       OR to select everything:
%           bps=char('ALL');
%           istarts=char('ALL');

[sdates3,stimes3,sevts3,rad_self] = cutlay_casino(C,time_ER60_db,bps,istarts);

%If the above does not work, try running it several times...











