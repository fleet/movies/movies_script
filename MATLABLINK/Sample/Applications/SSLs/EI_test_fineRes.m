%LB 02/04/2013 ajout r�cup�ration sondeur horizontal

%Clean workspace
clear all;

%% Set paths

pathHD='/media/mathieu/PELGAS2013_2/';

path_hac_survey=[pathHD,'HacPG13_Hermes/SmallScale'];

%path_config = [path_hac_survey,'/SSLs_1p_1ping/']
%path_config = [path_hac_survey,'/SSLs_1p_20m/']
%
path_config = [path_hac_survey,'/SSLs_1p_200m/']

path_save=[path_config,'/Result/']

%If save path does not exist, create it
if ~exist(path_save,'dir')
    mkdir(path_save)
end

%% Echo-integration
thresholds=[-80];
runs=[9];
nameTransect = ['Rad9test'];
dateStart = '04/05/2013'; timeStart = '07:00:00';
dateEnd = '04/05/2013'; timeEnd = '09:00:00';
batch_EI_multiTHR(path_hac_survey,path_config,path_save,thresholds,runs,nameTransect,dateStart,timeStart,dateEnd,timeEnd);

%% Bind EIlay results matrices
thresholds=[-80];
runs=[9];
survey_name='PELGAS13';
for ir=1:length(runs)
    for t=1:size(thresholds,2)
        str_run(ir,:)='000';        str_run(ir,end-length(num2str(runs(ir)))+1:end)=num2str(runs(ir));
        filename_ME70=[path_save,'/',survey_name,'-RUN' str_run(ir,:),'-TH' num2str(thresholds(t)),'-ME70-EIlay.mat'];
        filename_ER60=[path_save,'/',survey_name,'-RUN' str_run(ir,:),'-TH' num2str(thresholds(t)),'-ER60-EIlay.mat'];
        filename_ER60h=[path_save,'/',survey_name,'-RUN' str_run(ir,:),'-TH' num2str(thresholds(t)),'-ER60h-EIlay.mat'];
        path_results=[path_save,'/RUN' str_run(ir,:) '/',num2str(thresholds(:,t)),'/'];
        EIlayRes_bind(path_results,filename_ME70,filename_ER60,filename_ER60h);
    end
end

%% Section 3 - Visualization
fq = [18 38 70 120 200]; NbFreq = length(fq);
thresholds = [-80];
runs = [9];
survey_name = 'PELGAS13';

path_save_echog = path_save; %path to save the echograms

for ir = 1:length(runs)
    for t = 1:size(thresholds,2)
        str_run(ir,:) = '000'; str_run(ir,end-length(num2str(runs(ir)))+1:end) = num2str(runs(ir));
        datarun = load([path_save,'\',survey_name,'-RUN',str_run(ir,:),'-TH-80-ER60-EIlay.mat']);
        SV = datarun.Sv_surfER60_db;
        fieldnames(datarun);
        maxDepth=max(max(datarun.depth_bottom_ER60_db));
        %limdepth = [round(param.depthmin(ista)):round(param.depthmax(ista))];
        % Echogram
        f1 = figure(1); set(f1,'Units','Normalized','OuterPosition',[0 0 1 1]);
        for i = 1:NbFreq
            subplot (2,3,i)
            DrawEchoGram_brut (SV (:,1:(round(maxDepth)),i)',-45,-90);
%             set(gca,'YTickLabel',limdepth,'YTick',1:length(limdepth))
            title (['RUN',str_run(ir,:),' ',num2str(fq(i)),'kHz ',num2str(thresholds(t)),'dB'])   
        end
        subplot (2,3,6)
        colorbar ('location','southoutside') 
        % --- Save
%         saveas(figure(1),[path_save_echog,'RUN',str_run(ir,:),'_EI_Echogram'],'jpeg'); close(1);
    end
end

%% frequency response

ir = 1; t = 1;
str_run(ir,:) = '000'; str_run(ir,end-length(num2str(runs(ir)))+1:end) = num2str(runs(ir));
datarun = load([path_save,'\',survey_name,'-RUN',str_run(ir,:),'-TH-80-ER60-EIlay.mat']);
SV = datarun.Sv_surfER60_db; SV = SV(:,:,1:5);

repfreq = squeeze(10*log10(mean(mean(10.^(SV./10)))));
figure(2)
plot(repfreq,'k','LineWidth',2); set(gca,'XTickLabel',fq,'XTick',1:5,'FontSize',12)
xlabel('Frequencies [kHz]','FontSize',12); ylabel('Scattering Volume [dB]','FontSize',12);
title(['R',str_run,' Mean measured frequency response'],'FontSize',13);

saveas(figure(2),[path_save,'/R',str_run,'MeasuredRepFreq'],'jpeg')

%% Load bound EIlay results
thresholds=[-80];
runs=[9];
sels=[1];
str_run='000'; str_run(end-length(num2str(runs))+1:end)=num2str(runs);
% filename_ME70=[path_save,'/',survey_name,'-RUN' str_run,'-TH' num2str(thresholds),'-ME70-EIlay.mat'];
filename_ER60=[path_save,'/',survey_name,'-RUN' str_run,'-TH' num2str(thresholds),'-ER60-EIlay','.mat'];
% filename_ER60h=[path_save,'/',survey_name,'-RUN' str_run,'-TH' num2str(thresholds),'-ER60h-EIlay.mat'];
load(filename_ER60);
% load(filename_ER60h);

%% Exploratory plots
%choice for frequency
freq=1;

%Bottom detection (errors?)
figure;
plot(time_ER60_db,depth_bottom_ER60_db(:,1,freq))
datetick('x',15)
title('Bottom depth','FontSize',16);
xlabel('Time','FontSize',15);
ylabel('Bottom depth','FontSize',15);

% choice for surface layer
startlayer=1;
isregular=1;
maxDepth=max(max(datarun.depth_bottom_ER60_db));
endlayer=round(maxDepth); 
mSv_surfER60_db=mean(Sv_surfER60_db,3);
size(mSv_surfER60_db)
%Mean echogram
if isregular
    figure;imagesc(1:size(mSv_surfER60_db,1),depth_surface_ER60_db(1,startlayer:endlayer,freq),mSv_surfER60_db(:,1:endlayer)');colorbar;grid on
    DrawEchoGram_brut (mSv_surfER60_db(:,1:(round(maxDepth)))',-45,-90)
    hold on;
    plot(1:size(Sv_surfER60_db,1),depth_bottom_ER60_db(:,1,freq)-6,'r')
    title('Average echogram','FontSize',16)
    xlabel('ESU','FontSize',15);
    ylabel('Depth','FontSize',15);
else   
    figure;
    h0=pcolor(Sv_surfER60_db(:,startlayer:endlayer,freq)');colorbar;grid on
    set(h0,'EdgeColor','none','FaceColor','flat')
    title('Synthetic echogram','FontSize',16)
    xlabel('ESU','FontSize',15);
    ylabel('Layer','FontSize',15);
end



%Ship track and mean Sa
figure;
% coastline display with Matlab mapping toolbox
    landareas = shaperead('landareas.shp','UseGeoCoords', true);
 geoshow(landareas);
hold on;
scatter(mean(lon_surfER60_db(:,startlayer:endlayer,freq),2),mean(lat_surfER60_db(:,startlayer:endlayer,freq),2),20,mean(Sa_surfER60_db(:,startlayer:endlayer,freq),2));
text(mean(lon_surfER60_db((1:10:end),startlayer:endlayer,freq),2),mean(lat_surfER60_db((1:10:end),startlayer:endlayer,freq),2),datestr(time_ER60_db((1:10:end)),'dd/mm HH:MM'),'FontSize',6,'FontWeight','bold');
axis([min(lon_surfER60_db(:,1)) max(lon_surfER60_db(:,1)) min(lat_surfER60_db(:,1)) max(lat_surfER60_db(:,1))]);
title('Mean Sa for surface layers','FontSize',16);
colorbar;
xlabel('Longitude [degree]','FontSize',15);
ylabel('Latitude [degree]','FontSize',15);




















%% Exploratory plots
%choice for frequency
freq=2;

%Bottom detection (errors?)
figure;
plot(time_ER60_db,depth_bottom_ER60_db(:,freq))
datetick('x',15)
title('Bottom depth','FontSize',16);
xlabel('Time','FontSize',15);
ylabel('Bottom depth','FontSize',15);

% choice for surface layer
startlayer=1;
endlayer=200;
isregular=1;

%Synthetic echogram
if isregular
    figure;imagesc(time_ME70_db,depth_surface_ME70_db(1,startlayer:endlayer,freq),(squeeze(sum(Sa_surfME70_db(:,startlayer:endlayer,[28:32 32:36]),3))'));colorbar;grid on
    hold on;
    plot(time_ME70_db,depth_bottom_ME70_db(:,freq),'w','linewidth',3)
    title('Synthetic echogram mean NASC','FontSize',16)
    xlabel('ESU','FontSize',15);
    ylabel('Depth','FontSize',15);
    datetickzoom('x',15)
else   
    figure;
    h0=pcolor(Sv_surfER60h_db(:,startlayer:endlayer,freq)');colorbar;grid on
    set(h0,'EdgeColor','none','FaceColor','flat')
    title('Synthetic echogram','FontSize',16)
    xlabel('ESU','FontSize',15);
    ylabel('Layer','FontSize',15);
end



%Ship track and mean Sa

figure;
% coastline display with Matlab mapping toolbox
landareas = shaperead('landareas.shp','UseGeoCoords', true);
geoshow(landareas);
hold on
for freq=1:size(Sa_surfME70_db,3)
    scatter(mean(lon_surfME70_db(:,startlayer:endlayer,freq),2),mean(lat_surfME70_db(:,startlayer:endlayer,freq),2),20,mean(Sa_surfME70_db(:,startlayer:endlayer,freq),2));
    
end
text(mean(lon_surfME70_db((1:10:end),startlayer:endlayer,freq),2),mean(lat_surfME70_db((1:10:end),startlayer:endlayer,freq),2),datestr(time_ME70_db((1:10:end)),'dd/mm HH:MM'),'FontSize',6,'FontWeight','bold');
axis([min(lon_surfME70_db(:,1)) max(lon_surfME70_db(:,1)) min(lat_surfME70_db(:,1)) max(lat_surfME70_db(:,1))]);
title('Mean Sa for surface layers','FontSize',16);
colorbar;
xlabel('Longitude [degree]','FontSize',15);
ylabel('Latitude [degree]','FontSize',15);
% 
% 
% 
% %% import log file (Casino)
% %Path
% path_evt='L:/CLASS08/casino/CLASS08_evts.csv';
% 
% C=import_casino(path_evt);
% 
% %% Merge EI and log data with cutlay_casino function
% %Inputs:
% %-------
% %   C:          a casino output file
% %   timeER60:   ESU time stamps (e.g. from EI results)
% %   bps:        casino codes of sequences breakpoints to be selected
% %   istarts     casino codes of sequences starts to be selected
% %
% %       e.g. to select only on-transect and inter-transect sequences (i.e. to exclude trawl hauls), use:
% %
% bps=char('DEBURAD','REPRAD','INTERAD','STOPRAD','FINRAD');
% %
% istarts=char('DEBURAD','REPRAD','INTERAD');
% %       OR to select only on-transect sequences (excluding trawl hauls and inter-transects), use:
% %           bps=char('DEBURAD','REPRAD','STOPRAD','FINRAD')
% %           istarts=char('DEBURAD','REPRAD')
% %       OR to select only trawl hauls sequences, use:
% %           bps=char('DFIL','CULAB')
% %           istarts=char('DFIL')
% %       OR to select everything:
% %
% bps=char('ALL');
% %
% istarts=char('ALL');
% 
% [sdates3,stimes3,sevts3,rad_self] = cutlay_casino(C,time_ER60_db,bps,istarts);
% 
% %If the above does not work, try running it several times...








