% Classification des cellules
% ------------------------------------------------------------------------
clear all; close all

% ------------------------------------------------------------------------
% resultats EI
path_save = 'C:\Users\bremond\Documents\data\LargeScale\last_results\'; 
% resultats 1ere analyse
path_anal = 'C:\Users\bremond\Documents\data\LargeScale\last_results\results_analysis\'
% sauvegarde
pathsave = 'C:\Users\bremond\Documents\data\LargeScale\last_results\results_analysis\avec_l3\';
%pathsave=path_anal;
path_anal = '/media/mathieu/PELGAS2013_2/ReconstitutionRadiales/LargeScale/';
% sauvegarde
pathsave = '/media/mathieu/PELGAS2013_2/ReconstitutionRadiales/LargeScale/results';

rad = [1:24];% radiales
frequences = [18 38 70 120 200];


%% 2. Assemblage des radiales
SVraw = []; nbping = []; LAT=[]; LON=[]; DEPTH=[];

for i = 1:length(rad)
    i=17
    SVcoordTrans = []; svtrans=[];
    load ([path_save,'results_EI_GoToStop_corrected\PELGAS13-RAD',num2str(i),'-TH-80-ER60-EIlay.mat']);
    SV = Sv_surfER60_db;
    
    % lat et long
    lat = lat_surfER60_db;
    lon = lon_surfER60_db;
    LAT = [LAT;lat];
    LON = [LON;lon];
    
    % profondeur
    DEPTH = [DEPTH;depth_surface_ER60_db];
    
    % rangement des pings par ordre de longitude
    SVcoordSort=[]; 
    for f = 1:5
        SVcoord = [Sv_surfER60_db(:,:,f),squeeze(lon_surfER60_db(:,1,f))];
        SVcoordSort(:,:,f) = sortrows(SVcoord,size(SVcoord,2));
    end
    SVcoordSort = SVcoordSort (:,:,:);  
    SV = SVcoordSort(:,1:190,:); 
    SVraw = [SVraw;SV];
    
    % regroupement temps
%     time = time_ER60_db;
%     TIME = [TIME;time];
    
    % nombre de pings par radiale
    np = size(SV,1); 
    if i == 1
        p = np;
    else
        p = np + nbping(length(nbping));
    end
   nbping = [nbping;p] 
end

save([path_anal,'SVraw.mat'],'SVraw');
save([path_anal,'nbping.mat'],'nbping');
save([path_anal,'LAT.mat'],'LAT');
save([path_anal,'LON.mat'],'LON');
save([path_anal,'DEPTH.mat'],'DEPTH');
% ---
load([path_anal,'SVraw.mat']); load([path_anal,'nbping.mat']);
load([path_anal,'LAT.mat']); load([path_anal,'LON.mat']);
load([path_anal,'DEPTH.mat']);
%% ajout de la radiale nord du leg 3
  pl3=load([pathsave,'PELGAS13-RUN030-ER60-EIlay.mat']);

%  SVl3 = pl3.Sv_surfER60_db(:,:,1:5);
% size(SVl3)
% SVraw = [SVraw;SVl3];
% size(SVraw)
% nbping=[nbping;nbping(24)+size(SVl3,1)];
% 
% save([pathsave,'SVrawl3.mat'],'SVraw'); save([pathsave,'nbpingl3.mat'],'nbping');
 load([pathsave,'SVrawl3.mat']); load([pathsave,'nbpingl3.mat']);
%% 3. Standardisation des donn�es 
% par division de la valeur de chaque cellule � une fr�quence donn�e par 
% la valeur moyenne de la fr�quence
% SVstd=[]; standbasedmean=[]; 
% for f = 1:5
%     meanf = mean(mean(10.^(SVraw(:,:,f)./10)));
%     SVstdf = 10*log10((10.^(SVraw(:,:,f)./10))/meanf);
%     SVstd(:,:,f) = SVstdf;
%     standbasedmean = [standbasedmean;10*log10(meanf)];
% end
% % sauvegarde des objets
% save([pathsave,'StandBasedMeanl3.mat'],'standbasedmean'); % les moyennes globales par fr�quence
% save([pathsave,'SVstdl3.mat'],'SVstd'); 
% % ---
load([path_anal,'StandBasedMean.mat']); 
load([path_anal,'SVstd.mat']); 
load([pathsave,'StandBasedMeanl3.mat']); 
load([pathsave,'SVstdl3.mat']); 
% 
% SVstd = SVraw;
% % changement de structure des donn�es standardis�es
% SVlong = squeeze(reshape(SVstd,[],size(SVstd,3)));

%% 3bis. Standardisation en relatif � une fr�quence

vstd = SVraw(:,:,1);
Svstd=[];
for f = 1:5
    SVstd(:,:,f) = SVraw(:,:,f)-vstd;
end

% save
save([pathsave,'SVstdrelatif.mat'],'SVstd'); 
load([pathsave,'SVstdrelatif.mat']); 
 
% changement de structure 
SVlong = squeeze(reshape(SVstd,[],size(SVstd,3)));

%% 4. Analyse des correspondances multiples
% sur les donn�es standardis�es
% ---
% SVmax = max(max(SVlong))
% SVmin = min(min(SVlong))

% d�finition des classes et construction du tableau disjonctif complet
% ---------- EI config = 200m x 1m --------------
% icfq = [];
% for f = 1:5
%     i5fq = SVlong(:,f) < ceil(SVmax) & SVlong(:,f) >= -20;
%     i4fq = SVlong(:,f) < -20 & SVlong(:,f) >= -40;
%     i3fq = SVlong(:,f) < -40 & SVlong(:,f) >= -60;
%     i2fq = SVlong(:,f) < -60 & SVlong(:,f) >= -80;
%     i1fq = SVlong(:,f) < -80 & SVlong(:,f) >= floor(SVmin);
%     icfq = [i5fq i4fq i3fq i2fq i1fq icfq];
% end
% save([pathsave,'TablDisjComp.mat'],'icfq');
% ---------- EI config = 200m x 1m (donn�es centr�es) --------------
% icfq = [];
% for f = 1:5
%     i5fq = SVlong(:,f) < ceil(SVmax) & SVlong(:,f) >= 40;
%     i4fq = SVlong(:,f) < 40 & SVlong(:,f) >= 20;
%     i3fq = SVlong(:,f) < 20 & SVlong(:,f) >= 0;
%     i2fq = SVlong(:,f) < 0 & SVlong(:,f) >= -20;
%     i1fq = SVlong(:,f) < -20 & SVlong(:,f) >= floor(SVmin);
%     icfq = [i5fq i4fq i3fq i2fq i1fq icfq];
% end
% save([pathsave,'TablDisjCompl3.mat'],'icfq');
% % ---
load([path_anal,'TablDisjComp.mat']);
load([pathsave,'TablDisjCompl3.mat']);
% 
% 
% 
% % nom des variables
vlab = {'18_5','18_4','18_3','18_2','18_1','38_5','38_4','38_3','38_2','38_1',...
    '70_5','70_4','70_3','70_2','70_1','120_5','120_4','120_3','120_2','120_1',...
    '200_5','200_4','200_3','200_2','200_1'};

%% 4bis. classes sur SV standardis�s en relatif

SVmax = max(max(SVlong))
SVmin = min(min(SVlong))

% cas une radiale
% icfq = [];
% for f = 1:5
% 
%     % 18khz 
%     i5fq = SVlong(:,f) < ceil(SVmax) & SVlong(:,f) >= 50;
%     i4fq = SVlong(:,f) < 50 & SVlong(:,f) >= 21;
%     i3fq = SVlong(:,f) < 21 & SVlong(:,f) >= -8;
%     i2fq = SVlong(:,f) < -8 & SVlong(:,f) >= -37;
%     i1fq = SVlong(:,f) < -37 & SVlong(:,f) >= floor(SVmin);
%     icfq = [i5fq i4fq i3fq i2fq i1fq icfq];
%    
    % 38khz
%     i5fq = SVlong(:,f) < ceil(SVmax) & SVlong(:,f) >= 48;
%     i4fq = SVlong(:,f) < 48 & SVlong(:,f) >= 19;
%     i3fq = SVlong(:,f) < 19 & SVlong(:,f) >= -10;
%     i2fq = SVlong(:,f) < -10 & SVlong(:,f) >= -39;
%     i1fq = SVlong(:,f) < -39 & SVlong(:,f) >= floor(SVmin);
%     icfq = [i5fq i4fq i3fq i2fq i1fq icfq];
% 
    % 120 khz
%     i5fq = SVlong(:,f) < ceil(SVmax) & SVlong(:,f) >= 55;
%     i4fq = SVlong(:,f) < 55 & SVlong(:,f) >= 16;
%     i3fq = SVlong(:,f) < 16 & SVlong(:,f) >= -23;
%     i2fq = SVlong(:,f) < -23 & SVlong(:,f) >= -62;
%     i1fq = SVlong(:,f) < -62 & SVlong(:,f) >= floor(SVmin);
%     icfq = [i5fq i4fq i3fq i2fq i1fq icfq];
% end

icfq = [];
for f = 1:5

    % 18khz & 38khz
    i5fq = SVlong(:,f) < ceil(SVmax) & SVlong(:,f) >= 64;
    i4fq = SVlong(:,f) < 64 & SVlong(:,f) >= 29;
    i3fq = SVlong(:,f) < 29 & SVlong(:,f) >= -6;
    i2fq = SVlong(:,f) < -6 & SVlong(:,f) >= -41;
    i1fq = SVlong(:,f) < -41 & SVlong(:,f) >= floor(SVmin);
    icfq = [i5fq i4fq i3fq i2fq i1fq icfq];
% 
    % 120 khz
%     i5fq = SVlong(:,f) < ceil(SVmax) & SVlong(:,f) >= 55;
%     i4fq = SVlong(:,f) < 55 & SVlong(:,f) >= 16;
%     i3fq = SVlong(:,f) < 16 & SVlong(:,f) >= -23;
%     i2fq = SVlong(:,f) < -23 & SVlong(:,f) >= -62;
%     i1fq = SVlong(:,f) < -62 & SVlong(:,f) >= floor(SVmin);
%     icfq = [i5fq i4fq i3fq i2fq i1fq icfq];
end
%save
save([pathsave,'TablDisjComprelatif.mat'],'icfq');
load([pathsave,'TablDisjComprelatif.mat']);

% nom des variables
vlab = {'18_5','18_4','18_3','18_2','18_1','38_5','38_4','38_3','38_2','38_1',...
    '70_5','70_4','70_3','70_2','70_1','120_5','120_4','120_3','120_2','120_1',...
    '200_5','200_4','200_3','200_2','200_1'};



%% 5. Analyse en Composantes Principales 
% sur le tableau disjonctif complet
[coeff,score,latent] = princomp(icfq);
save([pathsave,'resPCA.mat'],'coeff','score','latent');
% ---
load([path_anal,'resPCA.mat']);
load([pathsave,'resPCA.mat']);

% Variance
Var = 100*latent ./ sum(latent); VarCum = cumsum(Var);
hold on; bar(Var); plot(VarCum, '-or','LineWidth',2)
legend({'Var. par PC' 'Var. cumul�e'},4); legend boxoff
xlabel('Composantes Principales'); ylabel('Variance expliqu�e [%]')
saveas(figure(1),[pathsave,'varPCA18'],'jpeg'); close(1)

% biplot (projection des variables et des individus dans l'espace des
% composantes principales)

% Le nombre de cellules (= d'individus) �tant trop important pour la visualisation
% graphique, s�lection al�atoire de 10000 individus projet�s dans l'espace
x = score(:,1); n = 10000;
nx = numel(x); inx = randperm(nx);
pc1 = score(inx(1:n),1);pc2 = score(inx(1:n),2);
pc3 = score(inx(1:n),3);pc4 = score(inx(1:n),4);

%% projection des individus/variables dans espace factoriel

% nuage du cluster 
sel1 = IDX == 1;
sel2 = IDX == 2;
sel3 = IDX == 3;
sel4 = IDX == 4;
sel5 = IDX == 5;
sel6 = IDX == 6;
score1 = score(sel1,:);
score2 = score(sel2,:);
score3 = score(sel3,:);
score4 = score(sel4,:);
score5 = score(sel5,:);
score6 = score(sel6,:);

% extraction des points extr�mes
x1 = convhull(score1(:,1),score1(:,2));
x2 = convhull(score2(:,1),score2(:,2));
x3 = convhull(score3(:,1),score3(:,2));
x4 = convhull(score4(:,1),score4(:,2));
x5 = convhull(score5(:,1),score5(:,2));
x6 = convhull(score6(:,1),score6(:,2));

% x1 = convhull(score1(:,2),score1(:,3));
% x2 = convhull(score2(:,2),score2(:,3));
% x3 = convhull(score3(:,2),score3(:,3));
% x4 = convhull(score4(:,2),score4(:,3));
% x5 = convhull(score5(:,2),score5(:,3));
% x6 = convhull(score6(:,2),score6(:,3));

% projection des polygones dans l'espace
figure();hold on;
% projection des polygones
%fill(score1(x1,1),score1(x1,2),'r-','FaceAlpha',0.6)
fill(score2(x2,1),score2(x2,2),'y-','FaceAlpha',0.6)
fill(score3(x3,1),score3(x3,2),'g-','FaceAlpha',0.6)
fill(score4(x4,1),score4(x4,2),'c-','FaceAlpha',0.6)
fill(score5(x5,1),score5(x5,2),'b-','FaceAlpha',0.6)
fill(score6(x6,1),score6(x6,2),'m-','FaceAlpha',0.6)
% projection des variables
biplot(5*coeff(:,1:2),'Varlabel',vlab)
% projection du rep�re
plot(repmat(0,[1,7]),[-3:3],'-k'); plot([-3:3],repmat(0,[1,7]),'-k');
xlabel(['CP 1 (',num2str(round(Var(1))),'%)'],'FontSize',13);
ylabel(['CP 2 (',num2str(round(Var(2))),'%)'],'FontSize',13);
legend('groupe 2','groupe 3','groupe 4','groupe 5','groupe 6',4)
legend boxoff


%---------------------
% pour une EI � grande �chelle, visualisation de tous les individus
% pc1 = score(:,1);pc2 = score(:,2);
% pc3 = score(:,3);pc4 = score(:,4);

% f1 = figure(1); set(f1,'Units','Normalized','OuterPosition',[0 0 1 1]);
% subplot(1,2,1); biplot(coeff(:,1:2),'Scores',[pc1,pc2],...
%     'VarLabels',vlab)
% xlabel (['PC1 - ',num2str(round(Var(1))),'%'])
% ylabel (['PC2 - ',num2str(round(Var(2))),'%'])
% title('10000 random scores')
% subplot(1,2,2); biplot(coeff(:,3:4),'Scores',[pc3,pc4],...
%     'VarLabels',vlab)
% xlabel (['PC3 - ',num2str(round(Var(3))),'%'])
% ylabel (['PC4 - ',num2str(round(Var(4))),'%'])                    
% saveas(figure(1),[path_anal,'PC1-2_PC3-4'],'jpeg'); close(1)
% 
%% 6. Kmeans clustering
% ---
X = score(:,1:6); % on ne prend que les 6 premi�res CP pour le clustering
k = 6;              % nombre de clusters

% algorithme Kmeans (�a peut �tre long...)
opt = statset('Display','final','MaxIter',100); 
[IDX,C,sumd] = kmeans(X,k,'Replicates',250,'Options',opt,'start','cluster','emptyaction','singleton'); 
save([pathsave,'facteurs_',num2str(k),'clusters38-250iter'],'IDX','C','sumd');
% ---
load([pathsave,'facteurs_6clustersl3']);
load([path_anal,'facteurs_',num2str(k),'clusters']);

it=800; j=2;
load([pathsave,'facteurs_',num2str(k),'clusters-',num2str(it),'iter-n',num2str(j)]);

% test nombre iterations
% for it = [300,400,500,800]
%     [IDX,C,sumd] = kmeans(X,k,'Replicates',it,'Options',opt,'start','cluster','emptyaction','singleton'); 
%     save([pathsave,'facteurs_',num2str(k),'clusters-',num2str(it),'iter-n2'],'IDX','C','sumd');
% end

% changement de structure des facteurs 
idx_wide = squeeze(reshape(IDX,size(SVraw,1),[],size(SVraw,2)));


%% cr�ation du cluster 0 (cellules du fond)
% profondeur maximale
% Dmax=[];
% for r=1:24
%     r%=2
%     if r==1
%         i1=1; i2=nbping(1);
%     else 
%         i1=nbping(r-1)+1; i2=nbping(r);
%     end
%     
%     idxr=idx_wide(i1:i2,:);
%     latr=LAT(i1:i2,1,1);
%     lonr=LON(i1:i2,1,1);
%     depthr=DEPTH(1,:,1);
%     
%     dmax=[]; 
%     for i=1:size(idxr,1) %boucle sur les esdus
%         %i=68
%         for j=190:-1:1
%             if idxr(i,j) ~= 1
%                 dmax = [dmax;j];
%                 break
%             end
%             if j == 1
%                 if idxr(i,j) ==1
%                     dmax=[dmax;1];
%                 end
%             end
%         end
%     end
%     Dmax=[Dmax;dmax];
% end
save([path_anal,'Dmax.mat'],'Dmax')
load([path_anal,'Dmax.mat'])

%test rad17
i1=nbping(17-1)+1;
i2=nbping(17);
Dmax=Dmax(i1:i2);

% cr�ation du cluster 0 (=cellules sous la ligne de fond)
newidx=[];
for i = 1:size(idx_wide,1)
    i
    dmaxi = Dmax(i)+1;
    idxi = idx_wide(i,:);
    idxi(dmaxi:190) = 7;
    newidx = [newidx;idxi];
end
%save
save([path_anal,'NEWIDX.mat'],'newidx')
load([path_anal,'NEWIDX.mat'])

save([pathsave,'NEWIDX718_250iter.mat'],'newidx')
load([pathsave,'NEWIDX7.mat'])




%% 7. Calcul des r�ponses fr�quentielles m�dianes par cluster
% ---
MDCLU = [];MCLU = [];
for clu = 1:6
    clu
    MDF = [];     MF = []; 
    for f = 1:length(frequences)
        svrawf = 10.^(SVraw(:,:,f)./10);      % sans le c0
        mdf = 10*log10(median(svrawf(IDX == clu)));    
        mf = 10*log10(mean(svrawf(IDX == clu)));    
% 
%         svrawf = 10.^(SVraw(:,:,f)./10);        % avec le cluster 0
%         mdf = 10*log10(median(svrawf(newidx == clu)));    
%         mf = 10*log10(mean(svrawf(newidx == clu)));    
       
        MDF = [MDF,mdf];        MF = [MF,mf];
    end
    MDCLU = [MDCLU;MDF];     MCLU = [MCLU;MF]; 
end
% save
save([pathsave,'RF_med_',num2str(k),'clust-',num2str(it),'iter-n',num2str(j)],'MDCLU');
save([pathsave,'RF_mean_',num2str(k),'clust-',num2str(it),'iter-n',num2str(j)],'MCLU');
% save([path_anal,'RF_med_',num2str(k),'clust'],'MDCLU');
% save([path_anal,'RF_mean_',num2str(k),'clust'],'MCLU');

load([pathsave,'RF_med_',num2str(k),'clust7-2']);
load([pathsave,'RF_mean_',num2str(k),'clust7-2']);
load([path_anal,'RepFreqMedianeByClust_',num2str(k),'clust']);
% load([pathsave,'RepFreqMedianeByClust_',num2str(k),'clustl3']);


%% 8. Repr�sentation graphique de la classification des cellules
% --
rad = [1:24]; % radiale 25 = leg3
for r = rad
   %r=17
    % donn�es de la radiale r
    if r == 1
        %idxrad = idx_wide(1:nbping(r),:,:);
        idxrad = newidx (1:nbping(r),:,:);
        svrawrad = 10.^(SVraw(1:nbping(r),:,:)./10);
        SVrawrad = SVraw(1:nbping(r),:,:);
    else
         idxrad = idx_wide((nbping(r-1)+1):nbping(r),:,:);
        %idxrad = newidx((nbping(r-1)+1):nbping(r),:,:);
        svrawrad = 10.^(SVraw((nbping(r-1)+1):nbping(r),:,:)./10);
        SVrawrad = SVraw((nbping(r-1)+1):nbping(r),:,:);
    end
    
    %test rad17
    r=17;
    idxrad=newidx;
    SVrawrad=SVraw;
    
    % longitude pour axes x sur echog
    lon = pl3.lon_surfER60_db(:,1,1);

    p17=load ([path_save,'results_EI_GoToStop_corrected\PELGAS13-RAD17-TH-80-ER60-EIlay.mat']);
    lon = p17.lon_surfER60_db(:,1,1);

    
    % figure
     f = figure(2); set(f,'Units','Normalized','OuterPosition',[0 0 1 1]); 
    
     % echogrammes aux 5 fr�quences
    for f = 1:5
        subplot(1,5,f); 
        f=5
        DrawEchoGram_brut(SVrawrad(:,1:150,f)',-40,-100);
        title(['Radiale ',num2str(r),' - ',num2str(frequences(f)),' kHz'],'FontSize',11);
        set(gca,'XTickLabel',round(10*lon(1:round(length(lon)/11):length(lon)))/10,'FontSize',10)
        xlabel('Longitude');
        freezeColors
    end

    % r�ponses fr�quentielles m�dianes par cluster
    c = colormap(HSV(6)); 
    txtleg = []; txtcolbar = []; 

    % classification des cellules
    %subplot(3,3,8:9); 
    imagesc(idxrad(:,1:150)');
    colorbar ('location','eastoutside','YTick',[1:6],'YTickLabel',...
        {'clust.1','clust.2','clust.3','clust.4','clust.5','clust.6'}) 
    title(['Radiale ',num2str(r),' - classification des cellules'],'FontSize',12);
            set(gca,'XTickLabel',round(10*lon(1:round(length(lon)/11):length(lon)))/10,'FontSize',10)
        xlabel('Longitude');


%     saveas(figure(2),[pathsave,'RAD',num2str(r),'_ResultatFinal_',num2str(k),'clust'],'fig');
%     close(2)
% end

% figure des r�ponses fr�quentielles
%subplot(3,3,7); c = colormap(Jet(7)); hold on
txtleg=[]; figure(); c = colormap(HSV(6)); 
hold on
for j = 1:6
    plot(MDCLU(j,1:size(MDCLU,2))','Color',c(j,:),'LineWidth',2)
%     if j ~= 7
        txtleg = [txtleg {['cluster ',num2str(j)]}];    % texte pour la l�gende
%     else
%         txtleg = [txtleg {['no data']}];    % texte pour la l�gende
%     end 
end
set(gca,'XTickLabel',{'18','38','70','120','200'},'XTick',1:5,'FontSize',11)
title('R�ponses fr�quentielles m�dianes par cluster','FontSize',11);
xlabel('Fr�quences [kHz]'); ylabel('Intensit� acoustique [dB]');
 legend(txtleg,4); legend boxoff

subplot(3,3,6); hold on
%figure(); hold on
for j = 1:k+1
    plot(MCLU(j,1:size(MCLU,2))','Color',c(j,:),'LineWidth',2)
    txtleg = [txtleg {['cluster ',num2str(j)]}];    % texte pour la l�gende
end
set(gca,'XTickLabel',{'18','38','70','120','200'},'XTick',1:5,'FontSize',11)
title('R�ponses fr�quentielles moyennes par cluster','FontSize',11);
xlabel('Fr�quences [kHz]'); ylabel('Intensit� acoustique [dB]');
% legend(txtleg,4); legend boxoff

%% distribution sv

load([pathsave,'facteurs_',num2str(k),'clusters']);

freq = [18 38 70 120 200];
cluster = [1 2 3 4 5 6]
figure; i = 1;
for clu = 1:6
    %clu=1
    for f = 1:5
        
        SVrawf = SVraw(:,:,f);
        selclu = newidx == clu;
        SVrawfc = SVrawf(selclu);
        
        medfc = 10*log10(median(10.^(SVrawfc./10)));
        
        subplot(6,5,i); hold on
        [n,xout]=hist(SVrawfc,50); 
        b=bar(xout,n)
        ylim = get(gca,'ylim');
        line([medfc,medfc],ylim,'Color','r','LineWidth',2);
        
        i = i+1;
    end
end
legend('SV','median',1); legend boxoff;

subplot(6,5,1); title('18 kHz','FontSize',12); ylabel('groupe 1','FontSize',12)
subplot(6,5,2); title('38 kHz','FontSize',12); 
subplot(6,5,3); title('70 kHz','FontSize',12); 
subplot(6,5,4); title('120 kHz','FontSize',12); 
subplot(6,5,5); title('200 kHz','FontSize',12); 
subplot(6,5,6); ylabel('groupe 2','FontSize',12)
subplot(6,5,11); ylabel('groupe 3','FontSize',12)
subplot(6,5,16); ylabel('groupe 4','FontSize',12)
subplot(6,5,21); ylabel('groupe 5','FontSize',12)
subplot(6,5,26); ylabel('groupe 6','FontSize',12)


%% box plot
path_anal = 'C:\Users\bremond\Documents\data\LargeScale\last_results\results_analysis\'
load([path_anal,'SVraw.mat']);
load([path_anal,'facteurs_',num2str(6),'clusters']);

path_anal = 'C:\Users\bremond\Documents\data\LargeScale\last_results\results_analysis\avec_l3\'
load([path_anal,'SVrawl3.mat']);
load([path_anal,'facteurs_6clustersl3.mat']);


SVrawLong = squeeze(reshape(SVraw,[],size(SVraw,3)));
figure()
for clu = 1:6
    MDF = [];
    SVrawLongs=SVrawLong(IDX == clu,:);
    subplot(2,3,clu);
    boxplot(SVrawLongs); xlabel('Fr�quences (kHz)','FontSize',12)
    ylabel('Sv (dB)','FontSize',12)
    set(gca,'XTick',[1:5],'XTickLabel',{'18','38','70','120','200'})
    ylim([-100,0])
    title(['Groupe ',num2str(clu)],'FontSize',13)

end

% barre plot du nombre de cellules par groupe
nbcell = [sum(IDX==2),sum(IDX==3),sum(IDX==4),sum(IDX==5),sum(IDX==6)];
bar(nbcell,'k'); xlabel('Groupes'); ylabel('Nombre de cellules')

nbtot=sum(nbcell)
prcell=round(nbcell*100/nbtot)



