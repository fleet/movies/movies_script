% Analyse des �chos correspondants aux couches diffusantes selon leurs 
% r�ponses fr�quentielles en prenant en compte toutes les radiales d'une 
% campagne.
% ------------------------------------------------------------------------
% ------------------------------------------------------------------------
% Nettoyage de l'historique de la console
clear all; close all

% ------------------------------------------------------------------------
% Param�tres d'entr�e pour l'�cho-int�gration.
% EI par radiale selon les bornes des fractions de prospection acoustiques
% d�finies dans le fichier inputsEIGotoStop.csv
%% Set paths

for scal=1:1

pathHD='/media/mathieu/PELGAS2013_2/';

path_hac_survey=[pathHD,'HacPG13_Hermes/SmallScale'];
%
path_config1 = [path_hac_survey,'/SSLs_1p_1ping'];
path_config2 = [path_hac_survey,'/SSLs_1p_20m/']
path_config3 = [path_hac_survey,'/SSLs_1p_200m/']

path_configs={path_config1,path_config2,path_config3};

path_config=path_configs{scal};

path_save=[path_config,'/Result/']
%If save path does not exist, create it
if ~exist(path_save,'dir')
    mkdir(path_save)
end

path_results = path_save;
thresholds = [-80];
frequences = [18 38 70 120 200];


%% 1. Echo-int�gration et assemblage des r�sultats par radiale
% ---
runs=[9]
rad=[9]
survey_name='PELGAS13'

%% 2. Assemblage des donn�es de toutes les radiales pour l'analyse
% ---
% path_anal = 'C:\Users\bremond\Documents\data\LargeScale\Approche5_StandCorrespAnalysis\wholedataset\results_analysis\'
%path_anal = 'C:\Users\bremond\Documents\data\LargeScale\last_results\results_analysis\'
path_anal=path_save;

SVraw = []; nbping = []; 
for i = 1:length(rad)
    SVcoordTrans = []; svtrans=[];
    str_run(i,:) = '000'; str_run(i,end-length(num2str(runs(1)))+1:end) = num2str(runs(1));
    load([path_save,'/',survey_name,'-RUN',str_run(i,:),'-TH-80-ER60-EIlay.mat']);
    %load ([path_save,'PELGAS13-RAD',num2str(i),'-TH-80-ER60-EIlay.mat']);
    SV = Sv_surfER60_db;
    size(SV)    
    % rangement des pings par ordre de longitude
    SVcoordSort=[]; 
    for f = 1:5
        SVcoord = [Sv_surfER60_db(:,:,f),squeeze(lon_surfER60_db(:,1,f))];
        SVcoordSort(:,:,f) = sortrows(SVcoord,size(SVcoord,2));
    end
    SVcoordSort = SVcoordSort (:,:,:);  
    SV = SVcoordSort(:,1:190,:); 
    SVraw = [SVraw;SV];
    maxDepth=round(max(max(depth_bottom_ER60_db)));
    % regroupement temps
%     time = time_ER60_db;
%     TIME = [TIME;time];
    
    % nombre de pings par radiale
    np = size(SV,1); 
    if i == 1
        p = np;
    else
        p = np + nbping(length(nbping));
    end
    nbping = [nbping;p] 
end
% save([path_anal,'SVraw.mat'],'SVraw'); 
% save([path_anal,'nbping.mat'],'nbping')
% load([path_anal,'SVraw.mat']); load([path_anal,'nbping.mat']);

%for r = rad
    
    % donn�es de la radiale r
    %if r == 1
%         svrawrad = 10.^(SVraw(1:nbping(r),:,:)./10);
%         SVrawrad = SVraw(1:nbping(r),:,:);
        svrawrad = 10.^(SVraw(:,:,:)./10);
        SVrawrad = SVraw(:,:,:);
%     else
%         svrawrad = 10.^(SVraw((nbping(r-1)+1):nbping(r),:,:)./10);
%         SVrawrad = SVraw((nbping(r-1)+1):nbping(r),:,:);
%     end
%     
    % figure large scale
    close all
    
    meanEchog=mean(SVrawrad(:,1:maxDepth,:),3);
    size(meanEchog)
    
    maxDepth=round(max(max(depth_bottom_ER60_db)));
    f1 = figure(1); set(f1,'Units','Normalized','OuterPosition',[0 0 1 1]); 
    % echogrammes aux 5 fr�quences
    for f = 1:5
        subplot(3,2,f); DrawEchoGram_brut(SVrawrad(:,1:maxDepth,f)',-45,-90);
        title([num2str(frequences(f)),' kHz'],'FontSize',11);
        freezeColors
    end
    subplot (3,2,6)
        DrawEchoGram_brut(meanEchog(:,1:maxDepth)',-45,-90);
        title(['Mean echogram'],'FontSize',11);
        freezeColors
    colorbar()
    
    saveas(figure(1),[path_save,'MediumScale-Echograms'],'jpeg');
    saveas(figure(1),[path_save,'MediumScale-Echograms'],'fig');

   % figure small scale 
   
   if scal==1
       pstart=6750;
       pend=7000;
   elseif scal==2
       pstart=round(6750*902/7146);
       pend=round(7000*902/7146);
   elseif scal==3
       pstart=round(6750*90/7146);
       pend=round(7000*90/7146);
   end
    
    maxDepth=55;
    f2 = figure(2); set(f2,'Units','Normalized','OuterPosition',[0 0 1 1]); 
    % echogrammes aux 5 fr�quences
    for f = 1:5
        subplot(3,2,f); DrawEchoGram_brut(SVrawrad(pstart:pend,1:maxDepth,f)',-45,-90);
        title([num2str(frequences(f)),' kHz'],'FontSize',11);
        freezeColors
    end
    subplot (3,2,6)
        DrawEchoGram_brut(meanEchog(pstart:pend,1:maxDepth)',-45,-90);
        title(['Mean echogram'],'FontSize',11);
        freezeColors
    colorbar()
    
    saveas(figure(2),[path_save,'SmallScale-Echograms'],'jpeg');
    saveas(figure(2),[path_save,'SmallScale-Echograms'],'fig');
    

%% 1. Analyse sur donnees petite échelle
   
(max(time_ER60_db(pstart:pend))-min(time_ER60_db(pstart:pend)))*24*60
 SVraws2=SVraw(pstart:pend,:,:);
 maxDepth=55;
 prefix='SmallScaleClusters';
 frequences = [18 38 70 120 200];
 
% Ballon
close all
[MDCLU1,BallonIndic]=BallonIndicator(SVraws2,frequences,prefix,maxDepth,path_anal);

% Trenkel&Berger
close all
[delta_ER60f,mfriClust]=mfrIndicator(SVraws2,frequences,prefix,maxDepth,path_anal);
 
% Unsupervised, 3 clusters
%----------------
 silhou=false;
 prefix='SmallScale3clusters'
 
 %Kmeans
 close all
 [IDX1ss3,C1ss3,sumd1ss3,MDCLU1ss3] = kmeanAcouClass(SVraws2,3,path_anal,prefix,maxDepth,frequences,silhou);
  
% classification by multiple correspondences
close all
[coeff,score,latent,IDX,idx_wide,MDCLU]=CMA_PCA_kmeanClass(SVraws2,path_anal,3,silhou,frequences,maxDepth,prefix);
    
% Unsupervised, 4 clusters

 prefix='SmallScale4clusters'
 
 %Kmeans
 close all
 [IDX1ss4,C1ss4,sumd1ss4,MDCLU1ss4] = kmeanAcouClass(SVraws2,4,path_anal,prefix,maxDepth,frequences,silhou);
  
% classification by multiple correspondences
close all
[coeffss4,scoress4,latentss4,IDXss4,idx_widess4,MDCLUss4]=CMA_PCA_kmeanClass(SVraws2,path_anal,4,silhou,frequences,maxDepth,prefix);
    
% Unsupervised, 5 clusters

 prefix='SmallScale5clusters'
 
 %Kmeans
 close all
 [IDX1ss5,C1ss5,sumd1ss5,MDCLU1ss5] = kmeanAcouClass(SVraws2,5,path_anal,prefix,maxDepth,frequences,silhou);
  
% classification by multiple correspondences
close all
[coeffss5,scoress5,latentss5,IDXss5,idx_widess5,MDCLUss5]=CMA_PCA_kmeanClass(SVraws2,path_anal,5,silhou,frequences,maxDepth,prefix);
    
% Unsupervised, 6 clusters

 prefix='SmallScale6clusters'
 
 %Kmeans
 close all
 [IDX1ss6,C1ss6,sumd1ss6,MDCLU1ss6] = kmeanAcouClass(SVraws2,6,path_anal,prefix,maxDepth,frequences,silhou);
  
% classification by multiple correspondences
close all
[coeffss6,scoress6,latentss6,IDXss6,idx_widess6,MDCLUss6]=CMA_PCA_kmeanClass(SVraws2,path_anal,6,silhou,frequences,maxDepth,prefix);
  
% Unsupervised, 7 clusters

 prefix='SmallScale7clusters'
 
 %Kmeans
 close all
 [IDX1ss7,C1ss7,sumd1ss7,MDCLU1ss7] = kmeanAcouClass(SVraws2,7,path_anal,prefix,maxDepth,frequences,silhou);
  
% classification by multiple correspondences
close all
[coeffss7,scoress7,latentss7,IDXss7,idx_widess7,MDCLUss7]=CMA_PCA_kmeanClass(SVraws2,path_anal,7,silhou,frequences,maxDepth,prefix);
  

%% 2. Analyse sur donnees a moyenne echelle
 
(max(time_ER60_db)-min(time_ER60_db))*24*60
 maxDepth=max(max(depth_bottom_ER60_db));
 prefix='MediumScaleClusters';
 frequences = [18 38 70 120 200];
 
% Ballon
close all
[MDCLU1ms,BallonIndicms]=BallonIndicator(SVraw,frequences,prefix,maxDepth,path_anal);

% Trenkel&Berger
close all
[delta_ER60fms,mfriClustms]=mfrIndicator(SVraw,frequences,prefix,maxDepth,path_anal);
 
% Unsupervised, 3 clusters
%----------------
 silhou=false;
 prefix='MediumScale3clusters'
 
 %Kmeans
 close all
 [IDX1ms3,C1ms3,sumd1ss3,MDCLU1ss3] = kmeanAcouClass(SVraw,3,path_anal,prefix,maxDepth,frequences,silhou);
  
% classification by multiple correspondences
close all
[coeffms3,scorems3,latentms3,IDXms3,idx_widems3,MDCLUms3]=CMA_PCA_kmeanClass(SVraw,path_anal,3,silhou,frequences,maxDepth,prefix);
    
% Unsupervised, 4 clusters

 prefix='MediumScale4clusters'
 
 %Kmeans
 close all
 [IDX1ms4,C1ms4,sumd1ms4,MDCLU1ms4] = kmeanAcouClass(SVraw,4,path_anal,prefix,maxDepth,frequences,silhou);
  
% classification by multiple correspondences
close all
[coeffms4,scorems4,latentms4,IDXms4,idx_widems4,MDCLUms4]=CMA_PCA_kmeanClass(SVraw,path_anal,4,silhou,frequences,maxDepth,prefix);
    
% Unsupervised, 5 clusters

 prefix='MediumScale5clusters'
 
 %Kmeans
 close all
 [IDX1ms5,C1ms5,sumd1ms5,MDCLU1ms5] = kmeanAcouClass(SVraw,5,path_anal,prefix,maxDepth,frequences,silhou);
  
% classification by multiple correspondences
close all
[coeffms5,scorems5,latentms5,IDXms5,idx_widems5,MDCLUms5]=CMA_PCA_kmeanClass(SVraw,path_anal,5,silhou,frequences,maxDepth,prefix);
    
% Unsupervised, 6 clusters

 prefix='MediumScale6clusters'
 
 %Kmeans
 close all
 [IDX1ms6,C1ms6,sumd1ms6,MDCLU1ms6] = kmeanAcouClass(SVraw,6,path_anal,prefix,maxDepth,frequences,silhou);
  
% classification by multiple correspondences
close all
[coeffms6,scorems6,latentms6,IDXms6,idx_widess6,MDCLUss6]=CMA_PCA_kmeanClass(SVraw,path_anal,6,silhou,frequences,maxDepth,prefix);
  
% Unsupervised, 7 clusters

 prefix='MediumScale7clusters'
 
 %Kmeans
 close all
 [IDX1ms7,C1ss7,sumd1ms7,MDCLU1ms7] = kmeanAcouClass(SVraw,7,path_anal,prefix,maxDepth,frequences,silhou);
  
% classification by multiple correspondences
close all
[coeffms7,scorems7,latentms7,IDXms7,idx_widems7,MDCLUms7]=CMA_PCA_kmeanClass(SVraw,path_anal,7,silhou,frequences,maxDepth,prefix);
          
end

