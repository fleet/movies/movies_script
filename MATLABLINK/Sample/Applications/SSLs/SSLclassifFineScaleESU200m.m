% Analyse des �chos correspondants aux couches diffusantes selon leurs 
% r�ponses fr�quentielles en prenant en compte toutes les radiales d'une 
% campagne.
% ------------------------------------------------------------------------
% ------------------------------------------------------------------------
% Nettoyage de l'historique de la console
clear all; close all

% ------------------------------------------------------------------------
% Param�tres d'entr�e pour l'�cho-int�gration.
% EI par radiale selon les bornes des fractions de prospection acoustiques
% d�finies dans le fichier inputsEIGotoStop.csv
%% Set paths

pathHD='/media/mathieu/PELGAS2013_2/';

path_hac_survey=[pathHD,'HacPG13_Hermes/SmallScale'];
%path_config = [path_hac_survey,'/SSLs_1p_1ping'];
%
path_config = [path_hac_survey,'/SSLs_1p_200m/']
%path_config = [path_hac_survey,'/SSLs_1p_200m/']

path_save=[path_config,'/Result/']
%If save path does not exist, create it
if ~exist(path_save,'dir')
    mkdir(path_save)
end

path_results = path_save;
thresholds = [-80];
frequences = [18 38 70 120 200];


%% 1. Echo-int�gration et assemblage des r�sultats par radiale
% ---
runs=[9]
rad=[9]
survey_name='PELGAS13'

%% 2. Assemblage des donn�es de toutes les radiales pour l'analyse
% ---
% path_anal = 'C:\Users\bremond\Documents\data\LargeScale\Approche5_StandCorrespAnalysis\wholedataset\results_analysis\'
%path_anal = 'C:\Users\bremond\Documents\data\LargeScale\last_results\results_analysis\'
path_anal=path_save;

SVraw = []; nbping = []; 
for i = 1:length(rad)
    SVcoordTrans = []; svtrans=[];
    str_run(i,:) = '000'; str_run(i,end-length(num2str(runs(1)))+1:end) = num2str(runs(1));
    load([path_save,'/',survey_name,'-RUN',str_run(i,:),'-TH-80-ER60-EIlay.mat']);
    %load ([path_save,'PELGAS13-RAD',num2str(i),'-TH-80-ER60-EIlay.mat']);
    SV = Sv_surfER60_db;
    size(SV)    
    % rangement des pings par ordre de longitude
    SVcoordSort=[]; 
    for f = 1:5
        SVcoord = [Sv_surfER60_db(:,:,f),squeeze(lon_surfER60_db(:,1,f))];
        SVcoordSort(:,:,f) = sortrows(SVcoord,size(SVcoord,2));
    end
    SVcoordSort = SVcoordSort (:,:,:);  
    SV = SVcoordSort(:,1:190,:); 
    SVraw = [SVraw;SV];
    maxDepth=max(max(depth_bottom_ER60_db));
    % regroupement temps
%     time = time_ER60_db;
%     TIME = [TIME;time];
    
    % nombre de pings par radiale
    np = size(SV,1); 
    if i == 1
        p = np;
    else
        p = np + nbping(length(nbping));
    end
    nbping = [nbping;p] 
end
save([path_anal,'SVraw.mat'],'SVraw'); 
save([path_anal,'nbping.mat'],'nbping')
% ---
load([path_anal,'SVraw.mat']); load([path_anal,'nbping.mat']);

%for r = rad
    
    % donn�es de la radiale r
    %if r == 1
%         svrawrad = 10.^(SVraw(1:nbping(r),:,:)./10);
%         SVrawrad = SVraw(1:nbping(r),:,:);
        svrawrad = 10.^(SVraw(:,:,:)./10);
        SVrawrad = SVraw(:,:,:);
%     else
%         svrawrad = 10.^(SVraw((nbping(r-1)+1):nbping(r),:,:)./10);
%         SVrawrad = SVraw((nbping(r-1)+1):nbping(r),:,:);
%     end
%     
    % figure large scale
        maxDepth=round(max(max(depth_bottom_ER60_db)));
    f = figure(2); set(f,'Units','Normalized','OuterPosition',[0 0 1 1]); 
    % echogrammes aux 5 fr�quences
    for f = 1:5
        subplot(3,3,f); DrawEchoGram_brut(SVrawrad(:,1:maxDepth,f)',-45,-90);
        title(['Radiale ',num2str(1),' - ',num2str(frequences(f)),' kHz'],'FontSize',11);
        freezeColors
    end
     subplot (2,3,6)
        colorbar ('location','southoutside') 
        
        
            % figure small scale
        maxDepth=round(max(max(depth_bottom_ER60_db)));
        figure()
    %f = figure(2); set(f,'Units','Normalized','OuterPosition',[0 0 1 1]); 
    % echogrammes aux 5 fr�quences
    for f = 1:5
        subplot(3,3,f); DrawEchoGram_brut(SVrawrad(6750:7100,1:55,f)',-45,-90);
        title(['Radiale ',num2str(1),' - ',num2str(frequences(f)),' kHz'],'FontSize',11);
        freezeColors
    end
     subplot (3,3,6)
        colorbar ('location','southoutside')
%end    

%% 1. Analyse sur donnees petite échelle
 
(max(time_ER60_db(6750:7000))-min(time_ER60_db(6750:7000)))*24*60
 SVraws2=SVraw(6750:7000,:,:);
 maxDepth=55;
 prefix='SmallScaleClusters';
 frequences = [18 38 70 120 200];
 
% Ballon
close all
[MDCLU1,BallonIndic]=BallonIndicator(SVraws2,frequences,prefix,maxDepth,path_anal);

% Trenkel&Berger
close all
[delta_ER60f,mfriClust]=mfrIndicator(SVraws2,frequences,prefix,maxDepth,path_anal);
 
% Unsupervised, 3 clusters
%----------------
 silhou=false;
 prefix='SmallScale3clusters'
 
 %Kmeans
 close all
 [IDX1ss3,C1ss3,sumd1ss3,MDCLU1ss3] = kmeanAcouClass(SVraws2,3,path_anal,prefix,maxDepth,frequences,silhou);
  
% classification by multiple correspondences
close all
[coeff,score,latent,IDX,idx_wide,MDCLU]=CMA_PCA_kmeanClass(SVraws2,path_anal,3,silhou,frequences,maxDepth,prefix);
    
% Unsupervised, 4 clusters

 prefix='SmallScale4clusters'
 
 %Kmeans
 close all
 [IDX1ss4,C1ss4,sumd1ss4,MDCLU1ss4] = kmeanAcouClass(SVraws2,4,path_anal,prefix,maxDepth,frequences,silhou);
  
% classification by multiple correspondences
close all
[coeffss4,scoress4,latentss4,IDXss4,idx_widess4,MDCLUss4]=CMA_PCA_kmeanClass(SVraws2,path_anal,4,silhou,frequences,maxDepth,prefix);
    
% Unsupervised, 5 clusters

 prefix='SmallScale5clusters'
 
 %Kmeans
 close all
 [IDX1ss5,C1ss5,sumd1ss5,MDCLU1ss5] = kmeanAcouClass(SVraws2,5,path_anal,prefix,maxDepth,frequences,silhou);
  
% classification by multiple correspondences
close all
[coeffss5,scoress5,latentss5,IDXss5,idx_widess5,MDCLUss5]=CMA_PCA_kmeanClass(SVraws2,path_anal,5,silhou,frequences,maxDepth,prefix);
    
% Unsupervised, 6 clusters

 prefix='SmallScale6clusters'
 
 %Kmeans
 close all
 [IDX1ss6,C1ss6,sumd1ss6,MDCLU1ss6] = kmeanAcouClass(SVraws2,6,path_anal,prefix,maxDepth,frequences,silhou);
  
% classification by multiple correspondences
close all
[coeffss6,scoress6,latentss6,IDXss6,idx_widess6,MDCLUss6]=CMA_PCA_kmeanClass(SVraws2,path_anal,6,silhou,frequences,maxDepth,prefix);
  
% Unsupervised, 7 clusters

 prefix='SmallScale7clusters'
 
 %Kmeans
 close all
 [IDX1ss7,C1ss7,sumd1ss7,MDCLU1ss7] = kmeanAcouClass(SVraws2,7,path_anal,prefix,maxDepth,frequences,silhou);
  
% classification by multiple correspondences
close all
[coeffss7,scoress7,latentss7,IDXss7,idx_widess7,MDCLUss7]=CMA_PCA_kmeanClass(SVraws2,path_anal,7,silhou,frequences,maxDepth,prefix);
  

%% 2. Analyse sur donnees a moyenne echelle
 
(max(time_ER60_db)-min(time_ER60_db))*24*60
 maxDepth=max(max(depth_bottom_ER60_db));
 prefix='MediumScaleClusters';
 frequences = [18 38 70 120 200];
 
% Ballon
close all
[MDCLU1ms,BallonIndicms]=BallonIndicator(SVraws,frequences,prefix,maxDepth,path_anal);

% Trenkel&Berger
close all
[delta_ER60fms,mfriClustms]=mfrIndicator(SVraws,frequences,prefix,maxDepth,path_anal);
 
% Unsupervised, 3 clusters
%----------------
 silhou=false;
 prefix='MediumScale3clusters'
 
 %Kmeans
 close all
 [IDX1ms3,C1ms3,sumd1ss3,MDCLU1ss3] = kmeanAcouClass(SVraws2,3,path_anal,prefix,maxDepth,frequences,silhou);
  
% classification by multiple correspondences
close all
[coeff,score,latent,IDX,idx_wide,MDCLU]=CMA_PCA_kmeanClass(SVraws2,path_anal,3,silhou,frequences,maxDepth,prefix);
    
% Unsupervised, 4 clusters

 prefix='SmallScale4clusters'
 
 %Kmeans
 close all
 [IDX1ss4,C1ss4,sumd1ss4,MDCLU1ss4] = kmeanAcouClass(SVraws2,4,path_anal,prefix,maxDepth,frequences,silhou);
  
% classification by multiple correspondences
close all
[coeffss4,scoress4,latentss4,IDXss4,idx_widess4,MDCLUss4]=CMA_PCA_kmeanClass(SVraws2,path_anal,4,silhou,frequences,maxDepth,prefix);
    
% Unsupervised, 5 clusters

 prefix='SmallScale5clusters'
 
 %Kmeans
 close all
 [IDX1ss5,C1ss5,sumd1ss5,MDCLU1ss5] = kmeanAcouClass(SVraws2,5,path_anal,prefix,maxDepth,frequences,silhou);
  
% classification by multiple correspondences
close all
[coeffss5,scoress5,latentss5,IDXss5,idx_widess5,MDCLUss5]=CMA_PCA_kmeanClass(SVraws2,path_anal,5,silhou,frequences,maxDepth,prefix);
    
        
    
    
    
    
    
    
    
    
    
    
    
    
    
    
%% 4. Analyse des correspondances multiples
% sur les donn�es standardis�es
% Large scale
%-------------
SVmax = max(max(SVlong));
SVmin = min(min(SVlong));
lag=(SVmax-SVmin)/5;
bks=[];
bks(1)=SVmin;
for i=1:4
    bks(i+1)=SVmin+lag*(i+1);
end
% d�finition des classes et construction du tableau disjonctif complet
% ---------- EI config = 1km x 1m --------------
% icfq = [];
% for f = 1:5
%     i5fq = SVlong(:,f) < ceil(SVmax) & SVlong(:,f) >= 20;
%     i4fq = SVlong(:,f) < 20 & SVlong(:,f) >= 7;
%     i3fq = SVlong(:,f) < 7 & SVlong(:,f) >= -6;
%     i2fq = SVlong(:,f) < -6 & SVlong(:,f) >= -19;
%     i1fq = SVlong(:,f) < -19 & SVlong(:,f) >= floor(SVmin);
%     icfq = [i5fq i4fq i3fq i2fq i1fq icfq];
% end
% ---------- EI config = 200m x 1m --------------
% icfq = [];
% for f = 1:5
%     i5fq = SVlong(:,f) < ceil(SVmax) & SVlong(:,f) >= 40;
%     i4fq = SVlong(:,f) < 40 & SVlong(:,f) >= 20;
%     i3fq = SVlong(:,f) < 20 & SVlong(:,f) >= 0;
%     i2fq = SVlong(:,f) < 0 & SVlong(:,f) >= -20;
%     i1fq = SVlong(:,f) < -20 & SVlong(:,f) >= floor(SVmin);
%     icfq = [i5fq i4fq i3fq i2fq i1fq icfq];
% end

% ---------- Auto --------------
icfq = [];
for f = 1:5
    i5fq = SVlong(:,f) < ceil(SVmax) & SVlong(:,f) >= bks(4);
    i4fq = SVlong(:,f) < bks(4) & SVlong(:,f) >= bks(3);
    i3fq = SVlong(:,f) < bks(3) & SVlong(:,f) >= bks(2);
    i2fq = SVlong(:,f) < bks(2) & SVlong(:,f) >= bks(1);
    i1fq = SVlong(:,f) < bks(1) & SVlong(:,f) >= floor(SVmin);
    icfq = [i5fq i4fq i3fq i2fq i1fq icfq];
end

save([path_anal,'TablDisjComp.mat'],'icfq');
% ---
load([path_anal,'TablDisjComp.mat']);

% nom des variables
vlab = {'18_5','18_4','18_3','18_2','18_1','38_5','38_4','38_3','38_2','38_1',...
    '70_5','70_4','70_3','70_2','70_1','120_5','120_4','120_3','120_2','120_1',...
    '200_5','200_4','200_3','200_2','200_1'};

% Small scale
%-------------
SVmaxs = max(max(SVlongs));
SVmins = min(min(SVlongs));
lags=(SVmaxs-SVmins)/5;
bkss=[];
bkss(1)=SVmins;
for i=1:4
    bkss(i+1)=SVmins+lags*(i+1);
end
% ---------- Auto --------------
icfq = [];
for f = 1:5
    i5fq = SVlong(:,f) < ceil(SVmax) & SVlong(:,f) >= bks(4);
    i4fq = SVlong(:,f) < bks(4) & SVlong(:,f) >= bks(3);
    i3fq = SVlong(:,f) < bks(3) & SVlong(:,f) >= bks(2);
    i2fq = SVlong(:,f) < bks(2) & SVlong(:,f) >= bks(1);
    i1fq = SVlong(:,f) < bks(1) & SVlong(:,f) >= floor(SVmin);
    icfqs = [i5fq i4fq i3fq i2fq i1fq icfq];
end

save([path_anal,'TablDisjComp.mat'],'icfqs');
% ---
load([path_anal,'TablDisjComp.mat']);

% nom des variables
vlab = {'18_5','18_4','18_3','18_2','18_1','38_5','38_4','38_3','38_2','38_1',...
    '70_5','70_4','70_3','70_2','70_1','120_5','120_4','120_3','120_2','120_1',...
    '200_5','200_4','200_3','200_2','200_1'};

%% 5. Analyse en Composantes Principales 
% sur le tableau disjonctif complet
[coeff,score,latent] = princomp(icfq);
save([path_anal,'1ping_resPCA.mat'],'coeff','score','latent');
% ---
load([path_anal,'1ping_resPCA.mat']);

% Variance
Var = 100*latent ./ sum(latent); VarCum = cumsum(Var);
hold on; bar(Var); plot(VarCum, '-or','LineWidth',2)
legend({'Var. par PC' 'Var. cumul�e'},4); legend boxoff
xlabel('Composantes Principales'); ylabel('Variance expliqu�e [%]')
saveas(figure(1),[path_anal,'varPCA'],'jpeg'); close(1)

% biplot (projection des variables et des individus dans l'espace des
% composantes principales)

% Le nombre de cellules (= d'individus) �tant trop important pour la visualisation
% graphique, s�lection al�atoire de 10000 individus projet�s dans l'espace
x = score(:,1); n = 10000;
nx = numel(x); inx = randperm(nx);
pc1 = score(inx(1:n),1);pc2 = score(inx(1:n),2);
pc3 = score(inx(1:n),3);pc4 = score(inx(1:n),4);

% pour une EI � grande �chelle, visualisation de tous les individus
% pc1 = score(:,1);pc2 = score(:,2);
% pc3 = score(:,3);pc4 = score(:,4);

f1 = figure(1); set(f1,'Units','Normalized','OuterPosition',[0 0 1 1]);
subplot(1,2,1); biplot(coeff(:,1:2),'Scores',[pc1,pc2],...
    'VarLabels',vlab)
xlabel (['PC1 - ',num2str(round(Var(1))),'%'])
ylabel (['PC2 - ',num2str(round(Var(2))),'%'])
title('10000 random scores')
subplot(1,2,2); biplot(coeff(:,3:4),'Scores',[pc3,pc4],...
    'VarLabels',vlab)
xlabel (['PC3 - ',num2str(round(Var(3))),'%'])
ylabel (['PC4 - ',num2str(round(Var(4))),'%'])                    
saveas(figure(1),[path_anal,'PC1-2_PC3-4'],'jpeg'); close(1)

%% 6. Kmeans clustering
% ---
X = score(:,1:7); % on ne prend que les 7 premi�res CP pour le clustering
k = 4;              % nombre de clusters
size(X)
% algorithme Kmeans (�a peut �tre long...)
opt = statset('Display','final','MaxIter',100); 
[IDX,C,sumd] = kmeans(X,k,'Replicates',40,'Options',opt,'start','cluster','emptyaction','singleton'); 

save([path_anal,'1pingFactorisedACP-KM_',num2str(k),'clusters'],'IDX','C','sumd');
% ---
load([path_anal,'1pingFactorisedACP-KM_',num2str(k),'clusters']);

x1 = X1(:,1); n = 10000;
nx1 = numel(x1); inx1 = randperm(nx1);
x1r=x1(inx1(1:n),:);
idx1r=IDX1(inx1(1:n),:);

figure;
[silh1,h1] = silhouette(x1r,idx1r);
h = gca;
h.Children.EdgeColor = [.8 .8 1];
xlabel 'Silhouette Value';
ylabel 'Cluster';

save([path_anal,'rawKMsilh_',num2str(k),'clusters'],'h1');
load([path_anal,'rawKMsilh_',num2str(k),'clusters']);

% changement de structure des facteurs 
idx_wide = squeeze(reshape(IDX,size(SVraw,1),[],size(SVraw,2)));

%% 7. Calcul des r�ponses fr�quentielles m�dianes par cluster
% ---

f2 = figure(2);
for clu = 1:k
    MDF = [];
    SVrawLongs=SVrawLong(IDX == clu,:);
    subplot(2,2,clu);
    boxplot(SVrawLongs)
    ylim([-100,-30])
    title(['Cluster',num2str(clu)])
%     for f = 1:length(frequences)
%         svrawf = 10.^(SVraw(:,:,f)./10); 
%         mdf = 10*log10(median(svrawf(IDX == clu)));    
%         MDF = [MDF,mdf];
%     end
%     MDCLU = [MDCLU;MDF]; 
end

% figure()
% for clu = 1:k
%     MDF = [];
%     SVrawLongs=SVrawLong(IDX == clu,:);
%     boxplot(SVrawLongs)
%     hold on
%     title(['Cluster',num2str(clu)])
% %     for f = 1:length(frequences)
% %         svrawf = 10.^(SVraw(:,:,f)./10); 
% %         mdf = 10*log10(median(svrawf(IDX == clu)));    
% %         MDF = [MDF,mdf];
% %     end
% %     MDCLU = [MDCLU;MDF]; 
% end

MDCLU = [];

for clu = 1:k
    MDF = [];
    for f = 1:length(frequences)
        svrawf = 10.^(SVraw(:,:,f)./10); 
        mdf = 10*log10(median(svrawf(IDX == clu)));    
        MDF = [MDF,mdf];
    end
    MDCLU = [MDCLU;MDF]; 
end


% save
save([path_save,'1ping_RepFreqMedianeByClust_',num2str(k),'clust'],'MDCLU');
load([path_save,'1ping_RepFreqMedianeByClust_',num2str(k),'clust']);

%% 8. Repr�sentation graphique de la classification des cellules
% --
%for r = rad
    
    % donn�es de la radiale r
%     if r == 1
%         idxrad = idx_wide(1:nbping(r),:,:);
%         svrawrad = 10.^(SVraw(1:nbping(r),:,:)./10);
%         SVrawrad = SVraw(1:nbping(r),:,:);
%     else
%         idxrad = idx_wide((nbping(r-1)+1):nbping(r),:,:);
%         svrawrad = 10.^(SVraw((nbping(r-1)+1):nbping(r),:,:)./10);
%         SVrawrad = SVraw((nbping(r-1)+1):nbping(r),:,:);
%     end

          idxrad = idx_wide;
          svrawrad = 10.^(SVraw./10);
          SVrawrad = SVraw;
          
    maxDepth=max(max(depth_bottom_ER60_db));
    % figure
    f2 = figure(2); set(f2,'Units','Normalized','OuterPosition',[0 0 1 1]); 
    % echogrammes aux 5 fr�quences
    for freq = 1:5
        subplot(3,3,freq); DrawEchoGram_brut(SVrawrad(:,1:(round(maxDepth)),freq)',-45,-90);
        title(['Radiale ',' - ',num2str(frequences(f)),' kHz'],'FontSize',11);
        freezeColors
    end

    % r�ponses fr�quentielles m�dianes par cluster
    c = colormap(hsv(k)); % jeu des couleurs pour le clustering
    txtleg = []; txtcolbar = []; subplot(3,3,7); hold on
    for j = 1:k
        plot(MDCLU(j,1:size(MDCLU,2))','Color',c(j,:),'LineWidth',2)
        txtleg = [txtleg {['cluster ',num2str(j)]}];    % texte pour la l�gende
        txtcolbar = [txtcolbar {num2str(j)}];       % texte pour la barre des couleurs
    end
    set(gca,'XTickLabel',{'18','38','70','120','200'},'XTick',1:5,'FontSize',11)
    title('Reponses frequentielles medianes par cluster','FontSize',11);
    xlabel('Frequences [kHz]'); ylabel('Intensite acoustique [dB]');
    legend(txtleg,4); legend boxoff

    % classification des cellules
    %
    subplot(3,3,8:9); 
    %figure()
    imagesc(idxrad(:,1:round(maxDepth))'); 
    colorbar ('location','eastoutside','XTickLabel',txtcolbar,'XTick',[1:k]) 
    title(['Cells classification'],'FontSize',11);
    %c = colormap(hsv(k)); % jeu des couleurs pour le clustering

    saveas(figure(2),[path_anal,'RAD',num2str(r),'_ResultatFinal_',num2str(k),'clust'],'jpeg');
    saveas(figure(2),[path_anal,'RAD',num2str(r),'_ResultatFinal_',num2str(k),'clust'],'fig');
    close(2)
%end

 %small scale with large scale classification
    figure()    
    imagesc(idxrad(6750:7100,1:55)'); 
    colorbar ('location','eastoutside','XTickLabel',txtcolbar,'XTick',[1:k]) 
    %title(['Cells classification'],'FontSize',11);

    DrawEchoGram_brut(SVrawrad(6750:7100,1:55,f)',-45,-90);
    
    saveas(figure(2),[path_anal,'RAD',num2str(r),'_ResultatFinal_',num2str(k),'clust'],'jpeg');
    saveas(figure(2),[path_anal,'RAD',num2str(r),'_ResultatFinal_',num2str(k),'clust'],'fig');
    close(2)
    
    %small scale with small scale classification
    idxrad1s = idx1_wides;
    svrawrads = 10.^(SVraws./10);
    SVrawrads = SVraws;
     
    figure()    
    imagesc(idxrad1s(:,1:55)'); 
    colorbar ('location','eastoutside','XTickLabel',txtcolbar,'XTick',[1:k1]) 
    %title(['Cells classification'],'FontSize',11);

% figure des r�ponses fr�quentielles

c = colormap(hsv(k)); % jeu des couleurs pour le clustering
txtleg = []; figure(); hold on
for j = 1:k
    plot(MDCLU(j,1:size(MDCLU,2))','Color',c(j,:),'LineWidth',2)
    txtleg = [txtleg {['cluster ',num2str(j)]}];    % texte pour la l�gende
end
set(gca,'XTickLabel',{'18','38','70','120','200'},'XTick',1:5,'FontSize',11)
title('Reponses frequentielles medianes par cluster','FontSize',11);
xlabel('Frequences [kHz]'); ylabel('Intensite acoustique [dB]');
legend(txtleg,4); legend boxoff

saveas(figure(1),[path_anal,'ReponseFrequentielle_',num2str(k),'clust'],'jpeg');
saveas(figure(1),[path_anal,'ReponseFrequentielle_',num2str(k),'clust'],'fig');























%% 9. Formatage donn�es pour inputs GLOBE.

clear all
path_resEI = 'C:\Users\bremond\Documents\data\LargeScale\last_results\results_EI_GoToStop_corrected\';
path_resAnalysis = 'C:\Users\bremond\Documents\data\LargeScale\last_results\results_analysis\';
path_save = 'C:\Users\bremond\Documents\data\LargeScale\last_results\DataForGLOBE\';
rad = [1:24];

% initial matrices
depth_surface_ER60_db = [];
lat_surfER60_db = [];
lon_surfER60_db = [];
Sa_surfER60_db = [];
Sv_surfER60_db = [];
time_ER60_db = [];
vol_surfER60_db = [];
idx_ER60 = [];
NbESU = [];

for ir = 1:length(rad)
%     ir=15
    SVcoordTrans = []; svtrans=[];
%     str_run(ir,:)='000'; str_run(ir,end-length(num2str(rad(ir)))+1:end)=num2str(rad(ir));

    % Echo-Integration data
    dataload = load ([path_resEI,'/PELGAS13-RAD',num2str(ir),'-TH-80-ER60-EIlay.mat']);% whole dataset + 200mScale
    
    % ordination par le temps
    
    
    % regroupement
    depth_surface_ER60_db = [depth_surface_ER60_db;dataload.depth_surface_ER60_db];
    lat_surfER60_db = [lat_surfER60_db;dataload.lat_surfER60_db];
    lon_surfER60_db = [lon_surfER60_db;dataload.lon_surfER60_db];
    Sa_surfER60_db = [Sa_surfER60_db;dataload.Sa_surfER60_db];
    Sv_surfER60_db = [Sv_surfER60_db;dataload.Sv_surfER60_db];
    if size(dataload.time_ER60_db,1) > 1
        time_ER60_db = [time_ER60_db,dataload.time_ER60_db(1,:)];
    else
        time_ER60_db = [time_ER60_db,dataload.time_ER60_db];
    end
    vol_surfER60_db = [vol_surfER60_db;dataload.vol_surfER60_db];
    
    
    
    % ESU number by transect
    NbESUtransect = size(dataload.Sv_surfER60_db,1); 
    if ir == 1
        p = NbESUtransect;
    else
        p = NbESUtransect + NbESU(length(NbESU));
    end
    NbESU = [NbESU;p] 
    
end

save([path_save,'NbESU.mat'],'NbESU')
load([path_save,'NbESU.mat'])
% Kmeans clustering data
dataanalysis = load([path_resAnalysis,'facteurs_6clusters.mat']);
idx_long_ER60 = dataanalysis.IDX;
idx_ER60 = squeeze(reshape(idx_long_ER60,size(Sv_surfER60_db,1),[],size(Sv_surfER60_db,2)));

% Meta donnees
sign1 = 'IFREMER';  %organisme producteur de la donn�e
sign2 = 'MOVIES3D'; %logiciel d'EI
sign3 = 'SSL_clustering';   %m�thode d'�chotypage 'SSL_clustering' ou 'Echotypage_Movies'
format = 20140228;          % date de changement du format de fichier

data = struct('Signature1',sign1,'Signature2',sign2,'Signature3',sign3,'Format',format,...
    'depth_surface_ER60_db',depth_surface_ER60_db,'lat_surfER60_db',lat_surfER60_db,...
    'lon_surfER60_db',lon_surfER60_db,'Sa_surfER60_db',Sa_surfER60_db,...
    'Sv_surfER60_db',Sv_surfER60_db,'time_ER60_db',time_ER60_db,...
    'vol_surfER60_db',vol_surfER60_db,'idx_ER60',idx_ER60,'NbESU',NbESU);

% Concatenating into a Matlab structure
save([path_save,'PELGAS13_SSLclustering_200m-1m.mat'],'data');
load([path_save,'PELGAS13_SSLclustering_200m-1m.mat']);

% V�rification des coordonn�es
figure(1); hold on
for r = 1:24
    if r == 1
        latrad = data.lat_surfER60_db(1:data.nESU(r),1,1);
        lonrad = data.lon_surfER60_db(1:data.nESU(r),1,1);
        plot(lonrad,latrad,'.k'); 
    else
        latrad = data.lat_surfER60_db((data.nESU(r-1)+1):data.nESU(r),1,1);
        lonrad = data.lon_surfER60_db((data.nESU(r-1)+1):data.nESU(r),1,1);
        plot(lonrad,latrad,'.k');
    end
end
saveas(figure(1),[path_save,'verif_coord'],'png')
saveas(figure(1),[path_save,'verif_coord'],'fig')

%%
path_resEI = 'C:\Users\bremond\Documents\data\LargeScale\last_results\results_EI_GoToStop_corrected\';

% metadata
sign1 = 'IFREMER';  %organisme producteur de la donn�e
sign2 = 'MOVIES3D'; %logiciel d'EI
sign3 = 'SSL_clustering';   %m�thode d'�chotypage 'SSL_clustering' ou 'Echotypage_Movies'
format = 20140228;          % date de changement du format de fichier

alldata = ();

for r = 12 % radiale
    radname = ['R',num2str(r)];
    data = load([path_resEI,'PELGAS13-RAD',num2str(r),'-TH-80-ER60-EIlay.mat']); % data
    
    if r == 1
        data.idx_ER60 = idx_wide(1:nbping(r),:,:);
    else
        data.idx_ER60 = idx_wide((nbping(r-1)+1):nbping(r),:,:);
    end
    data.name = radname;
    data.Signature1 = sign1;
    data.Signature2 = sign2;
    data.Signature3 = sign3;
    data.Format = format;
    
    alldata(1,1) = data;
end

data = struct('Signature1',sign1,'Signature2',sign2,'Signature3',sign3,'Format',format,...
    'depth_surface_ER60_db',depth_surface_ER60_db,'lat_surfER60_db',lat_surfER60_db,...
    'lon_surfER60_db',lon_surfER60_db,'Sa_surfER60_db',Sa_surfER60_db,...
    'Sv_surfER60_db',Sv_surfER60_db,'time_ER60_db',time_ER60_db,...
    'vol_surfER60_db',vol_surfER60_db,'idx_ER60',idx_ER60,'nESU',nESU);

