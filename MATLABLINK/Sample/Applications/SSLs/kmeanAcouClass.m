function [IDX1,C1,sumd1,MDCLU1] = kmeanAcouClass(SVraw,k1,path_anal,prefix,maxDepth,frequences,silhou)

SVrawLong = squeeze(reshape(SVraw,[],size(SVraw,3)));

%% Kmeans sur les donn�es brutes large échelle
% ---
X1 = SVrawLong;
% algorithme Kmeans (�a peut �tre long...)
opt = statset('Display','final','MaxIter',200); 
[IDX1,C1,sumd1] = kmeans(X1,k1,'Replicates',40,'Options',opt,'start','cluster','emptyaction','singleton'); 

save([path_anal,prefix,'_rawKM_',num2str(k1),'clusters'],'IDX1','C1','sumd1');

if silhou==true

    x1 = X1(:,1); n = 10000;
    nx1 = numel(x1); inx1 = randperm(nx1);
    x1r=x1(inx1(1:n),:);
    idx1r=IDX1(inx1(1:n),:);
    figure;
    [silh1,h1] = silhouette(x1r,idx1r);
    h = gca;
    h.Children.EdgeColor = [.8 .8 1];
    xlabel 'Silhouette Value';
    ylabel 'Cluster';
    
    save([path_anal,prefix,'_rawKMsilh_',num2str(k1),'clusters'],'h1');
end

% changement de structure des facteurs 
idx1_wide = squeeze(reshape(IDX1,size(SVraw,1),[],size(SVraw,2)));

figure(1);

if k1<5
    nrmax=2;
    ncmax=2;
elseif k1<7
    nrmax=3;    
    ncmax=2;
else
    nrmax=3;    
    ncmax=3;    
end

for clu = 1:k1
    SVrawLongs=SVrawLong(IDX1 == clu,:);
    subplot(nrmax,ncmax,clu);
    boxplot(SVrawLongs)
    ylim([-110,-30])
    title(['raw KM cluster',num2str(clu)])
end

    saveas(figure(1),[path_anal,prefix,'_rawKM-fClustBoxplots_',num2str(k1),'clust'],'jpeg');
    saveas(figure(1),[path_anal,prefix,'_rawKMfClustBoxplots_',num2str(k1),'clust'],'fig');

MDCLU1 = [];
for clu = 1:k1
    MDF1 = [];
    for f = 1:length(frequences)
        svrawf = 10.^(SVraw(:,:,f)./10); 
        mdf1 = 10*log10(median(svrawf(IDX1 == clu)));    
        MDF1 = [MDF1,mdf1];
    end
    MDCLU1 = [MDCLU1;MDF1]; 
end

% save
save([path_anal,prefix,'_rawKM RepFreqMedianeByClust_',num2str(k1),'clust'],'MDCLU1');

%% Representations graphiques clustering

     idxrad1 = idx1_wide;
     svrawrad = 10.^(SVraw./10);
     SVrawrad = SVraw;
          
    % figure
    f2 = figure(2); set(f2,'Units','Normalized','OuterPosition',[0 0 1 1]); 
    % echogrammes aux 5 fr�quences
    for freq = 1:5
        subplot(3,3,freq); DrawEchoGram_brut(SVrawrad(:,1:(round(maxDepth)),freq)',-45,-90);
        title([num2str(frequences(f)),' kHz'],'FontSize',11);
        freezeColors
    end

    % r�ponses fr�quentielles m�dianes par cluster
    c = colormap(hsv(k1)); % jeu des couleurs pour le clustering
    txtleg = []; txtcolbar = []; subplot(3,3,6); hold on
    for j = 1:k1
        plot(MDCLU1(j,1:size(MDCLU1,2))','Color',c(j,:),'LineWidth',2)
        txtleg = [txtleg {['cluster ',num2str(j)]}];    % texte pour la l�gende
        txtcolbar = [txtcolbar {num2str(j)}];       % texte pour la barre des couleurs
    end
    set(gca,'XTickLabel',{'18','38','70','120','200'},'XTick',1:5,'FontSize',10)
    %title('R�ponses fr�quentielles m�dianes par cluster','FontSize',11);
    xlabel('Frequences [kHz]','FontSize',10);%ylabel('Intensit� acoustique [dB]');
   
    % classification des cellules
    %
    subplot(3,3,7:9); 
    %figure()
    imagesc(idxrad1(:,1:round(maxDepth))'); 
    colorbar ('location','eastoutside','XTickLabel',txtleg,'XTick',[1:k1]) 
    title(['Cells classification'],'FontSize',11);
    c = colormap(hsv(k1)); % jeu des couleurs pour le clustering
   
    saveas(figure(2),[path_anal,prefix,'_rawKM_Classif_',num2str(k1),'clust'],'jpeg');
    saveas(figure(2),[path_anal,prefix,'_rawKM_Classif_',num2str(k1),'clust'],'fig');
    
    figure(3)
    imagesc(idxrad1(:,1:round(maxDepth))'); 
    colorbar ('location','eastoutside','XTickLabel',txtleg,'XTick',[1:k1]) 
    title(['Cells classification'],'FontSize',11);
    c = colormap(hsv(k1)); % jeu des couleurs pour le clustering

    saveas(figure(3),[path_anal,prefix,'_rawKM-ImageClassif_',num2str(k1),'clust'],'jpeg');
    saveas(figure(3),[path_anal,prefix,'_rawKM-ImageClassif_',num2str(k1),'clust'],'fig');
    
    % figure des r�ponses fr�quentielles

    c = colormap(hsv(k1)); % jeu des couleurs pour le clustering
    figure(4); hold on
    for j = 1:k1
        plot(MDCLU1(j,1:size(MDCLU1,2))','Color',c(j,:),'LineWidth',2)
    end
    set(gca,'XTickLabel',{'18','38','70','120','200'},'XTick',1:5,'FontSize',11)
    title('Median frequency response per cluster','FontSize',11);
    xlabel('Frequencies [kHz]'); %ylabel('Intensite acoustique [dB]');
    legend(txtleg,4); legend boxoff
    
    saveas(figure(4),[path_anal,prefix,'_rawKM_ReponseFrequentielleClusters_',num2str(k1),'clust'],'jpeg');
    saveas(figure(4),[path_anal,prefix,'_rawKM_ReponseFrequentielleClusters_',num2str(k1),'clust'],'fig');

end