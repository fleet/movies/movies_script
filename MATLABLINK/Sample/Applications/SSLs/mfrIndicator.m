function [delta_ER60f,mfriClust]=mfrIndicator(CellulesER60,frequences,prefix,maxDepth,path_anal)

%on calcule l'indicateur � partir du sv en naturel
        CellulesER60_naturel = 10.^(CellulesER60./10);
        %freq=[18 38 70 120 200];
        nbesuER60=size(CellulesER60_naturel,1);
        nblayerER60=size(CellulesER60_naturel,2);
        nbfreqER60=size(CellulesER60_naturel,3);
        
        deltanum_ER60f=zeros(nbesuER60,nblayerER60);
        deltadenum_ER60f=zeros(nbesuER60,nblayerER60);
        delta_ER60f=NaN(nbesuER60,nblayerER60);
        
        for esu=1:nbesuER60
            for layer=1:nblayerER60
                for freq1=2:nbfreqER60
                    for freq2=1:freq1-1
                        if (~isnan(CellulesER60_naturel(esu,layer,:)))
                            CellulesER60_naturel_freq1=(CellulesER60_naturel(esu,layer,freq1)-min(CellulesER60_naturel(esu,layer,:)))/(max(CellulesER60_naturel(esu,layer,:))-min(CellulesER60_naturel(esu,layer,:)));
                            CellulesER60_naturel_freq2=(CellulesER60_naturel(esu,layer,freq2)-min(CellulesER60_naturel(esu,layer,:)))/(max(CellulesER60_naturel(esu,layer,:))-min(CellulesER60_naturel(esu,layer,:)));
                            deltanum_ER60f(esu,layer)= deltanum_ER60f(esu,layer)+ CellulesER60_naturel_freq1*CellulesER60_naturel_freq2*(1-exp(-abs(frequences(freq1)-frequences(freq2))/(40)))*(1/frequences(freq1))*(1/frequences(freq2));
                            deltadenum_ER60f(esu,layer)= deltadenum_ER60f(esu,layer)+ CellulesER60_naturel_freq1*CellulesER60_naturel_freq2*(1/frequences(freq1))*(1/frequences(freq2));
                        end
                    end
                end
                if deltadenum_ER60f(esu,layer)>0
                    delta_ER60f(esu,layer)=(deltanum_ER60f(esu,layer)/deltadenum_ER60f(esu,layer)-0.4)/0.6;
                end
            end
        end

        %depthER60=depth_bottom_ER60(:,1,2)-2.5;
        
        %on sauvegarde les r�sultats
        save ([path_anal,prefix,'_mfrIndic'],'delta_ER60f');
        
        % on d�finit les bornes de classes d'esp�ces �
        % partir des valeurs d'index pour les r�ponses
        % fr�quentielles de SIMFAMI (ref
        % indicateur_rf_simul pour la simulation)
        mfriClust=zeros(nbesuER60,nblayerER60);
        % fish=1
        mfriClust(delta_ER60f<0.39)=1;
        % fluid-like=3
        mfriClust(delta_ER60f>=0.7&delta_ER60f<=0.8)=3;
       % mackerel=4
        mfriClust(delta_ER60f>0.8)=4;
       % gaseous=2
        mfriClust(delta_ER60f>=0.39&delta_ER60f<=0.58)=2;

        % changement de structure des facteurs
        size(mfriClust)
        idx1_wide = squeeze(reshape(mfriClust,size(CellulesER60,1),[],size(CellulesER60,2)));
        SVrawLong = squeeze(reshape(CellulesER60,[],size(CellulesER60,3)));
        
        clustersNames={'None','Fish','Other','Fluid-like','Mackerel'};
        uclu=unique(mfriClust);
        
        figure(1);
        for clu = 1:numel(uclu)
            SVrawLongs=SVrawLong(mfriClust== uclu(clu),:);
            subplot(3,2,clu);
            boxplot(SVrawLongs)
            ylim([-110,-30])
            set(gca,'XTickLabel',{'18','38','70','120','200'},'XTick',1:5,'FontSize',11)
            xlabel('Frequences [kHz]'); %ylabel('Intensite acoustique [dB]');
            title(['mfrInd ',clustersNames{clu}])
        end
        
        saveas(figure(1),[path_anal,prefix,'_mfrInd-fClustBoxplots'],'jpeg');
        saveas(figure(1),[path_anal,prefix,'_mfrInd-fClustBoxplots'],'fig');
        
        MDCLU1 = [];
        for clu = 1:numel(uclu)
            MDF1 = [];
            for f = 1:length(frequences)
                svrawf = 10.^(CellulesER60(:,:,f)./10);
                mdf1 = 10*log10(median(svrawf(mfriClust == uclu(clu))));
                MDF1 = [MDF1,mdf1];
            end
            MDCLU1 = [MDCLU1;MDF1];
        end
        
        % save
        save([path_anal,prefix,'_mfrIRepFreqMedianeByClust_'],'MDCLU1');
        
        %% 8. Repr�sentation graphique de la classification des cellules
        % --
        idxrad = idx1_wide;
        svrawrad = 10.^(CellulesER60./10);
        SVrawrad = CellulesER60;
        
        % figure
        f2 = figure(2); set(f2,'Units','Normalized','OuterPosition',[0 0 1 1]);
        % echogrammes aux 5 frequences
        for freq = 1:5
            subplot(3,3,freq); DrawEchoGram_brut(SVrawrad(:,1:(round(maxDepth)),freq)',-45,-90);
            title([num2str(frequences(freq)),' kHz'],'FontSize',11);
            freezeColors
        end
        
        % reponses frequentielles medianes par cluster
        c = colormap(hsv(numel(uclu))); % jeu des couleurs pour le clustering
        txtleg = []; txtcolbar = []; subplot(3,3,6); hold on
        for j = 1:numel(uclu)
            plot(MDCLU1(j,1:size(MDCLU1,2))','Color',c(j,:),'LineWidth',2)
            txtleg = [txtleg {clustersNames{j}}];    % texte pour la l�gende
            txtcolbar = [txtcolbar {num2str(j)}];       % texte pour la barre des couleurs
        end
        set(gca,'XTickLabel',{'18','38','70','120','200'},'XTick',1:5,'FontSize',10)
        title('Median frequency responses','FontSize',10);
        xlabel('Frequences [kHz]','FontSize',10); %ylabel('Intensite acoustique [dB]');
%         subplot(3,3,7)
%         hold on
%         for j = 1:5
%             plot(MDCLU1(j,1:size(MDCLU1,2))','Color',c(j,:),'LineWidth',2)
%          end
%         legend(txtleg)
        
        % classification des cellules
        %
        subplot(3,3,7:9);
        %figure()
        imagesc(mfriClust(:,1:round(maxDepth))');
        colorbar ('location','eastoutside','XTickLabel',txtleg,'XTick',[0:(numel(uclu)-1)])
        title(['Cells classification'],'FontSize',10);
        c = colormap(hsv(numel(uclu))); % jeu des couleurs pour le clustering
        
        saveas(figure(2),[path_anal,prefix,'_mfrInd-Classif'],'jpeg');
        saveas(figure(2),[path_anal,prefix,'_mfrInd-Classif'],'fig');
                
        figure(3)
        imagesc(mfriClust(:,1:round(maxDepth))');
        colorbar ('location','eastoutside','XTickLabel',txtleg,'XTick',[0:(numel(uclu)-1)])
        title(['Cells classification'],'FontSize',10);
        c = colormap(hsv(numel(uclu))); % jeu des couleurs pour le clustering
        
        saveas(figure(3),[path_anal,prefix,'_mfrInd-ImageClassif'],'jpeg');
        saveas(figure(3),[path_anal,prefix,'_mfrInd-ImageClassif'],'fig');
        
          % figure des r�ponses fr�quentielles

        c = colormap(hsv(numel(uclu))); % jeu des couleurs pour le clustering
        figure(6); hold on
        for j = 1:numel(uclu)
            plot(MDCLU1(j,1:size(MDCLU1,2))','Color',c(j,:),'LineWidth',2)
            txtleg = [txtleg {clustersNames{j}}];    % texte pour la l�gende
            txtcolbar = [txtcolbar {num2str(j)}];       % texte pour la barre des couleurs
        end
        set(gca,'XTickLabel',{'18','38','70','120','200'},'XTick',1:5,'FontSize',10)
        title('Median frequency response per cluster','FontSize',10);
        xlabel('Frequencies [kHz]','FontSize',10); %ylabel('Intensite acoustique [dB]');
        legend(txtleg)
        
        saveas(figure(6),[path_anal,prefix,'_mfrInd-RepFreq'],'jpeg');
        saveas(figure(6),[path_anal,prefix,'_mfrInd-RepFreq'],'fig');

        
end

