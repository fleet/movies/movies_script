% -----------------------------------------------------------------------
% CleaningEchog.m :
% The script cleans under (bubble shadowing) or over (wake or schools) 
% detections on EK60 echograms.
% -----------------------------------------------------------------------
% For RUNS without anomaly: 
%     -> Computation of median Sv vertical profile for each frequency.
% For RUNS with anomalies :
%     -> Filtering of under and over detection frequencies.
%     -> Computation of median Sv vertical profile for each frequency.
% -----------------------------------------------------------------------
% Inputs :
% Movies Echo-integration Matlab files for all runs.
% -----------------------------------------------------------------------
% Outputs :
%     -> Raw echograms (.jpeg)
%     -> Filtered echograms (.jpeg)
%     -> Filtered echo-integration data (.mat)
%     -> Median Profils for each frequency (.mat)
%     -> Filtering Quality (in terms of purcentage of total pings removed)
%     (.txt)
% -----------------------------------------------------------------------
% B. REMOND 19/10/2012                        Updated : 04/02/2014
% -----------------------------------------------------------------------

clear all
close all

%% Parameters and data loading

% Frequencies
fq = [18,38,70,120,200];
% Echo-integration threshold
THRESH = [-80];
 
% Path to the Echo-Integration results
path_result_EI = 'E:\Pelgas13_StationsL3J\EI\EI_withbottom\';

% Path to save the results
path_save = 'E:\Pelgas13_StationsL3J\EI\EI_withbottom\portions_v2\';
if ~exist(path_save,'dir')
    mkdir(path_save)
end

% Data
RUNS = [1:7];

%% Loop over the runs

NbFreq = size(fq,2);

for ir = 1:length(RUNS)
    
    str_run (ir,:) = '000';
    str_run (ir, end-length (num2str (RUNS (ir))) + 1 : end) = num2str (RUNS (ir));    
    run = str_run(ir,:)
    
    global fq THRESH run NbFreq depth

    % -----------------------------
    % Load data from Movies echo-integration
    resEI = load ([path_result_EI,'PELGAS13-RUN',str_run(ir,:),'-TH-80-ER60-EIlay.mat']);
 
    % Data
    SVsurf = resEI.Sv_surfER60_db;
    zmax = round(median(median(resEI.depth_bottom_ER60_db)));
    if zmax > 200
        zmax = 150;
    end
    SV = SVsurf (:,1:zmax,:);
    depth = squeeze(round(resEI.depth_surface_ER60_db(1,1:zmax,1)));
    % depth = depth(1,:);
    
    % -----------------------------
    % ordonate SV by longitude
    SVcoordSort=[];
    for f = 1:NbFreq
        SVcoord = [SV(:,:,f),squeeze(resEI.lon_surfER60_db(:,1,f))];
        SVcoordSort(:,:,f) = sortrows(SVcoord,size(SVcoord,2));
    end
    SV=SVcoordSort(:,1:zmax,:);
    
    
    % -----------------------------
    % If there is a 333 kHz transducter :
    % stop the depth at 100m to avoid 333 kHz noise
%     if sum(fq(1:size(fq,2)) == 333) > 0
%         if max (depth) > 100
%             seldepth = depth <= 100;
%             depth = depth (seldepth);
%             SV = SV (:,seldepth,:);
%         end
%     end
    % -----------------------------

    % -----------------------------
    % 18 and 38 kHz blind zone <---- Need to be more studied
%     SV18 = SV(:,:,1);
%     zcorr = 4;
%     % mean value on the first 3m
%     MeanBlindZone = 10*log10 (mean (mean (10.^(SV (:,1:zcorr,1)./10))));
%     % remove the mean value to 18 and 38 kHz first 4m
%     SV (:,1:zcorr,1:2) = SV (:,1:zcorr,1:2) + MeanBlindZone;
    
   
    % -------------------------
    % Plot raw echogram 
    fifi = figure (1)
    set(fifi,'Units','Normalized','OuterPosition',[0 0 1 1]);
    for i = 1:NbFreq
        subplot (2,3,i)
            DrawEchoGram_brut (SV (:,:,i)',-45,-90);
            title (['RUN',str_run(ir,:),' ',num2str(fq(i)),'kHz ',num2str(THRESH),'dB'])   
    end  
    colorbar
    colorbar ('location','southoutside')
    % ---------------------------------------------------------------------

    
    % -----------------------------------
    % Cut the ehcogram at the righ depth   
%     zmax = inputdlg ('At which depth do you want to cut the echogram? (zmax)')
%     zmax = str2num (cell2mat(zmax))
    close
    
%     SelectionDepth = depth <= zmax+10;
%     depth = depth(SelectionDepth);
%     SV = SV (:,SelectionDepth,:);    
    
    % ------------------------------
    % Raw median profils calculation
    RawProfils = [depth',squeeze(median (SV,1))];
    
    % -------------------------
    % Plot the resized raw echogram with raw median profils
    fifi = figure (1);
    set(fifi, 'Units','Normalized','OuterPosition',[0 0 1 1]);
    nfig = [1 2 3 5 6 7];
    for i = 1:NbFreq
        subplot (2,4,nfig(i))
            DrawEchoGram_brut (SV (:,:,i)',-45,-90);
            colorbar
            colorbar ('location','southoutside')
            title (['RUN',str_run(ir,:),' ',num2str(fq(i)),'kHz'])   
    end
    subplot (2,4,[4 8])
    plot (RawProfils(:,2:size(RawProfils,2)), -RawProfils(:,1), 'LineWidth',2)
    title ('Raw median profils')
    legend ('18','38','70','120','200'); legend boxoff
    xlim([-100 -40]) 

    % save the raw echogram
    fig1name = [path_save,'RUN',str_run(ir,:),'_RawEchograms']; saveas (figure(1),fig1name,'jpeg');
    saveas (figure(1),fig1name,'fig');
    
    % -----------------------------
    % Anomalies filtering
    
    % Ask which filters have to be applied
    filter = inputdlg ('Which filter do you want to apply to the RUN ? (F1 (under-detection), F2 (over-detection), 12 (F1 and F2), 0 (none))');
    filter = cell2mat(filter);
    close

    % -------------------------------------------------------    
    if filter == 'F1'                   % case of empty ping only
        
        % Filter 1 function
        threshold = 2;        
        [h,Result,Residu] = F1EP (SV,threshold);
        
        % Median profils
        MEDIANPROF = squeeze (median (Result));
        
        % -------
        % SAVING
        % -------       
        % filtered echograms
        fig2name = [path_save,'RUN',str_run(ir,:),'_Echograms',]; saveas (figure(2),fig2name,'jpeg'); close 
        % median profil
        filename1 = [path_save,'RUN',str_run(ir,:),'_MedianProfils.mat'];
        save (filename1,'MEDIANPROF');
        % filtered data matrix
        filename2 = [path_save,'RUN',str_run(ir,:),'_CutData.mat'];
        save (filename2,'Result');
      
        close all
        
    % -------------------------------------------------------    
    elseif filter == 'F2'               % case of wakes only
        
        % Filter 2 function
        threshold = 3.5*10^-5;
        [h,Result,Residu] = F2WP (SV,threshold);

        % Median profils 
        MEDIANPROF = squeeze (median (Result));
         
        % -------
        % SAVING
        % -------       
        % filtered echograms
        figname3 = [path_save,'RUN',str_run(ir,:),'_Echograms'];
        saveas (figure(2),figname3,'jpeg');
        close 
        % median profil
        filename3 = [path_save,'RUN',str_run(ir,:),'_MedianProfils.mat'];
        save (filename3,'MEDIANPROF');
        % filtered data matrix
        filename4 = [path_save,'RUN',str_run(ir,:),'_CutData.mat'];
        save (filename4,'Result');
        
        close all
      
    % -------------------------------------------------------          
    elseif filter == '12'            % case of empty pings and wakes
        
        thresholdF1 = 2;
        thresholdF2 = 3.5*10^-5;
        
        [h1,ResultF1,ResiduF1] = F1EP (SV,thresholdF1);
        [h2,ResultF2,ResiduF2] = F2WP (ResultF1,thresholdF2);
        
        % Median profils 
        MEDIANPROF = squeeze (median (ResultF2));     

        % -------
        % SAVING
        % -------       
        % filtered echograms
        figname4 = [path_save,'RUN',str_run(ir,:),'_Echograms'];
        saveas (figure(2),figname4,'jpeg');
        close 
        % median profil
        filename5 = [path_save,'RUN',str_run(ir,:),'_MedianProfils.mat'];
        save (filename5,'MEDIANPROF');
        % filtered data matrix
        filename6 = [path_save,'RUN',str_run(ir,:),'_CutData.mat'];
        save (filename6,'ResultF2');

        close all
        
    % -------------------------------------------------------    
    else                                % case of no filtering
        
        % Median profils
        MEDIANPROF = squeeze (median (SV));

        % New echogram
        fifi = figure (2);
        set(fifi, 'Units','Normalized','OuterPosition',[0 0 1 1]);
        nfig = [1 2 3 5 6 7];
        for i = 1:NbFreq
            subplot (2,4,nfig(i))
            DrawEchoGram_brut (SV (:,1:zmax,i)',-45,-90);
            colorbar
            colorbar ('location','southoutside')
            title (['RUN',str_run(ir,:),' ',num2str(fq(i)),'kHz'])   
        end
        subplot (2,4,[4 8])
        plot (RawProfils(1:zmax,2:size(RawProfils,2)), -RawProfils(1:zmax,1), 'LineWidth',2)
        title ('Raw median profils')
        legend ('18','38','70','120','200','333'); legend boxoff
        xlim([-100 -40]) 

        
        % -------
        % SAVING
        % -------       
        % filtered echograms
        figname5 = [path_save,'RUN',str_run(ir,:),'_Echograms'];
        saveas (figure(2),figname5,'jpeg');
        close 
        % median profil
        filename7 = [path_save,'RUN',str_run(ir,:),'_MedianProfils.mat'];
        save (filename7,'MEDIANPROF');
        % filtered data matrix
        filename8 = [path_save,'RUN',str_run(ir,:),'_CutData.mat'];
        save (filename8,'SV');
            
        close all
  
    end
    
end







