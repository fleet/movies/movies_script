% Clustering MFR FM

 clear all, close all;  


mydir  = pwd;
idcs   = strfind(mydir,'\')
pathUseful = [mydir(1:idcs(end-2)-1) '\UsefulFunctions'];

addpath(genpath(pathUseful)) 


%% Set paths
% path_hac_survey='C:\Users\lberger\Desktop\GAZCOGNE3\';

path_hac_survey='O:\GUERLEDAN_2019\HAC\FM\';
path_config = [path_hac_survey,'/Config_M3D/EI_lay']

%% 1. Echo-integration
%% add path for EI
mydir  = pwd;
idcs   = strfind(mydir,'\')
pathEI = [mydir(1:idcs(end-2)-1) '\EchoIntegration'];

addpath(genpath(pathEI)) 

thresholds=[-70];

datestart='10/10/2019';
dateend='10/10/2019';
heurestart='13:45:00';
heureend='14:15:00';
survey_name='GAZCOGNE3';
survey_name='GUERLEDAN';
runs=[ 1]; 
% runs=[5];

path_save=[path_hac_survey,'/Result/',survey_name]
%If save path does not exist, create it
if ~exist(path_save,'dir')
    mkdir(path_save)
end
path_anal=path_save;
   batch_EI_multiTHR(path_hac_survey,path_config,path_save,thresholds,runs,'',datestart,heurestart,dateend,heureend);



%% Bind EIlay results matrices

for ir=1:length(runs)
    for t=1:size(thresholds,2)
        str_run(ir,:)='000';        str_run(ir,end-length(num2str(runs(ir)))+1:end)=num2str(runs(ir));
        filename_ME70=[path_save,'/',survey_name,'-RUN' str_run(ir,:),'-TH' num2str(thresholds(t)),'-ME70-EIlay.mat'];
        filename_EK80=[path_save,'/',survey_name,'-RUN' str_run(ir,:),'-TH' num2str(thresholds(t)),'-EK80-EIlay.mat'];
        filename_EK80h=[path_save,'/',survey_name,'-RUN' str_run(ir,:),'-TH' num2str(thresholds(t)),'-EK80h-EIlay.mat'];
        path_results=[path_save,'/RUN' str_run(ir,:) '/',num2str(thresholds(:,t)),'/'];
        EIlayRes_bind(path_results,filename_ME70,filename_EK80,filename_EK80h,datestart,heurestart,dateend,heureend);
    end
end



    load([path_save,'/',survey_name,'-RUN' str_run,'-TH' num2str(thresholds),'-EK80-EIlay.mat']);
 
    Sv_ei_classif=Sv_surfEK80_db;



    for f=1:length(frequencies_EK80_db)
         frequencies{f}=frequencies_EK80_db{f}(1,:); 
    end
    
fond=max(depth_bottom_EK80_db{1});

frequencies_use_on_classif=2;



% frequencies_no_gap2 = cat(2, frequencies{1}, frequencies{2},frequencies{3}, frequencies{4},frequencies{5}, frequencies{6});     
% Sv_ei_no_gap2 = cat(3, Sv_ei_classif{1}, Sv_ei_classif{2},Sv_ei_classif{3}, Sv_ei_classif{4},Sv_ei_classif{5}, Sv_ei_classif{6}); 

if frequencies_use_on_classif == 6
    Sv_ei_no_gap = cat(3, Sv_ei_classif{1}, Sv_ei_classif{2},Sv_ei_classif{3}, Sv_ei_classif{4},Sv_ei_classif{5}, Sv_ei_classif{6}); 
    frequencies_no_gap = cat(2, frequencies{1}, frequencies{2},frequencies{3}, frequencies{4},frequencies{5}, frequencies{6}); 
     frequencies_center= [18 38 70 120 200 333];
elseif frequencies_use_on_classif == 5
    Sv_ei_no_gap = cat(3, Sv_ei_classif{1}, Sv_ei_classif{2},Sv_ei_classif{3}, Sv_ei_classif{4},Sv_ei_classif{5}); 
    frequencies_no_gap = cat(2, frequencies{1}, frequencies{2},frequencies{3}, frequencies{4},frequencies{5}); 
    frequencies_center= [18 38 70 120 200 ];
elseif frequencies_use_on_classif == 4
    Sv_ei_no_gap = cat(3, Sv_ei_classif{1}, Sv_ei_classif{2}(:,:,2:end-1),Sv_ei_classif{3}(:,:,2:end-1), Sv_ei_classif{4}(:,:,2:end-1)); 
    frequencies_no_gap = cat(2, frequencies{1}, frequencies{2}(2:end-1),frequencies{3}(2:end-1), frequencies{4}(2:end-1)); 
    frequencies_center= [18 38 70 120 ];
elseif frequencies_use_on_classif == 3
    Sv_ei_no_gap = cat(3, Sv_ei_classif{1}, Sv_ei_classif{2}(:,:,2:end-1),Sv_ei_classif{3}(:,:,2:end-1)); 
    frequencies_no_gap = cat(2, frequencies{1}, frequencies{2}(2:end-1),frequencies{3}(2:end-1)); 
    frequencies_center= [18 38 70  ];
elseif frequencies_use_on_classif == 2
    Sv_ei_no_gap = cat(3, Sv_ei_classif{1}, Sv_ei_classif{2}(:,:,2:end-1)); 
    frequencies_no_gap = cat(2, frequencies{1}, frequencies{2}(2:end-1)); 
    frequencies_center= [ 70  200];
end    



for x = 1:size(Sv_ei_no_gap,3)
    Sv_ei_tmp = reshape(Sv_ei_no_gap(:,:,x), 1, []);
    Sv_ei_clustering(:,x) = Sv_ei_tmp;
     %filter values below threshold on one frequency
        indfilter= Sv_ei_tmp==-100;
%       Sv_ei_clustering(indfilter,:) = nan;
end

% Sv_ei_clustering(any(isnan(Sv_ei_clustering),2),:) = [];





 %% classif %%
    % divis� par la moyenne du spectre?
    if (0)
    mean_sv = repmat(10*log10(nanmean(10.^(Sv_ei_clustering/10),2)),1,size(Sv_ei_clustering,2));
    Sv_ei_clustering2 = Sv_ei_clustering./mean_sv; 
    else
    Sv_ei_clustering2 = Sv_ei_clustering;
    end


X = Sv_ei_clustering2; % on ne prend que les 6 premieres CP pour le clustering


% algorithme Kmeans (ca peut etre long...)
k=3; %nb cluster
opt = statset('Display','final','MaxIter',100); 
[IDX,C,sumd] = kmeans(X,k,'Replicates',10,'Options',opt,'start','cluster','emptyaction','singleton'); 


% changement de structure des facteurs 
idx_wide = squeeze(reshape(IDX,size(Sv_ei_no_gap,1),[],size(Sv_ei_no_gap,2)));
indnan=(isnan(idx_wide));
idx_wide(indnan)=k+1;

MDCLU = [];
QUANT25 = [];
QUANT75 = [];
for clu = 1:k+1
    MDF = []; 
    quant25 = [];
    quant75 = [];
    for f = 1:size(Sv_ei_no_gap,3)
        svrawf = 10.^(Sv_ei_no_gap(:,:,f)./10); 
        mdf = 10*log10(median(svrawf(IDX == clu)));    
        %mdf = 10*log10(quantile((svrawf(IDX == clu)),0.5));
        qt25 = 10*log10(quantile((svrawf(IDX == clu)),0.25));
        qt75 = 10*log10(quantile((svrawf(IDX == clu)),0.75));
        MDF = [MDF,mdf];
        quant25 = [quant25, qt25];
        quant75 = [quant75, qt75];
    end
    MDCLU = [MDCLU;MDF]; 
    QUANT25 = [QUANT25; quant25];
    QUANT75 = [QUANT75; quant75];
end
% 

%% figure

figure(14)
    % echogrammes aux 5 frequences
    for f = 1:frequencies_use_on_classif
        if frequencies_use_on_classif>4
            subplot(3,3,f);
        else
            subplot(2,4,f);
        end
        DrawEchoGram_brut(squeeze(10*log10(nanmean(10.^(Sv_ei_classif{f}(:,:,:)/10),3)))',-30,-100);
        title([num2str(frequencies_center(f)),' kHz'],'FontSize',11);
        ylim([1 fond])
       freezeColors
    end

    col2='wrmgcbyk';
    
    % reponses frequentielles medianes par cluster
    c = colormap(hsv(k+1)); % jeu des couleurs pour le clustering % hsv
    txtleg = []; txtcolbar = [];
    if frequencies_use_on_classif>4
        subplot(3,3,7);
    else
        subplot(2,4,5);
    end
    
    hold on
    for j = 1:k
        plot(squeeze(frequencies_no_gap(1:size(Sv_ei_no_gap,3))), MDCLU(j,1:size(Sv_ei_no_gap,3))','Color',c(j,:),'LineWidth',2,'Marker','*'); %size(MDCLU,2)
        txtleg = [txtleg {['cluster ',num2str(j)]}];    % texte pour la legende
        txtcolbar = [txtcolbar {num2str(j)}];       % texte pour la barre des couleurs
    end

     ylim([-100 -55]);
    %set(gca,'XTickLabel',{'18','38','70','120','200'},'XTick',1:5,'FontSize',11)
    title('Reponses frequentielles medianes par cluster','FontSize',11);
    xlabel('Frequences [kHz]'); ylabel('Intensite acoustique [dB]');
    legend(txtleg,4); legend boxoff

    % classification des cellules
     if frequencies_use_on_classif>4
            subplot(3,3,8:9);
        else
            subplot(2,4,6:8);
     end
       c = colormap(hsv(k+1)); % jeu des couleurs pour le clustering % hsv
     imagesc(idx_wide');
    ylim([1 fond])

     grid on;
    title(['Transect - cells classification'],'FontSize',11);
    saveas(figure(14),[path_hac_survey,'threshold_',num2str(thresholds),'_timestart_',heurestart(1:2),'_timeend_',heureend(1:2),'_ResultatFinal_',num2str(k),'clust'],'jpeg');
    saveas(figure(14),[path_hac_survey,'threshold_',num2str(thresholds),'_timestart_',heurestart(1:2),'_timeend_',heureend(1:2),'_ResultatFinal_',num2str(k),'clust'],'fig');


c = colormap(hsv(k)); % jeu des couleurs pour le clustering
txtleg = []; figure(15); hold on
for j = 1:k
    plot(squeeze(frequencies_no_gap(1:size(Sv_ei_no_gap,3))) , MDCLU(j,1:size(Sv_ei_no_gap,3))','LineStyle', '-','LineWidth',2,'Color',c(j,:),'Marker','*')
    hold on
    %size(MDCLU,2)
    txtleg = [txtleg {['cluster ',num2str(j)]}];    % texte pour la legende
end
%set(gca,'XTickLabel',{'18','38','70','120','200','333'},'XTick',1:6,'FontSize',11)
title('Reponses frequentielles medianes par cluster','FontSize',11);
xlabel('Frequences [kHz]'); ylabel('Intensite acoustique [dB]');
% 
% legend(txtleg,4); legend boxoff
 hold on
%   for j = 1:k
%   plot(squeeze(frequencies_no_gap(1:size(Sv_ei_no_gap,3))), QUANT25(j,1:size(Sv_ei_no_gap,3))','Color',c(j,:),'LineStyle', '--','LineWidth',1,'Marker','*') %size(QUANT25,2)
%   hold on
%   plot(squeeze(frequencies_no_gap(1:size(Sv_ei_no_gap,3))), QUANT75(j,1:size(Sv_ei_no_gap,3))','Color',c(j,:),'LineStyle', '--','LineWidth',1,'Marker','*') %size(QUANT75,2)% 
%   end
   ylim([-100 -45]);
