% Analyse des �chos correspondants aux couches diffusantes selon leurs 
% r�ponses fr�quentielles en prenant en compte toutes les radiales d'une 
% campagne.
% ------------------------------------------------------------------------
% 1. EI 
% 2. Assemblade des donn�es de toutes les radiales 
% 3. Standardisation
% 4. Tableau disjonctif complet sur les donn�es standardis�es
% 5. ACP
% 6. Kmeans clustering
% 7. R�ponses fr�quentielles par cluster
% 8. Repr�sentation graphique
% 9. Formatage des donn�es pour visualisation dans logiciel GLOBE
% ------------------------------------------------------------------------
% Dans le cas o� l'EI a d�j� �t� faite pour chaque radiale, aller
% directement � la section 2. pour l'analyse.
% ------------------------------------------------------------------------
% B. Remond 26/02/2014, L. Berger 09/05/2018
% ------------------------------------------------------------------------
% Nettoyage de l'historique de la console
clear all; close all

%Clean workspace
clear all;
close all;


%% Set paths
path_hac_survey='M:\CAMPAGNE_SEEK-LEAK\Donn�es\SEEK-LEAK-NAVIRE\DATA\HERMES_HAC'
path_config = [path_hac_survey,'/Config_M3D/EI_lay']

%% 1. Echo-integration
%% add path for EI
mydir  = pwd;
idcs   = strfind(mydir,'\')
pathEI = [mydir(1:idcs(end-2)-1) '\EchoIntegration'];

addpath(genpath(pathEI)) 

thresholds=[-85];

datestart='10/09/2017';
dateend='10/09/2017';
heurestart='07:30:00';
heureend='07:35:00';
survey_name='SEEKLEAK';
runs=[ 6]; 


path_save=[path_hac_survey,'/Result/',survey_name]
%If save path does not exist, create it
if ~exist(path_save,'dir')
    mkdir(path_save)
end
path_anal=path_save;
 batch_EI_multiTHR(path_hac_survey,path_config,path_save,thresholds,runs,'',datestart,heurestart,dateend,heureend);



%% Bind EIlay results matrices

for ir=1:length(runs)
    for t=1:size(thresholds,2)
        str_run(ir,:)='000';        str_run(ir,end-length(num2str(runs(ir)))+1:end)=num2str(runs(ir));
        filename_ME70=[path_save,'/',survey_name,'-RUN' str_run(ir,:),'-TH' num2str(thresholds(t)),'-ME70-EIlay.mat'];
        filename_ER60=[path_save,'/',survey_name,'-RUN' str_run(ir,:),'-TH' num2str(thresholds(t)),'-ER60-EIlay.mat'];
        filename_ER60h=[path_save,'/',survey_name,'-RUN' str_run(ir,:),'-TH' num2str(thresholds(t)),'-ER60h-EIlay.mat'];
        path_results=[path_save,'/RUN' str_run(ir,:) '/',num2str(thresholds(:,t)),'/'];
        EIlayRes_bind(path_results,filename_ME70,filename_ER60,filename_ER60h,datestart,heurestart,dateend,heureend);
    end
end

%% 2. Assemblage des donn�es de toutes les radiales pour l'analyse
% ---
% path_inputEI = 'Y:\PELGAS2015\Acoustique\Segmentation\';
% path_hac_survey = 'X:\PELGAS15\sauvegardePG15\HacPG15_hermes\'; %path to .hac files
% path_config = 'Y:\PELGAS2015\Acoustique\ConfigsM3D\configsAnalyseM3D\MfrSegmentation\'; %path to configuration folder
% path_save = 'Y:\PELGAS2015\Acoustique\Segmentation\'; %path to save
% path_results = path_save;
frequences =  [38 70 120 200 333 ];
nf=length(frequences);
 addpath('C:\src\git\movies3d\src\MATLABLINK\Sample\UsefulFunctions')

rad=[1]


SVraw = []; nbping = []; 
for i = 1:length(rad)
    SVcoordTrans = []; svtrans=[];
    load([path_save,'/',survey_name,'-RUN' str_run,'-TH' num2str(thresholds),'-ER60-EIlay.mat']);
    SV = Sv_surfER60_db;

%     % rangement des pings par ordre de longitude
%     SVcoordSort=[]; 
%     for f = 1:nf
%         SVcoord = [Sv_surfER60_db(:,:,f),squeeze(lon_surfER60_db(:,1,f))];
%         SVcoordSort(:,:,f) = sortrows(SVcoord,size(SVcoord,2));
%     end
%     SVcoordSort = SVcoordSort (:,:,:);  
%     SV = SVcoordSort(:,1:160,:); 
    SVraw = [SVraw;SV];
    
    % regroupement temps
%     time = time_ER60_db;
%     TIME = [TIME;time];
    
    % nombre de pings par radiale
    np = size(SV,1); 
    if i == 1
        p = np;
    else
        p = np + nbping(length(nbping));
    end
    nbping = [nbping;p] 
end
save([path_save,'SVraw.mat'],'SVraw'); 
save([path_save,'nbping.mat'],'nbping')
% ---
load([path_save,'SVraw.mat']); load([path_save,'nbping.mat']);

for r = rad
    
    % donn�es de la radiale r
    if r == 1
        svrawrad = 10.^(SVraw(1:nbping(r),:,:)./10);
        SVrawrad = SVraw(1:nbping(r),:,:);
    else
        svrawrad = 10.^(SVraw((nbping(r-1)+1):nbping(r),:,:)./10);
        SVrawrad = SVraw((nbping(r-1)+1):nbping(r),:,:);
    end
    %figure
    f1 = figure(1); set(f1,'Units','Normalized','OuterPosition',[0 0 1 1]); 
    % echogrammes aux 6 fr�quences
    for f = 1:nf
        subplot(3,3,f); imagesc(SVrawrad(:,1:150,f)');
        title(['Radiale ',num2str(r),' - ',num2str(frequences(f)),' kHz'],'FontSize',11);
        freezeColors
         
    end
    saveas(figure(1),[path_save,'MediumScale-Echograms'],'jpeg');
    saveas(figure(1),[path_save,'MediumScale-Echograms'],'fig');
   
    % echogrammes moyen
    meanEchog=mean(SVrawrad(:,1:150,:),3);
    size(meanEchog)
    
    f2=figure(2); set(f2,'Units','Normalized','OuterPosition',[0 0 1 1]);
    
        imagesc(meanEchog(:,1:150)');
        title(['Mean echogram'],'FontSize',11);
        freezeColors
    colorbar()
    
    saveas(figure(2),[path_save,'MediumScale-MeanEchograms'],'jpeg');
    saveas(figure(2),[path_save,'MediumScale-MeanEchograms'],'fig');

end    

%% 3. Standardisation des donn�es 
% par division de la valeur de chaque cellule � une fr�quence donn�e par 
% la valeur moyenne de la fr�quence

%filtrage des donn�es
% indfilter= SVraw<-90;
% Svraw(indfilter)=nan;

SVstd=[]; standbasedmean=[]; 
for f = 1:nf
     meanf = mean(mean(10.^(SVraw(:,:,f)./10)));
%      meanf=1;
    SVstdf = 10*log10((10.^(SVraw(:,:,f)./10))/meanf);
    SVstd(:,:,f) = SVstdf;
    standbasedmean = [standbasedmean;10*log10(meanf)];
end

% SVstd=SVraw;

% sauvegarde des objets
% save([path_anal,'StandBasedMean.mat'],'standbasedmean'); % les moyennes globales par fr�quence
save([path_anal,'SVstd.mat'],'SVstd'); 
% ---
% load([path_anal,'StandBasedMean.mat']); 
load([path_anal,'SVstd.mat']); 

% changement de structure des donn�es standardis�es
SVlong = squeeze(reshape(SVstd,[],size(SVstd,3)));

%% 4. Analyse des correspondances multiples
% sur les donn�es standardis�es
% ---


SVmax = max(max(SVlong))
SVmin = min(min(SVlong))

% d�finition des classes et construction du tableau disjonctif complet
% ---------- EI config = 1km x 1m --------------
% icfq = [];
% for f = 1:5
%     i5fq = SVlong(:,f) < ceil(SVmax) & SVlong(:,f) >= 20;
%     i4fq = SVlong(:,f) < 20 & SVlong(:,f) >= 7;
%     i3fq = SVlong(:,f) < 7 & SVlong(:,f) >= -6;
%     i2fq = SVlong(:,f) < -6 & SVlong(:,f) >= -19;
%     i1fq = SVlong(:,f) < -19 & SVlong(:,f) >= floor(SVmin);
%     icfq = [i5fq i4fq i3fq i2fq i1fq icfq];
% end
% ---------- EI config = 200m x 1m --------------
t=(SVmax-SVmin)/nf

icfq = [];
for f = 1:nf
    i5fq = SVlong(:,f) < ceil(SVmax) & SVlong(:,f) >= SVmax-t;
    i4fq = SVlong(:,f) < SVmax-t & SVlong(:,f) >= SVmax-2*t; 
    i3fq = SVlong(:,f) < SVmax-2*t & SVlong(:,f) >= SVmax-3*t;
    i2fq = SVlong(:,f) < SVmax-3*t & SVlong(:,f) >= SVmax-4*t;
    i1fq = SVlong(:,f) < SVmax-4*t & SVlong(:,f) >= floor(SVmin);
    icfq = [i5fq i4fq i3fq i2fq i1fq icfq];
end
save([path_anal,'TablDisjComp.mat'],'icfq');
% ---
load([path_anal,'TablDisjComp.mat']);

% nom des variables
% vlab = {'18_5','18_4','18_3','18_2','18_1','38_5','38_4','38_3','38_2','38_1',...
%     '70_5','70_4','70_3','70_2','70_1','120_5','120_4','120_3','120_2','120_1',...
%     '200_5','200_4','200_3','200_2','200_1','333_1','333_2','333_3','333_4','333_5'};

vlab = {'18_5','18_4','18_3','18_2','18_1','38_5','38_4','38_3','38_2','38_1',...
    '70_5','70_4','70_3','70_2','70_1','120_5','120_4','120_3','120_2','120_1',...
    '200_5','200_4','200_3','200_2','200_1'}

%% 5. Analyse en Composantes Principales 
% sur le tableau disjonctif complet
[coeff,score,latent] = princomp(icfq);
save([path_anal,'resPCA.mat'],'coeff','score','latent');
% ---
load([path_anal,'resPCA.mat']);

% Variance
f3= figure (3)
Var = 100*latent ./ sum(latent); VarCum = cumsum(Var);
hold on; bar(Var); plot(VarCum, '-or','LineWidth',2)
legend({'Var. par PC' 'Var. cumul�e'},4); legend boxoff
xlabel('Composantes Principales'); ylabel('Variance expliqu�e [%]')
saveas(figure(1),[path_anal,'varPCA'],'jpeg');close(1) 

% biplot (projection des variables et des individus dans l'espace des
% composantes principales)

% Le nombre de cellules (= d'individus) �tant trop important pour la visualisation
% graphique, s�lection al�atoire de 10000 individus projet�s dans l'espace
x = score(:,1); n = 10000;
nx = numel(x); inx = randperm(nx);
pc1 = score(inx(1:n),1);pc2 = score(inx(1:n),2);
pc3 = score(inx(1:n),3);pc4 = score(inx(1:n),4);

% pour une EI � grande �chelle, visualisation de tous les individus
% pc1 = score(:,1);pc2 = score(:,2);
% pc3 = score(:,3);pc4 = score(:,4);

f4 = figure(4); set(f4,'Units','Normalized','OuterPosition',[0 0 1 1]);
subplot(1,2,1); biplot(coeff(:,1:2),'Scores',[pc1,pc2],...
    'VarLabels',vlab)
xlabel (['PC1 - ',num2str(round(Var(1))),'%'])
ylabel (['PC2 - ',num2str(round(Var(2))),'%'])
title('10000 random scores')
subplot(1,2,2); biplot(coeff(:,3:4),'Scores',[pc3,pc4],...
    'VarLabels',vlab)
xlabel (['PC3 - ',num2str(round(Var(3))),'%'])
ylabel (['PC4 - ',num2str(round(Var(4))),'%'])                    
saveas(figure(1),[path_anal,'PC1-2_PC3-4'],'jpeg'); close(1)

%% 6. Kmeans clustering
% ---
X = score(:,1:6); % on ne prend que les 6 premi�res CP pour le clustering
k = 5;              % nombre de clusters

% algorithme Kmeans (�a peut �tre long...)
opt = statset('Display','final','MaxIter',100); 
[IDX,C,sumd] = kmeans(X,k,'Replicates',40,'Options',opt,'start','cluster','emptyaction','singleton'); 
save([path_anal,'facteurs_',num2str(k),'clusters'],'IDX','C','sumd');
% ---
load([path_anal,'facteurs_',num2str(k),'clusters']);

% changement de structure des facteurs 
idx_wide = squeeze(reshape(IDX,size(SVraw,1),[],size(SVraw,2)));

%% 7. Calcul des r�ponses fr�quentielles m�dianes par cluster
% ---
MDCLU = [];
QUANT25 = [];
QUANT75 = [];
for clu = 1:k
    MDF = []; 
    quant25 = [];
    quant75 = [];
        for f = 1:length(frequences)
        svrawf = 10.^(SVraw(:,:,f)./10); 
        mdf = 10*log10(mean(svrawf(IDX == clu)));    
        qt25 = 10*log10(quantile((svrawf(IDX == clu)),0.25));
        qt75 = 10*log10(quantile((svrawf(IDX == clu)),0.75));
        MDF = [MDF,mdf];
        quant25 = [quant25, qt25];
        quant75 = [quant75, qt75];
    end
    MDCLU = [MDCLU;MDF]; 
    QUANT25 = [QUANT25; quant25];
    QUANT75 = [QUANT75; quant75];
end

% save
save([path_save,'RepFreqMedianeByClust_',num2str(k),'clust'],'MDCLU');
load([path_save,'RepFreqMedianeByClust_',num2str(k),'clust']);
%% 8. Repr�sentation graphique de la classification des cellules
% --
for r = rad
    
    % donn�es de la radiale r
    if r == 1
        idxrad = idx_wide(1:nbping(r),:,:);
        svrawrad = 10.^(SVraw(1:nbping(r),:,:)./10);
        SVrawrad = SVraw(1:nbping(r),:,:);
    else
        idxrad = idx_wide((nbping(r-1)+1):nbping(r),:,:);
        svrawrad = 10.^(SVraw((nbping(r-1)+1):nbping(r),:,:)./10);
        SVrawrad = SVraw((nbping(r-1)+1):nbping(r),:,:);
    end
    

 % figure
    f = figure(2); set(f,'Units','Normalized','OuterPosition',[0 0 1 1]); 
    % echogrammes aux 6 fr�quences
    colormap default;
    for f = 1:nf
        subplot(3,3,f); imagesc(SVrawrad(:,:,f)');
        title(['Radiale ',num2str(r),' - ',num2str(frequences(f)),' kHz'],'FontSize',11);
        freezeColors
        axis([1 nbping 0 80]);
    end

    % r�ponses fr�quentielles m�dianes par cluster
    c = colormap(hsv(k)); % jeu des couleurs pour le clustering
    txtleg = []; txtcolbar = []; subplot(3,3,7); hold on
    for j = 1:k
         plot(QUANT25(j,1:size(QUANT25,2))','Color',c(j,:),'LineStyle', '--','LineWidth',1)
        hlinec(j)=plot(MDCLU(j,1:size(MDCLU,2))','Color',c(j,:),'LineWidth',2)
         plot(QUANT75(j,1:size(QUANT75,2))','Color',c(j,:),'LineStyle', '--','LineWidth',1)
        txtleg = [txtleg {['cluster ',num2str(j)]}];    % texte pour la l�gende
        txtcolbar = [txtcolbar {num2str(j)}];       % texte pour la barre des couleurs
    end
    set(gca,'XTickLabel',{'38','70','120','200','333'},'XTick',1:6,'FontSize',11)
    title('R�ponses fr�quentielles m�dianes par cluster','FontSize',11);
    xlabel('Fr�quences [kHz]'); ylabel('Intensit� acoustique [dB]');
    legend(hlinec,txtleg,4); legend boxoff

    % classification des cellules
    subplot(3,3,8:9); imagesc(idxrad'); 
    colorbar ('location','eastoutside','XTickLabel',txtcolbar,'XTick',[1:k]) 
    title(['Transect ',num2str(r),' - cells classification'],'FontSize',11);
axis([1 nbping 0 80]);
    saveas(figure(2),[path_anal,'RAD',num2str(r),'_ResultatFinal_',num2str(k),'clust'],'jpeg');
    saveas(figure(2),[path_anal,'RAD',num2str(r),'_ResultatFinal_',num2str(k),'clust'],'fig');
    
end

% figure des r�ponses fr�quentielles
c = colormap(hsv(k)); % jeu des couleurs pour le clustering
txtleg = []; figure(1); hold on
for j = 1:k
       plot(QUANT25(j,1:size(QUANT25,2))','Color',c(j,:),'LineStyle', '--','LineWidth',1)
        hlinec(j)=plot(MDCLU(j,1:size(MDCLU,2))','Color',c(j,:),'LineWidth',2)
         plot(QUANT75(j,1:size(QUANT75,2))','Color',c(j,:),'LineStyle', '--','LineWidth',1)
    txtleg = [txtleg {['cluster ',num2str(j)]}];    % texte pour la l�gende
end
set(gca,'XTickLabel',{'38','70','120','200','333'},'XTick',1:6,'FontSize',11)
title('R�ponses fr�quentielles m�dianes par cluster','FontSize',11);
xlabel('Fr�quences [kHz]'); ylabel('Intensit� acoustique [dB]');
legend(hlinec,txtleg,4); legend boxoff
saveas(figure(1),[path_anal,'ReponseFrequentielle_',num2str(k),'clust'],'jpeg');
saveas(figure(1),[path_anal,'ReponseFrequentielle_',num2str(k),'clust'],'fig');

