% Analyse des �chos correspondants aux couches diffusantes selon leurs 
% r�ponses fr�quentielles en prenant en compte toutes les radiales d'une 
% campagne.
% ------------------------------------------------------------------------
% 1. EI + assemblage des r�sultats par radiale
% 2. Assemblade des donn�es de toutes les radiales 
% 3. Standardisation
% 4. Tableau disjonctif complet sur les donn�es standardis�es
% 5. ACP
% 6. Kmeans clustering
% 7. R�ponses fr�quentielles par cluster
% 8. Repr�sentation graphique
% 9. Formatage des donn�es pour visualisation dans logiciel GLOBE
% ------------------------------------------------------------------------
% Dans le cas o� l'EI a d�j� �t� faite pour chaque radiale, aller
% directement � la section 2. pour l'analyse.
% ------------------------------------------------------------------------
% B. Remond 26/02/2014, M. Doray, 09/05/2015, ajout import de jackpot
% ------------------------------------------------------------------------
% Nettoyage de l'historique de la console
clear all; close all

%% import jackpot file
%----------------------------------
%path of jackpot file
path_evt='T:\PELGAS2015\Casino\PELGAS2015_jackpot.csv';
%import
rads=[1:2];

[run,radiale,transect,DateStart,DateEnd,TimeStart,TimeEnd]=import_jackpot(path_evt,rads);

path_export='T:\PELGAS2015\Acoustique\Segmentation\Jackpot4Matlab.mat';

save(path_export,'run','radiale','transect','DateStart','DateEnd','TimeStart','TimeEnd')

% ------------------------------------------------------------------------
% Param�tres d'entr�e pour l'�cho-int�gration.
% EI par radiale selon les bornes des fractions de prospection acoustiques
% d�finies dans le fichier inputsEIGotoStop.csv
path_inputEI = 'T:\PELGAS2015\Acoustique\Segmentation\';
path_hac_survey = 'M:\PELGAS15\sauvegardePG15\HacPG15_hermes\'; %path to .hac files
path_config = 'T:\PELGAS2015\Acoustique\ConfigsM3D\configsAnalyseM3D\MfrSegmentation\'; %path to configuration folder
path_save = 'T:\PELGAS2015\Acoustique\Segmentation\'; %path to save
path_results = path_save;
thresholds = [-80];
frequences = [18 38 70 120 200 333];
nf=length(frequences);
% ---------------
% Lecture des fichiers des inputs
load([path_inputEI,'Jackpot4Matlab.mat'])

% x = load([path_inputEI,'radiale.mat']); radiale = x.vectrad;
% x = load([path_inputEI,'transects.mat']); transect = x.vecttrans;
% x = load([path_inputEI,'runs.mat']); run = x.vectrun;
% x = load([path_inputEI,'DateStart.mat']); DateStart = cell2mat(x.vectds);
% x = load([path_inputEI,'TimeStart.mat']); TimeStart = cell2mat(x.vectts);
% x = load([path_inputEI,'DateEnd.mat']); DateEnd = cell2mat(x.vectde);
% x = load([path_inputEI,'TimeEnd.mat']); TimeEnd = cell2mat(x.vectte);

%% 1. Echo-int�gration et assemblage des r�sultats par radiale
% ---
rad=[1]

for i = rad % boucle sur les radiales
    
    selradi = radiale == i; % s�lection des transects composant la radiale
    radi = radiale(selradi)
    transi = transect(selradi)
    runi = run(selradi)
    dsradi = DateStart(selradi,:)
    tsradi = TimeStart(selradi,:)
    deradi = DateEnd(selradi,:)
    teradi = TimeEnd(selradi,:)
    
    for j = 1:max(transi) % boucle sur les transects composant la radiale
%         j=3
        seltransj = transi == j
        runj = runi(seltransj)
        dstransj = dsradi(seltransj,:)
        tstransj = tsradi(seltransj,:)
        detransj = deradi(seltransj,:)
        tetransj = teradi(seltransj,:)

        % ------------------------------------
        % Echo-integration du transect j de la radiale i
        nameTransect = ['Rad',num2str(i),'_transect',num2str(j)];
        dateStart = dstransj; timeStart = tstransj;
        dateEnd = detransj; timeEnd = tetransj;
        runs = runj;

        batch_EI_multiTHR(path_hac_survey,path_config,path_save,thresholds,runs,nameTransect,dateStart,timeStart,dateEnd,timeEnd);
        
        % ------------------------------------
        % Assemblage des r�sultats de l'EI du transect j de la radiale i
        filename_ER60 = [path_save,'RAD',num2str(i),'-Transect',num2str(j)];
        if runj < 10
            path_results = [path_save,'RUN00',num2str(runj),'\-80\'];
        else
            path_results = [path_save,'RUN0',num2str(runj),'\-80\'];
        end
        EIlayRes_bind_transect(i,j,path_results,filename_ER60);
        
    end %--- transect j
    
    % ------------------------------------
    % Assemblage des r�sultats de l'EI pour les transects composant la
    % radiale i

    % matrices initiales
    data = [];
    depth_bottom_ER60_db = []; depth_surface_ER60_db = [];
    lat_surfER60_db = []; lon_surfER60_db = [];
    lat_botER60_db = []; lon_botER60_db = [];
    Sa_surfER60_db = []; Sa_botER60_db = [];
    Sv_surfER60_db = []; Sv_botER60_db = [];
    vol_surfER60_db = []; time_ER60_db = [];

    for j = 1:max(transi)        
        seltransj = transi == j
        runj = runi(seltransj)

        if j == 1
            data1 = load([path_save,'RAD',num2str(i),'-Transect1.mat']);
            
            if size(size(data1.depth_bottom_ER60_db),2) == 3
                depth_bottom_ER60_db = data1.depth_bottom_ER60_db(:,:,1:nf);
                depth_surface_ER60_db = data1.depth_surface_ER60_db(:,:,1:nf);
                lat_surfER60_db = data1.lat_surfER60_db(:,:,1:nf);
                lon_surfER60_db = data1.lon_surfER60_db(:,:,1:nf);
                lat_botER60_db = data1.lat_botER60_db(:,:,1:nf);
                lon_botER60_db = data1.lon_botER60_db(:,:,1:nf);
                Sa_surfER60_db = data1.Sa_surfER60_db(:,:,1:nf);
                Sa_botER60_db = data1.Sa_botER60_db(:,:,1:nf);
                Sv_surfER60_db = data1.Sv_surfER60_db(:,:,1:nf);
                Sv_botER60_db = data1.Sv_botER60_db(:,:,1:nf);
                vol_surfER60_db = data1.vol_surfER60_db(:,:,1:nf);
                time_ER60_db = data1.time_ER60_db(:,1:nf);
            else
                depth_bottom_ER60_db = data1.depth_bottom_ER60_db(:,1:nf);
                depth_surface_ER60_db = data1.depth_surface_ER60_db(:,:,1:nf);
                lat_surfER60_db = data1.lat_surfER60_db(:,:,1:nf);
                lon_surfER60_db = data1.lon_surfER60_db(:,:,1:nf);
                lat_botER60_db = data1.lat_botER60_db(:,1:nf);
                lon_botER60_db = data1.lon_botER60_db(:,1:nf);
                Sa_surfER60_db = data1.Sa_surfER60_db(:,:,1:nf);
                Sa_botER60_db = data1.Sa_botER60_db(:,1:nf);
                Sv_surfER60_db = data1.Sv_surfER60_db(:,:,1:nf);
                Sv_botER60_db = data1.Sv_botER60_db(:,1:nf);
                vol_surfER60_db = data1.vol_surfER60_db(:,:,1:nf);
                time_ER60_db = data1.time_ER60_db;
            end
        else 
            dataj = load([path_save,'RAD',num2str(i),'-Transect',num2str(j),'.mat']);
            
            if sum(size(dataj.depth_bottom_ER60_db)) ~= 0
            
                if size(size(depth_bottom_ER60_db),2) < size(size(dataj.depth_bottom_ER60_db),2)
                    depth_bottom_ER60_db_2 = reshape(depth_bottom_ER60_db,size(depth_bottom_ER60_db,1),1,size(depth_bottom_ER60_db,2));
                    depth_bottom_ER60_db = depth_bottom_ER60_db_2;
                    lat_botER60_db_2 = reshape(lat_botER60_db,size(lat_botER60_db,1),1,size(lat_botER60_db,2));
                    lat_botER60_db = lat_botER60_db_2;
                    lon_botER60_db_2 = reshape(lon_botER60_db,size(lon_botER60_db,1),1,size(lon_botER60_db,2));
                    lon_botER60_db = lon_botER60_db_2;
                    Sa_botER60_db_2 = reshape(Sa_botER60_db,size(Sa_botER60_db,1),1,size(Sa_botER60_db,2)); 
                    Sa_botER60_db = Sa_botER60_db_2;
                    Sv_botER60_db_2 = reshape(Sv_botER60_db,size(Sv_botER60_db,1),1,size(Sv_botER60_db,2)); 
                    Sv_botER60_db = Sv_botER60_db_2;
                elseif size(size(depth_bottom_ER60_db),2) > size(size(dataj.depth_bottom_ER60_db),2)
                    depth_bottom_ER60_db = squeeze(depth_bottom_ER60_db);
                    lat_botER60_db = squeeze(lat_botER60_db);
                    lon_botER60_db = squeeze(lon_botER60_db);
                    Sa_botER60_db = squeeze(Sa_botER60_db);
                    Sv_botER60_db = squeeze(Sv_botER60_db);                
                end

                if size(size(dataj.depth_bottom_ER60_db),2) == 3
                    depth_bottom_ER60_db = [depth_bottom_ER60_db;dataj.depth_bottom_ER60_db(:,:,1:5)];
                    depth_surface_ER60_db = [depth_surface_ER60_db;dataj.depth_surface_ER60_db(:,:,1:5)];
                    lat_surfER60_db = [lat_surfER60_db;dataj.lat_surfER60_db(:,:,1:5)];
                    lon_surfER60_db = [lon_surfER60_db;dataj.lon_surfER60_db(:,:,1:5)];
                    lat_botER60_db = [lat_botER60_db;dataj.lat_botER60_db(:,:,1:5)];
                    lon_botER60_db = [lon_botER60_db;dataj.lon_botER60_db(:,:,1:5)];
                    Sa_surfER60_db = [Sa_surfER60_db;dataj.Sa_surfER60_db(:,:,1:5)];
                    Sa_botER60_db = [Sa_botER60_db;dataj.Sa_botER60_db(:,:,1:5)];
                    Sv_surfER60_db = [Sv_surfER60_db;dataj.Sv_surfER60_db(:,:,1:5)];
                    Sv_botER60_db = [Sv_botER60_db;dataj.Sv_botER60_db(:,:,1:5)];
                    vol_surfER60_db = [vol_surfER60_db;dataj.vol_surfER60_db(:,:,1:5)];
                    time_ER60_db = [time_ER60_db,dataj.time_ER60_db(:,1:5)];
                else
                    depth_bottom_ER60_db = [depth_bottom_ER60_db;dataj.depth_bottom_ER60_db(:,1:5)];
                    depth_surface_ER60_db = [depth_surface_ER60_db;dataj.depth_surface_ER60_db(:,:,1:5)];
                    lat_surfER60_db = [lat_surfER60_db;dataj.lat_surfER60_db(:,:,1:5)];
                    lon_surfER60_db = [lon_surfER60_db;dataj.lon_surfER60_db(:,:,1:5)];
                    lat_botER60_db = [lat_botER60_db;dataj.lat_botER60_db(:,1:5)];
                    lon_botER60_db = [lon_botER60_db;dataj.lon_botER60_db(:,1:5)];
                    Sa_surfER60_db = [Sa_surfER60_db;dataj.Sa_surfER60_db(:,:,1:5)];
                    Sa_botER60_db = [Sa_botER60_db;dataj.Sa_botER60_db(:,1:5)];
                    Sv_surfER60_db = [Sv_surfER60_db;dataj.Sv_surfER60_db(:,:,1:5)];
                    Sv_botER60_db = [Sv_botER60_db;dataj.Sv_botER60_db(:,1:5)];
                    vol_surfER60_db = [vol_surfER60_db;dataj.vol_surfER60_db(:,:,1:5)];
                    time_ER60_db = [time_ER60_db,dataj.time_ER60_db];

                end
            end
        end
    end
    radname = ['PELGAS13-RAD',num2str(i),'-TH-80-ER60-EIlay.mat'];
    save([path_save,radname],'depth_bottom_ER60_db','depth_surface_ER60_db','lat_surfER60_db',...
        'lon_surfER60_db','lat_botER60_db','lon_botER60_db','Sa_surfER60_db','Sa_botER60_db',...
        'Sv_surfER60_db','Sv_botER60_db','vol_surfER60_db','time_ER60_db');

end % --- radiale i




%% 2. Assemblage des donn�es de toutes les radiales pour l'analyse
% ---
% path_anal = 'C:\Users\bremond\Documents\data\LargeScale\Approche5_StandCorrespAnalysis\wholedataset\results_analysis\'
%path_anal = 'C:\Users\bremond\Documents\data\LargeScale\last_results\results_analysis\'
path_anal='T:\PELGAS2014\Acoustique\Segmentation\'
path_anal='/media/mathieu/PELGAS2013_2/ReconstitutionRadiales/LargeScale/'
rad=[1:27]

SVraw = []; nbping = []; 
for i = 1:length(rad)
    SVcoordTrans = []; svtrans=[];
    load ([path_anal,'PELGAS13-RAD',num2str(i),'-TH-80-ER60-EIlay.mat']);
    SV = Sv_surfER60_db;

    % rangement des pings par ordre de longitude
    SVcoordSort=[]; 
    for f = 1:5
        SVcoord = [Sv_surfER60_db(:,:,f),squeeze(lon_surfER60_db(:,1,f))];
        SVcoordSort(:,:,f) = sortrows(SVcoord,size(SVcoord,2));
    end
    SVcoordSort = SVcoordSort (:,:,:);  
    SV = SVcoordSort(:,1:190,:); 
    SVraw = [SVraw;SV];
    
    % regroupement temps
%     time = time_ER60_db;
%     TIME = [TIME;time];
    
    % nombre de pings par radiale
    np = size(SV,1); 
    if i == 1
        p = np;
    else
        p = np + nbping(length(nbping));
    end
    nbping = [nbping;p] 
end
save([path_anal,'SVraw.mat'],'SVraw'); 
save([path_anal,'nbping.mat'],'nbping')
% ---
load([path_anal,'SVraw.mat']); load([path_anal,'nbping.mat']);

for r = rad
    
    % donn�es de la radiale r
    if r == 1
        svrawrad = 10.^(SVraw(1:nbping(r),:,:)./10);
        SVrawrad = SVraw(1:nbping(r),:,:);
    else
        svrawrad = 10.^(SVraw((nbping(r-1)+1):nbping(r),:,:)./10);
        SVrawrad = SVraw((nbping(r-1)+1):nbping(r),:,:);
    end
    
    % figure
    f = figure(2); set(f,'Units','Normalized','OuterPosition',[0 0 1 1]); 
    % echogrammes aux 5 fr�quences
    for f = 1:6
        subplot(3,3,f); DrawEchoGram_brut(SVrawrad(:,:,f)',-45,-90);
        title(['Radiale ',num2str(r),' - ',num2str(frequences(f)),' kHz'],'FontSize',11);
        freezeColors
    end
end    

%% 3. Standardisation des donn�es 
% par division de la valeur de chaque cellule � une fr�quence donn�e par 
% la valeur moyenne de la fr�quence
SVstd=[]; standbasedmean=[]; 
for f = 1:5
    meanf = mean(mean(10.^(SVraw(:,:,f)./10)));
    SVstdf = 10*log10((10.^(SVraw(:,:,f)./10))/meanf);
    SVstd(:,:,f) = SVstdf;
    standbasedmean = [standbasedmean;10*log10(meanf)];
end
% sauvegarde des objets
save([path_anal,'StandBasedMean.mat'],'standbasedmean'); % les moyennes globales par fr�quence
save([path_anal,'SVstd.mat'],'SVstd'); 
% ---
load([path_anal,'StandBasedMean.mat']); 
load([path_anal,'SVstd.mat']); 

% changement de structure des donn�es standardis�es
SVlong = squeeze(reshape(SVstd,[],size(SVstd,3)));

%% 4. Analyse des correspondances multiples
% sur les donn�es standardis�es
% ---
SVmax = max(max(SVlong))
SVmin = min(min(SVlong))

% d�finition des classes et construction du tableau disjonctif complet
% ---------- EI config = 1km x 1m --------------
% icfq = [];
% for f = 1:5
%     i5fq = SVlong(:,f) < ceil(SVmax) & SVlong(:,f) >= 20;
%     i4fq = SVlong(:,f) < 20 & SVlong(:,f) >= 7;
%     i3fq = SVlong(:,f) < 7 & SVlong(:,f) >= -6;
%     i2fq = SVlong(:,f) < -6 & SVlong(:,f) >= -19;
%     i1fq = SVlong(:,f) < -19 & SVlong(:,f) >= floor(SVmin);
%     icfq = [i5fq i4fq i3fq i2fq i1fq icfq];
% end
% ---------- EI config = 200m x 1m --------------
icfq = [];
for f = 1:5
    i5fq = SVlong(:,f) < ceil(SVmax) & SVlong(:,f) >= 40;
    i4fq = SVlong(:,f) < 40 & SVlong(:,f) >= 20;
    i3fq = SVlong(:,f) < 20 & SVlong(:,f) >= 0;
    i2fq = SVlong(:,f) < 0 & SVlong(:,f) >= -20;
    i1fq = SVlong(:,f) < -20 & SVlong(:,f) >= floor(SVmin);
    icfq = [i5fq i4fq i3fq i2fq i1fq icfq];
end
save([path_anal,'TablDisjComp.mat'],'icfq');
% ---
load([path_anal,'TablDisjComp.mat']);

% nom des variables
vlab = {'18_5','18_4','18_3','18_2','18_1','38_5','38_4','38_3','38_2','38_1',...
    '70_5','70_4','70_3','70_2','70_1','120_5','120_4','120_3','120_2','120_1',...
    '200_5','200_4','200_3','200_2','200_1'};

%% 5. Analyse en Composantes Principales 
% sur le tableau disjonctif complet
[coeff,score,latent] = princomp(icfq);
save([path_anal,'resPCA.mat'],'coeff','score','latent');
% ---
load([path_anal,'resPCA.mat']);

% Variance
Var = 100*latent ./ sum(latent); VarCum = cumsum(Var);
hold on; bar(Var); plot(VarCum, '-or','LineWidth',2)
legend({'Var. par PC' 'Var. cumul�e'},4); legend boxoff
xlabel('Composantes Principales'); ylabel('Variance expliqu�e [%]')
saveas(figure(1),[path_anal,'varPCA'],'jpeg'); close(1)

% biplot (projection des variables et des individus dans l'espace des
% composantes principales)

% Le nombre de cellules (= d'individus) �tant trop important pour la visualisation
% graphique, s�lection al�atoire de 10000 individus projet�s dans l'espace
x = score(:,1); n = 10000;
nx = numel(x); inx = randperm(nx);
pc1 = score(inx(1:n),1);pc2 = score(inx(1:n),2);
pc3 = score(inx(1:n),3);pc4 = score(inx(1:n),4);

% pour une EI � grande �chelle, visualisation de tous les individus
% pc1 = score(:,1);pc2 = score(:,2);
% pc3 = score(:,3);pc4 = score(:,4);

f1 = figure(1); set(f1,'Units','Normalized','OuterPosition',[0 0 1 1]);
subplot(1,2,1); biplot(coeff(:,1:2),'Scores',[pc1,pc2],...
    'VarLabels',vlab)
xlabel (['PC1 - ',num2str(round(Var(1))),'%'])
ylabel (['PC2 - ',num2str(round(Var(2))),'%'])
title('10000 random scores')
subplot(1,2,2); biplot(coeff(:,3:4),'Scores',[pc3,pc4],...
    'VarLabels',vlab)
xlabel (['PC3 - ',num2str(round(Var(3))),'%'])
ylabel (['PC4 - ',num2str(round(Var(4))),'%'])                    
saveas(figure(1),[path_anal,'PC1-2_PC3-4'],'jpeg'); close(1)

%% 6. Kmeans clustering
% ---
X = score(:,1:6); % on ne prend que les 6 premi�res CP pour le clustering
k = 6;              % nombre de clusters

% algorithme Kmeans (�a peut �tre long...)
opt = statset('Display','final','MaxIter',100); 
[IDX,C,sumd] = kmeans(X,k,'Replicates',40,'Options',opt,'start','cluster','emptyaction','singleton'); 
save([path_anal,'facteurs_',num2str(k),'clusters'],'IDX','C','sumd');
% ---
load([path_anal,'facteurs_',num2str(k),'clusters']);

% changement de structure des facteurs 
idx_wide = squeeze(reshape(IDX,size(SVraw,1),[],size(SVraw,2)));

%% 7. Calcul des r�ponses fr�quentielles m�dianes par cluster
% ---
MDCLU = [];
for clu = 1:k
    MDF = []; 
    for f = 1:length(frequences)
        svrawf = 10.^(SVraw(:,:,f)./10); 
        mdf = 10*log10(median(svrawf(IDX == clu)));    
        MDF = [MDF,mdf];
    end
    MDCLU = [MDCLU;MDF]; 
end
% save
save([path_save,'RepFreqMedianeByClust_',num2str(k),'clust'],'MDCLU');
load([path_save,'RepFreqMedianeByClust_',num2str(k),'clust']);

%% 8. Repr�sentation graphique de la classification des cellules
% --
for r = rad
    
    % donn�es de la radiale r
    if r == 1
        idxrad = idx_wide(1:nbping(r),:,:);
        svrawrad = 10.^(SVraw(1:nbping(r),:,:)./10);
        SVrawrad = SVraw(1:nbping(r),:,:);
    else
        idxrad = idx_wide((nbping(r-1)+1):nbping(r),:,:);
        svrawrad = 10.^(SVraw((nbping(r-1)+1):nbping(r),:,:)./10);
        SVrawrad = SVraw((nbping(r-1)+1):nbping(r),:,:);
    end
    
    % figure
    f = figure(2); set(f,'Units','Normalized','OuterPosition',[0 0 1 1]); 
    % echogrammes aux 5 fr�quences
    for f = 1:5
        subplot(3,3,f); DrawEchoGram_brut(SVrawrad(:,:,f)',-45,-90);
        title(['Radiale ',num2str(r),' - ',num2str(frequences(f)),' kHz'],'FontSize',11);
        freezeColors
    end

    % r�ponses fr�quentielles m�dianes par cluster
    c = colormap(HSV(k)); % jeu des couleurs pour le clustering
    txtleg = []; txtcolbar = []; subplot(3,3,7); hold on
    for j = 1:k
        plot(MDCLU(j,1:size(MDCLU,2))','Color',c(j,:),'LineWidth',2)
        txtleg = [txtleg {['cluster ',num2str(j)]}];    % texte pour la l�gende
        txtcolbar = [txtcolbar {num2str(j)}];       % texte pour la barre des couleurs
    end
    set(gca,'XTickLabel',{'18','38','70','120','200'},'XTick',1:5,'FontSize',11)
    title('R�ponses fr�quentielles m�dianes par cluster','FontSize',11);
    xlabel('Fr�quences [kHz]'); ylabel('Intensit� acoustique [dB]');
    legend(txtleg,4); legend boxoff

    % classification des cellules
    subplot(3,3,8:9); imagesc(idxrad'); 
    colorbar ('location','eastoutside','XTickLabel',txtcolbar,'XTick',[1:k]) 
    title(['Transect ',num2str(r),' - cells classification'],'FontSize',11);

    saveas(figure(2),[path_anal,'RAD',num2str(r),'_ResultatFinal_',num2str(k),'clust'],'jpeg');
    saveas(figure(2),[path_anal,'RAD',num2str(r),'_ResultatFinal_',num2str(k),'clust'],'fig');
    close(2)
end

% figure des r�ponses fr�quentielles
c = colormap(HSV(k)); % jeu des couleurs pour le clustering
txtleg = []; figure(1); hold on
for j = 1:k
    plot(MDCLU(j,1:size(MDCLU,2))','Color',c(j,:),'LineWidth',2)
    txtleg = [txtleg {['cluster ',num2str(j)]}];    % texte pour la l�gende
end
set(gca,'XTickLabel',{'18','38','70','120','200'},'XTick',1:5,'FontSize',11)
title('R�ponses fr�quentielles m�dianes par cluster','FontSize',11);
xlabel('Fr�quences [kHz]'); ylabel('Intensit� acoustique [dB]');
legend(txtleg,4); legend boxoff

saveas(figure(1),[path_anal,'ReponseFrequentielle_',num2str(k),'clust'],'jpeg');
saveas(figure(1),[path_anal,'ReponseFrequentielle_',num2str(k),'clust'],'fig');


%% 9. Formatage donn�es pour inputs GLOBE.

clear all
path_resEI = 'C:\Users\bremond\Documents\data\LargeScale\last_results\results_EI_GoToStop_corrected\';
path_resAnalysis = 'C:\Users\bremond\Documents\data\LargeScale\last_results\results_analysis\';
path_save = 'C:\Users\bremond\Documents\data\LargeScale\last_results\DataForGLOBE\';
rad = [1:24];

% initial matrices
depth_surface_ER60_db = [];
lat_surfER60_db = [];
lon_surfER60_db = [];
Sa_surfER60_db = [];
Sv_surfER60_db = [];
time_ER60_db = [];
vol_surfER60_db = [];
idx_ER60 = [];
NbESU = [];

for ir = 1:length(rad)
%     ir=15
    SVcoordTrans = []; svtrans=[];
%     str_run(ir,:)='000'; str_run(ir,end-length(num2str(rad(ir)))+1:end)=num2str(rad(ir));

    % Echo-Integration data
    dataload = load ([path_resEI,'/PELGAS13-RAD',num2str(ir),'-TH-80-ER60-EIlay.mat']);% whole dataset + 200mScale
    
    % ordination par le temps
    
    
    % regroupement
    depth_surface_ER60_db = [depth_surface_ER60_db;dataload.depth_surface_ER60_db];
    lat_surfER60_db = [lat_surfER60_db;dataload.lat_surfER60_db];
    lon_surfER60_db = [lon_surfER60_db;dataload.lon_surfER60_db];
    Sa_surfER60_db = [Sa_surfER60_db;dataload.Sa_surfER60_db];
    Sv_surfER60_db = [Sv_surfER60_db;dataload.Sv_surfER60_db];
    if size(dataload.time_ER60_db,1) > 1
        time_ER60_db = [time_ER60_db,dataload.time_ER60_db(1,:)];
    else
        time_ER60_db = [time_ER60_db,dataload.time_ER60_db];
    end
    vol_surfER60_db = [vol_surfER60_db;dataload.vol_surfER60_db];
    
    
    
    % ESU number by transect
    NbESUtransect = size(dataload.Sv_surfER60_db,1); 
    if ir == 1
        p = NbESUtransect;
    else
        p = NbESUtransect + NbESU(length(NbESU));
    end
    NbESU = [NbESU;p] 
    
end

save([path_save,'NbESU.mat'],'NbESU')
load([path_save,'NbESU.mat'])
% Kmeans clustering data
dataanalysis = load([path_resAnalysis,'facteurs_6clusters.mat']);
idx_long_ER60 = dataanalysis.IDX;
idx_ER60 = squeeze(reshape(idx_long_ER60,size(Sv_surfER60_db,1),[],size(Sv_surfER60_db,2)));

% Meta donnees
sign1 = 'IFREMER';  %organisme producteur de la donn�e
sign2 = 'MOVIES3D'; %logiciel d'EI
sign3 = 'SSL_clustering';   %m�thode d'�chotypage 'SSL_clustering' ou 'Echotypage_Movies'
format = 20140228;          % date de changement du format de fichier

data = struct('Signature1',sign1,'Signature2',sign2,'Signature3',sign3,'Format',format,...
    'depth_surface_ER60_db',depth_surface_ER60_db,'lat_surfER60_db',lat_surfER60_db,...
    'lon_surfER60_db',lon_surfER60_db,'Sa_surfER60_db',Sa_surfER60_db,...
    'Sv_surfER60_db',Sv_surfER60_db,'time_ER60_db',time_ER60_db,...
    'vol_surfER60_db',vol_surfER60_db,'idx_ER60',idx_ER60,'NbESU',NbESU);

% Concatenating into a Matlab structure
save([path_save,'PELGAS13_SSLclustering_200m-1m.mat'],'data');
load([path_save,'PELGAS13_SSLclustering_200m-1m.mat']);

% V�rification des coordonn�es
figure(1); hold on
for r = 1:24
    if r == 1
        latrad = data.lat_surfER60_db(1:data.nESU(r),1,1);
        lonrad = data.lon_surfER60_db(1:data.nESU(r),1,1);
        plot(lonrad,latrad,'.k'); 
    else
        latrad = data.lat_surfER60_db((data.nESU(r-1)+1):data.nESU(r),1,1);
        lonrad = data.lon_surfER60_db((data.nESU(r-1)+1):data.nESU(r),1,1);
        plot(lonrad,latrad,'.k');
    end
end
saveas(figure(1),[path_save,'verif_coord'],'png')
saveas(figure(1),[path_save,'verif_coord'],'fig')

%%
path_resEI = 'C:\Users\bremond\Documents\data\LargeScale\last_results\results_EI_GoToStop_corrected\';

% metadata
sign1 = 'IFREMER';  %organisme producteur de la donn�e
sign2 = 'MOVIES3D'; %logiciel d'EI
sign3 = 'SSL_clustering';   %m�thode d'�chotypage 'SSL_clustering' ou 'Echotypage_Movies'
format = 20140228;          % date de changement du format de fichier

alldata = ();

for r = 12 % radiale
    radname = ['R',num2str(r)];
    data = load([path_resEI,'PELGAS13-RAD',num2str(r),'-TH-80-ER60-EIlay.mat']); % data
    
    if r == 1
        data.idx_ER60 = idx_wide(1:nbping(r),:,:);
    else
        data.idx_ER60 = idx_wide((nbping(r-1)+1):nbping(r),:,:);
    end
    data.name = radname;
    data.Signature1 = sign1;
    data.Signature2 = sign2;
    data.Signature3 = sign3;
    data.Format = format;
    
    alldata(1,1) = data;
end

data = struct('Signature1',sign1,'Signature2',sign2,'Signature3',sign3,'Format',format,...
    'depth_surface_ER60_db',depth_surface_ER60_db,'lat_surfER60_db',lat_surfER60_db,...
    'lon_surfER60_db',lon_surfER60_db,'Sa_surfER60_db',Sa_surfER60_db,...
    'Sv_surfER60_db',Sv_surfER60_db,'time_ER60_db',time_ER60_db,...
    'vol_surfER60_db',vol_surfER60_db,'idx_ER60',idx_ER60,'nESU',nESU);

