%% Lance la d�tection de fond de tous les fichiers contenus dans un repertoire donne 
% renseigner le chemin du r�pertoire � traiter et le r�pertoire o�
% sauvegarder les nouveaux hac 

    clear all;
    
    %chargement config
    chemin_config = '.\config_det\';
    chemin_config_reverse = strrep(chemin_config, '\', '/');
    moLoadConfig(chemin_config_reverse);
    config=1;

    chemin_hac='L:\PELGAS12\HAC\HAC_Mplus\HacBrutPG12\RUN014\';
    chemin_save='C:\data\test_fond\HacBrutPG12\RUN014\';
    
    
    % chargement des param�tres du d�tecteur de fond
    niv=([-50 -45 -40 -35 -30 -25 -20 -15 -10]); %lignes de niveau (croissant, donc de + en + profondes)
    iniv_ref=3; % ligne de r�f�rence pour faire la d�tection de fond
    
    if ~exist(chemin_save,'dir')
            mkdir(chemin_save)
    end
    
    filelist = ls([chemin_hac,'*.hac']);  % ensemble des fichiers
    nb_files = size(filelist,1);  % nombre de fichiers
    
    choix_sdr=0;

    for numfile = 1:nb_files  % boucle sur l'ensemble des fichiers � traiter
    %for numfile = 1:1
        hacfilename = filelist(numfile,:);
        
        %premi�re �tape de s�lection du sondeur � traiter
        if choix_sdr==0 %sondeur a filtrer pas encore choisi
            FileName = [chemin_hac,hacfilename];
            if ~exist(FileName,'file')
                disp('le fichier n existe pas');
                return
            end
            moOpenHac(FileName);
             %presentation des sondeurs
            list_sounder=moGetSounderList;
            nb_snd=length(list_sounder);
            disp(' ');
            disp('pr�sentation des sondeurs');
            disp('-------------------------');
            disp(' ');
            disp(['nb sondeurs = ' num2str(nb_snd)]);
            for isdr = 1:nb_snd
                nb_transduc=list_sounder(isdr).m_numberOfTransducer;
                if (list_sounder(isdr).m_SounderId<10)
    %                ind_list(list_sounder(isdr).m_SounderId)=isdr;
                    ind_list(isdr)=list_sounder(isdr).m_SounderId;                
                    %disp(['sondeur num�ro ' num2str(isdr) ':    ' '(index: ' num2str(list_sounder(isdr).m_SounderId) ')   nb trans:' num2str(nb_transduc)]);
                    disp(['sondeur num�ro ' num2str(isdr) ':    '  '   nb transducteurs:' num2str(nb_transduc)]);
                    for itr=1:nb_transduc
                        nb_soft_chnel(isdr,itr)=list_sounder(isdr).m_transducer(itr).m_numberOfSoftChannel;
                        disp(['   transducteur num�ro ' num2str(itr) ':    ' 'nom: ' list_sounder(isdr).m_transducer(itr).m_transName '   nb soft_chnl:' num2str(nb_soft_chnel(isdr,itr))]);
                        for isf=1:nb_soft_chnel(isdr,itr)
                            disp(['       soft_chn ' num2str(isf) ':    ' '   freq: ' num2str(list_sounder(isdr).m_transducer(itr).m_SoftChannel(isf).m_acousticFrequency/1000) ' kHz']);

                        end
                    end
                else
                    disp('Pb sondeur Id');
                end
            end
            choix_sdr=1;
            disp(' ');
            disp(' ');
            num_sondeur=input('>>> num�ro du sondeur � traiter: ');
            num_trans=input('>>> num�ro du transducteur � traiter: ');
            
            if (isempty(num_sondeur) || isempty(num_trans) || ~isnumeric(num_sondeur) || ~isnumeric(num_trans))
                disp(' ');
                disp('ERREUR: de saisie des num�ros');
                return;    
            elseif (num_sondeur>nb_snd)
                disp(' ');
                disp('ERREUR: le num�ro du sondeur � traiter n existe pas. Corriger le num�ro');
                return;
            elseif(num_trans>list_sounder(num_sondeur).m_numberOfTransducer)
                disp(' ');
                disp('ERREUR: l index du transducteur � traitrer n existe pas. Corriger l index');
                return;
            end
        end

       goto=-1;
       [goto,name_sdr]=det_fond_hac(chemin_hac,hacfilename,chemin_save,chemin_config,num_sondeur,num_trans,goto,niv,iniv_ref);
       ind=1;
       while goto>-1
           name_ini=[chemin_save 'det_' name_sdr '_' hacfilename(1:end-4) '.hac'];
           if ind==1
            movefile(name_ini,[chemin_save 'det_' name_sdr '_' hacfilename(1:end-4) '_' num2str(ind) '.hac']);
           end
           ind=ind+1;
           [goto,name_sdr]=det_fond_hac(chemin_hac,hacfilename,chemin_save,chemin_config,num_sondeur,num_trans,goto,niv,iniv_ref);
           movefile(name_ini,[chemin_save 'det_' name_sdr '_' hacfilename(1:end-4) '_' num2str(ind) '.hac']);
       end
   end


