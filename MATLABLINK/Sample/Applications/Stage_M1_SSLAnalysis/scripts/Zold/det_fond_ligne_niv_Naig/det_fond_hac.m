%function [ir_niv,ir_niv2,ir_chx,imax_prox,ir_ini,max_rec,roll,pitch,heave,time,sv_mat,al_mat,at_mat,heading,delta,lat,long,nom_sdr]=det_fond_hac(chemin_hac,hacfilename,chemin_save,chemin_config,num_sondeur,num_trans,goto,niv,iniv_ref)
function [goto,nom_sdr]=det_fond_hac(chemin_hac,hacfilename,chemin_save,chemin_config,num_sondeur,num_trans,goto,niv,iniv_ref)
% chemin_hac : r�pertoire contenant le fichier hac � filtrer
% hacfilename : nom du fichier hac � filtrer
% chemin_save : r�pertoire o� sauver le fichier hac filtr�
% chemin_config : r�pertoire contenant la configuration movies3D � utiliser (un r�pertoire exemple \config_filtre\ est joint). 
% num_sondeur: num�ro du sondeur contenant le transducteur � filtrer
% num_trans: num�ro du transducteur � filtrer
% goto: indice de temps o� commencer la lecture, -1 si on commence au d�but
% (goto = hh*3600+mm*60+ss+(datenum('date_fichier')-datenum('01-Jan-1970'))*24*3600;

%% chargement des param�tres du d�tecteur de fond
if (0)
    niv=fliplr([-10 -15 -20 -25 -30 -35 -40 -45 -50]);
    iniv_ref=3;
end

n_niv=length(niv);


FileName = [chemin_hac,hacfilename];
    
%% param�tres de lecture du hac
moLoadConfig(strrep(chemin_config, '\', '/'));

if ~exist(FileName,'file')
    disp('le fichier n existe pas');
    return
end

moOpenHac(FileName);

if goto>-1
    ParameterR= moReaderParameter();
    ParameterDef=moLoadReaderParameter(ParameterR);
    taille_chunk=ParameterDef.m_chunkNumberOfPingFan;
    ParameterDef.m_chunkNumberOfPingFan=1;
    moSaveReaderParameter(ParameterR,ParameterDef);
    
    %lit premier chunk
    moOpenHac(FileName);
    
    moGoTo(goto);
    moReadChunk(); %lit un ping suppl�mentaire
    
    %vide la m�moire des pings pr�c�dents
    nb_pings=moGetNumberOfPingFan();
    for ip=nb_pings-1:-1:1
        MX= moGetPingFan(ip-1);
        moRemovePing(MX.m_pingId);
    end
    
    ParameterDef.m_chunkNumberOfPingFan=taille_chunk;
    moSaveReaderParameter(ParameterR,ParameterDef);
else
    moOpenHac(FileName);
end

%% lecture et filtrage
prem_lec=1;
iping=0;
FileStatus= moGetFileStatus;
nb_pings_prec=0;
ilec=0;
time=[];
heave=[];
heading=[];
roll=[];
pitch=[];


ir=[];

bottomf_ini=[];
range_ini=[];
ir_ini=[];
sv_sup_ini=[];
sv_sup_fin=[];
imax_prox=[];
prof_prox=[];
limax=[];
max_rec=[];
sv_mat=[];
al_mat=[];
at_mat=[];
lat=[];
long=[];

while FileStatus.m_StreamClosed < 1 || ilec==0
%while (ilec < 150 & FileStatus.m_StreamClosed < 1)
    ilec=ilec+1;
    
    %% lecture %%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    disp('debut readchunk')
    moReadChunk();
    disp('fin readchunk')
    FileStatus= moGetFileStatus();
    
    if (prem_lec==1) %premier ping lu, on pr�sente les sondeurs et choisit celui � traiter
        prem_lec=0;
        
        %presentation des sondeurs
        list_sounder=moGetSounderList;
        nb_snd=length(list_sounder);
        disp(' ');
        disp('pr�sentation des sondeurs');
        disp('-------------------------');
        disp(' ');
        disp(['nb sondeurs = ' num2str(nb_snd)]);
        for isdr = 1:nb_snd
            nb_transduc=list_sounder(isdr).m_numberOfTransducer;
            if (list_sounder(isdr).m_SounderId<10)
%                ind_list(list_sounder(isdr).m_SounderId)=isdr;
                ind_list(isdr)=list_sounder(isdr).m_SounderId;                
                %disp(['sondeur num�ro ' num2str(isdr) ':    ' '(index: ' num2str(list_sounder(isdr).m_SounderId) ')   nb trans:' num2str(nb_transduc)]);
                disp(['sondeur num�ro ' num2str(isdr) ':    '  '   nb transducteurs:' num2str(nb_transduc)]);
                for itr=1:nb_transduc
                    nb_soft_chnel(isdr,itr)=list_sounder(isdr).m_transducer(itr).m_numberOfSoftChannel;
                    disp(['   transducteur num�ro ' num2str(itr) ':    ' 'nom: ' list_sounder(isdr).m_transducer(itr).m_transName '   nb soft_chnl:' num2str(nb_soft_chnel(isdr,itr))]);
                    for isf=1:nb_soft_chnel(isdr,itr)
                        disp(['       soft_chn ' num2str(isf) ':    ' '   freq: ' num2str(list_sounder(isdr).m_transducer(itr).m_SoftChannel(isf).m_acousticFrequency/1000) ' kHz']);

                    end
                end
            else
                disp('Pb sondeur Id');
            end
        end
                
        if (num_sondeur>nb_snd)
            disp(' ');
            disp('ERREUR: le num�ro du sondeur � filtrer n existe pas. Corriger le num�ro');
            return;
        elseif(num_trans>list_sounder(num_sondeur).m_numberOfTransducer)
            disp(' ');
            disp('ERREUR: l index du transducteur � filtrer n existe pas. Corriger l index');
            return;
        end
        
              
        disp(' ');
        disp(['(choix actuel du sondeur � filtrer: ' list_sounder(num_sondeur).m_transducer(num_trans).m_transName ')']);
        
        disp(' ');
        disp('routine en pause, v�rifier les choix sondeur/transducteur');
        
         %pause; % pause � commenter (surtout en batch)
    end
    
    %longueur du tir
    delta=list_sounder(num_sondeur).m_transducer(num_trans).m_beamsSamplesSpacing;
    dec_tir=4*ceil(list_sounder(num_sondeur).m_transducer(num_trans).m_pulseDuration/list_sounder(num_sondeur).m_transducer(num_trans).m_timeSampleInterval);

    nb_pings=moGetNumberOfPingFan();
    %nb_pings=100;

    for index= nb_pings_prec:nb_pings-1
        MX= moGetPingFan(index);
        SounderDesc =  moGetFanSounder(MX.m_pingId);
        % lecture et cr�ation de la matrice de donn�es sv_data 
        if SounderDesc.m_SounderId == ind_list(num_sondeur)
            datalist = moGetPolarMatrix(MX.m_pingId,num_trans-1);
            sv_data=(datalist.m_Amplitude/100).';
            al=datalist.m_AlongAngle;
            at=datalist.m_AthwartAngle;
            iping=iping+1
            
            if (iping==1)
                dec_tir=max(dec_tir,min(find(sv_data<-30))+1); %longueur du tir (si sous-�valu�e)
            end
            
            %correction de pilonnement en fonction de l'attitude 
            cor1(iping)=MX.navigationAttitude.sensorHeaveMeter-0*tan(MX.navigationAttitude.pitchRad);
            cor2(iping)=cos(MX.navigationAttitude.pitchRad).*cos(MX.navigationAttitude.rollRad);
            
            
            %% detection fond %%%%%%%%%%%%%%%%%%%
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            nbeams=size(sv_data,1);
            nech=size(sv_data,2);
            
           
            %a priori pas de pilonnement � prendre en compte (plus tard par movies)
            % fond r�f�renc� antenne (ou MRU?...(6.15 vs 7.07))
            
            %detection elle-meme
            
            %regarde port�e max enregistr�e
            if max(sv_data(:,end))<-200
                max_rec(iping)=max(find(max(sv_data(:,:),[],1)>-200));
            else
                max_rec(iping)=nech;
            end
            
            %for ib=1:nbeams
            ib=1;

            % niveau maximum de sv sur le ping, et index de sample correspondant
                max_sv=max(sv_data(ib,dec_tir+1:end));
                imax=dec_tir+min(find(sv_data(ib,dec_tir+1:end)==max_sv));
                limax(iping)=imax;
                
                % initialisation du niveau maximum de sv dans le voisinage
                % de la profondeur d�tect�e au ping pr�c�dent
                imax_prox(iping)=imax;
                max_sv_prox=max_sv;

                %examen du maximum
                if max_sv<-35 %pas de fond, niveau maximum trop faible
                    ir_chx(iping)=nan;
                    ir_niv(1:n_niv,iping)=nan;
                    ir_niv2(1:n_niv,iping)=nan;
                    std_ir15(iping)=nan;
                    if iping>1 % on garde les valeurs du ping pr�c�dent
                        imax_prox(iping)=imax_prox(iping-1);
                        if isreal(ir_chx(iping-1))
                            std_ir15(iping)=std_ir15(iping-1);
                        end
                    else
                        imax_prox(iping)=nan;
                    end
                else  % on dispose d'un fond cr�dible
                    if iping>1 % on teste si le maximum de l'�cho de fond d�termin� est plausible (ce pourrait �tre un �cho fort de la colonne d'eau)
                        if abs((imax_prox(iping)+cor1(iping)/delta)*cor2(iping)-...
                            (imax_prox(iping-1)+cor1(iping-1)/delta)*cor2(iping-1))>3*std_ir15(iping-1) % on compare la profondeur du maximum du ping � celle du ping pr�c�dent. 
                        % Si le r�sultat est sup�rieur � 3 fois l'�cart-type observ�,
                        % on cherche un candidat de maximum de fond plus
                        % cr�dible � proximit� de la d�tection pr�c�dente

                            iim=floor(((imax_prox(iping-1)+cor1(iping-1)/delta)*cor2(iping-1)/cor2(iping)-cor1(iping)/delta)); % index d'�chantillon de m�me profondeur que le maximum du fond du ping pr�c�dent
                            nuu=ceil((3*std_ir15(iping-1))/delta);
                            uu=max(dec_tir,iim-nuu);
                            max_sv_prox=max(sv_data(ib,uu:iim+nuu)); % recherche du max du ping dans un voisinage du max du ping pr�c�dent, large de 3x l'�cart-type (et apr�s le tir)
                            imax_t=uu-1+min(find(sv_data(ib,uu:iim+nuu)==max_sv_prox)); % extraction de l'index d'�chantillon correspondant
                            if max_sv_prox>-35 %maximum plus cr�dible (de niveau suffisant)
                                imax_prox(iping)=imax_t;
                            end
                        end
                    end
                    
                    %lignes de niveau
                    for in=1:n_niv
                        vec_inf=sv_data(ib,:)<niv(in); % donn�es du ping inf�rieures au niveau consid�r�
                        vec_inf2=erode(double(vec_inf),ones(1,4)); % on �rode le r�sultat de 4 �chantillons, pour ne garder que les s�quences inf�rieures au niveau consid�r� sur au moins 4 �chantillons successifs
                        ind_niv=find(vec_inf2(1:imax_prox(iping))==1); % on trouve les �chantillons respectant ce crit�re entre le tir et le maximum du fond
                        if isempty(ind_niv) % pas d'�chantillon candidat, pas de passage sous la ligne de niveau
                            ir_niv(in,iping)=dec_tir;
                        else
                            ir_niv(in,iping)=max(ind_niv)+2; % l'�chantillon (+2) le plus proche du maximum du fond correspond � la ligne de niveau recherch�e
                        end
                        
                        % ligne de niveau �quivalente, mais apr�s le maximum de
                        % l'�cho de fond
                        ind_niv=find(vec_inf2(imax_prox(iping)+1:max_rec(iping))==1);
                        if ~isempty(ind_niv)
                            ir_niv2(in,iping)=imax_prox(iping)+min(ind_niv)-3;
                        else
                            ir_niv2(in,iping)=max_rec(iping);
                        end
                    end
                   

                    % on consid�re la ligne de niveau de r�f�rence comme
                    % d�tection initiale du fond. 
                    ir_chx(iping)=ir_niv(iniv_ref,iping);
                    % on regarde l'espacement des lignes de niveaux
                    % suivantes (s'�loignant du fond)
                    dec=ir_niv(2:iniv_ref+1,iping)-ir_niv(1:iniv_ref,iping);
                    %dec(iniv_ref)=2;
                    % on rep�re les espacements sup�rieurs � 2
                    % �chantillons, ou croissant par rapport � la
                    % diff�rence des deux lignes pr�c�dentes (plus loin du
                    % fond)
                    ind_plateau=find(dec(1:iniv_ref-1)>2 | dec(1:iniv_ref-1)>dec(2:iniv_ref));
                    % backstep �ventuel des lignes de niveau pour la
                    % d�tection de fond: 
                    if isempty(ind_plateau) % toutes les lignes sont tr�s proches (<1 ech) et se resserrent continuement en s'�loignant du fond (front montant)
                        ir_chx(iping)=ir_niv(1,iping); % on prend alors la ligne de niveau la plus faible (la moins profonde)
                    else
                        ir_chx(iping)=ir_niv(max(ind_plateau)+1,iping); % on prend la premi�re ligne de niveau pr�c�dente, pr�sentant 
                        % une distance de plus de 2 �chantillons avec celle d'avant , ou un �cart croissant (d�tection de d�but de front montant)
                    end


                    
                end
                
                prof_max(iping)=(imax_prox(iping)*delta+cor1(iping))*cor2(iping);
                
                % calcul de l'�cart-type de la ligne du niveau max de
                % l'�cho de fond sur les derniers pings
                if iping>1
                    u=prof_max(max(2,iping-14):iping);
                    u=u(~isnan(u));
                    std_ir15(iping)=sqrt(var(u));
                else
                    std_ir15(iping)=2*delta;
                end
                
%                ir_ini(iping)= MX.beam_data(num_trans).m_bottomRange/delta-2;
                
                %modification des donn�es de fond du hac
                if ~isnan(ir_chx(iping))
                    %MX.beam_data(num_trans).m_bottomRange=(ir_chx(iping)-1)*delta;
                    MX.beam_data(num_trans).m_bottomRange=(ir_chx(iping)-1-1)*delta; % on d�cale de 2 �chantillons comme Simrad
                    MX.m_maxRangeWasFound=1;
                    MX.beam_data(num_trans).m_bottomWasFound=1;
                else
                    MX.beam_data(num_trans).m_bottomWasFound=0;
                end
                
            %end
            
            %% changement donn�es m�moire
            moSetPingFan(MX.m_pingId,MX);
                end
            
%             sv_mat(iping,1:max_rec(iping))=sv_data(1:max_rec(iping));
%             al_mat(iping,1:max_rec(iping))=al(1:max_rec(iping));
%             at_mat(iping,1:max_rec(iping))=at(1:max_rec(iping));
%             time(iping)=MX.m_meanTimeCPU+MX.m_meanTimeFraction/10000;
%             roll(iping)=MX.navigationAttitude.rollRad;
%             pitch(iping)=MX.navigationAttitude.pitchRad;
%             heave(iping)=MX.navigationAttitude.sensorHeaveMeter;
%             heading(iping)=MX.navigationAttribute.m_headingRad*180/pi;
%             prof(iping)=(ir_chx(iping)*delta+cor1(iping))*cor2(iping);
%             lat(iping)=MX.navigationPosition.m_lattitudeDeg;
%             long(iping)=MX.navigationPosition.m_longitudeDeg;
    end
        nb_pings_prec=nb_pings;

end

if FileStatus.m_StreamClosed<1
    goto=MX.m_meanTimeCPU+MX.m_meanTimeFraction/10000;
else
    goto=-1;
end

if ilec>0
    name_sdr=list_sounder(num_sondeur).m_transducer(num_trans).m_transName;
else
    name_sdr='';
end

%% ecriture hac    
 moStartWrite(chemin_save,['det_' name_sdr '_']);
 moStopWrite;

%% sauvegarde param�tres 


fprintf('End of File');

disp(' ');
disp(' ');
if ilec==0
    disp('pas de donn�es pour ce hac');
else
    disp(['rappel : choix du sondeur : ' list_sounder(num_sondeur).m_transducer(num_trans).m_transName ]);
end
nom_sdr=list_sounder(num_sondeur).m_transducer(num_trans).m_transName;