


function[latent,score,coeff]=acp(A)

[n ,m]=size(A);
Amean=mean(A);
Astd=std(A);
B = (A - repmat(Amean,[n 1])) ./ repmat(Astd,[n 1]);%%Standardisation
[V D] = eig(cov(B));%% V contient les coefficients des composantes principales 
Lat=diag(D);%% par de variance expliqu�e par les composantes principales


latent=[];

for i=length(Lat):-1:1
   
    latent=[latent;Lat(i)];
end

Coef=[];
for j=size(V,2):-1:1
    Coef=[Coef;V(:,j)];
    
end   
coeff=reshape(Coef,size(V,1),size(V,2));
score=B*coeff;



end