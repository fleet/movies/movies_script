SVmax = max(max(SVlong))
SVmin = min(min(SVlong))

t=(SVmax-SVmin)/5


icfq = [];
for f = 1:nf
    i5fq = SVlong(:,f) < ceil(SVmax) & SVlong(:,f) >= SVmax-t & SVlong(:,f)~=0;
    i4fq = SVlong(:,f) < SVmax-t & SVlong(:,f) >= SVmax-2*t& SVlong(:,f)~=0; 
    i3fq = SVlong(:,f) < SVmax-2*t & SVlong(:,f) >= SVmax-3*t& SVlong(:,f)~=0;
    i2fq = SVlong(:,f) < SVmax-3*t & SVlong(:,f) >= SVmax-4*t& SVlong(:,f)~=0;
    i1fq = SVlong(:,f) < SVmax-4*t & SVlong(:,f) >= floor(SVmin)& SVlong(:,f)~=0;
    icfq = [i5fq i4fq i3fq i2fq i1fq icfq];
end