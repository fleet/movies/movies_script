X = mvnrnd([0;0],[1 .9;.9 1],100);
Y = [1 1;1 -1;-1 1;-1 -1];

hist(X)
hist(Y)

d1 = mahal(Y,X) % Mahalanobis
d2 = sum((Y-repmat(mean(X),4,1)).^2, 2) % Squared Euclidean

scatter(X(:,1),X(:,2))
hold on
scatter(Y(:,1),Y(:,2),100,d1,'*','LineWidth',2)
hb = colorbar;
ylabel(hb,'Mahalanobis Distance')
legend('X','Y','Location','NW')