%%
clear all;
% close all;

chemin_ini='F:\ts-65me\';

% chemin_config_reverse = strrep(chemin_ini, '\', '/');
% moLoadConfig(chemin_config_reverse);

filelist = ls([chemin_ini,'*.mat']);  % ensemble des fichiers
nb_files = size(filelist,1);  % nombre de fichiers

%� renseigner
indexsondeur=2;
nbeams=15;
threshold=-65;
surfaceOffset=4;
bottomOffset=1.7;

% 
% 
% TS=cell(nbeams,1);
% TSsimrad=cell(nbeams,1);
% TSsimradUncomp=cell(nbeams,1);
% DepthSimrad=cell(nbeams,1);
% % LatSimrad=cell(nbeams,1);
% % LongSimrad=cell(nbeams,1);
% Date=cell(nbeams,1);
% AngleSimradAthwart=cell(nbeams,1);
% AngleSimradAlong=cell(nbeams,1);
% 
% GainAdjI0=[-0.06  0.03 -0.13 -0.07  0.42  1.07  0.68  0.19 -0.24  0.03 -0.16  0.89  1.44];
% SaCorAdjI0=[-0.1 -0.09 -0.08 -0.08 -0.07 -0.08 -0.06 -0.09 -0.06 -0.09 -0.07 -0.07 -0.09];
% 
% GainAdjI10=[-0.45 -0.37 -0.41 -0.37 -0.11 -0.03 -0.23  -0.3 -0.43  -0.3 -0.53   0.5   0.6];
% SaCorAdjI10=[-0.15 -0.13  -0.1 -0.12  -0.1 -0.14  -0.1 -0.12 -0.11 -0.12 -0.11 -0.11 -0.12];
% 
% GainAdjI20=[-0.53 -0.65 -0.48 -0.49 -0.89 -1.44 -1.25 -0.78 -0.46 -0.47 -0.82   0.3  -0.2];
% SaCorAdjI20=[-0.1 -0.08 -0.06 -0.07 -0.06 -0.09 -0.05 -0.07 -0.06 -0.07 -0.07 -0.06 -0.08];
% 
% GainAdjI30=[-0.92 -1.35 -1.12 -1.46 -2.72 -3.65 -3.71 -1.96 -1.22 -1.23 -1.29 -0.15 -2.05];
% SaCorAdjI30=[-0.12 -0.12 -0.07  -0.1 -0.09 -0.13  -0.1 -0.12 -0.08  -0.1 -0.09 -0.09  -0.1];
% 
% GainAdjI40=[-1.82 -2.76 -1.82 -2.08 -4.21     0     0 -2.72 -1.72 -2.39 -2.38 -0.85 -4.63];
% SaCorAdjI40=[-0.09 -0.08 -0.04 -0.06 -0.07     0     0 -0.07 -0.05 -0.06 -0.06 -0.06  -0.1];
% 
% 
% Gain_adjust=GainAdjI0+SaCorAdjI0;
% 
% 
%     load('steering_shift.mat');
%     dep_trans2(15)=-dep_trans2(1);
%     ga2(15)=ga2(1)-0.7;
%     
% ind_ping=0;
% for numfile = 1:nb_files  % boucle sur l'ensemble des fichiers � traiter
%     
%     hacfilename = filelist(numfile,:);
%     FileName = [chemin_ini,hacfilename]
% %     
% %         [detec3,detecsimrad,detecsimraduncomp,posabsTS,anglesimradAthwart,anglesimradAlong,heure_hac,heure_hac_frac,frequency,steering]=ComputeTS(FileName,indexsondeur,threshold,surfaceOffset,bottomOffset);
% %         save([chemin_ini,'MBI2_',num2str(indexsondeur),'_',hacfilename(1:end-4)],'detec3','detecsimrad','detecsimraduncomp','posabsTS','anglesimradAthwart','anglesimradAlong','heure_hac','heure_hac_frac','frequency','steering');
% %     
%     load ([chemin_ini,'MBI2_',num2str(indexsondeur),'_',hacfilename(1:end-4)]);
%     
%     % mise en liste des d�tections, et positionnement en lat/long
%     npings=size(detec3,1);
%     
%     date = ((heure_hac+heure_hac_frac/10000)/86400 +719529);
% %     datestart=datenum(2010,1,18,23,50,0);
% %     dateend=datenum(2010,1,19,00,45,00);
%     datestart=datenum(2010,1,18,23,49,0);
%     dateend=datenum(2010,1,19,01,10,00);
%     
%     for i=1:npings
%         if (date(i)>datestart && date(i)<dateend)
%             for ib=1:nbeams
%                 %             TS{ib}=[TS{ib} ; detec3{i,ib}+2*Gain_adjust(ib)];
%                 if ~isempty(detecsimrad{i,ib})
%                     if (indexsondeur==2 &&nbeams==15)
%                         for p=1:length(detecsimrad{i,ib})
%                             TSsimrad{ib}=[TSsimrad{ib} ; detecsimrad{i,ib}(p)+delta_TS_steering_shift(ib,anglesimradAthwart{i,ib}(p)*180/pi,anglesimradAlong{i,ib}(p)*180/pi,dep_trans2,dep_long2,freq,a_trans,ga_sim,ga2)];
%                             TSsimradUncomp{ib}=[TSsimradUncomp{ib} ; detecsimraduncomp{i,ib}(p)+delta_TS_steering_shift(ib,anglesimradAthwart{i,ib}(p)*180/pi,anglesimradAlong{i,ib}(p)*180/pi,dep_trans2,dep_long2,freq,a_trans,ga_sim,ga2)];
%                         end
%                     elseif (indexsondeur==2 &&nbeams==13)
%                         TSsimrad{ib}=[TSsimrad{ib} ; detecsimrad{i,ib}'-2*Gain_adjust(ib)];
%                         TSsimradUncomp{ib}=[TSsimradUncomp{ib} ; detecsimraduncomp{i,ib}'-2*Gain_adjust(ib)];
%                     else
%                         TSsimrad{ib}=[TSsimrad{ib} ; detecsimrad{i,ib}'];
%                         TSsimradUncomp{ib}=[TSsimradUncomp{ib} ; detecsimraduncomp{i,ib}'];
%                     end
%                     pos=posabsTS{i,ib};
% %                     LatSimrad{ib}=[LatSimrad{ib} ;pos(2,:)'];
% %                     LongSimrad{ib}=[LongSimrad{ib} ;pos(1,:)'];
%                     DepthSimrad{ib}=[DepthSimrad{ib} ;pos(3,:)'];
%                     Date{ib}=[Date{ib} ;repmat(date(i),length(detecsimrad{i,ib}),1)];
%                     AngleSimradAthwart{ib}=[AngleSimradAthwart{ib} ;anglesimradAthwart{i,ib}'];
%                     AngleSimradAlong{ib}=[AngleSimradAlong{ib} ;anglesimradAlong{i,ib}'];
%                 end
%             end
%         end
%     end
%     
%     
% end
% 
% nbeams=nbeams-2;
% startbeam=1;
% 
% 
% steering=(pi/180)*[ -38.6736 -29.9841 -22.5463 -15.9467 -9.9476 -4.3973 0.7788 5.8878 11.3002 17.1513 23.5864 30.8312 39.2774 ];
% % steering=(pi/180)*[ 0 0 0 0];
% 
% openingAthwart=pi/180*[ 9.51 7.95 6.95 6.26 5.74 5.35 4.99 5.22 5.6 6.1 6.78 7.74 9.23]*1;
% openingAlong=pi/180*[ 7.43 6.89 6.42 6.01 5.66 5.35 4.99 5.19 5.49 5.83 6.21 6.65 7.15 ]*1;
% % openingAthwart=pi/180*[ 7 7 7 7]*1;
% % openingAlong=pi/180*[ 7 7 7 7]*1;
%   tetaminAth=min(steering-openingAthwart/2);
%   tetamaxAth=max(steering+openingAthwart/2);
%   tetaminAl=-max(openingAlong/2);
%   tetamaxAl=max(openingAlong/2);
%   nbanglesAth=60;
%   nbanglesAl=5;
%             
%   angleAthwart=NaN(1,nbanglesAth+1);
%   angleAlong=NaN(1,nbanglesAl+1);
%   
%   for i=1:nbanglesAth+1
%       angleat=asin(sin(tetaminAth)+(i-1)*(sin(tetamaxAth)-sin(tetaminAth))/nbanglesAth);
%       angleAthwart(i)=angleat;
%   end
%   for i=1:nbanglesAl+1
%       angleal=asin(sin(tetaminAl)+(i-1)*(sin(tetamaxAl)-sin(tetaminAl))/nbanglesAl);
%       angleAlong(i)=angleal;
%   end
%   
%   
% 
% depth=[10:12:22];
% for d=1:length(depth)
%     strdepth(d,:)=[num2str(depth(d)) 'm'];
% end
% 
% % 
% for d=1:length(depth)-1
%     for i=1:nbeams
%         indexg=TSsimrad{startbeam+i}-TSsimradUncomp{startbeam+i}>8;
%         indexd=(DepthSimrad{startbeam+i}>depth(d+1) | DepthSimrad{startbeam+i}<depth(d));
%         index=indexg|indexd;
%         TSsimrad{startbeam+i}(index)=nan;
%         AngleSimradAthwart{startbeam+i}(index)=nan;
%         AngleSimradAlong{startbeam+i}(index)=nan;
%         sum(~index)/sum(~indexd);
% 
%          
%     end
% end
%  
% threshold=-65;
% t=0:4:40;
% tsd=[0:2:15];
% y=-90:6:90;
% 
% 
% % pMW=nan(length(t),length(tsd),length(y),nbanglesAth,nbanglesAl,nbeams);
% % hMW=nan(length(t),length(tsd),length(y),nbanglesAth,nbanglesAl,nbeams);
% pKS=nan(length(t),length(tsd),length(y),nbanglesAth,nbanglesAl,nbeams);
% pKSc=nan(length(t),length(tsd),length(y),nbanglesAth,nbanglesAl,nbeams);
% % hKS=nan(length(t),length(tsd),length(y),nbanglesAth,nbanglesAl,nbeams);
% % ksstat=nan(length(t),length(tsd),length(y),nbanglesAth,nbanglesAl,nbeams);
% % D=nan(length(shift),length(t),length(tsd),length(y),nbanglesAth,nbanglesAl,nbeams);
% 
% TSmeanmodeldiff=nan(length(t),length(tsd),length(y),nbanglesAth,nbanglesAl,nbeams);
% % TSstdmodeldiff=nan(length(t),length(tsd),length(y),nbanglesAth,nbanglesAl,nbeams);
% 
% dist_ath=NaN(1,nbanglesAth+1);
% for a=1:nbanglesAth
%     if a==1
%         dist_ath(a)=(angleAthwart(a)-tetaminAth)/2;
%     elseif a==length(angleAthwart)
%         dist_ath(a)=(tetamaxAth-angleAthwart(a))/2;
%     else
%         dist_ath(a)=(angleAthwart(a+1)-angleAthwart(a))/2;
%     end
% end
% 
% dist_al=NaN(1,nbanglesAl+1);
% for a=1:nbanglesAl
%     if a==1
%         dist_al(a)=(angleAlong(a)-tetaminAl)/2;
%     elseif a==length(angleAlong)
%         dist_al(a)=(tetamaxAl-angleAlong(a))/2;
%     else
%         dist_al(a)=(angleAlong(a+1)-angleAlong(a))/2;
%     end
% end
% 
% for it=1:length(t)
%     it
%     for itsd=1:length(tsd)
%         itsd
%         for iy=1:length(y)
%             load([chemin_ini,'\TSME2\TSME2_',num2str(t(it)),'_',num2str(tsd(itsd)),'_',num2str(y(iy)),'_',num2str(tsd(itsd))]);
%             TSfish(TSfish<threshold)=NaN;
%             TSfish(TSfish==0)=NaN;
%             for ath=1:nbanglesAth
%                 for al=1:nbanglesAl
%                     for i = 1:nbeams
%                         temp=reshape(TSfish(:,ath,al,i),[],1);
%                         index=~isnan(temp);
%                         TSmodel=temp(index);
%                         if (length(TSmodel)>30)
%                             indexat=(AngleSimradAthwart{startbeam+i}<=angleAthwart(ath)+dist_ath(ath) & AngleSimradAthwart{startbeam+i}>=angleAthwart(ath)-dist_ath(ath));
%                             indexal=(AngleSimradAlong{startbeam+i}<=angleAlong(al)+dist_al(al) & AngleSimradAlong{startbeam+i}>=angleAlong(al)-dist_al(al));
%                             temp2=TSsimrad{startbeam+i};
%                             indexnan=isnan(temp2);
%                             TSmeasure=(temp2(indexat&indexal&~indexnan));
%                             if (length(TSmeasure)>30 )
%                                  TSmeanModel=   10*log10(mean(10.^((TSmodel)./10),1));
%                                 TSmeanInSitu =   10*log10(mean(10.^((TSmeasure)./10),1));
%                                 TSmeanmodeldiff(it,itsd,iy,ath,al,i)=   TSmeanModel-TSmeanInSitu;
% %                                 TSstdmodeldiff(it,itsd,iy,ath,al,i)=10*log10(std(10.^((TSmodel)./10),1))-10*log10(std(10.^(TSmeasure./10),1));
%                                 alpha=0.05;
% %                                 [~,hMW(it,itsd,iy,ath,al,i),~]=ranksum(TSmeasure,TSmodel,'alpha',alpha);
% %                                 [pMW(it,itsd,iy,ath,al,i),hMW(it,itsd,iy,ath,al,i),~]=ranksum(TSmeasure,TSmodel,'alpha',alpha);
% %                                 [hKS(it,itsd,iy,ath,al,i),pKS(it,itsd,iy,ath,al,i),~]=kstest2(TSmeasure,TSmodel,alpha);
%                                 [~,pKS(it,itsd,iy,ath,al,i),~]=kstest2(TSmeasure,TSmodel,alpha);
%                                     [~,pKSc(it,itsd,iy,ath,al,i),~]=kstest2(TSmeasure-mean(TSmeasure),TSmodel- mean(TSmodel),alpha);
% %                                 D(is,it,itsd,iy,ath,al,i)=pdist2(TSmeasure(1:min(length(TSmeasure),length(TSmodel)))',TSmodel(1:min(length(TSmeasure),length(TSmodel)))'-shift(is));
%                             end
%                         end
%                     end
%                 end
%             end
%         end
%     end
% end
% 
% save([chemin_ini,'errorME2ter'],'pKS','pKSc','TSmeanmodeldiff','t','tsd','y','nbeams','angleAthwart','angleAlong');
% % save([chemin_ini,'errorME2ter'],'hKS','pKS','ksstat','TSmeanmodeldiff','t','tsd','y','nbeams','angleAthwart','angleAlong');
% %  save([chemin_ini,'errorME2ter'],'hMW','hKS','TSmeanmodeldiff','TSstdmodeldiff','t','tsd','y','nbeams','angleAthwart','angleAlong');
% % save([chemin_ini,'errorME2bis'],'TSmeanmodeldiff','TSstdmodeldiff','t','tsd','y','nbeams','angleAthwart','angleAlong');

chemin_ini='X:\MDoray\IBTS10\';

load([chemin_ini,'\ts-65me\error\errorME2bis']);

pKS=pKS(:,:,1:2:end,:,:,:);
TSmeanmodeldiff=TSmeanmodeldiff(:,:,1:2:end,:,:,:);
y=y(1:2:end);
pitch=nan(nbeams,length(angleAthwart)-1,length(angleAlong)-1,length(t)*length(tsd)*15);
yaw=nan(nbeams,length(angleAthwart)-1,length(angleAlong)-1,length(t)*length(tsd)*15);
orstd=nan(nbeams,length(angleAthwart)-1,length(angleAlong)-1,length(t)*length(tsd)*15);
TSdiff=nan(nbeams,length(angleAthwart)-1,length(angleAlong)-1,length(t)*length(tsd)*15);
count=nan(nbeams,length(angleAthwart)-1,length(angleAlong)-1);

figure

i=13
ath=60
al=5

for i=1:nbeams
    
    for ath=1:length(angleAthwart)-1
        
        for al=1:length(angleAlong)-1.
            
            
            A = squeeze(pKS(1:1:end,1:1:end,1:1:end,ath,al,i));
            [Acor, h]=bonf_holm(A,0.05);
%             hist(A)
%             hist(Acor)
            %# finds the max of A and its position, when A is viewed as a 1D array
             position = find(Acor>700);
            
                %#transform the index in the 1D view to 3 indices, given the size of A
                [j,k,l] = ind2sub(size(A),position);
                
                if ~isempty(j)
                i
                ath
                al
                pitch(i,ath,al,1:length(j))=t(j);
                orstd(i,ath,al,1:length(k))=tsd(k);
                yaw(i,ath,al,1:length(l))=y(l);
                 B= squeeze(TSmeanmodeldiff(:,:,:,ath,al,i));
                TSdiff(i,ath,al,1:length(l))=B(position);
                count(i,ath,al)=length(j);
               
%                 subplot(2,2,1)
%                 hist(B(position))
%                 title('TS bias')
%                 subplot(2,2,2)
%                 hist(Acor(Acor>0.05))
%                 title('p-values')
%                 subplot(2,2,3)
%                 hist(t(j))
%                 title('Pitch')
%                 subplot(2,2,4)
%                 hist(tsd(k))
%                 title('Pitch SD')
                
                end
        end
    end
end

   
% mcountbeam=nan(nbeams,1);
% mpitchbeam=nan(nbeams,1);
% myawbeam=nan(nbeams,1);
% msdpitchbeam=nan(nbeams,1);
% 
% for i=1:nbeams
%     pitchvect=reshape(pitch(i,:,:,:),[],1);
%     pitchvect=pitchvect(~isnan(pitchvect));
%     quantile(pitchvect,[0.025 0.25 0.50 0.75 0.975])
%     mpitchbeam(i)=mean(pitchvect)
%     sdpitchvect=reshape(orstd(i,:,:,:),[],1);
%     sdpitchvect=sdpitchvect(~isnan(sdpitchvect));
%     quantile(sdpitchvect,[0.025 0.25 0.50 0.75 0.975])
%     msdpitchbeam(i)=mean(sdpitchvect)
%     countvect=reshape(count(i,:,:),[],1);
%     countvect=countvect(~isnan(countvect));
%     quantile(countvect,[0.025 0.25 0.50 0.75 0.975])
%     mcountbeam(i)=mean(countvect)
% end    

  
    
    % for i=1:nbeams
% figure
% imagesc(squeeze(count(i,:,:)));
% end


% for i=1:nbeams
% figure;
% imagesc(squeeze(orstd(i,:,:)));
% end
% 
% 
% nbfishesME70=zeros(13,length(tsd));
% for i=1:nbeams
%     for itsd=1:length(tsd)
%         for it=1:length(t)
%             for iy=1:length(y)
%                 for ath=1:length(angleAthwart)-1
%                     for al=1:length(angleAlong)-1
% %                         if hKS(it,itsd,iy,ath,al,i)==0 && abs(TSmeanmodeldiff(it,itsd,iy,ath,al,i))<3
% %                                                           if abs(TSmeanmodeldiff(it,itsd,iy,ath,al,i))<1 && abs(TSstdmodeldiff(it,itsd,iy,ath,al,i))<1
% if pKS(it,itsd,iy,ath,al,i)>0.05  && abs(TSmeanmodeldiff(it,itsd,iy,ath,al,i))<3
%                             nbfishesME70(i,itsd)=nbfishesME70(i,itsd)+1;
%                         end
%                     end
%                 end
%                 
%             end
%         end
%     end
% end

% load([chemin_ini,'\ts-65ek\errorEKd']);
% nbfishesEK60=zeros(4,length(tsd));
% for i=1:nbeams
%         for itsd=1:length(tsd)
%             for it=1:length(t)
%                 for iy=1:length(y)
%                     for ath=1:length(angleAthwart)-1
%                         for al=1:length(angleAlong)-1
%                             if hKS(it,itsd,iy,ath,al,i)==0 && abs(TSmeanmodeldiff(it,itsd,iy,ath,al,i))<3
%                                 %                                 if abs(TSmeanmodeldiff(it,itsd,iy,ath,al,i))<1 && abs(TSstdmodeldiff(it,itsd,iy,ath,al,i))<1
%                                 nbfishesEK60(i,itsd)=nbfishesEK60(i,itsd)+1;
%                             end
%                         end
%                     end
%                     
%                 end
%             end
%         end
% end
% 
% frequencyME70=[  78839 85016 91193 97370 103546 109723 117338 112812 106635 100458 94281 88104 81928 ];
% 
% frequencyEK60=[ 38000 70000 120000 200000  ];
% frequency=cat(2,frequencyEK60,frequencyME70);
% nbfishes=cat(1,nbfishesEK60,nbfishesME70);
% [value,index]=sort(frequencyME70);
% 
% figure;
% imagesc(tsd,round(frequencyME70(index)./1e3),squeeze(nbfishesME70(index,:)));     
% 
% set(gca,'YTick',round(frequencyME70(index)./1e3));
% caxis([0 1500]);
% colormap('gray');
%     toto=xlabel('Orientation standard deviation(�)');
%     set(toto,'FontName','Comic sans MS','FontSize',16)
%     toto=ylabel('Frequency (kHz)');
%     set(toto,'FontName','Comic sans MS','FontSize',16)
%     colorbar;
%     saveas(gcf,[chemin_ini,'\Nbfishes_ME70_2'],'fig')
% saveas(gcf,[chemin_ini,'\Nbfishes_ME70_2'],'jpg')

% figure;
% imagesc(tsd,round(frequencyEK60(:)./1e3),squeeze(nbfishesEK60(:,:)));     
% 
% set(gca,'YTick',round(frequencyEK60(:)./1e3));
% caxis([0 1500]);
% colormap('gray');
%     toto=xlabel('Orientation standard deviation(�)');
%     set(toto,'FontName','Comic sans MS','FontSize',16)
%     toto=ylabel('Frequency (kHz)');
%     set(toto,'FontName','Comic sans MS','FontSize',16)
%     colorbar;
%     saveas(gcf,[chemin_ini,'\Nbfishes_EK0_2'],'fig')
% saveas(gcf,[chemin_ini,'\Nbfishes_EK60_2'],'jpg')
    

% figure
% plot(tsd,median(nbfishesME70,1));


% depth=16;
% 
% col='rmgcbk';
% itsd=6;
% load([chemin_ini,'\ts-65me\errorME2bis']);
% 
% yaw=nan(length(angleAthwart)-1,length(angleAlong)-1,length(t)*length(y),nbeams);
% pitch=nan(length(angleAthwart)-1,length(angleAlong)-1,length(t)*length(y),nbeams);
% 
% yawmedian=nan(length(angleAthwart)-1,length(angleAlong)-1,nbeams);
% pitchmedian=nan(length(angleAthwart)-1,length(angleAlong)-1,nbeams);
% 
% 
% for ath=1:length(angleAthwart)-1
%     for al=1:length(angleAlong)-1
%         for i=1:nbeams
%             j=1;
%             for it=1:length(t)
%                 for iy=1:length(y)
% %                                        if hKS(it,itsd,iy,ath,al,i)==0 && abs(TSmeanmodeldiff(it,itsd,iy,ath,al,i))<3
% %                     if abs(TSmeanmodeldiff(it,itsd,iy,ath,al,i))<1 && abs(TSstdmodeldiff(it,itsd,iy,ath,al,i))<1
% if pKS(it,itsd,iy,ath,al,i)>0.7  %&& abs(TSmeanmodeldiff(it,itsd,iy,ath,al,i))<3
%                         yaw(ath,al,j,i)=y(iy);
%                         pitch(ath,al,j,i)=t(it);
%                         j=j+1;
%                     end
%                 end
%             end
%         end
%     end
% end

% title(['Fishes swimming direction: ',num2str(sum(nbfishes(:,is,itsd))),' std: ',num2str(tsd(itsd)),'shift: ',num2str(shift(is))]);

% yawmean=nan(length(angleAthwart)-1,length(angleAlong)-1);
% pitchmean=nan(length(angleAthwart)-1,length(angleAlong)-1);
% for i=1:nbeams
% 
%         figure;
%         imagesc(yawmedian(:,:,i));
%         caxis([0 60]);
%     
%     
%         figure;
%         imagesc(pitchmedian(:,:,i));
%         caxis([0 60]);
% end
yawmean=nan(length(angleAthwart)-1,nbeams);
yawstd=nan(length(angleAthwart)-1,nbeams,2);
pitchmean=nan(length(angleAthwart)-1,nbeams);
pitchstd=nan(length(angleAthwart)-1,nbeams,2);
orstdmean=nan(length(angleAthwart)-1,nbeams);
orstdstd=nan(length(angleAthwart)-1,nbeams,2);
TSdiffmean=nan(length(angleAthwart)-1,nbeams);
TSdiffstd=nan(length(angleAthwart)-1,nbeams,2);

for ath=1:length(angleAthwart)-1
    for i=1:nbeams
        yawvect=reshape(yaw(i,ath,:,:),[],1);
        yawvect=yawvect(~isnan(yawvect));
        pitchvect=reshape(pitch(i,ath,:,:),[],1);
        pitchvect=pitchvect(~isnan(pitchvect));
        orstdvect=reshape(orstd(i,ath,:,:),[],1);
        orstdvect=orstdvect(~isnan(orstdvect));
        TSdiffvect=reshape(TSdiff(i,ath,:,:),[],1);
        TSdiffvect=TSdiffvect(~isnan(TSdiffvect));
        if length(yawvect)>0
            yawmean(ath,i)=median( yawvect(~isnan(yawvect)));
            yawstd(ath,i,:)=prctile(yawvect(~isnan(yawvect)), [25 75]);
            pitchmean(ath,i)=median( pitchvect(~isnan(pitchvect)));
            pitchstd(ath,i,:)=prctile(pitchvect(~isnan(pitchvect)), [25 75]);
            orstdmean(ath,i)=median( orstdvect(~isnan(orstdvect)));
             orstdstd(ath,i,:)=prctile(orstdvect(~isnan(orstdvect)), [25 75]);
            TSdiffmean(ath,i)=median( TSdiffvect(~isnan(TSdiffvect)));
            TSdiffstd(ath,i,:)=prctile(TSdiffvect(~isnan(TSdiffvect)), [25 75]);
        end
    end
end

yawmeanall=[];
pitchmeanall=[];
orstdmeanall=[];
TSdiffmeanall=[];
figure
hold on;
  for i=1:nbeams

 errorbar(angleAthwart(1:end-1)*180/pi,yawmean(:,i),yawmean(:,i)-yawstd(:,i,1),yawstd(:,i,2)-yawmean(:,i),'k');
  plot(angleAthwart(1:end-1)*180/pi,yawmean(:,i),'k', 'LineWidth',3);
%   index=~isnan(yawmean(:,i));
%   yawmeanall=[yawmeanall yawmean(index,i)'];
  end
  
   toto=xlabel('Atwartship steering angle (�)');
    set(toto,'FontName','Comic sans MS','FontSize',16)
    toto=ylabel('Fish Yaw (�)');
    set(toto,'FontName','Comic sans MS','FontSize',16)

saveas(gcf,[chemin_ini,'\Yaw_max_5pc'],'fig')
saveas(gcf,[chemin_ini,'\Yaw_max_5pc'],'jpg')
  
 figure
 hold on;
  for i=1:nbeams
 errorbar(angleAthwart(1:end-1)*180/pi,pitchmean(:,i),pitchmean(:,i)-pitchstd(:,i,1),pitchstd(:,i,2)-pitchmean(:,i),'k');
   plot(angleAthwart(1:end-1)*180/pi,pitchmean(:,i),'k', 'LineWidth',3);
%      index=~isnan(pitchmean(:,i));
%   pitchmeanall=[pitchmeanall pitchmean(index,i)'];
  end
  
     toto=xlabel('Atwartship steering angle (�)');
    set(toto,'FontName','Comic sans MS','FontSize',16)
    toto=ylabel('Fish Pitch (�)');
    set(toto,'FontName','Comic sans MS','FontSize',16)
    
saveas(gcf,[chemin_ini,'\Pitch_max_5pc'],'fig')
saveas(gcf,[chemin_ini,'\Pitch_max_5pc'],'jpg')

 figure
 hold on;
  for i=1:nbeams
 errorbar(angleAthwart(1:end-1)*180/pi,orstdmean(:,i),orstdmean(:,i)-orstdstd(:,i,1),orstdstd(:,i,2)-orstdmean(:,i),'k');
   plot(angleAthwart(1:end-1)*180/pi,orstdmean(:,i),'k', 'LineWidth',3);
%      index=~isnan(orstdmean(:,i));
%   orstdmeanall=[orstdmeanall orstdmean(index,i)'];
  end
  
     toto=xlabel('Atwartship steering angle (�)');
    set(toto,'FontName','Comic sans MS','FontSize',16)
    toto=ylabel('Fish Orientation Std (�)');
    set(toto,'FontName','Comic sans MS','FontSize',16)
    
saveas(gcf,[chemin_ini,'\Orstd_max_5pc'],'fig')
saveas(gcf,[chemin_ini,'\Orstd_max_5pc'],'jpg')

 figure
 hold on;
  for i=1:nbeams
 errorbar(angleAthwart(1:end-1)*180/pi,TSdiffmean(:,i),TSdiffmean(:,i)-TSdiffstd(:,i,1),TSdiffstd(:,i,2)-TSdiffmean(:,i),'k');
   plot(angleAthwart(1:end-1)*180/pi,TSdiffmean(:,i),'k', 'LineWidth',3);
%      index=~isnan(orstdmean(:,i));
%   TSdiffmeanall=[TSdiffmeanall TSdiffmean(index,i)'];
  end
  
     toto=xlabel('Atwartship steering angle (�)');
    set(toto,'FontName','Comic sans MS','FontSize',16)
    toto=ylabel('TS difference (dB)');
    set(toto,'FontName','Comic sans MS','FontSize',16)

saveas(gcf,[chemin_ini,'\TSdiff_max_5pc'],'fig')
saveas(gcf,[chemin_ini,'\TSdiff_max_5pc'],'jpg')


%   figure;
%   hold on;
%   for ath=1:length(angleAthwart)-1
%       for i=1:nbeams
%           if ~isnan(yawmeanall(ath,i)) && ~isnan(pitchmeanall(ath,i))
%               [u,v,w]=sph2cart((pi/180)*yawmeanall(ath,i),-(pi/180)*pitchmeanall(ath,i),40);
%               quiver3(0,depth*sin(angleAthwart(ath)),depth*sin(angleAlong(al)),u,v,w,0.5,col(1+mod(round(pitchmeanall(ath,i)/4.5),6)));
%           end
%       end
%   end
%   
%   
%   yawmeanall=nan(length(angleAlong)-1,nbeams);
% pitchmeanall=nan(length(angleAlong)-1,nbeams);
% yawstdall=nan(length(angleAlong)-1,nbeams,2);
% pitchstdall=nan(length(angleAlong)-1,nbeams,2);
% for al=1:length(angleAlong)-1
%     for i=1:nbeams
%         yawmedianvect=reshape(yaw(:,al,:,i),[],1);
%         yawmedianvect=yawmedianvect(~isnan(yawmedianvect));
%         pitchmedianvect=reshape(pitch(:,al,:,i),[],1);
%         pitchmedianvect=pitchmedianvect(~isnan(pitchmedianvect));
%         if length(yawmedianvect)>0
%             yawmeanall(al,i)=mean( yawmedianvect(~isnan(yawmedianvect)));
%             pitchmeanall(al,i)=mean( pitchmedianvect(~isnan(pitchmedianvect)));
%             yawstdall(al,i,:)=prctile(yawmedianvect(~isnan(yawmedianvect)), [25 75]);
%             pitchstdall(al,i,:)=prctile(pitchmedianvect(~isnan(pitchmedianvect)), [25 75]);
%         end
%     end
% end
% 
% figure
% hold on;
%   for i=1:nbeams
% 
%  errorbar(angleAlong(1:end-1)*180/pi,yawmeanall(:,i),yawmeanall(:,i)-yawstdall(:,i,1),yawstdall(:,i,2)-yawmeanall(:,i),[col(1+mod(i,4))]);
%   end
%   
%  figure
%  hold on;
%   for i=1:nbeams
%  errorbar(angleAlong(1:end-1)*180/pi,pitchmeanall(:,i),pitchmeanall(:,i)-pitchstdall(:,i,1),pitchstdall(:,i,2)-pitchmeanall(:,i),[col(1+mod(i,4))]);
%   end
% 
