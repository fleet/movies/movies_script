function TSorient_simu_plots(pthr,angleAthwart,nbeams,chemin_ini,yaw,pitch,orstd,TSdiff,TSstddiff,nb_simus_beams)
%% Beam plots

pv=length(pthr);

% chemin_ini='~/.gvfs/q sur nantes/Projects/TS/Herring/Results/pKSsensitivity'
% chemin_ini='/home/mathieubuntu/Documents/Projects/TS/TSherring/pKSsensitivity'
% chemin_ini='/home/mathieubuntu/Documents/Projects/TS/TSherring/pKSsensitivity/ME2pKScor/'

% tilt, tiltSD, yaw, TS diff distributions in each beams
% define max number of parameters
for i=1:nbeams
    yawvect=reshape(yaw(i,:,:,:),[],1);
    yawvect=yawvect(~isnan(yawvect));
    if i==1
        lmax=length(yawvect);
    else
        if length(yawvect)>lmax
            lmax=length(yawvect);
        end
    end    
end

yawvects=nan(lmax,nbeams);
pitchvects=nan(lmax,nbeams);
orstdvects=nan(lmax,nbeams);
TSdiffvects=nan(lmax,nbeams);
TSstddiffvects=nan(lmax,nbeams);

figure(1)
figure(2)
figure(3)
figure(4)
figure(5)

for i=1:nbeams
    yawvect=reshape(yaw(i,:,:,:),[],1);
    yawvect=yawvect(~isnan(yawvect));
    pitchvect=reshape(pitch(i,:,:,:),[],1);
    pitchvect=pitchvect(~isnan(pitchvect));
    orstdvect=reshape(orstd(i,:,:,:),[],1);
    orstdvect=orstdvect(~isnan(orstdvect));
    TSdiffvect=reshape(TSdiff(i,:,:,:),[],1);
    TSdiffvect=TSdiffvect(~isnan(TSdiffvect));
    TSstddiffvect=reshape(TSstddiff(i,:,:,:),[],1);
    TSstddiffvect=TSstddiffvect(~isnan(TSstddiffvect));
    
    yawvects(1:length(yawvect),i)=yawvect;
    pitchvects(1:length(pitchvect),i)=pitchvect;
    orstdvects(1:length(orstdvect),i)=orstdvect;
    TSdiffvects(1:length(TSdiffvect),i)=TSdiffvect;
    TSstddiffvects(1:length(TSstddiffvect),i)=TSstddiffvect;
    nb_simus_beams(i,pv)=length(yawvect);
    
    figure(1)
    subplot(4,4,i)
    hist(yawvect)
    title(['pKS threshold=',num2str(pthr),',','Beam',num2str(i)])
    xlabel('Yaw (degrees)')
    figure(2)
    subplot(4,4,i)
    hist(pitchvect)
    title(['pKS threshold=',num2str(pthr),',','Beam',num2str(i)])
    xlabel('Pitch (degrees)')
    figure(3)
    subplot(4,4,i)
    hist(orstdvect)
    title(['pKS threshold=',num2str(pthr),',','Beam',num2str(i)])
    xlabel('PitchSD (degrees)')
    figure(4)
    subplot(4,4,i)    
    hist(TSdiffvect)
    title(['pKS threshold=',num2str(pthr),',','Beam',num2str(i)])
    xlabel('TS difference (dB)')
    figure(5)
    subplot(4,4,i)    
    hist(TSstddiffvect)
    title(['pKS threshold=',num2str(pthr),',','Beam',num2str(i)])
    xlabel('TS std difference (dB)')
end

%saveas(1,[chemin_ini,'/HistYawBeam_thr',num2str(pthr),'.fig'],'fig')
saveas(1,[chemin_ini,'/HistYawBeam_thr',num2str(pthr),'.jpg'],'jpg')
%saveas(2,[chemin_ini,'/HistPitchBeam_thr',num2str(pthr),'.fig'],'fig')
saveas(2,[chemin_ini,'/HistPitchBeam_thr',num2str(pthr),'.jpg'],'jpg')
%saveas(3,[chemin_ini,'/HistPitchSDBeam_thr',num2str(pthr),'.fig'],'fig')
saveas(3,[chemin_ini,'/HistPitchSDBeam_thr',num2str(pthr),'.jpg'],'jpg')
%saveas(4,[chemin_ini,'/HistTSdifBeam_thr',num2str(pthr),'.fig'],'fig')
saveas(4,[chemin_ini,'/HistTSdifBeam_thr',num2str(pthr),'.jpg'],'jpg')
saveas(5,[chemin_ini,'/HistTSstddifBeam_thr',num2str(pthr),'.jpg'],'jpg')

figure(6)
boxplot(yawvects,'plotstyle','compact')
title(['pKS threshold=',num2str(pthr)])
xlabel('Beam')
ylabel('Yaw')
figure(7)
boxplot(pitchvects,'plotstyle','compact')
title(['pKS threshold=',num2str(pthr)])
xlabel('Beam')
ylabel('Pitch')
figure(8)
boxplot(orstdvects,'plotstyle','compact')
title(['pKS threshold=',num2str(pthr)])
xlabel('Beam')
ylabel('PitchSD')
figure(9)
boxplot(TSstddiffvects,'plotstyle','compact')
title(['pKS threshold=',num2str(pthr)])
xlabel('Beam')
ylabel('TS std difference')

saveas(5,[chemin_ini,'/BoxplotYawBeam_thr',num2str(pthr),'.jpg'],'jpg')
saveas(6,[chemin_ini,'/BoxplotPitchBeam_thr',num2str(pthr),'.jpg'],'jpg')
saveas(7,[chemin_ini,'/BoxplotPitchSDBeam_thr',num2str(pthr),'.jpg'],'jpg')
saveas(8,[chemin_ini,'/BoxplotTSdiffBeam_thr',num2str(pthr),'.jpg'],'jpg')
saveas(9,[chemin_ini,'/BoxplotTSstddiffBeam_thr',num2str(pthr),'.jpg'],'jpg')

%% athwartship plots

% tilt, tiltSD, yaw, TS diff distributions in each athwart position

% define max number of parameters
for ath=1:length(angleAthwart)-1
    for i=1:nbeams
        yawvect=reshape(yaw(i,ath,:,:),[],1);
        yawvect=yawvect(~isnan(yawvect));
        if ath==1&i==1
            lmax=length(yawvect);
        else
            if length(yawvect)>lmax
                lmax=length(yawvect);
            end
        end
    end
end

yawvects=nan(lmax,length(angleAthwart)-1);
pitchvects=nan(lmax,length(angleAthwart)-1);
orstdvects=nan(lmax,length(angleAthwart)-1);
TSdiffvects=nan(lmax,length(angleAthwart)-1);
TSstddiffvects=nan(lmax,length(angleAthwart)-1);

for ath=1:length(angleAthwart)-1
    for i=1:nbeams
        yawvect=reshape(yaw(i,ath,:,:),[],1);
        yawvect=yawvect(~isnan(yawvect));
        pitchvect=reshape(pitch(i,ath,:,:),[],1);
        pitchvect=pitchvect(~isnan(pitchvect));
        orstdvect=reshape(orstd(i,ath,:,:),[],1);
        orstdvect=orstdvect(~isnan(orstdvect));
        TSdiffvect=reshape(TSdiff(i,ath,:,:),[],1);
        TSdiffvect=TSdiffvect(~isnan(TSdiffvect));
        TSstddiffvect=reshape(TSstddiff(i,ath,:,:),[],1);
        TSstddiffvect=TSstddiffvect(~isnan(TSstddiffvect));
        if length(yawvect)>0
                yawvects(1:length(yawvect),ath)=yawvect;
                pitchvects(1:length(pitchvect),ath)=pitchvect;
                orstdvects(1:length(orstdvect),ath)=orstdvect;
                TSdiffvects(1:length(TSdiffvect),ath)=TSdiffvect;
                TSstddiffvects(1:length(TSstddiffvect),ath)=TSstddiffvect;
        end
    end
    nb_simus_ath(i,pv)=length(yawvect);
end    

figure(10)
boxplot(yawvects,'plotstyle','compact')
title(['pKS threshold=',num2str(pthr)])
xlabel('Athwartship angle (degree)')
ylabel('Yaw')
figure(11)
boxplot(pitchvects,'plotstyle','compact')
title(['pKS threshold=',num2str(pthr)])
xlabel('Athwartship angle (degree)')
ylabel('Pitch')
figure(12)
boxplot(orstdvects,'plotstyle','compact')
title(['pKS threshold=',num2str(pthr)])
xlabel('Athwartship angle (degree)')
ylabel('PitchSD')
figure(13)
boxplot(TSdiffvects,'plotstyle','compact')
title(['pKS threshold=',num2str(pthr)])
xlabel('Athwartship angle (degree)')
ylabel('TS difference')
figure(14)
boxplot(TSstddiffvects,'plotstyle','compact')
title(['pKS threshold=',num2str(pthr)])
xlabel('Athwartship angle (degree)')
ylabel('TS std difference')

saveas(10,[chemin_ini,'/BoxplotYawAth_thr',num2str(pthr),'.jpg'],'jpg')
saveas(11,[chemin_ini,'/BoxplotPitchAth_thr',num2str(pthr),'.jpg'],'jpg')
saveas(12,[chemin_ini,'/BoxplotPitchSDAth_thr',num2str(pthr),'.jpg'],'jpg')
saveas(13,[chemin_ini,'/BoxplotTSdiffAth_thr',num2str(pthr),'.jpg'],'jpg')
saveas(14,[chemin_ini,'/BoxplotTSstddiffAth_thr',num2str(pthr),'.jpg'],'jpg')

end