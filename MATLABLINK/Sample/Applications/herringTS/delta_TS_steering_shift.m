function delta_TS=delta_TS_steering_shift(ibeam,teta_trans,teta_long,dep_trans2,dep_long2,freq,a_trans,ga_sim,ga2)

% ibeam: index de la voie contenant la d�tection
% teta_trans: angle transversal absolu de la cible en degr�s
% teta_long: angle longitudinal absolu de la cible en degr�s
% dep_trans2: liste des d�calages de pointage transversaux des voies en
% degr�s
% dep_long2: liste des d�calages de pointage longitudinaux des voies en
% degr�s
% freq: liste des fr�quence des voies en Hz
% a_trans: liste des pointages transversaux des voies en �
% ga_sim : liste des gain adjust simrad de chaque voie
% ga2: liste des gain adjust �valu�s apr�s d�pointage

% delta_TS: correction de TS � apporter au TS simrad en dB 

%ind_nom='101130';


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% dimensions               %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Trans_capteur_elem = 6.7/1000;
Long_capteur_elem = 14.2 /1000;

delta_Long = 15*0.001;
delta_Trans = 7.5*0.001;

         %Trans_capteur_elem = delta_Trans ;
         %Long_capteur_elem = delta_Long ;
         
c=1490;
lambda=c/freq(ibeam);
k=2*pi/lambda;


%% directivit� initiale

% directivit� elem simrad th
h_capt_trans=sin(k*Trans_capteur_elem./2*sind(teta_trans))./(k*Trans_capteur_elem./2*sind(teta_trans));
h_capt_long=sin(k*Long_capteur_elem./2*sind(teta_long))./(k*Long_capteur_elem./2*sind(teta_long));
h_capt_trans(find(isnan(h_capt_trans)))=1;
h_capt_long(find(isnan(h_capt_long)))=1;

dir_nat1=h_capt_trans*h_capt_long*sqrt(1-sind(teta_trans)^2-sind(teta_long)^2);

%% directivit� d�cal�e
teta_trans2=teta_trans-dep_trans2(ibeam);
teta_long2=teta_long-dep_long2(ibeam);
% directivit� elem simrad th
h_capt_trans=sin(k*Trans_capteur_elem./2*sind(teta_trans2))./(k*Trans_capteur_elem./2*sind(teta_trans2));
h_capt_long=sin(k*Long_capteur_elem./2*sind(teta_long2))./(k*Long_capteur_elem./2*sind(teta_long2));
h_capt_trans(find(isnan(h_capt_trans)))=1;
h_capt_long(find(isnan(h_capt_long)))=1;

dir_nat2=h_capt_trans*h_capt_long*sqrt(1-sind(teta_trans2)^2-sind(teta_long2)^2);

delta_TS_dir=-40*log10(dir_nat2)+40*log10(dir_nat1);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% ponderation des voies     %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

nb_capt_trans=40;
pond_trans=ones(1,nb_capt_trans);
nb_capt_long=20;
pond_long=ones(1,nb_capt_long);

if (length(freq)==21)
    %init (21?)
    pond_trans=[0.111994,0.092027,0.126928,0.167993,0.21512,0.267984,0.326026,0.388457,0.454268,0.522255,0.591046,0.659147,0.724988,0.786976,0.843552,0.893248,0.934743,0.966911,0.988867,1,1,0.988867,0.966911,0.934743,0.893248,0.843552,0.786976,0.724988,0.659147,0.591046,0.522255,0.454268,0.388457,0.326026,0.267984,0.21512,0.167993,0.126928,0.092027,0.111994];
    pond_long=[0.112676,0.161491,0.258945,0.376379,0.507236,0.642138,0.769867,0.878735,0.95813,1,1,0.95813,0.878735,0.769867,0.642138,0.507236,0.376379,0.258945,0.161491,0.112676];
elseif (length(freq)==15)
    %IBTS09 15
    pond_trans=[0.000361,0.001257,0.002729,0.005336,0.009906,0.017648,0.030237,0.049844,0.079065,0.120686,0.177267,0.250554,0.340784,0.446025,0.561749,0.680814,0.793994,0.891063,0.962283,1,1,0.962283,0.891063,0.793994,0.680814,0.561749,0.446025,0.340784,0.250554,0.177267,0.120686,0.079065,0.049844,0.030237,0.017648,0.009906,0.005336,0.002729,0.001257,0.000361];
    pond_long=[0.000775,0.003902,0.013481,0.039576,0.099579,0.214839,0.397437,0.630426,0.857455,1,1,0.857455,0.630426,0.397437,0.214839,0.099579,0.039576,0.013481,0.003902,0.000775];
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% formation de voie initiale      
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
psi=a_trans(ibeam);
%ym=(-(nb_capt_trans-1)/2:(nb_capt_trans-1)/2)*Trans_capteur_elem;
ym=(-(nb_capt_trans-1)/2:(nb_capt_trans-1)/2)*delta_Trans;
sum_nb_c_trans=zeros(1,length(teta_trans));  % somme des capteurs dephases/ponderes 
for i_c=1:nb_capt_trans
    sum_nb_c_trans=sum_nb_c_trans+exp(sqrt(-1)*k*sin(teta_trans*pi/180)*ym(i_c))...
        *exp(-sqrt(-1)*k*sin(psi*pi/180)*ym(i_c))*pond_trans(i_c);
end
sum_nb_c_trans1=abs(sum_nb_c_trans);

phi=0;
ym=(-(nb_capt_long-1)/2:(nb_capt_long-1)/2)*delta_Long;
sum_nb_c_long=zeros(1,length(teta_long));
for i_c=1:nb_capt_long
    sum_nb_c_long=sum_nb_c_long+exp(sqrt(-1)*k*sin(teta_long*pi/180)*ym(i_c))...
        *exp(-sqrt(-1)*k*sin(phi*pi/180)*ym(i_c))*pond_long(i_c);
end
sum_nb_c_long1=abs(sum_nb_c_long);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% formation de voie d�cal�e     
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
psi=a_trans(ibeam);
%ym=(-(nb_capt_trans-1)/2:(nb_capt_trans-1)/2)*Trans_capteur_elem;
ym=(-(nb_capt_trans-1)/2:(nb_capt_trans-1)/2)*delta_Trans;
sum_nb_c_trans=zeros(1,length(teta_trans2));  % somme des capteurs dephases/ponderes 
for i_c=1:nb_capt_trans
    sum_nb_c_trans=sum_nb_c_trans+exp(sqrt(-1)*k*sin(teta_trans2*pi/180)*ym(i_c))...
        *exp(-sqrt(-1)*k*sin(psi*pi/180)*ym(i_c))*pond_trans(i_c);
end
sum_nb_c_trans2=abs(sum_nb_c_trans);

phi=0;
ym=(-(nb_capt_long-1)/2:(nb_capt_long-1)/2)*delta_Long;
sum_nb_c_long=zeros(1,length(teta_long2));
for i_c=1:nb_capt_long
    sum_nb_c_long=sum_nb_c_long+exp(sqrt(-1)*k*sin(teta_long2*pi/180)*ym(i_c))...
        *exp(-sqrt(-1)*k*sin(phi*pi/180)*ym(i_c))*pond_long(i_c);
end
sum_nb_c_long2=abs(sum_nb_c_long);


delta_TS_voie=-40*log10(sum_nb_c_trans2)-40*log10(sum_nb_c_long2)+40*log10(sum_nb_c_trans1)+40*log10(sum_nb_c_long1);

delta_TS=delta_TS_dir+delta_TS_voie-2*(ga2(ibeam)-ga_sim(ibeam));
     
