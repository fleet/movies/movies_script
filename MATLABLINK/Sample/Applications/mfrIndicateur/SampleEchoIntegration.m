% This program computes echo-integration for raw HAC files using the MOVIES3D libraries

%Input parameters
%   chemin_ini       : path for HAC files 
%   chemin_save      : path for .mat result files for echo-integration
%   EchoIntegrModule : MOVIES3D object
%   nameTransect     : name of the transect for prefixing result files
%   dateStart        : start date for transect format DD/MM/YYY
%   timeStart        : start time for transect format HH:MM:SS
%   dateEnd          : end date for transect format DD/MM/YYY
%   timeEnd          : end time for transect format HH:MM:SS

%LB 02/04/2013 ajout r�cup�ration sondeur horizontal
%LB 07/05/2013 ajout goto
%LB 11/12/2013 ajout arret EI si l'heure de fin de radiale est d�pass�e
%LB 30/01/2014 quand il n'y a que trois arguments la date de fin d'EI est
%maintenant, on int�gre tout

function SampleEchoIntegration(chemin_ini,chemin_save,EchoIntegrModule,nameTransect,dateStart,timeStart,dateEnd,timeEnd)




    if nargin>3

        %	 date='22/02/2013'	
        %    time='15:19:00'
        goto=str2num(timeStart(:,1:2))*3600+str2num(timeStart(4:5))*60+str2num(timeStart(7:8))+(datenum(dateStart,'dd/mm/yyyy')-datenum('01-Jan-1970'))*24*3600;
        %         time4human(goto)
        
        ParameterR= moReaderParameter();
        ParameterDef=moLoadReaderParameter(ParameterR);
        taille_chunk=ParameterDef.m_chunkNumberOfPingFan;
        ParameterDef.m_chunkNumberOfPingFan=1;
        moSaveReaderParameter(ParameterR,ParameterDef);
        

         % et lecture du premier ping du r�pertoire
        moOpenHac(chemin_ini);

        %goto
        moGoTo(goto);
        moReadChunk(); %lit un ping suppl�mentaire
        
        %vide la m�moire des pings pr�c�dents
        nb_pings=moGetNumberOfPingFan();
        for ip=nb_pings-1:-1:1
            MX= moGetPingFan(ip-1);
            moRemovePing(MX.m_pingId);
        end
        
        ParameterDef.m_chunkNumberOfPingFan=taille_chunk;
        moSaveReaderParameter(ParameterR,ParameterDef);
        
        %vide la m�moire des ESU pr�c�dents
        AllData=moGetOutPutList(EchoIntegrModule);
        clear AllData;
        timeEndEI=datenum([dateEnd,' ',timeEnd],'dd/mm/yyyy HH:MM:SS');
        str_Name=['_',nameTransect,'_'];
    else
          % chargement de tous les fichiers HAC contenus dans le repertoire
         % et lecture du premier chunk
        moOpenHac(chemin_ini);
        timeEndEI=now;
        str_Name='';
        
    end
    
    num_bloc=1;
    

    FileStatus= moGetFileStatus;
    while FileStatus.m_StreamClosed < 1

        % init matrices
        time_ER60 = [];
        time_ME70 = [];
        Sa_surfME70 =[];
        Sa_botME70=[];
        Sa_surfER60=[];
        Sa_botER60=[];
        Sv_surfME70=[];
        Sv_botME70=[];
        Sv_surfER60=[];
        Sv_botER60=[];
        Lat_surfME70=[];
        Long_surfME70=[];
        Depth_surfME70=[];
        Lat_surfER60=[];
        Long_surfER60=[];
        Depth_surfER60=[];
        Lat_botME70=[];
        Long_botME70=[];
        Depth_botME70=[];
        Lat_botER60=[];
        Long_botER60=[];
        Depth_botER60=[];
        Volume_surfME70=[];
        Volume_surfER60=[];
        Volume_botME70=[];
        Volume_botER60=[];
        
        time_ER60h = [];
        Sa_surfER60h=[];
        Sv_surfER60h=[];
        Lat_surfER60h=[];
        Long_surfER60h=[];
        Depth_surfER60h=[];
        Volume_surfER60h=[];
        AllData=[];


        moReadChunk();


        AllData=moGetOutPutList(EchoIntegrModule);

        % r�cup�ration de la d�finition des ESU
        ESUParam = moGetESUDefinition();

        % r�cup�ration de la d�finition des couches
        LayersDef = moGetLayersDefinition(EchoIntegrModule);
        
        % r�cup�ration des volumes de chaque couche (pour 1 ping)
        LayersVolumes = moGetLayersVolumes(EchoIntegrModule);
        sounderNb = size(LayersVolumes(1).Sounders,2);
        
        % on souhaite r�cuperer les informations ME70 et EK60 MFR dans le
        % cas o� l'on a �galement un sondeur horizontal
        indexSounderER60=1;
        indexSounderER60h=1;
        indexSounderME70=1;
        nbchannelsME70=0;
        nbchannelsER60=1;
        nbchannelsER60h=0;
        for indexSounder=1:sounderNb
            if size(LayersVolumes(1).Sounders(indexSounder).Channels,2) >6 %ME70
                indexSounderME70=indexSounder;
                nbchannelsME70=size(LayersVolumes(1).Sounders(indexSounder).Channels,2);
            elseif size(LayersVolumes(1).Sounders(indexSounder).Channels,2) >1 %ER60 MFR
                indexSounderER60=indexSounder;
                nbchannelsER60=size(LayersVolumes(1).Sounders(indexSounder).Channels,2);
                
            elseif size(LayersVolumes(1).Sounders(indexSounder).Channels,2) == 1 && nbchannelsER60>1 %cas du sondeur horizontal de Thalassa avec un sondeur MFR avant
                indexSounderER60h=indexSounder;
                nbchannelsER60h=size(LayersVolumes(1).Sounders(indexSounder).Channels,2);
            end
        end
        
        %on r�cup�re l'ordre des r�sultats des fr�quences ER60 pour les
        %sauvegarder touojours dans le m�me ordre
        % on suppose qu'il y a un seul sondeur multi-transducteur !
        list_sounder=moGetSounderList;
        nb_snd=length(list_sounder);
        indexER60=1;
        list_freqER60=zeros(nbchannelsER60,1);
        for isdr = 1:nb_snd
            nb_transduc=list_sounder(isdr).m_numberOfTransducer;
            if (nb_transduc>1)
                for itr=1:nb_transduc
                    list_freqER60(itr)=list_sounder(isdr).m_transducer(itr).m_SoftChannel.m_acousticFrequency;
                end
                %[~,indexER60]=sort(list_freqER60);
                [B,IX]=sort(list_freqER60);
                indexER60=IX;
            end
        end    



        indexESUME70 = 1;
        indexESUER60 = 1;
        indexESUER60h = 1;        
        %nb d'ESUs traitees
        nbResults = size(AllData,2);
        for indexResults=1:nbResults
            nbbeams=size(AllData(1,indexResults).m_Sa,2);
            timeESU= AllData(1,indexResults).m_time/86400 +719529;
           
            if timeESU>timeEndEI
                return;
            end
            
            if nbbeams==nbchannelsME70

                time_ME70(indexESUME70) = AllData(1,indexResults).m_time;
                nbLayer= size(AllData(1,indexResults).m_Sa,1);
                indexLayerSurface = 1;
                indexLayerBottom = 1;

                for indexLayer=1:nbLayer
                    if LayersDef.LayerType(indexLayer)==0
                        Sa_surfME70(indexESUME70,indexLayerSurface,:)=max(0,AllData(1,indexResults).m_Sa(indexLayer,:));
                        Sv_surfME70(indexESUME70,indexLayerSurface,:)=max(-100,AllData(1,indexResults).m_Sv(indexLayer,:));
                        Lat_surfME70(indexESUME70,indexLayerSurface,:)=AllData(1,indexResults).m_latitude(indexLayer,:);
                        Long_surfME70(indexESUME70,indexLayerSurface,:)=AllData(1,indexResults).m_longitude(indexLayer,:);
                        Depth_surfME70(indexESUME70,indexLayerSurface,:)=AllData(1,indexResults).m_Depth(indexLayer,:);
                        Volume_surfME70(indexESUME70,indexLayerSurface,:)=LayersVolumes(indexLayer).Sounders(indexSounderME70).Channels(:);
                        indexLayerSurface = indexLayerSurface+1;
                    else
                        Sa_botME70(indexESUME70,indexLayerBottom,:)=max(0,AllData(1,indexResults).m_Sa(indexLayer,:));
                        Sv_botME70(indexESUME70,indexLayerBottom,:)=max(-100,AllData(1,indexResults).m_Sv(indexLayer,:));
                        Lat_botME70(indexESUME70,indexLayerBottom,:)=AllData(1,indexResults).m_latitude(indexLayer,:);
                        Long_botME70(indexESUME70,indexLayerBottom,:)=AllData(1,indexResults).m_longitude(indexLayer,:);
                        Depth_botME70(indexESUME70,indexLayerBottom,:)=AllData(1,indexResults).m_Depth(indexLayer,:);
                        Volume_botME70(indexESUME70,indexLayerBottom,:)=LayersVolumes(indexLayer).Sounders(indexSounderME70).Channels(:);
                        indexLayerBottom = indexLayerBottom+1;
                    end;

                end;

                indexESUME70=indexESUME70+1;
            elseif nbbeams==nbchannelsER60
                time_ER60(indexESUER60) = AllData(1,indexResults).m_time;
                
                nbLayer= size(AllData(1,indexResults).m_Sa,1);
                
                for i_freq=1:nbbeams
                    indexLayerSurface = 1;
                    indexLayerBottom = 1;

                    for indexLayer=1:nbLayer

                        if LayersDef.LayerType(indexLayer)==0
                            Sa_surfER60(indexESUER60,indexLayerSurface,i_freq)=max(0,AllData(1,indexResults).m_Sa(indexLayer,indexER60(i_freq)));
                            Sv_surfER60(indexESUER60,indexLayerSurface,i_freq)=max(-100,AllData(1,indexResults).m_Sv(indexLayer,indexER60(i_freq)));
                            Lat_surfER60(indexESUER60,indexLayerSurface,i_freq)=AllData(1,indexResults).m_latitude(indexLayer,indexER60(i_freq));
                            Long_surfER60(indexESUER60,indexLayerSurface,i_freq)=AllData(1,indexResults).m_longitude(indexLayer,indexER60(i_freq));
                            Depth_surfER60(indexESUER60,indexLayerSurface,i_freq)=AllData(1,indexResults).m_Depth(indexLayer,indexER60(i_freq));
                            Volume_surfER60(indexESUER60,indexLayerSurface,i_freq)=LayersVolumes(indexLayer).Sounders(indexSounderER60).Channels(indexER60(i_freq));
                            indexLayerSurface = indexLayerSurface+1;
                        else
                            Sa_botER60(indexESUER60,indexLayerBottom,i_freq)=max(0,AllData(1,indexResults).m_Sa(indexLayer,indexER60(i_freq)));
                            Sv_botER60(indexESUER60,indexLayerBottom,i_freq)=max(-100,AllData(1,indexResults).m_Sv(indexLayer,indexER60(i_freq)));
                            Lat_botER60(indexESUER60,indexLayerBottom,i_freq)=AllData(1,indexResults).m_latitude(indexLayer,indexER60(i_freq));
                            Long_botER60(indexESUER60,indexLayerBottom,i_freq)=AllData(1,indexResults).m_longitude(indexLayer,indexER60(i_freq));
                            Depth_botER60(indexESUER60,indexLayerBottom,i_freq)=AllData(1,indexResults).m_Depth(indexLayer,indexER60(i_freq));
                            Volume_botER60(indexESUER60,indexLayerBottom,i_freq)=LayersVolumes(indexLayer).Sounders(indexSounderER60).Channels(indexER60(i_freq));
                            indexLayerBottom = indexLayerBottom+1;
                        end;
                    end
                end;
                
                indexESUER60=indexESUER60+1;
            elseif nbbeams==nbchannelsER60h
                time_ER60h(indexESUER60h) = AllData(1,indexResults).m_time;
                
                nbLayer= size(AllData(1,indexResults).m_Sa,1);
                
                for i_freq=1:nbbeams
                    indexLayerSurface = 1;
                    
                    for indexLayer=1:nbLayer
                        
                        if LayersDef.LayerType(indexLayer)==2
                            Sa_surfER60h(indexESUER60h,indexLayerSurface,i_freq)=max(0,AllData(1,indexResults).m_Sa(indexLayer,:));
                            Sv_surfER60h(indexESUER60h,indexLayerSurface,i_freq)=max(-100,AllData(1,indexResults).m_Sv(indexLayer,:));
                            Lat_surfER60h(indexESUER60h,indexLayerSurface,i_freq)=AllData(1,indexResults).m_latitude(indexLayer,:);
                            Long_surfER60h(indexESUER60h,indexLayerSurface,i_freq)=AllData(1,indexResults).m_longitude(indexLayer,:);
                            Depth_surfER60h(indexESUER60h,indexLayerSurface,i_freq)=AllData(1,indexResults).m_Depth(indexLayer,:);
                            Volume_surfER60h(indexESUER60h,indexLayerSurface,i_freq)=LayersVolumes(indexLayer).Sounders(indexSounderER60h).Channels(:);
                            indexLayerSurface = indexLayerSurface+1;
                        end;
                    end
                end;
                indexESUER60h=indexESUER60h+1;
            end
        end
        
        %Save results
        if ~exist(chemin_save,'dir')
            mkdir(chemin_save)
        end 
        str_bloc='0000';
        str_bloc(end-length(num2str(num_bloc))+1:end)=num2str(num_bloc);
        save ([chemin_save, '/results','_',str_Name, str_bloc,'_EI'],'time_ER60','time_ER60h','time_ME70','Sa_surfME70', 'Sa_botME70', 'Sa_surfER60', 'Sa_botER60', 'Sa_surfER60h','Sv_surfME70', 'Sv_botME70', 'Sv_surfER60', 'Sv_botER60', 'Sv_surfER60h','Lat_surfME70', 'Long_surfME70', 'Depth_surfME70','Lat_surfER60','Long_surfER60','Depth_surfER60','Lat_surfER60h','Long_surfER60h','Depth_surfER60h','Lat_botME70', 'Long_botME70', 'Depth_botME70','Lat_botER60','Long_botER60','Depth_botER60','Volume_surfER60','Volume_botER60','Volume_surfER60h','Volume_surfME70','Volume_botME70') ;

        clear time_ER60 time_ER60h time_ME70 Sa_surfME70 Sa_botME70 Sa_surfER60 Sa_botER60 Sa_surfER60h  Sv_surfME70 Sv_botME70 Sv_surfER60 Sv_botER60  Sv_surfER60h Lat_surfME70 Long_surfME70 Depth_surfME70 Lat_surfER60 Long_surfER60 Depth_surfER60 Lat_surfER60h Long_surfER60h Depth_surfER60h Lat_botME70 Long_botME70 Depth_botME70 Lat_botER60 Long_botER60 Depth_botER60 
        clear AllData;
        clear Volume_surfER60 Volume_botER60 Volume_surfER60h  Volume_surfME70 Volume_botME70;

        num_bloc=num_bloc+1;
        FileStatus= moGetFileStatus();
    end    
    end

