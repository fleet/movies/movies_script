% Stephan van Zyl - IFREMER TFE
% 19.06.2007

% function chargecas charges data from casino files 


function [deth,lat,lon,vitf,vits,roul,tang,prof,radiation,venta,dira,cap,dirc...
    ventv,dirv,pilon,temperature,salinity,fluo] = chargecas (file)

%% creating a string which specifies how to read the values of the casino
%% file
n = 97;     % number of strings(fields) in the file type 'pel060508.cas'
str = '';
for k = 1:n;
    if (k == 1  || k == 2)          % date et heure
        str = [str '%s'];
    elseif (k == 5  || k == 6)      % latitude et longitude
        str = [str '%s'];
   elseif (k == 10  || k == 12)     % vitesse fond et surface
        str = [str '%n'];
    elseif (k == 16  || k == 17  || k == 18 || k == 19)    % cap navire, roulis, 
                                                           % tangage et pilonnement
        str = [str '%n'];
    elseif (k == 20)                % profondeur
        str = [str '%n'];
    elseif (k==35)              % radiation
         str = [str '%n'];
    elseif (k == 37  || k == 38 || k == 39 || k == 40)      % direction et force du vent
                                                            % apperent et vrai
        str = [str '%n'];
    elseif (k==43 || k == 45) %temperature et salinit�
         str = [str '%n'];
    elseif (k==69) %fluo
         str = [str '%n'];
    else                                                    % nothhing
        str = [str '%*s'];
    end
end 

%% opening the file with the pre-defined string str
fid = fopen(file);
[data] = textscan(fid,str,'delimiter','\t');
fclose(fid);

% for j = 1:length(data{1,1})
%     if length(data{1,1}{j,:}) ~= 8
%         j,
%     end
% end

%% saving the data
data1 = cell2mat(data{1,1});
data2 = cell2mat(data{1,2});

for i = 1:length(data1)
    dethh = ([data1(i,:),num2str(' '),data2(i,:)]);
    deth(i,:) = datevec(dethh, 'dd/mm/yy HH:MM:SS');
    latt = cell2mat(data{1,3}(i));
    if strncmp('N', latt,1) == 1 
        lat(i) = str2double(latt(3:4)) + str2double(latt(6:end))/60;
    elseif strncmp('S', latt,1) == 1 
        lat(i) = -str2double(latt(3:4)) - str2double(latt(6:end))/60;
    else
        lat(i)=NaN;
    end
    lonn = cell2mat(data{1,4}(i));
    if strncmp('E', lonn,1) == 1
        lon(i) = str2double(lonn(3:5)) + str2double(latt(7:end))/60; %attention longitude sur trois chiffres et latitude sur 2 chiffres est ce toujours le cas?
    elseif strncmp('W', lonn,1) == 1
        lon(i) = -str2double(lonn(3:5)) - str2double(lonn(7:end))/60;
    else
        lat(i)=NaN;
    end
    if data{1,13}(i) > 180 
        dira(i) = data{1,13}(i)-180;  % direction du vent en l'autre direction
    else
        dira(i) = data{1,13}(i)+180;
    end
    if data{1,15}(i) > 180 
        dirv(i) = data{1,15}(i)-180;  % direction du vent en l'autre direction
    else
        dirv(i) = data{1,15}(i)+180;
    end
end
vitf = data{1,5};
vits = data{1,6};
cap = data{1,7};
roul = data{1,8};
tang = data{1,9};
pilon = data{1,10};
prof = data{1,11};
radiation=data{1,12};
venta = data{1,13};
ventv = data{1,15};
temperature=data{1,17};
salinity=data{1,18};
fluo=data{1,19};

dirc = dirv - cap';
