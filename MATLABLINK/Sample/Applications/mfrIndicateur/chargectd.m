% Laurent BERGER- IFREMER
% 24.03.2011

% function chargecas charges data from ctd files 


function [nbstation,date,lat,lon,depth,temperature,salinity,fluo] = chargectd (file)

%% creating a string which specifies how to read the values of the ctd
%% file
n = 17;     % number of strings(fields) in the file type 'ctd2006.txt'
str = '';
for k = 1:n
    if (k == 2)          % num�ro station
        str = [str '%s'];
    elseif (k == 4)          % date 
        str = [str '%s'];
    elseif (k == 5  || k == 6)      % latitude et longitude
        str = [str '%n'];
    elseif (k == 8)                % profondeur
        str = [str '%n'];
    elseif (k == 9  || k == 11 || k == 13)      % temp�rature salinit� florim�trie
        str = [str '%n'];
    else                                                    % nothhing
        str = [str '%*s'];
    end
end 

%% opening the file with the pre-defined string str
fid = fopen(file);
[data] = textscan(fid,str,'delimiter','\t','Headerlines',1);
fclose(fid);


%% saving the data
station=cell2mat(data{1,1});
nbstation=str2num(station(:,3:end));
date=datenum(datevec(data{1,2}));
lat = data{1,3};
lon = data{1,4};
depth = data{1,5};
temperature = data{1,6};
salinity = data{1,8};
fluo = data{1,7};

