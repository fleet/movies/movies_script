%% file for reading biom results and averaging over a spatial
%% grid

 close all;
 clear all;

%import acoustics results
dirdata='K:\SMFH\Indicateurs\data\';
yearlist=[1997:1:2010];

filebiom = [dirdata,'EvhoeRichesseSpecifique.csv'];
   [year,richness,latESU,lonESU] = chargerichness (filebiom);
   
       
    mX=17;
    nX=17;
    alpha=1;
    longmin = -6;
    longmax= -1;
    latmin = 43.5;
    latmax = 48.5;
    maxdepth=200;
    hourmin=4;
    hourmax=19;
    
    matRichness=NaN(mX,nX,length(yearlist));
   
for years= 1:length(yearlist)
    

    
    % interlat = min(sallat):(max(sallat)-min(sallat))/mX:max(sallat);
    interlat = latmin:(latmax-latmin)/mX:latmax;
    for i = 1:mX
        slatm(i) = (interlat(i) );
    end
    %create vector with midpoint of latitude grid
    Lat=interlat(1:(mX))+ 0.5 * (latmax-latmin)/mX;
    % interlon = min(sallon):(max(sallon)-min(sallon))/nX:max(sallon);
    interlon = longmin:(longmax-longmin)/nX:longmax;
    for j = 1:nX
        slonn(j) = (interlon(j));
    end
    Long=interlon(1:(nX)) +0.5*(longmax-longmin)/nX;
    
    
    % creating mxn-matrix by depth category by averaging values in cells
    for m = 1:mX   %lat
        for n = 1:nX %long
            ff = find(latESU>= interlat(m) & latESU<= interlat(m+1) & lonESU>=interlon(n) & lonESU<= interlon(n+1));
           
            if ~isempty(ff)
                
                jj= find((year(ff)==(yearlist(years)))) ;
                
                if ~isempty(jj)
                      matRichness(m,n,years)=0;
                    number=richness(ff(jj));
                    for i=1:size(number,1)                      
                        matRichness(m,n,years)=matRichness(m,n,years)+number(i);
                    end
                    matRichness(m,n,years)=matRichness(m,n,years)/size(number,1);
                end
            end
        end
    end
    
    
end
   
        resf=[dirdata,'DataRichness.mat'];
        save(resf,'matRichness');
       
        data=matRichness;
        
        landareas = shaperead('landareas.shp','UseGeoCoords', true);
   rivers = shaperead('worldrivers', 'UseGeoCoords', true);
  cities = shaperead('worldcities', 'UseGeoCoords', true);
  
        datamean=zeros(mX,nX);
        nbmes=zeros(mX,nX);
        for years= 1:length(yearlist)
            for m = 1:mX   %lat
                for n = 1:nX %long
                    if (data(m,n,years)>0)
                        datamean(m,n)=datamean(m,n)+data(m,n,years);
                        nbmes(m,n)=nbmes(m,n)+1;
                    end
                end
            end
        end
        datamean= datamean./nbmes;
        
        figure(10);
        h=pcolor(Long,Lat,(datamean));
        
        set(h,'edgecolor','none')
        hold on;
geoshow(landareas, 'FaceColor', 'white');
        for index=1:numel(cities)
            if (strcmp(cities(index).Name,'Nantes'))
                text(cities(index).Lon, cities(index).Lat,'Loire','FontSize',11,'Color', 'blue');
            elseif (strcmp(cities(index).Name,'Bordeaux'))
                text(cities(index).Lon, cities(index).Lat,'Gironde','FontSize',11,'Color', 'blue');
            end
        end
        geoshow(rivers, 'Color', 'blue')
        caxis([15 33]);
        axis([-6 0 43 49]);
        colorbar;
        title('Mean species richness');
    
% Reshape data to 2D matrix of (lat_long_cell, depth)
data=reshape(data,[size(data,1)*size(data,2),size(data,3)]);
% transpose data matrix round to make it depth x latlong
data=data.';
%datamask are cells to be included: those with positive values
numdata=sum((data),1);
datamask=isfinite(numdata);
F=data(:,datamask);
[M,N]=size(F);
Fmean=mean(F);

% Eigenvectors and eigenvalues - use SVD
% [E,D,E2]=svd(R);
[E,D,E2]=svd(F);

% Normalized eigenvalues
evaln=diag(D)./sum(diag(D)); %./ elementwise devision

% Principal components
% F2=F;
% F2(isnan(F2))=0;
% U=(E'*F2')'; % dimension of U: depth x number of cells
U=E*D;

% Put the eigenvectors back into the original lon/lat state
% Remember to re-insert the gridpoints you left out
Ep=ones(mX*nX,N)*NaN;
Ep(datamask,:)=E2;
Ep=reshape(Ep,mX,nX,N);

Fp=ones(mX*nX,1)*NaN;
Fp(datamask)=Fmean;
Fp=reshape(Fp,mX,nX);

  
  
years=(yearlist);
for ip=1:2  % show all principal components (there are as many components as years)
       scrsz = get(0,'ScreenSize');
figure('Position',[20 scrsz(4)/2+20 scrsz(3)-40 scrsz(4)/2-40])
    h=subplot(1,2,1)
    subplot('Position',[0.03 0.05 0.53 0.9]);

    % turn signs around if eigenvalues are negative to faciliatate
    % interpretation of maps!
    h=pcolor(Long,Lat,squeeze(Ep(:,:,ip))*sign(U(1,ip)));
        set(h,'edgecolor','none')
        
hold on;
 geoshow(landareas, 'FaceColor', 'white');
for index=1:numel(cities)
    if (strcmp(cities(index).Name,'Nantes'))
        text(cities(index).Lon, cities(index).Lat,'Loire','FontSize',11,'Color', 'blue');
    elseif (strcmp(cities(index).Name,'Bordeaux'))
            text(cities(index).Lon, cities(index).Lat,'Gironde','FontSize',11,'Color', 'blue');
        end
    end
geoshow(rivers, 'Color', 'blue')

axis([-6 0 43 49]);

%     set(h,'edgecolor','none') 
%     caxis([-0.2 0.2]);
    colorbar;
    title(cat(2,cat(2,cat(2,'EOF',num2str(ip)),' across years ')),'FontSize',12);
% You can also use this for plotting (uncomment next two lines)
%contourf(lon,lat,squeeze(Ep(:,:,ip)),(-0.2:0.02:0.2));
%colorbar;
    h=subplot(1,2,2)
subplot('Position',[0.65 0.05 0.33 0.9]);
%     bar((1:M),evaln(1:M)); %evaln are normalised eigenvalues; show first 10
%     grid;
%     axis([0 5 0 1]);
%     title('Normalized eigenvalues');
%     subplot(2,2,3);
%     h=pcolor(Long,Lat,Fp);
%         set(h,'edgecolor','none')
%     title('Mean proportion resonant organisms');
%     hold on;
%      geoshow(landareas);
%     colorbar;
%     subplot(2,2,4)
    plot(years,U(:,ip).*sign(U(:,ip)),'--ks'); % U are eigenvectors (maximum number is number of cells)
    set(gca,'xtick',[2006 2008 2009 2010]);
    grid on;
    title(cat(2,cat(2,cat(2,'PC ',num2str(ip)),'  percent variance expl='),num2str(evaln(ip)*100)),'FontSize',12);
   saveas(gcf,[dirdata,'EofYearsBiom',num2str(ip)],'jpg');
end;

% Clear some vars
clear E2 F2 R F Fmean E D 


