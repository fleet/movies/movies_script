%% file for reading biom results and averaging over a spatial
%% grid

 close all;
 clear all;

%import acoustics results
dirdata='K:\SMFH\Indicateurs\data\Mars3D';
yearlist=[2006:1:2008];


   
    mX=25;
    nX=25;
    alpha=1;
    longmin = -6;
    longmax= -1;
    latmin = 43.5;
    latmax = 48.5;
    maxdepth=200;
    hourmin=4;
    hourmax=19;
    
    matPhyto=NaN(mX,nX,length(yearlist));
   
for years= 1:length(yearlist)
    

    filencdf = [dirdata,'\',num2str(yearlist(years)),'_ARP_GDGE_previ.nc.all'];
    ncid=netcdf.open(filencdf,'NC_NOWRITE')
    
    latESU=netcdf.getVar(ncid,0);
    lonESU=netcdf.getVar(ncid,1);
    time=netcdf.getVar(ncid,2)/86400+693962;
    sigma=netcdf.getVar(ncid,4);
    H0=netcdf.getVar(ncid,5);
    H0(H0==-999)=NaN;
%     MES=netcdf.getVar(ncid,15);
%     MES(MES==999)=NaN;
%     nanopico=netcdf.getVar(ncid,18);
%     nanopico(nanopico==999)=NaN;
%     mesozoo=netcdf.getVar(ncid,19);
%     mesozoo(mesozoo==999)=NaN;
%     microzoo=netcdf.getVar(ncid,20);
%     microzoo(microzoo==999)=NaN;
    prod_diatom=netcdf.getVar(ncid,25);
    prod_diatom(prod_diatom==999)=NaN;
    prod_dino=netcdf.getVar(ncid,26);
     prod_dino(prod_dino==999)=NaN;
    prod_pico=netcdf.getVar(ncid,27);
    prod_pico(prod_pico==999)=NaN;
    prod_phyto=prod_diatom+prod_dino+prod_pico;
    
    % interlat = min(sallat):(max(sallat)-min(sallat))/mX:max(sallat);
    interlat = latmin:(latmax-latmin)/mX:latmax;
    for i = 1:mX
        slatm(i) = (interlat(i) );
    end
    %create vector with midpoint of latitude grid
    Lat=interlat(1:(mX))+ 0.5 * (latmax-latmin)/mX;
    % interlon = min(sallon):(max(sallon)-min(sallon))/nX:max(sallon);
    interlon = longmin:(longmax-longmin)/nX:longmax;
    for j = 1:nX
        slonn(j) = (interlon(j));
    end
    Long=interlon(1:(nX)) +0.5*(longmax-longmin)/nX;
    
    matdirp='Q:\Projects\ObsHal\Indicateurs\Data\';
     datdir= [matdirp,'\',num2str(yearlist(years)),'\'];
    datf=[datdir,'DataMat.mat'];
    load(datf);
   
    
    % creating mxn-matrix by depth category by averaging values in cells
    for m = 1:mX   %lat
        for n = 1:nX %long
            ff = find(latESU>= interlat(m) & latESU<= interlat(m+1)  );
           
            if ~isempty(ff)
                
                jj= find(lonESU>=interlon(n) & lonESU<= interlon(n+1)) ;
                
                if ~isempty(jj)
                    tt=find(abs(time-matdateER60(m,n))<3);
                    if ~isempty(tt)
                        phyto=0;
                        nbPhyto=0;
                        for j=1:length(jj)
                            for f=1:length(ff)
                                if ((H0(jj(j),ff(f)))<200)
                                    depth=H0(jj(j),ff(f))*(1-sigma(end:-1:1));
                                    dd=find(depth>=0 & depth<=30);
%                                       if sum(dd<10)~=0
%                                         toto=0;
%                                     end
                                    if ~isempty(dd)
                                        phyto=phyto+mean(max(prod_phyto(jj(j),ff(f),end-dd+1,tt),[],3));
                                        nbPhyto=nbPhyto+1;
                                    end
                                end
                            end
                        end
                        if nbPhyto>0
                            matPhyto(m,n,years)=(phyto/nbPhyto);
                        end
                    end
                end
            end
        end
    end
    
    
end
   
        resf=[dirdata,'DataPhyto.mat'];
        save(resf,'matPhyto');
       
        data=matPhyto;
        
        landareas = shaperead('landareas.shp','UseGeoCoords', true);
  
        datamean=zeros(mX,nX);
        nbmes=zeros(mX,nX);
        for years= 1:length(yearlist)
            for m = 1:mX   %lat
                for n = 1:nX %long
                    if (data(m,n,years)>0)
                        datamean(m,n)=datamean(m,n)+data(m,n,years);
                        nbmes(m,n)=nbmes(m,n)+1;
                    end
                end
            end
        end
        datamean= datamean./nbmes;
        
        figure(10);
        h=pcolor(Long,Lat,(datamean));
        set(h,'edgecolor','none')
        hold on;
        geoshow(landareas);
          colorbar;
    title('Mean species richness');
    
     for years= 1:length(yearlist)
          figure;
        h=pcolor(Long,Lat,squeeze(data(:,:,years)));
        set(h,'edgecolor','none')
        hold on;
        geoshow(landareas);
          colorbar;
    title('Mean species richness');
     end
    
% Reshape data to 2D matrix of (lat_long_cell, depth)
data=reshape(data,[size(data,1)*size(data,2),size(data,3)]);
% transpose data matrix round to make it depth x latlong
data=data.';
%datamask are cells to be included: those with positive values
numdata=sum((data),1);
datamask=isfinite(numdata);
F=data(:,datamask);
[M,N]=size(F);
Fmean=mean(F);

% Eigenvectors and eigenvalues - use SVD
% [E,D,E2]=svd(R);
[E,D,E2]=svd(F);

% Normalized eigenvalues
evaln=diag(D)./sum(diag(D)); %./ elementwise devision

% Principal components
% F2=F;
% F2(isnan(F2))=0;
% U=(E'*F2')'; % dimension of U: depth x number of cells
U=E*D;

% Put the eigenvectors back into the original lon/lat state
% Remember to re-insert the gridpoints you left out
Ep=ones(mX*nX,N)*NaN;
Ep(datamask,:)=E2;
Ep=reshape(Ep,mX,nX,N);

Fp=ones(mX*nX,1)*NaN;
Fp(datamask)=Fmean;
Fp=reshape(Fp,mX,nX);


years=(yearlist);
for ip=1:2  % show all principal components (there are as many components as years)
    figure
    subplot(1,2,1)
    % turn signs around if eigenvalues are negative to faciliatate
    % interpretation of maps!
    h=pcolor(Long,Lat,squeeze(Ep(:,:,ip))*sign(U(1,ip)));
        set(h,'edgecolor','none')
hold on;
axis([longmin-2 longmax latmin latmax]);
 geoshow(landareas);


%     set(h,'edgecolor','none') 
%     caxis([-0.2 0.2]);
    colorbar;
    title(cat(2,cat(2,cat(2,'EOF',num2str(ip)),' across years ')));
% You can also use this for plotting (uncomment next two lines)
%contourf(lon,lat,squeeze(Ep(:,:,ip)),(-0.2:0.02:0.2));
%colorbar;
    subplot(1,2,2)
%     bar((1:M),evaln(1:M)); %evaln are normalised eigenvalues; show first 10
%     grid;
%     axis([0 5 0 1]);
%     title('Normalized eigenvalues');
%     subplot(2,2,3);
%     h=pcolor(Long,Lat,Fp);
%         set(h,'edgecolor','none')
%     title('Mean proportion resonant organisms');
%     hold on;
%      geoshow(landareas);
%     colorbar;
%     subplot(2,2,4)
    plot(years,U(:,ip).*sign(U(:,ip))); % U are eigenvectors (maximum number is number of cells)
    grid on;
    title(cat(2,cat(2,cat(2,'PC ',num2str(ip)),'  percent variance expl='),num2str(evaln(ip)*100)));
   saveas(gcf,[dirdata,'EofYearsBiom',num2str(ip)],'jpg');
end;

% Clear some vars
clear E2 F2 R F Fmean E D 


