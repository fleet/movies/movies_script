% Laurent BERGER- IFREMER
% 24.03.2011

% function chargecas charges data from ctd files 


function [datetime,lat,lon,year,specie,biom] = chargebiom (file)

%% creating a string which specifies how to read the values of the ctd
%% file
n = 17;     % number of strings(fields) in the file type 'Biomres.csv'
str = '';
for k = 1:n
    if ( k==3)          % date et heure
        str = [str '%s'];
    elseif    ( k==1)          % date et nom campagne
        str = [str '%s'];
    elseif (k == 9)          % esp�ce 
        str = [str '%s'];
          elseif (k == 12)                % biomasse
        str = [str '%n'];
    elseif (k == 6  || k == 7)      % latitude et longitude
        str = [str '%n'];
  
    else                                                    % nothhing
        str = [str '%*s'];
    end
end 

%% opening the file with the pre-defined string str
fid = fopen(file);
[data] = textscan(fid,str,'delimiter',',','Headerlines',1);
fclose(fid);


%% saving the data
datetime= data{1,2};
year=cell2mat(data{1,1});
year=str2num(year(:,5:6));
specie=cell2mat(data{1,5});
biom = data{1,3};
lat = data{1,4};
lon = data{1,6};


