%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%
%%% programme pour lire les metadonn�es de navigation et d'environnement et les mettre 
%%% en relation avec les d'indicateurs calcul�es par le script
%%% indicateurs et telles que d�finis dans la publi "Development and application of an
%%% acoustic multi-frequency diversity index for monitoring major scatter 
%%% groups in large scale pelagic ecosystems" soumise � MEPS en Aout 2011
%%% les filtrer suivant la m�teo et la navigation, caluler les proportions
%%% pour diff�rentes classes d'organismes et les moyenner spatialement
%%%
%%% Laurent Berger 09/2011
%%%
%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


clear all;
close all;

dirdata='C:\data\PELGAS\PELGAS09\';


%import acoustics results
filelist = ls([dirdata,'Clarke*.mat']);  % ensemble des fichiers
nb_files = size(filelist,1);  % nombre de fichiers
sort(filelist);


nblER60=0;
for numfile = 1:nb_files
    matfilename = filelist(numfile,:)

    FileName = [dirdata,matfilename(1:findstr('.',matfilename)-1),'.mat'];
    load(FileName);


    latER60all(nblER60+1:nblER60+length(latER60)) = latER60;
    longER60all(nblER60+1:nblER60+length(longER60)) = longER60;

    timeER60all(nblER60+1:nblER60+length(timeER60)) = timeER60/86400 +719529;

    depthER60all(nblER60+1:nblER60+length(depthER60)) = depthER60;

    SvER60all(nblER60+1:nblER60+size(CellulesER60,1),:,:) = CellulesER60;

    ClarkeER60all_f(nblER60+1:nblER60+size(delta_ER60f,1),:) = delta_ER60f;
    
   
    nblER60=length(latER60all);

    clear  latER60  longER60 timeER60 depthER60;
    clear  CellulesER60  delta_ER60g delta_ER60f;
end


%import CASINO
%%%%%%%%%%%%%%
jour =  {'0426' '0427' '0428' '0429' '0430' ...
         '0501' '0502' '0503' '0504' '0505' '0506' '0507' '0508' '0509' '0510' ...
         '0511' '0512' '0513' '0514' '0515' '0516' '0517' '0518' '0519' '0520' ...
         '0521' '0522' '0523' '0524' '0525' '0526' '0527' '0528' '0529' '0530' };

% nod = [2:24 26:32];
% nod = [7:34];
nod = [1:35];

dethcf=[];
ventvf=[];
ventaf=[];
vitscf=[];
pilof=[];
latf=[];
lonf=[];
radiationf=[];
temperaturef=[];
salinityf=[];
fluof=[];

for d = nod
    day{d} = num2str(jour{d});

    
    %% charging the data of Casino
    filec = [dirdata,'casino\PELGAS09\pel09',day{d},'.cas'];
    [dethc,latc,lonc,vitfc,vitsc,roul,...
        tang,prof,radiation,venta,dira,cap,dirc,ventv,dirv,pilo,temperature,salinity,fluo] = ...
        chargecas (filec);
    
    latf=cat(1,latf,latc');
    lonf=cat(1,lonf,lonc');
    dethcf=cat(1,dethcf, datenum(dethc));
    radiationf=cat(1,radiationf,radiation);
    ventvf=cat(1,ventvf, ventv);
    ventaf=cat(1,ventaf, venta);
    vitscf=cat(1,vitscf, vitsc);
    pilof=cat(1,pilof, pilo);
    temperaturef=cat(1,temperaturef, temperature);  
    salinityf=cat(1,salinityf, salinity); 
    fluof=cat(1,fluof, fluo); 
    
end

%on r��chantillonne sur les donn�es acoustiques
truewind=NaN(length(timeER60all),1);
vesselspeed=NaN(length(timeER60all),1);
heave=NaN(length(timeER60all),1);
temperature=NaN(length(timeER60all),1);
salinity=NaN(length(timeER60all),1);
fluo=NaN(length(timeER60all),1);
radiation=NaN(length(timeER60all),1);
for i=1:length(timeER60all)
   index=find( abs(timeER60all(i)-dethcf)<(15/86400));
   if ~isempty(index)
        truewind(i)=ventvf(index(1));
        vesselspeed(i)=vitscf(index(1));
        heave(i)=pilof(index(1));
        temperature(i)=temperaturef(index(1));
         salinity(i)=salinityf(index(1));
          fluo(i)=fluof(index(1));
          radiation(i)=radiationf(index(1));
   end
end

%on filtre les valeurs aberrantes
temperature(temperature<0)=NaN;
salinity(salinity<0)=NaN;
fluo(fluo<0)=NaN;

%on sauvegarde pour utilisation ult�rieure
save ([dirdata,'Nav'],'truewind','vesselspeed','heave');
save ([dirdata,'Env'],'temperature','salinity','fluo','radiation');


% %import ctd file
% %%%%%%%%%%%%%%%%
% filectd = [dirdata,'ctd\CTD2010.txt'];
%    [nostationctd,datectd,latctd,lonctd,depthctd,temperaturectd,salinityctd,fluoctd] = chargectd (filectd);
%    
% nostationctdunique=unique(nostationctd);
% 
% nbstation=length(nostationctdunique);
% fluoctdstation=NaN(nbstation,4);
% latctdstation=NaN(nbstation,1);
% lonctdstation=NaN(nbstation,1);
% datectdstation=NaN(nbstation,1);
% for i=1:nbstation
%     indexstation=find(nostationctd==nostationctdunique(i));
%     indexlayer1=find(depthctd(indexstation)>=10 & depthctd(indexstation)<=15);
%     fluoctdstation(i,1)=mean(fluoctd(indexlayer1));
%     indexlayer2=find(depthctd(indexstation)>=15 & depthctd(indexstation)<=20);
%     fluoctdstation(i,2)=mean(fluoctd(indexlayer2));
%     indexlayer3=find(depthctd(indexstation)>=20 & depthctd(indexstation)<=25);
%     fluoctdstation(i,3)=mean(fluoctd(indexlayer3));
%     indexlayer4=find(depthctd(indexstation)>=25 & depthctd(indexstation)<=30);
%     fluoctdstation(i,4)=mean(fluoctd(indexlayer4));    
%     latctdstation(i)=unique(latctd(indexstation));
%     lonctdstation(i)=unique(lonctd(indexstation));
%     datectdstation(i)=unique(datectd(indexstation));
% end
% 
% %on r��chantillonne sur les donn�es acoustiques
% indexdate=datectdstation>=min(timeER60all) & datectdstation<=max(timeER60all);
% latctd=latctdstation(indexdate);
% lonctd=lonctdstation(indexdate);
% fluoctd=fluoctdstation(indexdate,:);
% 
% %on sauvegarde pour utilisation ult�rieure
% save ([dirdata,'CTD'],'latctd','lonctd','fluoctd');
