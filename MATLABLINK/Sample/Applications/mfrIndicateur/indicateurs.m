%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%
%%% programme pour calculer les indictaurs de diversit� fr�quentielle
%%% tels que d�finis dans la publi "Development and application of an
%%% acoustic multi-frequency diversity index for monitoring major scatter 
%%% groups in large scale pelagic ecosystems" soumise � MEPS en Aout 2011
%%%
%%% Laurent Berger 09/2011
%%%
%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


clear all;
close all;

% r�pertoire des fichiers de configuration et de r�sultats
dirdata='C:\data\PELGAS09\';

% r�pertoire des donn�es acoustiques au fromat hac
dirhac='X:\PELGAS09\HAC-hermes\';

%chargement config M3D pr�alablement cr��e sous M3D 
% comportant la lecture des fichiers par chunk
% le filtrage de l'�cho de fond et les param�tres d'EI par couches
% la configuration des couches de la publi est
% 40 couches surface de 5 m
% 1 couche fond de 1 m
% 4 couches fond de 5 m de 1 � 21 m du fond
% un ESU de 0.01nm=18.5m
% un seuil de traitement de -85dB
chemin_config = dirdata;
chemin_config_reverse = strrep(chemin_config, '\', '/');
moLoadConfig(chemin_config_reverse);

%activation EI par couches
EchoIntegrModule = moEchoIntegration();
moSetEchoIntegrationEnabled(EchoIntegrModule);

% on boucle sur les RUN de la campagne pour faire l'EI par couches
% for m=[1:30]
% 
%     if m<10
%         RUN=strcat('RUN00',num2str(m))
%     else
%         RUN=strcat('RUN0',num2str(m))
%     end;
% 
%     chemin_hac=[dirhac,RUN,'\'];
%     chemin_save=[dirdata,'lay\',RUN,'\'];
%     if ~exist(chemin_save,'dir')
%             mkdir(chemin_save)
%     end
% 
%     SampleEchoIntegration(chemin_hac,chemin_save,chemin_config,EchoIntegrModule);
% 
% end;

% on boucle sur les r�sultats de l'EI couche pour calculer l'indicateur
for m=[4:30]
  
    if m<10
        RUN=strcat('RUN00',num2str(m))
    else
        RUN=strcat('RUN0',num2str(m));
    end;

    chemin_results=[dirdata,'lay\',RUN,'\'];

    % stockage des r�sultats
    depth_bottom_ER60=[];
    Sv_surface_ER60 = [];
    depth_bottom_ME70=[];
    Sv_surface_ME70 = [];
    latER60 = [];
    longER60 =[];

    % ensemble des fichiers r�sultats d'EI au format Matlab pr�c�demment
    % calcul�s
    filelist = ls([chemin_results,'*_EI.mat']);  % ensemble des fichiers
    nb_files = size(filelist,1);  % nombre de fichiers
    sort(filelist);

    nblER60=0;
    for numfile = 1:nb_files

        matfilename = filelist(numfile,:)
        FileName = [chemin_results,matfilename(1:findstr('.',matfilename)-1),'.mat'];
        load(FileName);

        if  size(Lat_surfER60,1)>0
            timeER60(nblER60+1:nblER60+size(Depth_botER60,1)) = time_ER60;
            depth_bottom_ER60(nblER60+1:nblER60+size(Depth_botER60,1),:,:) = Depth_botER60;
            depth_surface_ER60(nblER60+1:nblER60+size(Depth_surfER60,1),:,:) = Depth_surfER60;
            Sv_bottom_ER60(nblER60+1:nblER60+size(Sv_botER60,1),:,:) = Sv_botER60;
            Sv_surface_ER60(nblER60+1:nblER60+size(Sv_surfER60,1),:,:) = Sv_surfER60;
            latER60(nblER60+1:nblER60+size(Lat_surfER60,1)) = Lat_surfER60(:,1,2);
            longER60(nblER60+1:nblER60+size(Long_surfER60,1)) = Long_surfER60(:,1,2);

            nblER60=size(depth_bottom_ER60,1);
        end

        clear  Depth_botER60  Sv_botER60 Sv_surfER60;
        clear  Depth_botME70  Sv_botME70 Sv_surfME70 Volume_surfME70;
        clear Lat_surfER60 Long_surfER60

    end

    if size(Sv_surface_ER60)>0
        
        maxdepth = max(depth_bottom_ER60(depth_bottom_ER60(:,1,1)<200,1,1));
        nblayerutile=floor(maxdepth/5);

        nbfreqER60 = size(Sv_surface_ER60,3);
        nbesuER60 = size(Sv_surface_ER60,1);
     
        CellulesER60=[];
        for freq=1:nbfreqER60
            CellulesER60(:,:,freq)=[Sv_bottom_ER60(:,2:5,freq) Sv_surface_ER60(:,:,freq)];
        end
        nblayerER60=44;
        
        %% recalage �talonnage PELGAS
        % attention !!! correction �talonnage pour PELGAS
        %on corrige le 200kHz en 2007 et 2008
        if (strcmp(datestr(timeER60(1)/86400 +719529,'yyyy'),'2007') || strcmp(datestr(timeER60(1)/86400 +719529,'yyyy'),'2008'))
            CellulesER60(:,:,5)=CellulesER60(:,:,5)-2*(26.53-25.67);
        end
        
        %on corrige avec l'�talonnage de fin de campagne en 2011
        if (strcmp(datestr(timeER60(1)/86400 +719529,'yyyy'),'2010'))
            CellulesER60(:,:,2)=CellulesER60(:,:,2)-2*(25.6-25.75);
            CellulesER60(:,:,3)=CellulesER60(:,:,3)-2*(26.86-26.92);
            CellulesER60(:,:,4)=CellulesER60(:,:,4)-2*(26.27-25.7);
            CellulesER60(:,:,5)=CellulesER60(:,:,5)-2*(26.31-26.4);
        end
        
 
        %on calcule l'indicateur � partir du sv en naturel
        CellulesER60_naturel = 10.^(CellulesER60./10);
        freq=[18 38 70 120 200];
        deltanum_ER60f=zeros(nbesuER60,nblayerER60);
        deltadenum_ER60f=zeros(nbesuER60,nblayerER60);
        delta_ER60f=NaN(nbesuER60,nblayerER60);
        for esu=1:nbesuER60
            for layer=1:nblayerER60
                for freq1=2:nbfreqER60
                    for freq2=1:freq1-1
                        if (~isnan(CellulesER60_naturel(esu,layer,:)))
                            CellulesER60_naturel_freq1=(CellulesER60_naturel(esu,layer,freq1)-min(CellulesER60_naturel(esu,layer,:)))/(max(CellulesER60_naturel(esu,layer,:))-min(CellulesER60_naturel(esu,layer,:)));
                            CellulesER60_naturel_freq2=(CellulesER60_naturel(esu,layer,freq2)-min(CellulesER60_naturel(esu,layer,:)))/(max(CellulesER60_naturel(esu,layer,:))-min(CellulesER60_naturel(esu,layer,:)));
                            deltanum_ER60f(esu,layer)= deltanum_ER60f(esu,layer)+ CellulesER60_naturel_freq1*CellulesER60_naturel_freq2*(1-exp(-abs(freq(freq1)-freq(freq2))/(40)))*(1/freq(freq1))*(1/freq(freq2));
                            deltadenum_ER60f(esu,layer)= deltadenum_ER60f(esu,layer)+ CellulesER60_naturel_freq1*CellulesER60_naturel_freq2*(1/freq(freq1))*(1/freq(freq2));
                        end
                    end
                end
                if deltadenum_ER60f(esu,layer)>0
                    delta_ER60f(esu,layer)=(deltanum_ER60f(esu,layer)/deltadenum_ER60f(esu,layer)-0.4)/0.6;
                end
            end
        end

        depthER60=depth_bottom_ER60(:,1,2)-2.5;
        
        %on sauvegarde les r�sultats
        save ([dirdata,'Clarke_',RUN],'longER60','latER60','timeER60','depthER60','CellulesER60','delta_ER60f');

        clear timeER60 timeME70 depth_surface_ER60 depth_surface_ME70 depth_bottom_ER60 depth_bottom_ME70
        clear Sv_surface_ER60 Sv_surface_ME70 Sv_bottom_ER60 Sv_bottom_ME70
        clear latER60 longER60 lat ME70 longME70
    end
end 
