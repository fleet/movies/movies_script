%  clear all;

tree=xml_read('O:\GUERLEDAN_2019\Calibration bilan\TrList_calibration2019.xml')
%  tree=xml_read('C:\ProgramData\Simrad\EK80\ConfigSettings\TrList_calibration.xml')

transd=4;
% transd=5;
for j=1:length(tree.TransducerData.Transducer)
for i=1:length(tree.TransducerData.Transducer(j).FrequencyPar)
    Freq(j,i)=tree.TransducerData.Transducer(j).FrequencyPar(i).ATTRIBUTE.Frequency;
    Gain(j,i)=tree.TransducerData.Transducer(j).FrequencyPar(i).ATTRIBUTE.Gain;
    CalNo(j,i)=tree.TransducerData.Transducer(j).FrequencyPar(i).ATTRIBUTE.RefNo;
end
end

figure;
hold on;
for j=1:length(tree.TransducerData.Transducer)
    plot(Freq(j,:),Gain(j,:),'*');

end
    grid on;
    
tree1=xml_read('L:\ESTECH17\Calibration_EK80\EK80_FM_22mm_lest\CalibrationDataFile-D20170925-T144027_bille22mm_38kHz_FM_lest_v1-11-1_SansHits.xml');
tree2=xml_read('L:\ESTECH17\Calibration_EK80\EK80_FM_38mm_lest\CalibrationDataFile-D20170922-T164200_bille38mm_38kHz_FM_lest_v1-11-1_SansHits.xml');
tree3=xml_read('L:\ESTECH17\Calibration_EK80\EK80_FM_22mm_lest\CalibrationDataFile-D20170925-T143107_bille22mm_70kHz_FM_lest_v1-11-1_SansHits.xml');
tree4=xml_read('L:\ESTECH17\Calibration_EK80\EK80_FM_38mm_lest\CalibrationDataFile-D20170922-T185630_bille38mmlest_FM_70kHz_v1-11-1_SansHits.xml');
tree5=xml_read('L:\ESTECH17\Calibration_EK80\EK80_FM_22mm_lest\CalibrationDataFile-D20170925-T142234_bille22mm_120kHz_FM_lest_v1-11-1_SansHits.xml');
tree6=xml_read('L:\ESTECH17\Calibration_EK80\EK80_FM_38mm_lest\CalibrationDataFile-D20170922-T185630_bille38mmlest_FM_120kHz_v1-11-1_SansHits.xml');
tree7=xml_read('L:\ESTECH17\Calibration_EK80\EK80_FM_22mm_lest\CalibrationDataFile-D20170925-T141058_bille22mm_200kHz_FM_lest_v1-11-1_SansHits.xml');
tree8=xml_read('L:\ESTECH17\Calibration_EK80\EK80_FM_38mm_lest\CalibrationDataFile-D20170922-T185630_bille38mmlest_FM_200kHz_v1-11-1_SansHits.xml');
tree9=xml_read('L:\ESTECH17\Calibration_EK80\EK80_FM_22mm_lest\CalibrationDataFile-D20170925-T140055_bille22mm_333kHz_FM_lest_v1-11-1_SansHits.xml');
tree10=xml_read('L:\ESTECH17\Calibration_EK80\EK80_FM_38mm_lest\CalibrationDataFile-D20170922-T185630_bille38mmlest_FM_333kHz_v1-11-1_SansHits.xml');

tree15=xml_read('L:\ESTECH17\Calibration_EK80\EK80_FM_25mm_percee_lest\CalibrationDataFile-D20170926-T152448_bille25mm_38kHz_FM_lest_v1-11-1_15cm.xml');
tree16=xml_read('L:\ESTECH17\Calibration_EK80\EK80_FM_25mm_percee_lest\CalibrationDataFile-D20170926-T153135_bille25mm_70kHz_FM_lest_v1-11-1_15cm.xml');
tree17=xml_read('L:\ESTECH17\Calibration_EK80\EK80_FM_25mm_percee_lest\CalibrationDataFile-D20170926-T153614_bille25mm_120kHz_FM_lest_v1-11-1_15cm.xml');
% tree18=xml_read('L:\ESTECH17\Calibration_EK80\EK80_FM_25mm_percee_lest\CalibrationDataFile-D20170926-T154154_bille25mm_200kHz_FM_lest.xml');
tree19=xml_read('L:\ESTECH17\Calibration_EK80\EK80_FM_25mm_percee_lest\CalibrationDataFile-D20170923-T092540_bille25mm_333kHz_FM_lest_v1-11-1_15cm.xml');

tree20=xml_read('K:\Etalonnage\EK80\calibration cuve 2018-2019\Calibration cuve2018_70_333khz\CalibrationDataFile-D20181207-T123600_FM_44-90_512us_SansHits.xml');
tree21=xml_read('L:\MARHALIO\Etalonnage\SaintCast\CalibrationDataFile-D20190912-T112812-70kHz_FM512_25mm_SansHits.xml');
tree22=xml_read('O:\GUERLEDAN_11102018\CAlibration\Calibration70_v4_4db_reprocessed_SansHits.xml');

set(0,'DefaultAxesFontName', 'Arial')
set(0,'DefaultAxesFontSize', 14)
% Change default text fonts.
set(0,'DefaultTextFontname', 'Arial')
set(0,'DefaultTextFontSize', 16)
% Change default line width.
set(0,'defaultlinelinewidth', 1)



% figure;
% 
% hold on;
% plot(str2num(tree2.Calibration.CalibrationResults.Frequency)/1000,str2num(tree2.Calibration.CalibrationResults.TsRmsError),'ro')
% plot(str2num(tree3.Calibration.CalibrationResults.Frequency)/1000,str2num(tree3.Calibration.CalibrationResults.TsRmsError),'g+')
% plot(str2num(tree4.Calibration.CalibrationResults.Frequency)/1000,str2num(tree4.Calibration.CalibrationResults.TsRmsError),'b*')
% plot(str2num(tree5.Calibration.CalibrationResults.Frequency)/1000,str2num(tree5.Calibration.CalibrationResults.TsRmsError),'c*')
% % plot(str2num(tree6.Calibration.CalibrationResults.Frequency)/1000,str2num(tree6.Calibration.CalibrationResults.Gain),'m*')
% % scatter(str2num(tree5.Calibration.CalibrationResults.Frequency)/1000,str2num(tree5.Calibration.CalibrationResults.Gain))
% % colormap(hsv(length(unique(CalNo))))
% grid on;
% xlabel('Frequency [kHz]'); ylabel('RMS TS [dB]');
% legend('PELGAS17 cor','ESTECH17 bille 22mm','ESTECH17 bille 38mm','ESTECH17 bille 25mm');

% figure;
% 
% hold on;
% plot(str2num(tree2.Calibration.CalibrationResults.Frequency)/1000,str2num(tree2.Calibration.CalibrationResults.Gain),'ro')
% plot(str2num(tree3.Calibration.CalibrationResults.Frequency)/1000,str2num(tree3.Calibration.CalibrationResults.Gain),'g+')
% plot(str2num(tree4.Calibration.CalibrationResults.Frequency)/1000,str2num(tree4.Calibration.CalibrationResults.Gain),'b*')
% % plot(str2num(tree5.Calibration.CalibrationResults.Frequency)/1000,str2num(tree5.Calibration.CalibrationResults.Gain),'c*')
% % plot(str2num(tree6.Calibration.CalibrationResults.Frequency)/1000,str2num(tree6.Calibration.CalibrationResults.Gain),'m*')
% % scatter(str2num(tree5.Calibration.CalibrationResults.Frequency)/1000,str2num(tree5.Calibration.CalibrationResults.Gain))
% % colormap(hsv(length(unique(CalNo))))
% grid on;
% xlabel('Frequency [kHz]'); ylabel('RMS TS [dB]');
% legend('PELGAS17 cor','ESTECH17 bille 22mm','ESTECH17 bille 38mm','ESTECH17 bille 25mm');

freq38khz_22mm=str2num(tree1.Calibration.CalibrationResults.Frequency)/1000;
Gain38khz_22mm=str2num(tree1.Calibration.CalibrationResults.Gain);
GainStd38khz_22mm=str2num(tree1.Calibration.CalibrationResults.TsRmsError);

freq38khz_38mm=str2num(tree2.Calibration.CalibrationResults.Frequency)/1000;
Gain38khz_38mm=str2num(tree2.Calibration.CalibrationResults.Gain);
GainStd38khz_38mm=str2num(tree2.Calibration.CalibrationResults.TsRmsError);

freq70khz_22mm=str2num(tree3.Calibration.CalibrationResults.Frequency)/1000;
Gain70khz_22mm=str2num(tree3.Calibration.CalibrationResults.Gain);
GainStd70khz_22mm=str2num(tree3.Calibration.CalibrationResults.TsRmsError);

freq70khz_38mm=str2num(tree4.Calibration.CalibrationResults.Frequency)/1000;
Gain70khz_38mm=str2num(tree4.Calibration.CalibrationResults.Gain);
GainStd70khz_38mm=str2num(tree4.Calibration.CalibrationResults.TsRmsError);

freq120khz_22mm=str2num(tree5.Calibration.CalibrationResults.Frequency)/1000;
Gain120khz_22mm=str2num(tree5.Calibration.CalibrationResults.Gain);
GainStd120khz_22mm=str2num(tree5.Calibration.CalibrationResults.TsRmsError);

freq120khz_38mm=str2num(tree6.Calibration.CalibrationResults.Frequency)/1000;
Gain120khz_38mm=str2num(tree6.Calibration.CalibrationResults.Gain);
GainStd120khz_38mm=str2num(tree6.Calibration.CalibrationResults.TsRmsError);

freq200khz_22mm=str2num(tree7.Calibration.CalibrationResults.Frequency)/1000;
Gain200khz_22mm=str2num(tree7.Calibration.CalibrationResults.Gain);
GainStd200khz_22mm=str2num(tree7.Calibration.CalibrationResults.TsRmsError);
freq=str2num(tree7.Calibration.CalibrationResults.Frequency);
TSnew=calcule_TS_Sphere_function(freq/1000,6853,4171,1508,14.56,22);
freqRef=str2num(tree7.Calibration.TargetReference.Frequency)';
TSref=str2num(tree7.Calibration.TargetReference.Response)';
TSold=[];
for i=1:length(freq)
    ind=find(freqRef==freq(i));
    TSold(i)=TSref(ind);
end
corTS200kHz_22mm=TSold-TSnew;

freq200khz_38mm=str2num(tree8.Calibration.CalibrationResults.Frequency)/1000;
Gain200khz_38mm=str2num(tree8.Calibration.CalibrationResults.Gain);
GainStd200khz_38mm=str2num(tree8.Calibration.CalibrationResults.TsRmsError);
freq=str2num(tree8.Calibration.CalibrationResults.Frequency);
TSnew=calcule_TS_Sphere_function(freq/1000,6853,4171,1508,14.56,38.1);
freqRef=str2num(tree8.Calibration.TargetReference.Frequency)';
TSref=str2num(tree8.Calibration.TargetReference.Response)';
TSold=[];
for i=1:length(freq)
    ind=find(freqRef==freq(i));
    TSold(i)=TSref(ind);
end
corTS200kHz_38mm=TSold-TSnew;

freq333khz_22mm=str2num(tree9.Calibration.CalibrationResults.Frequency)/1000;
Gain333khz_22mm=str2num(tree9.Calibration.CalibrationResults.Gain);
GainStd333khz_22mm=str2num(tree9.Calibration.CalibrationResults.TsRmsError);
freq=str2num(tree9.Calibration.CalibrationResults.Frequency);
TSnew=calcule_TS_Sphere_function(freq/1000,6853,4171,1508,14.56,22);
freqRef=str2num(tree9.Calibration.TargetReference.Frequency)';
TSref=str2num(tree9.Calibration.TargetReference.Response)';
TSold=[];
for i=1:length(freq)
    ind=find(freqRef==freq(i));
    TSold(i)=TSref(ind);
end
corTS333kHz_22mm=TSold-TSnew;

freq333khz_38mm=str2num(tree10.Calibration.CalibrationResults.Frequency)/1000;
Gain333khz_38mm=str2num(tree10.Calibration.CalibrationResults.Gain);
GainStd333khz_38mm=str2num(tree10.Calibration.CalibrationResults.TsRmsError);

freq38khz_25mm=str2num(tree15.Calibration.CalibrationResults.Frequency)/1000;
Gain38khz_25mm=str2num(tree15.Calibration.CalibrationResults.Gain);
GainStd38khz_25mm=str2num(tree15.Calibration.CalibrationResults.TsRmsError);

freq70khz_25mm=str2num(tree20.Calibration.CalibrationResults.Frequency)/1000;
Gain70khz_25mm=str2num(tree20.Calibration.CalibrationResults.Gain);
GainStd70khz_25mm=str2num(tree20.Calibration.CalibrationResults.TsRmsError);

freq120khz_25mm=str2num(tree17.Calibration.CalibrationResults.Frequency)/1000;
Gain120khz_25mm=str2num(tree17.Calibration.CalibrationResults.Gain);
GainStd120khz_25mm=str2num(tree17.Calibration.CalibrationResults.TsRmsError);

freq333khz_25mm=str2num(tree19.Calibration.CalibrationResults.Frequency)/1000;
Gain333khz_25mm=str2num(tree19.Calibration.CalibrationResults.Gain);
GainStd333khz_25mm=str2num(tree19.Calibration.CalibrationResults.TsRmsError);

%comparaison imp�dances 
nbtransducer=6;
for j=1:nbtransducer
    switch j
        case 1, test=[1:4];   comment='EK80-18kHz'; frequence = 18000; commentb='EK80 Thalassa, quai Concarneau, 6/09/2017';
        case 2, test=[5:8];   comment='EK80-38kHz';frequence = 38000;commentb='EK80 Thalassa, quai Concarneau, 6/09/2017';
        case 3, test=[9:12];  comment='EK80-70kHz';frequence = 70000;commentb='EK80 Thalassa, quai Concarneau, 6/09/2017';
        case 4, test=[13:16]; comment='EK80-120kHz';frequence = 120000;commentb='EK80 Thalassa, quai Concarneau, 6/09/2017';
        case 5, test=[17:20]; comment='EK80-200kHz';frequence = 200000;commentb='EK80 Thalassa, quai Concarneau, 6/09/2017';
        case 6, test=[21:24]; comment='EK80-333kHz';frequence = 333000;commentb='EK80 Thalassa, quai Concarneau, 6/09/2017';
    end
    
    
    for i=1:length(test)
        itest=test(i);
        param=choix_im(itest);
        fname=[param.directory param.filename];
        fid=fopen(fname,'rb');
        for n=1:13, tline = fgetl(fid); end
        tmp =fscanf(fid,'%u\t �\t %u\t Hz\n');
        fmin=tmp(1); fmax=tmp(2); clear tmp;
        tline = fgetl(fid);
        tmp =fscanf(fid,'%u\t Hz\n'); deltaf=tmp(1);clear tmp;
        tline = fgetl(fid);
        tmp =fscanf(fid,'%u\t Volt\n'); tension=tmp(1);clear tmp;
        for n=1:4
            tline = fgetl(fid);
        end
        [data n] = fscanf(fid,'%f\t %f\t %f\t %f\t %f\t %f\t %f\t %f\t %f\t %f\t %f\t %f\t %f\t %f\t %f\t %f');
        fclose(fid);
        data=reshape(data,15,round(n/15));
        switch j
            case 2
                Freq38    = data(1,:)/1000; % (Hz)
                Z38q(i,:)       = data(2,:); % (Ohm)
            case 3
                Freq70    = data(1,:)/1000; % (Hz)
                Z70q(i,:)     = data(2,:); % (Ohm)
            case 4
                Freq120    = data(1,:)/1000; % (Hz)
                Z120q(i,:)     = data(2,:); % (Ohm)
            case 5
                Freq200    = data(1,:)/1000; % (Hz)
                Z200q(i,:)     = data(2,:); % (Ohm)
            case 6
                Freq333    = data(1,:)/1000; % (Hz)
                Z333q(i,:)     = data(2,:); % (Ohm)
        end
        
    end
end


ind38=Freq38>freq38khz_22mm(1) & Freq38<freq38khz_22mm(end);
ind70=Freq70>freq70khz_22mm(1) & Freq70<freq70khz_22mm(end);
ind120=Freq120>freq120khz_22mm(1) & Freq120<freq120khz_22mm(end);
ind200=Freq200>freq200khz_22mm(1) & Freq200<freq200khz_22mm(end);
ind333=Freq333>freq333khz_22mm(1) & Freq333<freq333khz_22mm(end);

Z38=mean(Z38q(:,ind38),1);
Z70=mean(Z70q(:,ind70),1);
Z120=mean(Z120q(:,ind120),1);
Z200=mean(Z200q(:,ind200),1);
Z333=mean(Z333q(:,ind333),1);

Freq38=Freq38(ind38);
Freq70=Freq70(ind70);
Freq120=Freq120(ind120);
Freq200=Freq200(ind200);
Freq333=Freq333(ind333);

Z38comp=10*log10(Z38)-20.7-20*log10(38./Freq38)+28;
Z70comp=10*log10(Z70)-20.7-20*log10(70./Freq70)+28;
Z120comp=10*log10(Z120)-20.7-20*log10(120./Freq120)+28;
Z200comp=10*log10(Z200)-20.7-20*log10(200./Freq200)+28;
Z333comp=10*log10(Z333)-20.7-20*log10(333./Freq333)+28;

Z38comp=10*log10(Z38)-20.7+28;
Z70comp=10*log10(Z70)-20.7+28;
Z120comp=10*log10(Z120)-20.7+28;
Z200comp=10*log10(Z200)-20.7+28;
Z333comp=10*log10(Z333)-20.7+28;


figure;hold on;
H(1)=shadedErrorBar(freq38khz_22mm,Gain38khz_22mm,GainStd38khz_22mm,'lineprops','g');
shadedErrorBar(freq70khz_22mm,Gain70khz_22mm,GainStd70khz_22mm,'lineprops','g');
shadedErrorBar(freq120khz_22mm,Gain120khz_22mm,GainStd120khz_22mm,'lineprops','g');
shadedErrorBar(freq200khz_22mm,Gain200khz_22mm-(corTS200kHz_22mm/2)',GainStd200khz_22mm,'lineprops','g');
shadedErrorBar(freq333khz_22mm,Gain333khz_22mm-(corTS333kHz_22mm/2)',GainStd333khz_22mm,'lineprops','g');

H(2)=shadedErrorBar(freq38khz_38mm,Gain38khz_38mm,GainStd38khz_38mm,'lineprops','r');
shadedErrorBar(freq70khz_38mm,Gain70khz_38mm,GainStd70khz_38mm,'lineprops','r');
shadedErrorBar(freq120khz_38mm,Gain120khz_38mm,GainStd120khz_38mm,'lineprops','r');
shadedErrorBar(freq200khz_38mm,Gain200khz_38mm-(corTS200kHz_38mm/2)',GainStd200khz_38mm,'lineprops','r');
shadedErrorBar(freq333khz_38mm,Gain333khz_38mm,GainStd333khz_38mm,'lineprops','r');


H(3)=shadedErrorBar(freq38khz_25mm,Gain38khz_25mm,GainStd38khz_25mm,'lineprops','c');
shadedErrorBar(freq70khz_25mm,Gain70khz_25mm,GainStd70khz_25mm,'lineprops','c');
shadedErrorBar(freq120khz_25mm,Gain120khz_25mm,GainStd120khz_25mm,'lineprops','c');
% shadedErrorBar(freq200khz_25mm,Gain200khz_25mm,GainStd200khz_25mm,'lineprops','c');
shadedErrorBar(freq333khz_25mm,Gain333khz_25mm,GainStd333khz_25mm,'lineprops','c');


plot(Freq38,Z38comp,'-k','LineW',3);
plot(Freq70,Z70comp,'-k','LineW',3);
plot(Freq120,Z120comp,'-k','LineW',3);
plot(Freq200,Z200comp,'-k','LineW',3);
plot(Freq333,Z333comp,'-k','LineW',3);

grid on;
xlabel('Frequency [kHz]'); ylabel('Gains [dB]');

legend([H(1).mainLine, H(2).mainLine,H(3).mainLine], ...
    'ESTECH17 bille 22mm panier et lest','ESTECH17 bille 38mm panier et lest','ESTECH17 bille 25mm perc�e et lest', ...
     '20*log10(Z)+DI',...
    'Location', 'Northwest');