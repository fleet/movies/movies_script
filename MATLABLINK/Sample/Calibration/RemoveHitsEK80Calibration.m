%% list of calibration files

filelist=[];
% filelist{end+1}='L:\PHOENIX17\Calibration\CalibrationDataFile-D20180528-T1339.xml';
% filelist{end+1}='L:\PHOENIX17\Calibration\CalibrationDataFile-D20180528-T1351.xml';
% filelist{end+1}='L:\PHOENIX17\Calibration\CalibrationDataFile-D20180528-T1347.xml';
% filelist{end+1}='L:\PHOENIX17\Calibration\CalibrationDataFile-D20180529-T0723.xml';
% filelist{end+1}='L:\PHOENIX17\Calibration\CalibrationDataFile-D20180529-T0731.xml';
% filelist{end+1}='L:\PHOENIX17\Calibration\CalibrationDataFile-D20180529-T0707.xml';
% filelist{end+1}='L:\PHOENIX17\Calibration\CalibrationDataFile-D20180529-T0814.xml';
% filelist{end+1}='L:\PHOENIX17\Calibration\CalibrationDataFile-D20180529-T0817.xml';
% filelist{end+1}='L:\PHOENIX17\Calibration\CalibrationDataFile-D20180529-T0820.xml';
% filelist{end+1}='L:\PHOENIX17\Calibration\CalibrationDataFile-D20180529-T0822.xml';
% filelist{end+1}='L:\PHOENIX17\Calibration\CalibrationDataFile-D20180529-T0804.xml';
% filelist{end+1}='L:\PHOENIX17\Calibration\CalibrationDataFile-D20180529-T0807.xml';
% filelist{end+1}='C:\Users\lberger\Desktop\Calibration\Calibration\Thalia70-7C-2ms-FM-CalibrationDataFile-D20181001-T1630.xml';
% filelist{end+1}='C:\Users\lberger\Desktop\Calibration\Calibration\Thalia2007C-2ms-FM-CalibrationDataFile-D20180930-T131541.xml';
filelist{end+1}='L:\MARHALIO\Etalonnage\SaintCast\CalibrationDataFile-D20190912-T112812-70kHz_FM512_25mm.xml';
% 
% filelist{end+1}='F:\Thalassa2017\EK80\Calibration\EK80_FM_38mm_lest\CalibrationDataFile-D20170922-T164200_bille38mm_38kHz_FM_lest_v1-11-1.xml';
% %filelist{end+1}='F:\Thalassa2017\EK80\Calibration\EK80_FM_38mm_lest\CalibrationDataFile-D20170922-T164200_bille38mm_38kHz_FM_lest_v1-11-1_30cm.xml';
% filelist{end+1}='F:\Thalassa2017\EK80\Calibration\EK80_FM_38mm_lest\CalibrationDataFile-D20170922-T185630_bille38mmlest_FM_70kHz_v1-11-1.xml';
% %filelist{end+1}='F:\Thalassa2017\EK80\Calibration\EK80_FM_38mm_lest\CalibrationDataFile-D20170922-T185630_bille38mmlest_FM_70kHz_v1-11-1_30cm.xml';
% filelist{end+1}='F:\Thalassa2017\EK80\Calibration\EK80_FM_38mm_lest\CalibrationDataFile-D20170922-T185630_bille38mmlest_FM_120kHz_v1-11-1.xml';
% %filelist{end+1}='F:\Thalassa2017\EK80\Calibration\EK80_FM_38mm_lest\CalibrationDataFile-D20170922-T185630_bille38mmlest_FM_120kHz_v1-11-1_30cm.xml';
% filelist{end+1}='F:\Thalassa2017\EK80\Calibration\EK80_FM_38mm_lest\CalibrationDataFile-D20170922-T185630_bille38mmlest_FM_200kHz_v1-11-1.xml';
% %filelist{end+1}='F:\Thalassa2017\EK80\Calibration\EK80_FM_38mm_lest\CalibrationDataFile-D20170922-T185630_bille38mmlest_FM_200kHz_v1-11-1_30cm.xml';
% filelist{end+1}='F:\Thalassa2017\EK80\Calibration\EK80_FM_38mm_lest\CalibrationDataFile-D20170922-T185630_bille38mmlest_FM_333kHz_v1-11-1.xml';
% %filelist{end+1}='F:\Thalassa2017\EK80\Calibration\EK80_FM_38mm_lest\CalibrationDataFile-D20170922-T185630_bille38mmlest_FM_333kHz_v1-11-1_30cm.xml';

%% xml reading option 2 (remove target hits)

%record file without target hits
nfiles=size(filelist,2);
for ific=1:nfiles
    file_in_name=filelist{ific};
    fid=fopen(file_in_name,'r');

    file_out_name=[file_in_name(1:end-4) '_SansHits.xml'];
    fid_out=fopen(file_out_name,'w');

    w=fgetl(fid);
    hit_list=false;
    while w~=-1 
        if ~hit_list
            fprintf(fid_out, '%s', w);
            fprintf(fid_out, '\r\n');
        end
        w=fgetl(fid);
        if ~isempty(strfind(w,'TargetHits')) hit_list=~hit_list; w=fgetl(fid);end
    end
    fclose(fid);
    fclose(fid_out);
end

%% read new xml
freq=[];
Gain=[];
GainStd=[];
for ific=1:nfiles
    file_name=filelist{ific};
    tree=xml_read([file_name(1:end-4) '_SansHits.xml']);
    freq{end+1}=str2num(tree.Calibration.CalibrationResults.Frequency)/1000;
    Gain{end+1}=str2num(tree.Calibration.CalibrationResults.Gain);
    GainStd{end+1}=str2num(tree.Calibration.CalibrationResults.TsRmsError);
end

%% plot

set(0,'DefaultAxesFontName', 'Arial')
set(0,'DefaultAxesFontSize', 14)
% Change default text fonts.
set(0,'DefaultTextFontname', 'Arial')
set(0,'DefaultTextFontSize', 16)
% Change default line width.
set(0,'defaultlinelinewidth', 1)


% figure;hold on;
for i=1:nfiles
    errorbar(freq{i},Gain{i},GainStd{i});
end

grid on;
xlabel('Frequency [kHz]'); ylabel('Gains [dB]');


% legend('22mm 2805','22mm 2805','22mm 2805','29mm 2905','29mm 2905','29mm 2905','22mm 2905','22mm 2905','22mm 2905','38mm 2905','38mm 2905','22mm 2905 120khZ seul','22mm 2905 bis');
