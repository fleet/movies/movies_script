%ATTENTION � la correction des param�tres suivants

% Basic data, physical properties of the sphere **************************
cc=[6853,4171];				% Sound speeds in sphere (m/s)  %%%%%point � actualiser suivant m�tal ([6853,4171] Tg)
c= 1465.9;					% Sound speed in water (m/s)   %%%%%point � actualiser
rho=14950/1027;				% Sphere/water density ratio    %%%%%point � actualiser aussi suivant salinit� (et m�tal, 14900 Tg), mais joue peu pour salinit�
a=25/2;					% Sphere radius in mm

BW=8;

%%%%%%%%%%%  calcule les TS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


fid=1;					% Default file identifier (screen)

% Plot form function against frequency ***********************************
% Frequency range (kHz) for each configuration
freq = (45:1:90);

%freq = (60:0.1:130);
q=2*pi*freq*a/c;			% ka range
f=form(rho,cc,c,q,a);
%plot(freq,f(:,1));
%xlabel('Frequency(kHz)'); ylabel('Mod F squared');
%title('Form function of tungsten carbide spheres')

% Print values of �F�^2 averaged over a bandwidth BW**********************
% BW=3.545; df=0.1;		% Centre frequency, bandwidth,increment (kHz)
df=0.1;		% bandwidth,increment (kHz)
TS=[];
for num_freq = 1:length(freq)
	f00=freq(num_freq);
	f0=(f00-BW/2):df:(f00+BW/2);	% Frequency range (kHz)
	q=2*pi*f0*a/c;			% ka range
	x=form(rho,cc,c,q,a);
	f=zeros(1,3);f(1)=f00;
	f(3)=mean(x(:,1));
	f(2)=10*log10((a/2000)^2*f(3));
	if num_freq == 1
		fprintf(fid,'Bandwidth (kHz) = %4.1f\n',BW);
		fprintf(fid,'c1(m/s)    c2(m/s)   c(m/s)      Rho      a(mm)\n');
		fmt='%6.1f     %6.1f    %6.1f     %6.3f    %5.2f\n';
		fprintf(fid,fmt,[cc c rho a]');
		fprintf(fid,'F(kHz)    TS(dB)     ModF^2\n');
	end
	fprintf(fid,'%6.1f     %6.2f    %6.3f\n',f');
	TS(num_freq) = f(2);
end
%fclose(fid);
TSBW=TS;

% ************* TS sans bande **********************
% BW=3.545; df=0.1;		% Centre frequency, bandwidth,increment (kHz)
B0=0;
df=0.1;		% bandwidth,increment (kHz)
for num_freq = 1:length(freq)
	f00=freq(num_freq);
	f0=(f00-B0/2):df:(f00+B0/2);	% Frequency range (kHz)
	q=2*pi*f0*a/c;			% ka range
	x=form(rho,cc,c,q,a);
	f=zeros(1,3);f(1)=f00;
	f(3)=mean(x(:,1));
	f(2)=10*log10((a/2000)^2*f(3));
	if num_freq == 1
		fprintf(fid,'Bandwidth (kHz) = %4.1f\n',BW);
		fprintf(fid,'c1(m/s)    c2(m/s)   c(m/s)      Rho      a(mm)\n');
		fmt='%6.1f     %6.1f    %6.1f     %6.3f    %5.2f\n';
		fprintf(fid,fmt,[cc c rho a]');
		fprintf(fid,'F(kHz)    TS(dB)     ModF^2\n');
	end
	fprintf(fid,'%6.1f     %6.2f    %6.3f\n',f');
	TS(num_freq) = f(2);
end
%fclose(fid);
TS0=TS;

% figure(3);plot(freq,TSBW,'LineW',2);grid on
figure;hold on;plot(freq,TSBW,'r','LineW',2);hold off
% xlabel('frequence en kHz');ylabel('TS');
% legend(['TS dans une bande ' num2str(BW) ' kHz'],'TS');
% title(['c�l�rit� de ' num2str(c) ' m/s, bille de ' num2str(2*a) ' mm'])
xlabel('frequency in kHz');ylabel('TS');
% legend(['mean TS in ' num2str(BW) ' kHz Bandwidth'],'TS');
title(['sound speed in water: ' num2str(c) ' m/s, sound speed in sphere: [' num2str(cc(1)),';',  num2str(cc(2)) '] m/s, Sphere/water density ratio:  ' num2str(rho) ])
