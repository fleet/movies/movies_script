function TS=calcule_TS_Sphere_function(freq,LongitudinalSphereSoundSpeed,TransversalSoundSphereSpeed,WaterSoundSpeed,Density,Diameter)



% Basic data, physical properties of the sphere **************************
cc=[LongitudinalSphereSoundSpeed,TransversalSoundSphereSpeed];				% Sound speeds in sphere (m/s)  %%%%%point � actualiser suivant m�tal ([6853,4171] Tg)
c= WaterSoundSpeed;					% Sound speed in water (m/s)   %%%%%point � actualiser
rho=Density;				% Sphere/water density ratio    %%%%%point � actualiser aussi suivant salinit� (et m�tal, 14900 Tg), mais joue peu pour salinit�
a=Diameter/2;					% Sphere radius in mm


%%%%%%%%%%%  calcule les TS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

q=2*pi*freq*a/c;			% ka range
f=form(rho,cc,c,q,a);

for num_freq = 1:length(freq)
	f00=freq(num_freq);
	q=2*pi*f00*a/c;			% ka range
	x=form(rho,cc,c,q,a);
	f=zeros(1,3);f(1)=f00;
	f(3)=mean(x(:,1));
	f(2)=10*log10((a/2000)^2*f(3));
	TS(num_freq) = f(2);
end


