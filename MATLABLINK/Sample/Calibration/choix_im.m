function data=choix_im(itest)
%function data=choix_im(itest)

data.directory = 'L:\ESTECH17\20170906_Thalassa_quai_impedances\';
switch itest
    case 1, data.filename = 'es18_q1.log'; data.comment = 'Thalassa - EK80 18kHz q1 - 6/09/2017';
    case 2, data.filename = 'es18_q2.log'; data.comment = 'Thalassa - EK80 18kHz q2 - 6/09/2017';
    case 3, data.filename = 'es18_q3.log'; data.comment = 'Thalassa - EK80 18kHz q3 - 6/09/2017';
    case 4, data.filename = 'es18_q4.log'; data.comment = 'Thalassa - EK80 18kHz q4 - 6/09/2017';

    case 5, data.filename = 'es38_q1.log'; data.comment = 'Thalassa - EK80 38kHz q1 - 6/09/2017';
    case 6, data.filename = 'es38_q2.log'; data.comment = 'Thalassa - EK80 38kHz q2 - 6/09/2017';
    case 7, data.filename = 'es38_q3.log'; data.comment = 'Thalassa - EK80 38kHz q3 - 6/09/2017';
    case 8, data.filename = 'es38_q4.log'; data.comment = 'Thalassa - EK80 38kHz q4 - 6/09/2017';

    case 9, data.filename = 'es70_q1.log'; data.comment = 'Thalassa - EK80 70kHz q1 - 6/09/2017';
    case 10, data.filename = 'es70_q2.log'; data.comment = 'Thalassa - EK80 70kHz q2 - 6/09/2017';
    case 11, data.filename = 'es70_q3.log'; data.comment = 'Thalassa - EK80 70kHz q3 - 6/09/2017';
    case 12, data.filename = 'es70_q4.log'; data.comment = 'Thalassa - EK80 70kHz q4 - 6/09/2017';

    case 13, data.filename = 'es120_q1.log'; data.comment = 'Thalassa - EK80 120kHz q1 - 6/09/2017';
    case 14, data.filename = 'es120_q2.log'; data.comment = 'Thalassa - EK80 120kHz q2 - 6/09/2017';
    case 15, data.filename = 'es120_q3.log'; data.comment = 'Thalassa - EK80 120kHz q3 - 6/09/2017';
    case 16, data.filename = 'es120_q4.log'; data.comment = 'Thalassa - EK80 120kHz q4 - 6/09/2017';

    case 17, data.filename = 'es200_q1.log'; data.comment = 'Thalassa - EK80 200kHz q1 - 6/09/2017';
    case 18, data.filename = 'es200_q2.log'; data.comment = 'Thalassa - EK80 200kHz q2 - 6/09/2017';
    case 19, data.filename = 'es200_q3.log'; data.comment = 'Thalassa - EK80 200kHz q3 - 6/09/2017';
    case 20, data.filename = 'es200_q4.log'; data.comment = 'Thalassa - EK80 200kHz q4 - 6/09/2017';

    case 21, data.filename = 'es333_q1.log'; data.comment = 'EK60 70kHz AUV, q1, janvier 2014';
    case 22, data.filename = 'es333_q2.log'; data.comment = 'EK60 70kHz AUV, q2, janvier 2014';
    case 23, data.filename = 'es333_q3.log'; data.comment = 'EK60 70kHz AUV, q3, janvier 2014';
    case 24, data.filename = 'es333_q4.log'; data.comment = 'EK60 70kHz AUV, q4, janvier 2014';

    case 25, data.filename = 'sds_tx1.log'; data.comment = 'Thalassa - SDS TX1 - 6/09/2017';
    case 26, data.filename = 'sds_tx2.log'; data.comment = 'Thalassa - SDS TX2 - 6/09/2017';
    case 27, data.filename = 'sds_tx3.log'; data.comment = 'Thalassa - SDS TX3 - 6/09/2017';
    case 28, data.filename = 'sds_tx4.log'; data.comment = 'Thalassa - SDS TX4 - 6/09/2017';
    case 29, data.filename = 'sds_tx5.log'; data.comment = 'Thalassa - SDS TX5 - 6/09/2017';

    case 30, data.filename = 'sds_Thalassa_antenne_5TX.log'; data.comment = 'Thalassa - SDS antenne 5 TX // - 2/10/2017';
end    
        
