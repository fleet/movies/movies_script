function compare_im(iliste)
%function compare_im(iliste)

saveimages=0;
ColorListe={'b','g','y','r','c','m','b--','c--','g--','y--','r--','m--'};

switch iliste
    case 1, test=[1:4];   comment='EK80-18kHz'; frequence = 18000; commentb='EK80 Thalassa, quai Concarneau, 6/09/2017';
    case 2, test=[5:8];   comment='EK80-38kHz';frequence = 38000;commentb='EK80 Thalassa, quai Concarneau, 6/09/2017';
    case 3, test=[9:12];  comment='EK80-70kHz';frequence = 70000;commentb='EK80 Thalassa, quai Concarneau, 6/09/2017';
    case 4, test=[13:16]; comment='EK80-120kHz';frequence = 120000;commentb='EK80 Thalassa, quai Concarneau, 6/09/2017';
    case 5, test=[17:20]; comment='EK80-200kHz';frequence = 200000;commentb='EK80 Thalassa, quai Concarneau, 6/09/2017';
    case 6, test=[21:24]; comment='EK80-333kHz';frequence = 333000;commentb='EK80 Thalassa, quai Concarneau, 6/09/2017';
    case 7, test=[25:29]; comment='SDS';frequence = 3500;commentb='SDS Thalassa, quai Concarneau, 6/09/2017';
    case 8, test=30; comment='SDS-antenne';frequence = 3500;commentb='SDS Thalassa, essais mer, 2/10/2017';
end

admittance=zeros(4,2);

for i=1:length(test)
    itest=test(i);
    param=choix_im(itest);
    fname=[param.directory param.filename];
    fid=fopen(fname,'rb');
    for n=1:13, tline = fgetl(fid); end
    tmp =fscanf(fid,'%u\t �\t %u\t Hz\n');
    fmin=tmp(1); fmax=tmp(2); clear tmp;
    tline = fgetl(fid);
    tmp =fscanf(fid,'%u\t Hz\n'); deltaf=tmp(1);clear tmp;
    tline = fgetl(fid);
    tmp =fscanf(fid,'%u\t Volt\n'); tension=tmp(1);clear tmp;
    for n=1:4
        tline = fgetl(fid);
    end
    [data n] = fscanf(fid,'%f\t %f\t %f\t %f\t %f\t %f\t %f\t %f\t %f\t %f\t %f\t %f\t %f\t %f\t %f\t %f');
    fclose(fid);
    data=reshape(data,15,round(n/15));
    Freq    = data(1,:); % (Hz)
    Z       = data(2,:); % (Ohm)       
    Y       = data(3,:); % (Siemens)   
    Phase   = data(4,:); % (Deg)    
    Cs      = data(5,:); % (F)        
    Cp      = data(6,:); % (F)        
    D       = data(7,:); %            
    Ls      = data(8,:); % (H)        
    Lp      = data(9,:); % (H)        
    Q       = data(10,:); %            
    Rs      = data(11,:); % (Ohm)      
    G       = data(12,:); % (Siemens)   
    Rp      = data(13,:); % (Ohm)      
    X       = data(14,:); % (Ohm)       
    B       = data(15,:); % (Siemens)
    
    Cp      = -sign(Phase).*Cp;
    B       = -sign(Phase).*B;


    figure(1);
    subplot(2,1,1);hold on;plot(Freq/1000,Rp,char(ColorListe(i)));
    subplot(2,1,2);hold on;plot(Freq/1000,Cp*1e9,char(ColorListe(i)));
    figure(2);
    subplot(2,1,1);hold on;plot(Freq/1000,G*1e3,char(ColorListe(i)));
    subplot(2,1,2);hold on;plot(Freq/1000,B*1e3,char(ColorListe(i)));
    figure(3);
    hold on;plot(G*1e3,B*1e3,char(ColorListe(i)));
    if i==4
        for t=1:10:length(Freq)
            hold on; plot(G(t)*1e3,B(t)*1e3,'kx');
            text(G(t)*1e3,B(t)*1e3,sprintf('%8.6g kHz',Freq(t)/1000),'vertical','top','FontSize',8,'color','k');
        end
    end
    for t=1:length(Freq)
        if Freq(t)==frequence
            admittance(i,:)=[G(t) B(t)];
        end
    end
    figure(4);
%     hold on;plot(Freq/1000,Z,char(ColorListe(i)),'Linewidth',1.5);
    subplot(2,1,1);hold on;plot(Freq/1000,Z,char(ColorListe(i))); %set(gca,'ylim',[100 420]);
    subplot(2,1,2);hold on;plot(Freq/1000,Phase,char(ColorListe(i)));

    texte(i).legend=sprintf('%s',param.filename);
end

if iliste<7
    hleg1 = legend(texte(1).legend,texte(2).legend,texte(3).legend,texte(4).legend);
elseif iliste ==7, hleg1 = legend(texte(1).legend,texte(2).legend,texte(3).legend,texte(4).legend,texte(5).legend);
elseif iliste ==8, hleg1 = legend(texte(1).legend);
else hleg1 = legend(texte(1).legend,texte(2).legend,texte(3).legend,texte(4).legend,texte(5).legend,texte(6).legend);
end

figure(1);subplot(2,1,1);
set(gca,'xlim',[Freq(1) Freq(end)]/1000);
xlabel('fr�quence [kHz]','FontSize',12);ylabel('Rp [Ohm]','FontSize',12);
titre = sprintf('%s, mesure imp�dance : Rp',comment);
title(titre,'FontSize',12);grid on; box on;
set(hleg1,'Interpreter','none')

figure(1);subplot(2,1,2);
set(gca,'xlim',[Freq(1) Freq(end)]/1000);%,'ylim',[0 1.1e-7]);
xlabel('fr�quence [kHz]','FontSize',12);ylabel('Cp [nF]','FontSize',12);
titre = sprintf('%s, mesure imp�dance : Cp',comment);
title(titre,'FontSize',12);grid on; box on;

figure(2);subplot(2,1,1);
set(gca,'xlim',[Freq(1) Freq(end)]/1000);%,'ylim',[0 25000]);
xlabel('fr�quence [kHz]','FontSize',12);ylabel('conductance G [mS]','FontSize',12);
titre = sprintf('%s, mesure conductance : G',comment);
title(titre,'FontSize',12);grid on; box on;
set(hleg1,'Interpreter','none')

figure(2);subplot(2,1,2);
set(gca,'xlim',[Freq(1) Freq(end)]/1000);%,'ylim',[0 1.1e-7]);
xlabel('fr�quence [kHz]','FontSize',12);ylabel('susceptance B [mS]','FontSize',12);
titre = sprintf('%s, mesure susceptance : B',comment);
title(titre,'FontSize',12);grid on; box on;

figure(3);
xlabel('conductance G [mS]','FontSize',12);ylabel('susceptance B [mS]','FontSize',12);
titre = sprintf('%s, mesure admittance : G,B',comment);
title(titre,'FontSize',12);grid on; box on;
set(hleg1,'Interpreter','none')
hold on; plot(admittance(:,1)*1e3,admittance(:,2)*1e3,'k^', 'MarkerEdgeColor','k','MarkerFaceColor','k','MarkerSize',10);

figure(4);subplot(2,1,1);
set(gca,'xlim',[Freq(1) Freq(end)]/1000);%,'ylim',[50 1420]);
xlabel('fr�quence [kHz]','FontSize',12);ylabel('Z [Ohm]','FontSize',12);
titre = sprintf('%s, imp�dance : Z',comment);
title(titre,'FontSize',12);grid on; box on;
set(hleg1,'Interpreter','none','location','northwest')

figure(4);subplot(2,1,2);
set(gca,'xlim',[Freq(1) Freq(end)]/1000);%,'ylim',[-80 80]);
xlabel('fr�quence [kHz]','FontSize',12);ylabel('Phase [degr�s]','FontSize',12);
titre = sprintf('%s, imp�dance : Phase',comment);
title(titre,'FontSize',12);grid on; box on;

if saveimages==1
    for i=1:4,
        set(i,'PaperUnits','centimeter','PaperType','A4','PaperOrientation','portrait','PaperPosition',[0 0 29.7 21]); 
    end
    print('-f1','-dpng',sprintf('%s_RpCp.png',comment));
    print('-f2','-dpng',sprintf('%s_GB.png',comment));
    print('-f3','-dpng',sprintf('%s_BfG.png',comment));
    print('-f4','-dpng',sprintf('%s_Zb.png',comment));
end
