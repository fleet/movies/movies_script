
% create a ME70 calibration sphere xml file, for a given water sound speed and
% adjusted to beam configuration. 

% beam configuration is used:
% - to averaged TS in reception bandwidth (it could be discussed if 1/Teff
% would be better)
% - to compute TS for exact beam frequencies. Indeed TS in xml file is
% given every kHz, and ME70 calibration utility uses for each beam the
% sphere TS at closest kHz value. Here TS at closest frequency is then
% adapted.

clear
%% input/output parameters

% path and name of an initial ME70 sphere xml file, used to have correct xml format and frequency sampling (probably always 70:1:120)
% initial files are found in C:\ProgramData\Simrad\ME70\CalibrationSpheres
file_in_name='C:\ProgramData\Simrad\ME70\CalibrationSpheres\Tungsten25mm.xml';

% path and name of the new xml file to be created, to be placed
% afterwards in C:\ProgramData\Simrad\ME70\CalibrationSpheres
file_out_name='C:\ProgramData\Simrad\ME70\CalibrationSpheres\Tungsten22mm_21beams_1495ms.xml'; 

% string with sphere description, that will be presented in the calibration
% utility (File/Setup) when choosing the sphere. It is recommended to
% mention sphere material and diameter, beam configuration, and water sound speed 
% Use only common caracters 
% BEWARE: do not give identical sphere description strings for different sphere xml files
% in C:\ProgramData\Simrad\ME70\CalibrationSpheres. Calibration utility is
% then unable to list them
sphere_out_description='Tungsten 22mm 21 beams 1495ms';

% path and name of the ME70 beam configuration xml file (in C:\ProgramData\Simrad\ME70\BeamConfigurations)
beam_config_name='C:\ProgramData\Simrad\ME70\BeamConfigurations\Replay Hte_Res_Fan21_Ref0_Gpe4.xml'; %name of xml beam configuration file
% beam_config_name='C:\lebouffant\portable\Configs_ME70\Hte_Res_Fan21_Ref0_Gpe4.xml';

% Basic data, physical properties of the sphere **************************
c= 1495;					% Sound speed in water (m/s)   
a=22/2;					    % Sphere radius in mm
cc=[6853,4171];				% Sound speeds in sphere (m/s)  %%%%% according to sphere material ([6853,4171] for Tungsten)
rho=14900/1026;				% Sphere/water density ratio    %%%%% according to sphere material (and salinity but impact is weaker) (14900  for Tungsten)



%% frequencies reading

% read in the initial sphere xml file, the list freq_in of frequencies for which TS
% is given (does not depend on beam configuration)
% read also initial TS for potential comparison
[tree, RootName]=xml_read(file_in_name);
for i=1:length(tree.SphereData)
    freq_in(i)=tree.SphereData(i).Frequency;
    TS_in(i)=tree.SphereData(i).TargetStrength;
end


% read in the beam configuration file the reception bandwidth BW and the
% list of beam frequencies freq_son
[tree_beam, RootName_beam]=xml_read(beam_config_name);
name_conf=fieldnames(tree_beam);
ifield=1;
if (strcmp(name_conf{ifield},'ATTRIBUTE'))
    ifield=ifield+1;
end
beam_config=getfield(tree_beam,name_conf{ifield});
if (strcmp(beam_config.BeamArrays.parameter(6).ATTRIBUTE.name,'FrequencyRx')==1)
    freq_son=str2num(beam_config.BeamArrays.parameter(6).ATTRIBUTE.value)/1000;
else
    disp('aborting: update field selection to read Rx frequencies and replay matlab utility')
    break;
end
if (strcmp(beam_config.Configuration.Frequency.parameter(4).ATTRIBUTE.name,'MaxBeamBandWidthRx')==1)
    BW=beam_config.Configuration.Frequency.parameter(4).ATTRIBUTE.value/1000;
else
    disp('aborting: update field selection to read Rx bandwith and replay matlab utility')
    break;
end
    
%% TS computation for each xml frequency, averaged over reception bandwidth

fid=1;					% Default file identifier (screen)

% form function against frequency ***********************************
% Frequency range (kHz) for each configuration
freq = freq_in;

q=2*pi*freq*a/c;			% ka range
f=form(rho,cc,c,q,a);
%plot(freq,f(:,1));
%xlabel('Frequency(kHz)'); ylabel('Mod F squared');
%title('Form function of tungsten carbide spheres')

% Print values of �F�^2 averaged over a bandwidth BW**********************
df=0.1;		% bandwidth,increment (kHz)
for num_freq = 1:length(freq)
	f00=freq(num_freq);
	f0=(f00-BW/2):df:(f00+BW/2);	% Frequency range (kHz)
	q=2*pi*f0*a/c;			% ka range
	x=form(rho,cc,c,q,a);
	f=zeros(1,3);f(1)=f00;
	f(3)=mean(x(:,1));
	f(2)=10*log10((a/2000)^2*f(3));
	if num_freq == 1
		fprintf(fid,'Bandwidth (kHz) = %4.1f\n',BW);
		fprintf(fid,'c1(m/s)    c2(m/s)   c(m/s)      Rho      a(mm)\n');
		fmt='%6.1f     %6.1f    %6.1f     %6.3f    %5.2f\n';
		fprintf(fid,fmt,[cc c rho a]');
		fprintf(fid,'F(kHz)    TS(dB)     ModF^2\n');
	end
	fprintf(fid,'%6.1f     %6.2f    %6.3f\n',f');
	TS(num_freq) = f(2);
end
%fclose(fid);
TS0=TS;

%%  TS computation for each exact beam frequency, averaged over reception
%%  bandwidth

for ib=1:length(freq_son)
    f00=freq_son(ib);
    f0=(f00-BW/2):df:(f00+BW/2);	% Frequency range (kHz)
    q=2*pi*f0*a/c;			% ka range
    x=form(rho,cc,c,q,a);
    f=zeros(1,3);f(1)=f00;
    f(3)=mean(x(:,1));
    f(2)=10*log10((a/2000)^2*f(3));
    if ib == 1
        fprintf(fid,'Bandwidth (kHz) = %4.1f\n',BW);
        fprintf(fid,'c1(m/s)    c2(m/s)   c(m/s)      Rho      a(mm)\n');
        fmt='%6.1f     %6.1f    %6.1f     %6.3f    %5.2f\n';
        fprintf(fid,fmt,[cc c rho a]');
        fprintf(fid,'F(kHz)    TS(dB)     ModF^2\n');
    end
    fprintf(fid,'%6.1f     %6.2f    %6.3f\n',f');
    TS_exact(ib) = f(2);
    
    % affect TS to freq_in neighbouring frequencies
    % ifreq=max(find(freq<freq_son(ib)));
    % TS(ifreq)=TS_exact(ib);
    % TS(ifreq+1)=TS_exact(ib);
    
    % affect TS to freq_in closest frequency
    ifreq=find(abs(freq-freq_son(ib))==min(abs(freq-freq_son(ib))));
    TS(ifreq(1))=TS_exact(ib);
end

%% create new sphere xml file

for i=1:length(tree.SphereData)
    tree.SphereData(i).TargetStrength=TS(i);
    
    if(1) % impose 1 and 2 decimal precision
        freq_char=num2str(round(10*freq(i)));
        freq_char(end:end+1)=['.' freq_char(end)];
        tree.SphereData(i).Frequency=freq_char;
        
        ts_char=num2str(round(100*tree.SphereData(i).TargetStrength));
        ts_char(end-1:end+1)=['.' ts_char(end-1:end)];
        tree.SphereData(i).TargetStrength=ts_char;
    end
end

Pref.StructItem=0;
xml_write('temp_sphere.xml', tree,RootName,Pref);

% read xml format beginning in ititial file
fid=fopen(file_in_name);
u=fgetl(fid);
v=fgetl(fid);
fclose(fid);


fid=fopen(file_out_name,'w');
%reproduce correct xml format first line
fprintf(fid, '%s\n', u);
%update sphere name
ind_b=strfind(v,'"');
fprintf(fid, '%s\n', [v(1:ind_b(1)) sphere_out_description v(ind_b(2):end)]);
%copy file
fid2=fopen('temp_sphere.xml','r');
w=fgetl(fid2);
w=fgetl(fid2);
w=fgetl(fid2);
while w~=-1
    fprintf(fid, '%s\n', w);
    w=fgetl(fid2);
end
fclose(fid);
fclose(fid2);
delete 'temp_sphere.xml';

%% figures

figure(1);plot(freq,TS_in,'LineW',1);grid on
figure(1);hold on;plot(freq,TS0,'r','LineW',1);hold off
figure(1);hold on;plot(freq,TS,'m','LineW',2);hold off
figure(1);hold on;plot(freq_son,TS_exact,'r*','LineW',1);hold off
xlabel('frequency in kHz');ylabel('TS');
title(['sphere diameter ' num2str(2*a) 'mm']);
legend('TS spectrum in initial xml file',['TS spectrum at ' num2str(c) 'm/s, bandwidth ' num2str(BW) 'kHz'],'TS recorded in new file','beams frequencies');

