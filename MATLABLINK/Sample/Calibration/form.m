function F=form(rho,cc,c,ka,a)
%
% Form.m is a Matlab procedure which calculates F, the complex far-field
% backscattering form function of a homogeneous solid sphere in water.
% Version date 								5/4/98
% *********************** INPUT DATA *********************************
% rho		Ratio of sphere and water densities.
% cc gives the sound speeds in the sphere as a 2-element vector
% cc(1)	Longitudinal sound speed (c1)	}In any units provided they
% cc(2)	Transverse sound speed (c2)	}are all the same.
% c		Sound speed in water		}Normally use m/s
% a		(optional) The sphere radius in mm. If a is included,
%		the TS is returned in F(:,2), otherwise F(:,2)=0
% ka		Dimensionless frequency, equal to w*a/c where
%		w is the angular frequency and (a,c) are in compatible units
% If ka is a vector, the calculation is repeated for each element
% *********************** OUTPUT DATA ********************************
% F gives the form function as an n by 3 vector where n is the length of ka
% The results for each element of ka are placed in successive rows of F
% F(:,1)	Square of the magnitude ( or modulus squared, |F|^2 )
% F(:,2)	Target strength (dB), or 0 if no radius is specified
% F(:,3)	Real part of F
% F(:,4)	Imaginary part of F
% The acoustic cross section is Sigma = pi * a^2 * |F|^2
% ********************************************************************
nr=length(ka); F=zeros(nr,4);			% nr = number of calculations
Lmax=fix(max(ka))+20;				% The maximum partial wave order
alpha=2*rho*(cc(2)/c)^2;
beta=rho*(cc(1)/c)^2-alpha;
nn=1:Lmax+1;n0=2:Lmax+1;lh=[nn-0.5]';
LL=[0:Lmax]';						% Full order list
S=(fix(LL/2)==LL/2)-(fix((LL+1)/2)==(LL+1)/2);	% The series [+1 -1 +1 -1 etc.]
L=[1:Lmax]';L=[L L L];					% The order matrix
jh=zeros(Lmax+1,3);djh=jh;ddjh=jh;			% Zero matrices
yh=zeros(Lmax+1,1);dyh=yh;
a1=yh;a2=yh;b1=yh;b2=yh;x=yh;y=yh;z=yh;

for jj=1:nr
  q=ka(jj);
  q1=q*c/cc(1); q2=q*c/cc(2);				% Set constant parameters
  qq=[q1, q2, q];bfac=sqrt((qq*2/pi));
  Qm=ones(Lmax,1)*qq;Bf=ones(Lmax+1,1)*bfac;

% jh/yh are spherical Bessel functions of the first/second kinds
% starting at order L=0 (i.e. order is row number - 1).
  jh=[besselj(lh,q1),besselj(lh,q2),besselj(lh,q)]./Bf;
  yh=bessely(lh,q)./Bf(:,3);
  djh(1,:)= -jh(1,:)+cos(qq);		 		 % Zero order derivatives
  dyh(1)=-yh(1)+sin(q);					 % djh= x*jL'(x); dyh= x*yL'(x)
  ddjh(1,:)=-2*djh(1,:)-qq.*sin(qq);		 % ddjh = x^2*jL''(x)
  djh(n0,:)=-(L+1).*jh(n0,:)+Qm.*jh(n0-1,:); % Derivatives for orders 2 upwards
  dyh(n0)=-(L(:,1)+1).*yh(n0)+Qm(:,3).*yh(n0-1);
  ddjh(n0,:)=((L+1).*(L+2)-Qm.*Qm).*jh(n0,:)-2*Qm.*jh(n0-1,:);

  a2=(LL.*LL+LL-2).*jh(:,2)+ddjh(:,2);
  a1=2*LL.*(LL+1).*(djh(:,1)-jh(:,1));
  b2=a2.*(beta*q1^2*jh(:,1)-alpha*ddjh(:,1))-alpha*a1.*(jh(:,2)-djh(:,2));
  b1=q*(a2.*djh(:,1)-a1.*jh(:,2));
  x=-b2.*djh(:,3)/q+b1.*jh(:,3); y=b2.*dyh/q-b1.*yh;
  z=S.*(2*LL+1).*x./(x.*x+y.*y); y=z.*y; x=z.*x;

  F(jj,3:4)=-(2/q)*sum([y,x]);			% The form function
  F(jj,1)=F(jj,3)^2+F(jj,4)^2;			% and its modulus squared
  if nargin==5; F(jj,2)=10*log10((a/2000)^2*F(jj,1)); end
end
