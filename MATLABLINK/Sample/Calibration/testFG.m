alibration sont disponibles au m�me endroit.
freq=[160:260];
depth=10;

for i=1:length(freq)
    alpha(i)=FG(12.6,35.4,freq(i),depth);
    alphareel(i)=FG(15,0,freq(i),depth);
end

erroralphamean=2*(alpha-alphareel)*depth*1e-3;
erroralphastd=2*alphareel*5/100*depth*1e-3;

figure;hold on;
H(1)=shadedErrorBar(freq,erroralphamean,erroralphastd,'lineprops','g');
grid on;
xlabel('Frequency [kHz]'); ylabel('Alpha error [dB]');