nbtransducer=6;
for j=1:nbtransducer
    switch j
        case 1, test=[1:4];   comment='EK80-18kHz'; frequence = 18000; commentb='EK80 Thalassa, quai Concarneau, 6/09/2017';
        case 2, test=[5:8];   comment='EK80-38kHz';frequence = 38000;commentb='EK80 Thalassa, quai Concarneau, 6/09/2017';
        case 3, test=[9:12];  comment='EK80-70kHz';frequence = 70000;commentb='EK80 Thalassa, quai Concarneau, 6/09/2017';
        case 4, test=[13:16]; comment='EK80-120kHz';frequence = 120000;commentb='EK80 Thalassa, quai Concarneau, 6/09/2017';
        case 5, test=[17:20]; comment='EK80-200kHz';frequence = 200000;commentb='EK80 Thalassa, quai Concarneau, 6/09/2017';
        case 6, test=[21:24]; comment='EK80-333kHz';frequence = 333000;commentb='EK80 Thalassa, quai Concarneau, 6/09/2017';
    end
    
    
    for i=1:length(test)
        itest=test(i);
        param=choix_im(itest);
        fname=[param.directory param.filename];
        fid=fopen(fname,'rb');
        for n=1:13, tline = fgetl(fid); end
        tmp =fscanf(fid,'%u\t �\t %u\t Hz\n');
        fmin=tmp(1); fmax=tmp(2); clear tmp;
        tline = fgetl(fid);
        tmp =fscanf(fid,'%u\t Hz\n'); deltaf=tmp(1);clear tmp;
        tline = fgetl(fid);
        tmp =fscanf(fid,'%u\t Volt\n'); tension=tmp(1);clear tmp;
        for n=1:4
            tline = fgetl(fid);
        end
        [data n] = fscanf(fid,'%f\t %f\t %f\t %f\t %f\t %f\t %f\t %f\t %f\t %f\t %f\t %f\t %f\t %f\t %f\t %f');
        fclose(fid);
        data=reshape(data,15,round(n/15));
        switch j
            case 2
                Freq38    = data(1,:); % (Hz)
                Z38(i,:)       = data(2,:); % (Ohm)
            case 3
                Freq70    = data(1,:); % (Hz)
                Z70(i,:)     = data(2,:); % (Ohm)
            case 4
                Freq120    = data(1,:); % (Hz)
                Z120(i,:)     = data(2,:); % (Ohm)
            case 5
                Freq200    = data(1,:); % (Hz)
                Z200(i,:)     = data(2,:); % (Ohm)
            case 6
                Freq333    = data(1,:); % (Hz)
                Z333(i,:)     = data(2,:); % (Ohm)
        end
        
    end
end