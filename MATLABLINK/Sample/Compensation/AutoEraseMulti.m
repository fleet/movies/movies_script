function AutoEraseMulti(AngleMaxDeg)
AngleRad=AngleMaxDeg*pi/180;
 SounderList= moGetSounderList();
    for numSounder=1:size(SounderList,2)
        sounder=SounderList(1,numSounder);
        if sounder.m_isMultiBeam
            for numTrans=1:size(sounder.m_transducer,2);
                Transducer=sounder.m_transducer(1,numTrans);
                for numChan=1:size(Transducer.m_SoftChannel,2)
                    SoftChannel=Transducer.m_SoftChannel(1,numChan);
                    StringList=SoftChannel.m_channelName;
                    if abs(SoftChannel.m_mainBeamAthwartSteeringAngleRad) >AngleRad
                        MyStruct=[];
                        MyStruct.sounderId=sounder.m_SounderId;
                        MyStruct.transIndex=numTrans-1;
                        MyStruct.chanIndex=numChan-1;
                        fprintf('Erasing Channel %s\n',StringList);
                        EraseChannelData(MyStruct.sounderId,MyStruct.transIndex,MyStruct.chanIndex);
                    end
                end;
            end;
        end;
    end;
    
    
end