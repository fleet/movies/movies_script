function EchoThreshold(ThresholdValue)
WaitBar= waitbar(0,'Thresholding...');
for numPingFan=0:moGetNumberOfPingFan()-1
    waitbar(numPingFan/moGetNumberOfPingFan());
    PingFan=moGetPingFan(numPingFan);
    PingId=PingFan.m_pingId;
    fanSounder = moGetFanSounder(PingId);
  
        for transducerIndex=0:fanSounder.m_numberOfTransducer-1
            PolarMatrix=moGetPolarMatrix(PingId,transducerIndex);
            MASK = ThresholdValue*100 < PolarMatrix.m_Amplitude  ;
            PolarMatrix.m_Amplitude=getFilteredMatrix(PolarMatrix.m_Amplitude,MASK);
            moSetPolarMatrix(PingId,transducerIndex,PolarMatrix);
        end;


end;
close(WaitBar);

end
function M = getFilteredMatrix(InputMatrix,MaskedMatrix)
    M=(InputMatrix+12000 ).* MaskedMatrix - 12000;
end
