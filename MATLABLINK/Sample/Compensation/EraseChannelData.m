function EraseChannelData(sounderId, transducerIndex,softChannelIdx)
% just print the number of ping fan
Sounder=moGetSounder(sounderId);
Transducer =Sounder.m_transducer(1,transducerIndex+1);
Channel=Transducer.m_SoftChannel(1,softChannelIdx+1);
WaitBar= waitbar(0,'Erasing Channel Data...');
for numPingFan=0:moGetNumberOfPingFan()-1
    waitbar(numPingFan/moGetNumberOfPingFan());
    PingFan=moGetPingFan(numPingFan);
    PingId=PingFan.m_pingId;
    fanSounder = moGetFanSounder(PingId);
    if fanSounder.m_SounderId==sounderId
        PolarMatrix=moGetPolarMatrix(PingId,transducerIndex);
        PolarMatrix.m_Amplitude(1:end,softChannelIdx+1)=-12000;
        moSetPolarMatrix(PingId,transducerIndex,PolarMatrix);
    end

end;
close(WaitBar);


