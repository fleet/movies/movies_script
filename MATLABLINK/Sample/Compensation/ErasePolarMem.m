function ErasePolarMem(minValue,maxValue)
WaitBar= waitbar(0,'erasing polar memory area...');
for numPingFan=0:moGetNumberOfPingFan()-1
    waitbar(numPingFan/moGetNumberOfPingFan());

    PingFan=moGetPingFan(numPingFan);
    PingId=PingFan.m_pingId;
    fanSounder = moGetFanSounder(PingId);
    for transducerIndex=0:fanSounder.m_numberOfTransducer-1
        PolarMatrix=moGetPolarMatrix(PingId,transducerIndex);
        beamSampleSpacing = fanSounder.m_transducer.m_beamsSamplesSpacing;
        indexMin = floor(minValue/beamSampleSpacing);
        indexMax = floor(maxValue/beamSampleSpacing);        
        PolarMatrix.m_Amplitude(1:indexMax,:)=-12000;
%         for beamIdx = 1:fanSounder.m_transducer(transducerIndex+1).m_numberOfSoftChannel
%             for SampleNumber = 1:size(PolarMatrix.m_Amplitude,1)
%                 Coord = moGetPolarToWorldCoord(PingId,transducerIndex,beamIdx-1,SampleNumber);
%                 if (Coord(3)<minValue || Coord(3)>maxValue)
%                     PolarMatrix.m_Amplitude(minValue+1:maxValue,:)=-12000;
%                 end;
% 
%             end;
%         end;
        moSetPolarMatrix(PingId,transducerIndex,PolarMatrix);
    end;
end;

close(WaitBar);

end