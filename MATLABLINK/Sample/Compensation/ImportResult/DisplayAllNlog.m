function DisplayAllNlog(Data, DisplayArea)
figure;
if DisplayArea==0
    ALLNlog=[];
    hold off;
    M=GetNLog(Data.Reference);
    plot(M,'-.xb');
    hold on;
    M=GetNLog(Data.Data);
    grid on;
    plot(M,'--or');

else
hold off
MyData=Data.Reference;
scatter(MyData.K38.data(:,1),MyData.K38.data(:,3),'ob');
hold on;
scatter(MyData.K70.data(:,1),MyData.K70.data(:,3),'xb');
scatter(MyData.K120.data(:,1),MyData.K120.data(:,3),'sb');
scatter(MyData.K200.data(:,1),MyData.K200.data(:,3),'*b');

MyData=Data.Data;
scatter(MyData.K38.data(:,1),MyData.K38.data(:,3),'or');
scatter(MyData.K70.data(:,1),MyData.K70.data(:,3),'xr');
scatter(MyData.K120.data(:,1),MyData.K120.data(:,3),'sr');
scatter(MyData.K200.data(:,1),MyData.K200.data(:,3),'*r');

 
    
end




function M=GetNLog(MyData)
    M=[
        %MyData.K18.data(:,1);    
        MyData.K38.data(:,1);    MyData.K70.data(:,1);    MyData.K120.data(:,1);    MyData.K200.data(:,1)];
    M=sort(M);

   
        
       