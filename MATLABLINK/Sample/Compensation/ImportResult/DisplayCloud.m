function DisplayCould( dataALL)


%MaxIndex=10;
BoolUseSV=0;

%% DISPLAY REFERENCE



if BoolUseSV==1
    IndexSA=4;
else
    IndexSA=3;
end

IndexNecho=7;
IndexProf=9;

NBmin = 10;
PrecisionLog = 0.005;
PrecisionProf=5;


% dataALL.Reference.K70.data(:,IndexSA)=dataALL.Reference.K70.data(:,IndexSA)-2;
% dataALL.SEUILB.K70.data(:,IndexSA)=dataALL.SEUILB.K70.data(:,IndexSA)-2;
% dataALL.FilterA.K70.data(:,IndexSA)=dataALL.FilterA.K70.data(:,IndexSA)-2;
% dataALL.FilterB.K70.data(:,IndexSA)=dataALL.FilterB.K70.data(:,IndexSA)-2;
% 
% dataALL.Reference.K120.data(:,IndexSA)=dataALL.Reference.K120.data(:,IndexSA)-2.5;
% dataALL.SEUILB.K120.data(:,IndexSA)=dataALL.SEUILB.K120.data(:,IndexSA)-2.5;
% dataALL.FilterA.K120.data(:,IndexSA)=dataALL.FilterA.K120.data(:,IndexSA)-2.5;
% dataALL.FilterB.K120.data(:,IndexSA)=dataALL.FilterB.K120.data(:,IndexSA)-2.5;
% 
% 
% dataALL.Reference.K200.data(:,IndexSA)=dataALL.Reference.K200.data(:,IndexSA)-0.5;
% dataALL.SEUILB.K200.data(:,IndexSA)=dataALL.SEUILB.K200.data(:,IndexSA)-0.5;
% dataALL.FilterA.K200.data(:,IndexSA)=dataALL.FilterA.K200.data(:,IndexSA)-0.5;
% dataALL.FilterB.K200.data(:,IndexSA)=dataALL.FilterB.K200.data(:,IndexSA)-0.5;

[NbEchoReference EnergyReference NlogReference] = MatchSchool(dataALL.Reference,IndexNecho,IndexSA,IndexProf,NBmin,PrecisionLog,PrecisionProf);
% [NbEchoSEUILA EnergySEUILA NlogSEUILA] = MatchSchool(dataALL.SEUILA,IndexNecho,IndexSA,NBmin,PrecisionLog,PrecisionProf);
[NbEchoSEUILB EnergySEUILB NlogSEUILB] = MatchSchool(dataALL.SEUILB,IndexNecho,IndexSA,IndexProf,NBmin,PrecisionLog,PrecisionProf);
[NbEchoFilterA EnergyFilterA NlogFilterA] = MatchSchool(dataALL.FilterA,IndexNecho,IndexSA,IndexProf,NBmin,PrecisionLog,PrecisionProf);
[NbEchoFilterB EnergyFilterB NlogFilterB] = MatchSchool(dataALL.FilterB,IndexNecho,IndexSA,IndexProf,NBmin,PrecisionLog,PrecisionProf);

% EnergySEUILA
% 
% NbEchoSEUILA
% 
% NlogSEUILA
% 
% EnergySEUILB
% 
% NbEchoSEUILB
% 
% NlogSEUILB

EnergyReference

NbEchoReference

NlogReference

EnergyFilterB

NbEchoFilterB

NlogFilterB



percentage= ones(length(NbEchoReference),5);
nbBancs = size(NlogFilterB,1);
for (numBanc=1:nbBancs)
    indexBanc =find(abs(NlogFilterB(numBanc,1)-NlogReference(:,1))<2*PrecisionLog);
    percentage(indexBanc,1) =  1-NbEchoFilterB(numBanc,1)/NbEchoReference(indexBanc,1);
    indexBanc =find(abs(NlogFilterB(numBanc,2)-NlogReference(:,2))<2*PrecisionLog);
    percentage(indexBanc,2) =  1-NbEchoFilterB(numBanc,2)/NbEchoReference(indexBanc,2);
    indexBanc =find(abs(NlogFilterB(numBanc,3)-NlogReference(:,3))<2*PrecisionLog);
    percentage(indexBanc,3) =  1-NbEchoFilterB(numBanc,3)/NbEchoReference(indexBanc,3);
    indexBanc =find(abs(NlogFilterB(numBanc,4)-NlogReference(:,4))<2*PrecisionLog);
    percentage(indexBanc,4) =  1-NbEchoFilterB(numBanc,4)/NbEchoReference(indexBanc,4);
    indexBanc =find(abs(NlogFilterB(numBanc,5)-NlogReference(:,5))<2*PrecisionLog);
    percentage(indexBanc,5) =  1-NbEchoFilterB(numBanc,5)/NbEchoReference(indexBanc,5);
end;




% figure;
% hold on;
% xlabel('School number');
% ylabel('Percentage of filtering');
% % plot( percentage(:,1),'b');
% plot(percentage(:,2),'g');
% plot(percentage(:,3),'c');
% plot(percentage(:,4),'r');
% plot(percentage(:,5),'y');
% 
% figure;
% hold on;
% xlabel('School number');
% ylabel('Variations ');
% plot( std(NbEchoFilterB(:,2:5),0,2),'-+b');
% plot(  100000*std(EnergyFilterB(:,2:5),0,2),'-or');
% 
% 
% figure;
% hold on;
% xlabel('Log');
% ylabel('Number of samples');
% plot( NlogFilterB(:,2),NbEchoFilterB(:,2),'-+b');
% plot( NlogReference(:,1),NbEchoReference(:,1),'-or');

figure;
hold on;
grid on;
xlabel('Frequency (kHz)');
ylabel('r(f)');
set(gca,'xtick',[18 38 70 120 200]);



PushResultToDisplay(18,38,70,120,200, EnergyReference(:,1),EnergyReference(:,2),EnergyReference(:,3),EnergyReference(:,4),EnergyReference(:,5),'b',0);

% PushResultToDisplay(18,38,70,120,200, EnergySEUILA(:,1),EnergySEUILA(:,2),EnergySEUILA(:,3),EnergySEUILA(:,4),EnergySEUILA(:,5),'c',0);


PushResultToDisplay(18,38,70,120,200, EnergySEUILB(:,1),EnergySEUILB(:,2),EnergySEUILB(:,3),EnergySEUILB(:,4),EnergySEUILB(:,5),'c',0);

PushResultToDisplay(18,38,70,120,200, EnergyFilterA(:,1),EnergyFilterA(:,2),EnergyFilterA(:,3),EnergyFilterA(:,4),EnergyFilterA(:,5),'g',0);

PushResultToDisplay(18,38,70,120,200, EnergyFilterB(:,1),EnergyFilterB(:,2),EnergyFilterB(:,3),EnergyFilterB(:,4),EnergyFilterB(:,5),'r',0);

 plot([18 38 70 120 200],[10^0 10^0 10^0 10^0 10^0],'-sk','LineWidth',2)


size(EnergyReference(:,1))
size(EnergyFilterB(:,1))

% figure;
% hold on;
% plot( dataALL.SEUILA.K38.data(:,IndexNecho)/dataALL.Reference.K38.data(:,IndexNecho),'b');
% plot(dataALL.SEUILA.K70.data(:,IndexNecho)/dataALL.Reference.K70.data(:,IndexNecho),'g');
% plot(dataALL.SEUILA.K120.data(:,IndexNecho)/dataALL.Reference.K120.data(:,IndexNecho),'c');
% plot(dataALL.SEUILA.K200.data(:,IndexNecho)/dataALL.Reference.K200.data(:,IndexNecho),'r');
% 
% figure;
% hold on;
% plot( dataALL.SEUILB.K38.data(:,IndexNecho)/dataALL.Reference.K38.data(:,IndexNecho),'b');
% plot(dataALL.SEUILB.K70.data(:,IndexNecho)/dataALL.Reference.K70.data(:,IndexNecho),'g');
% plot(dataALL.SEUILB.K120.data(:,IndexNecho)/dataALL.Reference.K120.data(:,IndexNecho),'c');
% plot(dataALL.SEUILB.K200.data(:,IndexNecho)/dataALL.Reference.K200.data(:,IndexNecho),'r');
% 

% 
% plot(NbEchoReference(:,1),'b:');
% plot(NbEchoReference(:,2),'g:');
% plot(NbEchoReference(:,3),'c:');
% plot(NbEchoReference(:,4),'r:');
% plot(NbEchoReference(:,5),'y:');
% 
% 
% for (index=1:length(NbEchoFilterA))
% 
% 
% 
% figure;
% hold on;
% hold on;
% xlabel('Frequency (kHz)');
% ylabel('MVBS (dB)');
% set(gca,'xtick',[18 38 70 120 200]);
% PushResultToDisplay(18,38,70,120,200, EnergyReference(index,1),EnergyReference(index,2),EnergyReference(index,3),EnergyReference(index,4),EnergyReference(index,5),'b',0);
% PushResultToDisplay(18,38,70,120,200, EnergySEUILA(index,1),EnergySEUILA(index,2),EnergySEUILA(index,3),EnergySEUILA(index,4),EnergySEUILA(index,5),'g',0);
% PushResultToDisplay(18,38,70,120,200, EnergySEUILB(index,1),EnergySEUILB(index,2),EnergySEUILB(index,3),EnergySEUILB(index,4),EnergySEUILB(index,5),'c',0);
% PushResultToDisplay(18,38,70,120,200, EnergyFilterA(index,1),EnergyFilterA(index,2),EnergyFilterA(index,3),EnergyFilterA(index,4),EnergyFilterA(index,5),'r',0);
% end;
%
% T=[0 ; 10 ; 20 ; 30];
% TT= [-48 -48 -46.5 -44];
% plot(T,TT,'--sk');

end
