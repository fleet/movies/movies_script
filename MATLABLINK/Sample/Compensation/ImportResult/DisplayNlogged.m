function varargout = DisplayNlogged(varargin)
% DISPLAYNLOGGED M-file for DisplayNlogged.fig
%      DISPLAYNLOGGED, by itself, creates a new DISPLAYNLOGGED or raises the existing
%      singleton*.
%
%      H = DISPLAYNLOGGED returns the handle to a new DISPLAYNLOGGED or the handle to
%      the existing singleton*.
%
%      DISPLAYNLOGGED('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in DISPLAYNLOGGED.M with the given input arguments.
%
%      DISPLAYNLOGGED('Property','Value',...) creates a new DISPLAYNLOGGED or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before DisplayNlogged_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to DisplayNlogged_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help DisplayNlogged

% Last Modified by GUIDE v2.5 10-Jun-2008 09:19:58

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @DisplayNlogged_OpeningFcn, ...
                   'gui_OutputFcn',  @DisplayNlogged_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before DisplayNlogged is made visible.
function DisplayNlogged_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to DisplayNlogged (see VARARGIN)

% Choose default command line output for DisplayNlogged
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes DisplayNlogged wait for user response (see UIRESUME)
% uiwait(handles.figure1);
optargin = size(varargin,2);

if optargin == 1
	handles.ALL_DATA =cell2mat(varargin);
    handles.MaxNlog=max(handles.ALL_DATA.Reference.K38.data(:,1));
    handles.MinNlog=min(handles.ALL_DATA.Reference.K38.data(:,1));
    AreaRef=[handles.ALL_DATA.Reference.K38.data(:,3); handles.ALL_DATA.Reference.K70.data(:,3); handles.ALL_DATA.Reference.K120.data(:,3);handles.ALL_DATA.Reference.K200.data(:,3)];
    AreaRefA=[handles.ALL_DATA.ReferenceA.K38.data(:,3);handles.ALL_DATA.ReferenceA.K70.data(:,3);handles.ALL_DATA.ReferenceA.K120.data(:,3);handles.ALL_DATA.ReferenceA.K200.data(:,3)];
    AreaRefB=[handles.ALL_DATA.ReferenceB.K38.data(:,3); handles.ALL_DATA.ReferenceB.K70.data(:,3);handles.ALL_DATA.ReferenceB.K120.data(:,3);handles.ALL_DATA.ReferenceB.K200.data(:,3)];
    AreaRefData=[handles.ALL_DATA.Data.K38.data(:,3); handles.ALL_DATA.Data.K70.data(:,3);handles.ALL_DATA.Data.K120.data(:,3);handles.ALL_DATA.Data.K200.data(:,3)];

    handles.MinSv=min([AreaRef;AreaRefA;AreaRefB;AreaRefData]);
    handles.MaxSv=max([AreaRef;AreaRefA;AreaRefB;AreaRefData]);
    set(handles.edit2,'String',handles.MaxNlog);
    set(handles.edit1,'String',handles.MinNlog);
    set(handles.edit3,'String',handles.MinSv);
    set(handles.edit4,'String',handles.MaxSv);
    guidata(hObject, handles);
  
end;


% --- Outputs from this function are returned to the command line.
function varargout = DisplayNlogged_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
DisplayAllNlog(handles.ALL_DATA,0);



function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double
 
 

% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

 handles.MaxNlog=str2double(get(handles.edit2,'String'));
 handles.MinNlog=str2double(get(handles.edit1,'String'));
 handles.MinSv=str2double(get(handles.edit3,'String'));
 handles.MaxSv=str2double(get(handles.edit4,'String'));
 guidata(hObject, handles);
DisplayResult(handles);


% --- Executes on key press with focus on pushbutton1 and none of its controls.
function pushbutton1_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  structure with the following fields (see UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)

function DisplayResult(handles)
figure();
hold on;
ALL_DATA=handles.ALL_DATA.Reference;
DisplayFileResult(handles, ALL_DATA, 'b',-2);
ALL_DATA=handles.ALL_DATA.Data;
DisplayFileResult(handles, ALL_DATA, 'r',-1.8);
ALL_DATA=handles.ALL_DATA.ReferenceA;
DisplayFileResult(handles, ALL_DATA, 'g',-1.6);
 ALL_DATA=handles.ALL_DATA.ReferenceB;
DisplayFileResult(handles, ALL_DATA, 'c',-1.4);
  

function DisplayFileResult(handles, ALL_DATA,Color,OffsetMean)
dist38=[];
value38=[];
dist70=[];
value70=[];
dist120=[];
value120=[];
dist200=[];
value200=[];

DATA = ALL_DATA.K38.data;
for i=1:size(DATA,1)
%dist38,dist70,dist120,dist200,value38,value70,value120,value200,color,offstMean
    Mydist38= DATA(i,1);
    MySv38= DATA(i,3);
    if Mydist38 <= handles.MaxNlog 
        if Mydist38 >= handles.MinNlog  
         if MySv38 <= handles.MaxSv 
            if MySv38 >= handles.MinSv  
                dist38=[dist38;Mydist38];
                value38=[value38; DATA(i,3)];
            end
         end
        end
    end
end;

DATA = ALL_DATA.K70.data;
for i=1:size(DATA,1)
    Mydist= DATA(i,1);
    MySv=DATA(i,3);
    if Mydist <=handles.MaxNlog 
        if Mydist > handles.MinNlog 
            if MySv <= handles.MaxSv
                if MySv >= handles.MinSv
                    dist70=[dist70;Mydist];
                    value70=[value70; DATA(i,3)];
                end
            end
        end
    end
end;
DATA = ALL_DATA.K120.data;
for i=1:size(DATA,1)
    Mydist= DATA(i,1);
    MySv=DATA(i,3);
    if Mydist <=handles.MaxNlog
        if Mydist > handles.MinNlog
            if MySv <= handles.MaxSv
                if MySv >= handles.MinSv
                    dist120=[dist120;Mydist];
                    value120=[value120; DATA(i,3)];
                end
            end
        end
    end
end;

DATA = ALL_DATA.K200.data;
for i=1:size(DATA,1)
    Mydist= DATA(i,1);
    MySv=DATA(i,3);
    if Mydist <=handles.MaxNlog 
        if Mydist > handles.MinNlog 
            if MySv <= handles.MaxSv
                if MySv >= handles.MinSv
                    dist200=[dist200;Mydist];
                    value200=[value200; DATA(i,3)];
                end
            end
        end
    end
end;
PushResultToDisplay(dist38,dist70,dist120,dist200,value38,value70,value120,value200,Color,OffsetMean);



function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double


% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit4 as text
%        str2double(get(hObject,'String')) returns contents of edit4 as a double


% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
DisplayAllNlog(handles.ALL_DATA,1);



