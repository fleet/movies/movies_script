Reference=[];
OrigDir=pwd;
%% START REFERENCE
cd C:\data\Simulation\20maquereauWithAngle\REWRITED\FILTER\BASE;
StartFileName='results_id';
EndFileName='kHz_shl.csv';



FileName=strcat(StartFileName,'1_38');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Reference.K38.data=Imp.data;
Reference.ColHeader=Imp.colheaders;

FileName=strcat(StartFileName,'2_70');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Reference.K70.data=Imp.data;


FileName=strcat(StartFileName,'3_120');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Reference.K120.data=Imp.data;

FileName=strcat(StartFileName,'4_200');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Reference.K200.data=Imp.data;


% END REFERENCE
 

%% START REFERENCE 65
cd C:\data\Simulation\20maquereauWithAngle\REWRITED\FILTER\SEUIL_75;
StartFileName='results_id';
EndFileName='kHz_shl.csv';


FileName=strcat(StartFileName,'1_38');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Reference65.K38.data=Imp.data;
Reference65.ColHeader=Imp.colheaders;


FileName=strcat(StartFileName,'2_70');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Reference65.K70.data=Imp.data;


FileName=strcat(StartFileName,'3_120');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Reference65.K120.data=Imp.data;

FileName=strcat(StartFileName,'4_200');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Reference65.K200.data=Imp.data;


% END REFERENCE 65
%% START 60

cd C:\data\Simulation\20maquereauWithAngle\REWRITED\FILTER\SEUIL_60;
StartFileName='results_id';
EndFileName='kHz_shl.csv';




FileName=strcat(StartFileName,'1_38');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Reference40.K38.data=Imp.data;
Reference40.ColHeader=Imp.colheaders;

FileName=strcat(StartFileName,'2_70');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Reference40.K70.data=Imp.data;


FileName=strcat(StartFileName,'3_120');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Reference40.K120.data=Imp.data;

FileName=strcat(StartFileName,'4_200');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Reference40.K200.data=Imp.data;


% END REFERENCE 40
%% START 20
cd C:\data\Simulation\20maquereauWithAngle\REWRITED\FILTER\RESULT_40;
StartFileName='results_id';




EndFileName='kHz_shl.csv';
Data=[];



FileName=strcat(StartFileName,'1_38');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Data.K38.data=Imp.data;
Data.ColHeader=Imp.colheaders;

FileName=strcat(StartFileName,'2_70');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Data.K70.data=Imp.data;


FileName=strcat(StartFileName,'3_120');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Data.K120.data=Imp.data;

FileName=strcat(StartFileName,'4_200');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Data.K200.data=Imp.data;

%  END 20
%% RESTORE

ALL.Reference=Reference;
ALL.ReferenceA=Reference65;
ALL.ReferenceB=Reference40;
ALL.Data=Data;
cd(OrigDir);
