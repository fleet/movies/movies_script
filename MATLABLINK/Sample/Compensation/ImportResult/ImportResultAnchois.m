Reference=[];
Reference45=[];
Reference60=[];

OrigDir=pwd;
%% START REFERENCE
cd C:\data\OVERLAP\Simulation\20anchoisWithAngle\REWRITED\FILTER\BASE;
StartFileName='results_id';
EndFileName='kHz_shl.csv';


FileName=strcat(StartFileName,'0_38');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Reference.K38.data=Imp.data;
% Reference.ColHeader=Imp.colheaders;

FileName=strcat(StartFileName,'1_70');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Reference.K70.data=Imp.data;


FileName=strcat(StartFileName,'2_120');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Reference.K120.data=Imp.data;

FileName=strcat(StartFileName,'3_200');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Reference.K200.data=Imp.data;


% END REFERENCE
 

%% START REFERENCE 45
cd C:\data\OVERLAP\Simulation\20anchoisWithAngle\REWRITED\FILTER\SEUIL_45;
StartFileName='results_id';
EndFileName='kHz_shl.csv';


FileName=strcat(StartFileName,'0_38');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Reference45.K38.data=Imp.data;
% Reference45.ColHeader=Imp.colheaders;

FileName=strcat(StartFileName,'1_70');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Reference45.K70.data=Imp.data;


FileName=strcat(StartFileName,'2_120');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Reference45.K120.data=Imp.data;

FileName=strcat(StartFileName,'3_200');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Reference45.K200.data=Imp.data;


% END REFERENCE 65
%% START 60

cd C:\data\OVERLAP\Simulation\20anchoisWithAngle\REWRITED\FILTER\SEUIL_60;
StartFileName='results_id';
EndFileName='kHz_shl.csv';

% FileName=strcat(StartFileName,'0_18');
% FileName=strcat(FileName,EndFileName);
% Imp=importdata(FileName);
% 
% Reference40.K18.data=Imp.data;
% 
FileName=strcat(StartFileName,'0_38');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Reference60.K38.data=Imp.data;
% Reference60.ColHeader=Imp.colheaders;
% 

FileName=strcat(StartFileName,'1_70');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Reference60.K70.data=Imp.data;


FileName=strcat(StartFileName,'2_120');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Reference60.K120.data=Imp.data;

FileName=strcat(StartFileName,'3_200');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Reference60.K200.data=Imp.data;


% END REFERENCE 40
%% START 20
cd C:\data\OVERLAP\Simulation\20anchoisWithAngle\REWRITED\FILTER\RESULT_90;
StartFileName='results_id';


EndFileName='kHz_shl.csv';
Data=[];



FileName=strcat(StartFileName,'0_38');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Data.K38.data=Imp.data;
% Data.ColHeader=Imp.colheaders;

FileName=strcat(StartFileName,'1_70');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Data.K70.data=Imp.data;


FileName=strcat(StartFileName,'2_120');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Data.K120.data=Imp.data;

FileName=strcat(StartFileName,'3_200');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Data.K200.data=Imp.data;

%  END 20
%% RESTORE

ALL.Reference=Reference;
ALL.ReferenceB=Reference45;
ALL.ReferenceA=Reference60;
ALL.Data=Data;
cd(OrigDir);
