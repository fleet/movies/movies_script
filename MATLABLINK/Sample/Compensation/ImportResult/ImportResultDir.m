function M=ImportResultDir(DirName)

Reference=[];
OrigDir=pwd;
%% START REFERENCE
cd(DirName);
StartFileName='results_id';
EndFileName='kHz_shl.csv';



FileName=strcat(StartFileName,'47_38');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Imp.data=Imp;
Reference.K38=Imp.data;
% Reference.ColHeader=Imp.colheaders;

FileName=strcat(StartFileName,'48_70');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Imp.data=Imp;
Reference.K70=Imp.data;


FileName=strcat(StartFileName,'49_120');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Imp.data=Imp;
Reference.K120=Imp.data;

FileName=strcat(StartFileName,'50_200');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Imp.data=Imp;
Reference.K200=Imp.data;


M=Reference;
cd(OrigDir);
