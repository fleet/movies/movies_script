

OrigDir=pwd;
%% START REFERENCE
cd C:\data\EXACHA08\RUN003\REWRITED\BASE\SEUIL65;
StartFileName='results_id';
EndFileName='kHz_shl.csv';

FileName=strcat(StartFileName,'46_18');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Reference.K18.data=Imp.data;

FileName=strcat(StartFileName,'47_38');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Reference.K38.data=Imp.data;
% Reference.ColHeader=Imp.colheaders;

FileName=strcat(StartFileName,'48_70');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Reference.K70.data=Imp.data;


FileName=strcat(StartFileName,'49_120');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Reference.K120.data=Imp.data;

FileName=strcat(StartFileName,'50_200');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Reference.K200.data=Imp.data;


% END REFERENCE
 


cd C:\data\EXACHA08\RUN003\REWRITED\BASE\SEUIL65;
StartFileName='results_id';
EndFileName='kHz_shl.csv';

% FileName=strcat(StartFileName,'0_18');
% FileName=strcat(FileName,EndFileName);
% Imp=importdata(FileName);
% 
% Reference40.K18.data=Imp.data;
% 

FileName=strcat(StartFileName,'46_18');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
SEUIL55.K18.data=Imp.data;

FileName=strcat(StartFileName,'47_38');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
SEUIL55.K38.data=Imp.data;

FileName=strcat(StartFileName,'48_70');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
SEUIL55.K70.data=Imp.data;


FileName=strcat(StartFileName,'49_120');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
SEUIL55.K120.data=Imp.data;

FileName=strcat(StartFileName,'50_200');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
SEUIL55.K200.data=Imp.data;

% END REFERENCE 60
%% START 80

cd C:\data\EXACHA08\RUN003\REWRITED\BASE\SEUIL60;
StartFileName='results_id';
EndFileName='kHz_shl.csv';

% FileName=strcat(StartFileName,'0_18');
% FileName=strcat(FileName,EndFileName);
% Imp=importdata(FileName);
% 
% Reference40.K18.data=Imp.data;
% 

FileName=strcat(StartFileName,'46_18');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
SEUIL50.K18.data=Imp.data;

FileName=strcat(StartFileName,'47_38');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
SEUIL50.K38.data=Imp.data;

FileName=strcat(StartFileName,'48_70');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
SEUIL50.K70.data=Imp.data;


FileName=strcat(StartFileName,'49_120');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
SEUIL50.K120.data=Imp.data;

FileName=strcat(StartFileName,'50_200');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
SEUIL50.K200.data=Imp.data;


%% START REFERENCE 60
cd C:\data\EXACHA08\RUN003\REWRITED\FILTER\RESULT_60;
StartFileName='results3_id';
EndFileName='kHz_shl.csv';

FileName=strcat(StartFileName,'46_18');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Filter40.K18.data=Imp.data;

FileName=strcat(StartFileName,'47_38');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Filter40.K38.data=Imp.data;
% Reference45.ColHeader=Imp.colheaders;

FileName=strcat(StartFileName,'48_70');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Filter40.K70.data=Imp.data;


FileName=strcat(StartFileName,'49_120');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Filter40.K120.data=Imp.data;

FileName=strcat(StartFileName,'50_200');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Filter40.K200.data=Imp.data;

% END REFERENCE 80
%% START 100
cd C:\data\EXACHA08\RUN003\REWRITED\FILTER\RESULT_80;
StartFileName='results3_id';


EndFileName='kHz_shl.csv';



% FileName=strcat(StartFileName,'46_18');
% FileName=strcat(FileName,EndFileName);
% Imp=importdata(FileName);
% Filter60.K18.data=Imp.data;

FileName=strcat(StartFileName,'47_38');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Filter60.K38.data=Imp.data;
% Data.ColHeader=Imp.colheaders;

FileName=strcat(StartFileName,'48_70');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Filter60.K70.data=Imp.data;


FileName=strcat(StartFileName,'49_120');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Filter60.K120.data=Imp.data;

FileName=strcat(StartFileName,'50_200');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Filter60.K200.data=Imp.data;

%  END 100
%% RESTORE

ALL.Reference=Reference;
ALL.SEUILA=SEUIL55;
ALL.SEUILB=SEUIL50;
ALL.FilterA=Filter40;
ALL.FilterB=Filter60;
cd(OrigDir);
