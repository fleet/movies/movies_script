Reference=[];
OrigDir=pwd;
%% START REFERENCE
cd C:\data\OVERLAP\PELGAS07\REWRITED\FILTER\BASE;
StartFileName='results_id';
EndFileName='kHz_shl.csv';



FileName=strcat(StartFileName,'47_38');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Reference.K38.data=Imp.data;
Reference.ColHeader=Imp.colheaders;

FileName=strcat(StartFileName,'48_70');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Reference.K70.data=Imp.data;


FileName=strcat(StartFileName,'49_120');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Reference.K120.data=Imp.data;

FileName=strcat(StartFileName,'50_200');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Reference.K200.data=Imp.data;


% END REFERENCE
 

%% START REFERENCE 65
cd C:\data\OVERLAP\PELGAS07\REWRITED\FILTER\SEUIL_50;
StartFileName='results_id';
EndFileName='kHz_shl.csv';


FileName=strcat(StartFileName,'47_38');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Reference65.K38.data=Imp.data;
Reference65.ColHeader=Imp.colheaders;


FileName=strcat(StartFileName,'48_70');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Reference65.K70.data=Imp.data;


FileName=strcat(StartFileName,'49_120');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Reference65.K120.data=Imp.data;

FileName=strcat(StartFileName,'50_200');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Reference65.K200.data=Imp.data;


% END REFERENCE 65
%% START 60

cd C:\data\OVERLAP\PELGAS07\REWRITED\FILTER\SEUIL_60;
StartFileName='results_id';
EndFileName='kHz_shl.csv';




FileName=strcat(StartFileName,'47_38');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Reference40.K38.data=Imp.data;
Reference40.ColHeader=Imp.colheaders;

FileName=strcat(StartFileName,'48_70');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Reference40.K70.data=Imp.data;


FileName=strcat(StartFileName,'49_120');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Reference40.K120.data=Imp.data;

FileName=strcat(StartFileName,'50_200');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Reference40.K200.data=Imp.data;


% END REFERENCE 40
%% START 20
cd C:\data\OVERLAP\PELGAS07\REWRITED\FILTER\RESULT_70;
StartFileName='results_id';




EndFileName='kHz_shl.csv';
Data=[];



FileName=strcat(StartFileName,'47_38');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Data.K38.data=Imp.data;
Data.ColHeader=Imp.colheaders;

FileName=strcat(StartFileName,'48_70');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Data.K70.data=Imp.data;


FileName=strcat(StartFileName,'49_120');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Data.K120.data=Imp.data;

FileName=strcat(StartFileName,'50_200');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Data.K200.data=Imp.data;

%  END 20
%% RESTORE

ALL.Reference=Reference;
ALL.ReferenceA=Reference65;
ALL.ReferenceB=Reference40;
ALL.Data=Data;
cd(OrigDir);
