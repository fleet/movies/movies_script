Reference=[];
Reference60=[];
Reference80=[];
Reference100=[];

OrigDir=pwd;
%% START REFERENCE
cd C:\data\OVERLAP\Simulation\60sardinsWithAngle\REWRITED\BASE\ALL\SEUIL50;
StartFileName='results_id';
EndFileName='kHz_shl.csv';

FileName=strcat(StartFileName,'0_18');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Reference.K18.data=Imp.data;

FileName=strcat(StartFileName,'1_38');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Reference.K38.data=Imp.data;
% Reference.ColHeader=Imp.colheaders;

FileName=strcat(StartFileName,'2_70');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Reference.K70.data=Imp.data;


FileName=strcat(StartFileName,'3_120');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Reference.K120.data=Imp.data;

FileName=strcat(StartFileName,'4_200');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Reference.K200.data=Imp.data;


% END REFERENCE
 


cd C:\data\OVERLAP\Simulation\60sardinsWithAngle\REWRITED\BASE\ALL\SEUIL50;
StartFileName='results_id';
EndFileName='kHz_shl.csv';

% FileName=strcat(StartFileName,'0_18');
% FileName=strcat(FileName,EndFileName);
% Imp=importdata(FileName);
% 
% Reference40.K18.data=Imp.data;
% 

FileName=strcat(StartFileName,'0_18');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
SEUIL50.K18.data=Imp.data;

FileName=strcat(StartFileName,'1_38');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
SEUIL50.K38.data=Imp.data;

FileName=strcat(StartFileName,'2_70');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
SEUIL50.K70.data=Imp.data;


FileName=strcat(StartFileName,'3_120');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
SEUIL50.K120.data=Imp.data;

FileName=strcat(StartFileName,'4_200');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
SEUIL50.K200.data=Imp.data;

% END REFERENCE 60
%% START 80

cd C:\data\OVERLAP\Simulation\60sardinsWithAngle\REWRITED\BASE\ALL\SEUIL60;
StartFileName='results_id';
EndFileName='kHz_shl.csv';

% FileName=strcat(StartFileName,'0_18');
% FileName=strcat(FileName,EndFileName);
% Imp=importdata(FileName);
% 
% Reference40.K18.data=Imp.data;
% 

FileName=strcat(StartFileName,'0_18');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
SEUIL60.K18.data=Imp.data;

FileName=strcat(StartFileName,'1_38');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
SEUIL60.K38.data=Imp.data;

FileName=strcat(StartFileName,'2_70');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
SEUIL60.K70.data=Imp.data;


FileName=strcat(StartFileName,'3_120');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
SEUIL60.K120.data=Imp.data;

FileName=strcat(StartFileName,'4_200');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
SEUIL60.K200.data=Imp.data;


%% START REFERENCE 60
cd C:\data\OVERLAP\Simulation\60sardinsWithAngle\REWRITED\FILTER\RESULT_60;
StartFileName='results_id';
EndFileName='kHz_shl.csv';

FileName=strcat(StartFileName,'0_18');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Filter60.K18.data=Imp.data;

FileName=strcat(StartFileName,'1_38');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Filter60.K38.data=Imp.data;
% Reference45.ColHeader=Imp.colheaders;

FileName=strcat(StartFileName,'2_70');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Filter60.K70.data=Imp.data;


FileName=strcat(StartFileName,'3_120');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Filter60.K120.data=Imp.data;

FileName=strcat(StartFileName,'4_200');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Filter60.K200.data=Imp.data;

% END REFERENCE 80
%% START 100
cd C:\data\OVERLAP\Simulation\60sardinsWithAngle\REWRITED\FILTER\RESULT_100;
StartFileName='results_id';


EndFileName='kHz_shl.csv';



FileName=strcat(StartFileName,'0_18');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Filter90.K18.data=Imp.data;

FileName=strcat(StartFileName,'1_38');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Filter90.K38.data=Imp.data;
% Data.ColHeader=Imp.colheaders;

FileName=strcat(StartFileName,'2_70');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Filter90.K70.data=Imp.data;


FileName=strcat(StartFileName,'3_120');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Filter90.K120.data=Imp.data;

FileName=strcat(StartFileName,'4_200');
FileName=strcat(FileName,EndFileName);
Imp=importdata(FileName);
Filter90.K200.data=Imp.data;

%  END 100
%% RESTORE

ALL.Reference=Reference;
ALL.SEUILA=SEUIL50;
ALL.SEUILB=SEUIL60;
ALL.FilterA=Filter60;
ALL.FilterB=Filter90;
cd(OrigDir);
