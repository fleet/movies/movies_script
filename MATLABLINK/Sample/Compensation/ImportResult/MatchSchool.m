function [NbEcho,Energy, Nlog]=MatchSchool(data,IndexNecho,IndexSA, IndexProf, Nbmin, precisionLog, precisionProf)


indexBanc = 1;
for numBanc=1:size(data.K38.data(:,1))
    
%     index18 = find(abs(data.K38.data(numBanc,2)-data.K18.data(:,2))<precisionLog);
%     index70 = find(abs(data.K38.data(numBanc,2)-data.K70.data(:,2))<precisionLog);
%     index120 = find(abs(data.K38.data(numBanc,2)-data.K120.data(:,2))<precisionLog);
%     index200 = find(abs(data.K38.data(numBanc,2)-data.K200.data(:,2))<precisionLog);

    index18 = find(abs(data.K38.data(numBanc,2)-data.K18.data(:,2))<precisionLog & abs(data.K38.data(numBanc,IndexProf)-data.K18.data(:,IndexProf))<precisionProf);
    index70 = find(abs(data.K38.data(numBanc,2)-data.K70.data(:,2))<precisionLog & abs(data.K38.data(numBanc,IndexProf)-data.K70.data(:,IndexProf))<precisionProf);
    index120 = find(abs(data.K38.data(numBanc,2)-data.K120.data(:,2))<precisionLog & abs(data.K38.data(numBanc,IndexProf)-data.K120.data(:,IndexProf))<precisionProf);
    index200 = find(abs(data.K38.data(numBanc,2)-data.K200.data(:,2))<precisionLog & abs(data.K38.data(numBanc,IndexProf)-data.K200.data(:,IndexProf))<precisionProf);
   
    
    
    if (~isempty(index18) && ~isempty(index70) &&  ~isempty(index120) && ~isempty(index200) ...
        && data.K18.data(min(index18),IndexNecho)>Nbmin*tan(10*pi/180)*data.K18.data(min(index18),IndexProf) ...
        && data.K38.data(numBanc,IndexNecho)>Nbmin*tan(3.5*pi/180)*data.K38.data(numBanc,IndexProf) ...
        && data.K70.data(min(index70),IndexNecho)>Nbmin*tan(3.5*pi/180)*data.K70.data(min(index70),IndexProf) ...
        && data.K120.data(min(index120),IndexNecho)>Nbmin*tan(3.5*pi/180)*data.K120.data(min(index120),IndexProf) ...
        && data.K200.data(min(index200),IndexNecho)>Nbmin*tan(3.5*pi/180)*data.K200.data(min(index200),IndexProf) )
        NbEcho(indexBanc,:) = [data.K18.data(min(index18),IndexNecho) data.K38.data(numBanc,IndexNecho) data.K70.data(min(index70),IndexNecho) data.K120.data(min(index120),IndexNecho) data.K200.data(min(index200),IndexNecho)];
        Energy(indexBanc,:) = [data.K18.data(min(index18),IndexSA) data.K38.data(numBanc,IndexSA) data.K70.data(min(index70),IndexSA) data.K120.data(min(index120),IndexSA) data.K200.data(min(index200),IndexSA)];
        Nlog(indexBanc,:) = [data.K18.data(min(index18),2) data.K38.data(numBanc,2) data.K70.data(min(index70),2) data.K120.data(min(index120),2) data.K200.data(min(index200),2)];
        indexBanc = indexBanc +1;
    end
end

