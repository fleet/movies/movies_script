%%
%%
function PushResultToDisplay(dist18,dist38,dist70,dist120,dist200,value18,value38,value70,value120,value200,color,offstMean)
lineSpc=strcat('*',color);


BoolUseSV=0;


if BoolUseSV==1
    value18 = value18-value38;

value70 = value70-value38
value120 = value120-value38;
value200 = value200-value38;
value38 = value38-value38;
else
    value18 = value18./value38;

value70 = value70./value38;
value120 = value120./value38;
value200 = value200./value38;
value38 = value38./value38;
end



% DecValue18=10.^(value18/10);
% DecValue38=10.^(value38/10);
% DecValue70=10.^(value70/10);
% DecValue120=10.^(value120/10);
% DecValue200=10.^(value200/10);

W=GetMeanStd(value18);
X=GetMeanStd(value38);
Y=GetMeanStd(value70);
Z=GetMeanStd(value120);
A=GetMeanStd(value200);


Median=[W.Median;X.Median;Y.Median;Z.Median;A.Median];
Mstd= [W.Std;X.Std;Y.Std;Z.Std;A.Std];
Prctile5 = [W.prctile5;X.prctile5;Y.prctile5;Z.prctile5;A.prctile5];
Prctile95 = [W.prctile95;X.prctile95;Y.prctile95;Z.prctile95;A.prctile95];


hold on;
abcisse = [ dist18 dist38 dist70 dist120 dist200];
scatter(dist18*ones(size(value18)),value18,lineSpc);
scatter(dist38*ones(size(value38)),value38,lineSpc);
scatter(dist70*ones(size(value70)),value70,lineSpc);
scatter(dist120*ones(size(value120)),value120,lineSpc);
scatter(dist200*ones(size(value200)),value200,lineSpc);

    ordonnee = Median;
    lineSpc=strcat('-',color);
plot(abcisse, ordonnee, lineSpc,'LineWidth',3);

    ordonnee = Prctile5;
    lineSpc=strcat('--',color);
plot(abcisse, ordonnee, lineSpc);

    ordonnee = Prctile95;
    lineSpc=strcat('--',color);
plot(abcisse, ordonnee, lineSpc);

end

function M= GetMeanStd(DecValue)
M=[];
M.Mean=(mean(DecValue));
M.Median=(median(DecValue));
M.Mode=(mode(DecValue));
M.prctile5 = (prctile(DecValue,5));
M.prctile95 = (prctile(DecValue,95));
M.Std=(std(DecValue));
end


