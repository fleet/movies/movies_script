function varargout = OverLapGUI(varargin)
% OVERLAPGUI M-file for OverLapGUI.fig
%      OVERLAPGUI, by itself, creates a new OVERLAPGUI or raises the existing
%      singleton*.
%
%      H = OVERLAPGUI returns the handle to a new OVERLAPGUI or the handle to
%      the existing singleton*.
%
%      OVERLAPGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in OVERLAPGUI.M with the given input arguments.
%
%      OVERLAPGUI('Property','Value',...) creates a new OVERLAPGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before OverLapGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to OverLapGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help OverLapGUI

% Last Modified by GUIDE v2.5 11-Jun-2008 09:39:51

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @OverLapGUI_OpeningFcn, ...
                   'gui_OutputFcn',  @OverLapGUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before OverLapGUI is made visible.
function OverLapGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to OverLapGUI (see VARARGIN)

% Choose default command line output for OverLapGUI
handles.output = hObject;
handles.CurrentFileName='';
handles.CurrentDirectory='c:\\';
 
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes OverLapGUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = OverLapGUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in LoadFileButton.
function LoadFileButton_Callback(hObject, eventdata, handles)
% hObject    handle to LoadFileButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[FileName,PathName,FilterIndex]=uigetfile('*.hac','Load HAC File');
if ~isequal(FileName,0)
    Name=strcat(PathName,FileName);
    handles.CurrentFileName=Name;
    handles.CurrentDirectory=PathName;
    
ParameterKernel= moKernelParameter();
ParameterDef=moLoadKernelParameter(ParameterKernel);


    ParameterDef.m_ScreenPixelSizeY=0.1;
    ParameterDef.m_ScreenPixelSizeX=0.1;

    % on filtre zone aveugle et fond
paramFilter = moGetFilterModuleParameter(moFilterModule);
paramFilter.SubFilterState_1 = 1;
paramFilter.SubFilterState_4 = 1;
paramFilter.SubFilterState_5 = 1;
moSetFilterModuleParameter(moFilterModule,paramFilter);

    moReadWholeFile(Name);
    UpdateStat(handles);  
    handles.CurrentDisplayList=[];
    handles.ChannelNameList=[];
    handles.ValuesChannelId=[];
    % no populate everything

    StringList=[];
    ValuesChannelId=[];
    SounderList= moGetSounderList();
    for numSounder=1:size(SounderList,2)
        sounder=SounderList(1,numSounder);
        for numTrans=1:size(sounder.m_transducer,2);
            Transducer=sounder.m_transducer(1,numTrans);
            for numChan=1:size(Transducer.m_SoftChannel,2)
                SoftChannel=Transducer.m_SoftChannel(1,numChan);
                StringList=strvcat(StringList,SoftChannel.m_channelName);
                MyStruct=[];
                MyStruct.sounderId=sounder.m_SounderId;
                MyStruct.transIndex=numTrans-1;
                MyStruct.chanIndex=numChan-1;
                ValuesChannelId=[ValuesChannelId;MyStruct];
            end;
        end;
    end;
    set(handles.ChannelList,'String',StringList);
    handles.ValuesChannelId=ValuesChannelId;
    handles.ChannelNameList=StringList;

    guidata(hObject, handles);

else
    msgbox('File does not exist');
end;


% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over LoadFileButton.
function LoadFileButton_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to LoadFileButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushWrite.
function pushWrite_Callback(hObject, eventdata, handles)
% hObject    handle to pushWrite (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
moStartWrite(handles.CurrentDirectory, 'MATLAB_OUTPUT_');
moStopWrite();

function UpdateStat(handles)

set( handles.textCurrentDirectory1,'String',handles.CurrentDirectory);
set( handles.textNumFan,'String',num2str(moGetNumberOfPingFan()));


% --- Executes on selection change in ChannelList.
function ChannelList_Callback(hObject, eventdata, handles)
% hObject    handle to ChannelList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns ChannelList contents as cell array
%        contents{get(hObject,'Value')} returns selected item from ChannelList


% --- Executes during object creation, after setting all properties.
function ChannelList_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ChannelList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in ViewChannel.
function ViewChannel_Callback(hObject, eventdata, handles)
% hObject    handle to ViewChannel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

Str=get(handles.ChannelList,'Value');
Coord=handles.ValuesChannelId(Str);


IdxCoord=get(handles.popupmenuCoordChoice,'Value');
T=get(handles.popupmenuCoordChoice,'String');
CoordString=cell2mat(T(IdxCoord,:));

ViewEchoGram(Coord.sounderId,Coord.transIndex,Coord.chanIndex,CoordString);


% --- Executes on selection change in popupmenuCoordChoice.
function popupmenuCoordChoice_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenuCoordChoice (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenuCoordChoice contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenuCoordChoice


% --- Executes during object creation, after setting all properties.
function popupmenuCoordChoice_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenuCoordChoice (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit4 as text
%        str2double(get(hObject,'String')) returns contents of edit4 as a double


% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit5_Callback(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit5 as text
%        str2double(get(hObject,'String')) returns contents of edit5 as a double


% --- Executes during object creation, after setting all properties.
function edit5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit6_Callback(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit6 as text
%        str2double(get(hObject,'String')) returns contents of edit6 as a double


% --- Executes during object creation, after setting all properties.
function edit6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbuttonErasePolarCoord.
function pushbuttonErasePolarCoord_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonErasePolarCoord (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

minValue=get(handles.edit4,'String');
maxValue=get(handles.edit5,'String');
sounderId=get(handles.edit6,'String');
ErasePolarMem(str2num(sounderId),str2num(minValue),str2num(maxValue));


function EditThresholdMin_Callback(hObject, eventdata, handles)
% hObject    handle to EditThresholdMin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of EditThresholdMin as text
%        str2double(get(hObject,'String')) returns contents of EditThresholdMin as a double


% --- Executes during object creation, after setting all properties.
function EditThresholdMin_CreateFcn(hObject, eventdata, handles)
% hObject    handle to EditThresholdMin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbuttonThreshold.
function pushbuttonThreshold_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonThreshold (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

ThresholdValue=get(handles.EditThresholdMin,'String');
ThresholdValue=str2num(ThresholdValue);
EchoThreshold(ThresholdValue);


% --- Executes on button press in pushbuttonEraseChannelData.
function pushbuttonEraseChannelData_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonEraseChannelData (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Str=get(handles.ChannelList,'Value');
Coord=handles.ValuesChannelId(Str);

EraseChannelData(Coord.sounderId,Coord.transIndex,Coord.chanIndex);


% --- Executes on button press in pushbutton7.
function pushbutton7_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

AutoEraseMulti(15);
