
function OverlapMemoryThreshold(ThresholdValue,MasterSounderId)
NumPingFan =moGetNumberOfPingFan();
S=sprintf('Thresholding memory with overlap > than %d',ThresholdValue);
WaitBar= waitbar(0,S);
% MyMax=1160
for i=0:NumPingFan-1
    waitbar(i/NumPingFan);
    PingFan=moGetPingFan(i);
	PingId=PingFan.m_pingId;
    fanSounder = moGetFanSounder(PingId);
    if fanSounder.m_SounderId~=MasterSounderId
        for transducerIndex=0:fanSounder.m_numberOfTransducer-1;
            
            PolarMatrix=moGetPolarMatrix(PingId,transducerIndex);
            for softChannelIdx=0:fanSounder.m_transducer(1,transducerIndex+1).m_numberOfSoftChannel-1;
                MyMatrixOverLap=  PolarMatrix.m_Overlap(:,softChannelIdx+1);
                MyMatrixEcho=PolarMatrix.m_Amplitude(:,softChannelIdx+1);
%                 MyMatrixOverLapBool = (MyMatrixOverLap==-1); 
%                 MyMatrixEcho(  MyMatrixOverLapBool) = -12000;
                MyMatrixOverLapBool =( MyMatrixOverLap >= ThresholdValue);
                MyMatrixEcho( ~ MyMatrixOverLapBool) = -12000;
                PolarMatrix.m_Amplitude(:,softChannelIdx+1)=MyMatrixEcho;
            end;
            moSetPolarMatrix(PingId,transducerIndex,PolarMatrix);
        end;
    end;
         

end
close(WaitBar);
end