function RunCompensation(RefThreshold,DataThreshold,nbPings)

T=moCompensation;
a=moGetCompensationParameter(T);
MyMax =moGetNumberOfPingFan();
a.m_spaceXYAngleFactor=0.2;
a.m_referenceWindowsPingCount=nbPings;
a.m_moduleReferenceThreshold=RefThreshold;
a.m_moduleDataThreshold=DataThreshold;
WaitBar= waitbar(0,'Computing Overlap...');
% MyMax=10702
for i=0:MyMax
    waitbar(i/MyMax);
    a.m_pingFilteredStart=i;
    a.m_pingFilteredStop=i+1;

    a.m_bIsEnable=1;
    moSetCompensationParameter(T,a);
    moRunOverLapFilter(T);
    fprintf('PingFan number %d over %d\n',i,MyMax);
    drawnow
end;
close(WaitBar);