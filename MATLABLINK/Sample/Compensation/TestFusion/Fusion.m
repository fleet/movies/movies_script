clear all;
cd C:\data\Simulation\20maquereauWithAngle
mkdir('C:\data\Simulation\20maquereauWithAngle\REWRITED');
mkdir('C:\data\Simulation\20maquereauWithAngle\REWRITED\ALL');

% h=msgbox('Please Compile with MOVE_CHANNEL_SOUNDER_ID defined in BackWardConst.h','modal');
% uiwait(h);
moReadWholeFile('20maquereau_monos_070508.hac');

moStartWrite('C:\data\Simulation\20maquereauWithAngle\REWRITED','REWRITED_');
moStopWrite

clear all

% h=msgbox('Please Compile with MOVE_CHANNEL_SOUNDER_ID NOT defined in BackWardConst.h','modal');
% uiwait(h);

moReadWholeFile('20maquereau_multi_130508.hac');
moStartWrite('C:\data\Simulation\20maquereauWithAngle\REWRITED','REWRITED_');
moStopWrite

clear all


cd REWRITED\
moOpenHac('C:\data\Simulation\20maquereauWithAngle\REWRITED');
Parameter= moReaderParameter();
ParameterDef=moLoadStruct(Parameter);
ParameterDef.m_bIgnorePhaseData=1
ParameterDef.m_bAutoLengthEnable=1;
ParameterDef.m_bAutoDepthEnable=1;
ParameterDef.m_sortDataBeforeWrite=1
moSaveStruct(Parameter,ParameterDef);
FileStatus= moGetFileStatus;
while FileStatus.m_StreamClosed < 1
moReadChunk();
FileStatus= moGetFileStatus();
end;
moStartWrite('C:\data\Simulation\20maquereauWithAngle\REWRITED\ALL','ALL_');
moStopWrite

clear all
