clear all;
cd C:\data\OVERLAP\Simulation\60maquereauAligned
mkdir('C:\data\OVERLAP\Simulation\60maquereauAligned\REWRITED\BASE');
mkdir('C:\data\OVERLAP\Simulation\60maquereauAligned\REWRITED\BASE\ALL');

% h=msgbox('Please Compile with MOVE_CHANNEL_SOUNDER_ID defined in BackWardConst.h','modal');
% uiwait(h);
moReadWholeFile('60maquereau_monos_aligned_151008.hac');

moStartWrite('C:\data\OVERLAP\Simulation\60maquereauAligned\REWRITED\BASE','REWRITED_');
moStopWrite

clear all

% h=msgbox('Please Compile with MOVE_CHANNEL_SOUNDER_ID NOT defined in BackWardConst.h','modal');
% uiwait(h);

moReadWholeFile('60maquereau_multi_151008.hac');
moStartWrite('C:\data\OVERLAP\Simulation\60maquereauAligned\REWRITED\BASE','REWRITED_');
moStopWrite

clear all

% Initialisation des param�tres hacreader
% Pour MultiFaisceau (peut �tre diff�rent vue la taille de ce qu'il y a �
% stocker...)

% define our chunk
% we define a chunk of  ping for the given sounderId
% initialisation de la config
ParameterKernel= moKernelParameter();
ParameterDef=moLoadKernelParameter(ParameterKernel);

%we read all the file
ParameterDef.m_bAutoLengthEnable  = 1;

% set the max number of pingFan we keep in memory
%ParameterDef.m_nbPingFanMax=2;
%TEST SEB
ParameterDef.m_nbPingFanMax = 1000;

ParameterDef.m_bIgnorePhaseData=0;

% Or set the max sample of pingFan to auto mode, ie we will increase the
% number of sample depending of the detected ground
%ParameterDef.m_bAutoDepthEnable = 0;
ParameterDef.m_bAutoDepthEnable = 1;
ParameterDef.m_MaxRange = 250;
ParameterDef.m_bAutoDepthTolerance = 20;
ParameterDef.m_sortDataBeforeWrite=1;

moSaveKernelParameter(ParameterKernel,ParameterDef);
clear ParameterKernel;
clear ParameterDef;

ParameterR= moReaderParameter();
ParameterDef=moLoadReaderParameter(ParameterR);

% we want to read pingFan by block of 1
ParameterDef.m_chunkNumberOfPingFan=1;
% we only care about this sounder
%ParameterDef.m_chunkSounderId=SounderId;
% apply our settings
moSaveReaderParameter(ParameterR,ParameterDef);

clear ParameterR;
clear ParameterDef;

% on filtre zone aveugle et fond
paramFilter = moGetFilterModuleParameter(moFilterModule);
paramFilter.SubFilterState_1 = 1;
paramFilter.SubFilterState_4 = 1;
paramFilter.SubFilterState_5 = 1;
moSetFilterModuleParameter(moFilterModule,paramFilter);


cd REWRITED\BASE
moOpenHac('C:\data\OVERLAP\Simulation\60maquereauAligned\REWRITED\BASE');


FileStatus= moGetFileStatus;
while FileStatus.m_StreamClosed < 1
moReadChunk();
FileStatus= moGetFileStatus();
end;
moStartWrite('C:\data\OVERLAP\Simulation\60maquereauAligned\REWRITED\BASE\ALL','ALL_');
moStopWrite

clear all
