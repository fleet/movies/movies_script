 Parameter= moReaderParameter();
    ParameterDef=moLoadStruct(Parameter);

    ParameterDef.m_nbPingFanMax=5
    ParameterDef.m_bAutoLengthEnable=1;
    ParameterDef.m_bAutoDepthEnable=1;

    % apply our settings
    moSaveStruct(Parameter,ParameterDef);

moOpenHac('C:\data\OVERLAP\EXACHA08\EXACHA08_003_20080319_094238.hac');

moReadChunk
%fill pingFan
WaitBar= waitbar(0,'Filling polar memory area...');
for numPingFan=0:moGetNumberOfPingFan()-1
    waitbar(numPingFan/moGetNumberOfPingFan());
    PingFan=moGetPingFan(numPingFan);
    PingId=PingFan.m_pingId;
    fanSounder = moGetFanSounder(PingId);
    for transducerIndex=0:fanSounder.m_numberOfTransducer-1
        PolarMatrix=moGetPolarMatrix(PingId,transducerIndex);
        PolarMatrix.m_Amplitude(:,:)=-6000;
        moSetPolarMatrix(PingId,transducerIndex,PolarMatrix);
    end;

end;
close(WaitBar);
for i=0:4
ViewEchoGram(1, i,0,'polar coord')
end
