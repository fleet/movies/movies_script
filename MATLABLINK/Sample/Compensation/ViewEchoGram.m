function ViewEchoGram(sounderId, transducerIndex,softChannelIdx,CoordString)

% just print the number of ping fan
Sounder=moGetSounder(sounderId);
Transducer =Sounder.m_transducer(1,transducerIndex+1);
Channel=Transducer.m_SoftChannel(1,softChannelIdx+1)
UsePolar=0;
ShowOverLapMemory=0;
UseOverLapMemoryAsMask=0;
switch lower(CoordString)
    case 'polar coord'
        UsePolar=1;
    case 'screen 	coord'
        UsePolar=0;
    case 'overlap memory'
        ShowOverLapMemory=1;
    case 'overlap memory'
        ShowOverLapMemory=1;
    case 'polar masked with overlap'
        UseOverLapMemoryAsMask=1;
        UsePolar=1;
end
WaitBar= waitbar(0,'building View matrix...');
DisplayFanNumber();
Matrix=[];
IndexAdded=0;
for numPingFan=0:moGetNumberOfPingFan()-1
  
    waitbar(numPingFan/moGetNumberOfPingFan());
    PingFan=moGetPingFan(numPingFan);
      PingId=PingFan.m_pingId;
       fanSounder = moGetFanSounder(PingId);
       if fanSounder.m_SounderId==sounderId
           
%            
%            
        if ShowOverLapMemory==1
            PolarMatrix=moGetPolarMatrix(PingId,transducerIndex);
            MyMatrix=  PolarMatrix.m_Overlap(:,softChannelIdx+1);
        else
            if UsePolar==0
                ScreenMatrix=moGetScreenMatrix(PingId,transducerIndex);
                CenterMatrix=    ceil(size(ScreenMatrix,2)/2);
                MyMatrix=ScreenMatrix(:,CenterMatrix);
               
            else
                PolarMatrix=moGetPolarMatrix(PingId,transducerIndex);
                MyMatrix=  PolarMatrix.m_Amplitude(:,softChannelIdx+1);
                if UseOverLapMemoryAsMask==1
                    MyMatrix = MyMatrix .*  (PolarMatrix.m_Overlap(:,softChannelIdx+1)>0);
                end;
            end;
        end;
        if size(Matrix,1) == 0
            sizeX=size(MyMatrix,1);
            sizeY=int32(moGetNumberOfPingFan());
            Matrix=zeros(sizeX,sizeY);
        end
%         fprintf('%d : %d\n',PingId,IndexAdded);
        IndexAdded=IndexAdded+1;
        
        Matrix(:,IndexAdded)=MyMatrix;
         

       end
   
end;
close(WaitBar);
h=figure();
if (ShowOverLapMemory==1)
    sampleSpacing=fanSounder.m_transducer(1,1).m_beamsSamplesSpacing;
    [dimX dimY] = size(Matrix);
    y=[35:sampleSpacing:35+dimX*sampleSpacing];
    x=[1:1:dimY];
    imagesc(x,y,Matrix(:,1:IndexAdded+1));
    xlabel('Ping number');
    title('Percentage of overlap for 38 kHz');
ylabel('Depth (m)');
else
    DrawEchoGram(Matrix(:,1:IndexAdded+1),0,-6000);
    title(Channel.m_channelName);
end





% just print the number of ping fan
function DisplayFanNumber()
    num=moGetNumberOfPingFan();
    fprintf('[%d] ping Fan\n', num);



