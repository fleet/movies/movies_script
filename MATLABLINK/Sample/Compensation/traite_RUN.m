% Lance le traitement de tous les fichiers contenus dans un repertoire donne 


clear all;


%% Partie "UTILISATEUR"

% Repertoire a traiter 
chemin_ini = 'C:\data\OVERLAP\Simulation\60maquereauAligned\REWRITED\BASE\ALL\';



%% Initialisation


% Initialisation des param�tres hacreader
% Pour MultiFaisceau (peut �tre diff�rent vue la taille de ce qu'il y a �
% stocker...)

% define our chunk
% we define a chunk of  ping for the given sounderId
% initialisation de la config
ParameterKernel= moKernelParameter();
ParameterDef=moLoadKernelParameter(ParameterKernel);

%we read all the file
ParameterDef.m_bAutoLengthEnable  = 1;

% set the max number of pingFan we keep in memory
%ParameterDef.m_nbPingFanMax=2;
%TEST SEB
ParameterDef.m_nbPingFanMax = 1000;

ParameterDef.m_bIgnorePhaseData=0;

% Or set the max sample of pingFan to auto mode, ie we will increase the
% number of sample depending of the detected ground
%ParameterDef.m_bAutoDepthEnable = 0;
ParameterDef.m_bAutoDepthEnable = 1;
ParameterDef.m_MaxRange = 250;
ParameterDef.m_bAutoDepthTolerance = 20;

moSaveKernelParameter(ParameterKernel,ParameterDef);
clear ParameterKernel;
clear ParameterDef;

ParameterR= moReaderParameter();
ParameterDef=moLoadReaderParameter(ParameterR);

% we want to read pingFan by block of 1
ParameterDef.m_chunkNumberOfPingFan=1;
% we only care about this sounder
%ParameterDef.m_chunkSounderId=SounderId;
% apply our settings
moSaveReaderParameter(ParameterR,ParameterDef);

clear ParameterR;
clear ParameterDef;

% on filtre zone aveugle et fond
paramFilter = moGetFilterModuleParameter(moFilterModule);
paramFilter.SubFilterState_1 = 1;
paramFilter.SubFilterState_4 = 1;
paramFilter.SubFilterState_5 = 1;
moSetFilterModuleParameter(moFilterModule,paramFilter);



%% Traitement
filelist = ls([chemin_ini,'*.hac']);  % ensemble des fichiers
nb_files = size(filelist,1);  % nombre de fichiers
for numfile = 1:nb_files  % boucle sur l'ensemble des fichiers � traiter
    hacfilename = filelist(numfile,:);
    filename = [chemin_ini,hacfilename];
    
    moReadWholeFile(filename);
    
    EchoThreshold(-80);
    RunCompensation(-80,-80,5);
    
    FilterAndSave(chemin_ini);
    
end
