function modif_rawEK60_absorption(directory,filename)
%function modif_rawEK60_absorption(itest)
% fonction qui lit un fichier de donn�es *.raw de l'EA600
% et qui modifie la valeur de l'absorption pour chaque ping
% cf. pb mauvaise config HERMES PELGAS16

HeaderLength=12; % dur�e de DgHeader

fname=[directory filename];
fname2=[fname(1:length(fname)-4) '_cor.raw'];
% fname=[fname(1:length(fname)-4) '_modif.raw'];

fid=fopen(fname,'rb','ieee-le');
if (fid==-1), disp(['Le fichier ' fname ' n''existe pas']);return;end

fid2=fopen(fname2,'wb','ieee-le');

%---- configuration datagram ----------------------------------------------
Length  = fread(fid,1,'int32'); fwrite(fid2,Length,'int32');
tmp     = fread(fid,Length,'int8'); fwrite(fid2,tmp,'int8');
Length  = fread(fid,1,'int32'); fwrite(fid2,Length,'int32');

%---- lecture des datagrams suivants -----------------------------
while (1)
    Length = fread(fid,1,'int32'); 
    if (feof(fid)),  break; end
    tmp1 = fread(fid,HeaderLength,'int8');
    
    char(tmp1(1:4)')
    
    if tmp1(1:4)'==double('RAW0')
        fwrite(fid2,Length,'int32');
        fwrite(fid2,tmp1,'int8');
        readandcorrectsampledata(fid,fid2);
        Length2 = fread(fid,1,'int32');
        fwrite(fid2,Length2,'int32');
    else
        fwrite(fid2,Length,'int32');
        fwrite(fid2,tmp1,'int8');
        tmp2 = fread(fid,Length-HeaderLength,'int8');
        Length2 = fread(fid,1,'int32');
        fwrite(fid2,tmp2,'int8');
        fwrite(fid2,Length2,'int32');
    end
end

fclose(fid); fclose(fid2);

