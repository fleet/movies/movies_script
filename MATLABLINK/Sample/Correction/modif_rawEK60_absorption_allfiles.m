% appel la modification de l'absorption sur l'ensemble des fichiers raw d'un
% répertoire

dbstop if error

fpath='C:\Users\lberger\Desktop\PELGAS16-D20160502-T071030\';
filelist = squeeze(dir(fullfile(fpath,'*.raw')));  % all files
nb_files = size(filelist,1);  % number of files

for numfile = 1:nb_files
    
    rawfilename = filelist(numfile,:).name;
    FileName = [fpath,rawfilename];
    modif_rawEK60_absorption(fpath,rawfilename)
end