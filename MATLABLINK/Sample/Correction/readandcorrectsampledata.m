% Reading EK60 raw data file sample data
% Simrad, Lars Nonboe Andersen, 12/04-02

function sampledata = readancorrectsampledata(fid,fid2)

sampledata.channel = fread(fid,1,'int16');
fwrite(fid2,sampledata.channel,'int16');
mode_low = fread(fid,1,'int8');
mode_high = fread(fid,1,'int8');
sampledata.mode = 256*mode_high + mode_low;
fwrite(fid2,mode_low,'int8');
fwrite(fid2,mode_high,'int8');
tmp2=fread(fid,7,'float32');
fwrite(fid2,tmp2,'float32');
sampledata.absorptioncoefficient = fread(fid,1,'float32');
if sampledata.channel==1
    fwrite(fid2,2.5/1000,'float32');
elseif sampledata.channel==2
    fwrite(fid2,9.5/1000,'float32');
elseif sampledata.channel==3
    fwrite(fid2,23.5/1000,'float32');
elseif sampledata.channel==4
    fwrite(fid2,41.1/1000,'float32');
elseif sampledata.channel==5
    fwrite(fid2,58.8/1000,'float32');
elseif sampledata.channel==6
    fwrite(fid2,84.1/1000,'float32');
end


tmp3=fread(fid,4,'float32');
fwrite(fid2,tmp3,'float32');

tmp4=fread(fid,2,'int16');
fwrite(fid2,tmp4,'int16');

tmp5=fread(fid,2,'float32');
fwrite(fid2,tmp5,'float32');

sampledata.offset = fread(fid,1,'int32');
fwrite(fid2,sampledata.offset,'int32');
sampledata.count = fread(fid,1,'int32');
fwrite(fid2,sampledata.count,'int32');

power = fread(fid,sampledata.count,'int16');
%on ne touche pas aux �chos
fwrite(fid2,power,'int16');
if (sampledata.mode>1)
    angle = fread(fid,[2 sampledata.count],'int8');
    fwrite(fid2,angle,'int8');
end


