function [RawData] = ReadEK80RawFile(fileName)
%READEK80RAWFILE Reads an EK80 raw data file
%
% CALL: [RawData] = ReadEK80RawFile(fileName)   
%
% Inputs:
%   fileName = 
%
% Outputs:
%   RawData  = 
%
% Description:
%
%
%
% Examples(s):
%   [RawData] = ReadEK80RawFile(fileName)
%

% References:
%
%
% Created by Lars Nonboe Andersen
%
%
%
% Copyright (c) 2015 Kongsberg Maritime
%
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
% THE SOFTWARE.
% ---------------------------------------------------------------------------

narginchk(1,1);
nargoutchk(0,1);

fileId = fopen(fileName,'r');

headerLength = 12; % Bytes in datagram header

pingNumber      = 0;
pingTime        = 0;
EnvironmentData = struct;
FilterData      = struct;
MotionData      = struct;
%nlb adding position
NMEAData      = struct;
heading=[];
speed_knt=[];
time_VTG=[];
latGLL=[];
longGLL=[];
timeNMEAGLL=[];
timeNMEAHeaderGLL=[];
latGGA=[];
longGGA=[];
timeNMEAGGA=[];
timeNMEAHeaderGGA=[];
% choose preferential type
%ChoiceTypNMEA='GLL'; 
ChoiceTypNMEA='GGA';

if (fileId==-1)
    error(['Could not open file: ' fileName]);
else

    while(1 & pingNumber<100)
    %while(1)
        datagramLength = fread(fileId,1,'int32');
        
        if (feof(fileId))
            break
        end
        
        DatagramHeader = ReadDatagramHeader(fileId);
%         disp(DatagramHeader.type)
        switch (DatagramHeader.type)
            
            case 'XML0' % XML datagram
                XmlDataUnparsed = ReadXmlData(fileId,datagramLength-headerLength);
                XmlData = ParseXmlData(XmlDataUnparsed);
%                 disp(['    ' XmlData.Name])
                switch (XmlData.Name)
                    case 'Configuration' % Configuration XML data
                        ConfigurationData           = ParseConfigurationXmlData(XmlData);
                        RawData.ConfigurationData   = ConfigurationData;
                        
                    case 'Environment' % Environment XML data
                        EnvironmentData = ParseEnvironmentXmlData(XmlData,EnvironmentData);
                        
                    case 'Parameter' % Sampledata parameter data
                        ParameterData = ParseParameterXmlData(XmlData,ConfigurationData.Header.FileFormatVersion);
                        
                    case 'InitialParameter' % Sampledata parameter data
                        'InitialParameter'
                        XmlData.Children.Children=XmlData.Children.Children(1)
                        ParameterData = ParseParameterXmlData(XmlData.Children,ConfigurationData.Header.FileFormatVersion);

                    otherwise
                        %error(['Unknown XML datagram in file: ' fileName]);
%                        disp(['Unknown XmlData.Name datagram in XML0: ' XmlData.Name]);
                end
                
            case 'FIL1' % Filter datagram
                FilterData = ReadFilterData(fileId);

                for ichannel=1:size(ConfigurationData.Transceivers,2) %ajout nlb for mix GPT/WBT case
                    if isfield(ConfigurationData.Transceivers(ichannel).Channels,'SampleInterval')
                        ConfigurationData.Transceivers(ichannel).Channels=rmfield(ConfigurationData.Transceivers(ichannel).Channels,'SampleInterval');
                    end
                      if isfield(ConfigurationData.Transceivers(ichannel).Channels,'PulseDurationFM')
                        ConfigurationData.Transceivers(ichannel).Channels=rmfield(ConfigurationData.Transceivers(ichannel).Channels,'PulseDurationFM');
                    end
                end
                Channels        = [ConfigurationData.Transceivers(:).Channels];
                channelNumber   = strcmp(deblank(FilterData.channelId),{Channels.ChannelID});
                
                FilterDataVector(channelNumber,FilterData.stage) = FilterData;
                
                RawData.FilterData = FilterDataVector;
                
            case 'NME0' % NMEA datagram
                NmeaData = ReadTextData(fileId,datagramLength-headerLength);
                %nlb : position reading
                if NmeaData.text(4:6)=='GLL'
%                     latGLL(end+1)=str2num(NmeaData.text(8:9))+str2num(NmeaData.text(10:17))/60;
%                     longGLL(end+1)=-(str2num(NmeaData.text(22:23))+str2num(NmeaData.text(24:31))/60);
%                     timeNMEAGLL(end+1)=str2num(NmeaData.text(35:43));
%                     timeNMEAHeaderGLL(end+1)=DatagramHeader.dateTime;
%                     latGLL(end+1)=0;
%                     longGLL(end+1)=0;
%                     timeNMEAGLL(end+1)=0;
%                     timeNMEAHeaderGLL(end+1)=0;
                end
                if NmeaData.text(4:6)=='GGA'
%                    lat_rec=str2num(NmeaData.text(18:27))/100;
%                     %long_rec=str2num(NmeaData.text(31:41))/100;
%                     long_rec=str2num(NmeaData.text(34:41))/100;
%                     latGGA(end+1)=floor(lat_rec)+(lat_rec-floor(lat_rec))*100/60; %convert minutes to decimal
%                     longGGA(end+1)=floor(long_rec)+(long_rec-floor(long_rec))*100/60;
%                     ind_sep=strfind(NmeaData.text,',');
%                     %heading(end+1)=str2num(NmeaData.text(ind_sep(9)+1:ind_sep(10)-1)); 
%                     timeNMEAGGA(end+1)=str2num(NmeaData.text(8:16));
%                     timeNMEAHeaderGGA(end+1)=DatagramHeader.dateTime;
                end
                if NmeaData.text(4:6)=='VTG'
                    time_VTG(end+1)=DatagramHeader.dateTime;
                    %heading(end+1)=str2num(NmeaData.text(8:13)); %true
                    ind_sep=strfind(NmeaData.text,',');
                    if ind_sep(4)>ind_sep(3)+1
                        heading(end+1)=str2num(NmeaData.text(ind_sep(3)+1:ind_sep(4)-1)); %magnetic
                    else
                        heading(end+1)=nan;
                    end
                    speed_knt(end+1)=str2num(NmeaData.text(ind_sep(5)+1:ind_sep(6)-1));
                end
                
            case 'TAG0' % Annotation datagram
                AnnotationData = ReadTextData(fileId,datagramLength-headerLength);
                
            case 'MRU0' % Motion data
                MotionData = ReadMotionData(fileId);
            case 'MRU1' % Motion data
                MotionData = ReadMotionData1(fileId);
                
            case 'RAW3' % Sample datagram
                if (DatagramHeader.dateTime~=pingTime)
                    pingTime    = DatagramHeader.dateTime;
                    pingNumber  = pingNumber+1;
                end
                
                SampleData = ReadSampleData(fileId,DatagramHeader.type);

                Channels        = [ConfigurationData.Transceivers(:).Channels];
                channelNumber   = strcmp(deblank(SampleData.channelId),deblank({Channels.ChannelID}));

                if ~isfield(SampleData,'power') %not GPT case
                    % Extract current samples from LSB of voltage samples
                    currentData             = ExtractCurrentData(FilterDataVector(channelNumber,:),ParameterData,SampleData);
                    SampleData.currentData  = currentData;
                end

                RawData.PingData(channelNumber,pingNumber).time            = pingTime;
                RawData.PingData(channelNumber,pingNumber).fileName        = fileName;
                RawData.PingData(channelNumber,pingNumber).EnvironmentData = EnvironmentData;
                RawData.PingData(channelNumber,pingNumber).MotionData      = MotionData;
                RawData.PingData(channelNumber,pingNumber).NMEAData        = NMEAData;
                %if ~isfield(SampleData,'power') %not GPT case
                %if exist('FilterDataVector') %not GPT case
                if ~strcmp(SampleData.channelId(1:3),'GPT') %not GPT case
                    RawData.PingData(channelNumber,pingNumber).FilterData  = FilterDataVector(channelNumber,:);
                else
                    RawData.PingData(channelNumber,pingNumber).FilterData  =[];
                end
                RawData.PingData(channelNumber,pingNumber).ParameterData   = ParameterData;
                RawData.PingData(channelNumber,pingNumber).SampleData      = SampleData;
                
                %RawData.PingData(channelNumber,pingNumber) = PingData;
                
                %nlb
                timePing(channelNumber,pingNumber) = pingTime;
                
            case 'RAW4' % Transmit Sample datagram
                if (DatagramHeader.dateTime~=pingTime)
                    pingTime    = DatagramHeader.dateTime;
                    pingNumber  = pingNumber+1;
                end
                TransmitSampleData = ReadTransmitSampleData(fileId,DatagramHeader.type);
            
                Channels        = [ConfigurationData.Transceivers(:).Channels];
                channelNumber   = strcmp(deblank(TransmitSampleData.channelId),deblank({Channels.ChannelID}));
                
                RawData.PingData(channelNumber,pingNumber).TransmitSampleData = TransmitSampleData;
                
                %nlb
                timePing(channelNumber,pingNumber) = pingTime;
                
        case 'CON0' % header EK60
            nrefbeams = 0;
            % Read configuration datagram
            configheader = readconfigheader(fileId);
            for i=1:configheader.transducercount,
                configtransducer(i) = readconfigtransducer(fileId);
                if ~isempty(findstr(configtransducer(i).channelid,'Reference'))
                    nrefbeams = nrefbeams+1;
                end
            end
            config = struct('header',configheader,'transducer',configtransducer);
            config.header.nrefbeams = nrefbeams;
            
            % EK80 Format
            ConfigurationData.Header=config.header; %keep EK60 format for header
            for itr=1:length(config.transducer)
                isep=strfind(config.transducer(itr).channelid,' ');
                Transceivers(itr).id='Transceiver';
                Transceivers(itr).EthernetAddress=config.transducer(itr).channelid(isep(3)+1:isep(4)-1);
                Transceivers(itr).IPAddress='';
                Transceivers(itr).MarketSegment= 'Scientific';
                Transceivers(itr).SerialNumber=[];
                Transceivers(itr).TransceiverName=config.transducer(itr).channelid(1:isep(4)-1);
                Transceivers(itr).TransceiverNumber=[];
                Transceivers(itr).TransceiverSoftwareVersion=[];
                Transceivers(itr).TransceiverType='GPT';
                Transceivers(itr).Version='';
                
                    %one channel case for the moment
                    Channel.Name='Channel';
                    Channel.ChannelID=config.transducer(itr).channelid;
                    Channel.ChannelIdShort='';
                    Channel.HWChannelConfiguration='';
                    Channel.MaxTxPowerTransceiver='';
                    Channel.PulseDuration=num2str(config.transducer(itr).pulselengthtable);
                    Channel.SampleInterval=config.transducer(itr).pulselengthtable/4;
                        Transducer.AngleOffsetAlongship=num2str(config.transducer(itr).angleoffsetalongship);
                        Transducer.AngleOffsetAthwartship=num2str(config.transducer(itr).angleoffsetathwartship);
                        Transducer.AngleSensitivityAlongship=num2str(config.transducer(itr).anglesensitivityalongship);
                        Transducer.AngleSensitivityAthwartship=num2str(config.transducer(itr).anglesensitivityathwartship);
                        Transducer.BeamType=config.transducer(itr).beamtype;
                        Transducer.BeamWidthAlongship=num2str(config.transducer(itr).beamwidthalongship);
                        Transducer.BeamWidthAthwartship=num2str(config.transducer(itr).beamwidthathwartship);
                        Transducer.DirectivityDropAt2XBeamWidth=[];
                        Transducer.EquivalentBeamAngle=num2str(config.transducer(itr).equivalentbeamangle);
                        Transducer.Frequency=num2str(config.transducer(itr).frequency);
                        Transducer.FrequencyMaximum=num2str(config.transducer(itr).frequency);
                        Transducer.FrequencyMinimum=num2str(config.transducer(itr).frequency);
                        Transducer.Gain=num2str(config.transducer(itr).gaintable);
                        Transducer.MaxTxPowerTransducer=[];
                        Transducer.SaCorrection=num2str(config.transducer(itr).sacorrectiontable);
                        Transducer.SerialNumber=[];
                        Transducer.TransducerName=config.transducer(itr).channelid(isep(5)+1:end);
                    Channel.Transducer=Transducer;
                Transceivers(itr).Channels=Channel;
            end
            ConfigurationData.Transceivers=Transceivers;
            RawData.ConfigurationData   = ConfigurationData;
            
            EmptyFilter.stage=[];
            EmptyFilter.channelNumber=[];
            EmptyFilter.channelId=[];
            EmptyFilter.nCoefficients=[];
            EmptyFilter.decimationFactor=[];
            EmptyFilter.coefficients=[];
            RawData.FilterData(1:length(config.transducer),1:2)=EmptyFilter;
                       
        case 'CON1' % SMS extra configuration datagram EK60
            text = readtextdata(fileId,length-headerlength);
            
        case 'RAW0' % Sample datagram EK60
        sampledata = readsampledata(fileId);

        % WRITE YOUR OWN CODE HERE TO PROCESS AND/OR DISPLAY SAMPLE DATA
        channel = sampledata.channel;
        %tvgsampledata = applytvg(config,sampledata);
             
        if (DatagramHeader.dateTime~=pingTime)
           pingTime    = DatagramHeader.dateTime;
           pingNumber  = pingNumber+1;
        end

        SampleData.channelId=Transceivers(channel).Channels.ChannelID;
        SampleData.modeLow=[];
        SampleData.modeHigh=[];
        SampleData.mode=sampledata.mode;
        SampleData.spare1=[];
        SampleData.offset=sampledata.offset;
        SampleData.count=sampledata.count;
        SampleData.power=sampledata.power;
        SampleData.angle=sampledata.angle;
        SampleData.angleAlongship=sampledata.alongship;
        SampleData.angleAthwartship=sampledata.athwartship;
        
        Channels        = [ConfigurationData.Transceivers(:).Channels];
        channelNumber   = strcmp(deblank(SampleData.channelId),deblank({Channels.ChannelID}));
        
        EnvironmentData.temperature=sampledata.temperature;
        EnvironmentData.salinity=[];
        EnvironmentData.depth=sampledata.transducerdepth;
        EnvironmentData.acidity=[];
        EnvironmentData.soundSpeed=sampledata.soundvelocity;
        EnvironmentData.absorptioncoefficient=sampledata.absorptioncoefficient;
        
        MotionData.heave=sampledata.heave;
        MotionData.roll=sampledata.roll;
        MotionData.pitch=sampledata.pitch;
        MotionData.heading=[];
        
        ParameterData.channelId= [];
        ParameterData.channelMode=[];
        ParameterData.transmitPower=sampledata.transmitpower;
        ParameterData.pulseForm= 0;
        ParameterData.pulseLength=sampledata.pulselength;
        ParameterData.fileFormatVersion=[];
        ParameterData.slope= 0;
        ParameterData.frequencyStart=sampledata.frequency;
        ParameterData.frequencyStop=sampledata.frequency;
        ParameterData.sampleInterval=sampledata.sampleinterval;
        ParameterData.transducerDepth=sampledata.transducerdepth;
        ParameterData.frequencyUsed=sampledata.frequency;
        
        PingData.time               = pingTime;
        PingData.fileName           = fileName;
        PingData.EnvironmentData    = EnvironmentData;
        PingData.MotionData         = MotionData;
        PingData.NMEAData           = NMEAData;
        if ~isfield(SampleData,'power') %not GPT case
            PingData.FilterData         = FilterDataVector(channel,:);
        else
            PingData.FilterData         =[];
        end
        PingData.ParameterData      = ParameterData;
        PingData.SampleData         = SampleData;
        RawData.PingData(channel,pingNumber) = PingData;

        %nlb
        timePing(channelNumber,pingNumber) = pingTime;
         
        otherwise
                error(strcat('Unknown datagram ''',DatagramHeader.type,''' in file'));
        end
        
        datagramLength = fread(fileId,1,'int32');
        
    end
    
    fclose(fileId);
end


for channelNumber=1:size(RawData.PingData,1)
    ind_ok=find(timePing(channelNumber,:)>0);
    if ~isempty(ind_ok) 
       if NmeaData.text(4:6)=='GLL' & strcmp(ChoiceTypNMEA,'GLL')
            if DatagramHeader.dateTime>timeNMEAHeaderGLL(end)
                latGLL(end+1)=str2num(NmeaData.text(8:9))+str2num(NmeaData.text(10:17))/60;
                longGLL(end+1)=-(str2num(NmeaData.text(22:23))+str2num(NmeaData.text(24:31))/60);
                timeNMEAGLL(end+1)=str2num(NmeaData.text(35:43));
                timeNMEAHeaderGLL(end+1)=DatagramHeader.dateTime;
            end
        end
        if NmeaData.text(4:6)=='GGA' & strcmp(ChoiceTypNMEA,'GGA') 
            if DatagramHeader.dateTime>timeNMEAHeaderGGA(end)
                lat_rec=str2num(NmeaData.text(18:27))/100;
                long_rec=str2num(NmeaData.text(31:41))/100;
                latGGA(end+1)=floor(lat_rec)+(lat_rec-floor(lat_rec))*100/60; %convert minutes to decimal
                longGGA(end+1)=floor(long_rec)+(long_rec-floor(long_rec))*100/60;
                timeNMEAGGA(end+1)=str2num(NmeaData.text(8:16));
                timeNMEAHeaderGGA(end+1)=DatagramHeader.dateTime;
            end
        end
        if ~isempty(timeNMEAHeaderGGA) & strcmp(ChoiceTypNMEA,'GGA') | isempty(timeNMEAHeaderGLL)
            lat=latGGA;
            long=longGGA;
            timeNMEA=timeNMEAGGA;
            timeNMEAHeader=timeNMEAHeaderGGA;
            typNMEA='GGA';
        end
        if ~isempty(timeNMEAHeaderGLL) & strcmp(ChoiceTypNMEA,'GLL') | isempty(timeNMEAHeaderGGA)
            lat=latGLL;
            long=longGLL;
            timeNMEA=timeNMEAGLL;
            timeNMEAHeader=timeNMEAHeaderGLL;
            typNMEA='GLL';
        end
            
        if ~isempty(timeNMEAHeader)
            [timeH,ind_unique,uu]=unique(timeNMEAHeader);
            lat_interp=interp1(timeH,lat(ind_unique),timePing(channelNumber,ind_ok),'linear','extrap');
            long_interp=interp1(timeH,long(ind_unique),timePing(channelNumber,ind_ok),'linear','extrap');
            timeNMEA_interp=interp1(timeH,timeNMEA(ind_unique),timePing(channelNumber,ind_ok),'linear','extrap');
        end
        if ~isempty(time_VTG)
            [timeV,ind_unique,uu]=unique(time_VTG);
            heading_interp=interp1(time_VTG(ind_unique),heading(ind_unique),timePing(channelNumber,ind_ok),'linear','extrap');
            speed_interp=interp1(time_VTG(ind_unique),speed_knt(ind_unique),timePing(channelNumber,ind_ok),'linear','extrap');
        end
        for iind_ok=1:length(ind_ok)
            PingData=RawData.PingData(channelNumber,ind_ok(iind_ok));
            NMEAData=PingData.NMEAData;
            if ~isempty(timeNMEAHeader)
                NMEAData.lat=lat_interp(iind_ok);
                NMEAData.long=long_interp(iind_ok);
                NMEAData.timeNMEA=timeNMEA_interp(iind_ok);
                NMEAData.typNMEA=typNMEA;
            end
            if ~isempty(time_VTG)
                NMEAData.heading=heading_interp(iind_ok);
                NMEAData.speed_knt=speed_interp(iind_ok);
            end
            if ~isfield(PingData.MotionData,'heading')
                PingData.MotionData.heading=[];
            end
                
            if isempty(PingData.MotionData.heading) & ~isempty(time_VTG)
                PingData.MotionData.heading=NMEAData.heading;
            end
            PingData.NMEAData=NMEAData;
            RawData.PingData(channelNumber,ind_ok(iind_ok))=PingData;
        end
    end
    %ajout filter data dans le cas o� il n'y a que des GPT
    if isempty(PingData.FilterData);
            EmptyFilter.stage=[];
            EmptyFilter.channelNumber=[];
            EmptyFilter.channelId=[];
            EmptyFilter.nCoefficients=[];
            EmptyFilter.decimationFactor=[];
            EmptyFilter.coefficients=[];
            RawData.FilterData(channelNumber,1:2)=EmptyFilter;
    end
end


end
