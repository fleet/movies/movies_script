function [freq_cal, gain_cal]=read_cal_channel_EK80(RawData,TrList_File,ichannel)

%TrList_File='J:\PELGAS17\calibration\EK80\results\TrList_calibrationPELGAS17cor.xml';% chemin calibration

TrList_calibration = xml2struct(TrList_File); 
Transducer = TrList_calibration.Root.TransducerData.Transducer;

fileNumber=1;

% pour une raison inexpliquée (version matlab suivant le poste?) parfois Transducer est un
% cell array ou une structure. On le passe en cell array.
if ~iscell(Transducer)
    for tr=1:size(Transducer,2)
        TransducerCell{tr}=Transducer(tr);
    end
    Transducer=TransducerCell;
    clear  TransducerCell;
end

% fichier de calibration 
itr_cal=0;
for tr=1:size(Transducer,2)
    if strcmp(Transducer{tr}.Attributes.TransducerName , RawData(fileNumber).Data.ConfigurationData.Transceivers(ichannel).Channels.Transducer.TransducerName) &...
            strcmp(Transducer{tr}.Attributes.TransducerSerialNumber , RawData(fileNumber).Data.ConfigurationData.Transceivers(ichannel).Channels.Transducer.SerialNumber)
        itr_cal=tr;
        break;
    end
end
if itr_cal>0
    nfr=length(Transducer{itr_cal}.FrequencyPar);
    freq_cal=zeros(1,nfr);
    gain_cal=zeros(1,nfr);
    for frq = 1:nfr  
        freq_cal(frq)=str2double(Transducer{itr_cal}.FrequencyPar{frq}.Attributes.Frequency);
        gain_cal(frq)=str2double(Transducer{itr_cal}.FrequencyPar{frq}.Attributes.Gain);
    end
end