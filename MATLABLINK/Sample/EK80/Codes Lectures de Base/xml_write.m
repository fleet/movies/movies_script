function DOMnode = xml_write(filename, tree, RootName, Pref)
%XML_WRITE  Writes Matlab data structures to XML file
%
% DESCRIPTION
% xml_write( filename, tree) Converts Matlab data structure 'tree' containing
% cells, structs, numbers and strings to Document Object Model (DOM) node
% tree, then saves it to XML file 'filename' using Matlab's xmlwrite
% function.
%
% xml_write(filename, tree, RootName, Pref) allows you to specify
% additional preferences about file format
%
% DOMnode = xml_write([], var, ...) same as above exept that DOM node is not
% saved but returned.
%
% INPUT
%   filename     filename
%   tree         Matlab structure tree to store in xml file.
%   RootName     XML tag name used for root (top level) node
%   Pref         Other preferences:
%     Pref.ItemName - default 'item' -  name of a special tag used to
%                     itemize cell arrays
%     Pref.CellItem   - default 'true' - allow cell arrays to use 'item'
%     notation
%     Pref.StructItem - default 'true' - allow arrays of structs to use
%       'item' notation. Example:
%         <a>
%           <b>
%             <item> ... <\item>
%             <item> ... <\item>
%           <\b>
%         <\a>
%       or use default struct notation:
%         <a>
%           <b> ... <\b>
%           <b> ... <\b>
%         <\a>
%
%
% OUTPUT
%   DOMnode      Document Object Model (DOM) node tree in the format
%                required as input to xmlwrite. (optional)
%
% See also
%   xml_read, xmlread, xmlwrite
%
% Written by Jarek Tuszynski, SAIC, jaroslaw.w.tuszynski_at_saic.com

%% default preferences
DPref.ItemName  = 'item'; % name of a special tag used to itemize cell arrays
DPref.StructItem = true;  % allow arrays of structs to use 'item' notation
DPref.CellItem   = true;  % allow cell arrays to use 'item' notation

%% read user preferences
if (nargin>3)
    if (isfield(Pref, 'ItemName'  )), DPref.ItemName   = Pref.ItemName;   end
    if (isfield(Pref, 'StructItem')), DPref.StructItem = Pref.StructItem; end
    if (isfield(Pref, 'CellItem'  )), DPref.CellItem   = Pref.CellItem;   end
end
if (nargin<3), RootName='ROOT'; end

%% Initialize jave object that will store xml data structure
DOMnode  = com.mathworks.xml.XMLUtils.createDocument(RootName);

%% Use recursive function to convert matlab data structure to XML
root = DOMnode.getDocumentElement;
struct2DOMnode(DOMnode, root, tree, DPref.ItemName, DPref);

%% Remove the only child of the root node
root   = DOMnode.getDocumentElement;
Child  = root.getChildNodes; % create array of children nodes
nChild = Child.getLength;    % number of children
if (nChild==1)
    node = root.removeChild(root.getFirstChild);
    while(node.hasChildNodes)
        root.appendChild(node.removeChild(node.getFirstChild));
    end
end

%% save java DOM tree to XML file
if (~isempty(filename)), xmlwrite(filename, DOMnode); end



%% ========================================================================

function [] = struct2DOMnode(xml, parent, s, name, Pref)
% struct2DOMnode is a recursive function that converts matlab's structs to
% DOM nodes.
% INPUTS:
%  xml - jave object that will store xml data structure
%  parent - parent DOM Element
%  s - Matlab data structure to save
%  name - name to be used in xml tags describing 's'
%  Pref - preferenced

ItemName = Pref.ItemName;
if (ischar(s) && min(size(s))>1) % if 2D array of characters
    s=cellstr(s);                  % than convert to cell array
end
while (iscell(s) && length(s)==1), s = s{1}; end
nItem = length(s);
if (iscell(s)) % if this is a cell array
    if (nItem==1 || strcmp(name, 'CONTENT') || ~Pref.CellItem)
        if (strcmp(name, 'CONTENT')), CellName = ItemName;  % use 'item' notation <item> ... <\item>
        else                          CellName = name; end  % don't use 'item' notation <a> ... <\a>
        for iItem=1:nItem   % save each cell separatly
            struct2DOMnode(xml, parent, s{iItem}, CellName, Pref); % recursive call
        end
    else % use 'item' notation  <a> <item> ... <\item> <\a>
        node = xml.createElement(name);
        for iItem=1:nItem   % save each cell separatly
            struct2DOMnode(xml, node, s{iItem}, ItemName , Pref); % recursive call
        end
        parent.appendChild(node);
    end
elseif (isstruct(s))  % if struct than deal with each field separatly
    fields = fieldnames(s);
    % if array of structs with no attributes than use 'items' notation
    if (nItem>1 && Pref.StructItem && ~isfield(s,'ATTRIBUTE') )
        node = xml.createElement(name);
        for iItem=1:nItem
            struct2DOMnode(xml, node, s(iItem), ItemName, Pref ); % recursive call
        end
        parent.appendChild(node);
    else % otherwise save each struct separatelly
        for j=1:nItem
            node = xml.createElement(name);
            for i=1:length(fields)
                field = fields{i};
                x = s(j).(field);
                if (isempty(x)), continue; end
                if (~isempty(strfind(field, 'CONTENT')) && iscell(x)) % set attributes of the node
                    struct2DOMnode(xml, node, x, field, Pref ); % recursive call will modify 'node'
                elseif (strfind(field, 'ATTRIBUTE')) % set attributes of the node
                    attName = fieldnames(x);       % get names of all the attributes
                    for k=1:length(attName)        % attach them to the node
                        att = xml.createAttribute(char(attName(k)));
                        att.setValue(char_(x.(attName{k})));
                        node.setAttributeNode(att);
                    end
                else                             % set children of the node
                    struct2DOMnode(xml, node, x, field, Pref ); % recursive call will modify 'node'
                end
            end  % end for i=1:nFields
            parent.appendChild(node);
        end  % end for j=1:nItem
    end
else  % if not a struct and not a cell ...
    try
        txt = xml.createTextNode(char_(s)); % ... than it can be converted to text
    catch
        txt = xml.createTextNode(char_(s')); % ... than it can be converted to text
    end
    if (strfind(name, 'CONTENT'))
        parent.appendChild(txt);
    else
        node = xml.createElement(name);
        node.appendChild(txt);
        parent.appendChild(node);
    end
end

%% =======================================================================
function s = char_(s)
% convert matlab variables to sting
if (isnumeric(s) || islogical(s))
    dim = size(s);
    if (length(dim)>2),
        s=s(:); s=s'; % if this is high dimension array than convert to 1D array
    end
    if (min(dim)>1)
        s=mat2str(s); % convert matrix to string
    else
        s=num2str(s); % convert array of numbers to string
    end
else
    s = char(s);
    if (min(size(s))>1)
        s=s(:); s=s'; % if this is high dimension array than convert to 1D array
    end
end
if (length(s)>1)
    s(s<32|s>127)=' ';            % convert no-printable characters to spaces
    s = strtrim(s);               % remove spaces from begining and the end
%     s = regexprep(s, '\s+', ' '); % remove multiple spaces
    
    for i=1:size(s,1)
        pppp = regexprep(s(i,:), '\s+', ' '); % remove multiple spaces
        s(i,1:length(pppp)) = pppp; % remove multiple spaces
    end
end


