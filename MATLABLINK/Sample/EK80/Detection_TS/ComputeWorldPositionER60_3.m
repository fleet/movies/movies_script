function [PosRel,PosAbsTrans]= ComputeWorldPositionER60_3(Transducer,headingRad,pitchRad,rollRad,sensorHeaveMeter, Range, AlongTSAngle, AthwartTSAngle)
	
Delay=0;

m_MatrixNav=CreateRotationMatrix_2(headingRad,pitchRad,rollRad );
m_TranslationNav=([0,0,sensorHeaveMeter] )';

m_transducerTranslation=[Transducer.m_pPlatform.along_ship_offset;Transducer.m_pPlatform.athwart_ship_offset;Transducer.m_pPlatform.depth_offset];

m_transducerRotation=CreateRotationMatrix_2(Transducer.m_transRotationAngleRad,Transducer.m_transFaceAlongAngleOffsetRad,Transducer.m_transFaceAthwarAngleOffsetRad);

temp = 	Range+Delay;
temp=temp*(cos(AlongTSAngle)*cos(AthwartTSAngle));
	
CoordSoftChannel=[temp*tan(AlongTSAngle);temp*tan(AthwartTSAngle);temp];

SoftRotation=CreateRotationMatrix_2(0,0,0);

%PosRel= m_MatrixNav*(m_transducerRotation*SoftRotation*CoordSoftChannel+m_transducerTranslation)+m_TranslationNav;
PosRel= m_MatrixNav*(m_transducerRotation*SoftRotation*CoordSoftChannel)+m_TranslationNav;

PosAbsTrans=m_MatrixNav*m_transducerTranslation;
