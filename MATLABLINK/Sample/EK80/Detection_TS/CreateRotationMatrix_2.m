function M= CreateRotationMatrix(YAngle, PAngle, RAngle)
    fCos = cos(RAngle);
	fSin = sin(RAngle);
    
	kXMat= [1.0,0.0,0.0;
        0.0,fCos,-fSin;
        0.0,fSin,fCos];
    
    	
	fCos = cos(PAngle);
	fSin = sin(PAngle);
    kYMat= [fCos,0.0,fSin;
        0.0,1.0,0.0;
        -fSin,0.0,fCos];

	fCos = cos(YAngle);
	fSin = sin(YAngle);
	kZMat= [fCos,-fSin,0.0;
        fSin,fCos,0.0;
        0.0,0.0,1.0];
    
    M = kZMat*(kYMat*kXMat);