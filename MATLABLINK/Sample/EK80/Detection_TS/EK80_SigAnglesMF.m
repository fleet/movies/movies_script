function [Sig_MF,filt,Along_MF_Deg,Athwart_MF_Deg]=EK80_SigAnglesMF(RawData,ichannel)

% fournit le signal d'amplitude complexe compressé pour chaque ping (pas d'application de
% calibration)

nchannel=size(RawData.Data.PingData,1);


%détermination ping d'émission
ip_list=[];
for ip=1:size(RawData.Data.PingData,2)
    if ~isempty(RawData.Data.PingData(ichannel,ip).time)
        ip_list(end+1)=ip;
    end
end
% détermination wbt et tsd (WBT tube)
if size(RawData.Data.PingData,1)==size(RawData.Data.ConfigurationData.Transceivers,2) %1freq/wbt
    iwbt=ichannel;
    itsd=1;
else
    wbt_list=[];
    tsd_list=[];
    for iwbt=1:size(RawData.Data.ConfigurationData.Transceivers,2)
        for itsd=1:size(RawData.Data.ConfigurationData.Transceivers(iwbt).Channels,2)
            wbt_list(end+1)=iwbt;
            tsd_list(end+1)=itsd;
        end
    end
    iwbt=wbt_list(ichannel);
    itsd=tsd_list(ichannel);
end
    

if strcmp(RawData.Data.ConfigurationData.Transceivers(iwbt).TransceiverType,'GPT')
    warndlg('GPT channel: stop processing spectrum. Choose WBT channel');
    return;
end

if (~RawData.Data.PingData(ichannel,ip_list(1)).ParameterData.pulseForm) %CW
    uiwait(msgbox('CW mode. Stop processing'));
    return;
end


%paramètres EK80
nSectors = size(RawData.Data.PingData(ichannel,ip_list(1)).SampleData.complexSamples,2);
nominalTransducerImpedance  = 75;
wbtImpedanceRx              = 5e3;
fe=1/RawData.Data.PingData(ichannel,ip_list(1)).ParameterData.sampleInterval;


f1=RawData.Data.PingData(ichannel,ip_list(1)).ParameterData.frequencyStart;
f2=RawData.Data.PingData(ichannel,ip_list(1)).ParameterData.frequencyStop;
T=RawData.Data.PingData(ichannel,ip_list(1)).ParameterData.pulseLength;
B=f2-f1;

% Transmit signal
transmitSignal = CreateTransmitSignal(RawData.Data.PingData(ichannel,ip_list(1)));
filt=transmitSignal/norm(transmitSignal)^2;

% taille de fft (pour la compression d'impulsion)
Nfft=2*ceil(max(T,length(filt)/fe)*fe);
%Nfft=2^(ceil(log2(Nfft)));
Tfft=Nfft/fe;

np=length(ip_list);
ns=size(RawData.Data.PingData(ichannel,ip_list(1)).SampleData.complexSamples,1);
shift=Nfft/2;
nb_fft=floor(ns/Nfft*2-1);

fft_filt=fft(filt,Nfft);

% Correlation with FFT
clear SigSectors_MF
for ip=1:np
%for ip=1218:1218
    for ic=1:nb_fft
        data4=RawData.Data.PingData(ichannel,ip_list(ip)).SampleData.complexSamples(1+(ic-1)*Nfft/2:(ic+1)*Nfft/2,:);
        amp_raw=data4*(wbtImpedanceRx+nominalTransducerImpedance)/wbtImpedanceRx/(2*sqrt(2*nominalTransducerImpedance*nSectors));
        %amp_raw=sum(data4,2);
        fft_amp_raw=fft(amp_raw);
        fft_MF=fft_amp_raw.*repmat(conj(fft_filt),1,nSectors);
        corrFilt=ifft(fft_MF);
        SigSectors_MF(ip,1:nSectors,1+(ic-1)*Nfft/2:ic*Nfft/2)=corrFilt(1:Nfft/2,:).';
    end
end



% Sig_MF
Sig_MF = squeeze(sum(SigSectors_MF,2));

% Angles MF
f1=RawData.Data.PingData(ichannel,ip_list(1)).ParameterData.frequencyStart;
f2=RawData.Data.PingData(ichannel,ip_list(1)).ParameterData.frequencyStop;
f_tsd=str2num(RawData.Data.ConfigurationData.Transceivers(iwbt).Channels(itsd).Transducer.Frequency);
AngleSensitivityAlongship=str2num(RawData.Data.ConfigurationData.Transceivers(iwbt).Channels(itsd).Transducer.AngleSensitivityAlongship);
AngleSensitivityAthwartship=str2num(RawData.Data.ConfigurationData.Transceivers(iwbt).Channels(itsd).Transducer.AngleSensitivityAthwartship);

complexFore         = squeeze(sum(SigSectors_MF(:,3:4,:),2)/2);
complexAft          = squeeze(sum(SigSectors_MF(:,1:2,:),2)/2);
complexStarboard    = squeeze((SigSectors_MF(:,1,:) + SigSectors_MF(:,4,:))/2);
complexPort         =  squeeze(sum(SigSectors_MF(:,2:3,:),2)/2);


Along_MF_Deg    = squeeze(angle( complexFore.*conj(complexAft)) *180/pi/AngleSensitivityAlongship*f_tsd/((f1+f2)/2));
Athwart_MF_Deg  = squeeze(angle( complexStarboard.*conj(complexPort)) *180/pi/AngleSensitivityAthwartship*f_tsd/((f1+f2)/2));

            
%Power_MF=20*log10(abs(Sig_MF));

