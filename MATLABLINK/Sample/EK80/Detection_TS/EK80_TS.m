function [Sp_MF,r,targets,heure,heading,pitchRad,rollRad,lat_ship,long_ship,heave,speed_knt]=EK80_TS(RawData,ichannel,Sig_MF,Along_MF_Deg,Athwart_MF_Deg,filt,d_prev,d_post,TSmin,MaxGainComp,MaxPhaseDev,varargin)


nchannel=size(RawData.Data.PingData,1);


%d�termination ping d'�mission
ip_list=[];
for ip=1:size(RawData.Data.PingData,2)
    if ~isempty(RawData.Data.PingData(ichannel,ip).time)
        ip_list(end+1)=ip;
    end
end
% d�termination wbt et tsd (WBT tube)
if size(RawData.Data.PingData,1)==size(RawData.Data.ConfigurationData.Transceivers,2) %1freq/wbt
    iwbt=ichannel;
    itsd=1;
else
    wbt_list=[];
    tsd_list=[];
    for iwbt=1:size(RawData.Data.ConfigurationData.Transceivers,2)
        for itsd=1:size(RawData.Data.ConfigurationData.Transceivers(iwbt).Channels,2)
            wbt_list(end+1)=iwbt;
            tsd_list(end+1)=itsd;
        end
    end
    iwbt=wbt_list(ichannel);
    itsd=tsd_list(ichannel);
end
    
if strcmp(RawData.Data.ConfigurationData.Transceivers(iwbt).TransceiverType,'GPT')
    uiwait(msgbox('GPT channel: stop processing spectrum. Choose WBT channel'));
    return;
end

if (~RawData.Data.PingData(ichannel,ip_list(1)).ParameterData.pulseForm) %CW
    uiwait(msgbox('CW mode. Stop processing'));
    return;
end


% EK80 parameters
fe=1/RawData.Data.PingData(ichannel,ip_list(1)).ParameterData.sampleInterval;
f1=RawData.Data.PingData(ichannel,ip_list(1)).ParameterData.frequencyStart;
f2=RawData.Data.PingData(ichannel,ip_list(1)).ParameterData.frequencyStop;
f_tsd=str2num(RawData.Data.ConfigurationData.Transceivers(iwbt).Channels(itsd).Transducer.Frequency);
AngleSensitivityAlongship=str2num(RawData.Data.ConfigurationData.Transceivers(iwbt).Channels(itsd).Transducer.AngleSensitivityAlongship);
AngleSensitivityAthwartship=str2num(RawData.Data.ConfigurationData.Transceivers(iwbt).Channels(itsd).Transducer.AngleSensitivityAthwartship);
T=RawData.Data.PingData(ichannel,ip_list(1)).ParameterData.pulseLength;
B=f2-f1;
soundSpeed=RawData.Data.PingData(ichannel,ip_list(1)).EnvironmentData.soundSpeed;
transmitPower=RawData.Data.PingData(ichannel,ip_list(1)).ParameterData.transmitPower; %W
soundSpeed=RawData.Data.PingData(ichannel,ip_list(1)).EnvironmentData.soundSpeed;
frequencyUsed=RawData.Data.PingData(ichannel,ip_list(1)).ParameterData.frequencyUsed;
absorptionCoefficientsCenter = EstimateAbsorptionCoefficients(RawData.Data.PingData(ichannel,ip_list(1)).EnvironmentData,frequencyUsed);
lambdaCenter=soundSpeed./frequencyUsed;
gainTable       = str2num(RawData.Data.ConfigurationData.Transceivers(iwbt).Channels(itsd).Transducer.Gain);
gainCenter = gainTable(end)+20*log10(frequencyUsed/str2num(RawData.Data.ConfigurationData.Transceivers(iwbt).Channels(itsd).Transducer.Frequency));
nSectors = size(RawData.Data.PingData(ichannel,ip_list(1)).SampleData.complexSamples,2);
nominalTransducerImpedance  = 75;
wbtImpedanceRx              = 5e3;


%% FFT parameters
if d_prev>T*soundSpeed/2
    disp('        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
    d_prev=T*soundSpeed/2;
    disp(['        Too large previous distance requested. Reduced to:' num2str(d_prev) 'm']);
    disp('        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
end
if d_post>T*soundSpeed/2
    disp('        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
    d_post=T*soundSpeed/2;
    disp(['        Too large post distance requested. Reduced to:' num2str(d_post) 'm']);
    disp('        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
end
if d_prev<1/B*soundSpeed/2
    disp('        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
    d_prev=1/B*soundSpeed/2;
    disp(['        Too small previous distance requested. Increased to:' num2str(d_prev) 'm']);
    disp('        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
end
if d_post<1/B*soundSpeed/2
    disp('        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
    d_post=1/B*soundSpeed/2;
    disp(['        Too small previous distance requested. Increased to:' num2str(d_post) 'm']);
    disp('        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
end

n_prev=ceil(d_prev/(soundSpeed/2/fe)*1.25);% avec hanning sur 2*0.2
n_post=ceil(d_post/(soundSpeed/2/fe)*1.25);% avec hanning sur 2*0.2
Ndf=n_prev+n_post+1;
Nfft=2^(ceil(log2(Ndf)));


%nShapingSamples = floor(0.7/2*Ndf);
nShapingSamples = floor(0.4/2*Ndf);
windowFunction  = ((1-cos(pi*(0:2*nShapingSamples-1).'/nShapingSamples))/2).^2;
shapingWindow   = [windowFunction(1:nShapingSamples); ones(Ndf-2*nShapingSamples,1); windowFunction(nShapingSamples+1:end)];
%shapingWindow   = shapingWindow*0+1;

% taille de fft (pour la compression d'impulsion)
Nfft_MF=2*ceil(max(T,length(filt)/fe)*fe);
fft_filt_MF=fft(filt,Nfft_MF);
decMF=ceil((length(filt)+1)/2);

%
%index des fr�quences
f2per=rem(f2,fe);
f1per=rem(f1,fe);
if f1per<f2per
    nper=floor(f1/fe);
    freq=(0:Nfft-1)/Nfft*fe+nper*fe;
else
    freqper=(0:Nfft-1)/Nfft*fe;
    ind_out=find(freqper>f2per & freqper<f1per);
    if ~isempty(freqper)
        iout0=ind_out(ceil(length(ind_out)/2));
    else
        iout0=max(freqper<f1per)+1;
    end
    nper=floor(f1/fe);
    freq=[freqper(1:iout0-1)+(nper+1)*fe freqper(iout0:end)+nper*fe];
end

%r�ordonnancement des fr�quences
[freq,indordf]=sort(freq);
ifreqB=find(freq>=f1 & freq <=f2);

% EK80 parameters
clear absorptionCoefficients
for i=1:length(freq)
    absorptionCoefficients(i) = EstimateAbsorptionCoefficients(RawData.Data.PingData(ichannel,ip_list(1)).EnvironmentData,freq(i));
end
lambda=soundSpeed./freq;

if nargin==11
    %%%%%%%%%%%%%%%%%%% l'estimation effectu�e ci-dessous des gain(f) � appliquer devra �tre remplac�e par les gain(f) lus dans le fichier d'�talonnage (interpol�s aux fr�quences freq)
    gainTable       = str2num(RawData.Data.ConfigurationData.Transceivers(iwbt).Channels(itsd).Transducer.Gain);
    gain = gainTable(end)+20*log10(freq/str2num(RawData.Data.ConfigurationData.Transceivers(iwbt).Channels(itsd).Transducer.Frequency));
    along_of=str2num(RawData.Data.ConfigurationData.Transceivers(iwbt).Channels(itsd).Transducer.AngleOffsetAlongship);
    athwart_of=str2num(RawData.Data.ConfigurationData.Transceivers(iwbt).Channels(itsd).Transducer.AngleOffsetAthwartship);
    ouv_al=str2num(RawData.Data.ConfigurationData.Transceivers(iwbt).Channels(itsd).Transducer.BeamWidthAlongship)*f_tsd./freq.';
    ouv_at=str2num(RawData.Data.ConfigurationData.Transceivers(iwbt).Channels(itsd).Transducer.BeamWidthAthwartship)*f_tsd./freq.';
else
    freq_cal=varargin{1};
    gain_cal=varargin{2};
    
    %gain moyen pour la resolution demandee
    df_cal=median(freq_cal(2:end)-freq_cal(1:end-1))/2;
    nfilt=round(resol_f/df_cal);
    
    freq_interp=(freq_cal(1):df_cal:freq_cal(end));
    if freq_interp(end)<freq_cal(end) freq_interp(end+1)=freq_cal(end); end
    gain_cal_interp=interp1(freq_cal,gain_cal,freq_interp); %interpole dans les stop bands
    
    gain_df=(filter(ones(1,nfilt)/nfilt,1,10.^(gain_cal_interp/10)));
    freq_gain_df=freq_interp-df_cal*(nfilt-1)/2;
    gain = 10*log10(interp1(freq_gain_df,gain_df,freq));  

    %figure;hold on;plot(freq_cal/1000,gain_cal);plot(freq/1000,gain);title('u')
    %% � changer pour int�grer �galement les ouvertures et offsets FM d'�talonnage
    along_of=str2num(RawData.Data.ConfigurationData.Transceivers(iwbt).Channels(itsd).Transducer.AngleOffsetAlongship);
    athwart_of=str2num(RawData.Data.ConfigurationData.Transceivers(iwbt).Channels(itsd).Transducer.AngleOffsetAthwartship);
    ouv_al=str2num(RawData.Data.ConfigurationData.Transceivers(iwbt).Channels(itsd).Transducer.BeamWidthAlongship)*f_tsd./freq.';
    ouv_at=str2num(RawData.Data.ConfigurationData.Transceivers(iwbt).Channels(itsd).Transducer.BeamWidthAthwartship)*f_tsd./freq.';
    
end


%filtre avec m�me traitement et bon df
if (0) % puissance moyenne sur la bande
    dsp_Pt(ifreqB)=transmitPower/B;
else % puissance plus fine calcul�e sur le spectre, m�me traitement
    %filtre avec m�me traitement et bon df
    MFfilt=xcorr(filt,filt); %filtre MF
    imed=ceil(length(MFfilt)/2);
    fft_filt=fft(MFfilt(imed-n_prev:imed+n_post).*shapingWindow,Nfft);
    fft_filt=sqrt(abs(fft_filt)).';
    [transmitSignal] = CreateTransmitSignal(RawData.Data.PingData(ichannel,ip_list(1)));
    dsp_Pt= transmitPower*(fft_filt(indordf)*norm(transmitSignal)^2).^2/(fe^2*T); 
end

%% Single target detection
%%%%%%%%%%%%%%%%%%%%%%%%%%%

np=length(ip_list);
r=(0:size(Sig_MF,2)-1)*soundSpeed/2/fe;
tvg40=max(0,40*log10(r)+2*absorptionCoefficientsCenter*r);


% Sp after matched filtering
Sp_MF = 20*log10(abs(Sig_MF))  + repmat(tvg40 - 10*log10(transmitPower*lambdaCenter^2/(16*pi^2)) - 2*gainCenter,np,1) ;


for ip=1:np
    ind_level=[];
    ind_ts=[];
    targetRange=[];
    unCompensatedTS=[];
    AlongShipAngleRad=[];
    AthwartShipAngleRad=[];
    CompensatedTS=[];
    
    %% Level candidates
    irmin=max([n_prev,n_post,Nfft_MF/2,ceil(T/fe)]);
    ns=size(RawData.Data.PingData(ichannel,ip_list(ip)).SampleData.complexSamples,1);
    irmax=min(size(Sp_MF,2),ns-decMF-Nfft_MF/2);
    ind_level=irmin+find(Sp_MF(ip,irmin+1:irmax-n_post)>=TSmin-2*MaxGainComp);
    %ind_level=2000+find(Sp_MF(ip,2000+1:6000)>=TSmin-2*MaxGainComp);
    
    %% local maximum (find the strongest and cancel close ones, recursively)
    ind_loc_max=[];
    if ~isempty(ind_level)
        while ~isempty(ind_level)
            [m,ii]=max(Sp_MF(ip,ind_level));
            if m>=max(Sp_MF(ip,ind_level(ii)- n_prev:ind_level(ii)+ n_post))
                ind_loc_max(end+1)=ind_level(ii); %store local maximum
                ind_level=ind_level(find(ind_level<ind_loc_max(end)- n_prev | ind_level>ind_loc_max(end)+ n_post)); % cancel close detections
            else
                ind_level=ind_level(find(ind_level~=ind_level(ii))); %reject detection
            end
        end
    end
    
    if ~isempty(ind_loc_max)
        for ii=1:length(ind_loc_max)
            
            %% Phase candidates
            al_det=Along_MF_Deg(ip,ind_loc_max(ii)-n_prev:ind_loc_max(ii)+n_post);
            ath_det=Athwart_MF_Deg(ip,ind_loc_max(ii)-n_prev:ind_loc_max(ii)+n_post);
            if max(al_det)-min(al_det)<MaxPhaseDev & max(ath_det)-min(ath_det)<MaxPhaseDev %keep detection
                ind_ts(end+1)=ind_loc_max(ii);
                targetRange(end+1)=r(ind_ts(end));
                spectrum=fft(Sig_MF(ip,ind_loc_max(ii)-n_prev:ind_loc_max(ii)+n_post).*shapingWindow.',Nfft)./conj(fft_filt);
                unCompensatedTS(end+1,1:length(ifreqB))=20*log10(abs(spectrum(indordf(ifreqB)))) -10*log10(fe^2*T)+ 40*log10(targetRange(end))+ 2*absorptionCoefficients(ifreqB).*targetRange(end)-10*log10(dsp_Pt(ifreqB).*lambda(ifreqB).^2/(16*pi^2))-2*gain(ifreqB);
                AlongShipAngleRad(end+1)=Along_MF_Deg(ip,ind_loc_max(ii))*pi/180;
                AthwartShipAngleRad(end+1)=Athwart_MF_Deg(ip,ind_loc_max(ii))*pi/180;  
%                 
                x=2*(AlongShipAngleRad(end)*180/pi-along_of)./ouv_al(ifreqB).';
                y=2*(AthwartShipAngleRad(end)*180/pi-athwart_of)./ouv_at(ifreqB).';
                beam_dir2=-6.0206*(x.^2+y.^2-0.18*x.^2.*y.^2);
                CompensatedTS(end+1,1:length(ifreqB))=unCompensatedTS(end,1:length(ifreqB))-beam_dir2;
            end
        end
    end
    
    %% record targets
    targets{ip}.ind_rg=ind_ts;
    targets{ip}.targetRange=targetRange;
    targets{ip}.unCompensatedTS=unCompensatedTS;
    targets{ip}.compensatedTS=CompensatedTS;
    targets{ip}.freq=freq(ifreqB);  
    targets{ip}.AlongShipAngleRad=AlongShipAngleRad;
    targets{ip}.AthwartShipAngleRad=AthwartShipAngleRad;
    heure(ip)=RawData.Data.PingData(ichannel,ip_list(ip)).time;
    heading(ip)=RawData.Data.PingData(ichannel,ip_list(ip)).MotionData.heading;
    pitchRad(ip)=RawData.Data.PingData(ichannel,ip_list(ip)).MotionData.pitch*pi/180;
    rollRad(ip)=RawData.Data.PingData(ichannel,ip_list(ip)).MotionData.roll*pi/180;
    heave(ip)=RawData.Data.PingData(ichannel,ip_list(ip)).MotionData.heave;
    if isfield(RawData.Data.PingData(ichannel,ip_list(ip)).NMEAData,'lat')
        lat_ship(ip)=RawData.Data.PingData(ichannel,ip_list(ip)).NMEAData.lat;
        long_ship(ip)=RawData.Data.PingData(ichannel,ip_list(ip)).NMEAData.long;
    else
        lat_ship(ip)=0;
        long_ship(ip)=0;
    end
    if isfield(RawData.Data.PingData(ichannel,ip_list(ip)).NMEAData,'speed_knt')
        speed_knt(ip)=RawData.Data.PingData(ichannel,ip_list(ip)).NMEAData.speed_knt;
    else
        speed_knt(ip)=0;
    end
end

