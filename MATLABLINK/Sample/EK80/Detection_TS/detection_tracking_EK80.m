%clear
ichannel=2;

addpath('C:\src\git\movies_script\MATLABLINK\Sample\EK80\Codes Lectures de Base') 

%paramètres de détection similaire simrad
d_prev=0.15;%m
d_post=0.15;%m
TSmin=-60; %dB
MaxGainComp=3; %1-way
MaxPhaseDev=200; %deg

disp('lecture raw')
[RawData] = ReadEK80Data('C:\Users\lberger\Desktop\GUERLEDAN2019\RAW etalonnage\CW\GUERLEDAN19-D20191010-T101942.raw');

[Sig_MF,filt,Along_MF_Deg,Athwart_MF_Deg]=EK80_SigAnglesMF(RawData,ichannel);

disp('detection cible')
[Sp_MF,r,targets,heure,heading,pitchRad,rollRad,lat_ship,long_ship,heave,speed_knt]=EK80_TS(RawData,ichannel,Sig_MF,Along_MF_Deg,Athwart_MF_Deg,filt,d_prev,d_post,TSmin,MaxGainComp,MaxPhaseDev);

%affichage
figure(1000);hold off;imagesc(Sp_MF.',[-110 -20]);%echogramme TS
axis([1 2000 0 1200])
title('Sp MF');ylabel('échantillons');xlabel('pings');colorbar
colormap(gray)

SeuilSpeed_knt=3;
ntrous=1;

if (1)
    load Carto_0_6E;[x_ship y_ship]=latlon2xy(Carto, lat_ship, long_ship);
else %position par vitesse et cap
    clear x_ship y_ship
    x_ship(1)=0;y_ship(1)=0;
    for ip=2:size(Sp_MF,1)
        x_ship(ip)=x_ship(ip-1)+speed_knt(ip)*1852/3600*(heure(ip)-heure(ip-1))/24/3600*cosd(heading(ip));
        y_ship(ip)=y_ship(ip-1)+speed_knt(ip)*1852/3600*(heure(ip)-heure(ip-1))/24/3600*sind(heading(ip));
    end
end


disp('pistage')
[tracks,targets]=track_det_EK80(targets,heure,heading,pitchRad,rollRad,x_ship,y_ship,heave,SeuilSpeed_knt,ntrous);

% affichage tracks 
col='mrgbykcw';
hold on;
delta=r(2)-r(1);
for it=1:length(tracks)
    plot(tracks{it}.ping,tracks{it}.range/delta+3,[col(1+mod(it,length(col))) 'o'],'lINeW',2);
    text(tracks{it}.ping(1),tracks{it}.range(1)/delta+3,num2str(it));
end
hold off;

%filtrage des tracks
for it=1:length(tracks)
    length_track(it)=length(tracks{it}.ping);
    dd=sqrt((tracks{it}.x(2:end)-tracks{it}.x(1:end-1)).^2+(tracks{it}.y(2:end)-tracks{it}.y(1:end-1)).^2+(tracks{it}.z(2:end)-tracks{it}.z(1:end-1)).^2);
    tracks{it}.speed_knt=[0 dd./(heure(tracks{it}.ping(2:end))-heure(tracks{it}.ping(1:end-1)))]/3600/24*3600/1852;
    max_speed_knt(it)=max(tracks{it}.speed_knt);
    median_ts(it,:)=median(tracks{it}.ts,1);
end
ind_t=find(length_track>=20);
% 
% affichage tracks filtrées
%figure(10);
hold on;
for it=1:length(ind_t)
plot(tracks{ind_t(it)}.ping,tracks{ind_t(it)}.range/delta+1,[col(1+mod(it,length(col))) '*'],'lINeW',4);
text(tracks{ind_t(it)}.ping(1),tracks{ind_t(it)}.range(1)/delta+1,num2str(ind_t(it)),'FontWeight','bold');
end
hold off;
% 
if(1)
    %affichage d'une track
    it=152; %index de la track
    figure(1);plot(tracks{it}.x-tracks{it}.x(1),tracks{it}.y-tracks{it}.y(1),'*');xlabel('x m');ylabel('y m');grid on; title(['parcours track ' num2str(it)]);
    hold on;plot(x_ship(tracks{it}.ping)-tracks{it}.x(1),y_ship(tracks{it}.ping)-tracks{it}.y(1),'r-*');hold off;legend('track','ship')
    figure(2);plot(tracks{it}.alongRad*180/pi,tracks{it}.athwartRad*180/pi,'*');xlabel('along deg');ylabel('athwart deg');grid on; title(['parcours ref transducteur track ' num2str(it)]);
    figure(3);plot(tracks{it}.ping,-tracks{it}.z,'*');xlabel('ping');ylabel('-z m');grid on;title(['-z(m) ref transducteur track ' num2str(it) ])
    figure(5);plot(tracks{it}.ping,tracks{it}.speed_knt,'*');xlabel('ping');ylabel('knt');grid on;title(['speed(knt) track ' num2str(it) ])
    figure(4);plot(tracks{it}.freq/1000,tracks{it}.ts.');xlabel('kHz');ylabel('TS dB');grid on;title(['TS track ' num2str(it) ])

    figure(1000);
end
