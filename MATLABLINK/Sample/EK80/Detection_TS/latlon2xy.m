% Passage de coordonn�es g�ographiques � coordonn�es cartographiques
%
% SYNTAX :
%   [x, y] = latlon2xy(Carto, lat, lon)
%
% INPUT PARAMETERS :
%   Carto : Instance de cartographie
%   lat   : Latitudes (deg d�cimaux)
%   lon   : Longitudes (deg d�cimaux)
%
% OUTPUT PARAMETERS :
%   x  : Description du parametre (m).
%   y  : Description du parametre (m).
%
% EXAMPLES :
%   Carto = cl_carto('Ellipsoide.Type', 12, 'Projection.Type', 4)
%   [x, y] = latlon2xy(Carto, 46.5, 3)
%
%   [lat, lon] = xy2latlon(Carto, x, y)
%   [lat, lon] = xy2latlon(Carto, 700000, 6600000)
%   [lat, lon] = xy2latlon(Carto, 1160000, 4313000)
%
% SEE ALSO   : cl_carto xy2latlon Authors
% REFERENCES : CARAIBES(R)
% AUTHORS    : JMA
% VERSION    : $Id: entete.m,v 1.2 2003/11/12 12:46:11 augustin Exp $
%-------------------------------------------------------------------------------

function [x, y, flag] = latlon2xy(this, lat, lon)

flag = 1;
E  = double(this.Ellipsoide.Excentricite);
E2 = E ^ 2; 
A  = double(this.Ellipsoide.DemiGrandAxe);
deg2rad = pi / 180;

x = NaN(size(lon), 'double');
y = NaN(size(lon), 'double');

switch this.Projection.Type
    case 1 % Geodetic
        %my_warndlg('Projection is set to "Geodetic", impossible to compute metric coordinates.', 0, 'Tag', 'latlon2xyGeodetic')
        disp('Projection is set to "Geodetic", impossible to compute metric coordinates.')
        flag = 0;
        
    case 2 % Mercator
        for i=1:size(lon,1)
            G = lon(i,:) * deg2rad;
            L = lat(i,:) * deg2rad;

            X0 = double(this.Projection.Mercator.X0);
            Y0 = double(this.Projection.Mercator.Y0);

            G0   = double(this.Projection.Mercator.long_merid_orig) * deg2rad;
            L0   = double(this.Projection.Mercator.lat_ech_cons)    * deg2rad;
            SL   = sin(L0) ^ 2;
            NORM = sqrt((1 - E2*SL) / (1-SL));
            ED   = E / 2;
            ES   = E * sin(L);
            LAT  = pi/4 + L/2;

            x(i,:) = A * (G-G0) / NORM + X0;
            y(i,:) = A*(log(tan(LAT)) + ED .* log((1-ES) ./ (1+ES)))/NORM + Y0;
        end
        
        %{
        mstruct = defaultm('mercator');
        mstruct.origin = [this.Projection.Mercator.long_merid_orig this.Projection.Mercator.lat_ech_cons  0];
        mstruct = defaultm(mstruct);
        tic; [x1, y1] = mfwdtran(mstruct,lat,lon); toc
%         tic; [x2, y2] = projfwd(mstruct, lat, lon);; toc
        figure; grid on;
        plot(x, y, '+k');
        hold on; plot(x1, y1, 'or');
%         hold on; plot(x2, y2, 'xm');
hold on; plot(x-x1, y-y1, 'or');
        %}
        
    case 3  % UTM
        for i=1:size(lon,1)
            G = lon(i,:) * deg2rad;
            L = lat(i,:) * deg2rad;
            X0 = double(this.Projection.UTM.X0);
            Y0 = double(this.Projection.UTM.Y0);
            G0    = ((double(this.Projection.UTM.Fuseau) - 30) * 6-3)*(pi / 180); 
            EP2   = E2 / (1 - E2); 
            C     = A / sqrt(1 - E2); 
            ALPHA = 0.75 * EP2; 
            BETA  = 0.9375 * EP2^2; 
            GAMMA = 0.546875 * EP2^3; 
            sinL = sin(L);
            cosL = cos(L);
            cosL2 = cosL .^ 2;
            N  = C ./ sqrt(1 + EP2 * cosL2);
            A1 = L + sinL .* cosL; 
            A2 = 0.75 * A1 + 0.5 * sinL .* (cosL .^ 3);
            A3 = (5 * A2 + 2 * sinL .* (cosL .^ 5)) / 3;
            S  = C * (L - ALPHA * A1 + BETA * A2 - GAMMA * A3); 
            W0 = cosL .* sin(G - G0); 
            W1 = tan(L) ./ cos(G - G0); 
            W2 = abs((1 + W0) ./ (1 - W0)); 
            W3 = EP2 * (log(W2) / 2) .^ 2 .* cosL2;
            x(i,:) = X0 + 0.9996 *  N .* log(W2) .* (1 + W3 / 6) / 2; 
            y(i,:) = Y0 + 0.9996 * (N .* (atan(W1) - L) .* (1 + W3 / 2) + S); 
        end

    case 4   %lambert
        L1   = double(this.Projection.Lambert.first_paral)     * deg2rad;
        L2   = double(this.Projection.Lambert.second_paral)    * deg2rad;
        G0   = double(this.Projection.Lambert.long_merid_orig) * deg2rad;
        X0   = double(this.Projection.Lambert.X0);
        Y0   = double(this.Projection.Lambert.Y0);
        RK   = double(this.Projection.Lambert.scale_factor);
        N1   = A / sqrt(1 - (E2 * sin(L1) ^ 2));
        N2   = A / sqrt(1 - (E2 * sin(L2) ^ 2));
        ISO1 = tan(L1/2+pi/4)*((1-E*sin(L1))/(1+E*sin(L1)))^(E/2);
        ISO2 = tan(L2/2+pi/4)*((1-E*sin(L2))/(1+E*sin(L2)))^(E/2);
        L12  = (L1 + L2) / 2;
        R0   = A / sqrt(1 - E2*sin(L12)^2) / sin(L12) * cos(L12) * RK;
        if ISO1 == ISO2
            N = 1;
        else
            N    = log((N2*cos(L2)) / (N1 * cos(L1))) / log(ISO1/ISO2);
        end
        C    = N1 * cos(L1) / N * ISO1^N;

        for i=1:size(lon,1)
            G = lon(i,:) * deg2rad;
            L = lat(i,:) * deg2rad;
            ISO  = tan(pi/4 + L/2) * ((1-E*sin(L)) / (1+E*sin(L))) ^ (E/2);

            IsoN   = ISO .^ N;
            NG_G0  = N .* (G - G0);
            x(i,:) =      C .* sin(NG_G0) ./ IsoN + X0;
            y(i,:) = R0 - C .* cos(NG_G0) ./ IsoN + Y0;
        end

    case 5 %stereo
        for i=1:size(lon,1)
            G = lon(i,:) * deg2rad;
            L = lat(i,:) * deg2rad;
            l_uP1 = L;
            l_uL1 = G;
            L0 = double(this.Projection.Stereo.lat_ech_cons);
            G0 = double(this.Projection.Stereo.long_merid_horiz);
            %         H0 = this.Projection.Stereo.hemisphere;
            uE = E;
            S  = uE * sin(abs(L0));
            uESS = uE * sin(l_uP1);
            l_rL = abs(L0);
            l_uES = S;
            l_pUC = A * sqrt((((1 - l_uES) / (1 + l_uES)) ^ uE) / (1 - l_uES*l_uES));
            % U  = A * sqrt((((1-S)/(1+S))^uE) / (1-S^2)) * cos(abs(L0)) / tan(pi/4-abs(L0)/2);
            if L0 < 0
                PIS4 = -pi / 4;
            else
                PIS4 = pi / 4;
            end

            if abs(l_uES - uE) < 1e-8
                l_uC1 = 2;
            else
                l_uC1 = cos(l_rL) / tan((pi/4) - l_rL/2);
            end

            uU = l_pUC * l_uC1;

            l_uC2 = tan(PIS4 - (l_uP1 / 2)) * sqrt(((1+uESS)/(1-uESS))^uE);
            l_uR =  abs(uU * l_uC2);

            %  R    = abs(U*tan(PIS4-l_uP1/2)*((1+uESS)/(1-uESS))^(E/2));

            x(i,:) = l_uR * cos(l_uL1 - G0);
            if L0 < 0
                y(i,:) = -l_uR * sin(l_uL1 - G0);
            else
                y(i,:) = l_uR * sin(l_uL1 - G0);
            end
        end

    otherwise
        disp('Pas fait')
end

% garbage_collector
