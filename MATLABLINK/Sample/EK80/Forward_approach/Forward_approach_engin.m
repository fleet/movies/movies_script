% Comparaison de la reponse mesuree pour le trajet d'un engin de peche,
% avec la r�ponse mod�lis� correspondant au capture de celui-ci
% ------------------------------------------------------------------------
% Input :   
%           - profondeur d'immersion de l'engin (DateHeure en format
%           numerique, profondeur d'immersion en m)
%           - donnee acoustique echo-integree correspondant au passage de
%           l'engin (sortie du script Base-EK80_2, fichier finissant en AllFreq_EI_2_v2ping)
%           - fichiers de captures par type d'organismes ("a" de
%           l'organisme pour le modele considere, nombre d'occurence (N)
%           pour cette taille) 
%           - volume �chantillonnee par l'engin
% ------------------------------------------------------------------------
% Output : 
%           - graphe de comparaison des spectres modelises au spectre 
%           mesure, avec ou sans legende 
%           - tableau de densitee de chaque type d'organismes   
% ------------------------------------------------------------------------
% Appelle de fonction : 
%           - celerityCalculation : calcul de la celerite du son � partir
%           des donnee d'entree  
%           - TSCalculation_arthur : calcul du TS de l'organisme pour les
%           frequences considere, le modele considere, les parametre 
%           considere et la taille consideree
%           - lineAspect : aspect des courbes en fonction du modele utilise
% ------------------------------------------------------------------------
% B. Remond 26/02/2014, M. Doray, 09/05/2015, ajout import de jackpot
% ------------------------------------------------------------------------

% Nettoyage de l'historique de la console
clear all; close all

% Chemin dossier Forward_Approach
pathHome = 'C:\Users\ablanlue\Documents\MATLAB\EK80_Matlab\Script_Arthur\Forward_Approach';
cd(pathHome) 


% positionnement de l'engin
path_depth_engin = 'C:\Users\ablanlue\Documents\Approche_directe\Immersion_engin'; 
[fileNameList,filePath,~] = uigetfile('*.csv','Select raw files','MultiSelect','off',path_depth_engin);
csvname = fullfile(filePath,fileNameList);
Engin_depth = csvread(csvname, 1,0);
Engin_depth(:,2) = Engin_depth(:,2) - 6; % tenir compte du tirant d'eau de la thalassa sur les echogrammmes

% Depth = [16, 19.5, 15, 19.5, 60];
% layer_depth = (Depth-5)*2;

hauteur_engin = 2; % ouverture vertical de l'engin
frequencies_center = [18, 38, 70, 120, 200, 333];
limit_depth = 4; % distance au sondeur a partir du quel on prend les donnees


path_Acou_engin = 'C:\Users\ablanlue\Documents\Approche_directe\Acoustique_engin\'; %fichier finissant en "AllFreq_EI_2_v2ping"
[fileNameList2,filePath2,~] = uigetfile('*.mat','Select raw files','MultiSelect','off',path_Acou_engin);
file_name = fullfile(filePath2,fileNameList2);

load([file_name]);

%-----------------------------------------------------------------
% Donnee le volume �chantillonnee par cette engin (voir metadata)
%-----------------------------------------------------------------
promptText      = {'Sampled Volume'};
dialogTitle     = 'Parameters';
defaultValues   = { '2945'};
answer = inputdlg(promptText,dialogTitle,1,defaultValues);
V = str2num(answer{1});

%-----------------------------------------------------------------
% Declare all variables as global to share with function TSCalculation
%-----------------------------------------------------------------

global gfluidSt89 hfluidSt89 gelasSt89 helasSt89 ggasSt89 hgasSt89

global LaOblSt89 LaScylSt89 tetaScylSt89 LaBcylSt89 LrBcylSt89

global relasSt94

global rfluidSt94 bDfluidSt94 sfluidSt94

global gCopTFS hCopTFS

global gCopDWBA hCopDWBA LaCopDWBA LbCopDWBA angCopDWBA dangCopDWBA
global phiCopDWBA angFCopDWBA angpdfCopDWBA nACopDWBA
global LFCopDWBA LpdfCopDWBA nLCopDWBA

global gGasDWBA hGasDWBA LaGasDWBA angGasDWBA dangGasDWBA
global angFGasDWBA angpdfGasDWBA nAGasDWBA
global LFGasDWBA LpdfGasDWBA nLGasDWBA

global gLEuphDWBA hLEuphDWBA LLEuphDWBA LaLEuphDWBA
global angLEuphDWBA dangLEuphDWBA tapLEuphDWBA rhoLLEuphDWBA
global angFLEuphDWBA angpdfLEuphDWBA nALEuphDWBA 
global LFLEuphDWBA LpdfLEuphDWBA nLLEuphDWBA

global gsEuphDWBA hsEuphDWBA cwsEuphDWBA LasEuphDWBA
global angsEuphDWBA dangsEuphDWBA tapsEuphDWBA rhoLsEuphDWBA
global angFsEuphDWBA angpdfsEuphDWBA nAsEuphDWBA 
global LFsEuphDWBA LpdfsEuphDWBA nLsEuphDWBA
global Dkloser

global elongationYe   rhoWaterYe  cWaterYe
global Dye gammaYe  etaYe tauYe


% Import des donnees de capture de l'engin considere 
name_folder = strsplit(fileNameList, '.');
name_station = strsplit(char(name_folder(1)), '-');
path_size = ['C:\Users\ablanlue\Documents\Approche_directe\Catch\',char(name_station(2)),'\'];
cd(path_size);
files = dir('*.csv');

Sv_forward = {};
name = {};
density = [];
fxv = 1:500; % frequecy in kHz
adepth= max(Engin_depth(:,2));
TMean = 12; 
SMean = 35;
cmoy = celerityCalculation(TMean,SMean,adepth);

f1 = figure(1);
f2 = figure(2);
for file = 1:length(files)
    
%-----------------------------------------------------------------
% Size vector
%-----------------------------------------------------------------
Size_vector = csvread(files(file).name, 1,0);
a = Size_vector(:,1);
N = Size_vector(:,2);  

name_species_tmp = strsplit(files(file).name, '.');
name_species_tmp2 = strsplit(char(name_species_tmp(1)), '_');
if length(name_species_tmp2) == 1
    name_species = char(name_species_tmp(1));
else
    name_species = char(name_species_tmp2(2));
end
%-----------------------------------------------------------------
% Model parameters
%-----------------------------------------------------------------
 if strcmp(name_species,'MYCT-SPP')
    model = 'gasbubbleYe';
    elongationYe = 1.8;
    rhoWaterYe = 1030;
    cWaterYe = 1500;
    gammaYe = 1.4;
    Dye = max(Engin_depth(:,2)); % moyenne?
    etaYe = 1;%2.4*1e-5*10^(247.8/(10+273.15-140)); %eau � 10�C, viscosit� en Pa.s-1
    tauYe = 200;%75*10^-3; %eau � 10�C, tension superficiel en N.m-1
    
 elseif strcmp(name_species,'CRYG-LIN')
    model = 'gasbubbleYe';
    elongationYe = 1.63;
    rhoWaterYe = 1030;
    cWaterYe = 1500;
    gammaYe = 1.4;
    Dye = max(Engin_depth(:,2)); % moyenne?
    etaYe = 1;%2.4*1e-5*10^(247.8/(10+273.15-140)); %eau � 10�C, viscosit� en Pa.s-1
    tauYe = 200;%75*10^-3; %eau � 10�C, tension superficiel en N.m-1
    
 elseif strcmp(name_species,'ECHI-DRU')
    model = 'gasbubbleYe';
    elongationYe = 2.75;
    rhoWaterYe = 1030;
    cWaterYe = 1500;
    gammaYe = 1.4;
    Dye = max(Engin_depth(:,2)); % moyenne?
    etaYe = 1;%2.4*1e-5*10^(247.8/(10+273.15-140)); %eau � 10�C, viscosit� en Pa.s-1
    tauYe = 200;%75*10^-3; %eau � 10�C, tension superficiel en N.m-1
    
  elseif strcmp(name_species,'SIPH-ONO') || strcmp(name_species,'Siphonophorae') || strcmp(name_species,'SIPH-ONO-video')
    model = 'gasbubbleYe';
    elongationYe = 2.35;
    rhoWaterYe = 1030;
    cWaterYe = 1500;
    gammaYe = 1.4;
    Dye = max(Engin_depth(:,2)); % moyenne? 
    etaYe = 2.4*1e-5*10^(247.8/(10+273.15-140)); %eau � 10�C, viscosit� en Pa.s-1
    tauYe = 75*10^-3; %eau � 10�C, tension superficiel en N.m-1
  
 elseif strcmp(name_species,'LARV-DIV')%|| strcmp(name_species,'Actinopterygii')
    model = 'gasbubbleYe';
    elongationYe = 2;
    rhoWaterYe = 1030;
    cWaterYe = 1500;
    gammaYe = 1.4;
    Dye = max(Engin_depth(:,2)); % moyenne? 
    etaYe = 1;%2.4*1e-5*10^(247.8/(10+273.15-140)); %eau � 10�C, viscosit� en Pa.s-1
    tauYe = 200;%75*10^-3; %eau � 10�C, tension superficiel en N.m-1
 
  elseif strcmp(name_species,'SOLE-SPP')%|| strcmp(name_species,'Actinopterygii')
    model = 'gasbubbleYe';
    elongationYe = 1;
    rhoWaterYe = 1030;
    cWaterYe = 1500;
    gammaYe = 1.4;
    Dye = max(Engin_depth(:,2)); % moyenne? 
    etaYe = 1;%2.4*1e-5*10^(247.8/(10+273.15-140)); %eau � 10�C, viscosit� en Pa.s-1
    tauYe = 200;%75*10^-3; %eau � 10�C, tension superficiel en N.m-1   
    
 elseif strcmp(name_species,'SCOM-SCO')||strcmp(name_species, 'ARGE-SPH')
    model = 'fluidcybtSt94';
    rfluidSt94 = 0.0173;
    bDfluidSt94 = 8;
    sfluidSt94 = 0.01;   
    
%   elseif strcmp(name_species,'PLEB-PIL')||strcmp(name_species, 'Ctenophora')
%     model = 'fluidcybtSt94';
%     rfluidSt94 = 0.0041;
%     bDfluidSt94 = 1;
%     sfluidSt94 = 0.01;
    
%   elseif strcmp(name_species,'BERO-OVA') pas de mesure de cette organisme
%     model = 'fluidcybtSt94';
%     rfluidSt94 = 0.0041;
%     bDfluidSt94 = 2;
%     sfluidSt94 = 0.01;  
    
%   elseif strcmp(name_species,'GELA-AUT')
%     model = 'fluidcybtSt94';
%     rfluidSt94 = 0.0041;
%     bDfluidSt94 = 3;
%     sfluidSt94 = 0.01;  
  
%   elseif strcmp(name_species,'SALP-FUS')|| strcmp(name_species, 'SOES-ZON') || strcmp(name_species, 'Thaliacea')
%     model = 'fluidcybtSt94';
%     rfluidSt94 = 0.0041;
%     bDfluidSt94 = 1.7;
%     sfluidSt94 = 0.01;  

 elseif strcmp(name_species,'Copepoda')
%     model = 'copepTFS';
%     gCopTFS = 1.02;
%     hCopTFS = 1.058;
  model = 'copepDWBA';
  gCopDWBA = 1.02;
  hCopDWBA = 1.058;
  LaCopDWBA = 4.5;%3.22; %(L/a)
  LbCopDWBA = 4.5;%3.22; %(L/b)
  angCopDWBA = 90; 
  dangCopDWBA = 30;
  phiCopDWBA = 1.5708; %angle d'incidence?, pas de diff�rence
  angFCopDWBA = 1;
  angpdfCopDWBA = 2;
  nACopDWBA = 10; %peu de dif de pass� 1
  LFCopDWBA = 0;
  LpdfCopDWBA = 2;
  nLCopDWBA = 1;

      
  elseif strcmp(name_species,'Limacina')
    model = 'elasShellSt94';
    relasSt94 = 0.5;

%   elseif strcmp(name_species, 'larvae')||strcmp(name_species, 'megalope')||strcmp(name_species, 'zoea')||strcmp(name_species, 'Amphipoda') % larvae => crustacean larvae
%     model = 'fluidcybtSt94';
%     rfluidSt94 = 0.058;
%     bDfluidSt94 = 2;
%     sfluidSt94 = 0.01;  
%  
  elseif strcmp(name_species,'Euphausiacea')%||strcmp(name_species,'Decapoda')
%      model = 'fluidcybtSt94';
%      rfluidSt94 = 0.058;
%      bDfluidSt94 = 10.5;
%      sfluidSt94 = 0.01;  
%       
     model = 'smallEuphDWBA';
    gsEuphDWBA = 1.016; %g
    hsEuphDWBA = 1.019;  %h
    LasEuphDWBA = 10.5; % ratio longueur/largeur
    angsEuphDWBA = 20; % en degr�s angle d'incidence
    dangsEuphDWBA = 20; % en degr�s std d'angle d'incidence 
    rhoLsEuphDWBA = 3; % rayon de courbure (en L*Rho, avec L la longueur de l'animal)
    angFsEuphDWBA = 1; % average sur l'angle? 1 oui, 0 non
    angpdfsEuphDWBA = 2;  % type de average 1 => uniforme, 2 gaussien
    nAsEuphDWBA = 10; % nbr d'angle moyenn�
    LFsEuphDWBA = 0; % average sur la longueur? 1 oui, 0 non
    LpdfsEuphDWBA = 2; % type de average 1 => uniforme, 2 gaussien
    nLsEuphDWBA = 1; % nbr d'angle moyenn�
    cwsEuphDWBA = cmoy; % sound speed in water (m/s)
    tapsEuphDWBA = 1; % parameter controls tapering funtion , ???
    
%   elseif strcmp(name_species,'Chaetognatha')||strcmp(name_species,'Polychaeta')
%     model = 'fluidcybtSt94';
%     rfluidSt94 = 0.030;
%     bDfluidSt94 = 17.15;
%     sfluidSt94 = 0.01; 
%     
  else 
   model = 'null';   
    

 end

 
    if strcmp(model,'null') == 0;


        Sv = [];
        TSreduceplot = [];
        TS = [];
        Sv_tot = [];

        %-----------------------------------------------------------------
        % Calcul du TS, puis du Sv pour chaque taille de chaque organismes
        %-----------------------------------------------------------------

    for x = 1:length(a)
        [TS,TAij,TSreduce,para] = TSCalculation_FA(fxv,a(x),model,TMean,SMean,adepth);
    %     for x = 1:length(a)
    %         kaxx = a(x) * 2 * pi * fxv * 1000 / cmoy;
    % 
    %     TSreduceplot = TSCalculationVska ...
    %                 (kaxx,model,TMean,SMean,adepth)';
    % 
    %     TS = TSreduceplot + 10*log10(pi*(a(x))^2);  

        Sv(:,x) = 10*log10(N(x)/V) + TS;

    end

        %-----------------------------------------------------------------
        % Cumul des Sv par taille de chaque organisme
        %-----------------------------------------------------------------
        for f = 1:length(fxv)
            Sv_tot(f) = 10*log10(sum(10.^(Sv(f,:)/10)));
        end


        %------------------------------------------------
        % PLOT
        %------------------------------------------------

        figure(f1);
            plot(fxv, Sv_tot,... %TSreduceplot
                [lineAspect(model,'lineStyle'),lineAspect(model,'color')],...
                'MarkerSize',4,'lineWidth',2, 'DisplayName', [model,'-',name_species]);
                hold on;

        figure(f2);
            plot(fxv, Sv_tot,... %TSreduceplot
                [lineAspect(model,'lineStyle'),lineAspect(model,'color')],...
                'MarkerSize',4,'lineWidth',2, 'DisplayName', [model,'-',name_species]);
                hold on;

        %------------------------------------------------
        % DENSITY
        %------------------------------------------------
        name{file} = name_species;
        density(file) = sum(N(:))/V;

            hold on; 

            Sv_forward{file}= Sv_tot;

    end
end



figure(f1);
 h = legend(gca,'show');
 set(h, 'Location', 'southeast');

tab_density = [name;num2cell(density)];

% calcul des dates/heures de debut et de fin de la p�che

for i = 1:length(Engin_depth)
    date_engin{i} = datestr(datenum([1970, 1, 1, 2, 0, Engin_depth(i,1)]));
    date_engin2(i) = datenum(date_engin{i});
end

date_acou = datestr(datenum(Time_Sv_ei{1} - 2/24));
date_acou2 = datenum(date_acou);
date_acou2 = date_acou2.';

Times=[];
bathy=[];
class_time=[];
bathy2=[];

minT = min(date_engin2);
maxT = max(date_engin2);

    %------------------------------------------------------------------
    % Calcul de la r�ponse acoustique du parcours de l'engin de p�che
    %------------------------------------------------------------------
Intervalle = maxT - minT;
%class = round(Intervalle/6);

class_time(1) = minT;

for j = 1:length(date_engin2)
    tmp = find(date_acou2(:) > class_time(j) & date_acou2(:) < class_time(j) + Intervalle/length(date_engin2));
    bathy_tmp = kron(Engin_depth(j,2), ones(1,length(tmp))); 
    Times_tmp = date_acou2(tmp);
    Times = [Times, Times_tmp];
    bathy = [bathy, bathy_tmp];
    class_time(j+1) = class_time(j) + Intervalle/length(date_engin2);
end
        
        
for j = 1:length(date_acou2)    
    if date_acou2(j) > minT & date_acou2(j) < maxT
        tmp = find(Times == date_acou2(j));
        bathy2(j) = bathy(tmp);
    else
        bathy2(j) = -6;
    end    
end        




depth_round = round(bathy2);
Sv_EI_engin_tmp = [];
Sv_EI_engin = {};

for f = 1:6
    Sv_EI_engin_tmp = [];
    for i = 1:length(Sv_ei{1}(:,1,1))
        if depth_round(i) >= limit_depth % profondeur limite 
        tmp1 = find(integration_depth == depth_round(i) | integration_depth == depth_round(i) + 1);
        tmp2 = Sv_ei{f}(i,tmp1,:);
        Sv_EI_engin_tmp = [Sv_EI_engin_tmp, tmp2];
        end
    end
   Sv_EI_engin{f} =  Sv_EI_engin_tmp;
end    

frequencies_begin = [18, 38, 45, 95, 180, 260]; 
frequencies_end = [18, 38, 90, 160, 250, 420];

% for f= 1:6
%     freq_id = find(frequencies >= frequencies_begin(f) & frequencies <= frequencies_end(f));
%     borne_freq{f} = freq_id;
%     Sv_ei_freq{f} = Sv_EI_engin(:,:,borne_freq{f});
%     frequencies2{f} = frequencies(:,borne_freq{f});
% end

for f= 1:6
    for x = 1:length(frequencies{1,f})
        reshape_Sv_ei{f} = reshape(Sv_EI_engin{f}(:,:,x), 1, []);
        quant_975{f}(x) = 10*log10(quantile(10.^(reshape_Sv_ei{f}/10),0.75));
        quant_025{f}(x) = 10*log10(quantile(10.^(reshape_Sv_ei{f}/10),0.25));
        mean_couche{f}(x)= 10*log10(mean(10.^(reshape_Sv_ei{f}/10)));
    end
end

    %------------------------------------------------------------------
    % Plot de la r�ponse mesuree pour le parcours de l'engin de p�che
    %------------------------------------------------------------------

figure(f1);
% set(0, 'DefaultAxesFontSize', 10);
for f = 1:2
    plot(frequencies{1,f},squeeze(mean_couche{f}(:)), '+black','LineWidth',2);
    % plot(frequencies{1,f},squeeze(quant_975{f}(:)), '+g');
    % plot(frequencies{1,f},squeeze(quant_025{f}(:)), '+g');
    hold on
end
for f= 3:6
    plot(frequencies{1,f},squeeze(mean_couche{f}(:)), 'black','LineWidth',2);
    % plot(frequencies{1,f},squeeze(quant_975{f}(:)), 'g');
    % plot(frequencies{1,f},squeeze(quant_025{f}(:)), 'g');
    hold on
end
    mina = 1;
    maxa = 500;
    %minSv = min(min(min(TSreduceplot)));
    %maxSv = max(max(max(TSreduceplot)));
    axis([mina maxa -140 -35]);
    xlim([mina maxa]);
    xlabel('f (KHz)');
    ylabel('Sv (dB)');
    %title('Comparaison de la reponse frequentielle de la couche et d''un mod�le GB (Ye)','FontSize',11);
    %legend('Reponse frequentielle moyenne de la couche','Mod�le de coquille elastique (Stanton 1994), a=1500�m, N=10N'); legend boxoff

    grid;
    Sv_forward_tmp = cat(1, Sv_forward{1:length(Sv_forward)}); 
    for f = 1:length(fxv)
    Sv_forward_sum(f) = 10*log10(sum(10.^(Sv_forward_tmp(:,f)/10)));
    end
    
plot(fxv,Sv_forward_sum, 'black','LineWidth',2);


       
figure(f2);
% set(0, 'DefaultAxesFontSize', 10);
for f = 1:2
    plot(frequencies{1,f},squeeze(mean_couche{f}(:)), '+black','LineWidth',2);
    % plot(frequencies{1,f},squeeze(quant_975{f}(:)), '+g');
    % plot(frequencies{1,f},squeeze(quant_025{f}(:)), '+g');
    hold on
end
for f= 3:6
    plot(frequencies{1,f},squeeze(mean_couche{f}(:)), 'black','LineWidth',2);
    % plot(frequencies{1,f},squeeze(quant_975{f}(:)), 'g');
    % plot(frequencies{1,f},squeeze(quant_025{f}(:)), 'g');
    hold on
end
    mina = 1;
    maxa = 500;
    %minSv = min(min(min(TSreduceplot)));
    %maxSv = max(max(max(TSreduceplot)));
    axis([mina maxa -140 -35]);
    xlim([mina maxa]);
    xlabel('f (KHz)');
    ylabel('Sv (dB)');
    %title('Comparaison de la reponse frequentielle de la couche et d''un mod�le GB (Ye)','FontSize',11);
    %legend('Reponse frequentielle moyenne de la couche','Mod�le de coquille elastique (Stanton 1994), a=1500�m, N=10N'); legend boxoff

    grid;
    Sv_forward_tmp = cat(1, Sv_forward{1:length(Sv_forward)}); 
    for f = 1:length(fxv)
    Sv_forward_sum(f) = 10*log10(sum(10.^(Sv_forward_tmp(:,f)/10)));
    end
    
plot(fxv,Sv_forward_sum, 'black','LineWidth',2);

% sauvegarde des output
  saveas(figure(f1),[filePath2,'forward_approach_spectrum'],'png');
  saveas(figure(f1),[filePath2,'forward_approach_spectrum'],'fig');
   
  saveas(figure(f2),[filePath2,'forward_approach_spectrum_nolegend'],'png');
  saveas(figure(f2),[filePath2,'forward_approach_spectrum_nolegend'],'fig');
%     
  save([filePath2,'density'],'tab_density');
    