%--------------------------------------------------------------------------
% TSCalculation_FA.m :                  dans testanne\CompilInv
%   Calculation of matrix of modelled Target Strength according to
%   a given model or material : 
%       - Truncated Fluid Sphere
%       - Spherical
%       - Oblong
%       - StraightCylinder
%       - Bent Cylinder
%       - materials : fluid, elastic, rigid/fixed, gazeous
%   TS(j,k) = target strength of Kth size at Jth frequency
%--------------------------------------------------------------------------
% Input :
%   fx       = vector of frequencies
%   tabSize  = vector of sizes
%   model    = model (txt)
%   Temp     = Mean Temperature �C
%   Sal      = Mean salinity o/oo
%   depthc   = depth (m) (negative)
%
% Output :
%   TS       = matrix of Target Strength in dB
%   TAij     = matrix of Target Strength (arithmetic values)
%   TSreduce = matrix of Reduced Target Strength
%--------------------------------------------------------------------------
% Procedures called :
%
% Functions called :
%   ghInit              = calculation of g and h parameter
%   celerityCalculation = calculation of celerity from T, S and depth
%   ModelSpherical      = calculation of TS matrix for spherical model
%   ModelSpheroidOblong = idem for oblong
%   ModelCylindreDroit  = idem for straigth cylinder
%   ModelCylindreCourbe = idem for bent cylinder
%   ModelTFS            = idem for TFS
%   gasbladder          = idem for micronekton, Kloser et al. 2002
%   gasbubbleye         = idem for gas bubble, Ye et al.
%--------------------------------------------------------------------------
% Version written for Matlab 6.5 Release 13 for PC
%--------------------------------------------------------------------------
% Original    : 
% New Version : 18/02/03 J.Balle-Beganton 
% Modified    : 22/08/03 jhb - implementation of CHU models
% Modified    : 17/05/2013 - ald - ajout du mod�le de Kloser
% Modified    : 15/10/2013 - BR - ajout du mod�le de Ye
% Modified    : 08/03/2017 - AB - ajout du mod�le meduse DWBA, gelatineux
%  DWBA, Scoulding, FA pour Forward Approach
%
% 
%--------------------------------------------------------------------------

function [TS,TAij,TSreduce,para] = TSCalculation_FA (fx,tabSize,model,Temp,Sal,depthc)

% fx=fxv;
% tabSize = abase;
% Temp=tempz;
% Sal=Smean;
% depthc = Dye;
%-----------------------------------------------------------------
% Declare all variables as global to share with function InitParamModel
%-----------------------------------------------------------------

global gfluidSt89 hfluidSt89 gelasSt89 helasSt89 ggasSt89 hgasSt89

global LaOblSt89 LaScylSt89 tetaScylSt89 LaBcylSt89 LrBcylSt89

global relasSt94

global rfluidSt94 bDfluidSt94 sfluidSt94

global gCopTFS hCopTFS

global gCopDWBA hCopDWBA LaCopDWBA LbCopDWBA angCopDWBA dangCopDWBA
global phiCopDWBA angFCopDWBA angpdfCopDWBA nACopDWBA
global LFCopDWBA LpdfCopDWBA nLCopDWBA

global gGasDWBA hGasDWBA LaGasDWBA angGasDWBA dangGasDWBA
global angFGasDWBA angpdfGasDWBA nAGasDWBA
global LFGasDWBA LpdfGasDWBA nLGasDWBA

global gLEuphDWBA hLEuphDWBA LLEuphDWBA LaLEuphDWBA
global angLEuphDWBA dangLEuphDWBA tapLEuphDWBA rhoLLEuphDWBA
global angFLEuphDWBA angpdfLEuphDWBA nALEuphDWBA 
global LFLEuphDWBA LpdfLEuphDWBA nLLEuphDWBA

global gsEuphDWBA hsEuphDWBA cwsEuphDWBA LasEuphDWBA
global angsEuphDWBA dangsEuphDWBA tapsEuphDWBA rhoLsEuphDWBA
global angFsEuphDWBA angpdfsEuphDWBA nAsEuphDWBA 
global LFsEuphDWBA LpdfsEuphDWBA nLsEuphDWBA
global Dkloser

global elongationYe   rhoWaterYe  cWaterYe
global Dye gammaYe etaYe tauYe tiYe
global mu_rSc tiSc

%-----------------------------------------------------------------
% Initialize variables length vector am and celerity c
%-----------------------------------------------------------------
cmoy  = celerityCalculation(Temp,Sal,depthc);   %�����������������������
%ALD 28/3/06
%cmoy = 1500 ; % ����������� ALD 28/3/06 pour comparer avec pgms Greenlaw
tabSizeM = tabSize ;   % tabSize vector from mm to meters
fx = fx *1000; % en Hz

% a verifier
% TS = zeros(length(fx),length(tabSize));
% TSreduce = zeros(length(fx),length(tabSize));

if length(findstr(model,'St89'))>0
    
    ghInitSt89;
    
    if length(findstr(model,'flui'))>0
        material = 'flui';
    elseif length(findstr(model,'elas'))>0
        material = 'elas';
    elseif length(findstr(model,'rgfx'))>0
        material = 'rgfx';
    elseif length(findstr(model,'gaze'))>0
        material = 'gaze';
    end
    
    if length(findstr(model,'sphr'))>0
        [TS, TSreduce, TAij] = ModelSpherical (material,fx,tabSizeM,g,h,cmoy);
    elseif length(findstr(model,'obln'))>0
        [TS, TSreduce, TAij] = ModelSpheroidOblong (material,fx,tabSizeM,g,h,cmoy,LaOblSt89);
        para.St89.LaOb = LaOblSt89;
    elseif length(findstr(model,'cyst'))>0
        [TS, TSreduce, TAij] = ModelCylindreDroit (material,fx,tabSizeM,g,h,cmoy,LaScylSt89,tetaScylSt89);
        para.St89.LaScy = LaScylSt89;
        para.St89.tetaScy = tetaScylSt89;
    elseif length(findstr(model,'cybt'))>0
        [TS, TSreduce, TAij] = ModelCylindreCourbe (material,fx,tabSizeM,g,h,cmoy,LaBcylSt89,LrBcylSt89);
        para.St89.LaBcy = LaBcylSt89;
        para.St89.rhoL = LrBcylSt89;
    end
    
    para.St89.g = g;
    para.St89.h = h;
    
    
elseif strcmp(model,'elasShellSt94')
    [TS, TSreduce, TAij] = ModelElasticShellSt94(fx,tabSizeM,cmoy,relasSt94);
    para.St94.rEl = relasSt94;    
    
elseif strcmp(model,'fluidcybtSt94')
    [TS, TSreduce, TAij] = ModelFluidBtCySt94(fx,tabSizeM,cmoy,rfluidSt94,bDfluidSt94,sfluidSt94);
    para.St94.rBcy = rfluidSt94;
    para.St94.bD = bDfluidSt94;
    para.St94.s = sfluidSt94;
    
elseif strcmp(model,'copepTFS')
    
    g = gCopTFS;
    h = hCopTFS;
    
    % (ref. Pieper and Holliday, J.Const.int.Explor.Mer.,1984)

    for freqIndex = 1:length(fx)
        f = fx(freqIndex);   % f in Hz
        k = 2 * pi * f / cmoy ;
        ka = k * tabSizeM;   
        for i = 1:length(tabSizeM)
            R2 = ModelTFS(ka(i),g,h);
            TSreduce(freqIndex,i) = 10*log10( R2/(4*pi) );
            TS(freqIndex,i) = 10*log10( tabSizeM(i)*tabSizeM(i)*R2/4 );
        end
    end
    TAij = 10 .^ ((TS)/10);  
    
    para.TFS.g = g;
    para.TFS.h = h;
    
elseif strcmp(model,'copepDWBA')  
    
    for freqIndex = 1:length(fx)
        for i = 1:length(tabSizeM)
            
            set_para_copepDWBA;
            para.simu.k  = 2 * pi * para.simu.freq1 * 1e3 / para.phy.cw; % wave number
            para.simu.ka = para.simu.k .* tabSizeM(i) ;	  %creates a vector of ka values

            [sigma_bs_L, para] = zoo_bscat_Arthur(para);
            
            TSreduce(freqIndex,i) = 10*log10(sigma_bs_L);	%calculates predicted target strength in dB.
            sigma_bs = ((tabSizeM(i)*para.shape.L_a)^(2))*sigma_bs_L;
            TS(freqIndex,i) = 10*log10(sigma_bs);
            TAij(freqIndex,i) = sigma_bs;
        end
    end

elseif strcmp(model,'largeEuphDWBA')  
    
    set_para_largeEuphDWBA;
    
    for freqIndex = 1:length(fx)
        
        for i = 1:length(tabSizeM)
            para.simu.f0 = fx(freqIndex)/1.e3;   % start frequecy in kHz
            para.simu.fe = fx(freqIndex)/1.e3;   % end frequency in kHz
        
            para.simu.freq0   = para.simu.f0:para.simu.df:para.simu.fe;
            para.simu.freq1   = para.simu.freq0;
    
        
            ns           = para.simu.min_ni; % number of sample points per wave length
            para.simu.k  = 2 * pi * para.simu.freq1 * 1e3 / para.phy.cw; % wave number
            kLmax        = max(para.simu.k) * para.shape.L * 1e-3 * 1.3; % 1+ 3 std(dL/L)
            para.simu.ni = max(para.simu.min_ni , ceil(kLmax*ns/(2*pi))); % integration points along z-axis

            para.simu.ka = para.simu.k .* tabSizeM(i) ;	  %creates a vector of ka values
        
            [sigma_bs_L, para] = zoo_bscat(para);
            TSreduce(freqIndex,i) = 10*log10(sigma_bs_L);	%calculates predicted target strength in dB.
            sigma_bs = ((tabSizeM(i)*para.shape.L_a)^(2))*sigma_bs_L;
            TS(freqIndex,i) = 10*log10(sigma_bs);
            TAij(freqIndex,i) = sigma_bs;
        end
    end
elseif strcmp(model,'smallEuphDWBA')  
    
    set_para_smallEuphDWBA;
    
    for freqIndex = 1:length(fx)
        
        for i = 1:length(tabSizeM)
            para.simu.f0 = fx(freqIndex)/1.e3;   % start frequecy in kHz
            para.simu.fe = fx(freqIndex)/1.e3;   % end frequency in kHz
        
            para.simu.freq0   = para.simu.f0:para.simu.df:para.simu.fe;
            para.simu.freq1   = para.simu.freq0;
    
        
            ns           = para.simu.min_ni; % number of sample points per wave length
            para.simu.k  = 2 * pi * para.simu.freq1 * 1e3 / para.phy.cw; % wave number
            kLmax        = max(para.simu.k) * para.shape.L * 1e-3 * 1.3; % 1+ 3 std(dL/L)
            para.simu.ni = max(para.simu.min_ni , ceil(kLmax*ns/(2*pi))); % integration points along z-axis

            para.simu.ka = para.simu.k .* tabSizeM(i) ;	  %creates a vector of ka values
        
            [sigma_bs_L, para] = zoo_bscat_Arthur(para);
            TSreduce(freqIndex,i) = 10*log10(sigma_bs_L);	%calculates predicted target strength in dB.
            sigma_bs = ((tabSizeM(i)*para.shape.L_a)^(2))*sigma_bs_L;
            TS(freqIndex,i) = 10*log10(sigma_bs);
            TAij(freqIndex,i) = sigma_bs;
        end
    end   
    elseif strcmp(model,'GelatinousDWBA')  % Ajout Arthur 08/03/2017 (en test) : tout organisme cylindrique non euph (corps de poisson, gelatineux...) , a partir de modele d'euphausiacee
    

    
    for freqIndex = 1:length(fx)
        
        set_para_GelatinousDWBA_Arthur;
        
        for i = 1:length(tabSizeM)
            para.simu.f0 = fx(freqIndex)/1.e3;   % start frequecy in kHz
            para.simu.fe = fx(freqIndex)/1.e3;   % end frequency in kHz
        
            para.simu.freq0   = para.simu.f0:para.simu.df:para.simu.fe;
            para.simu.freq1   = para.simu.freq0;
    
        
            ns           = para.simu.min_ni; % number of sample points per wave length
            para.simu.k  = 2 * pi * para.simu.freq1 * 1e3 / para.phy.cw; % wave number
            kLmax        = max(para.simu.k) * para.shape.L * 1e-3 * 1.3; % 1+ 3 std(dL/L)
            para.simu.ni = max(para.simu.min_ni , ceil(kLmax*ns/(2*pi))); % integration points along z-axis

            para.simu.ka = para.simu.k .* tabSizeM(i) ;	  %creates a vector of ka values
        
            [sigma_bs_L, para] = zoo_bscat_Arthur(para);
            TSreduce(freqIndex,i) = 10*log10(sigma_bs_L);	%calculates predicted target strength in dB.
            sigma_bs = ((tabSizeM(i)*para.shape.L_a)^(2))*sigma_bs_L;
            TS(freqIndex,i) = 10*log10(sigma_bs);
            TAij(freqIndex,i) = sigma_bs;
        end
    end  
     elseif strcmp(model,'MedusaeDWBA')  % Ajout Arthur 08/03/2017, d'apres Lawson, com perso
    

    
    for freqIndex = 1:length(fx)
        
        set_para_MedusaeDWBA_Arthur;
        
        for i = 1:length(tabSizeM)
            para.simu.f0 = fx(freqIndex)/1.e3;   % start frequecy in kHz
            para.simu.fe = fx(freqIndex)/1.e3;   % end frequency in kHz
        
            para.simu.freq0   = para.simu.f0:para.simu.df:para.simu.fe;
            para.simu.freq1   = para.simu.freq0;
    
        
            ns           = para.simu.min_ni; % number of sample points per wave length
            para.simu.k  = 2 * pi * para.simu.freq1 * 1e3 / para.phy.cw; % wave number
            kLmax        = max(para.simu.k) * para.shape.L * 1e-3 * 1.3; % 1+ 3 std(dL/L)
            para.simu.ni = max(para.simu.min_ni , ceil(kLmax*ns/(2*pi))); % integration points along z-axis

            para.simu.ka = para.simu.k .* tabSizeM(i) ;	  %creates a vector of ka values
        
            [sigma_bs_L, para] = zoo_bscat_Arthur(para);
            TSreduce(freqIndex,i) = 10*log10(sigma_bs_L);	%calculates predicted target strength in dB.
            sigma_bs = ((tabSizeM(i)*para.shape.L_a)^(2))*sigma_bs_L;
            TS(freqIndex,i) = 10*log10(sigma_bs);
            TAij(freqIndex,i) = sigma_bs;
        end
    end  
       
elseif strcmp(model,'gasBubbleDWBA')  
    % usage d�conseill� par Chu - ICES FAST Rome 2005
    
    set_para_gasBubbleDWBA;
    
    for freqIndex = 1:length(fx)
        % !!!! a revoir !!!!!!
        for i = 1:length(tabSizeM)
            para.simu.f0 = fx(freqIndex)/1.e3;   % start frequecy in kHz
            para.simu.fe = fx(freqIndex)/1.e3;   % end frequency in kHz
        
            para.simu.freq0   = para.simu.f0:para.simu.df:para.simu.fe;
            para.simu.freq1   = para.simu.freq0;
    
        
            ns           = para.simu.min_ni; % number of sample points per wave length
            para.simu.k  = 2 * pi * para.simu.freq1 * 1e3 / para.phy.cw; % wave number
            kLmax        = max(para.simu.k) * para.shape.L * 1e-3 * 1.3; % 1+ 3 std(dL/L)
            para.simu.ni = max(para.simu.min_ni , ceil(kLmax*ns/(2*pi))); % integration points along z-axis

            para.simu.ka = para.simu.k .* tabSizeM(i) ;	  %creates a vector of ka values
        
            [sigma_bs_L, para] = zoo_bscat(para);
            TSreduce(freqIndex,i) = 10*log10(sigma_bs_L);	%calculates predicted target strength in dB.
            sigma_bs = pi*tabSizeM(i)*tabSizeM(i)*sigma_bs_L;
            TS(freqIndex,i) = 10*log10(sigma_bs);
            TAij(freqIndex,i) = sigma_bs;
        end
    end  
    
elseif strcmp(model,'gasbladder')                  % AJOUT ALD DU 17/05/2013
    [TS, TSreduce, TAij] = gasbladder(fx,tabSizeM,Dkloser);
    para.gasblad.D = Dkloser;

 elseif strcmp(model,'gasbubbleYe')                  % Ajout BR 14/10/2013, mis a jour Arthur 17/01/2017
     [TS, TSreduce, TAij] = gasbubbleye2(fx,tabSizeM,Dye,elongationYe,rhoWaterYe,cWaterYe,gammaYe,etaYe,tauYe);
     para.gasbubb.Dye = Dye;
     para.gasbubb.elongationYe = elongationYe;
     para.gasbubb.rhoWaterYe = rhoWaterYe;
     para.gasbubb.cWaterYe = cWaterYe;
     para.gasbubb.gammaYe = gammaYe;
     para.gasbubb.etaYe = etaYe;
     para.gasbubb.tauYe = tauYe;
%     
 elseif strcmp(model,'gasbubbleScoulding')                  % Ajout Arthur 17/01/2017
     [TS, TSreduce, TAij] = gasbubbleScoulding(tiSc,mu_rSc,fx,tabSizeM,Dye,elongationYe,rhoWaterYe,cWaterYe,gammaYe,etaYe,tauYe);
     para.gasbubb.tiSc = tiSc;
     para.gasbubb.Dye = Dye;
     para.gasbubb.elongationYe = elongationYe;
     para.gasbubb.rhoWaterYe = rhoWaterYe;
     para.gasbubb.cWaterYe = cWaterYe;
     para.gasbubb.gammaYe = gammaYe;
     para.gasbubb.etaYe = etaYe;
     para.gasbubb.tauYe = tauYe;
     para.gasbubb.mu_rSc = mu_rSc;
end
