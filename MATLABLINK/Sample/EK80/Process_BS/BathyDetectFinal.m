function [bs_angA,bs_angP,bs_vol,indminA,indmaxA,nA,BsA,BsPH,phAt,phAl,angE,posA]=BathyDetectFinal(sv_range,bs_data,sv_data,anglesAt_data,anglesAl_data,AngTilt,meandep,A,T,psi)
%% 
bs_angA=[];bs_angPh=[];dataFile=[];ff=[];

ampl=sv_data;
%ampl=bs_data;
ampl(ampl==-inf)=nan;
ampl(isnan(ampl))=-180;
ampl2=[];ampl2=ampl-10*log10(sv_range);
ampl2(isnan(ampl2))=-180;
ampl(ampl>5)=-80;bs_data(bs_data>5)=-80;

[mm,indmm]=max(db2pow(ampl2'));
sv_range2=sv_range';
% meandep=nanmean(sv_range2(indmm));
% meandep=15;maxdep=200;
dep=meandep/cosd(abs(nanmean(AngTilt))+15); %dep=meandep+(dep/10);
% maxdep=dep+dep/5;
% mindep=dep-dep/5;
%mindep=10;maxdep=180;
[~,idmin]=min(abs(sv_range(1,:)-5));
[~,idrmax]=min(abs(sv_range(1,:)-dep));
ifrmax=size(bs_data,2);
idrmax=min(idrmax,size(bs_data,2));
img=db2mag(sv_data(:,idmin:idrmax));img(isnan(img))=0;
anglePH=anglesAt_data(:,idmin:end);anglePH(isnan(anglePH))=0;
sv_data(:,1:idmin)=-80;
%if abs(nanmean(AngTilt))<40
ipt=findchangepts((img),'Statistic','linear','MaxNumChanges',2);
if length(ipt)<2
    ipt=[idmin,size(bs_data,2)];
end

range=nanmean(sv_range);
[~,idrmin]=min(abs(sv_range(1,:)-10));
%[~,idrmax]=min(abs(sv_range(1,:)-120));

idmin=max(round(idmin+ipt(1)-((idmin+ipt(1))/5)),idrmin);
idma=min(round(idmin+ipt(2)+(ipt(2)/10)),size(sv_data,2));
%figure(66),findchangepts(phas,'Statistic','linear','MaxNumChanges',2);
% figure(77),findchangepts(ampl2,'Statistic','mean','MaxNumChanges',2);
%idma=size(sv_data,2);
% ampl2(:,1:idmin)=[];sv_range(:,1:idmin)=[];bs_data(:,1:idmin)=[];ampl(:,1:idmin)=[];anglesAt_data(:,1:idmin)=[];
ampl=ampl(:,idmin:idma);ampl2=ampl2(:,idmin:idma);sv_range=sv_range(:,idmin:idma);bs_data=bs_data(:,idmin:idma);
anglesAt_data=anglesAt_data(:,idmin:idma);anglesAl_data=anglesAl_data(:,idmin:idma);A=A(:,idmin:idma);
% phas=colfilt(anglesAt_data,[2 10],'sliding',@nanmean);
phas=anglesAt_data;
%phas=colfilt(anglesAt_data,[3 10],'sliding',@nanmean);phas(end-2:end,:)=anglesAt_data(end-2:end,:);
% indA(abs(diff(indA))>1000)=nanmean(indA);
np=size(bs_data,1);
magAmpl=db2mag(bs_data);
ne=1000;
x=sv_range(1,:);
bsAt=[];angt=[]; BsA=nan(np,ne);phAt=nan(np,ne);phAl=nan(np,ne);angE=nan(np,ne);BsPH=nan(np,ne);
angVal=1;
dr = nanmean(diff(x));

%%
R=meandep./cosd(AngTilt);
xx=sv_range(1,:);
for ip=1:np
    
    x=[];y=[];idA=[];idPh=[];qq=[];
    x=sv_range(ip,:);y=db2mag(ampl2(ip,:));x=x(:);y=y(:);ph=phas(ip,:);
    indmin=[];indmax=[];y(isnan(x))=[];ph(isnan(x))=[];x(isnan(x))=[];
    [envHigh, ~]=envelope(y,10,'peak');
    
    if abs(nanmean(AngTilt))<90
        
        ipt =findchangepts(cumsum(y),'Statistic','linear','MaxNumChanges',2);
        if length(ipt)<2
            ipt =findchangepts(y,'Statistic','linear','MaxNumChanges',2);
        end
        if length(ipt)<2
            ipt =findchangepts(ph,'Statistic','linear','MaxNumChanges',2);
            
        end
        
    else
        ipt =findchangepts(ph,'Statistic','linear','MaxNumChanges',2);
        if length(ipt)<2
            
            ipt =findchangepts(cumsum(y),'Statistic','linear','MaxNumChanges',2);
        end
    end
    if length(ipt)==1
        ipt(2)=length(y);
    end
    idV(ip,:)=[ipt(1) ipt(2)];
    
    indmin=ipt(1);indmax=ipt(2);
    
    
    
    
    peakThld = max(y(indmin:indmax)) * 0.5 - sqrt(eps);
    %     % calculate the centroids for each peak
    pkx = arrayfun(@(lm,rm,th) (((y(lm:rm)).*(y(lm:rm)>=th))'*x(lm:rm))/sum((y(lm:rm)).*(y(lm:rm)>=th)),indmin,indmax,peakThld);
    
    [~,indA(ip)]=min(abs(x-pkx));
    
    
    
    posA(ip)=x(indA(ip));
    indminA(ip)=round(indA(ip))-max(round(indA(ip)*tand(abs(AngTilt(ip)))*angVal*pi/180),2); %
    indmaxA(ip)=min(round(indA(ip))+max(round(indA(ip)*tand(abs(AngTilt(ip)))*angVal*pi/180),2),size(bs_data,2));%
    ph=phas(ip,:);ph(isnan(ph))=0;
    
    p0=polyfit(indmin:indmax,(ph(indmin:indmax)),2);
    val=p0(1)*(indmin:indmax).^2+p0(2)*(indmin:indmax)+p0(3);
    [~,idx0]=min(abs(val-0));
    [~,idx1]=min(abs(val-1));
    indPh(ip)=idx0+indmin-1;
    PosPh(ip)=x(indPh(ip));
    indminPh(ip)=round(indPh(ip))-max(round(indPh(ip)*tand(abs(AngTilt(ip)))*angVal*pi/180),2); %
    indmaxPh(ip)=min(round(indPh(ip))+max(round(indPh(ip)*tand(abs(AngTilt(ip)))*angVal*pi/180),2),size(bs_data,2));%
    nPh(ip)=indmaxPh(ip)-indminPh(ip);
    
    
    bs_smooth=bs_data(ip,:);
    idA=indminA(ip):indmaxA(ip);
    bssA=bs_smooth(idA);bssA(abs(anglesAt_data(ip,idA))>3)=nan;
    idp=indminPh(ip):indmaxPh(ip);
    bssph=bs_smooth(idp);bssph(abs(anglesAt_data(ip,idp))>3)=nan;
    
    BsA(ip,1:length(bssA))=bssA;
    BsPH(ip,1:length(bssph))=bssph;
    phAt(ip,1:length(bssA))=anglesAt_data(ip,idA);
    phAl(ip,1:length(bssA))=anglesAl_data(ip,idA);
    angE(ip,1:length(bssA))=repmat(AngTilt(ip),length(bssA),1);
    %bsAt=[bsAt bssA];
    %an=anglesAt_data(ip,idA);angt=[angt an];
    bs_angA(ip)=mag2db(nanmedian(db2mag(bssA)));%10*log10(nanmedian(10.^(bssA./10)));
    bs_angP(ip)=mag2db(nanmedian(db2mag(bssph)));%10*log10(nanmedian(10.^(bssph./10)));
    
   indmin(ip)=max(ipt(1)-max(round(ipt(1)*tand(abs(AngTilt(ip))+10)*10*pi/180),2),1); %
   indmax(ip)=min(ipt(2)+max(round(ipt(2)*tand(abs(AngTilt(ip))+10)*10*pi/180),2),size(bs_data,2));%
   idV(ip,:)=[ indmin(ip) indmax(ip)];       
    yp=db2pow(ampl(ip,:));yp=yp.*(sv_range(ip,:)./posA(ip)).^2;
    
    % yp=yp.*((A(ip,:))./(T*db2pow(psi)*posA(ip)^2));
    yp=yp( indmin(ip):indmax(ip));yp(isnan(yp))=[];
    %qq=cosd(AngTilt(ip)).*trapz(dr,yp);
    %if abs(nanmean(AngTilt))>10
    %qq=cosd(AngTilt(ip)).*sum(yp.*(cosd(AngTilt(ip))^2./(cosd(AngTilt(ip)+anglesAt_data(ip,indmin(ip):indmax(ip))).^2)).*dr);
    qq=cosd(AngTilt(ip)).*trapz(dr,yp);
    %qq=cosd(AngTilt(ip)).*sum(dr.*yp);
%     else
%             qq=cosd(AngTilt(ip)).*sum(yp.*dr);
% 
%     end
   bs_vol(ip)=pow2db(qq);
%    if abs(nanmean(AngTilt))<20
%        bs_vol(ip)=nanmax( ampl(ip,indmin:indmax))+10*log10(1500*T/2);
%     end
    %  figure; plot(x,bs_data(ip,:));hold on; plot(x(idA),bs_data(ip,idA),'r.');grid on
    %      hold on; plot(posA(ip),pow2db(y(indA(ip))),'k*')
    %figure; plot(x,ph);hold on; plot(x(idA),ph(idA),'r*');grid on
end
nA=indmaxA-indminA;
BsA(BsA==0)=nan;
%[Val, str] = stats(BsA(:), 'Seuil', [1 99]);
%BsA(BsA<Val.Quant_x(1))=nan;BsA( BsA > Val.Quant_x(2))=nan;
% nPh=indmaxPh-indminPh;
% figure(1);imagesc(sv_data');colorbar; colormap('jet');
% hold on;plot(1:size(bs_data,1),indminA+idmin,'k','linewidth',2);plot(1:size(bs_data,1),indmaxA+idmin,'k','linewidth',2);hold off;title([num2str(nanmean(AngTilt))]);
% hold on;plot(1:size(bs_data,1),indminPh+idmin,'r','linewidth',2);plot(1:size(bs_data,1),indmaxPh+idmin,'r','linewidth',2);
% hold on;plot(1:size(bs_data,1),idV(:,1)+idmin,'c','linewidth',2);plot(1:size(bs_data,1),idV(:,2)+idmin,'c','linewidth',2);hold off;
xx=fillmissing(xx,'linear');
figure(3),plot(1:length(AngTilt),bs_angA,1:length(AngTilt),bs_vol,'r',1:length(AngTilt),bs_angP,'k');grid on; title([num2str(nanmean(AngTilt))]);
figure(4),plot(angE(:)+phAt(:),BsA(:),'.');grid on; title([num2str(nanmean(AngTilt))]);
% figure, plotyy(x,db2pow(ampl(ip,:)),x,anglesAt_data(ip,:))
% figure, plot(x(indminA(ip):indmaxA(ip)),pow2db(y(indminA(ip):indmaxA(ip))),'.')
%  ph=anglesAt_data(ip,:);figure(4); plotyy(x,y,x(idp),ph(idp));grid on
% figure(5); plot(x,bs_smooth,x(idA),(bs_smooth(idA)),'k.',x(idp),(bs_smooth(idp)),'r.');grid on
ampl=bs_data;
figure(1);imagesc(1:size(ampl,1),xx,ampl');colorbar; colormap('jet');title('BS');
hold on;plot(1:size(ampl,1),xx(idV(:,1)),'k','linewidth',2);plot(1:size(ampl,1),xx(idV(:,2)),'k','linewidth',2);hold on
hold on;plot(1:size(ampl,1),xx(indminA),'g','linewidth',2);plot(1:size(ampl,1),xx(indmaxA),'g','linewidth',2);hold on;
hold on;plot(1:size(ampl,1),xx(indminPh),'c','linewidth',2);plot(1:size(ampl,1),xx(indmaxPh),'c','linewidth',2);hold off
title([num2str(nanmean(AngTilt))]);%caxis([-40 0]);

% figure(2);imagesc(1:size(ampl,1),xx,phas');colorbar; colormap('jet');title('BS');
% hold on;plot(1:size(ampl,1),xx(idV(:,1)),'k','linewidth',2);plot(1:size(ampl,1),x(idV(:,2)),'k','linewidth',2);hold on
% hold on;plot(1:size(ampl,1),xx(indminPh),'r','linewidth',2);plot(1:size(ampl,1),xx(indmaxPh),'r','linewidth',2);hold off
% title([num2str(nanmean(AngTilt))]);

