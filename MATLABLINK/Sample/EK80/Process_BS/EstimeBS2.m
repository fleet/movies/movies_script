function [data,Ech]=EstimeBS2(RepRaw,CalibFile)
RepPr=strtrim([RepRaw,filesep,'processed']);

filename = dir( fullfile(RepPr,'*.pr'));   
filename = {filename.name}';
filename = fullfile(RepPr,filename);
nbFilename = numel(filename);
%% detect minDepth
angT=[];
for il = 1:nbFilename
    load('-mat',filename{il},'Incl');
    angT(il)=Incl;
end
[~,indAng]=min(abs(angT));
load('-mat',filename{indAng},'sv_data','sv_range');

[~,idmin]=min(abs(sv_range(1,:)-5));
[mm,indmm]=max((sv_data(:,idmin:end)'));
range=nanmean(sv_range);
meandep=range(round(idmin+nanmedian(indmm)));
%%
data=table();BSmeanA=[];BSmeanPh=[];
for il = 1:nbFilename
    sv_data=[]; sv_range=[]; anglesAt_data=[]; anglesAl_data=[]; anglesAt_data=[];
    roll=[];  freq=[]; T=[]; incl=[]; Dir=[];  config=[]; soundvelocity=[];sv_data3=[];bs_data=[];absol=[]; heave=[]; pitch=[];
    incl=[];fc=[];Teff=[];psi=[];lLobe0=[];dataFile=[];Lat=[];Long=[];
    il
    load('-mat',filename{il},'sv_data','sv_range','anglesAt_data','anglesAl_data','absorpt',...
        'roll','freq','T1','Teff','Incl','Dir','soundvelocity','MotionParam','ouv_at_1w','ouv_al_1w','GainIn','gain_cal',...
        'absol','heave','pitch','Tid','fc','psi','ouv_at','ouv_al','SaCorrIn','cel','Lat','Long','heading','sa_corr','Te');
    
    
    %%
    
         [~, index] = min(abs(CalibFile.Freq-freq/1000));
            id=find(CalibFile.Freq==CalibFile.Freq(index) & fc/1000==CalibFile.Transducteur);id=id(1);
            gain_cal=CalibFile.GaindB(id);
            sa_corr=CalibFile.Sacor(id) ;
    %INSONIFIED AREA
    
    shift_incl_angl=0;
    incl=Incl+shift_incl_angl-roll;
      
    sv_data=sv_data+2*(absorpt-absol)*sv_range;
    %sv_data=sv_data+2*(GainIn+ 20*log10(freq/fc)-gain_cal) + 2*(SaCorrIn-sa_corr);
    sv_data=sv_data+2*(GainIn-gain_cal) + 2*(SaCorrIn-sa_corr);
    
    
    area=[];
    incl=incl';
    t1=cel*Teff/2./abs(sind(bsxfun(@plus,anglesAt_data,incl)));
    newt1=sv_range.*abs(sind(incl)).*(sqrt(1+2*(cel*Teff/2)./sv_range./(sind(incl)).^2)-1);
    t2= ouv_at*pi/180.*bsxfun(@rdivide,sv_range,cosd(incl));
        
    ouv_al=ouv_al_1w;ouv_at=ouv_at_1w;
    alpha=6.0206;
    beta=0.18;
    
    area=ouv_al*pi/180.*sv_range.*min(newt1,t2);
   
    %figure,plot(sv_range(10,:),newt1(10,:),sv_range(10,:),t2(10,:),'r',sv_range(10,:),min(t2(10,:),newt1(10,:)),'ko');grid on;
    
    lLobe0=[];
    lLobe0=-6.0206*( (2*anglesAl_data/ouv_al_1w*0).^2 + (2*anglesAt_data/ouv_at_1w).^2 - 0.18*(((2*anglesAl_data/ouv_al_1w*0).^2).*((2*anglesAt_data/ouv_at_1w).^2))); % XAVIER
  
    [np,ns]=size(sv_data);
    % BS
    cor_sv2bs=[];
    
        cor_sv2bs=psi+20*log10(sv_range)+10*log10(cel*Teff/2)-10*log10(area)-lLobe0;
        
    bs_data=sv_data+cor_sv2bs;
        
    %%
    BsA=[];phAt=[];phAl=[];angE=[];BsPH=[];bs_angP=[];posA=[];indminA=[];indmaxA=[];
    [bs_angA,bs_angP,bs_vol,indminA,indmaxA,nA,BsA,BsPH,phAt,phAl,angE,posA]=BathyDetectFinal(sv_range,bs_data,sv_data,anglesAt_data,anglesAl_data,incl,meandep,area,Teff,psi);
%     figure(5);pcolor(bs_data);hold off
%     title('Compensated BS(dB)');shading interp
%     colormap(gray);colorbar;caxis([-50 0]);
    
    %%
    Ech{il}.Bs=BsA;Ech{il}.At=phAt;Ech{il}.Al=phAl;Ech{il}.ang=angE;Ech{il}.BsP=BsPH;
    ff=repmat(freq,np,1);ffc=repmat(fc,np,1);TT=repmat(T1*10^6,np,1);tilt=repmat(Incl,np,1);
    Gain=repmat(gain_cal,np,1);
    dataFile=table();
    
    dataFile.ang=incl;dataFile.roll=roll';dataFile.bs_angA=bs_angA';dataFile.bs_angPh=bs_angP';dataFile.bs_vol=bs_vol';
    dataFile.nA=nA';dataFile.ff=ff;dataFile.fnom=ffc;dataFile.range=posA';
    dataFile.Puls=TT;dataFile.angIni=incl-shift_incl_angl;dataFile.tilt=tilt;dataFile.lat=Lat';dataFile.long=Long';
    dataFile.Gain=Gain;dataFile.cap=heading(1:length(roll))';
    data = vertcat(data,dataFile);
    
         %save('-mat',filename{il},'idA','idPh','incl','bs_data','idV','posA','bs_bottom','-append');
    
    
    
    
end

end
%%
function [shiftAng]=EstimShift(d,tide)
d=d(all(~isnan(d),2),:);
b=sortrows(d);

NomAngle=b(:,1);
Av_Range=b(:,2);
avroll=b(:,3);


zoff=(22.5+7.5)/100; %mounting: 22.5cm,  EK60: 7.5 cm;
xoff=-25/100; %
yoff=2/100;
m_along_ship_offset=-6.53+xoff;m_athwart_ship_offset=4.87+yoff;m_depth_offset=1.92+zoff;%thalia
%m_along_ship_offset=14.45;m_athwart_ship_offset=0.2;m_depth_offset=6.95;%thalassa
m_transRotationAngleRad=0;m_transFaceAlongAngleOffsetRad=0;


for k=1:2001
    shift(k)=(k-1001)*0.01;
    shift_angle=NomAngle'+shift(k);
    
    depth=zeros(1,length(Av_Range));
    for j=1:length(Av_Range)
        m_transFaceAthwarAngleOffsetRad=pi/180*(-shift_angle(j));
        headingRad=0;pitchRad=0;rollRad=pi/180*avroll(j);sensorHeaveMeter=0;Range=Av_Range(j);AlongTSAngle=0; AthwartTSAngle=0;
        
        %     Bathy=Av_Range'.*cosd(shift_angle)-tide_2;
        [PosRel,PosAbsTrans]= ComputeWorldPositionER60_Thalia(m_along_ship_offset,m_athwart_ship_offset,m_depth_offset,m_transRotationAngleRad,m_transFaceAlongAngleOffsetRad,m_transFaceAthwarAngleOffsetRad,headingRad,pitchRad,rollRad,sensorHeaveMeter, Range, AlongTSAngle, AthwartTSAngle);
        depth(j)=PosRel(3)+PosAbsTrans(3)-tide(j);
    end
    Av_Bathy(k)=mean(depth);
    std_Bathy(k)=std(depth);
end

% plot(shift,Av_Bathy,'.r')
% hold on
% plot(shift,Av_Bathy+std_Bathy,'.k')
% hold on
% plot(shift,Av_Bathy-std_Bathy,'.k')

rel_std_Bathy=std_Bathy./Av_Bathy;
id=rel_std_Bathy==min(rel_std_Bathy);
shiftAng=shift(id);
end
%%
function [d_ping,R_ping]=RangeDetect(sv_range,anglesAt_data,anglesAl_data,indminA,indmaxA,MotionParam)


% Range
zoff=(22.5+7.5)/100; %mounting: 22.5cm,  EK60: 7.5 cm;
xoff=-25/100; yoff=2/100;

m_along_ship_offset=-6.53+xoff;m_athwart_ship_offset=4.87+yoff;m_depth_offset=1.92+zoff;%thalia
m_transRotationAngleRad=0;m_transFaceAlongAngleOffsetRad=0;m_transFaceAthwarAngleOffsetRad=pi/180*(-Z_ang);

d_ping=[];R_ping=[];


for ip=1:np
    idA=indminA(ip):indmaxA(ip);
    Range=[];
    
    headingRad=pi/180*MotionParam.heading(ip);pitchRad=pi/180*MotionParam.pitch(ip);rollRad=pi/180*MotionParam.roll(ip);sensorHeaveMeter=MotionParam.heave(ip);Range=sv_range(ip,idA);
    AlongTSAngle=0*pi/180*anglesAl_data(ip,idA); AthwartTSAngle=pi/180*anglesAt_data(ip,idA);
    if  ~isempty(indA)
        depth=[];
        for kk=1:length(Range)
            [PosRel,PosAbsTrans]= ComputeWorldPositionER60_Thalia(m_along_ship_offset,m_athwart_ship_offset,...
                m_depth_offset,m_transRotationAngleRad,m_transFaceAlongAngleOffsetRad,m_transFaceAthwarAngleOffsetRad,...
                headingRad,pitchRad,rollRad,sensorHeaveMeter, Range(kk), AlongTSAngle(kk), AthwartTSAngle(kk));
            depth(kk)=PosRel(3)+PosAbsTrans(3);
        end
        
        d_pingA(ip)=nanmean(depth);
        R_pingA(ip)=nanmean(Range);
    end
    
    
end


end



