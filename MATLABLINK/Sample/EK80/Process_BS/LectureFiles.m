

function []=LectureFiles(RepRaw,Environment,CalibFile)


fileRaw = dir( fullfile(RepRaw,'*.raw'));   %# list all *.xyz files
fileRaw = {fileRaw.name}';
nbFilename = numel(fileRaw);
fname = fullfile(RepRaw,fileRaw);

fileParam=dir( fullfile(RepRaw,'*.mat'));
load(fullfile(fileParam.folder, fileParam.name));
%%
for il=1:nbFilename
    sign=+1;
    Incl=TiltAng(il);
    Dir=LineDir(il);
    Tid=tide(il);
    
    [RawData.Data] = ReadEK80RawFile(fname{il});
    % Perform basic processing of the raw complex sample data
    [BasicProcessedData] = BasicProcessData(RawData);
    [nf,~]=size(RawData.Data.PingData);
    % check the number of file (transducer)
    for ifile=1:nf
        
        %
        cel=Environment.CelSurface;
        fc=str2num(BasicProcessedData(ifile).ChannelData.Transducer.Frequency);
        Cs=Environment.Cs; Csim=Environment.Csim;% Fiche transducteur Simrad
        
        len=[];
        for ip=1:size(RawData.Data.PingData,2)
            len(ip)=length(BasicProcessedData(ifile).ProcessedSampleData(ip).sv);
            
        end
        len(len==0)=[];
        np=length(len);
        
        
        roll=[];heave=[];pitch=[];freq=[];T1=[];soundvelocity=[];absorptionCoefficients=[];MotionParam=[];Lat=[];Long=[];Te=[];
        sv_data=nan(np,max(len));sv_range=nan(np,max(len));anglesAl_data=nan(np,max(len));anglesAt_data=nan(np,max(len));
        power=nan(np,max(len)); sp=nan(np,max(len)); range=nan(np,max(len)); sp_range=nan(np,max(len));
        ns=1;
        for ip=1:size(RawData.Data.PingData,2)
            
            if ~isempty(RawData.Data.PingData(ifile, ip).MotionData)
                if ~isfield(RawData.Data.PingData(ifile, ip).MotionData,'heave')
                    heave(ns)=0;roll(ns)=0;pitch(ns)=0;heading(ns)=nan;
                else
                    EnvironmentData=RawData.Data.PingData(ifile, ip).EnvironmentData;
                    heave(ns)=RawData.Data.PingData(ifile, ip).MotionData.heave;
                    roll(ns)=RawData.Data.PingData(ifile, ip).MotionData.roll;
                    pitch(ns)=RawData.Data.PingData(ifile, ip).MotionData.pitch;
                    heading(ns)=RawData.Data.PingData(ifile, ip).MotionData.heading;
                end
                
                if (isfield (RawData.Data.PingData(ifile, ip),'NMEAData') && isfield(RawData.Data.PingData(ifile, ip).NMEAData,'lat'))
                    Lat(ns)=RawData.Data.PingData(ifile, ip).NMEAData.lat;
                    Long(ns)=RawData.Data.PingData(ifile, ip).NMEAData.long;
                else
                    Lat(ns)=nan; Long(ns)=nan;
                end
                freq(ns)=RawData.Data.PingData(ifile, ip).ParameterData.frequencyUsed;
                T1(ns)=RawData.Data.PingData(ifile, ip).ParameterData.pulseLength;
                Te(ns)=RawData.Data.PingData(ifile,ip).ParameterData.sampleInterval ;
                soundvelocity(ns)=RawData.Data.PingData(ifile, ip).EnvironmentData.soundSpeed;
                absorptionCoefficients(ns) = EstimateAbsorptionCoefficients(RawData.Data.PingData(ifile, ip).EnvironmentData  ,mode(freq(1,:)));
                
                ss=length(BasicProcessedData(ifile).ProcessedSampleData(ip).sv);
                sv_data(ns,1:ss)=BasicProcessedData(ifile).ProcessedSampleData(ip).sv';
                sv_range(ns,1:ss)=(BasicProcessedData(ifile).ProcessedSampleData(ip).svRange').*(soundvelocity(1)/cel);
                anglesAl_data(ns,1:ss)=BasicProcessedData(ifile).ProcessedSampleData(ip).alongship;
                anglesAt_data(ns,1:ss)=BasicProcessedData(ifile).ProcessedSampleData(ip).athwartship;
                power(ns,1:ss)=BasicProcessedData(ifile).ProcessedSampleData(ip).power';
                sp(ns,1:ss)=BasicProcessedData(ifile).ProcessedSampleData(ip).sp';
                sp_range(ns,1:ss)=BasicProcessedData(ifile).ProcessedSampleData(ip).spRange';
                range(ns,1:ss)=BasicProcessedData(ifile).ProcessedSampleData(ip).range';
                ns=ns+1;
            end
        end
        
        sv_range(sv_range<min(min(abs(sv_range))))=min(min(abs(sv_range)));
        freq=mode(freq(1,:));
        T1=mode(T1(1,:));
        Te=mode(Te(1,:));
        soundvelocity=mode(soundvelocity(1,:));
        absorptionCoefficients=mode(absorptionCoefficients(:,1));
        MotionParam.roll=roll;MotionParam.heave=heave;MotionParam.pitch=pitch;MotionParam.heading=heading;
        
        psi=str2num(BasicProcessedData(ifile).ChannelData.Transducer.EquivalentBeamAngle)+10*log10((fc/freq)^2);
        if isfield(RawData.Data.ConfigurationData.Transceivers(ifile).Channels,'PulseLength')
            pulselengthindex=find(str2num(RawData.Data.ConfigurationData.Transceivers(ifile).Channels.PulseLength)<T1+0.000001 & str2num(RawData.Data.ConfigurationData.Transceivers(ifile).Channels.PulseLength)>T1-0.000001);
        end
        if isfield(RawData.Data.ConfigurationData.Transceivers(ifile).Channels,'PulseDuration')
            pulselengthindex=find(str2num(RawData.Data.ConfigurationData.Transceivers(ifile).Channels.PulseDuration)<T1+0.000001 & str2num(RawData.Data.ConfigurationData.Transceivers(ifile).Channels.PulseDuration)>T1-0.000001);
        end
        gainTable       = str2num(RawData.Data.ConfigurationData.Transceivers(ifile).Channels.Transducer.Gain);
        SaCorrTable       = str2num(RawData.Data.ConfigurationData.Transceivers(ifile).Channels.Transducer.SaCorrection);
        %
        [~, index] = min(abs(CalibFile.Freq-freq/1000));
        id=find(CalibFile.Freq==CalibFile.Freq(index) & fc/1000==CalibFile.Transducteur);id=id(1);
        gain_cal=CalibFile.GaindB(id);
        sa_corr=CalibFile.Sacor(id) ;
        beamWidth_al=str2num(RawData.Data.ConfigurationData.Transceivers(ifile).Channels.Transducer.BeamWidthAlongship);
        beamWidth_ath=str2num(RawData.Data.ConfigurationData.Transceivers(ifile).Channels.Transducer.BeamWidthAthwartship);
        
        GainIn=gainTable(pulselengthindex);
        SaCorrIn=SaCorrTable(pulselengthindex);
        %         sv_data=sv_data+2*(GainIn+ 20*log10(freq/fc)-gain_cal) + 2*(SaCorrIn-sa_corr);
        %
        EnvironmentData.soundSpeed=Environment.soundSpeed;
        EnvironmentData.temperature=Environment.temperature;
        EnvironmentData.salinity=Environment.salinity;
        
        [absorpt] = EstimateAbsorptionCoefficients(EnvironmentData,freq);
        Teff=T1.*10.^(sa_corr/5);
        absol=absorptionCoefficients;
        %         sv_data=sv_data+2*(absorpt-absol)*sv_range;
        %ouverture mesur� en bassin adapt�e � la zone
        ouv_al=beamWidth_al*(5/7)*(5.5/5.2)*(Cs/Csim)*(fc/freq);
        ouv_at=beamWidth_ath*(5/7)*(5.5/5.2)*(Cs/Csim)*(fc/freq);
        %ouverture one way du mod�le Simrad
        ouv_al_1w=beamWidth_al*(fc/freq);
        ouv_at_1w=beamWidth_ath*(fc/freq);
        
        ProcessedSampleDataVector = struct('range',range,'power',power,'alongship',anglesAl_data,'athwartship',anglesAt_data,'spRange',sp_range,'sp',sp,'svRange',sv_range,'sv',sv_data);
        EnvironmentDatagram=EnvironmentData;
        EnvironmentDataVector=Environment;
        
        shift_incl_angl=0;ZA=[];RA=[];A_avg=[];
        incl=Incl+shift_incl_angl-roll;
        avroll(il)=nanmean(roll);
        standroll(il)=nanstd(roll);
        ang_avg=Incl+shift_incl_angl-avroll(il);
        Z_ang=Incl+shift_incl_angl;
        
        
        
        
        
        
        
        %%
        %%% sauvegarde des donn�es
        
        
        [Rep,nameboite,~] = fileparts(fname{il});
        if ~isfolder(fullfile(Rep,'processed'))
            mkdir(fullfile(Rep,'processed'));
        end
        Rep1=[];
        Rep1=strtrim([Rep,filesep,'processed',filesep,nameboite,'_',num2str(freq/1000),'_',num2str(Incl),'_',num2str(T1),'_','.pr']);
        
            
        save('-mat', Rep1, '-v7.3','sv_data','Lat','Long','sv_range','anglesAt_data','anglesAl_data','ouv_al','ouv_at','ouv_al_1w','ouv_at_1w',...
            'roll','freq','T1','Teff','Incl','Dir','soundvelocity','shift_incl_angl','heading','MotionParam','SaCorrIn',...
            'absol','heave','pitch','fc','cel','Tid','range','sa_corr','gain_cal','psi','absorpt','GainIn','Te','ProcessedSampleDataVector');
        
    end
    clear RawData.Data BasicProcessedData
end