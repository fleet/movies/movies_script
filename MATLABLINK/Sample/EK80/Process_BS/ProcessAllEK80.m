pathtot=uigetdir('.\Elorn');
d = dir(pathtot);
isub = [d(:).isdir];
nameFolds = {d(isub).name}';
nameFolds(ismember(nameFolds,{'.','..'})) = [];
files=nameFolds;

% add path for reading EK80 files in raw format
mydir  = pwd;
idcs   = strfind(mydir,'\')
pathUseful = [mydir(1:idcs(end)-1) '\Codes Lectures de Base'];
addpath(genpath(pathUseful)) 

%addpath('./ioFiles');

%% lecture et stockage des donn�es sous format matlab
%Rem2040;
EnvironmentData.soundSpeed=1495;EnvironmentData.temperature=11.7;EnvironmentData.salinity=34.8;
EnvironmentData.acidity=8;EnvironmentData.depth=3;
EnvironmentData.Cs=1492.3; EnvironmentData.Csim=1494.7;% Fiche transducteur Simrad
EnvironmentData.CelSurface=1492.3;
load('CalibFileRem2040.mat');
h = waitbar(0,'Please wait...');
for iFile=1:length(files)
    iFile
    waitbar(iFile / length(files))
    Rep=strtrim([pathtot,filesep,strtrim(files{iFile})]);
    LectureFiles(Rep,EnvironmentData,CalibFile);
end
%% Estimation BS
h = waitbar(0,'Please wait...');
data=[];dataEch=[];
%%
for iFile=1:length(files)
    %%
    iFile
    waitbar(iFile / length(files))
    Rep=strtrim([pathtot,filesep,strtrim(files{iFile})]);
    [data{iFile},dataEch{iFile}]= EstimeBS2(Rep,CalibFile);
  
end
%save dataRascass_1deg2020 data dataEch -v7.3
%%
