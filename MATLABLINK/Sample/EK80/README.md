This directory contains Matlab scripts for processing EK60/EK80 raw data files that have been used to compare with MOVIES3D processing for water column TS/Sv calculation in CW and FM
It also contains scripts for processing EK60/EK80 raw data files for BS calcul following method described in https://link.springer.com/article/10.1007/s11001-018-9348-5
