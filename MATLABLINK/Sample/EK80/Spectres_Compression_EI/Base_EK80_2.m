%% 
clear all; %close all
tic
% param�tres d'analyse
thick=2; % (m) �paisseur de la couche
df=1000; % (Hz) frequency sampling
compute_from_MF=1; %calcul des spectres � partir du signal compress� (ou pas)
integration_depth = (thick:thick:50);


% param�tres borne et calibrations
% frequencies_begin = [18, 34, 45, 90, 160, 280]; % bornes de fr�quences, � enlever?
% frequencies_end = [18, 45, 95, 170, 260, 450];
% frequencies_begin = [15.4, 36.2, 45, 90, 160, 280]; % bornes de fr�quences, � enlever?
% frequencies_end = [22.5, 40.9, 90, 170, 260, 450];
frequencies_begin = [18, 38, 45, 95, 180, 260]; % bornes de fr�quences, � enlever?
frequencies_end = [18, 38, 90, 160, 250, 420];
frequencies_center = [18, 38, 70, 120, 200, 333];

Gain_init_GPT = [23.16,27.05];
Sa_init_GPT = [-0.72, -0.6899];
Gain_cor_GPT = [23.22,26.97];
Sa_cor_GPT = [-0.6, -0.63];
%TrList_calibration = xml2struct('C:\Users\ablanlue\Documents\MATLAB\EK80_Matlab\Script_Arthur\TrList_calibration.xml'); % chemin calibration
% TrList_calibration = xml2struct('L:\GO SARS formation ICES\DATA HULL\EK80-DOCUMENTS-FOLDER\Calibration\TrList_calibration.xml'); % chemin calibration
%TrList_calibration = xml2struct('C:\ProgramData\Simrad\EK80\ConfigSettings\TrList_calibration.xml'); % chemin calibration
TrList_calibration = xml2struct('J:\PELGAS16\EK80\Calibration\CalibrationResults\TrList_calibration.xml'); % chemin calibration
Transducer = TrList_calibration.Root.TransducerData.Transducer;


% pour une raison inexpliqu�e (version matlab suivant le poste?) parfois Transducer est un
% cell array ou une structure. On le passe en cell array.
if ~iscell(Transducer)
    for tr=1:size(Transducer,2)
        TransducerCell{tr}=Transducer(tr);
    end
    Transducer=TransducerCell;
    clear  TransducerCell;
end

% % 24/05/17 SUPRESSION TEMPORAIRE DE LA CALIBRATION (tant que celle de
% % l'EK80 n'est pas retrait�e (suppression des oscillations))
% for tr=1:size(Transducer,2)
%     Transducer(tr).Attributes.TransducerName=' ';
% end

% modification de la calibration pour int�grer d'autres erreurs
% (ici ramping diff�rent acquisition/calibration)
% 24/05/17 LAISSER RAMPING A 0
erreur_ramping=0;

% param�tres echogrammes 
promptText      = {'Range [m]:','Range resolution [m]:','Minimum colour scale threshold [dB]:','Maximum colour scale threshold [dB]:'};
dialogTitle     = 'Echogram settings';
defaultValues   = {'200','1e-1','-70','-40'};
answer          = inputdlg(promptText,dialogTitle,1,defaultValues);

rangeEchogram               = str2num(answer{1});
rangeResolutionEchogram     = str2num(answer{2});
colorMinimum                = str2num(answer{3});
colorMaximum                = str2num(answer{4});
grid on;
% param�tres EI
Type_echo_integration = 'ping'; % 'ping', 'time' or 'distance' (distance pas encore impl�ment�)
nbr_ping_echo_integre = 2; % nbr de ping de l'ESU pour EI en ping
nbr_second_echo_integre = 60; % nbr de seconde de l'ESU pour EI en dur�e

path_save = 'F:\data\EK80\';

%% Choix des fichiers � lire
[RawData] = ReadEK80Data; %lectures des fichiers raw (Script Simrad)
nFiles = length(RawData);
nsdr=size(RawData(1).Data.PingData,1);

%% Preparation des donn�es pour l'EI

[Sv, frequencies , Time, range_Sv, resol_f_Sv,thick_eff_Sv] = Preparation_EI(RawData,df,Gain_init_GPT, Sa_init_GPT, Gain_cor_GPT, Sa_cor_GPT, Transducer, frequencies_begin, frequencies_end,erreur_ramping,compute_from_MF);

date_min = strsplit(datestr(min(Time{1,1})));
date_max = strsplit(datestr(max(Time{nFiles,1})));
time_min = strsplit(date_min{2},':');
time_max = strsplit(date_max{2},':');
time_min = strcat(time_min{:});
time_max = strcat(time_max{:});

savename = ['PELGAS16-EK80_',date_min{1},'_',time_min,'_to_',time_max,'_df',num2str(df),'.mat'];
save([path_save,savename],'Sv', 'frequencies', 'range_Sv', 'Time', 'resol_f_Sv','thick_eff_Sv');

%% Echograms

[BasicProcessedData] = BasicProcessData(RawData);

% Create and display simple Sv and Sp echograms
[svFigureHandle] = CreateEchograms2(BasicProcessedData, rangeEchogram, rangeResolutionEchogram, colorMinimum, colorMaximum);

clear 'BasicProcessedData';


for f = 1:length(frequencies_center)
saveas(figure(f),[path_save,'PELGAS16-EK80_',date_min{1},'_',time_min,'_to_',time_max,'_',num2str(frequencies_center(f)),'kHz'],'png');
saveas(figure(f),[path_save,'PELGAS16-EK80_',date_min{1},'_',time_min,'_to_',time_max,'_',num2str(frequencies_center(f)),'kHz'],'fig');
end

% close all

%% Echo-int�gration
if thick<max(cat(2,cat(1,thick_eff_Sv{1,:})))
    thick=max(cat(2,cat(1,thick_eff_Sv{1,:})));
    disp(['largeur de couche augment�e � ' num2str(thick) 'm']);
    integration_depth = (thick:thick:max(integration_depth));
end

for isdr=1:nsdr
    Sv_for_ei = cat(1, Sv{:,isdr}); % 'cat' suppose le m�me range d'acquisition sur tous les pings
    Time_for_ei = cat(2, Time{:,isdr});
    r_sv=range_Sv{1,isdr};
    range_for_ei = r_sv(1,:);
    %dr_sv=range_for_ei(end)-range_for_ei(end-1);
    dr_sv=range_for_ei(end)-range_for_ei(end-1);
    thick_eff=thick_eff_Sv{isdr};
    np=size(Sv_for_ei,1);
    nf=size(Sv_for_ei,3);
    nlayer=length(integration_depth);

     % echo-integration par couche 
    sv_layer = zeros(np,nlayer, nf);
    Sv_for_ei_nat=10.^(Sv_for_ei/10);
    
    %appartenance � la layer
    r=repmat(range_for_ei,nlayer,1);
    d=repmat(integration_depth.',1,size(Sv_for_ei,2));  
    if thick>thick_eff-dr_sv %des cellules enti�res sont inclues dans la couche
        %appartenance � la layer
        idlayer= cast((r>d+thick_eff/2-dr_sv).*(r<d+thick-thick_eff/2+dr_sv),'single');
        %proportion d'appartenance � la layer
        idlayer= idlayer.*min(1,(max(0,(r+thick_eff/2)-d)/thick_eff)).*min(1,max(0,d + thick - (r-thick_eff/2))/thick_eff);
    end
    
    for i = 1:nlayer
        ind=find(idlayer(i,:)>0);
        sv_layer(:,i,:) = mean(Sv_for_ei_nat(:,ind,:).*repmat(idlayer(i,ind),np,1,nf),2)./mean(repmat(idlayer(i,ind),np,1,nf),2);
    end 
    sv_layer=10*log10(sv_layer);
    
    % echo-integration par ESU
    if strcmp(Type_echo_integration, 'ping')
        Sv_block=reshape(sv_layer(1:floor(np/nbr_ping_echo_integre)*nbr_ping_echo_integre,:,:),[nbr_ping_echo_integre,floor(np/nbr_ping_echo_integre),nlayer,nf]);
        Sv_ei{isdr}=squeeze(10*log10(mean(10.^(Sv_block/10),1))); % Sv par time x range x freq
        time_block=reshape(Time_for_ei(1:floor(np/nbr_ping_echo_integre)*nbr_ping_echo_integre),[nbr_ping_echo_integre,floor(np/nbr_ping_echo_integre)]);
        Time_Sv_ei{isdr} = mean(time_block,1);

    elseif strcmp(Type_echo_integration, 'time')
        clear Sv_ei_tmp Time_tmp
        x=0;
        for t = min(Time_for_ei):nbr_second_echo_integre/(24*3600):max(Time_for_ei) %matlab time (in days)
           x = x + 1;
           id_ei_time = find(Time_for_ei >= t & Time_for_ei  < t + nbr_second_echo_integre);
           Sv_ei_tmp(x,:,:) = 10*log10(mean(10.^(sv_layer(id_ei_time,:,:)/10))); % Sv par time x freq x range
           Time_tmp(x) = min(Time_for_ei(id_ei_time)); % mean?
        end 
        Sv_ei_time{isdr}=permute(Sv_ei_tmp,[1 3 2]); % Sv par time x range x freq
        Time_Sv_ei{isdr} =Time_tmp;
    end
end

% %% Echogramme sans pulse-compression, apr�s �cho-int�gration (moyenne sur chaque freq LB)
% clear Sv_ei_echogramme
% freqid=1;
% for f= 1:nsdr
%     freq_id = find(frequencies{1,f} >= frequencies_begin(f) & frequencies{1,f} <= frequencies_end(f)); %d�j� fait dans Preparation_EI
%     Sv_ei_sdr=Sv_ei{f};
%     Sv_ei_echogramme(:,:,f) = 10*log10(mean(10.^((Sv_ei_sdr(:,:,freq_id))/10),3));
%   figure(100+f);
%   DrawEchoGram_brut(Sv_ei_echogramme(:,:,f)',-40,-70);
%         title([num2str(frequencies_center(f)),' kHz'],'FontSize',11);
%         colorbar
%         freezeColors
%     saveas(figure(f),[path_save,'PELGAS16-EK80_',date_min{1},'_',time_min,'_to_',time_max,'_',num2str(frequencies_center(f)),'kHz_EI_',num2str(nbr_ping_echo_integre),'ping'],'png');
%     saveas(figure(f),[path_save,'PELGAS16-EK80_',date_min{1},'_',time_min,'_to_',time_max,'_',num2str(frequencies_center(f)),'kHz_EI_',num2str(nbr_ping_echo_integre),'ping'],'fig');
% end    

layerbegin=2;
layerend=2;
col='rmgcb';
sv_ref=-70;
Sv_mean=[];
freqtot=[];
freqid=1;
portee=[];
for f= 1:nsdr
    Sv_ei_sdr=Sv_ei{f};
    for layer=layerbegin:layerend
        Sv_mean(layer,freqid:freqid+size(Sv_ei_sdr,3)-1)= squeeze(10*log10(mean(10.^(Sv_ei_sdr(:,layer,:)/10),1)));
    end
    freqtot(freqid:freqid+size(Sv_ei_sdr,3)-1)=frequencies{1,f};
     freqid=freqid+size(Sv_ei_sdr,3);
end
figure;
hold on;
plot(freqtot,Sv_mean,'*');
grid on;
 saveas(gcf,[path_save,'PELGAS16-EK80_',date_min{1},'_',time_min,'_to_',time_max,'_',num2str(frequencies_center(f)),'kHz_spectre_',num2str(compute_from_MF),'_',num2str(nbr_ping_echo_integre),'ping'],'png');
    saveas(gcf,[path_save,'PELGAS16-EK80_',date_min{1},'_',time_min,'_to_',time_max,'_',num2str(frequencies_center(f)),'kHz_spectre_',num2str(compute_from_MF),'_',num2str(nbr_ping_echo_integre),'ping'],'fig');
        
%close all

%     save([path_save,'PELGAS16-EK80_',date_min{1},'_',time_min,'_to_',time_max,'_AllFreq','_EI_',num2str(nbr_ping_echo_integre),'_v2','ping'],...
%     'Sv_ei','Time_Sv_ei', 'Sv_ei_echogramme', 'frequencies', 'integration_depth');
%   

    save([path_save,'PELGAS16-EK80_',date_min{1},'_',time_min,'_to_',time_max,'_AllFreq','_EI_',num2str(nbr_ping_echo_integre),'_v2','ping'],...
    'Sv_ei','Time_Sv_ei',  'frequencies', 'integration_depth');

% portee=nan(length(freqtot));
% for i=1:length(freqtot)
%     ind=find(Sv_mean(:,i)>=sv_ref);
%     if length(ind)>0
%     portee(i)=integration_depth(min(ind));
%     end
% end
% 
% figure;
% hold on;
% for f= 1:nsdr
%      plot(freqtot,portee,'*');
% end

toc

