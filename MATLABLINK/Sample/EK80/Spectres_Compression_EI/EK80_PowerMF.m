%function [Power_MF,r]=EK80_PowerMF(RawData,ichannel)

%[RawData] = ReadEK80Data; %� commenter apr�s la premi�re lecture (les RawData des diff�rents channels sont lus)

nchannel=size(RawData.Data.PingData,1);

if (1)
    ichannel=4; % channel index
    
    % facteur de sous-�chantillonnage (puissance de 2)
    sous_ech=1;
end

if strcmp(RawData.Data.ConfigurationData.Transceivers(ichannel).TransceiverType,'GPT')
    warndlg('GPT channel: stop processing spectrum. Choose WBT channel');
    break;
end

%d�termination �mission s�quentielle ou simultan�e
if nchannel==1
    seq=0; 
elseif isempty(RawData.Data.PingData(1,1).SampleData) | isempty(RawData.Data.PingData(2,1).SampleData)
    seq=1;
else
    seq=0;
end
if seq==0
    ipdec=1;
    ip0=1;
else
    ipdec=nchannel;
    ip0=1;
    while isempty(RawData.Data.PingData(ichannel,ip0).SampleData)
        ip0=ip0+1;
    end
end

%param�tres EK80
ipref=1;
nSectors = size(RawData.Data.PingData(ichannel,ip0+(ipref-1)*ipdec).SampleData.complexSamples,2);
nominalTransducerImpedance  = 75;
wbtImpedanceRx              = 5e3;
fe=1/RawData.Data.PingData(ichannel,ip0+(ipref-1)*ipdec).ParameterData.sampleInterval;


f1=RawData.Data.PingData(ichannel,ip0+(ipref-1)*ipdec).ParameterData.frequencyStart;
f2=RawData.Data.PingData(ichannel,ip0+(ipref-1)*ipdec).ParameterData.frequencyStop;
T=RawData.Data.PingData(ichannel,ip0+(ipref-1)*ipdec).ParameterData.pulseLength;
B=f2-f1;
soundSpeed=RawData.Data.PingData(ichannel,ip0+(ipref-1)*ipdec).EnvironmentData.soundSpeed;

% Transmit signal
transmitSignal = CreateTransmitSignal(RawData.Data.PingData(ichannel,ip0+(ipref-1)*ipdec));
filt=transmitSignal/norm(transmitSignal)^2;

% taille de fft (pour la compression d'impulsion)
Nfft=2*max(T,length(filt)/fe)*fe;
Nfft=2^(ceil(log2(Nfft)));
Tfft=Nfft/fe;

np=size(RawData.Data.PingData,2)/ipdec;
ns=size(RawData.Data.PingData(ichannel,ip0+(ipref-1)*ipdec).SampleData.complexSamples,1);
shift=Nfft/2;
nb_fft=floor(ns/Nfft*2-1);

fft_filt=fft(filt,Nfft);

% Correlation with FFT
clear Power_MF
tic
for ip=1:np
    for ic=1:nb_fft
        data4=RawData.Data.PingData(ichannel,ip0+(ip-1)*ipdec).SampleData.complexSamples(1+(ic-1)*Nfft/2:(ic+1)*Nfft/2,:);
        amp_raw=sum(data4,2)*(wbtImpedanceRx+nominalTransducerImpedance)/wbtImpedanceRx/(2*sqrt(2*nominalTransducerImpedance*nSectors));
        fft_amp_raw=fft(amp_raw);
        fft_MF=fft_amp_raw.*conj(fft_filt);
        if sous_ech>1
            fft_MF=reshape(fft_MF,Nfft/sous_ech,sous_ech);
            fft_MF=mean(fft_MF,2);
        end
        corrFilt=ifft(fft_MF);
        Power_MF(ip,1+(ic-1)*Nfft/2/sous_ech:ic*Nfft/2/sous_ech)=20*log10(abs(corrFilt(1:Nfft/2/sous_ech)));
    end
end
toc

if (0)
    tic 
    % Comparison with direct correlation
    clear Power_MF_cor
    for ip=1:np
        data4=RawData.Data.PingData(ichannel,ip0+(ip-1)*ipdec).SampleData.complexSamples;
        amp_raw=sum(data4,2)*(wbtImpedanceRx+nominalTransducerImpedance)/wbtImpedanceRx/(2*sqrt(2*nominalTransducerImpedance*nSectors));
        corrFilt2=conv2(flipud(conj(filt)),1,amp_raw);
        Power_MF_cor(ip,1:size(corrFilt2,1)-length(filt)+1)=20*log10(abs(corrFilt2(length(filt):end))).';
    end
    toc

    %comparison with Simrad
    clear Power_sm
    for ip=1:np
        Power_sm(ip,1:size(BasicProcessedData(ichannel).ProcessedSampleData(ip).power,1))=10*log10(BasicProcessedData(ichannel).ProcessedSampleData(ip).power);
    end
    
end

r=(0:size(Power_MF,2)-1)*soundSpeed/2/fe*sous_ech;

 figure;imagesc((1:np),r,Power_MF.');
 xlabel('pings');ylabel('depth (m)');title(['Power MF for ' RawData.Data.ConfigurationData.Transceivers(ichannel).Channels.ChannelIdShort])
 colorbar;caxis([-100 -20])



