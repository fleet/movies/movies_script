function [Sig_MF,filt]=EK80_SigMF(RawData,ichannel)

% fournit le signal d'amplitude complexe compressé pour chaque ping (pas d'application de
% calibration)

nchannel=size(RawData.PingData,1);


%détermination ping d'émission
ip_list=[];
for ip=1:size(RawData.PingData,2)
    if ~isempty(RawData.PingData(ichannel,ip).time)
        ip_list(end+1)=ip;
    end
end
% détermination wbt et tsd (WBT tube)
if size(RawData.PingData,1)==size(RawData.ConfigurationData.Transceivers,2) %1freq/wbt
    iwbt=ichannel;
    itsd=1;
else
    wbt_list=[];
    tsd_list=[];
    for iwbt=1:size(RawData.ConfigurationData.Transceivers,2)
        for itsd=1:size(RawData.ConfigurationData.Transceivers(iwbt).Channels,2)
            wbt_list(end+1)=iwbt;
            tsd_list(end+1)=itsd;
        end
    end
    iwbt=wbt_list(ichannel);
    itsd=tsd_list(ichannel);
end
    

if strcmp(RawData.ConfigurationData.Transceivers(iwbt).TransceiverType,'GPT')
    warndlg('GPT channel: stop processing spectrum. Choose WBT channel');
    return;
end

if (~RawData.PingData(ichannel,ip_list(1)).ParameterData.pulseForm) %CW
    uiwait(msgbox('CW mode. Stop processing'));
    return;
end


%paramètres EK80
nSectors = size(RawData.PingData(ichannel,ip_list(1)).SampleData.complexSamples,2);
nominalTransducerImpedance  = 75;
wbtImpedanceRx              = 5e3;
fe=1/RawData.PingData(ichannel,ip_list(1)).ParameterData.sampleInterval;


f1=RawData.PingData(ichannel,ip_list(1)).ParameterData.frequencyStart;
f2=RawData.PingData(ichannel,ip_list(1)).ParameterData.frequencyStop;
T=RawData.PingData(ichannel,ip_list(1)).ParameterData.pulseLength;
B=f2-f1;

% Transmit signal
transmitSignal = CreateTransmitSignal(RawData.PingData(ichannel,ip_list(1)));
filt=transmitSignal/norm(transmitSignal)^2;

% taille de fft (pour la compression d'impulsion)
Nfft=2*ceil(max(T,length(filt)/fe)*fe);
%Nfft=2^(ceil(log2(Nfft)));
Tfft=Nfft/fe;

np=length(ip_list);
ns=size(RawData.PingData(ichannel,ip_list(1)).SampleData.complexSamples,1);
shift=Nfft/2;
nb_fft=floor(ns/Nfft*2-1);

fft_filt=fft(filt,Nfft);

% Correlation with FFT
clear Sig_MF
for ip=1:np
    for ic=1:nb_fft
        data4=RawData.PingData(ichannel,ip_list(ip)).SampleData.complexSamples(1+(ic-1)*Nfft/2:(ic+1)*Nfft/2,:);
        amp_raw=sum(data4,2)*(wbtImpedanceRx+nominalTransducerImpedance)/wbtImpedanceRx/(2*sqrt(2*nominalTransducerImpedance*nSectors));
        %amp_raw=sum(data4,2);
        fft_amp_raw=fft(amp_raw);
        fft_MF=fft_amp_raw.*conj(fft_filt);
        corrFilt=ifft(fft_MF);
        Sig_MF(ip,1+(ic-1)*Nfft/2:ic*Nfft/2)=corrFilt(1:Nfft/2);
    end
end
%Power_MF=20*log10(abs(Sig_MF));

