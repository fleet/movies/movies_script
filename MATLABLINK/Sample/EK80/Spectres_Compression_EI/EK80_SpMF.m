%function [Sp_MF,r]=EK80_SpMF(RawData,ichannel)

%[RawData] = ReadEK80Data; %� commenter apr�s la premi�re lecture (les RawData des diff�rents channels sont lus)

nchannel=size(RawData.Data.PingData,1);

if (1)
    ichannel=4; % channel index
end

if strcmp(RawData.Data.ConfigurationData.Transceivers(ichannel).TransceiverType,'GPT')
    warndlg('GPT channel: stop processing spectrum. Choose WBT channel');
    break;
end

%d�termination �mission s�quentielle ou simultan�e
if nchannel==1
    seq=0; 
elseif isempty(RawData.Data.PingData(1,1).SampleData) | isempty(RawData.Data.PingData(2,1).SampleData)
    seq=1;
else
    seq=0;
end
if seq==0
    ipdec=1;
    ip0=1;
else
    ipdec=nchannel;
    ip0=1;
    while isempty(RawData.Data.PingData(ichannel,ip0).SampleData)
        ip0=ip0+1;
    end
end

%param�tres EK80
ipref=1;
nSectors = size(RawData.Data.PingData(ichannel,ip0+(ipref-1)*ipdec).SampleData.complexSamples,2);
nominalTransducerImpedance  = 75;
wbtImpedanceRx              = 5e3;
fe=1/RawData.Data.PingData(ichannel,ip0+(ipref-1)*ipdec).ParameterData.sampleInterval;


f1=RawData.Data.PingData(ichannel,ip0+(ipref-1)*ipdec).ParameterData.frequencyStart;
f2=RawData.Data.PingData(ichannel,ip0+(ipref-1)*ipdec).ParameterData.frequencyStop;
T=RawData.Data.PingData(ichannel,ip0+(ipref-1)*ipdec).ParameterData.pulseLength;
B=f2-f1;

% Transmit signal
transmitSignal = CreateTransmitSignal(RawData.Data.PingData(ichannel,ip0+(ipref-1)*ipdec));
filt=transmitSignal/norm(transmitSignal)^2;

% taille de fft (pour la compression d'impulsion)
Nfft=2*max(T,length(filt)/fe)*fe;
%Nfft=2^(ceil(log2(Nfft)));
Tfft=Nfft/fe;

%index des fr�quences
f2per=rem(f2,fe);
f1per=rem(f1,fe);
if f1per<f2per
    nper=floor(f1/fe);
    freq=(0:Nfft-1)/Nfft*fe+nper*fe;
else
    freqper=(0:Nfft-1)/Nfft*fe;
    ind_out=find(freqper>f2per & freqper<f1per);
    if ~isempty(freqper)
        iout0=ind_out(ceil(length(ind_out)/2));
    else
        iout0=max(freqper<f1per)+1;
    end
    nper=floor(f1/fe);
    freq=[freqper(1:iout0-1)+(nper+1)*fe freqper(iout0:end)+nper*fe];
end

np=floor(size(RawData.Data.PingData,2)/ipdec);
ns=size(RawData.Data.PingData(ichannel,ip0+(ipref-1)*ipdec).SampleData.complexSamples,1);
shift=Nfft/2;
nb_fft=floor(ns/Nfft*2-1);

fft_filt=fft(filt,Nfft);

% Correlation with FFT
clear Power_MF
clear Sig_MF
clear spectrum
for ip=1:np
    for ic=1:nb_fft
        data4=RawData.Data.PingData(ichannel,ip0+(ip-1)*ipdec).SampleData.complexSamples(1+(ic-1)*Nfft/2:(ic+1)*Nfft/2,:);
        amp_raw=sum(data4,2)*(wbtImpedanceRx+nominalTransducerImpedance)/wbtImpedanceRx/(2*sqrt(2*nominalTransducerImpedance*nSectors));
        fft_amp_raw=fft(amp_raw);
        spectrum(ip,ic,:)=fft_amp_raw;
        fft_MF=fft_amp_raw.*conj(fft_filt);
        corrFilt=ifft(fft_MF);
        Sig_MF(ip,1+(ic-1)*Nfft/2:ic*Nfft/2)=corrFilt(1:Nfft/2);
    end
end
Power_MF=20*log10(abs(Sig_MF));

[freq,indordf]=sort(freq);
spectrum=spectrum(:,:,indordf);

if (0)
    tic 
    % Comparison with direct correlation
    clear Power_MF_cor
    for ip=1:np
        data4=RawData.Data.PingData(ichannel,ip0+(ip-1)*ipdec).SampleData.complexSamples;
        amp_raw=sum(data4,2)*(wbtImpedanceRx+nominalTransducerImpedance)/wbtImpedanceRx/(2*sqrt(2*nominalTransducerImpedance*nSectors));
        corrFilt2=conv2(flipud(conj(filt)),1,amp_raw);
        Power_MF_cor(ip,1:size(corrFilt2,1)-length(filt)+1)=20*log10(abs(corrFilt2(length(filt):end))).';
    end
    toc

    %comparison with Simrad
    clear Power_sm
    for ip=1:np
        Power_sm(ip,1:size(BasicProcessedData(ichannel).ProcessedSampleData(ip).power,1))=10*log10(BasicProcessedData(ichannel).ProcessedSampleData(ip).power);
    end
    
    %comparison with Simrad
    clear sp_sm
    for ip=1:np
        sp_sm(ip,1:size(BasicProcessedData.ProcessedSampleData(ip).power,1))=BasicProcessedData.ProcessedSampleData(ip).sp;
    end
end

% EK80 parameters
transmitPower=RawData.Data.PingData(ichannel,ip0+(ipref-1)*ipdec).ParameterData.transmitPower; %W
soundSpeed=RawData.Data.PingData(ichannel,ip0+(ipref-1)*ipdec).EnvironmentData.soundSpeed;
frequencyCenter=RawData.Data.PingData(ichannel,ip0+(ipref-1)*ipdec).ParameterData.frequencyCenter;
clear absorptionCoefficients
for i=1:length(freq)
    absorptionCoefficients(i) = EstimateAbsorptionCoefficients(RawData.Data.PingData(ichannel,ip0+(ipref-1)*ipdec).EnvironmentData,freq(i));
end
absorptionCoefficientsCenter = EstimateAbsorptionCoefficients(RawData.Data.PingData(ichannel,ip0+(ipref-1)*ipdec).EnvironmentData,frequencyCenter);
lambda=soundSpeed./freq;
lambdaCenter=soundSpeed./frequencyCenter;
gainTable       = str2num(RawData.Data.ConfigurationData.Transceivers(ichannel).Channels.Transducer.Gain);
gainCenter = gainTable(end)+20*log10(frequencyCenter/str2num(RawData.Data.ConfigurationData.Transceivers(ichannel).Channels.Transducer.Frequency));
gain = gainTable(end)+20*log10(freq/str2num(RawData.Data.ConfigurationData.Transceivers(ichannel).Channels.Transducer.Frequency));
dsp_Pt=nan(1,length(freq));
ifreqB=find(freq>=f1 & freq <=f2);
dsp_Pt(ifreqB)=transmitPower/B;

r=(0:size(Power_MF,2)-1)*soundSpeed/2/fe;
tvg40=max(0,40*log10(r)+2*absorptionCoefficientsCenter*r);

% Sp after matched filtering
Sp_MF = Power_MF  + repmat(tvg40 - 10*log10(transmitPower*lambdaCenter^2/(16*pi^2)) - 2*gainCenter,np,1) ;

figure;imagesc((1:np),r,Sp_MF.');
xlabel('pings');ylabel('depth (m)');title(['Sp MF for ' RawData.Data.ConfigurationData.Transceivers(ichannel).Channels.ChannelIdShort])
colorbar;caxis([-100 -20])

% % Sp spectrum
% nr=size(spectrum,2);
% np=size(spectrum,1);
% nf=size(spectrum,3);
% r_spectrum=(Nfft/2+(0:nr-1)*shift)/fe*soundSpeed/2;
% 
% sp_spectrum=20*log10(squeeze(abs(spectrum)))-10*log10(fe*Nfft)+ repmat(40*log10(r_spectrum),np,1,nf) + permute(repmat(2*absorptionCoefficients.'*r_spectrum,1,1,np),[3,2,1])+permute(repmat(-10*log10(dsp_Pt.*lambda.^2/(16*pi^2))-2*gain,np,1,nr),[1 3 2]);
% 
% figure;imagesc(freq/1000,r_spectrum,squeeze(sp_spectrum(ipref,:,:)))
% xlabel('freq kHz');ylabel('depth (m)');title(['ping ' num2str(ipref) , ' Nfft=' num2str(Nfft) ', Sp spectra for ' RawData.Data.ConfigurationData.Transceivers(ichannel).Channels.ChannelIdShort])
% colorbar;caxis([-100 -20])


