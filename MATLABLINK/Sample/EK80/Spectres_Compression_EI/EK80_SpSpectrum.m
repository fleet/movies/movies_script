function [sp_spectrum,freq,r_spectrum,resol_f,resol_r]=EK80_SpSpectrum(RawData,ichannel,df,varargin)
% df: �chantillonnage spectral demand� (Hz)
% sp_spectrum: matrice ping x range x freq de Sp(f)
% freq: vecteur des fr�quences associ�es (Hz)
% r_spectrum: vecteur des ranges associ�s (m)
% resol_f: r�solution spectrale (Hz) (attention, diff�rente de l'�chantillonnage)
% resol_r: r�solution en range (m)



%[RawData] = ReadEK80Data; %� commenter apr�s la premi�re lecture (les RawData des diff�rents channels sont lus)
nchannel=size(RawData.Data.PingData,1);
ipref=1; % index du ping pour l'affichage de l'image des spectres

if (0)
    % param�tres d'analyse
    ichannel=4; % channel index
    df=500; % (Hz) frequency sampling
end


if strcmp(RawData.Data.ConfigurationData.Transceivers(ichannel).TransceiverType,'GPT')
    warndlg('GPT channel: stop processing spectrum. Choose WBT channel');
    return;
end

%d�termination �mission s�quentielle ou simultan�e
if nchannel==1
    seq=0; 
elseif isempty(RawData.Data.PingData(1,1).SampleData) | isempty(RawData.Data.PingData(2,1).SampleData)
    seq=1;
else
    seq=0;
end
if seq==0
    ipdec=1;
    ip0=1;
else
    ipdec=nchannel;
    ip0=1;
    while isempty(RawData.Data.PingData(ichannel,ip0).SampleData)
        ip0=ip0+1;
    end
end

%param�tres EK80
ipref=1;
nSectors = size(RawData.Data.PingData(ichannel,ip0+(ipref-1)*ipdec).SampleData.complexSamples,2);
nominalTransducerImpedance  = 75;
wbtImpedanceRx              = 5e3;
fe=1/RawData.Data.PingData(ichannel,ip0+(ipref-1)*ipdec).ParameterData.sampleInterval;
soundSpeed=RawData.Data.PingData(ichannel,ip0+(ipref-1)*ipdec).EnvironmentData.soundSpeed;


f1=RawData.Data.PingData(ichannel,ip0+(ipref-1)*ipdec).ParameterData.frequencyStart;
f2=RawData.Data.PingData(ichannel,ip0+(ipref-1)*ipdec).ParameterData.frequencyStop;
T=RawData.Data.PingData(ichannel,ip0+(ipref-1)*ipdec).ParameterData.pulseLength;
B=f2-f1;


if df>sqrt(B/T)/4
    disp('        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
    df=round(sqrt(B/T)/4);
    disp(['        Too large frequency resolution requested. Reduced to:' num2str(df) 'Hz']);
    disp('        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
end
if df<1/T
    disp('        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
    df=round(1/T);
    disp(['        Too small frequency resolution requested. Increased to:' num2str(df) 'Hz']);
    disp('        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
end

% taille de fft
%Ndf=round(min(fe/df,T*fe));
Ndf=round(min(fe/df*1.25,T*fe)); % avec hanning sur 2*0.2
Nfft=2^(ceil(log2(Ndf)));
Tfft=Nfft/fe;
%resol_f=fe/Ndf;
resol_f=fe/Ndf*1.25;% avec hanning sur 2*0.2
df_eff=fe/Nfft;
shift=max(1,floor(Ndf/4));
%shift=20;
%resol_r=Ndf/fe*soundSpeed/2;
resol_r=1/resol_f*soundSpeed/2;

%nShapingSamples = floor(0.7/2*Ndf);
nShapingSamples = floor(0.4/2*Ndf);
windowFunction  = ((1-cos(pi*(0:2*nShapingSamples-1).'/nShapingSamples))/2).^2;
shapingWindow   = [windowFunction(1:nShapingSamples); ones(Ndf-2*nShapingSamples,1); windowFunction(nShapingSamples+1:end)];

% calcul glissant des spectres
np=floor(size(RawData.Data.PingData,2)/ipdec);
ns=size(RawData.Data.PingData(ichannel,ip0+(ipref-1)*ipdec).SampleData.complexSamples,1);
%ns=round(12/750*fe);
clear spectrum
%i1=round((r1-thick_eff/2)/soundSpeed*2*fe);
%i2=round((r2+(T+Tfft/2)*soundSpeed/2)/soundSpeed*2*fe);
i1=1;
i2=ns;
nb_fft=floor((i2-i1-Ndf+1)/shift)+1;
for ip=1:np
    if size(RawData.Data.PingData(ichannel,ip0+(ip-1)*ipdec).SampleData.complexSamples,1)~=ns
        disp('Pb: change in data range. SvSpectrum routine not adequate');
        return;
    end
    for ic=1:nb_fft
        data4=RawData.Data.PingData(ichannel,ip0+(ip-1)*ipdec).SampleData.complexSamples(i1+(ic-1)*shift:i1+Ndf-1+(ic-1)*shift,:);
        amp_raw=sum(data4,2)*(wbtImpedanceRx+nominalTransducerImpedance)/wbtImpedanceRx/(2*sqrt(2*nominalTransducerImpedance*nSectors));
        %fft_amp_raw=fft(amp_raw,Nfft);
        fft_amp_raw=fft(amp_raw.*shapingWindow,Nfft);
        spectrum(ip,ic,:)=fft_amp_raw;
    end
end

%index des fr�quences
f2per=rem(f2,fe);
f1per=rem(f1,fe);
if f1per<f2per
    nper=floor(f1/fe);
    freq=(0:Nfft-1)/Nfft*fe+nper*fe;
else
    freqper=(0:Nfft-1)/Nfft*fe;
    ind_out=find(freqper>f2per & freqper<f1per);
    if ~isempty(freqper)
        iout0=ind_out(ceil(length(ind_out)/2));
    else
        iout0=max(freqper<f1per)+1;
    end
    nper=floor(f1/fe);
    freq=[freqper(1:iout0-1)+(nper+1)*fe freqper(iout0:end)+nper*fe];
end

%r�ordonnancement des fr�quences
[freq,indordf]=sort(freq);
spectrum=spectrum(:,:,indordf);

% EK80 parameters
transmitPower=RawData.Data.PingData(ichannel,ip0+(ipref-1)*ipdec).ParameterData.transmitPower; %W
frequencyCenter=RawData.Data.PingData(ichannel,ip0+(ipref-1)*ipdec).ParameterData.frequencyCenter;
clear absorptionCoefficients
for i=1:length(freq)
    absorptionCoefficients(i) = EstimateAbsorptionCoefficients(RawData.Data.PingData(ichannel,ip0+(ipref-1)*ipdec).EnvironmentData,freq(i));
end
absorptionCoefficientsCenter = EstimateAbsorptionCoefficients(RawData.Data.PingData(ichannel,ip0+(ipref-1)*ipdec).EnvironmentData,frequencyCenter);
lambda=soundSpeed./freq;
lambdaCenter=soundSpeed./frequencyCenter;
if nargin==3
    %%%%%%%%%%%%%%%%%%% l'estimation effectu�e ci-dessous des gain(f) � appliquer devra �tre remplac�e par les gain(f) lus dans le fichier d'�talonnage (interpol�s aux fr�quences freq)
    gainTable       = str2num(RawData.Data.ConfigurationData.Transceivers(ichannel).Channels.Transducer.Gain);
    gain = gainTable(end)+20*log10(freq/str2num(RawData.Data.ConfigurationData.Transceivers(ichannel).Channels.Transducer.Frequency));
else
    freq_cal=varargin{1};
    gain_cal=varargin{2};
    gain = interp1(freq_cal,gain_cal,freq);  %interpolation en dB?
end
dsp_Pt=nan(1,length(freq));
ifreqB=find(freq>=f1 & freq <=f2);
if (0) % puissance moyenne sur la bande
    dsp_Pt(ifreqB)=transmitPower/B;
else % puissance plus fine calcul�e sur le spectre
    [spectrumTx,freq_Tx,fe_Tx,NfftTx,normFilt]=TransmitSpectrum(RawData.Data.PingData(ichannel,ip0));
    dsp_Pt=transmitPower*abs(spectrumTx).'.^2/(fe_Tx*NfftTx) * NfftTx/fe_Tx/T;
    nfilt=round(resol_f/(freq_Tx(2)-freq_Tx(1)));
    dsp_Pt=(filter(ones(1,nfilt)/nfilt,1,sqrt(dsp_Pt))).^2;
    freq_Tx=freq_Tx-(freq_Tx(2)-freq_Tx(1))*(nfilt-1)/2;
    dsp_Pt= interp1(freq_Tx,dsp_Pt,freq); 
end

%spectre redress�
np=size(spectrum,1);
nf=size(spectrum,3);
nr=size(spectrum,2)-(round((freq(ifreqB(end))-f1)*T/B*fe/shift));
%r_spectrum=(i1-1+Ndf/2+(0:nr-1)*shift)/fe*soundSpeed/2;
r_spectrum=(i1-1+(0:nr-1)*shift)/fe*soundSpeed/2;
spectrumCor=nan(size(spectrum,1),nr,size(spectrum,3));
for ifr=ifreqB(1):ifreqB(end)
    %shift_fr=max(0,round((freq(ifr)-f1)*T/B*fe/shift));
    shift_fr=max(0,floor((freq(ifr)-f1)*T/B*fe/shift));
    spectrumCor(:,1:nr,ifr)=spectrum(:,1+shift_fr:nr+shift_fr,ifr);
end
r_spectrum=r_spectrum+Ndf/2/fe*soundSpeed/2;
dec_filtSim=(RawData.Data.FilterData(ichannel,1).nCoefficients+RawData.Data.FilterData(ichannel,2).nCoefficients-...
    (RawData.Data.FilterData(ichannel,1).decimationFactor+RawData.Data.FilterData(ichannel,2).decimationFactor)-T*fe)/2; % shift of pulse beginning due to WBT filtering
r_spectrum=max(r_spectrum-dec_filtSim/fe*soundSpeed/2,0.1);
%sp_spectrum=20*log10(squeeze(abs(spectrumCor)))-10*log10(fe*Ndf) + 10*log10(Ndf/fe/T) + repmat(40*log10(r_spectrum),np,1,nf) + permute(repmat(2*absorptionCoefficients.'*r_spectrum,1,1,np),[3,2,1])+permute(repmat(-10*log10(dsp_Pt.*lambda.^2/(16*pi^2))-2*gain,np,1,nr),[1 3 2]);
sp_spectrum=20*log10(squeeze(abs(spectrumCor)))-10*log10(fe*Ndf) + 10*log10(Ndf/fe/T) + repmat(40*log10(r_spectrum),[np 1 nf]) + permute(repmat(2*absorptionCoefficients.'*r_spectrum,[1 1 np]),[3,2,1])+permute(repmat(-10*log10(dsp_Pt.*lambda.^2/(16*pi^2))-2*gain,[np 1 nr]),[1 3 2]);

%restriction aux ranges>� 0.1m
i0_valid=min(find(r_spectrum>0.1));
r_spectrum=r_spectrum(i0_valid:end);
sp_spectrum=sp_spectrum(:,i0_valid:end,:);

figure;imagesc(freq/1000,r_spectrum,squeeze(sp_spectrum(ipref,:,:)))
xlabel('freq kHz');ylabel('depth (m)');title(['ping ' num2str(ipref) , ', Sp spectra for ' RawData.Data.ConfigurationData.Transceivers(ichannel).Channels.ChannelIdShort,', resol fr=' num2str(round(resol_f)) 'Hz', ', Ndf=' num2str(Ndf)])
colorbar;caxis([-100 -20])

%figure;imagesc(squeeze(10*log10(abs(spectrum(1,:,:)))));