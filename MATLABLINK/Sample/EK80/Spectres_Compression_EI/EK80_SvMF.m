function [sv_MF,r]=EK80_SvMF(RawData,ichannel)

%[RawData] = ReadEK80Data; %� commenter apr�s la premi�re lecture (les RawData des diff�rents channels sont lus)

nchannel=size(RawData.Data.PingData,1);

if (0)
    ichannel=3; % channel index
end

if strcmp(RawData.Data.ConfigurationData.Transceivers(ichannel).TransceiverType,'GPT')
    warndlg('GPT channel: stop processing spectrum. Choose WBT channel');
    return;
end

%d�termination �mission s�quentielle ou simultan�e
if nchannel==1
    seq=0; 
elseif isempty(RawData.Data.PingData(1,1).SampleData) | isempty(RawData.Data.PingData(2,1).SampleData)
    seq=1;
else
    seq=0;
end
if seq==0
    ipdec=1;
    ip0=1;
else
    ipdec=nchannel;
    ip0=1;
    while isempty(RawData.Data.PingData(ichannel,ip0).SampleData)
        ip0=ip0+1;
    end
end

%param�tres EK80
ipref=1;
nSectors = size(RawData.Data.PingData(ichannel,ip0+(ipref-1)*ipdec).SampleData.complexSamples,2);
nominalTransducerImpedance  = 75;
wbtImpedanceRx              = 5e3;
fe=1/RawData.Data.PingData(ichannel,ip0+(ipref-1)*ipdec).ParameterData.sampleInterval;


f1=RawData.Data.PingData(ichannel,ip0+(ipref-1)*ipdec).ParameterData.frequencyStart;
f2=RawData.Data.PingData(ichannel,ip0+(ipref-1)*ipdec).ParameterData.frequencyStop;
T=RawData.Data.PingData(ichannel,ip0+(ipref-1)*ipdec).ParameterData.pulseLength;
B=f2-f1;


% Transmit signal
transmitSignal = CreateTransmitSignal(RawData.Data.PingData(ichannel,ip0+(ipref-1)*ipdec));
filt=transmitSignal/norm(transmitSignal)^2;

% taille de fft (pour la compression d'impulsion)
Nfft=2*ceil(max(T,length(filt)/fe)*fe);
%Nfft=2^(ceil(log2(Nfft)));
Tfft=Nfft/fe;

np=floor(size(RawData.Data.PingData,2)/ipdec);
ns=size(RawData.Data.PingData(ichannel,ip0+(ipref-1)*ipdec).SampleData.complexSamples,1);
nb_fft=floor(ns/Nfft*2-1);

fft_filt=fft(filt,Nfft);

% Correlation with FFT
clear Power_MF
clear Sig_MF
clear spectrum
for ip=1:np
    for ic=1:nb_fft
        data4=RawData.Data.PingData(ichannel,ip0+(ip-1)*ipdec).SampleData.complexSamples(1+(ic-1)*Nfft/2:(ic+1)*Nfft/2,:);
        amp_raw=sum(data4,2)*(wbtImpedanceRx+nominalTransducerImpedance)/wbtImpedanceRx/(2*sqrt(2*nominalTransducerImpedance*nSectors));
        fft_amp_raw=fft(amp_raw);
        fft_MF=fft_amp_raw.*conj(fft_filt);
        corrFilt=ifft(fft_MF);
        Sig_MF(ip,1+(ic-1)*Nfft/2:ic*Nfft/2)=corrFilt(1:Nfft/2);
    end
end
Power_MF=20*log10(abs(Sig_MF));

% EK80 parameters
transmitPower=RawData.Data.PingData(ichannel,ip0+(ipref-1)*ipdec).ParameterData.transmitPower; %W
soundSpeed=RawData.Data.PingData(ichannel,ip0+(ipref-1)*ipdec).EnvironmentData.soundSpeed;
f1=RawData.Data.PingData(ichannel,ip0+(ipref-1)*ipdec).ParameterData.frequencyStart;
f2=RawData.Data.PingData(ichannel,ip0+(ipref-1)*ipdec).ParameterData.frequencyStop;
frequencyCenter=(f1+f2)/2;
absorptionCoefficientsCenter = EstimateAbsorptionCoefficients(RawData.Data.PingData(ichannel,ip0+(ipref-1)*ipdec).EnvironmentData,frequencyCenter);
lambdaCenter=soundSpeed./frequencyCenter;
gainTable       = str2num(RawData.Data.ConfigurationData.Transceivers(ichannel).Channels.Transducer.Gain);
gainCenter = gainTable(end)+20*log10(frequencyCenter/str2num(RawData.Data.ConfigurationData.Transceivers(ichannel).Channels.Transducer.Frequency));
%effectivePulselLength =1/B;
autoCorrelationTransmitSignal = conv(transmitSignal,flipud(conj(transmitSignal)))/norm(transmitSignal)^2;
autoCorrelationTransmitSignalPower = (abs(autoCorrelationTransmitSignal).^2);
effectivePulselLength  = 1/fe * sum(autoCorrelationTransmitSignalPower) / max(autoCorrelationTransmitSignalPower);
psiCenter=str2num(RawData.Data.ConfigurationData.Transceivers(ichannel).Channels.Transducer.EquivalentBeamAngle)-20*log10(frequencyCenter/str2num(RawData.Data.ConfigurationData.Transceivers(ichannel).Channels.Transducer.Frequency));

r=(0:size(Power_MF,2)-1)*soundSpeed/2/fe;
tvg20=max(0,20*log10(r)+2*absorptionCoefficientsCenter*r);

% Sv after matched filtering
sv_MF = Power_MF + repmat(tvg20 - 10*log10(transmitPower*lambdaCenter^2*soundSpeed/(32*pi^2)) - 2*gainCenter -10*log10(effectivePulselLength)-psiCenter,np,1) ;

figure;imagesc((1:np),r,sv_MF.');
xlabel('pings');ylabel('depth (m)');title(['Sv MF for ' RawData.Data.ConfigurationData.Transceivers(ichannel).Channels.ChannelIdShort])
colorbar;caxis([-100 -20])


