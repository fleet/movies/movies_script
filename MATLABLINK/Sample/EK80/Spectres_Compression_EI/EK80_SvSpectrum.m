function [Sv_spectrum,freq,r_spectrum,time_Sv,thick_eff,df_eff,resol_f]=EK80_SvSpectrum(RawData,ichannel,df,varargin)
% df: �chantillonnage spectral demand� (Hz)
% thick_eff: �paisseur effective des couches de Sv en m�tres
% Sv_spectrum: matrice ping x range x freq de Sv(f)
% freq: vecteur des fr�quences associ�es (Hz)
% r_spectrum: vecteur des ranges associ�s (m)
% df_eff: �chantillonnage spectral effectif (Hz)
% resol_f: r�solution spectrale (Hz)
% varargin:freq_cal,gain_cal: optionnel, pour renseigner des gains d'�talonnage (dB) en fonction de la fr�quence (Hz)



%[RawData] = ReadEK80Data; %� commenter apr�s la premi�re lecture (les RawData des diff�rents channels sont lus)

nchannel=size(RawData.Data.PingData,1);
ipref=1; % index du ping pour l'affichage de l'image des spectres de Sv

if (0)
    % param�tres d'analyse
    ichannel=4; % channel index
    df=2000; % (Hz) frequency sampling
end


if strcmp(RawData.Data.ConfigurationData.Transceivers(ichannel).TransceiverType,'GPT')
    warndlg('GPT channel: stop processing spectrum. Choose WBT channel');
    return;
end

%d�termination �mission s�quentielle ou simultan�e
if nchannel==1
    seq=0; 
elseif isempty(RawData.Data.PingData(1,1).SampleData) | isempty(RawData.Data.PingData(2,1).SampleData)
    seq=1;
else
    seq=0;
end
if seq==0
    ipdec=1;
    ip0=1;
else
    ipdec=nchannel;
    ip0=1;
    while isempty(RawData.Data.PingData(ichannel,ip0).SampleData)
        ip0=ip0+1;
    end
end

%param�tres EK80
nSectors = size(RawData.Data.PingData(ichannel,ip0+(ipref-1)*ipdec).SampleData.complexSamples,2);
nominalTransducerImpedance  = 75;
wbtImpedanceRx              = 5e3;
fe=1/RawData.Data.PingData(ichannel,ip0+(ipref-1)*ipdec).ParameterData.sampleInterval;
soundSpeed=RawData.Data.PingData(ichannel,ip0+(ipref-1)*ipdec).EnvironmentData.soundSpeed;

f1=RawData.Data.PingData(ichannel,ip0+(ipref-1)*ipdec).ParameterData.frequencyStart;
f2=RawData.Data.PingData(ichannel,ip0+(ipref-1)*ipdec).ParameterData.frequencyStop;
T=RawData.Data.PingData(ichannel,ip0+(ipref-1)*ipdec).ParameterData.pulseLength;
B=f2-f1;

% frequency resolution limitation
if df>sqrt(B/T)/4
    disp('        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
    df=round(sqrt(B/T)/4);
    disp(['        Too large frequency resolution requested. Reduced to:' num2str(df) 'Hz']);
    disp('        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
end
if df<1.25/T
    disp('        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
    df=round(1.25/T);
    disp(['        Too small frequency resolution requested. Increased to:' num2str(df) 'Hz']);
    disp('        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
end

% taille de fft 
% (Ndf est le nombre de point effectifs de signal, Nfft le
% nombre de points total avec zero-padding)
Ndf=round(fe/df*1.25); % avec hanning sur 2*0.2
Nfft=2^(ceil(log2(Ndf)));
Tfft=Nfft/fe;
resol_f=fe/Ndf*1.25;% avec hanning sur 2*0.2
df_eff=fe/Nfft;

thick_eff=Ndf/fe*soundSpeed/2; %/1.25 with Hanning?

% d�calage des analyses spectrales
shift=max(1,floor(Ndf/4));

% % display des param�tres
% disp(' ');
% disp(['nombre de points de fft:' num2str(Nfft)]);
% disp(['�chantillonnage en fr�quence:' num2str(round(df_eff)) 'Hz']);
% disp(['r�solution en fr�quence:' num2str(round(resol_f)) 'Hz']);
% disp(['�paisseur couches:' num2str(round(thick_eff*10)/10) 'm']);

% fen�tre de pond�ration de l'analyse spectrale
nShapingSamples = floor(0.4/2*Ndf); % hanning sur 2*0.2
windowFunction  = ((1-cos(pi*(0:2*nShapingSamples-1).'/nShapingSamples))/2).^2;
shapingWindow   = [windowFunction(1:nShapingSamples); ones(Ndf-2*nShapingSamples,1); windowFunction(nShapingSamples+1:end)];

% analyses spectrales
np=floor(size(RawData.Data.PingData,2)/ipdec);
ns=size(RawData.Data.PingData(ichannel,ip0+(ipref-1)*ipdec).SampleData.complexSamples,1);
clear spectrum
nb_fft=ceil((ns-Ndf)/shift);
time_Sv=zeros(1,np);
for ip=1:np
    if size(RawData.Data.PingData(ichannel,ip0+(ip-1)*ipdec).SampleData.complexSamples,1)~=ns
        disp('Pb: change in data range. SvSpectrum routine not adequate');
        return;
    end
    time_Sv(ip)=RawData.Data.PingData(ichannel,ip0+(ip-1)*ipdec).time;
    for ic=1:nb_fft
        data4=RawData.Data.PingData(ichannel,ip0+(ip-1)*ipdec).SampleData.complexSamples(1+(ic-1)*shift:Ndf+(ic-1)*shift,:);
        amp_raw=sum(data4,2)*(wbtImpedanceRx+nominalTransducerImpedance)/wbtImpedanceRx/(2*sqrt(2*nominalTransducerImpedance*nSectors));
        fft_amp_raw=fft(amp_raw.*shapingWindow,Nfft);
        spectrum(ip,ic,:)=fft_amp_raw;
    end
end

%index des fr�quences
f2per=rem(f2,fe);
f1per=rem(f1,fe);
if f1per<f2per
    nper=floor(f1/fe);
    freq=(0:Nfft-1)/Nfft*fe+nper*fe;
else
    freqper=(0:Nfft-1)/Nfft*fe;
    ind_out=find(freqper>f2per & freqper<f1per);
    if ~isempty(freqper)
        iout0=ind_out(ceil(length(ind_out)/2));
    else
        iout0=max(freqper<f1per)+1;
    end
    nper=floor(f1/fe);
    freq=[freqper(1:iout0-1)+(nper+1)*fe freqper(iout0:end)+nper*fe];
end

%r�ordonnancement des fr�quences
[freq,indordf]=sort(freq);
spectrum=spectrum(:,:,indordf);
ifreqB=find(freq>=f1 & freq <=f2);

%spectre redress�
nr=size(spectrum,2)-(floor((freq(ifreqB(end))-f1)*T/B*fe/shift));
np=size(spectrum,1);
nf=size(spectrum,3);
r_spectrum=(Ndf/2+(0:nr-1)*shift)/fe*soundSpeed/2;%range calcul� en milieu de couche
spectrumCor=nan(size(spectrum,1),nr,size(spectrum,3));
for ifr=ifreqB(1):ifreqB(end)
    shift_fr=max(0,floor((freq(ifr)-f1)*T/B*fe/shift));
    spectrumCor(:,1:nr,ifr)=spectrum(:,1+shift_fr:nr+shift_fr,ifr);
end
dec_filtSim=(RawData.Data.FilterData(ichannel,1).nCoefficients+RawData.Data.FilterData(ichannel,2).nCoefficients-...
    (RawData.Data.FilterData(ichannel,1).decimationFactor+RawData.Data.FilterData(ichannel,2).decimationFactor)-T*fe)/2; % shift of pulse beginning due to WBT filtering
r_spectrum=max(r_spectrum-dec_filtSim/fe*soundSpeed/2,0.1);


% EK80 parameters for Sv computation
transmitPower=RawData.Data.PingData(ichannel,ip0+(ipref-1)*ipdec).ParameterData.transmitPower; %W
frequencyCenter=RawData.Data.PingData(ichannel,ip0+(ipref-1)*ipdec).ParameterData.frequencyCenter;
clear absorptionCoefficients
for i=1:length(freq)
    absorptionCoefficients(i) = EstimateAbsorptionCoefficients(RawData.Data.PingData(ichannel,ip0+(ipref-1)*ipdec).EnvironmentData,freq(i));
end
lambda=soundSpeed./freq;
lambdaCenter=soundSpeed./frequencyCenter;
dsp_Pt=nan(1,length(freq));
if (0) % puissance moyenne sur la bande
    dsp_Pt(ifreqB)=transmitPower/B;
else % puissance plus fine calcul�e sur le spectre
    [spectrumTx,freq_Tx,fe_Tx,NfftTx]=TransmitSpectrum(RawData.Data.PingData(ichannel,ip0));
    dsp_Pt=transmitPower*abs(spectrumTx).'.^2/(fe_Tx*NfftTx) * NfftTx/fe_Tx/T;
    nfilt=round(resol_f/(freq_Tx(2)-freq_Tx(1)));
%     dsp_Pt=(filter(ones(1,nfilt)/nfilt,1,sqrt(dsp_Pt))).^2;
        dsp_Pt=(smooth(sqrt(dsp_Pt),nfilt)).^2;
    freq_Tx=freq_Tx-(freq_Tx(2)-freq_Tx(1))*(nfilt-1)/2;
    dsp_Pt= interp1(freq_Tx,dsp_Pt,freq); 
end
effectivePulselLength =Ndf/fe*mean(shapingWindow.^2);
psi=str2num(RawData.Data.ConfigurationData.Transceivers(ichannel).Channels.Transducer.EquivalentBeamAngle)-20*log10(freq/str2num(RawData.Data.ConfigurationData.Transceivers(ichannel).Channels.Transducer.Frequency));
if nargin==3
    %%%%%%%%%%%%%%%%%%% l'estimation effectu�e ci-dessous des gain(f) � appliquer devra �tre remplac�e par les gain(f) lus dans le fichier d'�talonnage (interpol�s aux fr�quences freq)
    gainTable       = str2num(RawData.Data.ConfigurationData.Transceivers(ichannel).Channels.Transducer.Gain);
    gain = gainTable(end)+20*log10(freq/str2num(RawData.Data.ConfigurationData.Transceivers(ichannel).Channels.Transducer.Frequency));
else
    freq_cal=varargin{1};
    gain_cal=varargin{2};
    gain = interp1(freq_cal,gain_cal,freq);  %interpolation en dB?
    indNan=find(isnan(gain));
    gain = smooth(gain,round((10*(freq_cal(end)-freq_cal(1))/length(freq_cal))*(length(freq)/(freq(end)-freq(1)))))'; % on lisse la courbe de gains pour calculer du Sv, on lisse sur l'�quivalent de 10 points de fr�quence de l'�talonnage 
    gain(indNan)=NaN;
end


% Sv(f) computation
Sv_spectrum=20*log10(squeeze(abs(spectrumCor)))-10*log10(fe*Ndf) + 10*log10(Ndf/fe/T)+ repmat(20*log10(r_spectrum),[np 1 nf]) + permute(repmat(2*absorptionCoefficients.'*r_spectrum,[1 1 np]),[3,2,1])+permute(repmat(-10*log10(dsp_Pt.*lambda.^2*soundSpeed/(32*pi^2))-2*gain -10*log10(effectivePulselLength)-psi,[np 1 nr]),[1 3 2]);

%restriction aux ranges>� 0.1m
i0_valid=min(find(r_spectrum>0.1));
r_spectrum=r_spectrum(i0_valid:end);
Sv_spectrum=Sv_spectrum(:,i0_valid:end,:);

% affichage ping ipref
if (0)
    figure;imagesc(freq/1000,r_spectrum,squeeze(Sv_spectrum(ipref,:,:)))
    xlabel('freq kHz');ylabel('depth (m)');title(['ping ' num2str(ipref) , ' Ndf=' num2str(Ndf) ', Sv spectra for ' RawData.Data.ConfigurationData.Transceivers(ichannel).Channels.ChannelIdShort])
    colorbar;caxis([-100 -20])
end



