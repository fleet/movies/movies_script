function [Sv_spectrum,freq,r_spectrum,time_Sv,thick_eff,df_eff,resol_f]=EK80_SvSpectrumMF(RawData,ichannel,Sig_MF,filt,df,varargin)
% df: �chantillonnage spectral demand� (Hz)
% thick_eff: �paisseur effective des couches de Sv en m�tres
% Sv_spectrum: matrice ping x range x freq de Sv(f)
% freq: vecteur des fr�quences associ�es (Hz)
% r_spectrum: vecteur des ranges associ�s (m)
% df_eff: �chantillonnage spectral effectif (Hz)
% resol_f: r�solution spectrale (Hz)
% varargin:freq_cal,gain_cal: optionnel, pour renseigner des gains d'�talonnage (dB) en fonction de la fr�quence (Hz)

nchannel=size(RawData.Data.PingData,1);
ipref=1; % index du ping pour l'affichage de l'image des spectres de Sv

if strcmp(RawData.Data.ConfigurationData.Transceivers(ichannel).TransceiverType,'GPT')
    uiwait(msgbox('GPT channel: stop processing spectrum. Choose WBT channel'));
    return;
end

%d�termination �mission s�quentielle ou simultan�e
if nchannel==1
    seq=0; 
elseif isempty(RawData.Data.PingData(1,1).SampleData) | isempty(RawData.Data.PingData(2,1).SampleData)
    seq=1;
else
    seq=0;
end
if seq==0
    ipdec=1;
    ip0=1;
else
    ipdec=nchannel;
    ip0=1;
    while isempty(RawData.Data.PingData(ichannel,ip0).SampleData)
        ip0=ip0+1;
    end
end

if (~RawData.Data.PingData(ichannel,ip0).ParameterData.pulseForm) %CW
    uiwait(msgbox('CW mode. Stop processing'));
    return;
end

%param�tres EK80
nSectors = size(RawData.Data.PingData(ichannel,ip0+(ipref-1)*ipdec).SampleData.complexSamples,2);
nominalTransducerImpedance  = 75;
wbtImpedanceRx              = 5e3;
fe=1/RawData.Data.PingData(ichannel,ip0+(ipref-1)*ipdec).ParameterData.sampleInterval;
soundSpeed=RawData.Data.PingData(ichannel,ip0+(ipref-1)*ipdec).EnvironmentData.soundSpeed;

f1=RawData.Data.PingData(ichannel,ip0+(ipref-1)*ipdec).ParameterData.frequencyStart;
f2=RawData.Data.PingData(ichannel,ip0+(ipref-1)*ipdec).ParameterData.frequencyStop;
T=RawData.Data.PingData(ichannel,ip0+(ipref-1)*ipdec).ParameterData.pulseLength;
B=f2-f1;

% frequency resolution limitation
if df>B/2
    disp('        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
    df=round(B/2);
    disp(['        Too large frequency resolution requested. Reduced to:' num2str(df) 'Hz']);
    disp('        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
end
if df<1/T
    disp('        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
    df=round(1/T);
    disp(['        Too small frequency resolution requested. Increased to:' num2str(df) 'Hz']);
    disp('        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
end

% taille de fft 
% (Ndf est le nombre de point effectifs de signal, Nfft le
% nombre de points total avec zero-padding)
Ndf=round(fe/df*1.25); % avec hanning sur 2*0.2
Nfft=2^(ceil(log2(Ndf)));
Tfft=Nfft/fe;
resol_f=fe/Ndf*1.25;% avec hanning sur 2*0.2
df_eff=fe/Nfft;

thick_eff=Ndf/fe*soundSpeed/2/1.25; %/1.25 with Hanning?

% d�calage des analyses spectrales
shift=max(1,floor(Ndf/4));

% % display des param�tres
% disp(' ');
% disp(['nombre de points de fft:' num2str(Nfft)]);
% disp(['�chantillonnage en fr�quence:' num2str(round(df_eff)) 'Hz']);
% disp(['r�solution en fr�quence:' num2str(round(resol_f)) 'Hz']);
% disp(['�paisseur couches:' num2str(round(thick_eff*10)/10) 'm']);

% fen�tre de pond�ration de l'analyse spectrale
nShapingSamples = floor(0.4/2*Ndf); % hanning sur 2*0.2
windowFunction  = ((1-cos(pi*(0:2*nShapingSamples-1).'/nShapingSamples))/2).^2;
shapingWindow   = [windowFunction(1:nShapingSamples); ones(Ndf-2*nShapingSamples,1); windowFunction(nShapingSamples+1:end)];
%shapingWindow   = shapingWindow*0+1;

% analyses spectrales
np=size(Sig_MF,1);
ns=size(Sig_MF,2);
clear spectrum

%filtre avec m�me traitement et bon df
MFfilt=xcorr(filt,filt); %filtre MF
imed=ceil(length(MFfilt)/2);
fft_filt=fft(MFfilt(imed-floor((Ndf-1)/2):imed+floor(Ndf/2)).*shapingWindow,Nfft);
fft_filt=sqrt(abs(fft_filt)).';


i1=1;
i2=ns;
nb_fft=floor((i2-i1-Ndf+1)/shift)+1;
time_Sv=zeros(1,np);
for ip=1:np
    time_Sv(ip)=RawData.Data.PingData(ichannel,ip0+(ip-1)*ipdec).time;
    for ic=1:nb_fft
        amp_MF=Sig_MF(ip,i1+(ic-1)*shift:i1+Ndf-1+(ic-1)*shift);
        fft_amp_MF=fft(amp_MF.*shapingWindow.',Nfft);
        %spectrum(ip,ic,:)=fft_amp_MF;
        spectrum(ip,ic,:)=fft_amp_MF./conj(fft_filt);
    end
end

%index des fr�quences
f2per=rem(f2,fe);
f1per=rem(f1,fe);
if f1per<f2per
    nper=floor(f1/fe);
    freq=(0:Nfft-1)/Nfft*fe+nper*fe;
else
    freqper=(0:Nfft-1)/Nfft*fe;
    ind_out=find(freqper>f2per & freqper<f1per);
    if ~isempty(freqper)
        iout0=ind_out(ceil(length(ind_out)/2));
    else
        iout0=max(freqper<f1per)+1;
    end
    nper=floor(f1/fe);
    freq=[freqper(1:iout0-1)+(nper+1)*fe freqper(iout0:end)+nper*fe];
end

%r�ordonnancement des fr�quences
[freq,indordf]=sort(freq);
spectrum=spectrum(:,:,indordf);
ifreqB=find(freq>=f1 & freq <=f2);

% EK80 parameters for Sv computation
transmitPower=RawData.Data.PingData(ichannel,ip0+(ipref-1)*ipdec).ParameterData.transmitPower; %W
frequencyCenter=RawData.Data.PingData(ichannel,ip0+(ipref-1)*ipdec).ParameterData.frequencyCenter;
clear absorptionCoefficients
for i=1:length(freq)
    absorptionCoefficients(i) = EstimateAbsorptionCoefficients(RawData.Data.PingData(ichannel,ip0+(ipref-1)*ipdec).EnvironmentData,freq(i));
end
lambda=soundSpeed./freq;
lambdaCenter=soundSpeed./frequencyCenter;

dsp_Pt=nan(1,length(freq));
ifreqB=find(freq>=f1 & freq <=f2);

if (0) % puissance moyenne sur la bande
    dsp_Pt(ifreqB)=transmitPower/B;
elseif (0) % puissance plus fine calcul�e sur le spectre, liss�e
    [spectrumTx,freq_Tx,fe_Tx,NfftTx,normFilt]=TransmitSpectrum(RawData.Data.PingData(ichannel,ip0));
    dsp_Pt=transmitPower*abs(spectrumTx).'.^2/(fe_Tx*NfftTx) * NfftTx/fe_Tx/T;
    nfilt=round(resol_f/(freq_Tx(2)-freq_Tx(1)));
    dsp_Pt=(filter(ones(1,nfilt)/nfilt,1,sqrt(dsp_Pt))).^2;
    freq_Tx=freq_Tx-(freq_Tx(2)-freq_Tx(1))*(nfilt-1)/2;
    dsp_Pt= interp1(freq_Tx,dsp_Pt,freq); 
else % puissance plus fine calcul�e sur le spectre, m�me traitement
    [transmitSignal] = CreateTransmitSignal(RawData.Data.PingData(ichannel,ip0+(ipref-1)*ipdec));
    dsp_Pt= transmitPower*(fft_filt(indordf)*norm(transmitSignal)^2).^2/(fe^2*T); 
end

if (0)
    effectivePulselLength =Ndf/fe*mean(shapingWindow.^2);
else
    %[transmitSignal] = CreateTransmitSignal(RawData.Data.PingData(ichannel,ip0+(ipref-1)*ipdec));
    % MFsig=xcorr(transmitSignal,filt); 
     MFsig=xcorr(filt,filt); 
     nsig=length(MFsig);
     Sasig=[zeros(nsig,1);MFsig; zeros(nsig,1)];
     clear spSa
     for i=1:length(Sasig)-Ndf
         spSa(i,:)=fft(Sasig(i:i+Ndf-1).*shapingWindow,Nfft)./conj(fft_filt).';
     end

     %iref=round(size(spSa,1)/2);
     iref=round(1.5*nsig-Ndf/2);
     sa=sum(abs(spSa).^2,1)/fe;
     spref=abs(spSa(iref,:)).^2;

     %sacorr=10*log10(sa./spref)-10*log10(Ndf/fe*mean(shapingWindow.^2)); 
     effectivePulselLength=sa./spref; 
     effectivePulselLength=effectivePulselLength(indordf); 
end

psi=str2num(RawData.Data.ConfigurationData.Transceivers(ichannel).Channels.Transducer.EquivalentBeamAngle)-20*log10(freq/str2num(RawData.Data.ConfigurationData.Transceivers(ichannel).Channels.Transducer.Frequency));

if nargin==5
    %%%%%%%%%%%%%%%%%%% l'estimation effectu�e ci-dessous des gain(f) � appliquer devra �tre remplac�e par les gain(f) lus dans le fichier d'�talonnage (interpol�s aux fr�quences freq)
    gainTable       = str2num(RawData.Data.ConfigurationData.Transceivers(ichannel).Channels.Transducer.Gain);
    gain = gainTable(end)+20*log10(freq/str2num(RawData.Data.ConfigurationData.Transceivers(ichannel).Channels.Transducer.Frequency));
else
    freq_cal=varargin{1};
    gain_cal=varargin{2};
    
    %gain moyen pour la resolution demandee
    df_cal=median(freq_cal(2:end)-freq_cal(1:end-1))/2;
    nfilt=round(resol_f/df_cal);
    
    freq_interp=(freq_cal(1):df_cal:freq_cal(end));
    if freq_interp(end)<freq_cal(end) freq_interp(end+1)=freq_cal(end); end
    gain_cal_interp=interp1(freq_cal,gain_cal,freq_interp); %interpole dans les stop bands
    
    gain_df=(filter(ones(1,nfilt)/nfilt,1,10.^(gain_cal_interp/10)));
    freq_gain_df=freq_interp-df_cal*(nfilt-1)/2;
    gain = 10*log10(interp1(freq_gain_df,gain_df,freq));  

    %figure;hold on;plot(freq_cal/1000,gain_cal);plot(freq/1000,gain);title('u')
end

np=size(spectrum,1);
nf=size(spectrum,3);
nr=size(spectrum,2);

r_spectrum=(Ndf/2+(0:nr-1)*shift)/fe*soundSpeed/2;%range calcul� en milieu de couche
% dec_filtSim=(RawData.Data.FilterData(ichannel,1).nCoefficients+RawData.Data.FilterData(ichannel,2).nCoefficients-...
%     (RawData.Data.FilterData(ichannel,1).decimationFactor+RawData.Data.FilterData(ichannel,2).decimationFactor)-T*fe)/2; % shift of pulse beginning due to WBT filtering
% f_ini=1.5*10^6; 
% dec_filtSim=(RawData.Data.FilterData(ichannel,1).nCoefficients/f_ini + RawData.Data.FilterData(ichannel,2).nCoefficients/(f_ini/RawData.Data.FilterData(ichannel,1).decimationFactor))/2;% shift of pulse beginning due to WBT filtering
% r_spectrum=max(r_spectrum-dec_filtSim/fe*soundSpeed/2,0.1);
r_spectrum=max(r_spectrum,0.1);

% Sv(f) computation
Sv_spectrum=20*log10(squeeze(abs(spectrum)))-10*log10(fe^2*T) + repmat(20*log10(r_spectrum),np,1,nf) + permute(repmat(2*absorptionCoefficients.'*r_spectrum,1,1,np),[3,2,1])+permute(repmat(-10*log10(dsp_Pt.*lambda.^2*soundSpeed/(32*pi^2))-2*gain -10*log10(effectivePulselLength)-psi,np,1,nr),[1 3 2]);
DSP_noise_ini=20*log10(squeeze(abs(spectrum)))-10*log10(fe^2*T);

% affichage ping ipref
if (0)
    figure;imagesc(freq/1000,r_spectrum,squeeze(Sv_spectrum(ipref,:,:)))
    xlabel('freq kHz');ylabel('depth (m)');title(['ping ' num2str(ipref) , ' Ndf=' num2str(Ndf) ', Sv spectra for ' RawData.Data.ConfigurationData.Transceivers(ichannel).Channels.ChannelIdShort])
    colorbar;caxis([-100 -20])
end



