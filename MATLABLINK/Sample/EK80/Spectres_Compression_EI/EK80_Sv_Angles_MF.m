function [Sv_MF,r,Along_MF_Deg,Athwart_MF_Deg,Phase_MF]=EK80_Sv_Angles_MF(RawData,ichannel)
tic
%détermination ping d'émission
ip_list=[];
for ip=1:size(RawData.Data.PingData,2)
    if ~isempty(RawData.Data.PingData(ichannel,ip).time)
        ip_list(end+1)=ip;
    end
end
% détermination wbt et tsd (WBT tube)
if size(RawData.Data.PingData,1)==size(RawData.Data.ConfigurationData.Transceivers,2) %1freq/wbt
    iwbt=ichannel;
    itsd=1;
else
    wbt_list=[];
    tsd_list=[];
    for iwbt=1:size(RawData.Data.ConfigurationData.Transceivers,2)
        for itsd=1:size(RawData.Data.ConfigurationData.Transceivers(iwbt).Channels,2)
            wbt_list(end+1)=iwbt;
            tsd_list(end+1)=itsd;
        end
    end
    iwbt=wbt_list(ichannel);
    itsd=tsd_list(ichannel);
end
    

if strcmp(RawData.Data.ConfigurationData.Transceivers(iwbt).TransceiverType,'GPT')
    warndlg('GPT channel: stop processing spectrum. Choose WBT channel');
    return;
end

if (~RawData.Data.PingData(ichannel,ip_list(1)).ParameterData.pulseForm) %CW
    uiwait(msgbox('CW mode. Stop processing'));
    return;
end


%paramètres EK80
nSectors = size(RawData.Data.PingData(ichannel,ip_list(1)).SampleData.complexSamples,2);
nominalTransducerImpedance  = 75;
wbtImpedanceRx              = 5e3;
fe=1/RawData.Data.PingData(ichannel,ip_list(1)).ParameterData.sampleInterval;


f1=RawData.Data.PingData(ichannel,ip_list(1)).ParameterData.frequencyStart;
f2=RawData.Data.PingData(ichannel,ip_list(1)).ParameterData.frequencyStop;
T=RawData.Data.PingData(ichannel,ip_list(1)).ParameterData.pulseLength;
B=f2-f1;

% Transmit signal
transmitSignal = CreateTransmitSignal(RawData.Data.PingData(ichannel,ip_list(1)));
filt=transmitSignal/norm(transmitSignal)^2;

% taille de fft (pour la compression d'impulsion)
Nfft=2*ceil(max(T,length(filt)/fe)*fe);
%Nfft=2^(ceil(log2(Nfft)));

np=length(ip_list);
ns=size(RawData.Data.PingData(ichannel,ip_list(1)).SampleData.complexSamples,1);
nb_fft=floor(ns/Nfft*2-1);

fft_filt=fft(filt,Nfft);

% Correlation with FFT
clear SigSectors_MF
for ip=1:np
%for ip=1218:1218
    for ic=1:nb_fft
        data4=RawData.Data.PingData(ichannel,ip_list(ip)).SampleData.complexSamples(1+(ic-1)*Nfft/2:(ic+1)*Nfft/2,:);
        amp_raw=data4*(wbtImpedanceRx+nominalTransducerImpedance)/wbtImpedanceRx/(2*sqrt(2*nominalTransducerImpedance*nSectors));
        %amp_raw=sum(data4,2);
        fft_amp_raw=fft(amp_raw);
        fft_MF=fft_amp_raw.*repmat(conj(fft_filt),1,nSectors);
        corrFilt=ifft(fft_MF);
        SigSectors_MF(ip,1:nSectors,1+(ic-1)*Nfft/2:ic*Nfft/2)=corrFilt(1:Nfft/2,:).';
    end
end



% Sig_MF
Sig_MF = squeeze(sum(SigSectors_MF,2));

% Angles MF
f1=RawData.Data.PingData(ichannel,ip_list(1)).ParameterData.frequencyStart;
f2=RawData.Data.PingData(ichannel,ip_list(1)).ParameterData.frequencyStop;
f_tsd=str2num(RawData.Data.ConfigurationData.Transceivers(iwbt).Channels(itsd).Transducer.Frequency);
AngleSensitivityAlongship=str2num(RawData.Data.ConfigurationData.Transceivers(iwbt).Channels(itsd).Transducer.AngleSensitivityAlongship);
AngleSensitivityAthwartship=str2num(RawData.Data.ConfigurationData.Transceivers(iwbt).Channels(itsd).Transducer.AngleSensitivityAthwartship);

complexFore         = squeeze(sum(SigSectors_MF(:,3:4,:),2)/2);
complexAft          = squeeze(sum(SigSectors_MF(:,1:2,:),2)/2);
complexStarboard    = squeeze((SigSectors_MF(:,1,:) + SigSectors_MF(:,4,:))/2);
complexPort         =  squeeze(sum(SigSectors_MF(:,2:3,:),2)/2);


Along_MF_Deg    = squeeze(angle( complexFore.*conj(complexAft)) *180/pi/AngleSensitivityAlongship*f_tsd/((f1+f2)/2));
Athwart_MF_Deg  = squeeze(angle( complexStarboard.*conj(complexPort)) *180/pi/AngleSensitivityAthwartship*f_tsd/((f1+f2)/2));

            
Power_MF=20*log10(abs(Sig_MF));
Phase_MF=angle(Sig_MF);

% EK80 parameters
transmitPower=RawData.Data.PingData(ichannel,ip_list(1)).ParameterData.transmitPower; %W
soundSpeed=RawData.Data.PingData(ichannel,ip_list(1)).EnvironmentData.soundSpeed;
f1=RawData.Data.PingData(ichannel,ip_list(1)).ParameterData.frequencyStart;
f2=RawData.Data.PingData(ichannel,ip_list(1)).ParameterData.frequencyStop;
frequencyCenter=(f1+f2)/2;
absorptionCoefficientsCenter = EstimateAbsorptionCoefficients(RawData.Data.PingData(ichannel,ip_list(1)).EnvironmentData,frequencyCenter);
lambdaCenter=soundSpeed./frequencyCenter;
gainTable       = str2num(RawData.Data.ConfigurationData.Transceivers(ichannel).Channels.Transducer.Gain);
gainCenter = gainTable(end)+20*log10(frequencyCenter/str2num(RawData.Data.ConfigurationData.Transceivers(ichannel).Channels.Transducer.Frequency));
%effectivePulselLength =1/B;
autoCorrelationTransmitSignal = conv(transmitSignal,flipud(conj(transmitSignal)))/norm(transmitSignal)^2;
autoCorrelationTransmitSignalPower = (abs(autoCorrelationTransmitSignal).^2);
effectivePulselLength  = 1/fe * sum(autoCorrelationTransmitSignalPower) / max(autoCorrelationTransmitSignalPower);
psiCenter=str2num(RawData.Data.ConfigurationData.Transceivers(ichannel).Channels.Transducer.EquivalentBeamAngle)-20*log10(frequencyCenter/str2num(RawData.Data.ConfigurationData.Transceivers(ichannel).Channels.Transducer.Frequency));

r=(0:size(Power_MF,2)-1)*soundSpeed/2/fe;
tvg20=max(0,20*log10(r)+2*absorptionCoefficientsCenter*r);

% Sv after matched filtering
Sv_MF = Power_MF + repmat(tvg20 - 10*log10(transmitPower*lambdaCenter^2*soundSpeed/(32*pi^2)) - 2*gainCenter -10*log10(effectivePulselLength)-psiCenter,np,1) ;

figure;imagesc((1:np),r,Sv_MF.');
xlabel('pings');ylabel('depth (m)');title(['Sv MF for ' RawData.Data.ConfigurationData.Transceivers(ichannel).Channels.ChannelIdShort])
colorbar;caxis([-100 -20])
toc
