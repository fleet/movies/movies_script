%RawData=RawData280360;
%RawData=RawData260460;
% input: noise spectrum from raw data
nchannel=size(RawData.Data.PingData,1);
ichannel=1;
nSectors = size(RawData.Data.PingData(ichannel,(ichannel-1)+1*nchannel).SampleData.complexSamples,2);
nominalTransducerImpedance  = 75;
wbtImpedanceRx              = 5e3;
fe=1/RawData.Data.PingData(ichannel,(ichannel-1)+1*nchannel).ParameterData.sampleInterval;

if (0) %set Nfft
    N=1024;
    df=fe/N;
else %set df
    df=250;
    N=round(fe/df);
end
Tfft=N/fe;
np=3; %number of pings to average

fd=1500000/RawData.Data.FilterData(ichannel,1).decimationFactor/RawData.Data.FilterData(ichannel,2).decimationFactor; %?


% raw noise spectrum
fft_all2=zeros(N,1);
for ip=1:np
    %u4=RawData.Data.PingData(ichannel,(ichannel-1)+ip*nchannel).SampleData.complexSamples(4000:6500,:);
    u4=RawData.Data.PingData(ichannel,(ichannel-1)+ip*nchannel).SampleData.complexSamples(1000:2000,:);
    usum=sum(u4,2)*(wbtImpedanceRx+nominalTransducerImpedance)/wbtImpedanceRx/(2*sqrt(2*nominalTransducerImpedance*nSectors));
    fft_usum=fft(usum(1:N));
    fft_all2=fft_all2+abs(fft_usum).^2;
end
fft_all2=fft_all2/np;
dsp_noise_ini=fft_all2/(fe^2*Tfft);

if fd<RawData.Data.PingData(ichannel,(ichannel-1)+1*nchannel).ParameterData.frequencyStart
    freq=fd+(0:N-1)/N*fe;
else
    dsp_noise_ini=fftshift(dsp_noise_ini);
    freq=fd+(0:N-1)/N*fe-fe/2;
end
nfreq=N;

figure(1);plot(freq/1000,10*log10(dsp_noise_ini));grid on;ylim([-220 -150]);xlim([0 500])
xlabel('kHz');ylabel('dB ref 1W');title('Noise Power Spectral Density dB ref 1W')

% pulse parameters...
if (0)
    f1=260*1000;
    f2=280*1000;
    T=2048e-6;
else
    f1=RawData.Data.PingData(ichannel,(ichannel-1)+1*nchannel).ParameterData.frequencyStart;
    f2=RawData.Data.PingData(ichannel,(ichannel-1)+1*nchannel).ParameterData.frequencyStop;
    T=RawData.Data.PingData(ichannel,(ichannel-1)+1*nchannel).ParameterData.pulseLength;
end
B=f2-f1;

ifreqB=find(freq>=f1 & freq <=f2);
figure(1);hold on;plot(freq(ifreqB)/1000,10*log10(dsp_noise_ini(ifreqB)),'r');hold off

%Noise
P_noise_ini=10*log10(sum(dsp_noise_ini(ifreqB))*df)
P_noise_MF=P_noise_ini-10*log10(B*T);
dsp_noise_MF=dsp_noise_ini/(B*T);

if (0) %comparison with direct MF processing
    % 
    transmitSignal = CreateTransmitSignal(RawData.Data.PingData(ichannel,(ichannel-1)+1*nchannel));
    filt=transmitSignal/norm(transmitSignal)^2;
    fft_conv2=zeros(N,1);
    uconv2=zeros(2501,1);
    % clear fft_x
    for ip=1:np
        u4=RawData.Data.PingData(ichannel,(ichannel-1)+ip*nchannel).SampleData.complexSamples(4000:6500,:);
        %u4=RawData.Data.PingData(ichannel,(ichannel-1)+ip*nchannel).SampleData.complexSamples(1000:3000,:);
        usum=sum(u4,2)*(wbtImpedanceRx+nominalTransducerImpedance)/wbtImpedanceRx/(2*sqrt(2*nominalTransducerImpedance*nSectors));
        uconv=conv2(flipud(conj(filt)),1,usum);
        uconv = uconv(length(filt):end);
        uconv2=uconv2+abs(uconv).^2;
        fft_conv2=fft_conv2+abs(fft(uconv(1:N))).^2;
    %     N2=N/1000;
    %     for ibin=1:N
    %         %fft_x(ibin)=sum(usum(ibin:ibin+1).'.*exp(-sqrt(-1)*2*pi*(ibin-1)*(ibin:ibin+1)/N));
    %         %ind=[zeros(1,ibin-1) ones(1,N2) zeros(1,N-N2-ibin+1)];
    %         ind=(ibin:ibin+N2-1);
    %         ind=mod(ind-1,N)+1;
    %         %fft_x(ibin)=sum(usum(1:N).'.*exp(-sqrt(-1)*2*pi*(ibin-1)*(0:N-1)/N));
    %         fft_x(ibin)=sum(usum(ind).'.*exp(-sqrt(-1)*2*pi*(ibin-1)*(ind-1)/N));
    %     end
    %     fft_xall=fft_xall+abs(fft_x.').^2;
    end
    % fft_xall=fft_xall/np;
    % dsp_noise_x=fft_xall/(fe^2*Tfft);
    fft_conv2=fft_conv2/np;
    uconv2=uconv2/np;
    dsp_noise_MF0=fft_conv2/(fe^2*Tfft);
    figure;plot(freq/1000,10*log10(dsp_noise_MF))
    hold on;plot(freq/1000,10*log10(dsp_noise_MF0),'r');hold off
    legend('dsp raw - 10.logBT','dsp after MF')
    figure;plot(10*log10(uconv2));hold on;plot(uconv2*0+P_noise_MF,'r');hold off
    legend('mean ping power after MF','Noise ini - 10*log(BT)');xlabel('sample')
end


% EK80 parameters
transmitPower=RawData.Data.PingData(ichannel,(ichannel-1)+1*nchannel).ParameterData.transmitPower; %W
soundSpeed=RawData.Data.PingData(ichannel,(ichannel-1)+1*nchannel).EnvironmentData.soundSpeed;
clear absorptionCoefficients
for i=1:length(freq)
    absorptionCoefficients(i) = EstimateAbsorptionCoefficients(RawData.Data.PingData(ichannel,(ichannel-1)+1*nchannel).EnvironmentData,freq(i));
end
lambda=soundSpeed./freq;
gainTable       = str2num(RawData.Data.ConfigurationData.Transceivers(ichannel).Channels.Transducer.Gain);
gain = gainTable(end)+20*log10(freq/str2num(RawData.Data.ConfigurationData.Transceivers(ichannel).Channels.Transducer.Frequency));
effectivePulselLength =1/B;
%effectivePulselLength =df*T/B*soundSpeed/2;
psi=str2num(RawData.Data.ConfigurationData.Transceivers(ichannel).Channels.Transducer.EquivalentBeamAngle)-20*log10(freq/str2num(RawData.Data.ConfigurationData.Transceivers(ichannel).Channels.Transducer.Frequency));
dsp_Pt=nan(1,nfreq);
dsp_Pt(ifreqB)=transmitPower/B;

r=(1:1:1000);

%% Single target range
%%%%%%%%%%%%%%%%%%%%
TS_thr=zeros(1,nfreq);
TS_thr(:)=-100;
TS_thr_MF=-100;

% Single target spectrum
TS=nan(length(r),nfreq);
for i=1:length(r)
    TS(i,:)=10*log10(dsp_noise_ini).' +40*log10(r(i))+2*absorptionCoefficients*r(i)-10*log10(dsp_Pt.*lambda.^2/(16*pi^2))-2*gain;
end
TS=TS-5*log10(B*T); %optimal spectral analysis foir noise reduction
figure(5);imagesc(freq(ifreqB)/1000,r,TS(:,ifreqB));caxis([-90 -20]);colorbar
xlabel('kHz');ylabel('range m');title('TS(f) dB')
hold on;[c, h] = contour(freq(ifreqB)/1000,r,TS(:,ifreqB),[-60 -50 -40],'k'); clabel(c, h);hold off

% spectrum range limit
range_TS=nan(1,nfreq);
for i=1:length(ifreqB)
    range_TS(ifreqB(i))=r(max(find(TS(:,ifreqB(i))<=TS_thr(ifreqB(i)))));
end
figure(6);plot(freq(ifreqB)/1000,range_TS(ifreqB));grid
xlabel('kHz');ylabel('range m');title(['range where noise induces TS(f)=' num2str(median(TS_thr)) 'dB'])

% MF range limit
TS_MF=10*log10(mean(10.^(TS(:,ifreqB)/10),2))-5*log10(B*T); %keep -5 for the moment (i.e -10/ini) /BT (MF and compression (compensation of mean over T))
range_TS_MF=r(max(find(TS_MF<=TS_thr_MF)));
figure(7);plot(r,TS_MF);xlabel('range m');ylabel('TS compressed');grid on;title(['range where noise induces TS=' num2str(TS_thr_MF) 'dB =' num2str(round(range_TS_MF)) 'm'])

%% Volume backscatter 
% 
Sv_thr=zeros(1,nfreq);
Sv_thr(:)=-60;
Sv_thr_MF=-100;

Sv=nan(length(r),nfreq);
for i=1:length(r)
    Sv(i,:)=10*log10(dsp_noise_ini).' +20*log10(r(i))+2*absorptionCoefficients*r(i)-10*log10(dsp_Pt.*lambda.^2*soundSpeed/(32*pi^2))-2*gain-10*log10(effectivePulselLength)-psi;
end
Sv=Sv-10*log10(B*T); % no BT compression gain on noise
figure(2);imagesc(freq(ifreqB)/1000,r,Sv(:,ifreqB));caxis([-100 -20]);colorbar
xlabel('kHz');ylabel('range m');title('Sv(f) dB')
hold on;[c, h] = contour(freq(ifreqB)/1000,r,Sv(:,ifreqB),[-70 -60 -50],'k'); clabel(c, h);hold off

range_Sv=nan(1,nfreq);
for i=1:length(ifreqB)
    range_Sv(ifreqB(i))=r(max(find(Sv(:,ifreqB(i))<=Sv_thr(ifreqB(i)))));
end
figure(3);plot(freq(ifreqB)/1000,range_Sv(ifreqB));grid
xlabel('kHz');ylabel('range m');title(['range where noise induces Sv(f)=' num2str(median(Sv_thr)) 'dB'])

Sv_MF=10*log10(mean(10.^(Sv(:,ifreqB)/10),2));%-10*log10(B*T);
range_Sv_MF=r(max(find(Sv_MF<=Sv_thr_MF)));
figure(4);plot(r,Sv_MF);xlabel('range m');ylabel('Sv compressed');grid on;title(['range where noise induces Sv=' num2str(Sv_thr_MF) 'dB =' num2str(round(range_Sv_MF)) 'm'])



