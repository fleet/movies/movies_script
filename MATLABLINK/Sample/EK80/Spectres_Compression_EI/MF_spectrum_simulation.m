%% pulse parameters
fe=1.5e6; % sampling frequency Hz
T=2048e-6; % pulse length s
f1=150e3; % starting frequency Hz
f2=250e3; % stoping frequency Hz
B=f2-f1; % bandwidth

nSamples = floor(T*fe);
timeVector  = 1/fe*(0:nSamples-1)';

disp(['1/B -> ' num2str(ceil(fe/B)) ' samples']);

%% Tx signal (introduce optional spectrum target response L17, or noise L21)
transmitSignal = chirp(timeVector,f1,T,f2)*sqrt(2);

SignalShape=ones(nSamples,1);
%SignalShape(1:nSamples/4)=4; %modulation of "target response"

noise=zeros(nSamples,1);
SNRraw=-5; %initial SNR (before MF) in dB (within bandwidth B)
%noise=10^(-SNRraw/20)*sqrt(fe/B)*randn(nSamples,1); % compensate level for noise generated in bandwidth fe

TxSignal=transmitSignal.*SignalShape+noise;

figure(1);plot(timeVector,TxSignal);title('Tx Signal');
xlabel('s');ylabel('dB')

%% Signal Spectrum
fft_Tx=fft(TxSignal);
freq=(0:nSamples-1)/nSamples*fe;
figure(2);plot(freq/1000,20*log10(abs(fft_Tx)));title('Signal Spectrum')
xlabel('kHz');ylabel('dB')
 
%% MF Signal
MF=conv2(flipud(conj(TxSignal)),1,transmitSignal)/norm(transmitSignal)^2;
MF = fftshift(MF(length(transmitSignal)/2:end-length(transmitSignal)/2));
figure(3);plot(20*log10(abs(MF)));title(['20*log|MF|, 1/B=' num2str(round(1/B*10^6)) '�s, 1/B ->' num2str(ceil(fe/B)) ' samples'])
xlabel('samples');ylabel('dB');grid on


%% initial MF Spectrum 
ifig=4;
col='m-*';
fft_MF=fft(MF);
figure(ifig);plot(freq/1000,20*log10(abs(fft_MF)));title(['MF Spectrum (T= ' num2str(round(T*10^6)) '�s, 1/B=' num2str(round(1/B*10^6)) '�s)'])
xlabel('kHz');ylabel('dB')
 

%% MF Spectrum on Tfft<T (set ncut parameter L51, and 0-padding or not L53)
nfft=length(fft_MF);
ncut=100; %number of samples of MF signal kept for FFT
MFcut = [MF(1:ceil(ncut/2)) ; MF(end-floor(ncut/2)+1:end)]; % select ncut samples centered on MF peak
if (0) %0-padding for same freq sampling
    fft_MF2=fft(MFcut,nfft);  %can change 0-padding rate (nfft-> ...)
else % no zero padding (reflects frequency resolution)
    fft_MF2=fft(MFcut,ncut); 
end
freqMF=(0:length(fft_MF2)-1)/length(fft_MF2)*fe;
figure(ifig);hold on;plot(freqMF/1000,20*log10(abs(fft_MF2)),col);hold off;
legend(['initial MF spectrum on T= ' num2str(round(T*10^6)) '�s'],['MF spectrum on Tfft= ' num2str(round(ncut/fe*10^6)) '�s'])

