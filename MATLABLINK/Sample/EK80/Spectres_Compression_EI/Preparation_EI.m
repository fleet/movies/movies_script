function [Sv, freq_simple_channel, Time, range_Sv, resol_f_Sv,thick_eff_Sv]=Preparation_EI(RawData,df,Gain_init_GPT, Sa_init_GPT, Gain_cor_GPT, Sa_cor_GPT, Transducer, frequencies_begin, frequencies_end,erreur_ramping,compute_from_MF)
tic

% rejeu des jeu de donnees Raw EK80
%   
%   Description: 
%   Extrait les Sv(f) des donn�es RAW
%   Incorpore la calibration


%   References: Script SIMRAD EK80 Lars Nonboe Andersen
%
%   Created by Arthur Blanluet and Naig Le Bouffant
%
%
%

nFiles = length(RawData);
nsdr=size(RawData(1).Data.PingData,1);

step=5; % nombre de fichiers � traiter avant enregistrement interm�diaire (pour limiter la charge m�m�oire et le ralentissement)
Sv = cell(nFiles, nsdr);
freq_simple_channel = cell(nFiles, nsdr);
Time = cell(nFiles, nsdr);
range_Sv = cell(nFiles, nsdr);
resol_f_Sv = cell(nFiles, nsdr);
thick_eff_Sv = cell(nFiles, nsdr);

mkdir('.','temp_prepa_EI');
dirsave='./temp_prepa_EI/';

for fileNumber = 1:nFiles;

    nchannel=size(RawData(fileNumber).Data.PingData,1);

    for ichannel = 1:nchannel % channel index   (1 � 6 pour 18kHz � 333kHz).
               ip0=1;
                while isempty(RawData(fileNumber).Data.PingData(ichannel,ip0).time) 
                    ip0=ip0+1;
                    if ip0>size( RawData(fileNumber).Data.PingData,2) break;end
                end
         if strcmp(RawData(fileNumber).Data.ConfigurationData.Transceivers(ichannel).TransceiverType,'GPT') ... % Si GPT
            || ( strcmp(RawData(fileNumber).Data.ConfigurationData.Transceivers(ichannel).TransceiverType,'WBT') ...
            && ~RawData(fileNumber).Data.PingData(ichannel,ip0).ParameterData.pulseForm)

             [BasicProcessedData] = BasicProcessData(RawData(fileNumber),ichannel);

             empty_elems = arrayfun(@(s) all(structfun(@isempty,s)), BasicProcessedData(1).ProcessedSampleData); % virer les cellules vides (1 ping sur 6)
             BasicProcessedData(1).ProcessedSampleData = BasicProcessedData(1).ProcessedSampleData(~empty_elems);     
             rawChannel=RawData(fileNumber).Data.PingData(ichannel,~empty_elems);
             
             np=size(BasicProcessedData(1).ProcessedSampleData,2);
             Time_channel=zeros(1,np);
             clear sv_spectrum_simple range_spectrum
             
             freq_simple = str2num(RawData(fileNumber).Data.ConfigurationData.Transceivers(ichannel).Channels.Transducer.Frequency)/1000;

             if strcmp(RawData(fileNumber).Data.ConfigurationData.Transceivers(ichannel).TransceiverType,'GPT')
                calibration = 2*(Gain_init_GPT(ichannel) - Gain_cor_GPT(ichannel)) + 2*(Sa_init_GPT(ichannel) - Sa_cor_GPT(ichannel));
             else 
                 calibration=0;
             end
                         
             for z = 1:np
                 Time_channel(z)=rawChannel(z).time;
                 sv_spectrum_simple(z,:,1) = BasicProcessedData(1).ProcessedSampleData(z).sv + calibration;
                 rsv=BasicProcessedData(1).ProcessedSampleData(z).svRange;
                 range_spectrum(z,1:size(rsv,1))= rsv;
             end
             resol_f=0;
             thick_eff=BasicProcessedData(1).ChannelData.Transducer.effectivePulselLength*rawChannel(1).EnvironmentData.soundSpeed/2;
                 
         else % if TransceiverType = WBT
             
            % fichier de calibration 
            itr_cal=0;
            for tr=1:size(Transducer,2)
                if strcmp(Transducer{tr}.Attributes.TransducerName , RawData(fileNumber).Data.ConfigurationData.Transceivers(ichannel).Channels.Transducer.TransducerName) &...
                        strcmp(Transducer{tr}.Attributes.TransducerSerialNumber , RawData(fileNumber).Data.ConfigurationData.Transceivers(ichannel).Channels.Transducer.SerialNumber)
                    itr_cal=tr;
                    break;
                end
            end
            if itr_cal>0
                nfr=length(Transducer{itr_cal}.FrequencyPar);
                freq_cal=zeros(1,nfr);
                gain_cal=zeros(1,nfr);
                for frq = 1:nfr  
                    freq_cal(frq)=str2double(Transducer{itr_cal}.FrequencyPar{frq}.Attributes.Frequency);
                    gain_cal(frq)=str2double(Transducer{itr_cal}.FrequencyPar{frq}.Attributes.Gain);
                end
            end

            % modification de la calibration pour int�grer d'autres erreurs
            % (ici ramping diff�rent acquisition/calibration)
            if (erreur_ramping==1)
                ip0=1;
                while isempty(RawData(fileNumber).Data.PingData(ichannel,ip0).time) 
                    ip0=ip0+1;
                    if ip0>size( RawData(fileNumber).Data.PingData,2) break;end
                end
%                  RawData(fileNumber).Data.PingData(ichannel,ip0).ParameterData.frequencyStart=str2num(RawData.Data.ConfigurationData.Transceivers(ichannel).Channels.Transducer.FrequencyMinimum);
%                 RawData(fileNumber).Data.PingData(ichannel,ip0).ParameterData.frequencyStop=str2num(RawData.Data.ConfigurationData.Transceivers(ichannel).Channels.Transducer.FrequencyMaximum);
%                 RawData(fileNumber).Data.PingData(ichannel,ip0).ParameterData.frequencyCenter=(RawData(fileNumber).Data.PingData(ichannel,ip0).ParameterData.frequencyStart+RawData(fileNumber).Data.PingData(ichannel,ip0).ParameterData.frequencyStop)/2;
                [transmitSignal] = CreateTransmitSignal(RawData(fileNumber).Data.PingData(ichannel,ip0)); % signal sondeur
                f1=RawData(fileNumber).Data.PingData(ichannel,ip0).ParameterData.frequencyStart;
                [transmitSignalCal] = CreateTransmitSignal(RawData(fileNumber).Data.PingData(ichannel,ip0),200/(2048e-6 * f1)); % signal calibr�
                
                %correction de calibration
                fe=1/RawData(fileNumber).Data.PingData(ichannel,ip0).ParameterData.sampleInterval;
                f2=RawData(fileNumber).Data.PingData(ichannel,ip0).ParameterData.frequencyStop;
                
                Nfftcor=length(transmitSignal);
                [spectrumTx,freq_Tx,fe_Tx,NfftTx]=TransmitSpectrum(RawData(fileNumber).Data.PingData(ichannel,ip0)); % signal sondeur
                [spectrumTxCal,freq_Tx,fe_Tx,NfftTx]=TransmitSpectrum(RawData(fileNumber).Data.PingData(ichannel,ip0),200/(2048e-6 * f1)); % signal calibr�
                
                cor_spectrumdB_ini=20*log10(abs(spectrumTx))-20*log10(abs(spectrumTxCal));
                                
                % liste des corrections fr�quentielles � appliquer
                if exist('freq_cal') 
                    freq_cor=freq_cal;
                else %note: corriger le ramping n'a pas de sens actuellement s'il n'y a pas de fichier de calibration associ�. Mais on garde le cas pour d'autres types �ventuels de correction
                    freq_cor=(floor(f1-df):df/2:ceil(f2+df));
                end
                clear cor_spectrumdB;
                for ii=1:length(freq_cor)
                    indm=find(freq_Tx>=freq_cor(ii)-df/4 & freq_Tx<freq_cor(ii)+df/4);
                    cor_spectrumdB(ii)=10*log10(mean(10.^(cor_spectrumdB_ini(indm)/10)));
                end
                 
                if exist('gain_cal')
                    gain_cor=gain_cal+cor_spectrumdB/2;
                else %gain par d�faut
                    gainTable       = str2num(RawData(fileNumber).Data.ConfigurationData.Transceivers(ichannel).Channels.Transducer.Gain);
                    gain_cor = gainTable(end)+20*log10(freq_cor/str2num(RawData(fileNumber).Data.ConfigurationData.Transceivers(ichannel).Channels.Transducer.Frequency));
                end
                
            end
            
            % on compl�te les tailles des matrices d'�chantillons quand un
            % seul channel auto range
            if nchannel==1
                ns=size(RawData.Data.PingData(ichannel,1).SampleData.complexSamples,1);
                for ip=1:size(RawData.Data.PingData,2)
                    if size(RawData.Data.PingData(ichannel,ip).SampleData.complexSamples,1)>ns
                        RawData.Data.PingData(ichannel,ip).SampleData.complexSamples=RawData.Data.PingData(ichannel,ip).SampleData.complexSamples(1:ns,4);
                    elseif size(RawData.Data.PingData(ichannel,ip).SampleData.complexSamples,1)<ns
                        RawData.Data.PingData(ichannel,ip).SampleData.complexSamples=cat(1,RawData.Data.PingData(ichannel,ip).SampleData.complexSamples,zeros(ns-size(RawData.Data.PingData(ichannel,ip).SampleData.complexSamples,1),4));                     
                    end
                end
            end
            
            % calcul des spectres (avec ou sans info de calibration)
            if compute_from_MF==1
                [Sig_MF,filt]=EK80_SigMF(RawData(fileNumber),ichannel); %calcul du signal compress�
                if (erreur_ramping==1)
                    [sv_spectrum,freq,r_spectrum,Time_channel,thick_eff,df_eff,resol_f]=EK80_SvSpectrumMF(RawData(fileNumber),ichannel,Sig_MF,filt,df,freq_cor,gain_cor);%le gain �tant appliqu� deux fois (� r�fl�chir)
                elseif itr_cal>0
                     figure;plot(freq_cal/1000,gain_cal);xlabel('kHz');ylabel('dB');title(['gain d �talonnage channel ' num2str(ichannel)]);
                     [sv_spectrum,freq,r_spectrum,Time_channel,thick_eff,df_eff,resol_f]=EK80_SvSpectrumMF(RawData(fileNumber),ichannel,Sig_MF,filt,df,freq_cal,gain_cal);
                else
                    [sv_spectrum,freq,r_spectrum,Time_channel,thick_eff,df_eff,resol_f]=EK80_SvSpectrumMF(RawData(fileNumber),ichannel,Sig_MF,filt,df);
                end
            else
                if (erreur_ramping==1)
                    [sv_spectrum,freq,r_spectrum,Time_channel,thick_eff,df_eff,resol_f]=EK80_SvSpectrum(RawData(fileNumber),ichannel,df,freq_cor,gain_cor);%le gain �tant appliqu� deux fois (� r�fl�chir)
                elseif itr_cal>0
                    [sv_spectrum,freq,r_spectrum,Time_channel,thick_eff,df_eff,resol_f]=EK80_SvSpectrum(RawData(fileNumber),ichannel,df,freq_cal,gain_cal);
                else
                    [sv_spectrum,freq,r_spectrum,Time_channel,thick_eff,df_eff,resol_f]=EK80_SvSpectrum(RawData(fileNumber),ichannel,df);
                end
            end

            % s�lection de la bande de fr�quence
            idborne = find(freq/1000 > frequencies_begin(ichannel) & freq/1000 < frequencies_end(ichannel));
            sv_spectrum_simple = sv_spectrum(:,:,idborne);
            freq_simple = freq(idborne)/1000;
            range_spectrum=repmat(r_spectrum,size(sv_spectrum,1),1);
            
            
%             % int�gration par couche (� reprendre, vaut mieux le faire dans la routine d'appel?)
%             sv_spectrum_simple = zeros(length(sv_spectrum_tmp(:,1,1)),length(integration_depth) , length(sv_spectrum_tmp(1,1,:)));
%             for z = 1:np
%                 for y = 1:length(freq_simple)
%                     for i = 1:length(integration_depth)
%                     iddepth = find(r_spectrum >= integration_depth(i) & r_spectrum <= integration_depth(i) + thick); % range  + depth echo-sounder 
%                     sv_spectrum_simple(z,i,y) = 10*log10(mean(10.^((sv_spectrum_tmp(z,iddepth,y))/10)));
%                     end    
%                 end
%             end          % simplifier la boucle?

         end

         freq_simple_temp{mod(fileNumber-1,step)+1,ichannel} = freq_simple;
         Sv_temp{mod(fileNumber-1,step)+1,ichannel} = sv_spectrum_simple; % Sv par ping, depth,frequences
         Time_temp{mod(fileNumber-1,step)+1,ichannel} = Time_channel;
         range_Sv_temp{mod(fileNumber-1,step)+1,ichannel} = range_spectrum;
         resol_f_temp{mod(fileNumber-1,step)+1,ichannel} = resol_f;
         thick_eff_temp{mod(fileNumber-1,step)+1,ichannel} = thick_eff;
         

    end %nchannel
    
     if mod(fileNumber,step)==0 | fileNumber==nFiles
        save([dirsave,'SvSpectrum_' int2str(ceil(fileNumber/step))],'freq_simple_temp', 'Sv_temp', 'Time_temp', 'range_Sv_temp','resol_f_temp', 'thick_eff_temp');
        clear freq_simple_temp Sv_temp Time_temp range_Sv_temp resol_f_temp thick_eff_temp
     end
     
    disp(['Finished extract Sv from file ' int2str(fileNumber) ' of ' int2str(nFiles)]);



end %nFiles

for filestep = 1:ceil(nFiles/step)
    load([dirsave,'SvSpectrum_' int2str(filestep)]);
    nstep=size(freq_simple_temp,1);
    freq_simple_channel(1+step*(filestep-1):nstep+step*(filestep-1),:) = freq_simple_temp;
    Sv(1+step*(filestep-1):nstep+step*(filestep-1),:) = Sv_temp; % Sv par ping, depth,frequences
    Time(1+step*(filestep-1):nstep+step*(filestep-1),:) = Time_temp;
    range_Sv(1+step*(filestep-1):nstep+step*(filestep-1),:) = range_Sv_temp;
    resol_f_Sv(1+step*(filestep-1):nstep+step*(filestep-1),:) = resol_f_temp;
    thick_eff_Sv(1+step*(filestep-1):nstep+step*(filestep-1),:) = thick_eff_temp;
end

delete([dirsave,'*.*']);
rmdir(dirsave);

toc
