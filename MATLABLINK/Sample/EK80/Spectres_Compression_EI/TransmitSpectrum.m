function [spectrum,freq,fe,Nfft,normFilt]=TransmitSpectrum(PingDataSingle,varargin)

% create spectrum of transmit signal, with correct frequency indexation
% fft length is the length of the signal created by CreateTransmitSignal.m
% Inputs:
%   PingData du transducetur/ping considéré
%   slope (pourcent) optionnelle

if nargin>1 %nlb pour modification éventuelle du ramping
    slope=varargin{1}/100;
    [transmitSignal] = CreateTransmitSignal(PingDataSingle,slope);
else
    [transmitSignal] = CreateTransmitSignal(PingDataSingle);
end

normFilt=transmitSignal/norm(transmitSignal)^2;

f1=PingDataSingle.ParameterData.frequencyStart;
f2=PingDataSingle.ParameterData.frequencyStop;
fe=1/PingDataSingle.ParameterData.sampleInterval;

Nfft=length(transmitSignal);
spectrum=fft(transmitSignal,Nfft);

%index des fréquences
f2per=rem(f2,fe);
f1per=rem(f1,fe);
if f1per<f2per
    nper=floor(f1/fe);
    freq=(0:Nfft-1)/Nfft*fe+nper*fe;
else
    freqper=(0:Nfft-1)/Nfft*fe;
    ind_out=find(freqper>f2per & freqper<f1per);
    if ~isempty(freqper)
        iout0=ind_out(ceil(length(ind_out)/2));
    else
        iout0=max(freqper<f1per)+1;
    end
    nper=floor(f1/fe);
    freq=[freqper(1:iout0-1)+(nper+1)*fe freqper(iout0:end)+nper*fe];
end

%réordonnancement des fréquences
[freq,indordf]=sort(freq);
spectrum=spectrum(indordf);