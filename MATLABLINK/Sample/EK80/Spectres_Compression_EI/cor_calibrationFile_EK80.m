function cor_calibrationFile_EK80(ichannel)

% routine corrigeant le fichier TrList_calibration.xml initial pour le
% transducteur indiqu� en entr�e (rapport au fichier raw de calibration)
% rensigner les chemins/noms de fichiers TrList_calibration.xml et
% raw d'�talonnage, ainsi que les param�tres de distance utilis�s 

% calibration raw data file (to have pulse, sampling parameters)
%---------------------------------------------------------------
fileName='J:\PELGAS16\EK80\raw\Calibration_01062016\PELGAS16-D20160601-T085308.raw';
%fileName='J:\PELGAS17\calibration\EK80\FM 22mm\PELGAS17-D20170422-T112950.raw';

%TrList_calibration.xml file to modify
%-------------------------------------
%TrList_calibrationFile='C:\ProgramData\Simrad\EK80\ConfigSettings\TrList_calibration.xml';
TrList_calibrationFile='J:\PELGAS16\EK80\Calibration\CalibrationResults\TrList_calibration.xml';
name_sav_ini=[TrList_calibrationFile(1:end-4) '_ini.xml'];

% param�tres utilis�s en �talonnage
%----------------------------------
dist_before=0.15; %m
dist_after=0.7; %m

% read raw data
disp('debut lecture raw')
RawData.Data = ReadEK80RawFile(fileName);
disp('fin lecture raw')

%% read calibration

% if exist(name_sav_ini,'file')
%     [TrList_calibration, RootName]=xml_read(name_sav_ini);
% else
%     [TrList_calibration, RootName]=xml_read(TrList_calibrationFile);
% end
[TrList_calibration, RootName]=xml_read(TrList_calibrationFile);

Transducer = TrList_calibration.TransducerData.Transducer;

itr_cal=0;
for tr=1:size(Transducer,1)
    if strcmp(Transducer(tr).ATTRIBUTE.TransducerName , RawData.Data.ConfigurationData.Transceivers(ichannel).Channels.Transducer.TransducerName) &...
            strcmp(num2str(Transducer(tr).ATTRIBUTE.TransducerSerialNumber) , RawData.Data.ConfigurationData.Transceivers(ichannel).Channels.Transducer.SerialNumber)
        itr_cal=tr;
        break;
    end
end

if itr_cal>0
    nfr=length(Transducer(itr_cal).FrequencyPar);
    freq_cal=zeros(1,nfr);
    gain_cal=zeros(1,nfr);
    for frq = 1:nfr  
        freq_cal(frq)=Transducer(itr_cal).FrequencyPar(frq).ATTRIBUTE.Frequency;
        gain_cal(frq)=Transducer(itr_cal).FrequencyPar(frq).ATTRIBUTE.Gain;
    end
else
    disp(['pas d info d etalonnage pour le ' RawData.Data.ConfigurationData.Transceivers(ichannel).Channels.Transducer.TransducerName ' n�' RawData.Data.ConfigurationData.Transceivers(ichannel).Channels.Transducer.SerialNumber]);
    return
end

%% calcul de la correction d'�talonnage

%[cor_cal,freq_cor_cal]=cor_calibration_EK80(RawData,ichannel);
[cor_cal,freq_cor_cal]=cor_calibration_EK80_distBeforeAfter(RawData,ichannel,dist_before,dist_after);

cor=interp1(freq_cor_cal,cor_cal,freq_cal);
cor=round(cor*100)/100;
gain_cor=gain_cal+cor;


figure;plot(freq_cal/1000,gain_cal);
hold on;plot(freq_cal/1000,gain_cor);
xlabel('kHz');ylabel('dB');
legend('gain du fichier d etalonnage','gain corrige du traitement simrad');
title([Transducer(tr).ATTRIBUTE.TransducerName ' n�' num2str(Transducer(tr).ATTRIBUTE.TransducerSerialNumber)])

%lissage et interpolation (pour les stop band)
if (1) 
    df_filt=2; %largeur de filtre en kHz
    df_fic=median(freq_cal(2:end)-freq_cal(1:end-1))/2;
    nfilt=2*round(df_filt*1000/df_fic/2);
    
    freq_interp=(freq_cal(1):df_fic:freq_cal(end));
    if freq_interp(end)<freq_cal(end) freq_interp(end+1)=freq_cal(end); end
    gain_cor_interp=interp1(freq_cal,gain_cor,freq_interp); %interpole dans les stop bands
    
    gain_cor_filt=[ones(1,nfilt/2)*gain_cor_interp(1),gain_cor_interp,ones(1,nfilt/2)*gain_cor_interp(end)];
    gain_cor_filt=filter(ones(1,nfilt+1)/(nfilt+1),1,gain_cor_filt);
    gain_cor_filt=gain_cor_filt(nfilt+1:end);
    gain_cor_filt=interp1(freq_interp,gain_cor_filt,freq_cal);
    plot(freq_cal/1000,gain_cor_filt);
    legend('gain du fichier d etalonnage','gain corrig� du traitement simrad',['gain corrig� liss�, df=' num2str(df_filt) 'kHz']);
end


%% enregistrement du xml

for frq = 1:nfr  
    Transducer(itr_cal).FrequencyPar(frq).ATTRIBUTE.Gain=gain_cor(frq);
end
TrList_calibration.TransducerData.Transducer= Transducer;
Pref.StructItem=false;
Pref.CellItem=false;

if ~exist(name_sav_ini,'file')
    movefile(TrList_calibrationFile,name_sav_ini);
    disp(['fichier initial sous ' name_sav_ini]);
end
xml_write(TrList_calibrationFile,TrList_calibration,RootName,Pref);

%reproduce correct xml two first lines
fid=fopen(name_sav_ini);
u=fgetl(fid);
v=fgetl(fid);
fclose(fid);

copyfile(TrList_calibrationFile,[TrList_calibrationFile(1:end-4) '_temp.xml']);
fid=fopen(TrList_calibrationFile,'w');
fid2=fopen([TrList_calibrationFile(1:end-4) '_temp.xml'],'r');
%reproduce correct xml format two first lines
fprintf(fid, '%s\n', u);
fprintf(fid, '%s\n', v);
w=fgetl(fid2);
w=fgetl(fid2);
w=fgetl(fid2);
while w~=-1
    fprintf(fid, '%s\n', w);
    w=fgetl(fid2);
end
fclose(fid);
fclose(fid2);

delete([TrList_calibrationFile(1:end-4) '_temp.xml']);

disp(['fichier ' TrList_calibrationFile ' actualis�']);