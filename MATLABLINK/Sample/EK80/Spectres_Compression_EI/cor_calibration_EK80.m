function [cor_cal,freq_cor_cal]=cor_calibration_EK80(RawData,ich)

%[RawData] = ReadEK80Data;
[transmitSignal] = CreateTransmitSignal(RawData(1).Data.PingData(ich,ich));
filt=transmitSignal/norm(transmitSignal)^2;

df=250;

%% paramètres de traitement 
f1=RawData.Data.PingData(ich,ich).ParameterData.frequencyStart;
f2=RawData.Data.PingData(ich,ich).ParameterData.frequencyStop;
T=RawData.Data.PingData(ich,ich).ParameterData.pulseLength;
B=f2-f1;
fe=1/RawData.Data.PingData(ich,ich).ParameterData.sampleInterval;
transmitPower=RawData.Data.PingData(ich,ich).ParameterData.transmitPower; %W

if df>B/2
    disp('        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
    df=round(B/2);
    disp(['        Too large frequency resolution requested. Reduced to:' num2str(df) 'Hz']);
    disp('        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
end
if df<1/(2*T)
    disp('        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
    df=round(1/(2*T));
    disp(['        Too small frequency resolution requested. Increased to:' num2str(df) 'Hz']);
    disp('        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
end

% taille de fft
Ndf=2*round(min(fe/df,2*T*fe)/2); 
%Ndf=2*round(min(fe/df*1.25,2*T*fe)/2); % avec hanning sur 2*0.2

Nfft=2^(ceil(log2(Ndf)));
Tfft=Nfft/fe;
resol_f=fe/Ndf;
df_eff=fe/Nfft;
shift=max(1,floor(Ndf/4));

nShapingSamples = floor(0.4/2*Ndf);
windowFunction  = ((1-cos(pi*(0:2*nShapingSamples-1).'/nShapingSamples))/2).^2;
shapingWindow   = [windowFunction(1:nShapingSamples); ones(Ndf-2*nShapingSamples,1); windowFunction(nShapingSamples+1:end)];
shapingWindow   = shapingWindow*0+1;

%% paramètres utilisés par Simrad pour le spectre transmis
dfSmooth=2500;

if dfSmooth>B/2
    disp('        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
    dfSmooth=round(B/2);
    disp(['        Too large frequency resolution requested. Reduced to:' num2str(dfSmooth) 'Hz']);
    disp('        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
end
if dfSmooth<1/(2*T)
    disp('        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
    dfSmooth=round(1/(2*T));
    disp(['        Too small frequency resolution requested. Increased to:' num2str(dfSmooth) 'Hz']);
    disp('        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
end

% taille de fft
NdfSmooth=2*round(min(fe/dfSmooth,2*T*fe)/2);
%NdfSmooth=2*round(min(fe/dfSmooth*1.25,2*T*fe)/2) % avec hanning sur 2*0.2


%% spectre du filtre, avec le bon df
MFfilt=xcorr(filt,filt); %filtre MF

imed=ceil(length(MFfilt)/2);
%fft_filtOK=fft(MFfilt(imed-floor((Ndf-1)/2):imed+floor(Ndf/2)).*shapingWindow,Nfft);
fft_filtOK=fft(MFfilt(imed-floor(0.15/750*fe):imed+floor(0.7/750*fe)),Nfft);
fft_filtOK=sqrt(abs(fft_filtOK));

%% spectre lissé (avec 0-padding)
nShapingSamples = floor(0.4/2*NdfSmooth);
windowFunction  = ((1-cos(pi*(0:2*nShapingSamples-1).'/nShapingSamples))/2).^2;
shapingWindow   = [windowFunction(1:nShapingSamples); ones(NdfSmooth-2*nShapingSamples,1); windowFunction(nShapingSamples+1:end)];
shapingWindow   = shapingWindow*0+1;

fft_filtSmooth=fft(MFfilt(imed-floor((NdfSmooth-1)/2):imed+floor(NdfSmooth/2)).*shapingWindow,Nfft);
fft_filtSmooth=sqrt(abs(fft_filtSmooth));


   
%% index des fréquences
f2per=rem(f2,fe);
f1per=rem(f1,fe);
if f1per<f2per
    nper=floor(f1/fe);
    freq=(0:Nfft-1)/Nfft*fe+nper*fe;
else
    freqper=(0:Nfft-1)/Nfft*fe;
    ind_out=find(freqper>f2per & freqper<f1per);
    if ~isempty(freqper)
        iout0=ind_out(ceil(length(ind_out)/2));
    else
        iout0=max(freqper<f1per)+1;
    end
    nper=floor(f1/fe);
    freq=[freqper(1:iout0-1)+(nper+1)*fe freqper(iout0:end)+nper*fe];
end

%réordonnancement des fréquences
[freq_cor_cal,indordf]=sort(freq);

%%

cor_cal= 20*log10(squeeze(fft_filtSmooth(indordf)))-20*log10(squeeze(fft_filtOK(indordf)));  


figure(1);hold on;plot(freq_cor_cal/1000,20*log10(squeeze(fft_filtSmooth(indordf))),'-*');xlim([f1 f2]/1000)
plot(freq_cor_cal/1000,20*log10(squeeze(fft_filtOK(indordf))),'-*');xlim([f1 f2]/1000)

figure(2);hold on;plot(freq_cor_cal/1000,cor_cal,'-*');xlim([f1 f2]/1000)
