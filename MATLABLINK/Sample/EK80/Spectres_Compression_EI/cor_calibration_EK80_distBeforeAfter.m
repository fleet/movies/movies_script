function [cor_cal,freq_cor_cal]=cor_calibration_EK80_distBeforeAfter(RawData,ichannel,dist_before,dist_after)

%[RawData] = ReadEK80Data;
%dist_before=0.15; %m
%dist_after=0.7; %m

[transmitSignal] = CreateTransmitSignal(RawData(1).Data.PingData(ichannel,ichannel));
filt=transmitSignal/norm(transmitSignal)^2;

%% paramètres de traitement 
f1=RawData.Data.PingData(ichannel,ichannel).ParameterData.frequencyStart;
f2=RawData.Data.PingData(ichannel,ichannel).ParameterData.frequencyStop;
T=RawData.Data.PingData(ichannel,ichannel).ParameterData.pulseLength;
B=f2-f1;
fe=1/RawData.Data.PingData(ichannel,ichannel).ParameterData.sampleInterval;
c=RawData.Data.PingData(ichannel,ichannel).EnvironmentData.soundSpeed;

% taille de fft
Ndf=ceil(min(2*2*(dist_before+dist_after)/c*fe,2*T*fe)); %facteur 2 pour mieux échantillonner

Nfft=2^(ceil(log2(Ndf)));
Tfft=Nfft/fe;

%% paramètres utilisés par Simrad pour le spectre transmis

% ~taille de fft
NdfSmooth=round(min(2*(0.15+0.15)/c*fe,T*fe));

%% spectre du filtre, avec le bon df
MFfilt=xcorr(filt,filt); %filtre MF

imed=ceil(length(MFfilt)/2);
fft_filtOK=fft(MFfilt(imed-floor(2*dist_before/c*fe):imed+floor(2*dist_after/c*fe)),Nfft);
fft_filtOK=sqrt(abs(fft_filtOK));

%% spectre lissé (avec 0-padding)
fft_filtSmooth=fft(MFfilt(imed-floor(2*0.15/c*fe):imed+floor(2*0.15/c*fe)),Nfft);
fft_filtSmooth=sqrt(abs(fft_filtSmooth));


   
%% index des fréquences
f2per=rem(f2,fe);
f1per=rem(f1,fe);
if f1per<f2per
    nper=floor(f1/fe);
    freq=(0:Nfft-1)/Nfft*fe+nper*fe;
else
    freqper=(0:Nfft-1)/Nfft*fe;
    ind_out=find(freqper>f2per & freqper<f1per);
    if ~isempty(freqper)
        iout0=ind_out(ceil(length(ind_out)/2));
    else
        iout0=max(freqper<f1per)+1;
    end
    nper=floor(f1/fe);
    freq=[freqper(1:iout0-1)+(nper+1)*fe freqper(iout0:end)+nper*fe];
end

%réordonnancement des fréquences
[freq_cor_cal,indordf]=sort(freq);

%%

cor_cal= 20*log10(squeeze(fft_filtSmooth(indordf)))-20*log10(squeeze(fft_filtOK(indordf)));  


figure(1);hold on;plot(freq_cor_cal/1000,20*log10(squeeze(fft_filtSmooth(indordf))),'-*');xlim([f1 f2]/1000)
plot(freq_cor_cal/1000,20*log10(squeeze(fft_filtOK(indordf))),'-*');xlim([f1 f2]/1000)

figure(2);hold on;plot(freq_cor_cal/1000,cor_cal,'-*');xlim([f1 f2]/1000)
