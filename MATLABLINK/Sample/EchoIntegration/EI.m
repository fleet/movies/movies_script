%% Lance l'�cho-int�grationt de tous les fichiers contenus dans un repertoire donne 

    clear all;
    
    %chargement config
    chemin_config = 'C:\data\PELGAS10\';
    chemin_config_reverse = strrep(chemin_config, '\', '/');
    moLoadConfig(chemin_config_reverse);

    %activation EI
    EchoIntegrModule = moEchoIntegration();
    moSetEchoIntegrationEnabled(EchoIntegrModule);
    
for m=[3:3]
    
    if m<10
        RUN=strcat('RUN00',num2str(m))
    else
        RUN=strcat('RUN0',num2str(m))
    end;
    
    chemin_hac=['C:\data\PELGAS10\',RUN,'\'];
    chemin_save=['C:\data\PELGAS10\lay\',RUN,'\'];
    if ~exist(chemin_save,'dir')
            mkdir(chemin_save)
    end

    SampleEchoIntegration(chemin_hac,chemin_save,chemin_config,EchoIntegrModule);


    
end;
