
clear all;

% en local
chemin='C:/Users/mdoray.IFR/Documents/Data/HAC/CLASStest/';

% ou sur meskl
chemin='M:/MDoray/CLASStest/';

thr=[-80:1:-50 ];
thr=-80

runs=[35]; 
 
    for ir=1:length(runs)        
        for f=1:size(thr,2)
           %cr�e les strings adapt�s
           %str_run(ir,:)='000';
           %str_run(ir,end-length(num2str(runs(ir)))+1:end)=num2str(runs(ir));
           %chemin_config=[chemin,'/Config/EI Lay','/',num2str(thr(:,f)),'/'];
           chemin_config=[chemin,'EIlay-80dB-PELGAS-smallESU/'];
           %chemin_hac=[chemin,'/RUN' str_run(ir,:) '/'];
           chemin_hac=[chemin];
           %chemin_save=[chemin,'/Result/EI Lay/','/RUN' str_run(ir,:) '/',num2str(thr(:,f)),'/'];
           chemin_save=[chemin,'Result/EILay7/'];

           if ~exist(chemin_save,'dir')
                mkdir(chemin_save)
            end
          %load configuration
            chemin_config_reverse = strrep(chemin_config, '\', '/');
            moLoadConfig(chemin_config_reverse);
          %To check kernel parameters: moKernelParameter()
            %Activate 'EchoIntegrModule'
            EchoIntegrModule = moEchoIntegration();
            moSetEchoIntegrationEnabled(EchoIntegrModule);

            %Run EI
            SampleEchoIntegration(chemin_hac,chemin_save,chemin_config,EchoIntegrModule);
            %SampleEchoIntegration2(chemin_hac,chemin_save,chemin_config);
            moSetEchoIntegrationDisabled(EchoIntegrModule);
            clear EchoIntegrModule;
        end
    end;
    
    
    
    