%LB 02/04/2013 ajout récupération sondeur horizontal

%Clean workspace
clear all;
close all;

%% Set paths
path_hac_survey='C:\Users\lberger\Desktop\PELGAS19\'
path_config = [path_hac_survey,'/Config_M3D/EI_lay']


%% Echo-integration
thresholds=[-60];

datestart='04/04/2019';
dateend='04/07/2019';
heurestart='04:00:00';
heureend='06:00:00';
survey_name='PELGAS19';
runs=[ 16]; 



path_save=[path_hac_survey,'/Result/',survey_name]
%If save path does not exist, create it
if ~exist(path_save,'dir')
    mkdir(path_save)
end

 batch_EI_multiTHR(path_hac_survey,path_config,path_save,thresholds,runs,'',datestart,heurestart,dateend,heureend);
%     datestart=datenum(2010,1,18,23,50,0);
%     dateend=datenum(2010,1,19,00,45,00);


%% Bind EIlay results matrices

for ir=1:length(runs)
    for t=1:size(thresholds,2)
        str_run(ir,:)='000';        str_run(ir,end-length(num2str(runs(ir)))+1:end)=num2str(runs(ir));
        filename_ME70=[path_save,'/',survey_name,'-RUN' str_run(ir,:),'-TH' num2str(thresholds(t)),'-ME70-EIlay.mat'];
        filename_EK80=[path_save,'/',survey_name,'-RUN' str_run(ir,:),'-TH' num2str(thresholds(t)),'-EK80-EIlay.mat'];
        filename_EK80h=[path_save,'/',survey_name,'-RUN' str_run(ir,:),'-TH' num2str(thresholds(t)),'-EK80h-EIlay.mat'];
        path_results=[path_save,'/RUN' str_run(ir,:) '/',num2str(thresholds(:,t)),'/'];
    EIlayRes_bind(path_results,filename_ME70,filename_EK80,filename_EK80h,datestart,heurestart,dateend,heureend);
    end
end

%% Load bound EIlay results
str_run='000'; str_run(end-length(num2str(runs))+1:end)=num2str(runs);
filename_ME70=[path_save,'/',survey_name,'-RUN' str_run,'-TH' num2str(thresholds),'-ME70-EIlay.mat'];
filename_EK80=[path_save,'/',survey_name,'-RUN' str_run,'-TH' num2str(thresholds),'-EK80-EIlay.mat'];
filename_EK80h=[path_save,'/',survey_name,'-RUN' str_run,'-TH' num2str(thresholds),'-EK80h-EIlay.mat'];
load(filename_EK80);
% load(filename_EK80h);
load(filename_ME70);

%% Exploratory plots
%choice for frequency
freqME70=11;
freqEK80=3;

figure;
imagesc(depth_surface_EK80_db{freqEK80},Sv_surfEK80_db{freqEK80}');
