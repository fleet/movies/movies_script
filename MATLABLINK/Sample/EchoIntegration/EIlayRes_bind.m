function EIlayRes_bind(fpath,filename_ME70,filename_EK80,filename_EK80h,datestart,heurestart,dateend,heureend)

%% Load EI lay results stored in 'fpath' and bind them into arrays

filelist = squeeze(dir(fullfile(fpath,'*.mat')));  % all files
nb_files = size(filelist,1);  % number of files

depth_bottom_ME70_db=[];
depth_surface_ME70_db=[];
lat_surfME70_db=[];
lon_surfME70_db=[];
lat_botME70_db=[];
lon_botME70_db=[];
Sa_surfME70_db = [];
Sa_botME70_db = [];
Sv_surfME70_db = [];
Sv_botME70_db = [];
time_ME70_db = [];
vol_surfME70_db = [];

depth_bottom_EK80_db=[];
depth_surface_EK80_db=[];
lat_surfEK80_db=[];
lon_surfEK80_db=[];
lat_botEK80_db=[];
lon_botEK80_db=[];
Sa_surfEK80_db = [];
Sa_botEK80_db = [];
% Sv_surfEK80_db = [];
Sv_botEK80_db = [];
time_EK80_db = [];
vol_surfEK80_db = [];

depth_surface_EK80h_db=[];
lat_surfEK80h_db=[];
lon_surfEK80h_db=[];
Sa_surfEK80h_db = [];
Sv_surfEK80h_db = [];
time_EK80h_db = [];
vol_surfEK80h_db = [];

frequencies_ME70_db=[];
frequencies_EK80_db=[];
frequencies_EK80h_db=[];

nblME70=0;
nblEK80=0;
nblEK80h=0;

datestart=datenum([datestart,' ',heurestart],'dd/mm/yyyy HH:MM:SS');
dateend=datenum([dateend,' ',heureend],'dd/mm/yyyy HH:MM:SS');

for numfile = 1:nb_files
    
    hacfilename = filelist(numfile,:).name;
    FileName = [fpath,hacfilename];
    load(FileName)
    
    if (((length(time_ME70))>0))
        %         if (nblEK80==0)
        timeME70=time_ME70/86400 +719529;
        if (timeME70(1)>=datestart && timeME70(end)<=dateend)
             time_ME70_db(nblME70+1:nblME70+length(time_ME70)) = timeME70;
                for ibeam=1:length(Sv_surfME70)
            %cas une seule fr�quence un seul ESU attention il faut inverser pour avoir toujours esu,layer,freq !!!
         if((size(Depth_botME70{ibeam},1)==1 && size(Depth_botME70{ibeam},3)==1) || (size(Depth_surfME70{ibeam},1)==1 && size(Depth_surfME70{ibeam},3)==1))
                    frequencies_ME70_db{ibeam}(nblME70+1:nblME70+size(frequenciesME70{ibeam},1),1) = frequenciesME70{ibeam}';
                    depth_bottom_ME70_db{ibeam}(nblME70+1:nblME70+size(Depth_botME70{ibeam},1),:,:) = Depth_botME70{ibeam}';
                    depth_surface_ME70_db{ibeam}(nblME70+1:nblME70+size(Depth_surfME70{ibeam},1),:,:) = Depth_surfME70{ibeam}';
%                     Sa_surfME70_db{ibeam}(nblME70+1:nblME70+size(Sa_surfME70{ibeam},1),:,:) = Sa_surfME70{ibeam}';
%                     Sa_botME70_db{ibeam}(nblME70+1:nblME70+size(Sa_botME70{ibeam},1),:,:) = Sa_botME70{ibeam}';
                    Sv_surfME70_db{ibeam}(nblME70+1:nblME70+size(Sv_surfME70{ibeam},1),:,:) = Sv_surfME70{ibeam}';
                    Sv_botME70_db{ibeam}(nblME70+1:nblME70+size(Sv_botME70{ibeam},1),:,:) = Sv_botME70{ibeam}';
                    lat_surfME70_db{ibeam}(nblME70+1:nblME70+size(Lat_surfME70{ibeam},1),:,:) = Lat_surfME70{ibeam}';
                    lat_botME70_db{ibeam}(nblME70+1:nblME70+size(Lat_botME70{ibeam},1),:,:) = Lat_botME70{ibeam}';
                    lon_surfME70_db{ibeam}(nblME70+1:nblME70+size(Long_surfME70{ibeam},1),:,:) = Long_surfME70{ibeam}';
                    lon_botME70_db{ibeam}(nblME70+1:nblME70+size(Long_botME70{ibeam},1),:,:) = Long_botME70{ibeam}';
                    vol_surfME70_db{ibeam}(nblME70+1:nblME70+size(Volume_surfME70{ibeam},1),:,:) = Volume_surfME70{ibeam}';
                    
                elseif (nblME70==0 || (nblME70>0 && (size(depth_bottom_ME70_db{ibeam},ndims(depth_bottom_ME70_db{ibeam}))==size(Depth_botME70{ibeam},ndims(Depth_botME70{ibeam})) && size(depth_surface_ME70_db{ibeam},ndims(depth_surface_ME70_db{ibeam}))==size(Depth_surfME70{ibeam},ndims(Depth_surfME70{ibeam})))))
                    frequencies_ME70_db{ibeam}(nblME70+1:nblME70+size(frequenciesME70{ibeam},1),:) = frequenciesME70{ibeam};
                    depth_bottom_ME70_db{ibeam}(nblME70+1:nblME70+size(Depth_botME70{ibeam},1),:,:) = Depth_botME70{ibeam};
                    depth_surface_ME70_db{ibeam}(nblME70+1:nblME70+size(Depth_surfME70{ibeam},1),:,:) = Depth_surfME70{ibeam};
%                     Sa_surfME70_db{ibeam}(nblME70+1:nblME70+size(Sa_surfME70{ibeam},1),:,:) = Sa_surfME70{ibeam};
%                     Sa_botME70_db{ibeam}(nblME70+1:nblME70+size(Sa_botME70{ibeam},1),:,:) = Sa_botME70{ibeam};
                    Sv_surfME70_db{ibeam}(nblME70+1:nblME70+size(Sv_surfME70{ibeam},1),:,:) = Sv_surfME70{ibeam};
                    Sv_botME70_db{ibeam}(nblME70+1:nblME70+size(Sv_botME70{ibeam},1),:,:) = Sv_botME70{ibeam};
                    lat_surfME70_db{ibeam}(nblME70+1:nblME70+size(Lat_surfME70{ibeam},1),:,:) = Lat_surfME70{ibeam};
                    lat_botME70_db{ibeam}(nblME70+1:nblME70+size(Lat_botME70{ibeam},1),:,:) = Lat_botME70{ibeam};
                    lon_surfME70_db{ibeam}(nblME70+1:nblME70+size(Long_surfME70{ibeam},1),:,:) = Long_surfME70{ibeam};
                    lon_botME70_db{ibeam}(nblME70+1:nblME70+size(Long_botME70{ibeam},1),:,:) = Long_botME70{ibeam};
                    vol_surfME70_db{ibeam}(nblME70+1:nblME70+size(Volume_surfME70{ibeam},1),:,:) = Volume_surfME70{ibeam};
         end
        end
        end
    end
    if (((length(time_EK80))>0))
        timeEK80=time_EK80/86400 +719529;
        if (timeEK80(1)>=datestart && timeEK80(end)<=dateend)
            time_EK80_db(nblEK80+1:nblEK80+length(time_EK80)) = timeEK80;
            for ibeam=1:length(Sv_surfEK80)
                            
                %cas une seule fr�quence un seul ESU attention il faut inverser pour avoir toujours esu,layer,freq !!!
                if((size(Depth_botEK80{ibeam},1)==1 && size(Depth_botEK80{ibeam},3)==1) || (size(Depth_surfEK80{ibeam},1)==1 && size(Depth_surfEK80{ibeam},3)==1))
                    frequencies_EK80_db{ibeam}(nblEK80+1:nblEK80+size(frequenciesEK80{ibeam},1),:) = frequenciesEK80{ibeam}';
                    depth_bottom_EK80_db{ibeam}(nblEK80+1:nblEK80+size(Depth_botEK80{ibeam},1),:,:) = Depth_botEK80{ibeam}';
                    depth_surface_EK80_db{ibeam}(nblEK80+1:nblEK80+size(Depth_surfEK80{ibeam},1),:,:) = Depth_surfEK80{ibeam}';
%                     Sa_surfEK80_db{ibeam}(nblEK80+1:nblEK80+size(Sa_surfEK80{ibeam},1),:,:) = Sa_surfEK80{ibeam}';
%                     Sa_botEK80_db{ibeam}(nblEK80+1:nblEK80+size(Sa_botEK80{ibeam},1),:,:) = Sa_botEK80{ibeam}';
                    Sv_surfEK80_db{ibeam}(nblEK80+1:nblEK80+size(Sv_surfEK80{ibeam},1),:,:) = Sv_surfEK80{ibeam}';
                    Sv_botEK80_db{ibeam}(nblEK80+1:nblEK80+size(Sv_botEK80{ibeam},1),:,:) = Sv_botEK80{ibeam}';
                    lat_surfEK80_db{ibeam}(nblEK80+1:nblEK80+size(Lat_surfEK80{ibeam},1),:,:) = Lat_surfEK80{ibeam}';
                    lat_botEK80_db{ibeam}(nblEK80+1:nblEK80+size(Lat_botEK80{ibeam},1),:,:) = Lat_botEK80{ibeam}';
                    lon_surfEK80_db{ibeam}(nblEK80+1:nblEK80+size(Long_surfEK80{ibeam},1),:,:) = Long_surfEK80{ibeam}';
                    lon_botEK80_db{ibeam}(nblEK80+1:nblEK80+size(Long_botEK80{ibeam},1),:,:) = Long_botEK80{ibeam}';
                    vol_surfEK80_db{ibeam}(nblEK80+1:nblEK80+size(Volume_surfEK80{ibeam},1),:,:) = Volume_surfEK80{ibeam}';
                    
                elseif (nblEK80==0 || (nblEK80>0 && (size(depth_bottom_EK80_db{ibeam},ndims(depth_bottom_EK80_db{ibeam}))==size(Depth_botEK80{ibeam},ndims(Depth_botEK80{ibeam})) && size(depth_surface_EK80_db{ibeam},ndims(depth_surface_EK80_db{ibeam}))==size(Depth_surfEK80{ibeam},ndims(Depth_surfEK80{ibeam})))))
                    frequencies_EK80_db{ibeam}(nblEK80+1:nblEK80+size(frequenciesEK80{ibeam},1),:) = frequenciesEK80{ibeam};
                    depth_bottom_EK80_db{ibeam}(nblEK80+1:nblEK80+size(Depth_botEK80{ibeam},1),:,:) = Depth_botEK80{ibeam};
                    depth_surface_EK80_db{ibeam}(nblEK80+1:nblEK80+size(Depth_surfEK80{ibeam},1),:,:) = Depth_surfEK80{ibeam};
%                     Sa_surfEK80_db{ibeam}(nblEK80+1:nblEK80+size(Sa_surfEK80{ibeam},1),:,:) = Sa_surfEK80{ibeam};
%                     Sa_botEK80_db{ibeam}(nblEK80+1:nblEK80+size(Sa_botEK80{ibeam},1),:,:) = Sa_botEK80{ibeam};
                    Sv_surfEK80_db{ibeam}(nblEK80+1:nblEK80+size(Sv_surfEK80{ibeam},1),:,:) = Sv_surfEK80{ibeam};
                    Sv_botEK80_db{ibeam}(nblEK80+1:nblEK80+size(Sv_botEK80{ibeam},1),:,:) = Sv_botEK80{ibeam};
                    lat_surfEK80_db{ibeam}(nblEK80+1:nblEK80+size(Lat_surfEK80{ibeam},1),:,:) = Lat_surfEK80{ibeam};
                    lat_botEK80_db{ibeam}(nblEK80+1:nblEK80+size(Lat_botEK80{ibeam},1),:,:) = Lat_botEK80{ibeam};
                    lon_surfEK80_db{ibeam}(nblEK80+1:nblEK80+size(Long_surfEK80{ibeam},1),:,:) = Long_surfEK80{ibeam};
                    lon_botEK80_db{ibeam}(nblEK80+1:nblEK80+size(Long_botEK80{ibeam},1),:,:) = Long_botEK80{ibeam};
                    vol_surfEK80_db{ibeam}(nblEK80+1:nblEK80+size(Volume_surfEK80{ibeam},1),:,:) = Volume_surfEK80{ibeam};
                end
                
            end
            
        end
    end
    if (exist('time_EK80h','var')>0)
        if (((length(time_EK80h))>0))
            timeEK80h=time_EK80h/86400 +719529;
            if (timeEK80h(1)>=datestart && timeEK80h(end)<=dateend)
                time_EK80h_db(nblEK80h+1:nblEK80h+length(time_EK80h)) = timeEK80h;
                for ibeam=1:length(Sv_surfEK80h)
                    if(size(Depth_surfEK80h,1)==1 && size(Depth_surfEK80h,3)==1)
                        frequencies_EK80h_db{ibeam}(nblEK80+1:nblEK80+size(frequenciesEK80h{ibeam},1),1) = frequenciesEK80h{ibeam}';
                        depth_surface_EK80h_db{ibeam}(nblEK80h+1:nblEK80h+size(Depth_surfEK80h{ibeam},1),:,:) = Depth_surfEK80h{ibeam}';
                        Sa_surfEK80h_db{ibeam}(nblEK80h+1:nblEK80h+size(Sa_surfEK80h{ibeam},1),:,:) = Sa_surfEK80h{ibeam}';
                        Sv_surfEK80h_db{ibeam}(nblEK80h+1:nblEK80h+size(Sv_surfEK80h{ibeam},1),:,:) = Sv_surfEK80h{ibeam}';
                        lat_surfEK80h_db{ibeam}(nblEK80h+1:nblEK80h+size(Lat_surfEK80h{ibeam},1),:,:) = Lat_surfEK80h{ibeam}';
                        lon_surfEK80h_db{ibeam}(nblEK80h+1:nblEK80h+size(Long_surfEK80h{ibeam},1),:,:) = Long_surfEK80h{ibeam}';
                        vol_surfEK80h_db{ibeam}(nblEK80h+1:nblEK80h+size(Volume_surfEK80h{ibeam},1),:,:) = Volume_surfEK80h{ibeam}';
                    elseif (nblEK80h==0 || (nblEK80h>0 && size(depth_surface_EK80h_db{ibeam},3)==size(Depth_surfEK80h{ibeam},ndims(Depth_surfEK80h{ibeam}))))
                        frequencies_EK80h_db{ibeam}(nblEK80+1:nblEK80+size(frequenciesEK80h{ibeam},1),:) = frequenciesEK80h{ibeam};
                        depth_surface_EK80h_db{ibeam}(nblEK80h+1:nblEK80h+size(Depth_surfEK80h{ibeam},1),:,:) = Depth_surfEK80h{ibeam};
                        Sa_surfEK80h_db{ibeam}(nblEK80h+1:nblEK80h+size(Sa_surfEK80h{ibeam},1),:,:) = Sa_surfEK80h{ibeam};
                        Sv_surfEK80h_db{ibeam}(nblEK80h+1:nblEK80h+size(Sv_surfEK80h{ibeam},1),:,:) = Sv_surfEK80h{ibeam};
                        lat_surfEK80h_db{ibeam}(nblEK80h+1:nblEK80h+size(Lat_surfEK80hv,1),:,:) = Lat_surfEK80h{ibeam};
                        lon_surfEK80h_db{ibeam}(nblEK80h+1:nblEK80h+size(Long_surfEK80h{ibeam},1),:,:) = Long_surfEK80h{ibeam};
                        vol_surfEK80h_db{ibeam}(nblEK80h+1:nblEK80h+size(Volume_surfEK80h{ibeam},1),:,:) = Volume_surfEK80h{ibeam};
                    end
                end
            end
        end
    end
    
    nblME70=length(time_ME70_db);
    nblEK80=length(time_EK80_db);
    nblEK80h=length(time_EK80h_db);
    
    
end;

save(filename_ME70,'time_ME70_db','depth_surface_ME70_db','depth_bottom_ME70_db','Sv_surfME70_db','Sv_botME70_db','Sa_surfME70_db','Sa_botME70_db','lat_surfME70_db','lon_surfME70_db','lat_botME70_db','lon_botME70_db','vol_surfME70_db');
save(filename_EK80,'time_EK80_db','depth_surface_EK80_db','depth_bottom_EK80_db','Sv_surfEK80_db','Sv_botEK80_db','Sa_surfEK80_db','Sa_botEK80_db','lat_surfEK80_db','lon_surfEK80_db','lat_botEK80_db','lon_botEK80_db','vol_surfEK80_db','frequencies_EK80_db','frequencies_EK80h_db','frequencies_ME70_db');
save(filename_EK80h,'time_EK80h_db','depth_surface_EK80h_db','Sv_surfEK80h_db','Sa_surfEK80h_db','lat_surfEK80h_db','lon_surfEK80h_db','vol_surfEK80h_db');
end

