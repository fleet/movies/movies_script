function EIlayRes_bind_transect(rad,transect,fpath,filename_ME70,filename_ER60,filename_ER60h)

%% Load EI lay results stored in 'fpath' and bind them into arrays

filelist = squeeze(dir(fullfile(fpath,['results__Rad',num2str(rad),'_transect',num2str(transect),'*.mat'])));  % all files
nb_files = size(filelist,1);  % number of files

depth_bottom_ME70_db=[];
depth_surface_ME70_db=[];
lat_surfME70_db=[];
lon_surfME70_db=[];
lat_botME70_db=[];
lon_botME70_db=[];
Sa_surfME70_db = [];
Sa_botME70_db = [];
Sv_surfME70_db = [];
Sv_botME70_db = [];
time_ME70_db = [];
vol_surfME70_db = [];

depth_bottom_ER60_db=[];
depth_surface_ER60_db=[];
lat_surfER60_db=[];
lon_surfER60_db=[];
lat_botER60_db=[];
lon_botER60_db=[];
Sa_surfER60_db = [];
Sa_botER60_db = [];
Sv_surfER60_db = [];
Sv_botER60_db = [];
time_ER60_db = [];
vol_surfER60_db = [];

depth_surface_ER60h_db=[];
lat_surfER60h_db=[];
lon_surfER60h_db=[];
Sa_surfER60h_db = [];
Sv_surfER60h_db = [];
time_ER60h_db = [];
vol_surfER60h_db = [];

nblME70=0;
nblER60=0;
nblER60h=0;

for numfile = 1:nb_files
    
    hacfilename = filelist(numfile,:).name;
    FileName = [fpath,hacfilename];
    load(FileName)
    
    if (((length(time_ME70))>0))
%         if (nblER60==0)
 
               %cas une seule fr�quence un seul ESU attention il faut inverser pour avoir toujours esu,layer,freq !!!
            if((size(Depth_botME70,1)==1 && size(Depth_botME70,3)==1) || (size(Depth_surfME70,1)==1 && size(Depth_surfME70,3)==1))
                 time_ME70_db(nblME70+1:nblME70+length(time_ME70)) = time_ME70/86400 +719529;
                depth_bottom_ME70_db(nblME70+1:nblME70+size(Depth_botME70,1),:,:) = Depth_botME70';
                depth_surface_ME70_db(nblME70+1:nblME70+size(Depth_surfME70,1),:,:) = Depth_surfME70';
                Sa_surfME70_db(nblME70+1:nblME70+size(Sa_surfME70,1),:,:) = Sa_surfME70';
                Sa_botME70_db(nblME70+1:nblME70+size(Sa_botME70,1),:,:) = Sa_botME70';
                Sv_surfME70_db(nblME70+1:nblME70+size(Sv_surfME70,1),:,:) = Sv_surfME70';
                Sv_botME70_db(nblME70+1:nblME70+size(Sv_botME70,1),:,:) = Sv_botME70';
                lat_surfME70_db(nblME70+1:nblME70+size(Lat_surfME70,1),:,:) = Lat_surfME70';
                lat_botME70_db(nblME70+1:nblME70+size(Lat_botME70,1),:,:) = Lat_botME70';
                lon_surfME70_db(nblME70+1:nblME70+size(Long_surfME70,1),:,:) = Long_surfME70';
                lon_botME70_db(nblME70+1:nblME70+size(Long_botME70,1),:,:) = Long_botME70';
                vol_surfME70_db(nblME70+1:nblME70+size(Volume_surfME70,1),:,:) = Volume_surfME70';
            elseif (nblME70==0 || (nblME70>0 && (size(depth_bottom_ME70_db,ndims(depth_bottom_ME70_db))==size(Depth_botME70,ndims(Depth_botME70)) && size(depth_surface_ME70_db,ndims(depth_surface_ME70_db))==size(Depth_surfME70,ndims(Depth_surfME70)))))
                time_ME70_db(nblME70+1:nblME70+length(time_ME70)) = time_ME70/86400 +719529;
                depth_bottom_ME70_db(nblME70+1:nblME70+size(Depth_botME70,1),:,:) = Depth_botME70;
                depth_surface_ME70_db(nblME70+1:nblME70+size(Depth_surfME70,1),:,:) = Depth_surfME70;
                Sa_surfME70_db(nblME70+1:nblME70+size(Sa_surfME70,1),:,:) = Sa_surfME70;
                Sa_botME70_db(nblME70+1:nblME70+size(Sa_botME70,1),:,:) = Sa_botME70;
                Sv_surfME70_db(nblME70+1:nblME70+size(Sv_surfME70,1),:,:) = Sv_surfME70;
                Sv_botME70_db(nblME70+1:nblME70+size(Sv_botME70,1),:,:) = Sv_botME70;
                lat_surfME70_db(nblME70+1:nblME70+size(Lat_surfME70,1),:,:) = Lat_surfME70;
                lat_botME70_db(nblME70+1:nblME70+size(Lat_botME70,1),:,:) = Lat_botME70;
                lon_surfME70_db(nblME70+1:nblME70+size(Long_surfME70,1),:,:) = Long_surfME70;
                lon_botME70_db(nblME70+1:nblME70+size(Long_botME70,1),:,:) = Long_botME70;
                vol_surfME70_db(nblME70+1:nblME70+size(Volume_surfME70,1),:,:) = Volume_surfME70;
            end
    end
    if (((length(time_ER60))>0))
              
            %cas une seule fr�quence un seul ESU attention il faut inverser pour avoir toujours esu,layer,freq !!!
            if((size(Depth_botER60,1)==1 && size(Depth_botER60,3)==1) || (size(Depth_surfER60,1)==1 && size(Depth_surfER60,3)==1))
                 time_ER60_db(nblER60+1:nblER60+length(time_ER60)) = time_ER60/86400 +719529;
                depth_bottom_ER60_db(nblER60+1:nblER60+size(Depth_botER60,1),:,:) = Depth_botER60';
                depth_surface_ER60_db(nblER60+1:nblER60+size(Depth_surfER60,1),:,:) = Depth_surfER60';
                Sa_surfER60_db(nblER60+1:nblER60+size(Sa_surfER60,1),:,:) = Sa_surfER60';
                Sa_botER60_db(nblER60+1:nblER60+size(Sa_botER60,1),:,:) = Sa_botER60';
                Sv_surfER60_db(nblER60+1:nblER60+size(Sv_surfER60,1),:,:) = Sv_surfER60';
                Sv_botER60_db(nblER60+1:nblER60+size(Sv_botER60,1),:,:) = Sv_botER60';
                lat_surfER60_db(nblER60+1:nblER60+size(Lat_surfER60,1),:,:) = Lat_surfER60';
                lat_botER60_db(nblER60+1:nblER60+size(Lat_botER60,1),:,:) = Lat_botER60';
                lon_surfER60_db(nblER60+1:nblER60+size(Long_surfER60,1),:,:) = Long_surfER60';
                lon_botER60_db(nblER60+1:nblER60+size(Long_botER60,1),:,:) = Long_botER60';
                vol_surfER60_db(nblER60+1:nblER60+size(Volume_surfER60,1),:,:) = Volume_surfER60';
           
            elseif (nblER60==0 || (nblER60>0 && (size(depth_bottom_ER60_db,ndims(depth_bottom_ER60_db))==size(Depth_botER60,ndims(Depth_botER60)) && size(depth_surface_ER60_db,ndims(depth_surface_ER60_db))==size(Depth_surfER60,ndims(Depth_surfER60)))))
                 time_ER60_db(nblER60+1:nblER60+length(time_ER60)) = time_ER60/86400 +719529;
                depth_bottom_ER60_db(nblER60+1:nblER60+size(Depth_botER60,1),:,:) = Depth_botER60;
                depth_surface_ER60_db(nblER60+1:nblER60+size(Depth_surfER60,1),:,:) = Depth_surfER60;
                Sa_surfER60_db(nblER60+1:nblER60+size(Sa_surfER60,1),:,:) = Sa_surfER60;
                Sa_botER60_db(nblER60+1:nblER60+size(Sa_botER60,1),:,:) = Sa_botER60;
                Sv_surfER60_db(nblER60+1:nblER60+size(Sv_surfER60,1),:,:) = Sv_surfER60;
                Sv_botER60_db(nblER60+1:nblER60+size(Sv_botER60,1),:,:) = Sv_botER60;
                lat_surfER60_db(nblER60+1:nblER60+size(Lat_surfER60,1),:,:) = Lat_surfER60;
                lat_botER60_db(nblER60+1:nblER60+size(Lat_botER60,1),:,:) = Lat_botER60;
                lon_surfER60_db(nblER60+1:nblER60+size(Long_surfER60,1),:,:) = Long_surfER60;
                lon_botER60_db(nblER60+1:nblER60+size(Long_botER60,1),:,:) = Long_botER60;
                vol_surfER60_db(nblER60+1:nblER60+size(Volume_surfER60,1),:,:) = Volume_surfER60;
            end
    end
    if (exist('time_ER60h','var')>0)
        if (((length(time_ER60h))>0))
            
            if(size(Depth_surfER60h,1)==1 && size(Depth_surfER60h,3)==1)
                time_ER60h_db(nblER60h+1:nblER60h+length(time_ER60h)) = time_ER60h/86400 +719529;
                depth_surface_ER60h_db(nblER60h+1:nblER60h+size(Depth_surfER60h,1),:,:) = Depth_surfER60h';
                Sa_surfER60h_db(nblER60h+1:nblER60h+size(Sa_surfER60h,1),:,:) = Sa_surfER60h';
                Sv_surfER60h_db(nblER60h+1:nblER60h+size(Sv_surfER60h,1),:,:) = Sv_surfER60h';
                lat_surfER60h_db(nblER60h+1:nblER60h+size(Lat_surfER60h,1),:,:) = Lat_surfER60h';
                lon_surfER60h_db(nblER60h+1:nblER60h+size(Long_surfER60h,1),:,:) = Long_surfER60h';
                vol_surfER60h_db(nblER60h+1:nblER60h+size(Volume_surfER60h,1),:,:) = Volume_surfER60h';
            elseif (nblER60h==0 || (nblER60h>0 && size(depth_surface_ER60h_db,3)==size(Depth_surfER60h,ndims(Depth_surfER60h))))
                time_ER60h_db(nblER60h+1:nblER60h+length(time_ER60h)) = time_ER60h/86400 +719529;
                depth_surface_ER60h_db(nblER60h+1:nblER60h+size(Depth_surfER60h,1),:,:) = Depth_surfER60h;
                Sa_surfER60h_db(nblER60h+1:nblER60h+size(Sa_surfER60h,1),:,:) = Sa_surfER60h;
                Sv_surfER60h_db(nblER60h+1:nblER60h+size(Sv_surfER60h,1),:,:) = Sv_surfER60h;
                lat_surfER60h_db(nblER60h+1:nblER60h+size(Lat_surfER60h,1),:,:) = Lat_surfER60h;
                lon_surfER60h_db(nblER60h+1:nblER60h+size(Long_surfER60h,1),:,:) = Long_surfER60h;
                vol_surfER60h_db(nblER60h+1:nblER60h+size(Volume_surfER60h,1),:,:) = Volume_surfER60h;
            end
        end
    end

    nblME70=length(time_ME70_db);
    nblER60=length(time_ER60_db);
    nblER60h=length(time_ER60h_db);

    
end;

save(filename_ME70,'time_ME70_db','depth_surface_ME70_db','depth_bottom_ME70_db','Sv_surfME70_db','Sv_botME70_db','Sa_surfME70_db','Sa_botME70_db','lat_surfME70_db','lon_surfME70_db','lat_botME70_db','lon_botME70_db','vol_surfME70_db');
save(filename_ER60,'time_ER60_db','depth_surface_ER60_db','depth_bottom_ER60_db','Sv_surfER60_db','Sv_botER60_db','Sa_surfER60_db','Sa_botER60_db','lat_surfER60_db','lon_surfER60_db','lat_botER60_db','lon_botER60_db','vol_surfER60_db');
save(filename_ER60h,'time_ER60h_db','depth_surface_ER60h_db','Sv_surfER60h_db','Sa_surfER60h_db','lat_surfER60h_db','lon_surfER60h_db','vol_surfER60h_db');
end

