%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%
%%% file for reading annual matrices of diversity index and calculating empirical orthogonal
%%% functions (eof) across years by depth strata (mean for surface and
%%% bottom layers) ref "Development and application of an
%%% acoustic multi-frequency diversity index for monitoring major scatter
%%% groups in large scale pelagic ecosystems" submitted to MEPS in August 2011
%%%
%%% Verena Trenkel 03/2011
%%% Laurent Berger 09/2011
%%%
%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

close all;
clear all;

%import acoustic descriptor in terms of 3D matrix (lat,long,depth) of diversity index
%and associated proportions for species groups
dirdata='Q:\projects\ObsHal\Indicateurs\Data\';
yearlist={'2006', '2008','2009','2010'};

resdir='C:\FAST\results\Indicateurs\AnalyseSpaceTime\'

propmean=NaN(length(yearlist),2,5);

%import data matrice
for years= 1:length(yearlist)
    datdir=cell2mat(strcat(dirdata,yearlist(years),'\'));
    datf=[datdir,'DataMat.mat'];
    load(datf);
    mX=size(matClarkeER60f,1);
    nX=size(matClarkeER60f,2);
    
     % we define here a middle layer that starts at 30m (7th
    % layer surface hence 11th layer in the matrix) and stops 20m above the
    % bottom, this layer is not fixed size and can be empty
    matPropER60fmeanmiddle=NaN(mX,nX,3);
    for m = 1:mX
        for n = 1:nX
            layerend=floor(matdepthER60(m,n)/5)-4;
            if (layerend>0 && layerend<=size(matSvER60,3))
                for class=[1 2 5]
                    if(size(matPropER60f(isfinite(matPropER60f(m,n,11:layerend,class))))>0)
                        propER60= matPropER60f(m,n,7:layerend,class);
                        matPropER60fmeanmiddle(m,n,class)=mean(propER60(isfinite(propER60)));
                    end
                end
            end
        end
    end
    
    %we compute average proportion for surface bottom and middle layer for
    % each species group for each year
    index=1;
    for class=[1 2 5]
        propER60=matPropER60f(:,:,7:10,class);
        propmean(years,1,index)=mean(propER60(isfinite(propER60)));
        propER60=matPropER60f(:,:,1:4,class);
        propmean(years,2,index)=mean(propER60(isfinite(propER60)));
        propER60=matPropER60fmeanmiddle(:,:,class);
        propmean(years,3,index)=mean(propER60(isfinite(propER60)));
        index=index+1;
    end
    
    
    % we choose here on which kind of data is computed EOF
%     data=matPropER60f(:,:,:,1); %swimbladder fish ref FilterandGridIndex.m
%     data=matPropER60f(:,:,:,2); %fluidlike zooplancton fish ref FilterandGridIndex.m
%     data=matPropER60f(:,:,:,5); %resonant organisms ref FilterandGridIndex.m
    data=matClarkeER60fcoeffv; %species richness ref FilterandGridIndex.m
    
    for m = 1:mX
        for n = 1:nX
            % mean across non null surface layers
            temp=0;
            count=0;
            for p=7:10
                if(isfinite(data(m,n,p)))
                    temp=temp+data(m,n,p);
                    count=count+1;
                end
                if count>0
                    DataSurface(m,n,years)=temp/count;
                else
                    DataSurface(m,n,years)=NaN;
                end
            end
            
            % mean across non null bottom layers
            temp=0;
            count=0;
            for p=1:4
                if(isfinite(data(m,n,p)))
                    temp=temp+data(m,n,p);
                    count=count+1;
                end
                if count>0
                    DataBottom(m,n,years)=temp/count;
                else
                    DataBottom(m,n,years)=NaN;
                end
            end
            
        end
    end
    
end


% we plot the mean proportions through years for each species group and
% for surface, bottom and middle layer
figure;
plot([2006 2008 2009 2010 ],squeeze(propmean(:,1,:)));
set(gca,'XTick',[2006 2008 2009 2010 ])
hold on;
title(['Mean proportion per scattering group in surface layers'],'FontSize',15)
xlabel('Year','FontSize',15);
ylabel('Proportion','FontSize',15);
legend('swimbladder fish','copepods and small euphausids','gas bearing organisms resonant at 38kHz or 70 kHz ');
grid on;
saveas(gcf,[resdir,'Mean_proportion_surface'],'fig')
saveas(gcf,[resdir,'Mean_proportion_surface'],'jpg')
close

figure;
plot([2006 2008 2009 2010],squeeze(propmean(:,2,:)));
set(gca,'XTick',[2006 2008 2009 2010 ])
hold on;
title(['Mean proportion per scattering group in bottom layers'],'FontSize',15)
xlabel('Year','FontSize',15);
ylabel('Proportion','FontSize',15);
legend('swimbladder fish','copepods and small euphausids','gas bearing organisms resonant at 38kHz or 70 kHz ');
grid on;
saveas(gcf,[resdir,'Mean_proportion_bottom'],'fig')
saveas(gcf,[resdir,'Mean_proportion_bottom'],'jpg')
close

figure;
plot([2006 2008 2009 2010],squeeze(propmean(:,3,:)));
set(gca,'XTick',[2006 2008 2009 2010 ])
hold on;
title(['Mean proportion per scattering group in middle layer'],'FontSize',15)
xlabel('Year','FontSize',15);
ylabel('Proportion','FontSize',15);
legend('swimbladder fish','copepods and small euphausids','gas bearing organisms resonant at 38kHz or 70 kHz ');
grid on;
saveas(gcf,[resdir,'Mean_proportion_middle'],'fig')
saveas(gcf,[resdir,'Mean_proportion_middle'],'jpg')
close

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% calculate eofs for matClarkeER60f analysis across years and plot
% Data is of dimensions lat x long x years
% analyse 4 surface layers and 4 bottom layers separately; ignore
% intermediate layer
namelayer={'surface','bottom'};

for layer=1:2
    
    % select layers
    if(layer==1) data=DataSurface;
    end
    if (layer==2) data=DataBottom;
    end
    
    % Reshape data to 2D matrix of (lat_long_cell, depth)
    data=reshape(data,[size(data,1)*size(data,2),size(data,3)]);
    % transpose data matrix round to make it depth x latlong
    data=data.';
    %datamask are cells to be included: those with positive values
    numdata=sum((data),1);
    datamask=isfinite(numdata);
    F=data(:,datamask);
    [M,N]=size(F);
    Fmean=mean(F);
    
    % Eigenvectors and eigenvalues - use SVD
    [E,D,E2]=svd(F);
    
    % Normalized eigenvalues
    evaln=diag(D)./sum(diag(D)); %./ elementwise devision
    
    % Principal components
    U=E*D;
    
    % Put the eigenvectors back into the original lon/lat state
    % Remember to re-insert the gridpoints you left out
    Ep=ones(size(matClarkeER60f,1)*size(matClarkeER60f,2),N)*NaN;
    Ep(datamask,:)=E2;
    Ep=reshape(Ep,size(matClarkeER60f,1),size(matClarkeER60f,2),N);
    
    Fp=ones(size(matClarkeER60f,1)*size(matClarkeER60f,2),1)*NaN;
    Fp(datamask)=Fmean;
    Fp=reshape(Fp,size(matClarkeER60f,1),size(matClarkeER60f,2));
    
    %coastline and rivers
    landareas = shaperead('landareas.shp','UseGeoCoords', true);
    rivers = shaperead('worldrivers', 'UseGeoCoords', true);
    cities = shaperead('worldcities', 'UseGeoCoords', true);
    
    years=str2double(yearlist);
    for ip=1:2  % show two first principal components (there are as many components as years)
        scrsz = get(0,'ScreenSize');
        figure('Position',[20 scrsz(4)/2+20 scrsz(3)-40 scrsz(4)/2-40])
        h=subplot(1,2,1);
        subplot('Position',[0.03 0.05 0.53 0.9]);
        
        % turn signs around if eigenvalues are negative to faciliatate
        % interpretation of maps!
        h=pcolor(Long,Lat,squeeze(Ep(:,:,ip))*sign(U(1,ip)));
        set(h,'edgecolor','none')
        
        hold on;
        geoshow(landareas, 'FaceColor', 'white');
        for index=1:numel(cities)
            if (strcmp(cities(index).Name,'Nantes'))
                text(cities(index).Lon, cities(index).Lat,'Loire','FontSize',11,'Color', 'blue');
            elseif (strcmp(cities(index).Name,'Bordeaux'))
                text(cities(index).Lon, cities(index).Lat,'Gironde','FontSize',11,'Color', 'blue');
            end
        end
        geoshow(rivers, 'Color', 'blue')
        
        axis([-6 0 43 49]);
        
        %     set(h,'edgecolor','none')
        %     caxis([-0.2 0.2]);
        colorbar;
        title(cat(2,cat(2,cat(2,'EOF',num2str(ip)),' across years '),cell2mat(namelayer(layer))),'FontSize',12);

        h=subplot(1,2,2);
        subplot('Position',[0.65 0.05 0.33 0.9]);
        plot(years,U(:,ip).*sign(U(:,ip)),'--ks'); % U are eigenvectors (maximum number is number of cells)
        set(gca,'xtick',[2006 2008 2009 2010]);
        grid on;
        title(cat(2,cat(2,cat(2,'PC ',num2str(ip)),'  percent variance expl='),num2str(evaln(ip)*100)),'FontSize',12);
        saveas(gcf,[resdir,'EofYearsRichness',num2str(ip),'_',cell2mat(namelayer(layer))],'jpg');
    end;
    % Save various variables for later recall
    fres=[resdir,'yearEOF',cell2mat(namelayer(layer)),'.mat'];
    save(fres, 'F', 'D', 'E', 'U', 'Fmean', 'Ep','evaln');
    
    % Clear some vars
    clear E2 F2 R F Fmean E D
end
