%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%
%%% programme pour lire les valeurs d'indicateurs calcul�es par le script
%%% indicateurs et telles que d�finis dans la publi "Development and application of an
%%% acoustic multi-frequency diversity index for monitoring major scatter 
%%% groups in large scale pelagic ecosystems" soumise � MEPS en Aout 2011
%%% les filtrer suivant la m�teo et la navigation, caluler les proportions
%%% pour diff�rentes classes d'organismes et les moyenner spatialement
%%%
%%% Laurent Berger 09/2011
%%%
%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

close all;
clear all;

  
%r�pertoire des donn�es de Sv multifr�quence et d'indicateurs ainsi que des meta donn�es pour les r�f�rencer
% spatialement et temporellement
dh='K:\';
dirlist={[dh,'SMFH\Indicateurs\data\HacCorPel06'],[dh,'SMFH\Indicateurs\data\HacCorPel07'],[dh,'SMFH\Indicateurs\data\HacCorPG08'],[dh,'SMFH\Indicateurs\data\PELGAS09'],[dh,'SMFH\Indicateurs\data\HacCorPel10']};

%ann�es prises en compte
yearlist={'2006', '2007','2008','2009','2010'};

% r�pertoire des donn�es r�sultats
resdirp='Q:\projects\ObsHal\Indicateurs\Data\';

%on charge les donn�es acoustiques calcul�es au pr�alable
for years= 1:length(yearlist)
    resdir=cell2mat(strcat(resdirp,yearlist(years),'\'));
    dirdata=cell2mat(dirlist(years));
    filelist = ls([dirdata,'\Clarke*.mat']);  % ensemble des fichiers
    nb_files = size(filelist,1);  % nombre de fichiers
    sort(filelist);
    
    nblER60=0;
    for numfile = 1:nb_files
        matfilename = filelist(numfile,:)
        FileName = [dirdata,'\',matfilename(1:findstr('.',matfilename)-1),'.mat'];
        load(FileName);
        latER60all(nblER60+1:nblER60+length(latER60)) = latER60;
        longER60all(nblER60+1:nblER60+length(longER60)) = longER60;
        timeER60all(nblER60+1:nblER60+length(timeER60)) = timeER60;
        depthER60all(nblER60+1:nblER60+length(depthER60)) = depthER60;
        SvER60all(nblER60+1:nblER60+size(CellulesER60,1),:,:) = CellulesER60;
        ClarkeER60all_f(nblER60+1:nblER60+size(delta_ER60f,1),:) = delta_ER60f;
        nblER60=length(latER60all);
        clear  latER60  longER60 timeER60 depthER60;
        clear  CellulesER60  delta_ER60g delta_ER60f;
    end
    
    % transposition de temps HAC en temps Matlab
    hourER60all = str2num(datestr(timeER60all/86400 +719529,'HH'))';
    dateER60all=timeER60all/86400 +719529;
    
    %chargement des donn�es meteo � partir de vecteurs de donn�es de vent
    %d'attitude et de navigation issues de casino et sauvegard�es au format
    %matlab sous forme de vecteur pour chaque param�tres
    %save ([dirdata,'Nav'],'truewind','vesselspeed','heave');
    %save ([dirdata,'Env'],'temperature','salinity','fluo','radiation');
    load([dirdata,'\','Nav.mat']);
    clear heave;
    load([dirdata,'\','Env.mat']);
    
    % lissage des donn�es de vent
    windowWidth = (20);
    halfWidth = windowWidth / 2;
    gaussFilter = gausswin(20);
    gaussFilter = gaussFilter / sum(gaussFilter); % Normalize.
    smoothedwind = conv(truewind, gaussFilter);
    truewind=smoothedwind(halfWidth:end-halfWidth);
    
    % d�finition de la grille spatiale de moyennage (�tendue et nombre de cellules)
    mX=25;
    nX=25;
    longmin = -6;
    longmax= -1;
    latmin = 43.5;
    latmax = 48.5;
    interlat = latmin:(latmax-latmin)/mX:latmax;
    for i = 1:mX
        slatm(i) = (interlat(i) );
    end
    Lat=interlat(1:(mX))+ 0.5 * (latmax-latmin)/mX;
    interlon = longmin:(longmax-longmin)/nX:longmax;
    for j = 1:nX
        slonn(j) = (interlon(j));
    end
    Long=interlon(1:(nX)) +0.5*(longmax-longmin)/nX;
    
    % param�tres des filtres pour restreindre la donn�e � la donn�e non
    % potentiellement biais�e
    maxdepth=200;
    hourmin=4;
    hourmax=19;
    vesselspeedmin=6;
    windspeedmax=20;
    
    %trait de c�te
    landareas = shaperead('landareas.shp','UseGeoCoords', true);  
  
    
    matPropER60f=NaN(mX,nX,size(ClarkeER60all_f,2),5);
    matClarkeER60f=NaN(mX,nX,size(ClarkeER60all_f,2));
    matClarkeER60fcoeffv=NaN(mX,nX,size(ClarkeER60all_f,2));
    matSvER60=NaN(mX,nX,size(SvER60all,2),size(SvER60all,3));
    matdepthER60=NaN(mX,nX);
    mattimeER60=NaN(mX,nX);
    matdateER60=NaN(mX,nX);
    matWind=NaN(mX,nX);
    matTemperature=NaN(mX,nX);
    matSalinity=NaN(mX,nX);
    matFluorimetry=NaN(mX,nX);
    
    % calcul des valeurs moyennes de l'index du CV et des proportions pour
    % diff�rents groupes d'esp�ces
    for layer=1:size(ClarkeER60all_f,2)
        for m = 1:mX   %lat
            for n = 1:nX %long
                %on r�cup�re les index pour la cellule en longitude et
                %latitude
                ff = find(latER60all>= interlat(m) & latER60all<= interlat(m+1) & longER60all>=interlon(n) & longER60all<= interlon(n+1));
                
                if ~isempty(ff)
                    % on filtre � 200m de profondeur et de jour
                    jj= find(depthER60all(ff)<maxdepth & hourER60all(ff)>=hourmin & hourER60all(ff)<=hourmax) ;
                    
                    if ~isempty(jj)
                        
                        matWind(m,n)=mean(truewind(ff(jj)));
                        matTemperature(m,n)=mean(temperature(ff(jj)));
                        matSalinity(m,n)=mean(salinity(ff(jj)));
                        matFluorimetry(m,n)=mean(fluo(ff(jj)));
                        matdepthER60(m,n)=mean(depthER60all(ff(jj)));
                        mattimeER60(m,n)=mean(hourER60all(ff(jj)));
                        matdateER60(m,n)=mean(dateER60all(ff(jj)));
                        
                        %on filtre les donn�es en Sv moyen
                        kk= find((SvER60all(ff(jj),layer,1)>-95)'& (SvER60all(ff(jj),layer,2)>-95)'& (SvER60all(ff(jj),layer,3)>-95)'& (SvER60all(ff(jj),layer,4)>-95)'& (SvER60all(ff(jj),layer,5)>-95)' ...
                            & (SvER60all(ff(jj),layer,1)<-30)'& (SvER60all(ff(jj),layer,2)<-30)'& (SvER60all(ff(jj),layer,3)<-30)'& (SvER60all(ff(jj),layer,4)<-30)'& (SvER60all(ff(jj),layer,5)<-30)');
                        
                        %on filtre en fonction du vent et de la vitesse
                        %navire
                        ll=find(truewind(ff(jj(kk)))<=windspeedmax &  vesselspeed(ff(jj(kk)))>=vesselspeedmin);
                        
                        ii=ff(jj(kk(ll)));
                        
                        if ~isempty(ii)
                            %Sv moyen dans la cellule
                            for freq=1:size(SvER60all,3)
                                matSvER60(m,n,layer,freq) = 10*log10(mean((10.^((SvER60all(ii,layer,freq))./10))));
                            end
                            
                            %index moyen dans la cellule et CV
                            matClarkeER60f(m,n,layer) = mean(ClarkeER60all_f(ii,layer));
                            matClarkeER60fcoeffv(m,n,layer) = std(ClarkeER60all_f(ii,layer))/mean(ClarkeER60all_f(ii,layer));
                            
                            % on d�finit les bornes de classes d'esp�ces �
                            % partir des valeurs d'index pour les r�ponses
                            % fr�quentielles de SIMFAMI (ref
                            % indicateur_rf_simul pour la simulation)
                            indexfish=length(ClarkeER60all_f(ii(ClarkeER60all_f(ii,layer)>=0.4 & ClarkeER60all_f(ii,layer)<=0.5),layer));
                            indexfluidplancton = length(ClarkeER60all_f(ii(ClarkeER60all_f(ii,layer)>=0.7 & ClarkeER60all_f(ii,layer)<0.85),layer));
                            indexmackerel = length(ClarkeER60all_f(ii(ClarkeER60all_f(ii,layer)>0.9),layer));
                            indexmicrostructure= length(ClarkeER60all_f(ii(ClarkeER60all_f(ii,layer)>0.65 & ClarkeER60all_f(ii,layer)<=0.7),layer));
                            indexgaseeous= length(ClarkeER60all_f(ii(ClarkeER60all_f(ii,layer)>0.55 & ClarkeER60all_f(ii,layer)<=0.65),layer));
                            total=length(ClarkeER60all_f(ii(ClarkeER60all_f(ii,layer)>0)));
                            
                            matPropER60f(m,n,layer,1) = (indexfish)/total;
                            matPropER60f(m,n,layer,2) = (indexfluidplancton)/total;
                            matPropER60f(m,n,layer,3) = (indexmackerel)/total;
                            matPropER60f(m,n,layer,4) = (indexmicrostructure)/total;
                            matPropER60f(m,n,layer,5) = (indexgaseeous)/total;
                        end
                    end
                end
            end
        end
    end
    
    %on sauvegarde les r�sultats
    resf=[resdir,'DataMat.mat'];
    save(resf,'matSvER60' ,'matClarkeER60f','matClarkeER60fcoeffv', 'mattimeER60', 'matdateER60','matdepthER60' ,'Long', 'Lat', 'matWind', 'matTemperature', 'matSalinity', 'matFluorimetry', 'matPropER60f');
    
    
    
            
    clear  latER60all  longER60all timeER60all depthER60all hourER60all dateER60all;
    clear SvER60all ClarkeER60all_f;
    clear fluo temperature salinity;
    clear vesselspeed heave truewind;
end

%autres repr�sentations des donn�es d'EI
%***************************************
% 
% % pour la figure 2 � l'�chelle d'une campagne les donn�es non de sv 
% % et d'index par cellules non moyenn�es spatialement
% jj=find(hourER60all>=hourmin & hourER60all<=hourmax);
% kk=find( (SvER60all(jj,1,1)>-95)'& (SvER60all(jj,1,2)>-95)'& (SvER60all(jj,1,3)>-95)'& (SvER60all(jj,1,4)>-95)'& (SvER60all(jj,1,5)>-95)' ...
%     & (SvER60all(jj,1,1)<-30)'& (SvER60all(jj,1,2)<-30)'& (SvER60all(jj,1,3)<-30)'& (SvER60all(jj,1,4)<-30)'& (SvER60all(jj,1,5)<-30)');
% ll=find(truewind(jj(kk))'<=windspeedmax &    vesselspeed(jj(kk))'>=vesselspeedmin);
% 
% long=longER60all(jj(kk(ll)));
% lat=latER60all(jj(kk(ll)));
% sv=10*log10(mean((10.^((SvER60all(jj(kk(ll)),1,5))./10)),2));
% clarke=ClarkeER60all_f(jj(kk(ll)),1);
% 
% figure;
% scatter(long(1:50:end),lat(1:50:end),15,sv(1:50:end),'filled');
% hold on;
% geoshow(landareas);
% title([' Sv 200 kHz first bottom layer'],'FontSize',15)
% xlabel('Longitude [degree]','FontSize',15);
% ylabel('Latitude [degree]','FontSize',15);
% zlabel('Sv (dB) ','FontSize',15)
% colorbar('location','eastoutside')
% axis([longmin longmax latmin latmax]);
% caxis([-75 -40]);
% grid on;
% saveas(gcf,[resdirp,'Sv200'],'fig')
% saveas(gcf,[resdirp,'Sv200'],'jpg')
% close;
% 
% figure;
% scatter(long(1:50:end),lat(1:50:end),15,clarke(1:50:end),'filled');
% hold on;
% geoshow(landareas);
% title([' Multi-frequency index for first bottom layer'],'FontSize',15)
% xlabel('Longitude [degree]','FontSize',15);
% ylabel('Latitude [degree]','FontSize',15);
% zlabel('Clarke','FontSize',15)
% colorbar('location','eastoutside')
% axis([longmin longmax latmin latmax]);
% caxis([0.4 0.8]);
% grid on;
% saveas(gcf,[resdirp,'Clarke'],'fig')
% saveas(gcf,[resdirp,'Clarke'],'jpg')
% close;

       
%         %histogram diversit� et r�ponse fr�quentielle � l'�chelle d'une
%         %campagne
%         data=ClarkeER60all_f(:,7:44);
%         
%         edge=[0.3:0.05:1];
%         
%         index=  hourER60all>hourmin & hourER60all<hourmax ;
%         
%         n=histc(data,edge);
%         figure;
%         bar(edge,n);
%         xlabel('Diversity ');
%         title(['Histogram multifrequency diversyty for each layer']);
%         saveas(gcf,[dirdata,'HistogramDiversity'],'fig')
%         saveas(gcf,[dirdata,'HistogramDiversity'],'jpg')
%         
%         edge=[0 0.3 0.5 0.7 0.85 1];
%         
%         index=  hourER60all>hourmin & hourER60all<hourmax ;
%         
%         n=histc(data,edge);
%         for i=1:9
%             n(:,i)=n(:,i)./length(data(data(:,i)>0));
%         end
%         figure;
%         bar(edge,n);
%         xlabel('Diversity ');
%         title(['Proportion species group  for each layer']);
%         saveas(gcf,[dirdata,'ProportionSpecies'],'fig')
%         saveas(gcf,[dirdata,'ProportionSpecies'],'jpg')
%         
%         clear index n data;
%         
%         freq_response=SvER60all(:,7:44,:)-repmat(SvER60all(:,7:44,2),[1 1 5]);
%         
%         index_Fish=(data<0.5 & data>0.3);
%         ind=find(index_Fish>0);
%         
%         matfreq_Fish18=squeeze(freq_response(:,:,1));
%         matfreq_Fish38=squeeze(freq_response(:,:,2));
%         matfreq_Fish70=squeeze(freq_response(:,:,3));
%         matfreq_Fish120=squeeze(freq_response(:,:,4));
%         matfreq_Fish200=squeeze(freq_response(:,:,5));
%         
%         figure;boxplot([matfreq_Fish18(ind) matfreq_Fish38(ind) matfreq_Fish70(ind) matfreq_Fish120(ind) matfreq_Fish200(ind)],[18 38 70 120 200]);
%         xlabel('Frequency ');
%         title(['Frequency response Swimbladder Fish']);
%         saveas(gcf,[dirdata,'FrequencyResponseFish'],'fig')
%         saveas(gcf,[dirdata,'FrequencyResponseFish'],'jpg')
%         
%         index_Plankton=(data<0.85 &data>0.7);
%         ind=find(index_Plankton>0);
%         
%         matfreq_Plankton18=squeeze(freq_response(:,:,1));
%         matfreq_Plankton38=squeeze(freq_response(:,:,2));
%         matfreq_Plankton70=squeeze(freq_response(:,:,3));
%         matfreq_Plankton120=squeeze(freq_response(:,:,4));
%         matfreq_Plankton200=squeeze(freq_response(:,:,5));
%         
%         figure;boxplot([matfreq_Plankton18(ind) matfreq_Plankton38(ind) matfreq_Plankton70(ind) matfreq_Plankton120(ind) matfreq_Plankton200(ind)],[18 38 70 120 200]);
%         xlabel('Frequency ');
%         title(['Frequency response Plankton']);
%         saveas(gcf,[dirdata,'FrequencyResponsePlankton'],'fig')
%         saveas(gcf,[dirdata,'FrequencyResponsePlankton'],'jpg')
%         
%         index_Mackerel=(data<1 &data>0.85);
%         ind=find(index_Mackerel>0);
%         
%         matfreq_Mackerel18=squeeze(freq_response(:,:,1));
%         matfreq_Mackerel38=squeeze(freq_response(:,:,2));
%         matfreq_Mackerel70=squeeze(freq_response(:,:,3));
%         matfreq_Mackerel120=squeeze(freq_response(:,:,4));
%         matfreq_Mackerel200=squeeze(freq_response(:,:,5));
        
%         figure;boxplot([matfreq_Mackerel18(ind) matfreq_Mackerel38(ind) matfreq_Mackerel70(ind) matfreq_Mackerel120(ind) matfreq_Mackerel200(ind)],[18 38 70 120 200]);
%         xlabel('Frequency ');
%         title(['Frequency response Mackerel']);
%         saveas(gcf,[dirdata,'FrequencyResponseMackerel'],'fig')
%         saveas(gcf,[dirdata,'FrequencyResponseMackerel'],'jpg')