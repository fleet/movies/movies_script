close all
clear all


chemin ='C:\data\EM302\MARMESONET\20091113_HAC\';


%   chemin_config_reverse = strrep(chemin, '\', '/');
%     moLoadConfig(chemin_config_reverse);
% 
%     %activation EI
%     EchoIntegrModule = moEchoIntegration();
%     moSetEchoIntegrationEnabled(EchoIntegrModule);
% %%Echo-int�gration (peut �tre comment� si elle a d�j� �t� ex�cut�e)
% 
% SampleEchoIntegration(chemin,chemin,chemin,EchoIntegrModule);
% clear functions;

% chargement des r�sultats

depth_bottom_ER60=[];
Sa_surface_ER60 = [];
depth_bottom_ME70=[];
Sa_surface_ME70 = [];

seuil=0;


filelist = ls([chemin,'*_EI.mat']);  % ensemble des fichiers
nb_files = size(filelist,1);  % nombre de fichiers
sort(filelist);

nblME70=0;
nblER60=0;
for numfile = 1:nb_files
  
    matfilename = filelist(numfile,:)
    
   
    FileName = [chemin,matfilename(1:findstr('.',matfilename)-1),'.mat'];
    load(FileName);

   
       if  size(Depth_botME70,1)>0

        timeME70(nblME70+1:nblME70+size(Depth_botME70,1)) = time_ME70;
        timeER60(nblER60+1:nblER60+size(Depth_botER60,1)) = time_ER60;
        
        depth_bottom_ME70(nblME70+1:nblME70+size(Depth_botME70,1),:,:) = Depth_botME70;
        depth_bottom_ER60(nblER60+1:nblER60+size(Depth_botER60,1),:,:) = Depth_botER60;

        Sa_bottom_ME70(nblME70+1:nblME70+size(Sa_botME70,1),:,:) = Sa_botME70;
        Sa_bottom_ER60(nblER60+1:nblER60+size(Sa_botER60,1),:,:) = Sa_botER60;

        Sa_surface_ME70(nblME70+1:nblME70+size(Sa_surfME70,1),:,:) = Sa_surfME70;
        Sa_surface_ER60(nblER60+1:nblER60+size(Sa_surfER60,1),:,:) = Sa_surfER60;
        
   

        
        Sa_surface_ME70_mean(nblME70+1:nblME70+size(Sa_surfME70,1),:)=(sum((Volume_surfME70(:,:,:)).*Sa_surfME70(:,:,:),3))./sum((Volume_surfME70(:,:,:)),3);
        Sa_surface_ME70_centre(nblME70+1:nblME70+size(Sa_surfME70,1),:) = Sa_surfME70(:,:,11);
        Sa_surface_ME70_mean_centre(nblME70+1:nblME70+size(Sa_surfME70,1),:) = (sum(((Volume_surfME70(:,:,10:12)).*Sa_surfME70(:,:,10:12)),3)./sum((Volume_surfME70(:,:,10:12)),3));
        

        nblME70=size(depth_bottom_ME70,1);
        nblER60=size(depth_bottom_ER60,1);
    end
    

                
    
  
    
    clear  Depth_botER60  Sa_botER60 Sa_surfER60;
    
            clear  Depth_botME70  Sa_botME70 Sa_surfME70 Volume_surfME70;
        
    
end

    
    Nmesuremono1(:,:)=Sa_surface_ER60(:,:,1);
    Nmesuremono2(:,:)=Sa_surface_ER60(:,:,2);
    Nmesuremono3(:,:)=Sa_surface_ER60(:,:,3);
    Nmesuremono4(:,:)=Sa_surface_ER60(:,:,4);
    Nmesuremono5(:,:)=Sa_surface_ER60(:,:,5);
    
      
    Nmesuremulti(:,:) = (Sa_surface_ME70_mean);
%     NmesuremultiSeuil(:) = (moyenne_Sa_surface_ME70(:,1)/sigma)*((D1*D2)/1852^2);
    Nmesuremulti11(:,:) = (Sa_surface_ME70_centre);
    Nmesuremultimoyennecentre(:,:) = (Sa_surface_ME70_mean_centre);
   

Profondeur = round(depth_bottom_ER60(:,1));

%v:un vecteur par fichier contenant les valeurs de SA avec seuil sur les valeurs des SA
% for jj=1:nb_files
%     v=reshape(Sa_surface_ME70(jj,:),1,21);
%     v=v(v>=seuil);
%     moyenne_Sa_surface_ME70(jj,1)=mean(v);
% end
%     
%graphiques
%cr�ation de vecteurs pour le label de l'axe X
for i=1:1:max(nb_files)
    a(i)=depth_bottom_ER60(i,1);
end

map = colormap(hsv(100));

timeER60ml = timeER60;
timeME70ml = timeME70;


figure;
% plot( Nmesuremono1(:),'b', 'linewidth',2)

plot( timeER60ml,Nmesuremono4(:,1),'r')
hold on
% plot( Nmesuremono3(:),'y', 'linewidth',2)
% plot( Nmesuremono4(:),'g', 'linewidth',2)
% plot( Nmesuremono5(:),'c', 'linewidth',2)
plot(timeME70ml, Nmesuremulti(:,1), 'm' )
plot (timeME70ml, Nmesuremulti11(:,1), 'k' )
% plot(timeME70ml,Nmesuremultimoyennecentre(:,1),'+-','color',map(80,:))

nbESUmono=size(Nmesuremono2,1);
nbESUmulti=size(Nmesuremulti,1);

% plot ((1:1:nb_files),mean(Nmesuremono1(1:numel))*ones(1,nb_files),'--b')
plot (timeER60ml,mean(Nmesuremono4(:,1))*ones(1,nbESUmono),'--r','linewidth',2)
% plot ((1::),mean(Nmesuremono3(1:numel))*ones(1,nb_files),'--y')
% plot ((1::),mean(Nmesuremono4(1:numel))*ones(1,nb_files),'--g')
% plot ((1::),mean(Nmesuremono5(1:numel))*ones(1,nb_files),'--c')
plot (timeME70ml,mean(Nmesuremulti(:,1))*ones(1,nbESUmulti),'--m','linewidth',2)
plot (timeME70ml,mean(Nmesuremulti11(:,1))*ones(1,nbESUmulti),'--k','linewidth',2)
% plot(timeME70ml,mean(Nmesuremultimoyennecentre(:,1))*ones(1,nbESUmulti),'--','color',map(80,:),'linewidth',2)




% legend('mono 18KHz','moyenne mono 18KHz','mono 120KHz',' moyenne mono 120KHz','mono 70KHz','moyenne mono 70KHz','mono 120KHz','moyenne mono 120KHz','mono 200 KHz','moyenne mono 200 KHz','moyenne des 21 faisceaux multi','moyenne des sc�nes multi','faisceau central','moyenne des sc�nes faisceau central','3 faisceaux du centre','moyenne des sc�nes 3 faisceaux centraux','biomasse totale inject�e')
% legend('mono 18KHz','mono 120KHz','mono 70KHz','mono 120KHz','mono 200 KHz','moyenne des 21 faisceaux multi','faisceau central','3 faisceaux du centre','biomasse totale inject�e')
legend('mono 120KHz','mean multi','center multi')
title(['Abundance estimates: ', sprintf('15 to 30 m')])
datetickzoom('x','HH:MM');
ylabel('NASC (m2/mille2)')
xlabel('Heure')
% set(gca,'xtick',1:max(nbESUmono,nbESUmulti))
% set(gca,'xticklabel',time)


xticklabel_rotate

saveas(gcf,[chemin,sprintf('comparaison_sardine_mean')],'fig')

beams=[72 76 81 85 90 94 99 103 108 113 118 115 110 106 101 97 92 88 83 79 74];


figure;
surf([1:1:length(beams)],[1:1:length(timeME70ml)],squeeze(Sa_surface_ME70(:,1,:)))
set(gca,'Ytick',[1:25:length(timeME70ml)]);
set(gca,'YTickLabel',datestr(timeME70ml(1:25:end),'HH:MM'))
set(gca,'Xtick',[1:3:length(beams)]);
set(gca,'XTickLabel',num2str(beams(1:3:end)'))
% datetick('y','HH:MM');
xlabel('Frequency (kHz)');
ylabel('Time (HH:MM)');
zlabel('NASC (m2/mille2)');
saveas(gcf,[chemin,sprintf('comparaison_reel_NASCmulti_15to30m')],'fig')
saveas(gcf,[chemin,sprintf('comparaison_reel_NASCmulti _15to30m')],'jpg')


t11=Nmesuremono4(find(Nmesuremono4(1:min(nbESUmono,nbESUmulti),1)>seuil),1);
t21=Nmesuremulti(find(Nmesuremulti(1:min(nbESUmono,nbESUmulti),1)>seuil),1);
t31= Nmesuremulti11(find(Nmesuremulti11(1:min(nbESUmono,nbESUmulti),1)>seuil),1);

label(1:size(t11))={'mono 120KHz'};
label(size(t11)+1:size(t11)+1+size(t21))={'mean multi'};
label(size(t11)+1+size(t21)+1:size(t11)+1+size(t21)+1+size(t31))={'center multi'};
x = cat(1,t11,t21,t31);
anova1(x,label);
hold on
plot([mean(t11) mean(t21) mean(t31)],'r');
title(['Abundance estimates: ', sprintf('15 to 30 m')])
ylabel('NASC (m2/mille2)')
axis([0 4 0 200])
saveas(gcf,[chemin,sprintf('comparaison_reel_15to30m')],'fig')
saveas(gcf,[chemin,sprintf('comparaison_reel_15to30m')],'jpg')
% 
% figure;
% % plot( Nmesuremono1(:),'b', 'linewidth',2)
% 
% plot( timeER60ml,Nmesuremono4(:,2),'r')
% hold on
% % plot( Nmesuremono3(:),'y', 'linewidth',2)
% % plot( Nmesuremono4(:),'g', 'linewidth',2)
% % plot( Nmesuremono5(:),'c', 'linewidth',2)
% plot( timeME70ml,Nmesuremulti(:,2), 'm' )
% plot ( timeME70ml,Nmesuremulti11(:,2), 'k' )
% % plot(timeME70ml,Nmesuremultimoyennecentre(:,2),'+-','color',map(80,:))
% 
% nbESUmono=size(Nmesuremono2,1);
% nbESUmulti=size(Nmesuremulti,1);
% 
% % plot ((1:1:nb_files),mean(Nmesuremono1(1:numel))*ones(1,nb_files),'--b')
% plot (timeER60ml,mean(Nmesuremono4(:,2))*ones(1,nbESUmono),'--r','linewidth',2)
% % plot ((1::),mean(Nmesuremono3(1:numel))*ones(1,nb_files),'--y')
% % plot ((1::),mean(Nmesuremono4(1:numel))*ones(1,nb_files),'--g')
% % plot ((1::),mean(Nmesuremono5(1:numel))*ones(1,nb_files),'--c')
% plot (timeME70ml,mean(Nmesuremulti(:,2))*ones(1,nbESUmulti),'--m','linewidth',2)
% plot (timeME70ml,mean(Nmesuremulti11(:,2))*ones(1,nbESUmulti),'--k','linewidth',2)
% % plot(timeME70ml,mean(Nmesuremultimoyennecentre(:,2))*ones(1,nbESUmulti),'--','color',map(80,:),'linewidth',2)
% 
% 
% 
% 
% % legend('mono 18KHz','moyenne mono 18KHz','mono 120KHz',' moyenne mono 120KHz','mono 70KHz','moyenne mono 70KHz','mono 120KHz','moyenne mono 120KHz','mono 200 KHz','moyenne mono 200 KHz','moyenne des 21 faisceaux multi','moyenne des sc�nes multi','faisceau central','moyenne des sc�nes faisceau central','3 faisceaux du centre','moyenne des sc�nes 3 faisceaux centraux','biomasse totale inject�e')
% % legend('mono 18KHz','mono 120KHz','mono 70KHz','mono 120KHz','mono 200 KHz','moyenne des 21 faisceaux multi','faisceau central','3 faisceaux du centre','biomasse totale inject�e')
% legend('mono 120KHz','mean multi','center multi')
% title(['Abundance estimates: ', sprintf('30 to 100 m')])
% datetickzoom('x','HH:MM');
% ylabel('nombre de poissons')
% xlabel('Heure')
% 
% xticklabel_rotate
% 
% saveas(gcf,[chemin_reel,sprintf('comparaison_sardine_mean')],'fig')
% 
% figure;
% surf(squeeze(Sa_surface_ME70(:,2,:)))
% xlabel('Beam Number');
% ylabel('ESU Number');
% zlabel('NASC (m2/mille2)');
% saveas(gcf,['C:\temp\stageSMFH2009\developpement\simulation\figures\',sprintf('comparaison_reel_NASCmulti_30to100m')],'fig')
% saveas(gcf,['C:\temp\stageSMFH2009\developpement\simulation\figures\',sprintf('comparaison_reel_NASCmulti _30to100m')],'jpg')
% 
% t12=Nmesuremono4(find(Nmesuremono4(1:min(nbESUmono,nbESUmulti),2)>seuil),2);
% t22=Nmesuremulti(find(Nmesuremulti(1:min(nbESUmono,nbESUmulti),2)>seuil),2);
% t32= Nmesuremulti11(find(Nmesuremulti11(1:min(nbESUmono,nbESUmulti),2)>seuil),2);
% label(1:size(t12))={'mono 120KHz'};
% label(size(t12)+1:size(t12)+1+size(t22))={'mean multi'};
% label(size(t12)+1+size(t22)+1:size(t12)+1+size(t22)+1+size(t32))={'center multi'};
% x = cat(1,t12,t22,t32);
% anova1(x,label);
% title(['Abundance estimates: ', sprintf('30 to 100 m')])
% ylabel('NASC (m2/mille2)')
% saveas(gcf,['C:\temp\stageSMFH2009\developpement\simulation\figures\',sprintf('comparaison_reel_30to100m')],'fig')
% saveas(gcf,['C:\temp\stageSMFH2009\developpement\simulation\figures\',sprintf('comparaison_reel_30to100m')],'jpg')
% 
% 
% 
% figure;
% % plot( Nmesuremono1(:),'b', 'linewidth',2)
% 
% plot( timeER60ml,Nmesuremono4(:,3),'r')
% hold on
% % plot( Nmesuremono3(:),'y', 'linewidth',2)
% % plot( Nmesuremono4(:),'g', 'linewidth',2)
% % plot( Nmesuremono5(:),'c', 'linewidth',2)
% plot( timeME70ml,Nmesuremulti(:,3), 'm' )
% plot ( timeME70ml,Nmesuremulti11(:,3), 'k' )
% % plot(timeME70ml,Nmesuremultimoyennecentre(:,3),'+-','color',map(80,:))
% 
% nbESUmono=size(Nmesuremono2,1);
% nbESUmulti=size(Nmesuremulti,1);
% 
% % plot ((1:1:nb_files),mean(Nmesuremono1(1:numel))*ones(1,nb_files),'--b')
% plot (timeER60ml,mean(Nmesuremono4(:,3))*ones(1,nbESUmono),'--r','linewidth',2)
% % plot ((1::),mean(Nmesuremono3(1:numel))*ones(1,nb_files),'--y')
% % plot ((1::),mean(Nmesuremono4(1:numel))*ones(1,nb_files),'--g')
% % plot ((1::),mean(Nmesuremono5(1:numel))*ones(1,nb_files),'--c')
% plot (timeME70ml,mean(Nmesuremulti(:,3))*ones(1,nbESUmulti),'--m','linewidth',2)
% plot (timeME70ml,mean(Nmesuremulti11(:,3))*ones(1,nbESUmulti),'--k','linewidth',2)
% % plot(timeME70ml,mean(Nmesuremultimoyennecentre(:,3))*ones(1,nbESUmulti),'--','color',map(80,:),'linewidth',2)
% 
% 
% 
% 
% % legend('mono 18KHz','moyenne mono 18KHz','mono 120KHz',' moyenne mono 120KHz','mono 70KHz','moyenne mono 70KHz','mono 120KHz','moyenne mono 120KHz','mono 200 KHz','moyenne mono 200 KHz','moyenne des 21 faisceaux multi','moyenne des sc�nes multi','faisceau central','moyenne des sc�nes faisceau central','3 faisceaux du centre','moyenne des sc�nes 3 faisceaux centraux','biomasse totale inject�e')
% % legend('mono 18KHz','mono 120KHz','mono 70KHz','mono 120KHz','mono 200 KHz','moyenne des 21 faisceaux multi','faisceau central','3 faisceaux du centre','biomasse totale inject�e')
% legend('mono 120KHz','mean multi','center multi')
% title(['Abundance estimates: ', sprintf('100 to 110 m')])
% datetickzoom('x','HH:MM');
% ylabel('nombre de poissons')
% xlabel('Heure')
% 
% xticklabel_rotate
% 
% saveas(gcf,[chemin_reel,sprintf('comparaison_sardine_mean')],'fig')
% 
% figure;
% surf(squeeze(Sa_surface_ME70(:,3,:)))
% xlabel('Beam Number');
% ylabel('ESU Number');
% zlabel('NASC (m2/mille2)');
% saveas(gcf,['C:\temp\stageSMFH2009\developpement\simulation\figures\',sprintf('comparaison_reel_NASCmulti_100to110m')],'fig')
% saveas(gcf,['C:\temp\stageSMFH2009\developpement\simulation\figures\',sprintf('comparaison_reel_NASCmulti _100to110m')],'jpg')
% 
% t13=Nmesuremono4(find(Nmesuremono4(1:min(nbESUmono,nbESUmulti),3)>seuil),3);
% t23=Nmesuremulti(find(Nmesuremulti(1:min(nbESUmono,nbESUmulti),3)>seuil),3);
% t33= Nmesuremulti11(find(Nmesuremulti11(1:min(nbESUmono,nbESUmulti),3)>seuil),3);
% label(1:size(t13))={'mono 120KHz'};
% label(size(t13)+1:size(t13)+1+size(t23))={'mean multi'};
% label(size(t13)+1+size(t23)+1:size(t13)+1+size(t23)+1+size(t33))={'center multi'};
% x = cat(1,t13,t23,t33);
% anova1(x,label);
% title(['Abundance estimates: ', sprintf('100 to 110 m')])
% ylabel('NASC (m2/mille2)')
% saveas(gcf,['C:\temp\stageSMFH2009\developpement\simulation\figures\',sprintf('comparaison_reel_100to110m')],'fig')
% saveas(gcf,['C:\temp\stageSMFH2009\developpement\simulation\figures\',sprintf('comparaison_reel_100to110m')],'jpg')

% figure;
% plot([mean(Nmesuremono4(:,1))/mean(Nmesuremono4(:,1));mean(Nmesuremulti(:,1))/mean(Nmesuremono4(:,1));mean(Nmesuremulti11(:,1))/mean(Nmesuremono4(:,1))],'b')
% set(gca,'XTick',1:1:3)
% set(gca,'XTickLabel',{'mono 120KHz','mean multi','center multi'})
% hold on
% plot([mean(Nmesuremono4(:,2))/mean(Nmesuremono4(:,2));mean(Nmesuremulti(:,2))/mean(Nmesuremono4(:,2));mean(Nmesuremulti11(:,2))/mean(Nmesuremono4(:,2))],'g')
% plot([mean(Nmesuremono4(:,3))/mean(Nmesuremono4(:,3));mean(Nmesuremulti(:,3))/mean(Nmesuremono4(:,3));mean(Nmesuremulti11(:,3))/mean(Nmesuremono4(:,3))],'r')
% legend('15 to 30 m','30 to 100 m','100 to 110 m')
% ylabel('Relative NASC')
% saveas(gcf,['C:\temp\stageSMFH2009\developpement\simulation\figures\',sprintf('comparaison_reel_moyenne')],'fig')
% saveas(gcf,['C:\temp\stageSMFH2009\developpement\simulation\figures\',sprintf('comparaison_reel_moyenne')],'jpg')

% figure;
% plot (Sa_surface_ER60(:,1),'b')
% hold on
% plot (Sa_surface_ER60(:,2),'r')
% hold on
% plot (Sa_surface_ER60(:,3),'y')
% hold on 
% plot (Sa_surface_ER60(:,4),'g')
% hold on
% plot (Sa_surface_ER60(:,5),'c')
% hold on
% legend('fr�quence 1','fr�quence 2','fr�quence 3','fr�quence 4','fr�quence 5');
% title('monofaisceau')
% set(gca,'xtick',1:1:nb_files)
% set(gca,'xticklabel',a)
% xticklabel_rotate
% saveas(gcf,['S:\etude et stages\stageSMFH2009\developpement\figures\',sprintf('monofaisceau_Sa_scene_%g_sardine_%g_%g',seuil,Masse,nbbanc)],'fig')
% 
% 
% 
% figure;
% plot (moyenne_Sa_surface_ME70(:,1),'m')
% hold on
% plot (Sa_surface_ME70(:,11),'k')
% hold on
% title('multifaisceau: Sa')
% legend('moyenne des faisceaux multi','faisceau central');
% set(gca,'xtick',1:1:nb_files)
% set(gca,'xticklabel',a)
% xticklabel_rotate  
% saveas(gcf,['S:\etude et stages\stageSMFH2009\developpement\figures\',sprintf('multifaisceau_Sa_scene_%g_sardine_%g_%g',seuil,Masse,nbbanc)],'fig')
% 
% 
% 
% 
% figure;
% plot (((Sa_surface_ME70(:,:))/sigma)*((D1*D2)/1852^2))
% legend('faisceau 1','faisceau 2','faisceau 3','faisceau 4','faisceau 5','faisceau 6','faisceau 7',...
%         'faisceau 8','faisceau 9','faisceau 10','faisceau 11','faisceau 12','faisceau 13','faisceau 14',...
%         'faisceau 15','faisceau 16','faisceau 17','faisceau 18','faisceau 19','faisceau 20','faisceau 21')
% % set(gca,'xtick',1:1:nb_files)
% % set(gca,'xticklabel',a)
% % xticklabel_rotate
% saveas(gcf,['S:\etude et stages\stageSMFH2009\developpement\figures\',sprintf('21_faisceaux_multifaisceaux_%g_Sa_scene_sardine_%g_%g',seuil,Masse,nbbanc)],'fig')
% % 
% % 
% % 
% 
% figure;
% plot (Sa_surface_ER60(:,1),'b')
% hold on
% plot (Sa_surface_ER60(:,2),'r')
% hold on
% plot (Sa_surface_ER60(:,3),'y')
% hold on 
% plot (Sa_surface_ER60(:,4),'g')
% hold on
% plot (Sa_surface_ER60(:,5),'c')
% hold on
% plot (moyenne_Sa_surface_ME70(:,1),'m','LineWidth',2)
% hold on
% plot (Sa_surface_ME70(:,11),'k','LineWidth',2)
% hold on
% legend('fr�quence 1','fr�quence 2','fr�quence 3','fr�quence 4','fr�quence 5','moyenne des faisceaux multi','faisceau central multi');
% set(gca,'xtick',1:1:nb_files)
% set(gca,'xticklabel',a)
% xticklabel_rotate
% saveas(gcf,['S:\etude et stages\stageSMFH2009\developpement\figures\',sprintf('comparaison_mono_multi_Sa_%g_scene_sardine_%g_%g',seuil,Masse,nbbanc)],'fig')
% 
% 
% 
% 
% figure;
% plot ((1:1:numel_profondeur),[mean(Sa_surface_ER60((1:numel),1)),...
%     mean(Sa_surface_ER60((numel+1:2*numel),1))],'-b')
% hold on
% plot ((1:1:numel_profondeur),[mean(Sa_surface_ER60((1:numel),2)),...
%     mean(Sa_surface_ER60((numel+1:2*numel),2))],'-r')
% hold on
% plot ((1:1:numel_profondeur),[mean(Sa_surface_ER60((1:numel),3)),...
%     mean(Sa_surface_ER60((numel+1:2*numel),3))],'-y')
% hold on
% plot ((1:1:numel_profondeur),[mean(Sa_surface_ER60((1:numel),4)),...
%     mean(Sa_surface_ER60((numel+1:2*numel),4))],'-g')
% hold on
% plot ((1:1:numel_profondeur),[mean(Sa_surface_ER60((1:numel),5)),...
%     mean(Sa_surface_ER60((numel+1:2*numel),5))],'-c')
% title('moyenne des SA par profondeur en monofaisceau')
% legend('fr�quence 1','fr�quence 2','fr�quence 3','fr�quence 4','fr�quence 5');
% set(gca,'xtick',1:1:numel_profondeur)
% set(gca,'xticklabel',b)
% saveas(gcf,['S:\etude et stages\stageSMFH2009\developpement\figures\',sprintf('moyenne_SA_par_profondeur_monofaisceau_%g_scene_sardine_%g_%g',seuil,Masse,nbbanc)],'fig')
% 
% 
% 
% figure
% plot ((1:1:numel_profondeur),[(mean(mean(Sa_surface_ME70((1:numel),:),2),1)),...
%                 (mean(mean(Sa_surface_ME70((numel+1:2*numel),:),2),1))],'-m')
% hold on
% plot ((1:1:numel_profondeur),[(mean(Sa_surface_ME70((1:numel),11),1)),...
%                 (mean(Sa_surface_ME70((numel+1:2*numel),11),1))],'-k')
% title('moyenne des SA par profondeur en multifaisceau')
% legend('moyenne des faisceaux','faisceau 11');
% set(gca,'xtick',1:1:numel_profondeur)
% set(gca,'xticklabel',b)
% saveas(gcf,['S:\etude et stages\stageSMFH2009\developpement\figures\',sprintf('moyenne_SA_par_profondeur_multifaisceau_%g_scene_sardine_%g_%g',seuil,Masse,nbbanc)],'fig')
% 
% 
% figure;
% plot ((1:1:numel_profondeur),[mean(Sa_surface_ER60((1:numel),1)),...
%     mean(Sa_surface_ER60((numel+1:2*numel),1))],'-b')
% hold on
% plot ((1:1:numel_profondeur),[mean(Sa_surface_ER60((1:numel),2)),...
%     mean(Sa_surface_ER60((numel+1:2*numel),2))],'-r')
% hold on
% plot ((1:1:numel_profondeur),[mean(Sa_surface_ER60((1:numel),3)),...
%     mean(Sa_surface_ER60((numel+1:2*numel),3))],'-y')
% hold on
% plot ((1:1:numel_profondeur),[mean(Sa_surface_ER60((1:numel),4)),...
%     mean(Sa_surface_ER60((numel+1:2*numel),4))],'-g')
% hold on
% plot ((1:1:numel_profondeur),[mean(Sa_surface_ER60((1:numel),5)),...
%     mean(Sa_surface_ER60((numel+1:2*numel),5))],'-c')
% hold on
% plot ((1:1:numel_profondeur),[(mean(mean(Sa_surface_ME70((1:numel),:),2),1)),...
%                 (mean(mean(Sa_surface_ME70((numel+1:2*numel),:),2),1))],'-m')
% hold on
% plot ((1:1:numel_profondeur),[(mean(Sa_surface_ME70((1:numel),11),1)),...
%                 (mean(Sa_surface_ME70((numel+1:2*numel),11),1))],'--k')
% 
% title('moyenne des SA par profondeur')
% legend('fr�quence 1','fr�quence 2','fr�quence 3','fr�quence 4','fr�quence 5','moyenne des faisceaux','faisceau 11');
% set(gca,'xtick',1:1:numel_profondeur)
% set(gca,'xticklabel',b)
% saveas(gcf,['S:\etude et stages\stageSMFH2009\developpement\figures\',sprintf('moyenne_SA_profondeur_mono_multi_%g_scene_sardine_%g_%g',seuil,Masse,nbbanc)],'fig')
% 
% 
% 
% figure;
% plot(Nmesuremono(:),'b')
% hold on
% plot(Nmesuremulti(:),'r')
% hold on
% plot(Nechmono(:),'--b')
% hold on
% plot(Nechmulti(:),'--r')
% title('nombre de poissons mesur�')
% legend('monofaisceau','multifaisceau','nb poissons simules mono','nb poissons simules multi')
% set(gca,'xtick',1:1:nb_files)
% set(gca,'xticklabel',a)
% xticklabel_rotate
% saveas(gcf,['S:\etude et stages\stageSMFH2009\developpement\figures\',sprintf('nb_moissons_mesures_mono_multi_%g_scene_sardine_%g_%g',seuil,Masse,nbbanc)],'fig')
% 
% 
% figure;
% plot(Nfaisceaumulti(:,:),'LineWidth',0.2)
% hold on 
% plot(Nfaisceaumulti(:,11),'g','LineWidth',3)
% hold on
% plot(mean(Nfaisceaumulti(:,:),2),'m','LineWidth',3)
% title('multifaisceaux nombre de poissons mesur� par chaque faisceau')
% legend('faisceau 1','faisceau 2','faisceau 3','faisceau 4','faisceau 5','faisceau 6','faisceau 7',...
%         'faisceau 8','faisceau 9','faisceau 10','faisceau 11','faisceau 12','faisceau 13','faisceau 14',...
%         'faisceau 15','faisceau 16','faisceau 17','faisceau 18','faisceau 19','faisceau 20','faisceau 21','faisceau 11')
% set(gca,'xtick',1:1:nb_files)
% set(gca,'xticklabel',a)
% xticklabel_rotate
% saveas(gcf,['S:\etude et stages\stageSMFH2009\developpement\figures\',sprintf('multi_nb_poissons_mesure_21faisceaux_%g_scene_sardine_%g_%g',seuil,Masse,nbbanc)],'fig')
% 
% 
% figure;
% plot(Nechmono(:),'b')
% hold on
% plot(Nechmulti(:),'r')
% title('nombre de poissons �chantillon�s')
% legend('mono','multi')
% set(gca,'xtick',1:1:nb_files)
% set(gca,'xticklabel',a)
% xticklabel_rotate
% saveas(gcf,['S:\etude et stages\stageSMFH2009\developpement\figures\',sprintf('nb_poissons_�chantillon�s_mono_multi_%g_scene_sardine_%g_%g',seuil,Masse,nbbanc)],'fig')
% 



% figure;
% plot ((1:1:numel_profondeur),[mean(Sa_surface_ER60((1:numel),1)),...
%     mean(Sa_surface_ER60((numel+1:2*numel),1)),...
%     mean(Sa_surface_ER60((2*numel+1:3*numel),1)),...
%     mean(Sa_surface_ER60((3*numel+1:4*numel),1)),...
%     mean(Sa_surface_ER60((4*numel+1:5*numel),1))],'-b')
% hold on
% plot ((1:1:numel_profondeur),[mean(Sa_surface_ER60((1:numel),2)),...
%     mean(Sa_surface_ER60((numel+1:2*numel),2)),...
%     mean(Sa_surface_ER60((2*numel+1:3*numel),2)),...
%     mean(Sa_surface_ER60((3*numel+1:4*numel),2)),...
%     mean(Sa_surface_ER60((4*numel+1:5*numel),2))],'-r')
% hold on
% plot ((1:1:numel_profondeur),[mean(Sa_surface_ER60((1:numel),3)),...
%     mean(Sa_surface_ER60((numel+1:2*numel),3)),...
%     mean(Sa_surface_ER60((2*numel+1:3*numel),3)),...
%     mean(Sa_surface_ER60((3*numel+1:4*numel),3)),...
%     mean(Sa_surface_ER60((4*numel+1:5*numel),3))],'-y')
% hold on
% plot ((1:1:numel_profondeur),[mean(Sa_surface_ER60((1:numel),4)),...
%     mean(Sa_surface_ER60((numel+1:2*numel),4)),...
%     mean(Sa_surface_ER60((2*numel+1:3*numel),4)),...
%     mean(Sa_surface_ER60((3*numel+1:4*numel),4)),...
%     mean(Sa_surface_ER60((4*numel+1:5*numel),4))],'-g')
% hold on
% plot ((1:1:numel_profondeur),[mean(Sa_surface_ER60((1:numel),5)),...
%     mean(Sa_surface_ER60((numel+1:2*numel),5)),...
%     mean(Sa_surface_ER60((2*numel+1:3*numel),5)),...
%     mean(Sa_surface_ER60((3*numel+1:4*numel),5)),...
%     mean(Sa_surface_ER60((4*numel+1:5*numel),5))],'-c')
% title('moyenne des SA par profondeur en monofaisceau')
% legend('fr�quence 1','fr�quence 2','fr�quence 3','fr�quence 4','fr�quence 5');
% set(gca,'xtick',1:1:numel_profondeur)
% set(gca,'xticklabel',b)
% saveas(gcf,['S:\etude et stages\stageSMFH2009\developpement\figures\',sprintf('moyenne_SA_par_profondeur_monofaisceau_%g_scene_sardine_%g_%g',seuil,Masse,nbbanc)],'fig')
% 
% 
% 
% figure
% plot ((1:1:numel_profondeur),[(mean(mean(Sa_surface_ME70((1:numel),:),2),1)),...
%                 (mean(mean(Sa_surface_ME70((numel+1:2*numel),:),2),1)),...
%                 (mean(mean(Sa_surface_ME70((2*numel+1:3*numel),:),2),1)),...
%                 (mean(mean(Sa_surface_ME70((3*numel+1:4*numel),:),2),1)),...
%                 (mean(mean(Sa_surface_ME70((4*numel+1:5*numel),:),2),1))],'-m')
% hold on
% plot ((1:1:numel_profondeur),[(mean(Sa_surface_ME70((1:numel),11),1)),...
%                 (mean(Sa_surface_ME70((numel+1:2*numel),11),1)),...
%                 (mean(Sa_surface_ME70((2*numel+1:3*numel),11),1)),...
%                 (mean(Sa_surface_ME70((3*numel+1:4*numel),11),1)),...
%                 (mean(Sa_surface_ME70((4*numel+1:5*numel),11),1))],'-k')
% title('moyenne des SA par profondeur en multifaisceau')
% legend('moyenne des faisceaux','faisceau 11');
% set(gca,'xtick',1:1:numel_profondeur)
% set(gca,'xticklabel',b)
% saveas(gcf,['S:\etude et stages\stageSMFH2009\developpement\figures\',sprintf('moyenne_SA_par_profondeur_multifaisceau_%g_scene_sardine_%g_%g',seuil,Masse,nbbanc)],'fig')
% 
% 
% figure;
% plot ((1:1:numel_profondeur),[mean(Sa_surface_ER60((1:numel),1)),...
%     mean(Sa_surface_ER60((numel+1:2*numel),1)),...
%     mean(Sa_surface_ER60((2*numel+1:3*numel),1)),...
%     mean(Sa_surface_ER60((3*numel+1:4*numel),1)),...
%     mean(Sa_surface_ER60((4*numel+1:5*numel),1))],'-b')
% hold on
% plot ((1:1:numel_profondeur),[mean(Sa_surface_ER60((1:numel),2)),...
%     mean(Sa_surface_ER60((numel+1:2*numel),2)),...
%     mean(Sa_surface_ER60((2*numel+1:3*numel),2)),...
%     mean(Sa_surface_ER60((3*numel+1:4*numel),2)),...
%     mean(Sa_surface_ER60((4*numel+1:5*numel),2))],'-r')
% hold on
% plot ((1:1:numel_profondeur),[mean(Sa_surface_ER60((1:numel),3)),...
%     mean(Sa_surface_ER60((numel+1:2*numel),3)),...
%     mean(Sa_surface_ER60((2*numel+1:3*numel),3)),...
%     mean(Sa_surface_ER60((3*numel+1:4*numel),3)),...
%     mean(Sa_surface_ER60((4*numel+1:5*numel),3))],'-y')
% hold on
% plot ((1:1:numel_profondeur),[mean(Sa_surface_ER60((1:numel),4)),...
%     mean(Sa_surface_ER60((numel+1:2*numel),4)),...
%     mean(Sa_surface_ER60((2*numel+1:3*numel),4)),...
%     mean(Sa_surface_ER60((3*numel+1:4*numel),4)),...
%     mean(Sa_surface_ER60((4*numel+1:5*numel),4))],'-g')
% hold on
% plot ((1:1:numel_profondeur),[mean(Sa_surface_ER60((1:numel),5)),...
%     mean(Sa_surface_ER60((numel+1:2*numel),5)),...
%     mean(Sa_surface_ER60((2*numel+1:3*numel),5)),...
%     mean(Sa_surface_ER60((3*numel+1:4*numel),5)),...
%     mean(Sa_surface_ER60((4*numel+1:5*numel),5))],'-c')
% hold on
% plot ((1:1:numel_profondeur),[(mean(mean(Sa_surface_ME70((1:numel),:),2),1)),...
%                 (mean(mean(Sa_surface_ME70((numel+1:2*numel),:),2),1)),...
%                 (mean(mean(Sa_surface_ME70((2*numel+1:3*numel),:),2),1)),...
%                 (mean(mean(Sa_surface_ME70((3*numel+1:4*numel),:),2),1)),...
%                 (mean(mean(Sa_surface_ME70((4*numel+1:5*numel),:),2),1))],'-m')
% hold on
% plot ((1:1:numel_profondeur),[(mean(Sa_surface_ME70((1:numel),11),1)),...
%                 (mean(Sa_surface_ME70((numel+1:2*numel),11),1)),...
%                 (mean(Sa_surface_ME70((2*numel+1:3*numel),11),1)),...
%                 (mean(Sa_surface_ME70((3*numel+1:4*numel),11),1)),...
%                 (mean(Sa_surface_ME70((4*numel+1:5*numel),11),1))],'--k')
% 
% title('moyenne des SA par profondeur')
% legend('fr�quence 1','fr�quence 2','fr�quence 3','fr�quence 4','fr�quence 5','moyenne des faisceaux','faisceau 11');
% set(gca,'xtick',1:1:numel_profondeur)
% set(gca,'xticklabel',b)
% saveas(gcf,['S:\etude et stages\stageSMFH2009\developpement\figures\',sprintf('moyenne_SA_profondeur_mono_multi_%g_scene_sardine_%g_%g',seuil,Masse,nbbanc)],'fig')
