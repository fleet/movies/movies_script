
clear all;
% close all;

dirdata='C:\data\BASIN\HAC\Partie 2\';

%chargement config
chemin_config = dirdata;
chemin_config_reverse = strrep(chemin_config, '\', '/');
moLoadConfig(chemin_config_reverse);
%activation EI
EchoIntegrModule = moEchoIntegration();
moSetEchoIntegrationEnabled(EchoIntegrModule);

SampleEchoIntegration(dirdata,dirdata,chemin_config,EchoIntegrModule);


filelist = ls([dirdata,'*.mat']);  % ensemble des fichiers
nb_files = size(filelist,1);  % nombre de fichiers
sort(filelist);

nblME70=0;
nblER60=0;
 for numfile = 1:nb_files

        matfilename = filelist(numfile,:)

        FileName = [dirdata,matfilename(1:findstr('.',matfilename)-1),'.mat'];
        load(FileName);

%         if  size(time_ME70,1)>0
% 
%             timeME70(nblME70+1:nblME70+size(time_ME70,2)) = time_ME70;
% 
%             depth_bottom_ME70(nblME70+1:nblME70+size(Depth_botME70,1),:,:) = Depth_botME70(:,:,:);
% 
% %             Sa_bottom_ME70(nblME70+1:nblME70+size(Sv_botME70,1),:,:) = Sa_botME70(:,:,:);
%              Sa_surface_ME70(nblME70+1:nblME70+size(Sv_botME70,1),:,:) = Sa_surfME70(:,:,:);
% 
%             latME70(nblME70+1:nblME70+size(Sv_botME70,1),:,:) = Lat_botME70(:,:,:);
%             longME70(nblME70+1:nblME70+size(Sv_botME70,1),:,:) = Long_botME70(:,:,:);
% 
%             nblME70=size(depth_bottom_ME70,1);
%         end

        if  size(time_ER60,1)>0

            timeER60(nblER60+1:nblER60+size(time_ER60,2)) = time_ER60;

            depth_surface_ER60(nblER60+1:nblER60+size(Depth_surfER60,1),:,:) = Depth_surfER60(:,:,:);
           
            depth(nblER60+1:nblER60+size(Depth_botER60,1),1,:) = Depth_botER60(:,1,:);

%             Sa_bottom_ER60(nblER60+1:nblER60+size(Sv_botER60,1),:,:) = Sa_botER60(:,:,:);
             Sa_surface_ER60(nblER60+1:nblER60+size(Sa_surfER60,1),:,:) = Sa_surfER60(:,:,:);
           Sv_surface_ER60(nblER60+1:nblER60+size(Sv_surfER60,1),:,:) = Sv_surfER60(:,:,:);

            latER60(nblER60+1:nblER60+size(Lat_surfER60,1),:,:) = Lat_surfER60(:,:,:);
            longER60(nblER60+1:nblER60+size(Lat_surfER60,1),:,:) = Long_surfER60(:,:,:);

            nblER60=size(Sa_surface_ER60,1);
        end
 end
 
%  nblayers = size(Sa_bottom_ME70,2);
%  
%  for i=1:5
%  figure;
%  imagesc(log(squeeze(Sa_bottom_ME70(:,i,:))));
%  
%  figure;
%  geoshow(squeeze(latME70(:,i,:)),squeeze(longME70(:,i,:)),log(squeeze(Sa_bottom_ME70(:,i,:))),'DisplayType','surface');
%   axis([27.7 27.9 40.8 40.9]);
%  end
%  
 nblayers = size(Sa_surface_ER60,2);
  nbfreq=size(Sa_surface_ER60,3);
  
   for freq=1:nbfreq
    figure;
 plot(timeER60/86400 +719529,100*(Sv_surface_ER60(:,1,freq)/mean(Sv_surface_ER60(:,1,freq))-1));
 datetickzoom;
 ylabel('Normalized sA relative to mean value (%)')
xlabel('Temps (HH:MM)')
   end
   
      for freq=1:nbfreq
    figure;
 plot(timeER60/86400 +719529,((Sv_surface_ER60(:,1,freq))));
 datetickzoom;
  ylabel('Sv (dB)')
xlabel('Temps (HH:MM)')
 title('Sv moyen sur les quatre premiers �chantillons et pour 20 pings');
      end
   
%       depth(depth==2.147483147000000e+06)=NaN;
%       
%         figure;
%   plot(timeER60/86400 +719529,depth');
%  datetickzoom;
%    
%      for freq=1:nbfreq
%     figure;
%  plot(timeER60/86400 +719529,depth(:,1,freq));
%  datetickzoom;
%    end
 
  landareas = shaperead('landareas.shp','UseGeoCoords', true);
%   bathy=shaperead('IOC_BATHY_GEBCO2003_A','UseGeoCoords', true);
  
  debutlayerutiles=7;
  finlayerutiles=16;
  
    
  index=find((timeER60/86400 +719529>datenum(2011,1,17) & timeER60/86400 +719529<datenum(2011,1,30)) | (timeER60/86400 +719529>datenum(2011,2,26) & timeER60/86400 +719529<datenum(2011,2,28)) | (timeER60/86400 +719529>datenum(2011,3,3)) & max(Sv_surface_ER60(:,2:end)')<-50 );
  index=find(max(Sv_surface_ER60(:,2:end),[],2)<-50 & depth>600);
  
  
 for freq=1:nbfreq
     
       depth(depth==2.147483147000000e+06)=NaN;
   figure;
 geoshow(landareas);
%  geoshow(bathy);
 hold on;
 scatter(mean(longER60(index,debutlayerutiles:finlayerutiles),2),mean(latER60(index,debutlayerutiles:finlayerutiles),2),20,depth(index));
  text(mean(longER60(index(1:100:end),debutlayerutiles:finlayerutiles,freq),2),mean(latER60(index(1:100:end),debutlayerutiles:finlayerutiles,freq),2),datestr(timeER60(index(1:100:end))/86400 +719529,'dd/mm'),'FontSize',6,'FontWeight','bold');
axis([min(longER60(index,1))-1 max(longER60(index,1))+1 min(latER60(index,1))-1 max(latER60(index,1))+1]);
title('Sonde moyennne','FontSize',16);
colorbar;


 figure;
 geoshow(landareas);
%  geoshow(bathy);
 hold on;
 scatter(mean(longER60(index,debutlayerutiles:finlayerutiles),2),mean(latER60(index,debutlayerutiles:finlayerutiles),2),20,10*log10(mean(10.^(Sv_surface_ER60(index,debutlayerutiles:finlayerutiles)./10),2)));
  text(mean(longER60(index(1:100:end),debutlayerutiles:finlayerutiles,freq),2),mean(latER60(index(1:100:end),debutlayerutiles:finlayerutiles,freq),2),datestr(timeER60(index(1:100:end))/86400 +719529,'dd/mm'),'FontSize',6,'FontWeight','bold');
axis([min(longER60(index,1))-1 max(longER60(index,1))+1 min(latER60(index,1))-1 max(latER60(index,1))+1]);
title('Sv moyen de 200 m � 700m','FontSize',16);
colorbar;

 figure;
 boxplot((Sv_surface_ER60(index,debutlayerutiles:finlayerutiles,freq)));
  set(gca,'XTick',1:10)
 set(gca,'XTickLabel',{'200-250' ;'250-300'; '300-350'; '350-400' ;'400-450' ;'450-500'; '500-550'; '550-600'; '600-650' ;'650-700'});
  set(get(gca,'XTickLabel'),'Rotation',90.0)
  title('Sv par couches de profondeur','FontSize',16);
  legend;
  

  

 end
%  
% %  
% %  for i=1:nblayers
% % %  figure;
% % %  imagesc(log(squeeze(Sa_surface_ER60(:,i,:))));
% %  
% %  figure;
% %  geoshow(landareas);
% %  hold on;
% %  scatter(squeeze(longER60(:,i,:)),squeeze(latER60(:,i,:)),(squeeze(Sa_surface_ER60(:,i,:))),log(squeeze(Sa_surface_ER60(:,i,:))));
% % axis([-20 -5 50 60]);
% %  end
%  
%  