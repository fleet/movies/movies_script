% This program computes echo-integration for raw HAC files using the MOVIES3D libraries

%Input parameters
%   chemin_ini       : path for HAC files 
%   chemin_save      : path for .mat result files for echo-integration
%   EchoIntegrModule : MOVIES3D object
%   nameTransect     : name of the transect for prefixing result files
%   dateStart        : start date for transect format DD/MM/YYY
%   timeStart        : start time for transect format HH:MM:SS
%   dateEnd          : end date for transect format DD/MM/YYY
%   timeEnd          : end time for transect format HH:MM:SS

%LB 02/04/2013 ajout r�cup�ration sondeur horizontal
%LB 07/05/2013 ajout goto
%LB 11/12/2013 ajout arret EI si l'heure de fin de radiale est d�pass�e
%LB 30/01/2014 quand il n'y a que trois arguments la date de fin d'EI est
%maintenant, on int�gre tout

function SampleEchoIntegration(chemin_ini,chemin_save,EchoIntegrModule,nameTransect,dateStart,timeStart,dateEnd,timeEnd)




    if nargin>4

        %	 date='22/02/2013'	
        %    time='15:19:00'
        goto=str2num(timeStart(:,1:2))*3600+str2num(timeStart(4:5))*60+str2num(timeStart(7:8))+(datenum(dateStart,'dd/mm/yyyy')-datenum('01-Jan-1970'))*24*3600;
        %         time4human(goto)
        
        ParameterR= moReaderParameter();
        ParameterDef=moLoadReaderParameter(ParameterR);
        taille_chunk=ParameterDef.m_chunkNumberOfPingFan;
        ParameterDef.m_chunkNumberOfPingFan=2;
        moSaveReaderParameter(ParameterR,ParameterDef);
        

         % et lecture du premier ping du r�pertoire
        moOpenHac(chemin_ini);

        %goto
        moGoTo(goto);
        moReadChunk(); %lit un ping suppl�mentaire
        
        %vide la m�moire des pings pr�c�dents
        nb_pings=moGetNumberOfPingFan();
        for ip=nb_pings-1:-1:1
            MX= moGetPingFan(ip-1);
            moRemovePing(MX.m_pingId);
        end
        
        ParameterDef.m_chunkNumberOfPingFan=taille_chunk;
        moSaveReaderParameter(ParameterR,ParameterDef);
        
        %vide la m�moire des ESU pr�c�dents
        AllData=moGetOutPutList(EchoIntegrModule);
        clear AllData;
        timeEndEI=datenum([dateEnd,' ',timeEnd],'dd/mm/yyyy HH:MM:SS');
        str_Name=['_',nameTransect,'_'];
    else
          % chargement de tous les fichiers HAC contenus dans le repertoire
         % et lecture du premier chunk
        moOpenHac(chemin_ini);
        timeEndEI=now;
        str_Name='';
        
    end
    
    num_bloc=1;
    

    FileStatus= moGetFileStatus;
    while FileStatus.m_StreamClosed < 1

        % init matrices
        time_EK80 = [];
        time_ME70 = [];
        Sa_surfME70 =[];
        Sa_botME70=[];
        Sa_surfEK80=[];
        Ni_surfEK80=[];
        Nt_surfEK80=[];
        Sa_botEK80=[];
        Sv_surfME70=[];
        Ni_surfME70=[];
        Nt_surfME70=[];
        Sv_botME70=[];
        Sv_surfEK80=[];
        Sv_botEK80=[];
        Lat_surfME70=[];
        Long_surfME70=[];
        Depth_surfME70=[];
        Lat_surfEK80=[];
        Long_surfEK80=[];
        Depth_surfEK80=[];
        Lat_botME70=[];
        Long_botME70=[];
        Depth_botME70=[];
        Lat_botEK80=[];
        Long_botEK80=[];
        Depth_botEK80=[];
        Volume_surfME70=[];
        Volume_surfEK80=[];
        Volume_botME70=[];
        Volume_botEK80=[];
        
        time_EK80h = [];
        Sa_surfEK80h=[];
        Sv_surfEK80h=[];
        Lat_surfEK80h=[];
        Long_surfEK80h=[];
        Depth_surfEK80h=[];
        Volume_surfEK80h=[];
        AllData=[];
        
        frequenciesEK80=[];
        frequenciesEK80h=[];
        frequenciesME70=[];
        


        moReadChunk();


        AllData=moGetOutPutList(EchoIntegrModule);

        % r�cup�ration de la d�finition des ESU
        ESUParam = moGetESUDefinition();

        % r�cup�ration de la d�finition des couches
        LayersDef = moGetLayersDefinition(EchoIntegrModule);
        
        % r�cup�ration des volumes de chaque couche (pour 1 ping)
        LayersVolumes = moGetLayersVolumes(EchoIntegrModule);
        sounderNb = size(LayersVolumes(1).Sounders,2);
        
        % on souhaite r�cuperer les informations ME70 et EK60 MFR dans le
        % cas o� l'on a �galement un sondeur horizontal
        indexSounderEK80=1;
        indexSounderEK80h=1;
        indexSounderME70=1;
        nbchannelsME70=0;
        nbchannelsEK80=1;
        nbchannelsEK80h=0;
        for indexSounder=1:sounderNb
            if size(LayersVolumes(1).Sounders(indexSounder).Channels,2) >6 %ME70
                indexSounderME70=indexSounder;
                nbchannelsME70=size(LayersVolumes(1).Sounders(indexSounder).Channels,2);
            elseif size(LayersVolumes(1).Sounders(indexSounder).Channels,2) >1 %EK80 MFR
                indexSounderEK80=indexSounder;
                nbchannelsEK80=size(LayersVolumes(1).Sounders(indexSounder).Channels,2);
                
            elseif size(LayersVolumes(1).Sounders(indexSounder).Channels,2) == 1 && nbchannelsEK80>1 %cas du sondeur horizontal de Thalassa avec un sondeur MFR avant
                indexSounderEK80h=indexSounder;
                nbchannelsEK80h=size(LayersVolumes(1).Sounders(indexSounder).Channels,2);
            end
        end
        
        %on r�cup�re l'ordre des r�sultats des fr�quences EK80 pour les
        %sauvegarder touojours dans le m�me ordre
        % on suppose qu'il y a un seul sondeur multi-transducteur !
        list_sounder=moGetSounderList;
        nb_snd=length(list_sounder);
        indexEK80=1;
        list_freqEK80=zeros(nbchannelsEK80,1);
        for isdr = 1:nb_snd
            nb_transduc=list_sounder(isdr).m_numberOfTransducer;
            if (nb_transduc>1)
                for itr=1:nb_transduc
                    list_freqEK80(itr)=list_sounder(isdr).m_transducer(itr).m_SoftChannel.m_acousticFrequency;
                end
                %[~,indexEK80]=sort(list_freqEK80);
                [B,IX]=sort(list_freqEK80);
                indexEK80=IX;
            end
        end    



        indexESUME70 = 1;
        indexESUEK80 = 1;
        indexESUEK80h = 1;        
        %nb d'ESUs traitees
        nbResults = size(AllData,2);
        for indexResults=1:nbResults
            nbbeams=size(AllData(1,indexResults).m_Sa,2);
            timeESU= AllData(1,indexResults).m_time/86400 +719529;
           
            if timeESU>timeEndEI
                return;
            end
            
            if nbbeams==nbchannelsME70

                time_ME70(indexESUME70) = AllData(1,indexResults).m_time;
                
                for ibeam=1:nbbeams
                    indexLayerSurface = 1;
                    indexLayerBottom = 1;
                    frequenciesME70{ibeam}(indexESUME70,:)=cell2mat(AllData(1,indexResults).m_freq(ibeam));
                    SaTemp=cell2mat(AllData(1,indexResults).m_Sa(ibeam));
%                     NiTemp=cell2mat(AllData(1,indexResults).m_Ni(ibeam));
%                     NtTemp=cell2mat(AllData(1,indexResults).m_Nt(ibeam));
                    SvTemp=cell2mat(AllData(1,indexResults).m_Sv(ibeam));
                         latTemp=cell2mat(AllData(1,indexResults).m_latitude(ibeam));
                     longTemp=cell2mat(AllData(1,indexResults).m_longitude(ibeam));
                      depthTemp=cell2mat(AllData(1,indexResults).m_Depth(ibeam));
                    nbLayer= size(SvTemp,1);
                
                    for indexLayer=1:nbLayer
                        if LayersDef.LayerType(indexLayer)==0
                            
                            Sa_surfME70{ibeam}(indexESUME70,indexLayerSurface,:)=max(0,SaTemp(indexLayer,:));
%                             Ni_surfME70{ibeam}(indexESUME70,indexLayerSurface,:)=max(0,NiTemp(indexLayer,:));
%                             Nt_surfME70{ibeam}(indexESUME70,indexLayerSurface,:)=max(0,NtTemp(indexLayer,:));
                            Sv_surfME70{ibeam}(indexESUME70,indexLayerSurface,:)=max(-100,SvTemp(indexLayer,:));
                            Lat_surfME70{ibeam}(indexESUME70,indexLayerSurface)=latTemp(indexLayer);
                            Long_surfME70{ibeam}(indexESUME70,indexLayerSurface)=longTemp(indexLayer);
                            Depth_surfME70{ibeam}(indexESUME70,indexLayerSurface)=depthTemp(indexLayer);
                            Volume_surfME70{ibeam}(indexESUME70,indexLayerSurface)=LayersVolumes(indexLayer).Sounders(indexSounderME70).Channels(ibeam);
                            indexLayerSurface = indexLayerSurface+1;
                        else
                            Sa_botME70{ibeam}(indexESUME70,indexLayerBottom,:)=max(0,SaTemp(indexLayer,:));
%                             Ni_botME70{ibeam}(indexESUME70,indexLayerBottom,:)=max(0,NiTemp(indexLayer,:));
%                             Nt_botME70{ibeam}(indexESUME70,indexLayerBottom,:)=max(0,NtTemp(indexLayer,:));
                            Sv_botME70{ibeam}(indexESUME70,indexLayerBottom,:)=max(-100,SvTemp(indexLayer,:));
                            Lat_botME70{ibeam}(indexESUME70,indexLayerBottom)=latTemp(indexLayer);
                            Long_botME70{ibeam}(indexESUME70,indexLayerBottom)=longTemp(indexLayer);
                            Depth_botME70{ibeam}(indexESUME70,indexLayerBottom)=depthTemp(indexLayer);
                            Volume_botME70{ibeam}(indexESUME70,indexLayerBottom)=LayersVolumes(indexLayer).Sounders(indexSounderME70).Channels(ibeam);
                            indexLayerBottom = indexLayerBottom+1;
                        end;
                    end
                end;

                indexESUME70=indexESUME70+1;
            elseif nbbeams==nbchannelsEK80
                time_EK80(indexESUEK80) = AllData(1,indexResults).m_time;
                
                for ibeam=1:nbbeams
                    indexLayerSurface = 1;
                    indexLayerBottom = 1;
                    frequenciesEK80{ibeam}(indexESUEK80,:)=cell2mat(AllData(1,indexResults).m_freq(indexEK80(ibeam)));
                    SaTemp=cell2mat(AllData(1,indexResults).m_Sa(indexEK80(ibeam)));
%                     NiTemp=cell2mat(AllData(1,indexResults).m_Ni(indexEK80(ibeam)));
%                     NtTemp=cell2mat(AllData(1,indexResults).m_Nt(indexEK80(ibeam)));
                    SvTemp=cell2mat(AllData(1,indexResults).m_Sv(indexEK80(ibeam)));
                    latTemp=cell2mat(AllData(1,indexResults).m_latitude(indexEK80(ibeam)));
                     longTemp=cell2mat(AllData(1,indexResults).m_longitude(indexEK80(ibeam)));
                      depthTemp=cell2mat(AllData(1,indexResults).m_Depth(indexEK80(ibeam)));
                    nbLayer= size(SvTemp,1);
                
                    for indexLayer=1:nbLayer
                        if LayersDef.LayerType(indexLayer)==0
                            
                            Sa_surfEK80{ibeam}(indexESUEK80,indexLayerSurface,:)=max(0,SaTemp(indexLayer,:));
%                             Ni_surfEK80{ibeam}(indexESUEK80,indexLayerSurface,:)=max(0,NiTemp(indexLayer,:));
%                             Nt_surfEK80{ibeam}(indexESUEK80,indexLayerSurface,:)=max(0,NtTemp(indexLayer,:));
                            Sv_surfEK80{ibeam}(indexESUEK80,indexLayerSurface,:)=max(-100,SvTemp(indexLayer,:));
                            Lat_surfEK80{ibeam}(indexESUEK80,indexLayerSurface)=latTemp(indexLayer);
                            Long_surfEK80{ibeam}(indexESUEK80,indexLayerSurface)=longTemp(indexLayer);
                            Depth_surfEK80{ibeam}(indexESUEK80,indexLayerSurface)=depthTemp(indexLayer);
                            Volume_surfEK80{ibeam}(indexESUEK80,indexLayerSurface)=LayersVolumes(indexLayer).Sounders(indexSounderEK80).Channels(indexEK80(ibeam));
                            indexLayerSurface = indexLayerSurface+1;
                        else
                            Sa_botEK80{ibeam}(indexESUEK80,indexLayerBottom,:)=max(0,SaTemp(indexLayer,:));
%                             Ni_botEK80{ibeam}(indexESUEK80,indexLayerBottom,:)=max(0,NiTemp(indexLayer,:));
%                             Nt_botEK80{ibeam}(indexESUEK80,indexLayerBottom,:)=max(0,NtTemp(indexLayer,:));
                            Sv_botEK80{ibeam}(indexESUEK80,indexLayerBottom,:)=max(-100,SvTemp(indexLayer,:));
                            Lat_botEK80{ibeam}(indexESUEK80,indexLayerBottom)=latTemp(indexLayer);
                            Long_botEK80{ibeam}(indexESUEK80,indexLayerBottom)=longTemp(indexLayer);
                            Depth_botEK80{ibeam}(indexESUEK80,indexLayerBottom)=depthTemp(indexLayer);
                            Volume_botEK80{ibeam}(indexESUEK80,indexLayerBottom)=LayersVolumes(indexLayer).Sounders(indexSounderEK80).Channels(indexEK80(ibeam));
                            indexLayerBottom = indexLayerBottom+1;
                        end;
                    end
                end;
                
                indexESUEK80=indexESUEK80+1;
            elseif nbbeams==nbchannelsEK80h
                time_EK80h(indexESUEK80h) = AllData(1,indexResults).m_time;
                    
                for ibeam=1:nbbeams
                    indexLayerSurface = 1;
                    frequenciesEK80h{ibeam}(indexESUEK80,:)=cell2mat(AllData(1,indexResults).m_freq(ibeam));
%                     SaTemp=cell2mat(AllData(1,indexResults).m_Sa(ibeam);
%                     NiTemp=cell2mat(AllData(1,indexResults).m_Ni(ibeam));
%                     NtTemp=cell2mat(AllData(1,indexResults).m_Nt(ibeam));
                    SvTemp=cell2mat(AllData(1,indexResults).m_Sv(ibeam));
                     latTemp=cell2mat(AllData(1,indexResults).m_latitude(ibeam));
                     longTemp=cell2mat(AllData(1,indexResults).m_longitude(ibeam));
                      depthTemp=cell2mat(AllData(1,indexResults).m_Depth(ibeam));
                    nbLayer= size(SvTemp,1);
                
                    for indexLayer=1:nbLayer
                        if LayersDef.LayerType(indexLayer)==2
                            
%                             Sa_surfEK80{ibeam}(indexESUEK80,indexLayerSurface,:)=max(0,SaTemp(indexLayer,:));
%                             Ni_surfEK80{ibeam}(indexESUEK80,indexLayerSurface,:)=max(0,NiTemp(indexLayer,:));
%                             Nt_surfEK80{ibeam}(indexESUEK80,indexLayerSurface,:)=max(0,NtTemp(indexLayer,:));
                            Sv_surfEK80h{ibeam}(indexESUEK80,indexLayerSurface,:)=max(-100,SvTemp(indexLayer,:));
                            Lat_surfEK80h{ibeam}(indexESUEK80,indexLayerSurface)=latTemp(indexLayer);
                            Long_surfEK80h{ibeam}(indexESUEK80,indexLayerSurface)=longTemp(indexLayer);
                            Depth_surfEK80h{ibeam}(indexESUEK80,indexLayerSurface)=depthTemp(indexLayer);
                            Volume_surfEK80h{ibeam}(indexESUEK80,indexLayerSurface)=LayersVolumes(indexLayer).Sounders(indexSounderEK80).Channels(ibeam);
                            indexLayerSurface = indexLayerSurface+1;
                        end;
                    end
                end;
                indexESUEK80h=indexESUEK80h+1;
            end
            
        end
        
        %Save results
        if ~exist(chemin_save,'dir')
            mkdir(chemin_save)
        end 
        str_bloc='0000';
        str_bloc(end-length(num2str(num_bloc))+1:end)=num2str(num_bloc);
        save ([chemin_save, '/results','_',str_Name, str_bloc,'_EI'],'time_EK80','time_EK80h','time_ME70','Sa_surfME70', 'Sa_botME70', 'Sa_surfEK80','Ni_surfEK80','Nt_surfEK80', 'Sa_botEK80', 'Sa_surfEK80h','Sv_surfME70', 'Ni_surfME70','Nt_surfME70','Sv_botME70', 'Sv_surfEK80', 'Sv_botEK80', 'Sv_surfEK80h','Lat_surfME70', 'Long_surfME70', 'Depth_surfME70','Lat_surfEK80','Long_surfEK80','Depth_surfEK80','Lat_surfEK80h','Long_surfEK80h','Depth_surfEK80h','Lat_botME70', 'Long_botME70', 'Depth_botME70','Lat_botEK80','Long_botEK80','Depth_botEK80','Volume_surfEK80','Volume_botEK80','Volume_surfEK80h','Volume_surfME70','Volume_botME70','frequenciesEK80','frequenciesEK80h','frequenciesME70') ;

        clear time_EK80 time_EK80h time_ME70 Sa_surfME70 Sa_botME70 Sa_surfEK80 Ni_surfEK80 Nt_surfEK80 Sa_botEK80 Sa_surfEK80h  Sv_surfME70 Ni_surfME70 Nt_surfME70 Sv_botME70 Sv_surfEK80 Sv_botEK80  Sv_surfEK80h Lat_surfME70 Long_surfME70 Depth_surfME70 Lat_surfEK80 Long_surfEK80 Depth_surfEK80 Lat_surfEK80h Long_surfEK80h Depth_surfEK80h Lat_botME70 Long_botME70 Depth_botME70 Lat_botEK80 Long_botEK80 Depth_botEK80 frequenciesEK80 frequenciesEK80h frequenciesME70
        clear AllData;
        clear Volume_surfEK80 Volume_botEK80 Volume_surfEK80h  Volume_surfME70 Volume_botME70;

        num_bloc=num_bloc+1;
        FileStatus= moGetFileStatus();
        

    end    
    end

