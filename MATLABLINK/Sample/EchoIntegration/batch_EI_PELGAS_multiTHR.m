% This program computes echo-integration for raw HAC files using the MOVIES3D libraries

% The HAC files are stored in folders named RUNXXX for each day of the
% survey as stored by MOVIES+ or HERMES
% The configuration for reading HAC files, filtering, and defining the cell
% size for integration and threshold are located in a dedicated folder with
% one sub folder for each threshold.

%Input parameters
%   path_hac_survey : path for HAC files 
%   path_config     : path for xml configuration files created with MOVIES3D
%   path_result     : path for .mat result files for echo-integration
%   thresholds      : list of thresolds for echo-integration,
%                   corresponding configuration have been created before
%   runs            : list of runs for echo-integration

%Modification
%   LB 02/04/2013 ajout récupération sondeur horizontal

function batch_EI_multiTHR(path_hac_survey,path_config,path_save,thresholds,runs,nameTransect,dateStart,timeStart,dateEnd,timeEnd)

for ir=1:length(runs)
    for t=1:size(thresholds,2)
        %build dedicated string
        str_run(ir,:)='000';
        str_run(ir,end-length(num2str(runs(ir)))+1:end)=num2str(runs(ir));
        chemin_config=[path_config,'/',num2str(thresholds(:,t))];
        if (~exist(chemin_config))
           error([chemin_config,' : Invalid path for MOVIES3D configuration'])
        end
        
        chemin_hac=[path_hac_survey,'/RUN' str_run(ir,:)];
        if (~exist(chemin_hac))
            error([chemin_hac,' : Invalid path for HAC configuration'])
        else
            filelist = ls([chemin_hac,'/*.hac']);  % ensemble des fichiers hac
            nb_files = size(filelist,1);  % nombre de fichiers
            if nb_files==0
                 error([chemin_hac,' does not contain HAC files'])
            end
        end
        
        chemin_save=[path_save,'/RUN' str_run(ir,:) '/',num2str(thresholds(:,t))];
        
        %load configuration
        chemin_config_reverse = strrep(chemin_config, '\', '/');
        moLoadConfig(chemin_config_reverse);
   
        %Activate 'EchoIntegrModule'
        EchoIntegrModule = moEchoIntegration();
        moSetEchoIntegrationEnabled(EchoIntegrModule);
        
        %Run EI
%         SampleEchoIntegration(path_hac,path_save,EchoIntegrModule,'20-Jan-2013','04:00:00');
        SampleEchoIntegration(chemin_hac,chemin_save,EchoIntegrModule,nameTransect,dateStart,timeStart,dateEnd,timeEnd);
   
        %Disable 'EchoIntegrModule'
        moSetEchoIntegrationDisabled(EchoIntegrModule);
        clear EchoIntegrModule;
    end
end;
    
    
    
    