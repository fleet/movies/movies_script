%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%
%%% programme pour calculer les valeurs simul�es de l'indicateur
%%% multi-fr�quence � partir des donn�es empiriques ou modellis�es de
%%% SIMFAMI et selon la m�thode d�crite dans "Development and application of an
%%% acoustic multi-frequency diversity index for monitoring major scatter 
%%% groups in large scale pelagic ecosystems" soumise � MEPS en Aout 2011
%%% les filtrer suivant la m�teo et la navigation, caluler les proportions
%%% pour diff�rentes classes d'organismes et les moyenner spatialement
%%%
%%% Laurent Berger 09/2011
%%%
%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all

%plage de fr�quences
freq=[18 38 70 120 200];

%r�ponses fr�quentielles pour diff�rents groupes d'esp�ces
CellulesER60_naturel(1,:)=10.^([-105 -92.5 -82 -72.5 -65 -60]./10); %copepods SIMFAMI
CellulesER60_naturel(2,:)=10.^([-95 -82.5 -72.5 -66 -64 -70]./10); %small euphausids SIMFAMI
% CellulesER60_naturel(3,:)=10.^([-75 -73 -72 -70 -73]./10); %microstructure Lavery 2007
% CellulesER60_naturel(2,:)=10.^([-75 -73 -90 -70 -73]./10); %siphonophore Lavery 2007
CellulesER60_naturel(3,:)=10.^([-59 -60 -60 -58 -54 -55]./10); %mackerel SIMFAMI
CellulesER60_naturel(4,:)=10.^([-63 -48 -57 -58.5 -59.5 -60]./10); %small bubbles SIMFAMI
CellulesER60_naturel(5,:)=10.^([-42 -44 -45 -47 -48 -49]./10); %large bubbles SIMFAMI
CellulesER60_naturel(6,:)=10.^([-47.3 -48 -51.5 -50.8 -49.7 -49]./10); %deep physostomes SIMFAMI
CellulesER60_naturel(7,:)=10.^([-47.3 -48 -48.5 -49.2 -49 -49]./10); %deep physoclists SIMFAMI
% CellulesER60_naturel(7,:)=10.^([-34.5 -37.4 -38.4 -40.3 -38.4]./10); %pacific sardine KRM Demer
% CellulesER60_naturel(8,:)=10.^([-52.5 -53.8 -54.6 -55.2 -55.6]./10); %gas bubbles from the seafloor

nbespeces= size(CellulesER60_naturel,1);

delta_ER60f=zeros(100,nbespeces);

%calcul de l'indicateur pour les diff�rentes esp�ces et pour delta entre 1
%et 100
for i=1:nbespeces
    for j=1:100
        deltanum_ER60f=0;
        deltadenum_ER60f=0;
        for freq1=2:length(freq)
            for freq2=1:freq1-1
                if (CellulesER60_naturel(i,freq1)~=10^(-100/10) && CellulesER60_naturel(i,freq2)~=10^(-100/10))
                    CellulesER60_naturel_freq1=(CellulesER60_naturel(i,freq1)-min(CellulesER60_naturel(i,:)))/(max(CellulesER60_naturel(i,:))-min(CellulesER60_naturel(i,:)));
                    CellulesER60_naturel_freq2=(CellulesER60_naturel(i,freq2)-min(CellulesER60_naturel(i,:)))/(max(CellulesER60_naturel(i,:))-min(CellulesER60_naturel(i,:)));
                    deltanum_ER60f= deltanum_ER60f+ CellulesER60_naturel_freq1*CellulesER60_naturel_freq2*(1-exp(-abs(freq(freq1)-freq(freq2))/(j)))*(1/freq(freq1))*(1/freq(freq2));
                    deltadenum_ER60f= deltadenum_ER60f+ CellulesER60_naturel_freq1*CellulesER60_naturel_freq2*(1/freq(freq1))*(1/freq(freq2));
                end
            end
        end
        if deltadenum_ER60f>0
            delta_ER60f(j,i)=deltanum_ER60f/deltadenum_ER60f;
        end
    end
end

set(0,'DefaultAxesColorOrder',[0 0 1;0 1 0;1 0 0;0 0 0;0.5 0 0.5;0 1 1;1 0 1]);

% R�ponse fr�quentielle en dB
figure
plot([18 38 70 120 200],10*log10(CellulesER60_naturel'),'LineWidth',2);
set(gca,'XTick',[18 38 70 120 200])
set(gca,'XTickLabel',{'18','38','70','120','200'})
legend('copepods','small euphausids','mackerel','small resonant bubbles' ,'large bubbles','deep physostomes','deep physoclists');
% title('Frequency response for major scattering groups');
xlabel('Frequency (kHz)');
ylabel('Volume Bacbscattering Sv (dB)');
grid on;

% Index multi-fr�quence en fonction de delta param�tre permettant de
% pond�rer le poids donn� � la r�partition des fr�quences
figure
plot(1:100,delta_ER60f,'LineWidth',2);
set(gca,'XTick',[18 38 70 120 200])
set(gca,'XTickLabel',{'18','38','70','120','200'})
legend('copepods','small euphausids','mackerel','small resonant bubbles' ,'large bubbles','deep physostomes','deep physoclists');
% title('Frequency diversity index');
xlabel('Delta');
ylabel('Clarke index');
grid on;