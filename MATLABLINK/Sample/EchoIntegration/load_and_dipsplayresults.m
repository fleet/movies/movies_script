close all

chemin_ini='C:\data\CLASS08\RUN004\seq_11_13h\chalut_12h09_fond_corr_AP\filtre DZ corrig� � la verticale +0.75m\';



Sa_surf_Me70 = [];
Sa_surf_ER60 = [];

Sa_bot_Me70 = [];
Sa_bot_ER60 = [];

Sv_surf_Me70 = [];
Sv_surf_ER60 = [];

Sv_bot_Me70 = [];
Sv_bot_ER60 = [];

depth_bottom_Me70 = [];
depth_bottom_ER60 = [];



    FileName = [chemin_ini,'Results.mat'];
    
    load(FileName);
    
    Sa_surf_Me70 = Sa_surfME70;
    Sa_surf_ER60 = Sa_surfER60;

    Sv_surf_Me70 = Sv_surfME70;
    Sv_surf_ER60 = Sv_surfER60;
    
    depth_bottom_Me70 = Depth_botME70;
    depth_bottom_ER60 = Depth_botER60;
    

    
    %pour tronquer les matrices en cas de besoin    
    data1_Sa_surf_Me70 = Sa_surf_Me70(:,:,:);
    data1_Sa_surf_ER60 = Sa_surf_ER60(:,:,:);
    
    data1_depth_bottom_Me70 = depth_bottom_Me70(:,:,:);
    data1_depth_bottom_ER60 = depth_bottom_ER60(:,:,:);
    
    
data1_Lat_ME70 = Lat_surfME70(:,:,:);
data1_Long_ME70 = Long_surfME70(:,:,:);
data1_Depth_ME70 = Depth_surfME70(:,:,:);
    
data1_Lat_ER60 = Lat_surfER60(:,:,:);
data1_Long_ER60 = Long_surfER60(:,:,:);
data1_Depth_ER60 = Depth_surfER60(:,:,:);


Sa_tot_ME70 = sum(data1_Sa_surf_Me70(:,:,1:end),2);
Sa_tot_ER60 = sum(data1_Sa_surf_ER60(:,:,:),2);


    %figure 1
    figure;
    surf(squeeze(sum(data1_Sa_surf_Me70(:,:,:),2)));
      title('Sa ME70');
    ylabel('ESU number');
    xlabel('Beam number');
    zlabel('Sa surface layers ME-70');
    
    %figure 2
    figure;
    surf(squeeze(sum(data1_Sa_surf_ER60(:,:,:),2)));
            title('Sa ER60');
    ylabel('ESU number');
    xlabel('Beam number')
    zlabel('Sa surface layers ER-60');


    %figure 3
    figure;
    surf(squeeze(mean(data1_Sa_surf_Me70,1)));
                  title('Sa ME70');
    ylabel('Layer number');
    xlabel('Beam number');
    zlabel('Average Sa surface layers ME-70');
   
    %figure 4
    figure;
    surf(squeeze(mean(data1_Sa_surf_ER60,1)));
        title('Sa ER60');
    ylabel('Layer number');
    xlabel('Beam number');
    zlabel('Average Sa surface layers ER-60');
    
    %figure 5
    figure;
    surf(squeeze(std(data1_Sa_surf_Me70,0,1)));
             title('STD Sa ME70');
    ylabel('Layer number');
    xlabel('Beam number');
    zlabel('STD Sa surface layers ME-70');
    
    %figure 6
    figure;
    surf(squeeze(std(data1_Sa_surf_ER60,0,1)));
        title('STD Sa ME70');
    ylabel('Layer number');
    xlabel('Beam number');
    zlabel('STD Sa surface layers ER-60');
    


    %figure 7
    figure;
    title('Sa ME70 for each layer');
    xlabel('Beam number');
    ylabel('Sa');
    hold all;
    nbLayer = size(data1_Sa_surf_Me70,2);
    set(0,'DefaultSurfaceEdgeColor');
    for indexLayer=1:nbLayer
      plot(mean(squeeze(data1_Sa_surf_Me70(:,indexLayer,:)),1));
    end;
        plot(mean(squeeze(sum(data1_Sa_surf_Me70(:,:,:),2)),1),'b+');
      
    %figure 8
    figure;
    title('Sa ER60 for each layer');
    xlabel('Beam number');
    ylabel('Sa');
    hold all;
    nbLayer = size(data1_Sa_surf_ER60,2);
    set(0,'DefaultSurfaceEdgeColor');
    for indexLayer=1:nbLayer
        plot(mean(squeeze(data1_Sa_surf_ER60(:,indexLayer,:)),1));
    end
    plot(mean(squeeze(sum(data1_Sa_surf_ER60(:,:,:),2)),1),'r+');
    
    
%     figure;
%     title('Sa ME70 for each ESU');
%     xlabel('Beam number');
%     ylabel('Sa');
%     hold all;
%     nbESU = size(data1_Sa_surf_Me70,1);
%     set(0,'DefaultSurfaceEdgeColor');
%     for indexESU=1:nbESU
%       plot(mean(squeeze(data1_Sa_surf_Me70(indexESU,:,:)),1));
%     end;
%         plot(sum(squeeze(mean(data1_Sa_surf_Me70(:,:,:),1)),1),'b+');
%       
%     figure;
%      title('Sa ER60 for each ESU');
%     xlabel('Beam number');
%     ylabel('Sa');
%     hold all;
%     nbESU = size(data1_Sa_surf_ER60,1);
%     set(0,'DefaultSurfaceEdgeColor');
%     for indexESU=1:nbESU
%       plot(mean(squeeze(data1_Sa_surf_ER60(indexESU,:,:)),1));
%     end;
%         plot(sum(squeeze(mean(data1_Sa_surf_ER60(:,:,:),1)),1),'b+');
%         
%     figure;
% 
%     surf(squeeze(mean(data1_Sa_surf_Me70,1)));
%     title('Sa ME70');
%     ylabel('Layer number');
%     xlabel('Beam number');

    
    %figure 9 et 10 ne marche pas si bcp de pings, il faut tronquer
%     figure;
%     scatter3(reshape(data1_Long_ME70(:,:,:),1,[]),reshape(data1_Lat_ME70(:,:,:),1,[]),reshape(data1_Depth_ME70(:,:,:),1,[]),10,reshape(data1_Sa_surf_Me70(:,:,:),1,[]),'filled');
%     figure;
%     scatter3(reshape(data1_Long_ER60(:,:,:),1,[]),reshape(data1_Lat_ER60(:,:,:),1,[]),reshape(data1_Depth_ER60(:,:,:),1,[]),10,reshape(data1_Sa_surf_ER60(:,:,:),1,[]),'filled');
    
     
%     
%     FileNameAscii = [chemin_ini,'Results_ER60.csv'];
%     fid = fopen(FileNameAscii,'w');
%     result = [reshape(data1_Nav_ER60(:,1,1),1,[])' reshape(data1_Nav_ER60(:,1,2),1,[])' reshape(Sa_tot_ER60(:,1),1,[])'];
%     
%     nbpoints = size(result,1);
%     fprintf(fid,'Latitude\t');
%     fprintf(fid,'Longitude\t');
%     fprintf(fid,'Sa\n');
%     for i=1:nbpoints
%         fprintf(fid,'%3.6f\t',result(i,1));
%         fprintf(fid,'%3.6f\t',result(i,2));
%         fprintf(fid,'%3.6f\n',result(i,3));
%     end;
%         
%     
%     fclose(fid);
%     
%         FileNameAscii = [chemin_ini,'Results_ME70.csv'];
%     fid = fopen(FileNameAscii,'w');
%     result = [reshape(data1_Nav_ME70(:,:,1),1,[])' reshape(data1_Nav_ME70(:,:,2),1,[])' reshape(Sa_tot_ME70(:,:),1,[])'];
%     
%     nbpoints = size(result,1);
%     fprintf(fid,'Latitude\t');
%     fprintf(fid,'Longitude\t');
%     fprintf(fid,'Sa\n');
%     for i=1:nbpoints
%         fprintf(fid,'%3.6f\t',result(i,1));
%         fprintf(fid,'%3.6f\t',result(i,2));
%         fprintf(fid,'%3.6f\n',result(i,3));
%     end;
%         
%     
%     fclose(fid);


%figures profondeur de la couche bottom
%figure 11
    figure;
    surf(squeeze(-data1_depth_bottom_Me70(:,:,:)));
    ylabel('ESU number');
    xlabel('Beam number');
    zlabel('Depth bottom layer ME-70');
    
    %figure 12
    figure;
    surf(squeeze(-data1_depth_bottom_ER60(:,:,:)));
    ylabel('ESU number');
    xlabel('Beam number')
    zlabel('Depth bottom layer ER-60');
    
    


  