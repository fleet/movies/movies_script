clear all;
close all;

dirdata='C:\data\HacCorPel10\';


%import acoustics results
filelist = ls([dirdata,'Clarke*.mat']);  % ensemble des fichiers
nb_files = size(filelist,1);  % nombre de fichiers
sort(filelist);

nblME70=0;
nblER60=0;
for numfile = 1:nb_files
    matfilename = filelist(numfile,:)

    FileName = [dirdata,matfilename(1:findstr('.',matfilename)-1),'.mat'];
    load(FileName);


    latER60all(nblER60+1:nblER60+length(latER60)) = latER60;
    longER60all(nblER60+1:nblER60+length(longER60)) = longER60;

    timeER60all(nblER60+1:nblER60+length(timeER60)) = timeER60/86400 +719529;

    depthER60all(nblER60+1:nblER60+length(depthER60)) = depthER60;

    SvER60all(nblER60+1:nblER60+size(CellulesER60,1),:,:) = CellulesER60;

    ClarkeER60all_f(nblER60+1:nblER60+size(delta_ER60f,1),:) = delta_ER60f;
    
%     ClarkeER60all_f(nblER60+1:100:nblER60+size(delta_ER60g,1)*100,:,:) = delta_ER60g;
%     if (size(ClarkeER60all_f,1)>size(ClarkeER60all_f,1))
%         ClarkeER60all_f(size(ClarkeER60all_f,1)+1:size(ClarkeER60all_f,1),:,:) = zeros(size(ClarkeER60all_f,1)-size(ClarkeER60all_f,1),size(delta_ER60g,2),size(delta_ER60g,3));
%     end
    
    nblER60=length(latER60all);

    clear  latER60  longER60 timeER60 depthER60;
    clear  CellulesER60  delta_ER60g delta_ER60f;
end

jour =  {'0426' '0427' '0428' '0429' '0430' ...
         '0501' '0502' '0503' '0504' '0505' '0506' '0507' '0508' '0509' '0510' ...
         '0511' '0512' '0513' '0514' '0515' '0516' '0517' '0518' '0519' '0520' ...
         '0521' '0522' '0523' '0524' '0525' '0526' '0527' '0528' '0529' '0530' };

nod = [2:24 26:32];

dethcf=[];
ventvf=[];
ventaf=[];
vitscf=[];
pilof=[];
latf=[];
lonf=[];
radiationf=[];
temperaturef=[];
salinityf=[];
fluof=[];
for d = nod
    day{d} = num2str(jour{d});

    
    %% charging the data of Casino
    filec = [dirdata,'casino\Pelgas10\pel10',day{d},'.cas'];
    [dethc,latc,lonc,vitfc,vitsc,roul,...
        tang,prof,radiation,venta,dira,cap,dirc,ventv,dirv,pilo,temperature,salinity,fluo] = ...
        chargecas (filec);
    
    latf=cat(1,latf,latc');
    lonf=cat(1,lonf,lonc');
    dethcf=cat(1,dethcf, datenum(dethc));
    radiationf=cat(1,radiationf,radiation);
    ventvf=cat(1,ventvf, ventv);
    ventaf=cat(1,ventaf, venta);
    vitscf=cat(1,vitscf, vitsc);
    pilof=cat(1,pilof, pilo);
    temperaturef=cat(1,temperaturef, temperature);  
    salinityf=cat(1,salinityf, salinity); 
    fluof=cat(1,fluof, fluo); 
    
end


truewind=NaN(length(timeER60all),1);
vesselspeed=NaN(length(timeER60all),1);
heave=NaN(length(timeER60all),1);
temperature=NaN(length(timeER60all),1);
salinity=NaN(length(timeER60all),1);
fluo=NaN(length(timeER60all),1);
radiation=NaN(length(timeER60all),1);
for i=1:length(timeER60all)
   index=find( abs(timeER60all(i)-dethcf)<(15/86400));
   if ~isempty(index)
        truewind(i)=ventvf(index(1));
        vesselspeed(i)=vitscf(index(1));
        heave(i)=pilof(index(1));
        temperature(i)=temperaturef(index(1));
         salinity(i)=salinityf(index(1));
          fluo(i)=fluof(index(1));
          radiation(i)=radiationf(index(1));
   end
end
temperature(temperature<0)=NaN;
salinity(salinity<0)=NaN;
fluo(fluo<0)=NaN;

save ([dirdata,'Nav'],'truewind','vesselspeed','heave');
save ([dirdata,'Env'],'temperature','salinity','fluo','radiation');


%import ctd file
filectd = [dirdata,'ctd\CTD2009.txt'];
   [nostationctd,datectd,latctd,lonctd,depthctd,temperaturectd,salinityctd,fluoctd] = chargectd (filectd);
   
nostationctdunique=unique(nostationctd);

nbstation=length(nostationctdunique);
fluoctdstation=NaN(nbstation,4);
latctdstation=NaN(nbstation,1);
lonctdstation=NaN(nbstation,1);
datectdstation=NaN(nbstation,1);
for i=1:nbstation
    indexstation=find(nostationctd==nostationctdunique(i));
    indexlayer1=find(depthctd(indexstation)>=10 & depthctd(indexstation)<=15);
    fluoctdstation(i,1)=mean(fluoctd(indexlayer1));
    indexlayer2=find(depthctd(indexstation)>=15 & depthctd(indexstation)<=20);
    fluoctdstation(i,2)=mean(fluoctd(indexlayer2));
    indexlayer3=find(depthctd(indexstation)>=20 & depthctd(indexstation)<=25);
    fluoctdstation(i,3)=mean(fluoctd(indexlayer3));
    indexlayer4=find(depthctd(indexstation)>=25 & depthctd(indexstation)<=30);
    fluoctdstation(i,4)=mean(fluoctd(indexlayer4));    
    latctdstation(i)=unique(latctd(indexstation));
    lonctdstation(i)=unique(lonctd(indexstation));
    datectdstation(i)=unique(datectd(indexstation));
end

%on filtre sur les donn�es acoustiques
indexdate=datectdstation>=min(timeER60all) & datectdstation<=max(timeER60all);
latctd=latctdstation(indexdate);
lonctd=lonctdstation(indexdate);
fluoctd=fluoctdstation(indexdate,:);

save ([dirdata,'CTD'],'latctd','lonctd','fluoctd');

% 
% mX=25;
% nX=25;
% alpha=1;
% 
% longmin = -6;
% longmax= -1;
% latmin = 43.5;
% latmax = 48.5;
% 
% vesselspeedmin=6;
% windspeedmax=25;
% heavemax=1.5;
% 
% maxdepth=200;
% 
% hourmin=5;
% hourmax=20;
% 
% hourER60all = str2num(datestr(timeER60all/86400 +719529,'HH'))';
% 
% % interlat = min(sallat):(max(sallat)-min(sallat))/mX:max(sallat);
% interlat = latmin:(latmax-latmin)/mX:latmax;
% for i = 1:mX
%     slatm(i) = (interlat(i) );
% end
% % interlon = min(sallon):(max(sallon)-min(sallon))/nX:max(sallon);
% interlon = longmin:(longmax-longmin)/nX:longmax;
% for j = 1:nX
%     slonn(j) = (interlon(j));
% end
% 
% matPropER60f=NaN(mX,nX,size(ClarkeER60all_f,2),4);
% matClarkeER60f=NaN(mX,nX,size(ClarkeER60all_f,2));
% matSvER60=NaN(mX,nX,size(SvER60all,2),size(SvER60all,3));
% matdepthER60=NaN(mX,nX);
% mattimeER60=NaN(mX,nX);
% indexfish=zeros(mX,nX,size(ClarkeER60all_f,2));
% indexfluidplancton=zeros(mX,nX,size(ClarkeER60all_f,2));
% indexmackerel=zeros(mX,nX,size(ClarkeER60all_f,2),50*mX);
% indexresonant=zeros(mX,nX,size(ClarkeER60all_f,2),50*mX);
% % creating a mxn-matrix with avaraged data
% for layer=1:size(ClarkeER60all_f,2)
%     for m = 1:mX
%         for n = 1:nX
%             
%             jj = find(latER60all>= interlat(m) & latER60all<= interlat(m+1) & longER60all>=interlon(n) & longER60all<= interlon(n+1) & depthER60all<maxdepth ...
%                    & hourER60all>hourmin & hourER60all<hourmax) ;
%             
%             kk=jj( SvER60all(jj,layer,1)'<-30 & SvER60all(jj,layer,2)'<-30  & SvER60all(jj,layer,3)'<-30 & SvER60all(jj,layer,4)'<-30 & SvER60all(jj,layer,5)'<-30 ...
%                  & ~isnan(SvER60all(jj,layer,1))' & ~isnan(SvER60all(jj,layer,2))'& ~isnan(SvER60all(jj,layer,3))'& ~isnan(SvER60all(jj,layer,4))'& ~isnan(SvER60all(jj,layer,5))');
%              
%             ii=kk(truewind(kk)<=windspeedmax & abs(heave(kk))<heavemax & vesselspeed(kk)>=vesselspeedmin);
%             
% 
%             %         matClarkeER60g(m,n) = mean(nonzeros(ClarkeER60g(ii)));
%             if size(nonzeros(ClarkeER60all_f(ii,layer)))>0
%                 indexfish(m,n,layer)=length(ClarkeER60all_f(ii(ClarkeER60all_f(ii,layer)>0.4 & ClarkeER60all_f(ii,layer)<=0.5),layer));
%                 indexfluidplancton(m,n,layer) = length(ClarkeER60all_f(ii(ClarkeER60all_f(ii,layer)>0.7 & ClarkeER60all_f(ii,layer)<=0.85),layer));
%                 indexmackerel(m,n,layer) = length(ClarkeER60all_f(ii(ClarkeER60all_f(ii,layer)>0.85),layer));
%                 indexresonant(m,n,layer) = length(ClarkeER60all_f(ii(ClarkeER60all_f(ii,layer)>0.6 & ClarkeER60all_f(ii,layer)<=0.7),layer));
%                 total=length(ClarkeER60all_f(ii(ClarkeER60all_f(ii,layer)>0)));
%                 
%                 matPropER60f(m,n,layer,1) = (indexfish(m,n,layer))/total;
%                  matPropER60f(m,n,layer,2) = (indexfluidplancton(m,n,layer))/total;
%                   matPropER60f(m,n,layer,3) = (indexmackerel(m,n,layer))/total;
%                    matPropER60f(m,n,layer,4) = (indexresonant(m,n,layer))/total;
%                    
%                    matClarkeER60f(m,n,layer) = mean(ClarkeER60all_f(ii,layer));
%                  
%             end
% %             for freq=1:size(ClarkeER60all_f,3)
% %                 if size(nonzeros(ClarkeER60all_f(ii,layer,freq)))>0
% %                     matClarkeER60g(m,n,layer,freq) = mean(nonzeros(ClarkeER60all_f(ii,layer,freq)));
% %                 end
% %             end
%              for freq=1:size(SvER60all,3)
%                  matSvER60(m,n,layer,freq) = 10*log10(mean((10.^((SvER60all(ii,layer,freq))./10))));
%              end
%              if size(depthER60all(ii))>0
%                  matdepthER60(m,n)=mean(depthER60all(ii));
%              end
%              
%              if size(timeER60all(ii))>0
%                  [year, month, day, hour, mn, s] = datevec(timeER60all(ii)/86400 +719529);
%                  mattimeER60(m,n)=mean(hour);
%              end
%         end
%     end
% end
% 
% 
% 
% 
% 
%     figure;
%     h2 = pcolor(slonn,slatm,matdepthER60(:,:));
%     hold on;
%     if isempty(alpha) == 0
%         set(h2,'EdgeColor','none','FaceColor','flat',...
%             'FaceAlpha',alpha)
%     end
%     title([' Depth 38 kHz'],'FontSize',15)
%     xlabel('Longitude [degree]','FontSize',15);
%     ylabel('Latitude [degree]','FontSize',15);
%     zlabel('Depth','FontSize',15)
%     scatter(longER60all(1:100:end),latER60all(1:100:end),7,'filled');
%      %text(longER60all(1:3000:end),latER60all(1:3000:end),datestr(timeER60all(1:3000:end)/86400 +719529,'HH'),'FontSize',8,'FontWeight','bold');
%     colorbar('location','eastoutside')
%     axis([longmin longmax latmin latmax]);
%     caxis([0 maxdepth]);
%     view(0,90);grid on;
%     saveas(gcf,[dirdata,'DepthER60_map'],'fig')
%     saveas(gcf,[dirdata,'DepthER60_map'],'jpg')
%     
% 
%         figure;
%     h2 = pcolor(slonn,slatm,mattimeER60);
%     hold on;
%     if isempty(alpha) == 0
%        set(h2,'EdgeColor','none','FaceColor','flat',...
%             'FaceAlpha',alpha)
%     end
%     title([' Time (HH)'],'FontSize',15)
%     xlabel('Longitude [degree]','FontSize',15);
%     ylabel('Latitude [degree]','FontSize',15);
%     zlabel('Hour','FontSize',15)
%     scatter(longER60all(1:100:end),latER60all(1:100:end),7,'filled');
%      %text(longER60all(1:3000:end),latER60all(1:3000:end),datestr(timeER60all(1:3000:end)/86400 +719529,'HH'),'FontSize',8,'FontWeight','bold');
%     colorbar('location','eastoutside')
%     axis([longmin longmax latmin latmax]);
%     caxis([4 20]);
%     view(0,90);  grid on;
%     saveas(gcf,[dirdata,'TimeER60_map'],'fig')
%     saveas(gcf,[dirdata,'TimeER60_map'],'jpg')
%     
%     
%     colorminSv=-80;
%     colormaxSv=-40;
% 
%         figure;
%         h0 = pcolor(slonn,slatm,10*log10(mean(mean(10.^((matSvER60(:,:,6:9,:)./10)),3),4)));
%         hold on
%         if isempty(alpha) == 0
%             set(h0,'EdgeColor','none','FaceColor','flat',...
%                 'FaceAlpha',alpha)
%         end
%         title([' SvER60 surface '],'FontSize',15)
%         xlabel('Longitude [degree]','FontSize',15);
%         ylabel('Latitude [degree]','FontSize',15);
%         zlabel('SvER60 frequency','FontSize',15)
%         scatter(longER60all(1:100:end),latER60all(1:100:end),7,'filled');
%         %text(longER60all(1:3000:end),latER60all(1:3000:end),datestr(timeER60all(1:3000:end)/86400 +719529,'HH'),'FontSize',8,'FontWeight','bold');
%         colorbar('location','eastoutside')
%         axis([longmin longmax latmin latmax]);
%         caxis([colorminSv colormaxSv]);
% %         axis tight
%         view(0,90);
%         grid on;
%         saveas(gcf,[dirdata,'SvER60_map_surface'],'fig')
%         saveas(gcf,[dirdata,'SvER60_map_surface'],'jpg')
% 
% %         figure;
% %         h0 = pcolor(slonn,slatm,10*log10(std(std(10.^((matSvER60(:,:,6:9,:)./10)),0,3),0,4)));
% %         hold on
% %         if isempty(alpha) == 0
% %             set(h0,'EdgeColor','none','FaceColor','flat',...
% %                 'FaceAlpha',alpha)
% %         end
% %         title([' SvER60 surface std '],'FontSize',15)
% %         xlabel('Longitude [degree]','FontSize',15);
% %         ylabel('Latitude [degree]','FontSize',15);
% %         zlabel('SvER60 frequency','FontSize',15)
% %         scatter(longER60all(1:100:end),latER60all(1:100:end),7,'filled');
% %         %%text(longER60all(1:3000:end),latER60all(1:3000:end),datestr(timeER60all(1:3000:end)/86400 +719529,'HH'),'FontSize',8,'FontWeight','bold');
% %         colorbar('location','eastoutside')
% %         axis([longmin longmax latmin latmax]);
% %         caxis([colorminSv colormaxSv]);
% %         %         axis tight
% %         view(0,90);  grid on;;
% %         saveas(gcf,[dirdata,'SvER60_map_surface_std'],'fig')
% %         saveas(gcf,[dirdata,'SvER60_map_surface_std'],'jpg')
% 
%         figure;
%         h0 = pcolor(slonn,slatm,10*log10(mean(mean(10.^((matSvER60(:,:,1:4,:)./10)),3),4)));
%         hold on;
%         if isempty(alpha) == 0
%             set(h0,'EdgeColor','none','FaceColor','flat',...
%                 'FaceAlpha',alpha)
%         end
%         title([' SvER60 bottom '],'FontSize',15)
%         xlabel('Longitude [degree]','FontSize',15);
%         ylabel('Latitude [degree]','FontSize',15);
%         zlabel('SvER60 frequency','FontSize',15)
%         scatter(longER60all(1:100:end),latER60all(1:100:end),7,'filled');
%         %%text(longER60all(1:3000:end),latER60all(1:3000:end),datestr(timeER60all(1:3000:end)/86400 +719529,'HH'),'FontSize',8,'FontWeight','bold');
%         colorbar('location','eastoutside')
%          axis([longmin longmax latmin latmax]);
%         caxis([colorminSv colormaxSv]);
%         %         axis tight
%         view(0,90);  grid on;;
%         saveas(gcf,[dirdata,'SvER60_map_bottom'],'fig')
%         saveas(gcf,[dirdata,'SvER60_map_bottom'],'jpg')
% 
% %         figure;
% %         h0 = pcolor(slonn,slatm,10*log10(std(std(10.^((matSvER60(:,:,1:4,:)./10)),0,3),0,4)));
% %         hold on;
% %         if isempty(alpha) == 0
% %             set(h0,'EdgeColor','none','FaceColor','flat',...
% %                 'FaceAlpha',alpha)
% %         end
% %         title([' SvER60 bottom std '],'FontSize',15)
% %         xlabel('Longitude [degree]','FontSize',15);
% %         ylabel('Latitude [degree]','FontSize',15);
% %         zlabel('SvER60 frequency','FontSize',15)
% %         scatter(longER60all(1:100:end),latER60all(1:100:end),7,'filled');
% %         %%text(longER60all(1:3000:end),latER60all(1:3000:end),datestr(timeER60all(1:3000:end)/86400 +719529,'HH'),'FontSize',8,'FontWeight','bold');
% %         colorbar('location','eastoutside')
% %          axis([longmin longmax latmin latmax]);
% %         caxis([colorminSv colormaxSv]);
% %         %         axis tight
% %         view(0,90);  grid on;;
% %         saveas(gcf,[dirdata,'SvER60_map_bottom_std'],'fig')
% %         saveas(gcf,[dirdata,'SvER60_map_bottom_std'],'jpg')
% 
%         figure;
%         h0 = pcolor(slonn,slatm,10*log10(mean(10.^((matSvER60(:,:,5,:)./10)),4)));
%         hold on;
%         if isempty(alpha) == 0
%             set(h0,'EdgeColor','none','FaceColor','flat',...
%                 'FaceAlpha',alpha)
%         end
%         title([' SvER60 middle '],'FontSize',15)
%         xlabel('Longitude [degree]','FontSize',15);
%         ylabel('Latitude [degree]','FontSize',15);
%         zlabel('SvER60 frequency','FontSize',15)
%         scatter(longER60all(1:100:end),latER60all(1:100:end),7,'filled');
%         %%text(longER60all(1:3000:end),latER60all(1:3000:end),datestr(timeER60all(1:3000:end)/86400 +719529,'HH'),'FontSize',8,'FontWeight','bold');
%         colorbar('location','eastoutside')
%          axis([longmin longmax latmin latmax]);
%         caxis([colorminSv colormaxSv]);
%         %         axis tight
%         view(0,90);  grid on;;
%         saveas(gcf,[dirdata,'SvER60_map_middle'],'fig')
%         saveas(gcf,[dirdata,'SvER60_map_middle'],'jpg')
%         
%         matSvER60mean=NaN(mX,nX,5);
%         for m = 1:mX
%             for n = 1:nX
%                 for freq=1:5
%                     if(size(matSvER60(isfinite(matSvER60(m,n,:,freq))))>0)
%                         matSvER60mean(m,n,freq)=10*log10(mean(10.^(matSvER60(m,n,isfinite(matSvER60(m,n,:,freq)),freq)./10)));
%                     end
%                 end
%             end
%         end
%             
%             
%         figure;
%         h0 = pcolor(slonn,slatm,10*log10(mean(10.^(((matSvER60mean(:,:,:))./10)),3)));
%         hold on;
%         if isempty(alpha) == 0
%             set(h0,'EdgeColor','none','FaceColor','flat',...
%                 'FaceAlpha',alpha)
%         end
%         title([' SvER60'],'FontSize',15)
%         xlabel('Longitude [degree]','FontSize',15);
%         ylabel('Latitude [degree]','FontSize',15);
%         zlabel('SvER60 mean','FontSize',15)
%         scatter(longER60all(1:100:end),latER60all(1:100:end),7,'filled');
%         %%text(longER60all(1:3000:end),latER60all(1:3000:end),datestr(timeER60all(1:3000:end)/86400 +719529,'HH'),'FontSize',8,'FontWeight','bold');
%         colorbar('location','eastoutside')
%          axis([longmin longmax latmin latmax]);
%         caxis([colorminSv colormaxSv]);
%         %         axis tight
%         view(0,90);  grid on;;
%         saveas(gcf,[dirdata,'SvER60_map_all'],'fig')
%         saveas(gcf,[dirdata,'SvER60_map_all'],'jpg')
% 
% 
% %     for freq=1:size(matSvER60,4)
% %         figure;
% %         h0 = pcolor(slonn,slatm,10*log10(mean(10.^((matSvER60(:,:,6:9,freq)./10)),3)));
% %         hold on
% %         if isempty(alpha) == 0
% %             set(h0,'EdgeColor','none','FaceColor','flat',...
% %                 'FaceAlpha',alpha)
% %         end
% %         title([' SvER60 surface for frequency ', sprintf('%g',freq)],'FontSize',15)
% %         xlabel('Longitude [degree]','FontSize',15);
% %         ylabel('Latitude [degree]','FontSize',15);
% %         zlabel('SvER60 frequency','FontSize',15)
% %         scatter(longER60all(1:100:end),latER60all(1:100:end),7,'filled');
% % %         text(longER60all(1:3000:end),latER60all(1:3000:end),datestr(timeER60all(1:3000:end)/86400 +719529,'HH'),'FontSize',8,'FontWeight','bold','FontWeight','bold');
% %         colorbar('location','eastoutside')
% %          axis([longmin longmax latmin latmax]);
% %         caxis([colorminSv colormaxSv]);
% %         %         axis tight
% %         view(0,90);  grid on;;
% %         saveas(gcf,[dirdata,'SvER60_map_surface', sprintf('%g',freq)],'fig')
% %         saveas(gcf,[dirdata,'SvER60_map_surface', sprintf('%g',freq)],'jpg')
% %         close;
% 
% %         figure;
% %         h0 = pcolor(slonn,slatm,10*log10(std(10.^((matSvER60(:,:,6:9,freq)./10)),0,3)));
% %         hold on
% %         if isempty(alpha) == 0
% %             set(h0,'EdgeColor','none','FaceColor','flat',...
% %                 'FaceAlpha',alpha)
% %         end
% %         title([' SvER60 surface std for frequency ', sprintf('%g',freq)],'FontSize',15)
% %         xlabel('Longitude [degree]','FontSize',15);
% %         ylabel('Latitude [degree]','FontSize',15);
% %         zlabel('SvER60 frequency','FontSize',15)
% %         scatter(longER60all(1:100:end),latER60all(1:100:end),7,'filled');
% %         %%text(longER60all(1:3000:end),latER60all(1:3000:end),datestr(timeER60all(1:3000:end)/86400 +719529,'HH'),'FontSize',8,'FontWeight','bold');
% %         colorbar('location','eastoutside')
% %          axis([longmin longmax latmin latmax]);
% %         caxis([colorminSv colormaxSv]);
% %         %         axis tight
% %         view(0,90);  grid on;;
% %         saveas(gcf,[dirdata,'SvER60_map_surface_std', sprintf('%g',freq)],'fig')
% %         saveas(gcf,[dirdata,'SvER60_map_surface_std', sprintf('%g',freq)],'jpg')
% %         close;
% 
% %         figure;
% %         h0 = pcolor(slonn,slatm,10*log10(mean(10.^((matSvER60(:,:,1:4,freq)./10)),3)));
% %         hold on;
% %         if isempty(alpha) == 0
% %             set(h0,'EdgeColor','none','FaceColor','flat',...
% %                 'FaceAlpha',alpha)
% %         end
% %         title([' SvER60 bottom for frequency ', sprintf('%g',freq)],'FontSize',15)
% %         xlabel('Longitude [degree]','FontSize',15);
% %         ylabel('Latitude [degree]','FontSize',15);
% %         zlabel('SvER60 frequency','FontSize',15)
% %         scatter(longER60all(1:100:end),latER60all(1:100:end),7,'filled');
% %         %%%text(longER60all(1:3000:end),latER60all(1:3000:end),datestr(timeER60all(1:3000:end)/86400 +719529,'HH'),'FontSize',8,'FontWeight','bold');
% %         colorbar('location','eastoutside')
% %          axis([longmin longmax latmin latmax]);
% %         caxis([colorminSv colormaxSv]);
% %         %         axis tight
% %         view(0,90);  grid on;;
% %         saveas(gcf,[dirdata,'SvER60_map_bottom', sprintf('%g',freq)],'fig')
% %         saveas(gcf,[dirdata,'SvER60_map_bottom', sprintf('%g',freq)],'jpg')
% %         close;
% % 
% %         figure;
% %         h0 = pcolor(slonn,slatm,10*log10(std(10.^((matSvER60(:,:,1:4,freq)./10)),0,3)));
% %         hold on;
% %         if isempty(alpha) == 0
% %             set(h0,'EdgeColor','none','FaceColor','flat',...
% %                 'FaceAlpha',alpha)
% %         end
% %         title([' SvER60 bottom std for frequency ', sprintf('%g',freq)],'FontSize',15)
% %         xlabel('Longitude [degree]','FontSize',15);
% %         ylabel('Latitude [degree]','FontSize',15);
% %         zlabel('SvER60 frequency','FontSize',15)
% %         scatter(longER60all(1:100:end),latER60all(1:100:end),7,'filled');
% %         %%%text(longER60all(1:3000:end),latER60all(1:3000:end),datestr(timeER60all(1:3000:end)/86400 +719529,'HH'),'FontSize',8,'FontWeight','bold');
% %         colorbar('location','eastoutside')
% %          axis([longmin longmax latmin latmax]);
% %         caxis([colorminSv colormaxSv]);
% %         %         axis tight
% %         view(0,90);  grid on;;
% %         saveas(gcf,[dirdata,'SvER60_map_bottom_std', sprintf('%g',freq)],'fig')
% %         saveas(gcf,[dirdata,'SvER60_map_bottom_std', sprintf('%g',freq)],'jpg')
% %         close;
% 
% %         figure;
% %         h0 = pcolor(slonn,slatm,10*log10(mean(10.^((matSvER60(:,:,5,freq)./10)),3)));
% %         hold on;
% %         if isempty(alpha) == 0
% %             set(h0,'EdgeColor','none','FaceColor','flat',...
% %                 'FaceAlpha',alpha)
% %         end
% %         title([' SvER60 middle for frequency ', sprintf('%g',freq)],'FontSize',15)
% %         xlabel('Longitude [degree]','FontSize',15);
% %         ylabel('Latitude [degree]','FontSize',15);
% %         zlabel('SvER60 frequency','FontSize',15)
% %         scatter(longER60all(1:100:end),latER60all(1:100:end),7,'filled');
% %         %%%text(longER60all(1:3000:end),latER60all(1:3000:end),datestr(timeER60all(1:3000:end)/86400 +719529,'HH'),'FontSize',8,'FontWeight','bold');
% %         colorbar('location','eastoutside')
% %          axis([longmin longmax latmin latmax]);
% %         caxis([colorminSv colormaxSv]);
% %         %         axis tight
% %         view(0,90);  grid on;;
% %         saveas(gcf,[dirdata,'SvER60_map_middle', sprintf('%g',freq)],'fig')
% %         saveas(gcf,[dirdata,'SvER60_map_middle', sprintf('%g',freq)],'jpg')
% %         close;
% % 
% %     end
% 
% 
% % for layer=1:size(matSvER60,3)
% %     figure;
% %     h1 = pcolor(slonn,slatm,matSvER60(:,:,layer));
% %     hold on;
% %     if isempty(alpha) == 0
% %         set(h1,'EdgeColor','none','FaceColor','flat',...
% %             'FaceAlpha',alpha)
% %     end
% %     title([' SvER60 for layer ', sprintf('%g',layer)],'FontSize',15)
% %     xlabel('Longitude [degree]','FontSize',15);
% %     ylabel('Latitude [degree]','FontSize',15);
% %     zlabel('SvER60 frequency','FontSize',15)
% %         scatter(longER60all(1:100:end),latER60all(1:100:end),7,'filled');
% %      %%%text(longER60all(1:3000:end),latER60all(1:3000:end),datestr(timeER60all(1:3000:end)/86400 +719529,'HH'),'FontSize',8,'FontWeight','bold');
% %     colorbar('location','eastoutside')
% %      axis([longmin longmax latmin latmax]);
% %     caxis([colorminSv colormaxSv]);
% %     %         axis tight
% %     view(0,90);  grid on;;
% %     saveas(gcf,[dirdata,'SvER60_map', sprintf('%g',layer)],'fig')
% %     saveas(gcf,[dirdata,'SvER60_map', sprintf('%g',layer)],'jpg')
% %     close;
% % end
% 
% colorminClarke= 0.4;
% colormaxClarke = 0.8;
% 
% for layer=1:size(matSvER60,3)
%     figure;
%     h1 = pcolor(slonn,slatm,matClarkeER60f(:,:,layer));
%     hold on;
%     if isempty(alpha) == 0
%         set(h1,'EdgeColor','none','FaceColor','flat',...
%             'FaceAlpha',alpha)
%     end
%     title([' ClarkeER60 for layer ', sprintf('%g',layer)],'FontSize',15)
%     xlabel('Longitude [degree]','FontSize',15);
%     ylabel('Latitude [degree]','FontSize',15);
%     zlabel('ClarkeER60 layer','FontSize',15)
%         scatter(longER60all(1:100:end),latER60all(1:100:end),7,'filled');
%      %%%text(longER60all(1:3000:end),latER60all(1:3000:end),datestr(timeER60all(1:3000:end)/86400 +719529,'HH'),'FontSize',8,'FontWeight','bold');
%     colorbar('location','eastoutside')
%      axis([longmin longmax latmin latmax]);
%     caxis([colorminClarke colormaxClarke]);
%     %         axis tight
%     view(0,90);  grid on;;
%     saveas(gcf,[dirdata,'ClarkeER60_map', sprintf('%g',layer)],'fig')
%     saveas(gcf,[dirdata,'ClarkeER60_map', sprintf('%g',layer)],'jpg')
%     close;
% end
% 
%  figure;
%     h2 = pcolor(slonn,slatm,mean(matClarkeER60f(:,:,:),3));
%     hold on;
%     if isempty(alpha) == 0
%        set(h2,'EdgeColor','none','FaceColor','flat',...
%             'FaceAlpha',alpha)
%     end
%     title(['ClarkeER60'],'FontSize',15)
%     xlabel('Longitude [degree]','FontSize',15);
%     ylabel('Latitude [degree]','FontSize',15);
%     zlabel('Proportion ','FontSize',15)
%     scatter(longER60all(1:100:end),latER60all(1:100:end),7,'filled');
%      %%%text(longER60all(1:3000:end),latER60all(1:3000:end),datestr(timeER60all(1:3000:end)/86400 +719529,'HH'),'FontSize',8,'FontWeight','bold');
%     colorbar('location','eastoutside')
%      axis([longmin longmax latmin latmax]);
%     caxis([colorminClarke colormaxClarke]);
%     %         axis tight
%     view(0,90);
%     grid on;
%     saveas(gcf,[dirdata,'ClarkeER60f_map'],'fig')
%     saveas(gcf,[dirdata,'ClarkeER60f_map'],'jpg')
%     
%     
% colorminProp= 0;
% colormaxProp = 0.5;
% 
% 
% 
%     figure;
%     h2 = pcolor(slonn,slatm,mean(matPropER60f(:,:,6:9,2),3));
%     hold on;
%     if isempty(alpha) == 0
%        set(h2,'EdgeColor','none','FaceColor','flat',...
%             'FaceAlpha',alpha)
%     end
%     title([' Proportion  surface plankton'],'FontSize',15)
%     xlabel('Longitude [degree]','FontSize',15);
%     ylabel('Latitude [degree]','FontSize',15);
%     zlabel('Proportion ','FontSize',15)
%     scatter(longER60all(1:100:end),latER60all(1:100:end),7,'filled');
%      %%%text(longER60all(1:3000:end),latER60all(1:3000:end),datestr(timeER60all(1:3000:end)/86400 +719529,'HH'),'FontSize',8,'FontWeight','bold');
%     colorbar('location','eastoutside')
%      axis([longmin longmax latmin latmax]);
%     caxis([colorminProp colormaxProp]);
%     %         axis tight
%     view(0,90);
%     grid on;
%     saveas(gcf,[dirdata,'ClarkeER60f_map_surface_plankton'],'fig')
%     saveas(gcf,[dirdata,'ClarkeER60f_map_surface_plankton'],'jpg')
%     
% 
% 
%       figure;
%     h2 = pcolor(slonn,slatm,mean(matPropER60f(:,:,6:9,1),3));
%     hold on;
%     if isempty(alpha) == 0
%        set(h2,'EdgeColor','none','FaceColor','flat',...
%             'FaceAlpha',alpha)
%     end
%     title([' Proportion  surface swimbladder fish'],'FontSize',15)
%     xlabel('Longitude [degree]','FontSize',15);
%     ylabel('Latitude [degree]','FontSize',15);
%     zlabel('Proportion ','FontSize',15)
%     scatter(longER60all(1:100:end),latER60all(1:100:end),7,'filled');
%      %%%text(longER60all(1:3000:end),latER60all(1:3000:end),datestr(timeER60all(1:3000:end)/86400 +719529,'HH'),'FontSize',8,'FontWeight','bold');
%     colorbar('location','eastoutside')
%      axis([longmin longmax latmin latmax]);
%     caxis([colorminProp colormaxProp]);
%     %         axis tight
%     view(0,90);
%     grid on;
%     saveas(gcf,[dirdata,'ClarkeER60f_map_surface_fish'],'fig')
%     saveas(gcf,[dirdata,'ClarkeER60f_map_surface_fish'],'jpg')
%     
% 
%       figure;
%     h2 = pcolor(slonn,slatm,mean(matPropER60f(:,:,6:9,3),3));
%     hold on;
%     if isempty(alpha) == 0
%        set(h2,'EdgeColor','none','FaceColor','flat',...
%             'FaceAlpha',alpha)
%     end
%     title([' Proportion  surface mackerel'],'FontSize',15)
%     xlabel('Longitude [degree]','FontSize',15);
%     ylabel('Latitude [degree]','FontSize',15);
%     zlabel('Proportion ','FontSize',15)
%     scatter(longER60all(1:100:end),latER60all(1:100:end),7,'filled');
%      %%%text(longER60all(1:3000:end),latER60all(1:3000:end),datestr(timeER60all(1:3000:end)/86400 +719529,'HH'),'FontSize',8,'FontWeight','bold');
%     colorbar('location','eastoutside')
%      axis([longmin longmax latmin latmax]);
%     caxis([colorminProp colormaxProp]);
%     %         axis tight
%     view(0,90);
%     grid on;
%     saveas(gcf,[dirdata,'ClarkeER60f_map_surface_mackerel'],'fig')
%     saveas(gcf,[dirdata,'ClarkeER60f_map_surface_mackerel'],'jpg')
% 
%      figure;
%     h2 = pcolor(slonn,slatm,mean(matPropER60f(:,:,6:9,4),3));
%     hold on;
%     if isempty(alpha) == 0
%        set(h2,'EdgeColor','none','FaceColor','flat',...
%             'FaceAlpha',alpha)
%     end
%     title([' Proportion  surface resonant'],'FontSize',15)
%     xlabel('Longitude [degree]','FontSize',15);
%     ylabel('Latitude [degree]','FontSize',15);
%     zlabel('Proportion ','FontSize',15)
%     scatter(longER60all(1:100:end),latER60all(1:100:end),7,'filled');
%      %%%text(longER60all(1:3000:end),latER60all(1:3000:end),datestr(timeER60all(1:3000:end)/86400 +719529,'HH'),'FontSize',8,'FontWeight','bold');
%     colorbar('location','eastoutside')
%      axis([longmin longmax latmin latmax]);
%     caxis([colorminProp colormaxProp]);
%     %         axis tight
%     view(0,90);
%     grid on;
%     saveas(gcf,[dirdata,'ClarkeER60f_map_surface_resonant'],'fig')
%     saveas(gcf,[dirdata,'ClarkeER60f_map_surface_resonant'],'jpg')
% 
% 
% 
%     figure;
%     h2 = pcolor(slonn,slatm,mean(matPropER60f(:,:,1:4,2),3));
%     hold on;
%     if isempty(alpha) == 0
%        set(h2,'EdgeColor','none','FaceColor','flat',...
%             'FaceAlpha',alpha)
%     end
%     title([' Proportion  bottom plankton'],'FontSize',15)
%     xlabel('Longitude [degree]','FontSize',15);
%     ylabel('Latitude [degree]','FontSize',15);
%     zlabel('Proportion ','FontSize',15)
%     scatter(longER60all(1:100:end),latER60all(1:100:end),7,'filled');
%      %%%text(longER60all(1:3000:end),latER60all(1:3000:end),datestr(timeER60all(1:3000:end)/86400 +719529,'HH'),'FontSize',8,'FontWeight','bold');
%     colorbar('location','eastoutside')
%      axis([longmin longmax latmin latmax]);
%     caxis([colorminProp colormaxProp]);
%     %         axis tight
%     view(0,90);
%     grid on;
%     saveas(gcf,[dirdata,'ClarkeER60f_map_bottom_plankton'],'fig')
%     saveas(gcf,[dirdata,'ClarkeER60f_map_bottom_plankton'],'jpg')
%     
% 
%     figure;
%     h2 = pcolor(slonn,slatm,mean(matPropER60f(:,:,1:4,1),3));
%     hold on;
%     if isempty(alpha) == 0
%        set(h2,'EdgeColor','none','FaceColor','flat',...
%             'FaceAlpha',alpha)
%     end
%     title([' Proportion  bottom swimbladder fish'],'FontSize',15)
%     xlabel('Longitude [degree]','FontSize',15);
%     ylabel('Latitude [degree]','FontSize',15);
%     zlabel('Proportion ','FontSize',15)
%     scatter(longER60all(1:100:end),latER60all(1:100:end),7,'filled');
%      %%%text(longER60all(1:3000:end),latER60all(1:3000:end),datestr(timeER60all(1:3000:end)/86400 +719529,'HH'),'FontSize',8,'FontWeight','bold');
%     colorbar('location','eastoutside')
%      axis([longmin longmax latmin latmax]);
%     caxis([colorminProp colormaxProp]);
%     %         axis tight
%     view(0,90);
%     grid on;
%     saveas(gcf,[dirdata,'ClarkeER60f_map_bottom_fish'],'fig')
%     saveas(gcf,[dirdata,'ClarkeER60f_map_bottom_fish'],'jpg')
% 
%     figure;
%     h2 = pcolor(slonn,slatm,mean(matPropER60f(:,:,1:4,3),3));
%     hold on;
%     if isempty(alpha) == 0
%        set(h2,'EdgeColor','none','FaceColor','flat',...
%             'FaceAlpha',alpha)
%     end
%     title([' Proportion  bottom mackerel'],'FontSize',15)
%     xlabel('Longitude [degree]','FontSize',15);
%     ylabel('Latitude [degree]','FontSize',15);
%     zlabel('Proportion ','FontSize',15)
%     scatter(longER60all(1:100:end),latER60all(1:100:end),7,'filled');
%      %%%text(longER60all(1:3000:end),latER60all(1:3000:end),datestr(timeER60all(1:3000:end)/86400 +719529,'HH'),'FontSize',8,'FontWeight','bold');
%     colorbar('location','eastoutside')
%      axis([longmin longmax latmin latmax]);
%     caxis([colorminProp colormaxProp]);
%     %         axis tight
%     view(0,90);
%     grid on;
%     saveas(gcf,[dirdata,'ClarkeER60f_map_bottom_mackerel'],'fig')
%     saveas(gcf,[dirdata,'ClarkeER60f_map_bottom_mackerel'],'jpg')
%  
%         figure;
%     h2 = pcolor(slonn,slatm,mean(matPropER60f(:,:,1:4,4),3));
%     hold on;
%     if isempty(alpha) == 0
%        set(h2,'EdgeColor','none','FaceColor','flat',...
%             'FaceAlpha',alpha)
%     end
%     title([' Proportion  bottom resonant'],'FontSize',15)
%     xlabel('Longitude [degree]','FontSize',15);
%     ylabel('Latitude [degree]','FontSize',15);
%     zlabel('Proportion ','FontSize',15)
%     scatter(longER60all(1:100:end),latER60all(1:100:end),7,'filled');
%      %%%text(longER60all(1:3000:end),latER60all(1:3000:end),datestr(timeER60all(1:3000:end)/86400 +719529,'HH'),'FontSize',8,'FontWeight','bold');
%     colorbar('location','eastoutside')
%      axis([longmin longmax latmin latmax]);
%     caxis([colorminProp colormaxProp]);
%     %         axis tight
%     view(0,90);
%     grid on;
%     saveas(gcf,[dirdata,'ClarkeER60f_map_bottom_resonant'],'fig')
%     saveas(gcf,[dirdata,'ClarkeER60f_map_bottom_resonant'],'jpg')
%     
%     
%             matPropER60fmean=NaN(mX,nX,4);
%         for m = 1:mX
%             for n = 1:nX
%                 for class=1:4
%                     if(size(matPropER60f(isfinite(matPropER60f(m,n,:,class))))>0)
%                         matPropER60fmean(m,n,class)=mean(matPropER60f(m,n,isfinite(matPropER60f(m,n,:,class)),class));
%                     end
%                 end
%             end
%         end
%         
% 
%        figure;
%     h2 = pcolor(slonn,slatm,matPropER60fmean(:,:,2));
%     hold on;
%     if isempty(alpha) == 0
%        set(h2,'EdgeColor','none','FaceColor','flat',...
%             'FaceAlpha',alpha)
%     end
%     title([' Proportion  plankton'],'FontSize',15)
%     xlabel('Longitude [degree]','FontSize',15);
%     ylabel('Latitude [degree]','FontSize',15);
%     zlabel('Proportion ','FontSize',15)
%     scatter(longER60all(1:100:end),latER60all(1:100:end),7,'filled');
%      %%%text(longER60all(1:3000:end),latER60all(1:3000:end),datestr(timeER60all(1:3000:end)/86400 +719529,'HH'),'FontSize',8,'FontWeight','bold');
%     colorbar('location','eastoutside')
%      axis([longmin longmax latmin latmax]);
%     caxis([colorminProp colormaxProp]);
%     %         axis tight
%     view(0,90);
%     grid on;
%     saveas(gcf,[dirdata,'ClarkeER60f_map_plankton'],'fig')
%     saveas(gcf,[dirdata,'ClarkeER60f_map_plankton'],'jpg')
%     
% 
%     figure;
%     h2 = pcolor(slonn,slatm,matPropER60fmean(:,:,1));
%     hold on;
%     if isempty(alpha) == 0
%        set(h2,'EdgeColor','none','FaceColor','flat',...
%             'FaceAlpha',alpha)
%     end
%     title([' Proportion  swimbladder fish'],'FontSize',15)
%     xlabel('Longitude [degree]','FontSize',15);
%     ylabel('Latitude [degree]','FontSize',15);
%     zlabel('Proportion ','FontSize',15)
%     scatter(longER60all(1:100:end),latER60all(1:100:end),7,'filled');
%      %%%text(longER60all(1:3000:end),latER60all(1:3000:end),datestr(timeER60all(1:3000:end)/86400 +719529,'HH'),'FontSize',8,'FontWeight','bold');
%     colorbar('location','eastoutside')
%      axis([longmin longmax latmin latmax]);
%     caxis([colorminProp colormaxProp]);
%     %         axis tight
%     view(0,90);
%     grid on;
%     saveas(gcf,[dirdata,'ClarkeER60f_map_fish'],'fig')
%     saveas(gcf,[dirdata,'ClarkeER60f_map_fish'],'jpg')
% 
%     figure;
%     h2 = pcolor(slonn,slatm,matPropER60fmean(:,:,3));
%     hold on;
%     if isempty(alpha) == 0
%        set(h2,'EdgeColor','none','FaceColor','flat',...
%             'FaceAlpha',alpha)
%     end
%     title([' Proportion  mackerel'],'FontSize',15)
%     xlabel('Longitude [degree]','FontSize',15);
%     ylabel('Latitude [degree]','FontSize',15);
%     zlabel('Proportion ','FontSize',15)
%     scatter(longER60all(1:100:end),latER60all(1:100:end),7,'filled');
%      %%%text(longER60all(1:3000:end),latER60all(1:3000:end),datestr(timeER60all(1:3000:end)/86400 +719529,'HH'),'FontSize',8,'FontWeight','bold');
%     colorbar('location','eastoutside')
%      axis([longmin longmax latmin latmax]);
%     caxis([colorminProp colormaxProp]);
%     %         axis tight
%     view(0,90);
%     grid on;
%     saveas(gcf,[dirdata,'ClarkeER60f_map_mackerel'],'fig')
%     saveas(gcf,[dirdata,'ClarkeER60f_map_mackerel'],'jpg')
%  
%         figure;
%     h2 = pcolor(slonn,slatm,matPropER60fmean(:,:,4));
%     hold on;
%     if isempty(alpha) == 0
%        set(h2,'EdgeColor','none','FaceColor','flat',...
%             'FaceAlpha',alpha)
%     end
%     title([' Proportion   resonant'],'FontSize',15)
%     xlabel('Longitude [degree]','FontSize',15);
%     ylabel('Latitude [degree]','FontSize',15);
%     zlabel('Proportion ','FontSize',15)
%     scatter(longER60all(1:100:end),latER60all(1:100:end),7,'filled');
%      %%%text(longER60all(1:3000:end),latER60all(1:3000:end),datestr(timeER60all(1:3000:end)/86400 +719529,'HH'),'FontSize',8,'FontWeight','bold');
%     colorbar('location','eastoutside')
%      axis([longmin longmax latmin latmax]);
%     caxis([colorminProp colormaxProp]);
%     %         axis tight
%     view(0,90);
%     grid on;
%     saveas(gcf,[dirdata,'ClarkeER60f_map_resonant'],'fig')
%     saveas(gcf,[dirdata,'ClarkeER60f_map_resonant'],'jpg')
%     
%             edge=[0.3:0.05:1];
%      
%             index=  hourER60all>hourmin & hourER60all<hourmax ;
%            
%             n=histc(ClarkeER60all_f(index,:),edge);
%             figure;
%             bar(edge,n);
%             xlabel('Diversity ');
%             title(['Histogram multifrequency diversyty for each layer']);
%             saveas(gcf,[dirdata,'HistogramDiversity'],'fig')
%             saveas(gcf,[dirdata,'HistogramDiversity'],'jpg')
%             
%        edge=[0 0.3 0.5 0.7 0.85 1];
%      
%         index=  hourER60all>hourmin & hourER60all<hourmax ;
%            
%             n=histc(ClarkeER60all_f(index,:),edge);
%             for i=1:9
%                 n(:,i)=n(:,i)./length(ClarkeER60all_f(ClarkeER60all_f(index,i)>0));
%             end
%             figure;
%             bar(edge,n);
%             xlabel('Diversity ');
%             title(['Proportion species group  for each layer']);
%             saveas(gcf,[dirdata,'ProportionSpecies'],'fig')
%             saveas(gcf,[dirdata,'ProportionSpecies'],'jpg')
%     
%     freq_response=SvER60all(:,:,:)-repmat(SvER60all(:,:,2),[1 1 5]);
%     
%     index_Fish=(ClarkeER60all_f(:,:)<=0.5 &ClarkeER60all_f(:,:)>0.4);
%     ind=find(index_Fish>0);
%         
%     matfreq_Fish18=squeeze(freq_response(:,:,1));
%     matfreq_Fish38=squeeze(freq_response(:,:,2));
%     matfreq_Fish70=squeeze(freq_response(:,:,3));
%     matfreq_Fish120=squeeze(freq_response(:,:,4));
%     matfreq_Fish200=squeeze(freq_response(:,:,5));
%     
%     figure;boxplot([matfreq_Fish18(ind) matfreq_Fish38(ind) matfreq_Fish70(ind) matfreq_Fish120(ind) matfreq_Fish200(ind)],[18 38 70 120 200]);
%        xlabel('Frequency ');
%             title(['Frequency response Swimbladder Fish']);
%             saveas(gcf,[dirdata,'FrequencyResponseFish'],'fig')
%             saveas(gcf,[dirdata,'FrequencyResponseFish'],'jpg')
%             
%     index_Plankton=(ClarkeER60all_f(:,:)<=0.85 &ClarkeER60all_f(:,:)>0.7);
%     ind=find(index_Plankton>0);
%         
%     matfreq_Plankton18=squeeze(freq_response(:,:,1));
%     matfreq_Plankton38=squeeze(freq_response(:,:,2));
%     matfreq_Plankton70=squeeze(freq_response(:,:,3));
%     matfreq_Plankton120=squeeze(freq_response(:,:,4));
%     matfreq_Plankton200=squeeze(freq_response(:,:,5));
%     
%     figure;boxplot([matfreq_Plankton18(ind) matfreq_Plankton38(ind) matfreq_Plankton70(ind) matfreq_Plankton120(ind) matfreq_Plankton200(ind)],[18 38 70 120 200]);
%      xlabel('Frequency ');
%             title(['Frequency response Plankton']);
%             saveas(gcf,[dirdata,'FrequencyResponsePlankton'],'fig')
%             saveas(gcf,[dirdata,'FrequencyResponsePlankton'],'jpg')
%             
%             index_Mackerel=(ClarkeER60all_f(:,:)<1 &ClarkeER60all_f(:,:)>0.85);
%     ind=find(index_Mackerel>0);
%         
%     matfreq_Mackerel18=squeeze(freq_response(:,:,1));
%     matfreq_Mackerel38=squeeze(freq_response(:,:,2));
%     matfreq_Mackerel70=squeeze(freq_response(:,:,3));
%     matfreq_Mackerel120=squeeze(freq_response(:,:,4));
%     matfreq_Mackerel200=squeeze(freq_response(:,:,5));
%     
%     figure;boxplot([matfreq_Mackerel18(ind) matfreq_Mackerel38(ind) matfreq_Mackerel70(ind) matfreq_Mackerel120(ind) matfreq_Mackerel200(ind)],[18 38 70 120 200]);
%      xlabel('Frequency ');
%             title(['Frequency response Mackerel']);
%             saveas(gcf,[dirdata,'FrequencyResponseMackerel'],'fig')
%             saveas(gcf,[dirdata,'FrequencyResponseMackerel'],'jpg')
%            
%     index_Resonant=(ClarkeER60all_f(:,:)<0.7 &ClarkeER60all_f(:,:)>=0.6);
%     ind=find(index_Resonant>0);
%     
%     matfreq_Resonant18=squeeze(freq_response(:,:,1));
%     matfreq_Resonant38=squeeze(freq_response(:,:,2));
%     matfreq_Resonant70=squeeze(freq_response(:,:,3));
%     matfreq_Resonant120=squeeze(freq_response(:,:,4));
%     matfreq_Resonant200=squeeze(freq_response(:,:,5));
%     
%     figure;boxplot([matfreq_Resonant18(ind) matfreq_Resonant38(ind) matfreq_Resonant70(ind) matfreq_Resonant120(ind) matfreq_Resonant200(ind)],[18 38 70 120 200]);
%      xlabel('Frequency ');
%             title(['Frequency response Resonant']);
%             saveas(gcf,[dirdata,'FrequencyResponseResonant'],'fig')
%             saveas(gcf,[dirdata,'FrequencyResponseResonant'],'jpg')
% 
% 
% %     figure;
% %     h2 = pcolor(slonn,slatm,mean(matPropER60f(:,:,5,2),3));
% %     hold on;
% %     if isempty(alpha) == 0
% %        set(h2,'EdgeColor','none','FaceColor','flat',...
% %             'FaceAlpha',alpha)
% %     end
% %     title([' ClarkeER60 frequency middle mean'],'FontSize',15)
% %     xlabel('Longitude [degree]','FontSize',15);
% %     ylabel('Latitude [degree]','FontSize',15);
% %     zlabel('ClarkeER60 frequency','FontSize',15)
% %     scatter(longER60all(1:100:end),latER60all(1:100:end),7,'filled');
% %      %%%text(longER60all(1:3000:end),latER60all(1:3000:end),datestr(timeER60all(1:3000:end)/86400 +719529,'HH'),'FontSize',8,'FontWeight','bold');
% %     colorbar('location','eastoutside')
% %      axis([longmin longmax latmin latmax]);
% %     caxis([colorminProp colormaxProp]);
% %     %         axis tight
% %     view(0,90);  grid on;;
% %     saveas(gcf,[dirdata,'ClarkeER60f_map_middle_mean'],'fig')
% %     saveas(gcf,[dirdata,'ClarkeER60f_map_middle_mean'],'jpg')
% %     
% %     
% % for layer=1:size(ClarkeER60all_f,2)
% %     figure;
% %     h3 = pcolor(slonn,slatm,matPropER60f(:,:,layer,2));
% %     hold on;
% %     if isempty(alpha) == 0
% %         set(h3,'EdgeColor','none','FaceColor','flat',...
% %             'FaceAlpha',alpha)
% %     end
% %     title([' ClarkeER60 frequency for layer', sprintf('%g',layer)],'FontSize',15)
% %     xlabel('Longitude [degree]','FontSize',15);
% %     ylabel('Latitude [degree]','FontSize',15);
% %     zlabel('ClarkeER60 frequency','FontSize',15)
% %     scatter(longER60all(1:100:end),latER60all(1:100:end),7,'filled');
% %      %%%text(longER60all(1:3000:end),latER60all(1:3000:end),datestr(timeER60all(1:3000:end)/86400 +719529,'HH'),'FontSize',8,'FontWeight','bold');
% %     colorbar('location','eastoutside')
% %  axis([longmin longmax latmin latmax]);
% %     caxis([colorminProp colormaxProp]);
% %     %         axis tight
% %     view(0,90);  grid on;;
% %     saveas(gcf,[dirdata,'ClarkeER60f_map_mean', sprintf('%g',layer)],'fig')
% %     saveas(gcf,[dirdata,'ClarkeER60f_map_mean', sprintf('%g',layer)],'jpg')
% %     close;
% % end
% % 
% % colorminProp= 0.65;
% % colormaxProp = 0.9;
% % 
% % for freq=1:size(matClarkeER60g,4)
% %     figure;
% %     h4 = pcolor(slonn,slatm,mean(matClarkeER60g(:,:,6:9,freq),3));
% %     hold on;
% %     if isempty(alpha) == 0
% %         set(h4,'EdgeColor','none','FaceColor','flat',...
% %             'FaceAlpha',alpha)
% %     end
% %     title([' ClarkeER60 group surface for frequency ', sprintf('%g',freq)],'FontSize',15)
% %     xlabel('Longitude [degree]','FontSize',15);
% %     ylabel('Latitude [degree]','FontSize',15);
% %     zlabel('ClarkeER60 group','FontSize',15)
% %     scatter(longER60all(1:100:end),latER60all(1:100:end),7,'filled');
% %     %%%text(longER60all(1:3000:end),latER60all(1:3000:end),datestr(timeER60all(1:3000:end)/86400 +719529,'HH'),'FontSize',8,'FontWeight','bold');
% %     colorbar('location','eastoutside')
% %      axis([longmin longmax latmin latmax]);
% %     caxis([colorminProp colormaxProp]);
% %     %         axis tight
% %     view(0,90);  grid on;;
% %     grid on;
% %     saveas(gcf,[dirdata,'ClarkeER60g_map_surface', sprintf('%g',freq)],'fig')
% %     saveas(gcf,[dirdata,'ClarkeER60g_map_surface', sprintf('%g',freq)],'jpg')
% %     close;
% % 
% %     figure;
% %     h4 = pcolor(slonn,slatm,std(matClarkeER60g(:,:,6:9,freq),0,3));
% %     hold on;
% %     if isempty(alpha) == 0
% %         set(h4,'EdgeColor','none','FaceColor','flat',...
% %             'FaceAlpha',alpha)
% %     end
% %     title([' ClarkeER60 group surface std for frequency ', sprintf('%g',freq)],'FontSize',15)
% %     xlabel('Longitude [degree]','FontSize',15);
% %     ylabel('Latitude [degree]','FontSize',15);
% %     zlabel('ClarkeER60 group','FontSize',15)
% %     scatter(longER60all(1:100:end),latER60all(1:100:end),7,'filled');
% %     %%%text(longER60all(1:3000:end),latER60all(1:3000:end),datestr(timeER60all(1:3000:end)/86400 +719529,'HH'),'FontSize',8,'FontWeight','bold');
% %     colorbar('location','eastoutside')
% %      axis([longmin longmax latmin latmax]);
% %     caxis([colorminClarkeStd colormaxClarkeStd]);
% %     %         axis tight
% %     view(0,90);  grid on;;
% %     saveas(gcf,[dirdata,'ClarkeER60g_map_surface_std', sprintf('%g',freq)],'fig')
% %     saveas(gcf,[dirdata,'ClarkeER60g_map_surface_std', sprintf('%g',freq)],'jpg')
% %     close;
% % 
% %     figure;
% %     h4 = pcolor(slonn,slatm,mean(matClarkeER60g(:,:,1:4,freq),3));
% %     hold on;
% %     if isempty(alpha) == 0
% %         set(h4,'EdgeColor','none','FaceColor','flat',...
% %             'FaceAlpha',alpha)
% %     end
% %     title([' ClarkeER60 group bottom for frequency ', sprintf('%g',freq)],'FontSize',15)
% %     xlabel('Longitude [degree]','FontSize',15);
% %     ylabel('Latitude [degree]','FontSize',15);
% %     zlabel('ClarkeER60 group','FontSize',15)
% %     scatter(longER60all(1:100:end),latER60all(1:100:end),7,'filled');
% %     %%%text(longER60all(1:3000:end),latER60all(1:3000:end),datestr(timeER60all(1:3000:end)/86400 +719529,'HH'),'FontSize',8,'FontWeight','bold');
% %     colorbar('location','eastoutside')
% %      axis([longmin longmax latmin latmax]);
% %     caxis([colorminProp colormaxProp]);
% %     %         axis tight
% %     view(0,90);  grid on;;
% %     grid on;
% %     saveas(gcf,[dirdata,'ClarkeER60g_map_bottom', sprintf('%g',freq)],'fig')
% %     saveas(gcf,[dirdata,'ClarkeER60g_map_bottom', sprintf('%g',freq)],'jpg')
% %     close;
% % 
% %     figure;
% %     h4 = pcolor(slonn,slatm,std(matClarkeER60g(:,:,1:4,freq),0,3));
% %     hold on;
% %     if isempty(alpha) == 0
% %         set(h4,'EdgeColor','none','FaceColor','flat',...
% %             'FaceAlpha',alpha)
% %     end
% %     title([' ClarkeER60 group bottom std for frequency ', sprintf('%g',freq)],'FontSize',15)
% %     xlabel('Longitude [degree]','FontSize',15);
% %     ylabel('Latitude [degree]','FontSize',15);
% %     zlabel('ClarkeER60 group','FontSize',15)
% %     scatter(longER60all(1:100:end),latER60all(1:100:end),7,'filled');
% %     %%%text(longER60all(1:3000:end),latER60all(1:3000:end),datestr(timeER60all(1:3000:end)/86400 +719529,'HH'),'FontSize',8,'FontWeight','bold');
% %     colorbar('location','eastoutside')
% %      axis([longmin longmax latmin latmax]);
% %     caxis([colorminClarkeStd colormaxClarkeStd]);
% %     %         axis tight
% %     view(0,90);  grid on;;
% %     saveas(gcf,[dirdata,'ClarkeER60g_map_bottom_std', sprintf('%g',freq)],'fig')
% %     saveas(gcf,[dirdata,'ClarkeER60g_map_bottom_std', sprintf('%g',freq)],'jpg')
% %     close;
% % 
% %     figure;
% %     h4 = pcolor(slonn,slatm,mean(matClarkeER60g(:,:,5),3));
% %     hold on;
% %     if isempty(alpha) == 0
% %         set(h4,'EdgeColor','none','FaceColor','flat',...
% %             'FaceAlpha',alpha)
% %     end
% %     title([' ClarkeER60 group middle for frequency ', sprintf('%g',freq)],'FontSize',15)
% %     xlabel('Longitude [degree]','FontSize',15);
% %     ylabel('Latitude [degree]','FontSize',15);
% %     zlabel('ClarkeER60 group','FontSize',15)
% %     scatter(longER60all(1:100:end),latER60all(1:100:end),7,'filled');
% %     %%%text(longER60all(1:3000:end),latER60all(1:3000:end),datestr(timeER60all(1:3000:end)/86400 +719529,'HH'),'FontSize',8,'FontWeight','bold');
% %     colorbar('location','eastoutside')
% %      axis([longmin longmax latmin latmax]);
% %     caxis([colorminProp colormaxProp]);
% %     %         axis tight
% %     view(0,90);  grid on;;
% %     saveas(gcf,[dirdata,'ClarkeER60g_map_middle', sprintf('%g',freq)],'fig')
% %     saveas(gcf,[dirdata,'ClarkeER60g_map_middle', sprintf('%g',freq)],'jpg')
% %     close;
% % 
% %     % colormin=min(min(min(nonzeros(matClarkeER60g(:,:,:)))));
% %     % colormax=max(max(max(matClarkeER60g(:,:,:))));
% %     % for layer=1:size(ClarkeER60all_f,2)
% %     %     figure;
% %     %     h5 = pcolor(slonn,slatn,matClarkeER60g(:,:,layer));
% %     %     hold on;
% %     %     if isempty(alpha) == 0
% %     %         set(h5,'EdgeColor','none','FaceColor','flat','FaceColor','flat',...
% %     %             'FaceAlpha',alpha)
% %     %     end
% %     %     title([' ClarkeER60 group for layer for frequency ', sprintf('%g',freq), sprintf('%g',layer)],'FontSize',15)
% %     %     xlabel('Longitude [degree]','FontSize',15);
% %     %     ylabel('Latitude [degree]','FontSize',15);
% %     %     zlabel('ClarkeER60 group','FontSize',15)
% %     %     scatter(longER60all(1:100:end),latER60all(1:100:end),7,'filled');
% %     %      %%%text(longER60all(1:3000:end),latER60all(1:3000:end),datestr(timeER60all(1:3000:end)/86400 +719529,'HH'),'FontSize',8,'FontWeight','bold');
% %     %     colorbar('location','eastoutside')
% % %      axis([longmin longmax latmin latmax]);
% %     %     caxis([colormin colormax]);
% %     %     %         axis tight
% %     %     view(0,90);
% %     %     saveas(gcf,[dirdata,'ClarkeER60g_map', sprintf('%g',layer), sprintf('%g',freq)],'fig')
% %     %     saveas(gcf,[dirdata,'ClarkeER60g_map', sprintf('%g',layer), sprintf('%g',freq)],'jpg')
% %     % end
% % 
% % end
% % 
% % index=find(depthER60all<200);
% %         
% %              [year, month, day, hour, mn, s] = datevec(timeER60all/86400 +719529);
% %  
% % 
% %              for layer=1:size(ClarkeER60all_f,2)
% %                  figure;
% %                  scatter(hour(index)', squeeze(ClarkeER60all_f(index,layer)),10,'x');
% %                  %         datetick('x','HH:MM');
% %                  xlabel('Time (HH)');
% %                  title(['Clarke ER60 frequency layer', sprintf('%g',layer)]);
% %                  %         legend('bot1','bot2','bot3','bot4','middle','surf4','surf3','surf2','surf1');
% %                  saveas(gcf,[dirdata,'ClarkeER60f_time_layer', sprintf('%g',layer)],'fig')
% %                  saveas(gcf,[dirdata,'ClarkeER60f_time_layer', sprintf('%g',layer)],'bmp')
% %                  close;
% % 
% % 
% % 
% %                  A=[depthER60all(index)', squeeze(ClarkeER60all_f(index,layer))];
% %                  B=sortrows(A,1);
% %                  figure;
% %                  scatter(B(:,1),B(:,2),10,'x');
% %                  xlabel('Depth (m)');
% %                  title(['Clarke ER60 frequency layer', sprintf('%g',layer)]);
% %                  %         legend('bot1','bot2','bot3','bot4','middle','surf4','surf3','surf2','surf1');
% %                  saveas(gcf,[dirdata,'ClarkeER60f_depth_layer', sprintf('%g',layer)],'fig')
% %                  saveas(gcf,[dirdata,'ClarkeER60f_depth_layer', sprintf('%g',layer)],'bmp')
% %                  close;
% %              end
% %                      
% % 
% %         index=find(depthER60all<200 & ClarkeER60all_f(:,1,3)'>0);
% %         [y, m, d, h, mn, s] = datevec(timeER60all/86400 +719529);
% % 
% %         for layer=1:size(ClarkeER60all_f,2)
% %             figure;
% %             scatter(h(index)', squeeze(mean((ClarkeER60all_f(index,layer,3)),2)),10,'x');
% %             %         datetick('x','HH:MM');
% %             xlabel('Time (HH)');
% %             title(['Clarke ER60 group layer', sprintf('%g',layer)]);
% %             %         legend('bot1','bot2','bot3','bot4','middle','surf4','surf3','surf2','surf1');
% %             axis([0 24 0.6 0.85]);
% %             saveas(gcf,[dirdata,'ClarkeER60g_time_layer', sprintf('%g',layer)],'fig')
% %             saveas(gcf,[dirdata,'ClarkeER60g_time_layer', sprintf('%g',layer)],'jpg')
% %             close;
% % 
% %             A=[depthER60all(index)', squeeze(ClarkeER60all_f(index,layer,3))];
% %             B=sortrows(A,1);
% %             figure;
% %             scatter(B(:,1),B(:,2),10,'x');
% %             xlabel('Depth (m)');
% %             title(['Clarke ER60 group layer', sprintf('%g',layer)]);
% %             %         legend('bot1','bot2','bot3','bot4','middle','surf4','surf3','surf2','surf1');
% %             axis([0 200 0.6 0.85]);
% %             saveas(gcf,[dirdata,'ClarkeER60g_depth_layer', sprintf('%g',layer)],'fig')
% %             saveas(gcf,[dirdata,'ClarkeER60g_depth_layer', sprintf('%g',layer)],'jpg')
% %             close;
% %         end
% % % 
% % 
% %         A=[depthER60all(index)', squeeze(mean(ClarkeER60all_f(index,6:9),2))];
% %         B=sortrows(A,1);
% %         figure;
% %         scatter(B(:,1),B(:,2));
% %         xlabel('Depth (m)');
% %         title(['Clarke ER60 group surface']);
% % %         legend('bot1','bot2','bot3','bot4','middle','surf4','surf3','surf2','surf1');
% %         saveas(gcf,[dirdata,'ClarkeER60g_depth_surface', sprintf('%g',freq)],'fig')
% %         saveas(gcf,[dirdata,'ClarkeER60g_depth_surface', sprintf('%g',freq)],'jpg')
% %         close;
% %         
% %            A=[depthER60all(index)', squeeze(mean(ClarkeER60all_f(index,1:4),2))];
% %         B=sortrows(A,1);
% %         figure;
% %         scatter(B(:,1),B(:,2));
% %         xlabel('Depth (m)');
% %         title(['Clarke ER60 group bottom']);
% % %         legend('bot1','bot2','bot3','bot4','middle','surf4','surf3','surf2','surf1');
% %         saveas(gcf,[dirdata,'ClarkeER60g_depth_bottom', sprintf('%g',freq)],'fig')
% %         saveas(gcf,[dirdata,'ClarkeER60g_depth_bottom', sprintf('%g',freq)],'jpg')
% %         close;
% %         
% %            A=[depthER60all(index)', squeeze(mean(ClarkeER60all_f(index,5),2))];
% %         B=sortrows(A,1);
% %         figure;
% %         scatter(B(:,1),B(:,2));
% %         xlabel('Depth (m)');
% %         title(['Clarke ER60 group middle']);
% % %         legend('bot1','bot2','bot3','bot4','middle','surf4','surf3','surf2','surf1');
% %         saveas(gcf,[dirdata,'ClarkeER60g_depth_middle', sprintf('%g',freq)],'fig')
% %         saveas(gcf,[dirdata,'ClarkeER60g_depth_middle', sprintf('%g',freq)],'jpg')
% %         close;
% % 
% %         
% %         
% %         for freq=1:nbfreqER60
% %         figure;
% %         plot(timeER60(1:100:nbzoneER60*100)', squeeze(delta_ER60g(:,:,freq)));
% %         datetick('x','HH:MM');
% %         xlabel('Time (HH:MM)');
% %         title(['Clarke ER60 group for frequency ', sprintf('%g',freq)]);
% %         legend('bot1','bot2','bot3','bot4','middle','surf4','surf3','surf2','surf1');
% %         saveas(gcf,[chemin_results,'ClarkeER60g_time', sprintf('%g',freq)],'fig')
% %         saveas(gcf,[chemin_results,'ClarkeER60g_time', sprintf('%g',freq)],'jpg')
% %         close;
% % 
% %         A=[depth_bottom_ER60(1:100:nbzoneER60*100,1,freq), squeeze(delta_ER60g(:,:,freq))];
% %         B=sortrows(A,1);
% %         figure;
% %         plot(B(:,1),B(:,2:nblayerER60+1));
% %         xlabel('Depth (m)');
% %         title(['Clarke ER60 group for frequency ', sprintf('%g',freq)]);
% %         legend('bot1','bot2','bot3','bot4','middle','surf4','surf3','surf2','surf1');
% %         saveas(gcf,[chemin_results,'ClarkeER60g_depth', sprintf('%g',freq)],'fig')
% %         saveas(gcf,[chemin_results,'ClarkeER60g_depth', sprintf('%g',freq)],'jpg')
% %         close;
% % 
% %     end
% % 
% %     
% %     for freq=1:nbfreqER60
% %         figure;
% %         plot(timeER60all, squeeze(ClarkeER60all_f(:,:,freq)));
% %         datetick('x','HH:MM');
% %         xlabel('Time (HH:MM)');
% %         title(['Shannon ER60 group for frequency ', sprintf('%g',freq)]);
% %         legend('bot1','bot2','bot3','bot4','middle','surf4','surf3','surf2','surf1');
% %         saveas(gcf,[chemin_results,'ShannonER60g_time', sprintf('%g',freq)],'fig')
% %         saveas(gcf,[chemin_results,'ShannonER60g_time', sprintf('%g',freq)],'jpg')
% %         close;
% % 
% %         A=[depth_bottom_ER60(1:100:nbzoneER60*100,1,freq), squeeze(H_ER60g(:,:,freq))];
% %         B=sortrows(A,1);
% %         figure;
% %         plot(B(:,1),B(:,2:nblayerER60+1));
% %         xlabel('Depth (m)');
% %         title(['Shannon ER60 group for frequency ', sprintf('%g',freq)]);
% %         legend('bot1','bot2','bot3','bot4','middle','surf4','surf3','surf2','surf1');
% %         saveas(gcf,[chemin_results,'ShannonER60g_depth', sprintf('%g',freq)],'fig')
% %         saveas(gcf,[chemin_results,'ShannonER60g_depth', sprintf('%g',freq)],'jpg')
% %         close;
% %     end
% %     for freq=1:nbfreqER60
% %         figure;
% %         plot(timeER60(1:100:nbzoneER60*100)', squeeze(delta_ER60g(:,:,freq)));
% %         datetick('x','HH:MM');
% %         xlabel('Time (HH:MM)');
% %         title(['Clarke ER60 group for frequency ', sprintf('%g',freq)]);
% %         legend('bot1','bot2','bot3','bot4','middle','surf4','surf3','surf2','surf1');
% %         saveas(gcf,[chemin_results,'ClarkeER60g_time', sprintf('%g',freq)],'fig')
% %         saveas(gcf,[chemin_results,'ClarkeER60g_time', sprintf('%g',freq)],'jpg')
% %         close;
% % 
% %         A=[depth_bottom_ER60(1:100:nbzoneER60*100,1,freq), squeeze(delta_ER60g(:,:,freq))];
% %         B=sortrows(A,1);
% %         figure;
% %         plot(B(:,1),B(:,2:nblayerER60+1));
% %         xlabel('Depth (m)');
% %         title(['Clarke ER60 group for frequency ', sprintf('%g',freq)]);
% %         legend('bot1','bot2','bot3','bot4','middle','surf4','surf3','surf2','surf1');
% %         saveas(gcf,[chemin_results,'ClarkeER60g_depth', sprintf('%g',freq)],'fig')
% %         saveas(gcf,[chemin_results,'ClarkeER60g_depth', sprintf('%g',freq)],'jpg')
% %         close;
% % 
% %     end
