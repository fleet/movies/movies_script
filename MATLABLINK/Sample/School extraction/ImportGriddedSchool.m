
filename='C:\Users\lberger\Desktop\Pour Laurent\re-echantillonage Anchois 01ind mean 10.csv';

% filename='C:\Users\lberger\Desktop\Pour Laurent\re-echantillonage anchois moyen 10.csv';

formatSpec='%s%f%d%[^\n\r]';

fileID = fopen(filename,'r');

dataArray = textscan(fileID, formatSpec, 'Delimiter', ',','Headerlines',1);

% r�cup�ration du sv des �chos en naturel et des indices de grille par �cho
sv=dataArray{2};
index=dataArray{3};

% liste des indices de grille
index_unique=unique(index);

%Fit de Weibull
parmhat=[];
nbech_valid=[];
index_unique_valid=[];
Ypd=[];
idvalid=1;
for i=1:length(index_unique)
    numech=find(index==index_unique(i));
    if length(numech)>10
        [parmhat(idvalid,:)] = wblfit(sqrt(sv(numech)));
         Ypd{idvalid}= wblpdf(sqrt(sv(numech)),parmhat(idvalid,1),parmhat(idvalid,2));
         index_unique_valid(idvalid)=index_unique(i);
         nbech_valid(idvalid)=length(numech);
         idvalid=idvalid+1;
    end
end

% Affichage du param�tre beta de Weibull fitt� en fonction du nombre
% d'�chos pour chaque point de grille
figure;
plot(nbech_valid,(parmhat(:,2)),'*');
title('Test Rayleigh banc 1 ind/m3 60 m profondeur grille 3*3*1');ylabel('Beta Weibull');xlabel('Nombre de points par pas de grille')
