function SampleShoalExtraction(chemin_hac,chemin_save,ShoalExtractModule)

    warning('off');
    moOpenHac(chemin_hac);

    num_bloc=1;

    FileStatus= moGetFileStatus;
    %on ne boucle pas : on ne lit qu'un bloc...
    while FileStatus.m_StreamClosed < 1

        schools_ER60 = [];
        schools_ME70 = [];
     
        moReadChunk();
      
        % on souhaite r�cuperer les informations ME70 et EK60 MFR dans le
        % cas o� l'on a �galement un sondeur horizontal
        indexSounderER60=0;
        indexSounderER60h=3;
        indexSounderME70=2;
        nbchannelsME70=0;
        nbchannelsER60=1;
        nbchannelsER60h=1;
        %liste des sondeurs
        list_sounder=moGetSounderList;
        % r�cup�ration du nombre de sondeur
        sounderNb = length(moGetSounderList);        
        
        %pour chaque sondeur identifie
        for indexSounder=1:sounderNb
            %test si sondeur multifaisceaux (>6faisceaux)
            if list_sounder(indexSounder).m_nbBeamPerFan >6 %ME70
                indexSounderME70=list_sounder(indexSounder).m_SounderId;
                nbchannelsME70=list_sounder(indexSounder).m_nbBeamPerFan;
            elseif list_sounder(indexSounder).m_nbBeamPerFan >1 %ER60 MFR
                indexSounderER60=list_sounder(indexSounder).m_SounderId;
                nbchannelsER60=list_sounder(indexSounder).m_nbBeamPerFan;
            elseif size(list_sounder(indexSounder).m_nbBeamPerFan,2) ==1 %ER60 lateral
                indexSounderER60h=list_sounder(indexSounder).m_SounderId;
                nbchannelsER60h=list_sounder(indexSounder).m_nbBeamPerFan;    
            end
        end        
        
%         %on r�cup�re l'ordre des r�sultats des fr�quences ER60 pour les sauvegarder toujours dans le m�me ordre
%         % on suppose qu'il y a un seul sondeur multi-transducteur !
%         nb_snd=length(list_sounder);
%         indexER60=1;
%         list_freqER60=zeros(nbchannelsER60,1);
%         %for isdr = 1:nb_snd
%             nb_transduc=list_sounder(indexSounderER60).m_numberOfTransducer;
%             if (nb_transduc>1)
%                 for itr=1:nb_transduc
%                     list_freqER60(itr)=list_sounder(indexSounderER60).m_transducer(itr).m_SoftChannel.m_acousticFrequency;
%                 end
%                 %[~,indexER60]=sort(list_freqER60);
%                 [B,IX]=sort(list_freqER60);
%                 indexER60=IX;
%             end
%         %end    
                
        AllData = moGetOutPutList(ShoalExtractModule);

        % nombre de bancs a traiter    
        nbSchools = size(AllData,2) 

        indexME70 = 1;
        indexER60 = 1;
        for indexSchool=1:nbSchools
            nbbeams=size(AllData(1,indexSchool).m_length,2);
            sndID=AllData(1,indexSchool).m_sounderId;
            if sndID==indexSounderME70
                schools_ME70(indexME70).time = AllData(1,indexSchool).m_startTS/86400 +719529;
                schools_ME70(indexME70).volume = AllData(1,indexSchool).m_volume;
                schools_ME70(indexME70).length = AllData(1,indexSchool).m_length;
                schools_ME70(indexME70).width = AllData(1,indexSchool).m_width;
                schools_ME70(indexME70).height = AllData(1,indexSchool).m_height;
                schools_ME70(indexME70).MVBS_weighted = AllData(1,indexSchool).m_MVBS_weighted;
                schools_ME70(indexME70).sigma_ag = AllData(1,indexSchool).m_sigma_ag;
                schools_ME70(indexME70).position(1) = AllData(1,indexSchool).m_energyCenterLat;
                schools_ME70(indexME70).position(2) = AllData(1,indexSchool).m_energyCenterLong;
                schools_ME70(indexME70).position(3) = AllData(1,indexSchool).m_energyCenterDepth;
                schools_ME70(indexME70).contours = AllData(1,indexSchool).m_contourPoints;
                schools_ME70(indexME70).echoes = AllData(1,indexSchool).m_echoData;
                indexME70=indexME70+1;
            elseif sndID==indexSounderER60                  
                    if (AllData(1,indexSchool).m_transId==0)
                        schools_ER60(indexER60).time = AllData(1,indexSchool).m_startTS/86400 +719529;
                        schools_ER60(indexER60).volume = AllData(1,indexSchool).m_volume;
                        schools_ER60(indexER60).length = AllData(1,indexSchool).m_length;
                        schools_ER60(indexER60).width = AllData(1,indexSchool).m_width;
                        schools_ER60(indexER60).height = AllData(1,indexSchool).m_height;
                        schools_ER60(indexER60).MVBS_weighted = AllData(1,indexSchool).m_MVBS_weighted;
                        schools_ER60(indexER60).sigma_ag = AllData(1,indexSchool).m_sigma_ag;
                        schools_ER60(indexER60).position(1) = AllData(1,indexSchool).m_energyCenterLat;
                        schools_ER60(indexER60).position(2) = AllData(1,indexSchool).m_energyCenterLong;
                        schools_ER60(indexER60).position(3) = AllData(1,indexSchool).m_energyCenterDepth;
                        schools_ER60(indexER60).contours = AllData(1,indexSchool).m_contourPoints;
                        schools_ER60(indexER60).echoes = AllData(1,indexSchool).m_echoData;
                        indexER60=indexER60+1;
                    end
            end                        
        end
      
        clear AllData;

        str_bloc='00000';        
        str_bloc(end-length(num2str(num_bloc))+1:end)=num2str(num_bloc);
        if (nbSchools >0)
        save ([chemin_save, '/results','_', str_bloc,'_school'],'schools_ER60','schools_ME70') ;
        end
        clear schools_ER60 schools_ME70;

        FileStatus= moGetFileStatus();
        num_bloc=num_bloc + 1;
    end
end












