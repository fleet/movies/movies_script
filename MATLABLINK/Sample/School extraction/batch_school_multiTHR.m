% This program computes echo-integration for raw HAC files using the MOVIES3D libraries

% The HAC files are stored in folders named RUNXXX for each day of the
% survey as stored by MOVIES+ or HERMES
% The configuration for reading HAC files, filtering, and defining the cell
% size for integration and threshold are located in a dedicated folder with
% one sub folder for each threshold.

%Input parameters
%   path_hac_survey : path for HAC files 
%   path_config     : path for xml configuration files created with MOVIES3D
%   path_result     : path for .mat result files for echo-integration
%   thresholds      : list of thresholds for echo-integration,
%                   corresponding configuration have been created before
%   runs            : list of runs for echo-integration

function batch_school_multiTHR(path_hac_survey,path_conf,path_result,thresholds,runs,esu)

for ir=1:length(runs)
    for t=1:size(thresholds,2)
        %build dedicated string
        str_run(ir,:)='000';
        str_run(ir,end-length(num2str(runs(ir)))+1:end)=num2str(runs(ir));
        path_config=[path_conf,'/',num2str(thresholds(:,t)),'/',num2str(esu)];
        if (~exist(path_config))
           error([path_config,' : Invalid path for MOVIES3D configuration'])
        end
        
       path_hac=[path_hac_survey,'/RUN' str_run(ir,:)];
        if (~exist(path_hac))
            error([path_hac,' : Invalid path for HAC configuration'])
        else
            filelist = ls([path_hac,'/*.hac']);  % ensemble des fichiers hac
            nb_files = size(filelist,1);  % nombre de fichiers
            if nb_files==0
                 error([path_hac,' does not contain HAC files'])
            end
        end
        
        path_save=[path_result,'/RUN' str_run(ir,:), '/',num2str(thresholds(:,t)),'/',num2str(esu)];
        if ~exist(path_save,'dir')
           mkdir(path_save)
        end
       
        %load configuration
        path_config_reverse = strrep(path_config, '\', '/');
        moLoadConfig(path_config_reverse);
   
        %Activate 'extractionModule'
        ShoalExtractModule = moShoalExtraction();
        moSetShoalExtractionEnabled(ShoalExtractModule);
        
        %Run Shoal Extraction
        SampleShoalExtraction(path_hac,path_save,ShoalExtractModule);
   
        %Disable 'ShoalExtractModule'
        moSetShoalExtractionDisabled(ShoalExtractModule);
        clear ShoalExtractModule;
    end
end;