function D=distance3D(position_ME701,position_ME702)

R=6378000;

pos1_cart.x=R*cos(position_ME701(1)*pi/180)*cos(position_ME701(2)*pi/180);
pos1_cart.y=R*cos(position_ME701(1)*pi/180)*sin(position_ME701(2)*pi/180);
pos1_cart.z=R*sin(position_ME701(1)*pi/180)-position_ME701(3);

pos2_cart.x=R*cos(position_ME702(1)*pi/180)*cos(position_ME702(2)*pi/180);
pos2_cart.y=R*cos(position_ME702(1)*pi/180)*sin(position_ME702(2)*pi/180);
pos2_cart.z=R*sin(position_ME702(1)*pi/180)-position_ME702(3);

D=sqrt((pos1_cart.x-pos2_cart.x)^2+(pos1_cart.y-pos2_cart.y)^2+(pos1_cart.z-pos2_cart.z)^2);





