%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Shoals extraction program
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all;

% keep path_acoustic_data path_config_shoal_extraction fishing_trip esu_size_shoal thresholds;

%% Set pathsl
%add path to scripts
% addpath('./privat_EI_shoals/');
path_acoustic_data='C:\data\SimuNans\';
type_donnees = 'hac-filtered';
% path_hac_survey = [path_acoustic_data,fishing_trip,'\',type_donnees];
% path_save=[path_acoustic_data,fishing_trip,'/results_shoal_extraction'];
path_hac_survey=path_acoustic_data;
path_save=path_acoustic_data;
path_config_shoal_extraction='C:\data\SimuNans\Config_M3D\EIShl';
thresholds=[-60:1:-60];
esu_size_shoal=1;

%If save path does not exist, create it
if ~exist(path_save,'dir')
    mkdir(path_save)
end

%% Echo-integration par bancs
% peut �tre comment�e si d�j� ex�cut�e
runs = 2;
  batch_school_multiTHR(path_hac_survey,path_config_shoal_extraction,path_save,thresholds,runs,esu_size_shoal);
  
%% Bind school results matrices
for ir=1:length(runs)
    for t=1:size(thresholds,2)
        str_run(ir,:)='000';
        str_run(ir,end-length(num2str(runs(ir)))+1:end)=num2str(runs(ir));
        %filename_ME70=[path_save,'/RUN' str_run(ir,:) '/',num2str(thresholds(:,t)),'/',survey_name,'-RUN' str_run(ir,:),'-TH' num2str(thresholds(t)),'-ME70-EIlay.mat'];
        filename_ME70=[path_save,'/RUN' str_run(ir,:),'/',num2str(thresholds(t)),'/','RUN' str_run(ir,:),'-TH' num2str(thresholds(t)),'-ME70-EIlay.mat'];
        filename_ER60=[path_save,'/RUN' str_run(ir,:) '/',num2str(thresholds(:,t)),'/','RUN' str_run(ir,:),'-TH' num2str(thresholds(t)),'-ER60-EIlay.mat'];
        %     filename_ER60=[path_save,'/',num2str(thresholds(t)),'/',num2str(esu_size_shoal),'/ER60-EIschools.mat'];
        path_results=[path_save,'/RUN' str_run(ir,:) '/','/',num2str(thresholds(t)),'/',num2str(esu_size_shoal),'/'];
        school_res_bind(path_results,filename_ER60,filename_ME70,thresholds(t));
    end
end


% 
% 
% 
% 
% %% Exploratory plots
% seuil_sigma_ag=0;
% seuil_volume = 0;
% 
% percentiledepth = [];
% occurrence = [];
% 
% ir=1;
% for i=1:length(thresholds)
%     filename_ER60=[path_save,'/RUN' str_run(ir,:) '/',num2str(thresholds(:,i)),'/','RUN' str_run(ir,:),'-TH' num2str(thresholds(i)),'-ER60-EIlay.mat'];
%     load(filename_ER60);
% %recupere les dates de debut et fin de la s�quence trait�e
% debut=datenum(time_ER60(1));
% fin=datenum(time_ER60(end));
% index=find((time_ER60)>=debut & (time_ER60)<=fin & volume_ER60>seuil_volume);  
%     percentiledepth(i,:)=prctile(position_ER60(find(seuil_ER60(index)==thresholds(i)),3),[5 25 50 75 95]);
%     occurrencedepth(i)=size(position_ER60(find(seuil_ER60(index)==thresholds(i)),3),1);
%      percentilelength(i,:)=prctile(length_ER60(find(seuil_ER60(index)==thresholds(i))),[5 25 50 75 95]);
%     occurencelength(i)=size(length_ER60(find(seuil_ER60(index)==thresholds(i))),2);
%      percentileWidth(i,:)=prctile(width_ER60(find(seuil_ER60(index)==thresholds(i))),[5 25 50 75 95]);
%     occurenceWidth(i)=size(width_ER60(find(seuil_ER60(index)==thresholds(i))),2);
%         percentileHeight(i,:)=prctile(height_ER60(find(seuil_ER60(index)==thresholds(i))),[5 25 50 75 95]);
%     occurenceHeight(i)=size(height_ER60(find(seuil_ER60(index)==thresholds(i))),2);
%         percentileVolume(i,:)=prctile(volume_ER60(find(seuil_ER60(index)==thresholds(i))),[5 25 50 75 95]);
%     occurenceVolume(i)=size(volume_ER60(find(seuil_ER60(index)==thresholds(i))),2);
%      percentileAspect(i,:)=prctile(volume_ER60(find(seuil_ER60(index)==thresholds(i)))./(height_ER60(find(seuil_ER60(index)==thresholds(i))).*width_ER60(find(seuil_ER60(index)==thresholds(i))).*length_ER60(find(seuil_ER60(index)==thresholds(i)))),[5 25 50 75 95]);
%     occurenceAspect(i)=size(length_ER60(find(seuil_ER60(index)==thresholds(i))),2);
%     percentileMVBS(i,:)=prctile(MVBS_ER60(find(seuil_ER60(index)==thresholds(i))),[5 25 50 75 95]);
%     occurenceMVBS(i)=size(MVBS_ER60(find(seuil_ER60(index)==thresholds(i))),2);
%      volume_ratio(i)=sum(volume_ER60(seuil_ER60(index)==thresholds(i)))/sum(volume_ER60(seuil_ER60(index)==min(thresholds)));
%      sigma_ag_ratio(i)=sum(sigma_ag_ER60(seuil_ER60(index)==thresholds(i)))/sum(sigma_ag_ER60(seuil_ER60(index)==min(thresholds)));
% end
% 
% %% Reorganisation des matrices suivant la date
% [time_ER60,I] = sort(time_ER60);
% length_ER60 = length_ER60(I);
% width_ER60 = width_ER60(I);
% height_ER60 = height_ER60(I);
% volume_ER60 = volume_ER60(I);
% MVBS_ER60 = MVBS_ER60(I);
% sigma_ag_ER60 = sigma_ag_ER60(I);
% position_ER60 = position_ER60(I,:);
% seuil_ER60 = seuil_ER60(I);
% clear I;
% 
% figure;
% [AX,H1,H2] = plotyy(thresholds,percentiledepth,thresholds,occurrencedepth);
% set(get(AX(1),'Ylabel'),'String','depth (m)')
% set(get(AX(2),'Ylabel'),'String','Occurence')
% set(gca, 'YTickMode', 'auto');
% xlabel('Threshold (dB)');
% 
% legend('Occurence','5% percentile', '25% percentile','50% percentile', '75% percentile','95% percentile');
% title('depth distribution of schools at various threshold')
% saveas(gcf,[path_save,sprintf('depth')],'fig')
% saveas(gcf,[path_save,sprintf('depth')],'jpg')
% 
% figure;
% [AX,H1,H2] = plotyy(thresholds,percentilelength,thresholds,occurencelength);
% set(get(AX(1),'Ylabel'),'String','Length (m)')
% set(get(AX(2),'Ylabel'),'String','Occurence')
% set(gca, 'YTickMode', 'auto');
% xlabel('Threshold (dB)');
% 
% legend('Occurence','5% percentile', '25% percentile','50% percentile', '75% percentile','95% percentile');
% title('Length distribution of schools at various threshold')
% saveas(gcf,[path_save,sprintf('Length')],'fig')
% saveas(gcf,[path_save,sprintf('Length')],'jpg')
% 
% 
% figure;
% [AX,H1,H2] = plotyy(thresholds,percentileWidth,thresholds,occurenceWidth);
% set(get(AX(1),'Ylabel'),'String','Width (m)')
% set(get(AX(2),'Ylabel'),'String','Occurence')
% set(gca, 'YTickMode', 'auto');
% xlabel('Threshold (dB)');
% 
% legend('Occurence','5% percentile', '25% percentile','50% percentile', '75% percentile','95% percentile');
% title('Width distribution of schools at various threshold')
% saveas(gcf,[path_save,sprintf('Width')],'fig')
% saveas(gcf,[path_save,sprintf('Width')],'jpg')
% 
% figure;
% [AX,H1,H2] = plotyy(thresholds,percentileHeight,thresholds,occurenceHeight);
% set(get(AX(1),'Ylabel'),'String','Height (m)')
% set(get(AX(2),'Ylabel'),'String','Occurence')
% set(gca, 'YTickMode', 'auto');
% xlabel('Threshold (dB)');
% 
% legend('Occurence','5% percentile', '25% percentile','50% percentile', '75% percentile','95% percentile');
% title('Height distribution of schools at various threshold')
% saveas(gcf,[path_save,sprintf('Height')],'fig')
% saveas(gcf,[path_save,sprintf('Height')],'jpg')
% 
% figure;
% [AX,H1,H2] = plotyy(thresholds,percentileVolume,thresholds,occurenceVolume);
% set(get(AX(1),'Ylabel'),'String','Volume (m3)')
% set(get(AX(2),'Ylabel'),'String','Occurence')
% set(gca, 'YTickMode', 'auto');
% xlabel('Threshold (dB)');
% 
% legend('Occurence','5% percentile', '25% percentile','50% percentile', '75% percentile','95% percentile');
% title('Volume distribution of schools at various threshold')
% saveas(gcf,[path_save,sprintf('Volume')],'fig')
% saveas(gcf,[path_save,sprintf('Volume')],'jpg')
% 
% figure;
% [AX,H1,H2] = plotyy(thresholds,percentileAspect,thresholds,occurenceAspect);
% set(get(AX(1),'Ylabel'),'String','Aspect (unitless)')
% set(get(AX(2),'Ylabel'),'String','Occurence')
% set(gca, 'YTickMode', 'auto');
% xlabel('Threshold (dB)');
% 
% legend('Occurence','5% percentile', '25% percentile','50% percentile', '75% percentile','95% percentile');
% title('Aspect distribution of schools at various threshold (Volume ratio of school relative to bounding box)')
% saveas(gcf,[path_save,sprintf('Aspect')],'fig')
% saveas(gcf,[path_save,sprintf('Aspect')],'jpg')
% 
% figure;
% [AX,H1,H2] = plotyy(thresholds,percentileMVBS,thresholds,occurenceMVBS);
% set(get(AX(1),'Ylabel'),'String','MVBS (dB)')
% set(get(AX(2),'Ylabel'),'String','Occurence')
% set(gca, 'YTickMode', 'auto');
% xlabel('Threshold (dB)');
% 
% legend('Occurence','5% percentile', '25% percentile','50% percentile', '75% percentile','95% percentile');
% title('MVBS distribution of schools at various threshold')
% saveas(gcf,[path_save,sprintf('MVBS')],'fig')
% saveas(gcf,[path_save,sprintf('MVBS')],'jpg')
% 
% 
% 
% % D=zeros(size(position_ER60(index),2),21);
% % for i=1:size(position_ER60(index),2)
% %     k=1;
% %     for j=max(1,i-10):min(i+10,size(position_ER60(index),2))
% %         D(i,k)=distance3D(position_ER60(index(i),:),position_ER60(index(j),:));
% %         k=k+1;
% %     end
% % end
% % 
% % for i=1:size(position_ER60(index),2)
% %     Dmin(i)=min(D(i,find(D(i,:)>0)));
% % end
% % 
% % for i=1:nb_seuil
% %     percentileDistance(i,:)=prctile(Dmin(find(seuil_ER60(index)==seuil(i))),[5 25 50 75 95]);
% %     occurence(i)=size(Dmin(find(seuil_ER60(index)==seuil(i))),2);
% % end
% % figure;
% % [AX,H1,H2] = plotyy(seuil,percentileDistance,seuil,occurence);
% % set(get(AX(1),'Ylabel'),'String','Distance (m)')
% % set(get(AX(2),'Ylabel'),'String','Occurence')
% % xlabel('Threshold (dB)');
% % 
% % legend('Occurence','5% percentile', '50% percentile', '95% percentile');
% % title('Distance distribution of schools various threshold')
% % saveas(gcf,[path_save,sprintf('Distance')],'fig')
% % saveas(gcf,[path_save,sprintf('Distance')],'jpg')
% 
% % figure;
% % boxplot(Dmin,seuil_ER60(index))
% % xlabel('Threshold (dB)');
% % ylabel('Distance (dB)');
% % title('Distance between schools for various threshold')
% % saveas(gcf,[path_save,sprintf('Distance ')],'fig')
% % saveas(gcf,[path_save,sprintf('Distance ')],'jpg')
% 
% 
% 
% % figure;
% % plot(thresholds,volume_ratio);
% % xlabel('Threshold (dB)');
% % ylabel('Volume ratio (unitless)');
% % title('Volume ratio relative to lowest threshold')
% % saveas(gcf,[path_save,sprintf('Volume ratio')],'fig')
% % saveas(gcf,[path_save,sprintf('Volume ratio')],'jpg')
% % 
% % figure;
% % plot(thresholds,sigma_ag_ratio);
% % xlabel('Threshold (dB)');
% % ylabel('Backscatter ratio (m)');
% % title('Backscatter ratio relative to lowest threshold')
% % saveas(gcf,[path_save,sprintf('Backscatter ratio ')],'fig')
% % saveas(gcf,[path_save,sprintf('Backscatter ratio ')],'jpg')
% 
% 
% 
% for i=1:length(thresholds)
% filename_ER60=[path_save,'/RUN' str_run(ir,:) '/',num2str(thresholds(:,i)),'/','RUN' str_run(ir,:),'-TH' num2str(thresholds(i)),'-ER60-EIlay.mat'];
%     load(filename_ER60);
% %recupere les dates de debut et fin de la s�quence trait�e
% debut=datenum(time_ER60(1));
% fin=datenum(time_ER60(end));
% index=find((time_ER60)>=debut & (time_ER60)<=fin & volume_ER60>seuil_volume);  
%     figure
%     scatter3(position_ER60(find(seuil_ER60()==thresholds(i)),1),position_ER60(find(seuil_ER60()==thresholds(i)),2),-position_ER60(find(seuil_ER60()==thresholds(i)),3),sqrt(length_ER60(find(seuil_ER60()==thresholds(i))).*width_ER60(find(seuil_ER60()==thresholds(i)))),MVBS_ER60(find(seuil_ER60()==thresholds(i))));
%     hold on;
% %     scatter3(reshape(Latitude(:,indexNav(1:10:end)),1,[]),reshape(Longitude(:,indexNav(1:10:end)),1,[]),-reshape(Depth(:,indexNav(1:10:end)),1,[]),'k');
%     xlabel('Latitude (�)');
%     ylabel('Longitude (�)');
%     ylabel('Depth (m)');
%     title(['Spatial distribution of schools at ',num2str(thresholds(i)),'dB threshold, size represents length color represents density'])
%     colorbar;
%     axis([min(position_ER60(:,1)) max(position_ER60(:,1)) min(position_ER60(:,2)) max(position_ER60(:,2)) min(-position_ER60(:,3)) max(-position_ER60(:,3)) min(MVBS_ER60(:)) max(MVBS_ER60(:))]);
%     view(-40,20);
%     saveas(gcf,[path_save,'r�partition spatiale seuil ',num2str(thresholds(i)),'dB'],'fig')
%     saveas(gcf,[path_save,'r�partition spatiale seuil ',num2str(thresholds(i)),'dB'],'jpg')
% 
% 
% end
% 
% % for i=1:nb_seuil
% % 
% %     figure
% %     scatter(position_ER60(find(seuil_ER60()==seuil(i)),1),position_ER60(find(seuil_ER60()==seuil(i)),2),sqrt(length_ER60(find(seuil_ER60()==seuil(i))).*width_ER60(find(seuil_ER60()==seuil(i)))),height_ER60(find(seuil_ER60()==seuil(i))));
% %     hold on;
% %     %     scatter3(reshape(Latitude(:,indexNav(1:10:end)),1,[]),reshape(Longitude(:,indexNav(1:10:end)),1,[]),-reshape(Depth(:,indexNav(1:10:end)),1,[]),'k');
% %     xlabel('Latitude (�)');
% %     ylabel('Longitude (�)');
% % 
% %     title(['Spatial distribution of schools (vertical projection) at ',num2str(seuil(i)),'dB threshold, size represents length, color represents density'])
% %     colorbar;
% %     axis([min(position_ER60(:,1)) max(position_ER60(:,1)) min(position_ER60(:,2)) max(position_ER60(:,2))]);
% %     caxis( [min(height_ER60(:)) max(height_ER60(:))]);
% %     %     view(-40,20);
% %     saveas(gcf,[path_save,'r�partition spatiale seuil ',num2str(seuil(i)),'dB'],'fig')
% %     saveas(gcf,[path_save,'r�partition spatiale seuil ',num2str(seuil(i)),'dB'],'jpg')
% % 
% % 
% % end
i=1;
filename_ME70=[path_save,'/RUN' str_run(ir,:) '/',num2str(thresholds(:,i)),'/','RUN' str_run(ir,:),'-TH' num2str(thresholds(i)),'-ME70-EIlay.mat'];
  load(filename_ME70);
  
idshoal=1;

for i=1:size(echoes_ME70,2)
    if size(echoes_ME70(i).echoes,2)>1000
        Amp{idshoal}=db2mag([echoes_ME70(i).echoes.m_sv].'); %passage d'intensit� de Sv en dB en amplitude lin�aire
        [parmhat{idshoal}] = wblfit(Amp{idshoal});
        Ypd{idshoal}= wblpdf(Amp{idshoal},parmhat{idshoal}(1),parmhat{idshoal}(2));
        idshoal=idshoal+1;
    end
end


figure;
hold on;
for i=1:idshoal-1
plot(i,parmhat{i}(2),'*')
end

figure;
hold on;
for i=1:idshoal-1
plot(Amp{i},Ypd{i},'*')
end

for i=1:idshoal-1
figure;
hold on;
hist(Amp{i},100)
end