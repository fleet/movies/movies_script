function school_res_bind(fpath,filename_ER60,filename_ME70, threshold)

%% Load EI lay results stored in 'fpath' and bind them into arrays

%init des matrices
time_ER60=[];
length_ER60=[];
width_ER60=[];
height_ER60=[];
volume_ER60=[];
MVBS_ER60=[];
sigma_ag_ER60 = [];
position_ER60 = [];
contours_ER60 = [];
echoes_ER60 = [];
seuil_ER60 = []; 

time_ME70=[];
length_ME70=[];
width_ME70=[];
height_ME70=[];
volume_ME70=[];
MVBS_ME70=[];
sigma_ag_ME70 = [];
position_ME70 = [];
contours_ME70 = [];
echoes_ME70 = [];
seuil_ME70 = []; 

filelist = squeeze(dir(fullfile(fpath,'*.mat')));  % all files
nb_files = size(filelist,1);  % number of files
 
nblME70=0;
nblER60=0;

for numfile = 1:nb_files

    matfilename = filelist(numfile,:).name;
    FileName = [fpath,matfilename];
    disp(FileName);
    load(FileName);

    if size(schools_ER60,1)>0
       for i=1:size(schools_ER60,2) %pour chaque banc
           time_ER60(nblER60+i) = schools_ER60(1,i).time;
           length_ER60(nblER60+i) = schools_ER60(1,i).length;
           width_ER60(nblER60+i) = schools_ER60(1,i).width;
           height_ER60(nblER60+i) = schools_ER60(1,i).height;
           volume_ER60(nblER60+i) = schools_ER60(1,i).volume;
           MVBS_ER60(nblER60+i) = schools_ER60(1,i).MVBS_weighted;
           sigma_ag_ER60(nblER60+i) = schools_ER60(1,i).sigma_ag;
           position_ER60(nblER60+i,:) = schools_ER60(1,i).position;
           for j=1:size(schools_ER60(1,i).contours,2) %pour chaque structure contour
               contours_ER60(nblER60+i).contours(1,j).m_latitude = schools_ER60(1,i).contours(1,j).m_latitude;
               contours_ER60(nblER60+i).contours(1,j).m_longitude = schools_ER60(1,i).contours(1,j).m_longitude;
               contours_ER60(nblER60+i).contours(1,j).m_depth = schools_ER60(1,i).contours(1,j).m_depth;
               contours_ER60(nblER60+i).contours(1,j).m_facet = schools_ER60(1,i).contours(1,j).m_facet;
           end
           for j=1:size(schools_ER60(1,i).echoes,2) %pour chaque structure echos du banc
               echoes_ER60(nblER60+i).echoes(1,j).m_latitude = schools_ER60(1,i).echoes(1,j).m_latitude;
               echoes_ER60(nblER60+i).echoes(1,j).m_longitude = schools_ER60(1,i).echoes(1,j).m_longitude;
               echoes_ER60(nblER60+i).echoes(1,j).m_depth = schools_ER60(1,i).echoes(1,j).m_depth;
                echoes_ER60(nblER60+i).echoes(1,j).m_sv = schools_ER60(1,i).echoes(1,j).m_db;
           end
           seuil_ER60(nblER60+i,:) = threshold;
       end
       nblER60=nblER60+size(schools_ER60,2);
    end

    if size(schools_ME70,1)>0
       for i=1:size(schools_ME70,2)
           time_ME70(nblME70+i) = schools_ME70(1,i).time;
           length_ME70(nblME70+i) = schools_ME70(1,i).length;
           width_ME70(nblME70+i) = schools_ME70(1,i).width;
           height_ME70(nblME70+i) = schools_ME70(1,i).height;
           volume_ME70(nblME70+i) = schools_ME70(1,i).volume;
           MVBS_ME70(nblME70+i) = schools_ME70(1,i).MVBS_weighted;
           sigma_ag_ME70(nblME70+i) = schools_ME70(1,i).sigma_ag;
           position_ME70(nblME70+i,:) = schools_ME70(1,i).position;
           for j=1:size(schools_ME70(1,i).contours,2) %pour chaque structure contour
               contours_ME70(nblME70+i).contours(1,j).m_latitude = schools_ME70(1,i).contours(1,j).m_latitude;
               contours_ME70(nblME70+i).contours(1,j).m_longitude = schools_ME70(1,i).contours(1,j).m_longitude;
               contours_ME70(nblME70+i).contours(1,j).m_depth = schools_ME70(1,i).contours(1,j).m_depth;
               contours_ME70(nblME70+i).contours(1,j).m_facet = schools_ME70(1,i).contours(1,j).m_facet;
           end
           for j=1:size(schools_ME70(1,i).echoes,2) %pour chaque structure echos du banc
               echoes_ME70(nblME70+i).echoes(1,j).m_latitude = schools_ME70(1,i).echoes(1,j).m_latitude;
               echoes_ME70(nblME70+i).echoes(1,j).m_longitude = schools_ME70(1,i).echoes(1,j).m_longitude;
               echoes_ME70(nblME70+i).echoes(1,j).m_depth = schools_ME70(1,i).echoes(1,j).m_depth;
               echoes_ME70(nblME70+i).echoes(1,j).m_sv = schools_ME70(1,i).echoes(1,j).m_db;
           end
           seuil_ME70(nblME70+i,:) = threshold;
       end
    nblME70=nblME70+size(schools_ME70,2);
    end
    clear schools_ER60 schools_ME70;
end
save(filename_ME70,'time_ME70','length_ME70','width_ME70','height_ME70','volume_ME70','MVBS_ME70','sigma_ag_ME70','position_ME70','contours_ME70','echoes_ME70','seuil_ME70');
save(filename_ER60,'time_ER60','length_ER60','width_ER60','height_ER60','volume_ER60','MVBS_ER60','sigma_ag_ER60','position_ER60','contours_ER60','echoes_ER60','seuil_ER60');

end

