%function [detec,lat,long,heure_hac,heure_hac_frac,roll_hac,heave_hac,heave_hac2,at_hac,al_hac,res]=test_MBI2
function [detec,detecsimrad,detecsimraduncomp,posabsTS,anglesimradAthwart,anglesimradAlong,heure_hac,heure_hac_frac,frequency,steering]=ComputeTS(FileName,index_sondeur,threshold,surfaceOffset,bottomOffset)
%detec est un cell array npings x nbeams o� chaque cellule est une matrice
%ndetections x y sv


%implantation MBI directe avec lecture hac et fonctions movies

%nom et chemin fichier
if(0)
chemin_ini='K:\SMFH\EXACHA10\HAC HERMES\EXACHA10\RUN001\';
nom_fic='EXACHA10_001_20100319_171559.hac';
 FileName = [chemin_ini,nom_fic]
end

if (1) %definition config � la main
%noyau lecture
ParameterKernel= moKernelParameter();
ParameterDef=moLoadKernelParameter(ParameterKernel);

ParameterDef.m_bAutoLengthEnable=1;
ParameterDef.m_MaxRange = 30;
ParameterDef.m_bAutoDepthEnable=1;
ParameterDef.m_bIgnorePhaseData=0;

moSaveKernelParameter(ParameterKernel,ParameterDef);
clear ParameterDef;

ParameterR= moReaderParameter();
ParameterDef=moLoadReaderParameter(ParameterR);

ParameterDef.m_chunkNumberOfPingFan=50000;

moSaveReaderParameter(ParameterR,ParameterDef);


end

%lecture hac
%%%%%%%%%%%%%%%
moOpenHac(FileName);

FileStatus= moGetFileStatus;
ilec=1;
while FileStatus.m_StreamClosed < 1
%while FileStatus.m_StreamClosed < 1 & ilec<=3
    moReadChunk();
    FileStatus= moGetFileStatus();
    ilec=ilec+1;
end;
fprintf('End of File');

%presentation des sondeurs
%%%%%%%%%%%%%%%%%%%%%%%%%%
list_sounder=moGetSounderList;
nb_snd=size(list_sounder,2);
disp(' ');
disp(['nb sondeurs = ' num2str(nb_snd)]);
for isdr = 1:nb_snd
    nb_transduc=list_sounder(isdr).m_numberOfTransducer;
    if (list_sounder(isdr).m_SounderId<10)
        ind_list(isdr)=list_sounder(isdr).m_SounderId;
        disp(['sondeur ' num2str(isdr) ':    ' 'index: ' num2str(list_sounder(isdr).m_SounderId) '   nb trans:' num2str(nb_transduc)]);
        for itr=1:nb_transduc
            for ibeam=1:list_sounder(isdr).m_transducer(itr).m_numberOfSoftChannel
                disp(['   trans ' num2str(itr) ':    ' 'nom: ' list_sounder(isdr).m_transducer(itr).m_transName '   freq: ' num2str(list_sounder(isdr).m_transducer(itr).m_SoftChannel(ibeam).m_acousticFrequency/1000) ' kHz']);
            end
        end
    else
        disp('Pb sondeur Id');
    end
end

if nb_snd==1
    index_sondeur=1;
end

detec=cell(moGetNumberOfPingFan(),list_sounder(index_sondeur).m_numberOfTransducer,list_sounder(index_sondeur).m_transducer.m_numberOfSoftChannel);
detecsimrad=cell(moGetNumberOfPingFan(),list_sounder(index_sondeur).m_numberOfTransducer,list_sounder(index_sondeur).m_transducer.m_numberOfSoftChannel);
detecsimraduncomp=cell(moGetNumberOfPingFan(),list_sounder(index_sondeur).m_numberOfTransducer,list_sounder(index_sondeur).m_transducer.m_numberOfSoftChannel);
posabsTS=cell(moGetNumberOfPingFan(),list_sounder(index_sondeur).m_numberOfTransducer,list_sounder(index_sondeur).m_transducer.m_numberOfSoftChannel);
anglesimradAthwart=cell(moGetNumberOfPingFan(),list_sounder(index_sondeur).m_numberOfTransducer,list_sounder(index_sondeur).m_transducer.m_numberOfSoftChannel);
anglesimradAlong=cell(moGetNumberOfPingFan(),list_sounder(index_sondeur).m_numberOfTransducer,list_sounder(index_sondeur).m_transducer.m_numberOfSoftChannel);
steering=zeros(list_sounder(index_sondeur).m_numberOfTransducer,list_sounder(index_sondeur).m_transducer.m_numberOfSoftChannel);
frequency=zeros(list_sounder(index_sondeur).m_numberOfTransducer,list_sounder(index_sondeur).m_transducer.m_numberOfSoftChannel);


        heure_hac=NaN(moGetNumberOfPingFan(),1);
        heure_hac_frac=NaN(moGetNumberOfPingFan(),1);
%         roll_hac=[];
%         heave_hac=[];
%         heave_hac2=[];
%         at_hac=[];
%         al_hac=[];
%         lat=NaN(moGetNumberOfPingFan());
%         long=NaN(moGetNumberOfPingFan());
%         heading=NaN(moGetNumberOfPingFan());
%         vit=[];
        
        ind_ping=0;
        
for index= 0:moGetNumberOfPingFan()-1  %boucle sur les pings
           
    MX= moGetPingFan(index);
    %if (MX.m_pingId >0)
    SounderDesc =  moGetFanSounder(MX.m_pingId);
    if SounderDesc.m_SounderId == list_sounder(index_sondeur).m_SounderId

        ind_ping=ind_ping+1;
         heure_hac(ind_ping)=MX.m_meanTimeCPU;
         heure_hac_frac(ind_ping)=MX.m_meanTimeFraction;
%          roll_hac(ind_ping)=MX.navigationAttitude.rollRad*180/pi;
%          heave_hac(ind_ping)=MX.navigationAttitude.sensorHeaveMeter;
%          heave_hac2(ind_ping)=MX.beam_data(1,1).m_compensateheave;
%         lat(ind_ping)=MX.navigationPosition.m_lattitudeDeg;
%         long(ind_ping)=MX.navigationPosition.m_longitudeDeg;
%         heading(ind_ping)=MX.navigationAttribute.m_headingRad*180/pi;
%         vit(ind_ping)=MX.navigationAttribute.m_speedMeter*3600/1852;
        for transducerIndex=1:SounderDesc.m_numberOfTransducer
        %for transducerIndex=1:1
            datalist = moGetPolarMatrix(MX.m_pingId,transducerIndex-1);
            sv_mat=datalist.m_Amplitude/100;
            Ath_mat=datalist.m_AthwartAngle;
%            at_hac(:,end+1,:)=Ath_mat.';
            Al_mat=datalist.m_AlongAngle;
%            al_hac(:,end+1,:)=Al_mat.';
            
            for ib=1:SounderDesc.m_transducer(1,transducerIndex).m_numberOfSoftChannel  %boucle sur les voies
                %BeamData=MX.beam_data(1,transducerIndex); 
                if ind_ping==1
                    steer_at(ib)=SounderDesc.m_transducer(transducerIndex).m_SoftChannel(1,ib).m_mainBeamAthwartSteeringAngleRad;
                    ouv_at(ib)=SounderDesc.m_transducer(transducerIndex).m_SoftChannel(1,ib).m_beam3dBWidthAthwartRad;
                    ouv_al(ib)=SounderDesc.m_transducer(transducerIndex).m_SoftChannel(1,ib).m_beam3dBWidthAlongRad;
                    sa_cor(ib)=SounderDesc.m_transducer(transducerIndex).m_SoftChannel(ib).m_beamSACorrection;
                    psi(ib)=SounderDesc.m_transducer(transducerIndex).m_SoftChannel(ib).m_beamEquTwoWayAngle;
                    correction_sv(ib)=10*log10(SounderDesc.m_soundVelocity*SounderDesc.m_transducer(transducerIndex).m_pulseDuration/2*1e-6)+psi(ib)+2*sa_cor(ib);
                    
                    steering(transducerIndex,ib)=steer_at(ib);
                    frequency(transducerIndex,ib)=SounderDesc.m_transducer(transducerIndex).m_SoftChannel(1,ib).m_acousticFrequency;
                end
                
                if ~isempty(Ath_mat)
                %correction enregistrement angles switch� dans les hac
                Ath_l_cor=asin(sin(steer_at(ib))+cos(steer_at(ib))/cos(0)*ouv_at(ib)/ouv_al(ib)*(sin(Al_mat(:,ib))-sin(0)));
                Ath_l_cor=Ath_l_cor-steer_at(ib);
                Al_l_cor=asin(sin(0)+cos(0)/cos(steer_at(ib))*ouv_al(ib)/ouv_at(ib)*(sin(Ath_mat(:,ib))-sin(steer_at(ib))));
             
                %detection au-dessus seuil (et pas au max de la quantification de phase)
                range=(1:1:length(sv_mat(:,ib)))*SounderDesc.m_transducer(transducerIndex).m_beamsSamplesSpacing;
                ind_ech=find(range'>surfaceOffset & range'<(MX.beam_data(1,ib).m_bottomRange-bottomOffset) & abs(squeeze(Ath_l_cor))<ouv_at(ib)*3/4*120/128 & abs(squeeze(Al_l_cor))<ouv_al(ib)*3/4*120/128); 
                ndet=size(ind_ech,1);
                if (ndet>0)
                    %figure(10);plot(squeeze(sv_mat(:,ib)),'-*');grid on;hold on;plot(ind_ech,squeeze(sv_mat(ind_ech,ib)),'r*');hold off;figure(20);plot(squeeze(Ath_l_cor)*180/pi,'-*');grid on;hold on;plot(ind_ech,Ath_l_cor(ind_ech)*180/pi,'r*');hold off;figure(200);plot(squeeze(Al_l_cor)*180/pi,'-*');grid on;hold on;plot(ind_ech,Al_l_cor(ind_ech)*180/pi,'r*');hold off
                    %pause;
                    for idet=1:ndet
                        %PosAbs= ComputeWorldPositionME70_2(0,SounderDesc.m_transducer.m_transFaceAlongAngleOffsetRad,SounderDesc.m_transducer.m_transFaceAthwarAngleOffsetRad, MX, SounderDesc.m_transducer, SounderDesc.m_transducer.m_SoftChannel(1,ib) , (ind_ech(idet)-1)*SounderDesc.m_transducer.m_beamsSamplesSpacing, 0, Ath_l_cor(ind_ech(idet)));
                        %                         PosAbs=
                        %                         ComputeWorldPositionME70_2(0,SounderDesc.m_transducer(transducerIndex).m_transFaceAlongAngleOffsetRad,SounderDesc.m_transducer(transducerIndex).m_transFaceAthwarAngleOffsetRad, MX, SounderDesc.m_transducer(transducerIndex), SounderDesc.m_transducer(transducerIndex).m_SoftChannel(1,ib) , (ind_ech(idet)-1)*SounderDesc.m_transducer(transducerIndex).m_beamsSamplesSpacing, Al_l_cor(ind_ech(idet)), Ath_l_cor(ind_ech(idet)));
                        %                             if (range>10 && range<15)
                        x=(2*Al_l_cor(ind_ech(idet))/ouv_al(ib));
                        y=(2*Ath_l_cor(ind_ech(idet))/ouv_at(ib));
                        TS= sv_mat(ind_ech(idet),ib)+ 6.0206*(x^2+y^2-0.18*x^2*y^2)+20*log10(range(ind_ech(idet)))+correction_sv(ib);
                        if TS>threshold
                            detec{ind_ping,transducerIndex,ib}=[detec{ind_ping,transducerIndex,ib};  TS];
                        end
                    end
                end
                end
                
                targets=moGetSingleTargetFan(MX.m_pingId);
                for i=1:size(targets.m_splitBeamData,2)
                    if targets.m_splitBeamData(i).m_parentSTId==SounderDesc.m_transducer(transducerIndex).m_SoftChannel(ib).m_softChannelId
                        nbcibles=size(targets.m_splitBeamData(i).m_targetList,2);
                        for p=1:nbcibles                
                            if (MX.beam_data(1,ib).m_bottomRange~=(2^31-1)/1000 && targets.m_splitBeamData(i).m_targetList(p).m_targetRange<(MX.beam_data(1,ib).m_bottomRange-bottomOffset) && targets.m_splitBeamData(i).m_targetList(p).m_targetRange>surfaceOffset)
                                detecsimrad{ind_ping,transducerIndex,ib}=[detecsimrad{ind_ping,transducerIndex,ib} targets.m_splitBeamData(i).m_targetList(p).m_compensatedTS];
                                detecsimraduncomp{ind_ping,transducerIndex,ib}=[detecsimraduncomp{ind_ping,transducerIndex,ib} targets.m_splitBeamData(i).m_targetList(p).m_unCompensatedTS];
                                PosAbs= ComputeWorldPositionME70_2(0,SounderDesc.m_transducer(transducerIndex).m_transFaceAlongAngleOffsetRad,SounderDesc.m_transducer(transducerIndex).m_transFaceAthwarAngleOffsetRad, MX, SounderDesc.m_transducer(transducerIndex), SounderDesc.m_transducer(transducerIndex).m_SoftChannel(1,ib) , targets.m_splitBeamData(i).m_targetList(p).m_targetRange, targets.m_splitBeamData(i).m_targetList(p).m_AlongShipAngleRad, targets.m_splitBeamData(i).m_targetList(p).m_AthwartShipAngleRad);
                                posabsTS{ind_ping,transducerIndex,ib}=[posabsTS{ind_ping,transducerIndex,ib} PosAbs];
                                anglesimradAthwart{ind_ping,transducerIndex,ib}=[anglesimradAthwart{ind_ping,transducerIndex,ib} SounderDesc.m_transducer(transducerIndex).m_SoftChannel(1,ib).m_mainBeamAthwartSteeringAngleRad+targets.m_splitBeamData(i).m_targetList(p).m_AthwartShipAngleRad];
                                anglesimradAlong{ind_ping,transducerIndex,ib}=[anglesimradAlong{ind_ping,transducerIndex,ib} SounderDesc.m_transducer(transducerIndex).m_SoftChannel(1,ib).m_mainBeamAlongSteeringAngleRad+targets.m_splitBeamData(i).m_targetList(p).m_AlongShipAngleRad];
                            end
                        end
                    end
                end
            end
        end
    end
end

