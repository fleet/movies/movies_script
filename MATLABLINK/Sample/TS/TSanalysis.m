clear all;
% close all;

chemin_ini='C:\data\MAYOBS\SBES_MBES\MAYOBS\RUN035\';

chemin_config_reverse = strrep(chemin_ini, '\', '/');
% moLoadConfig(chemin_config_reverse);

filelist = ls([chemin_ini,'*.hac']);  % ensemble des fichiers
nb_files = size(filelist,1);  % nombre de fichiers

%� renseigner
indexsondeur=1;
nbeams=15;
threshold=-65;
surfaceOffset=4;
bottomOffset=1.7;



TS=cell(nbeams,1);
TSsimrad=cell(nbeams,1);
TSsimradUncomp=cell(nbeams,1);
DepthSimrad=cell(nbeams,1);
% LatSimrad=cell(nbeams,1);
% LongSimrad=cell(nbeams,1);
Date=cell(nbeams,1);
AngleSimradAthwart=cell(nbeams,1);
AngleSimradAlong=cell(nbeams,1);

GainAdjI0=[-0.06  0.03 -0.13 -0.07  0.42  1.07  0.68  0.19 -0.24  0.03 -0.16  0.89  1.44];
SaCorAdjI0=[-0.1 -0.09 -0.08 -0.08 -0.07 -0.08 -0.06 -0.09 -0.06 -0.09 -0.07 -0.07 -0.09];

GainAdjI10=[-0.45 -0.37 -0.41 -0.37 -0.11 -0.03 -0.23  -0.3 -0.43  -0.3 -0.53   0.5   0.6];
SaCorAdjI10=[-0.15 -0.13  -0.1 -0.12  -0.1 -0.14  -0.1 -0.12 -0.11 -0.12 -0.11 -0.11 -0.12];

GainAdjI20=[-0.53 -0.65 -0.48 -0.49 -0.89 -1.44 -1.25 -0.78 -0.46 -0.47 -0.82   0.3  -0.2];
SaCorAdjI20=[-0.1 -0.08 -0.06 -0.07 -0.06 -0.09 -0.05 -0.07 -0.06 -0.07 -0.07 -0.06 -0.08];

GainAdjI30=[-0.92 -1.35 -1.12 -1.46 -2.72 -3.65 -3.71 -1.96 -1.22 -1.23 -1.29 -0.15 -2.05];
SaCorAdjI30=[-0.12 -0.12 -0.07  -0.1 -0.09 -0.13  -0.1 -0.12 -0.08  -0.1 -0.09 -0.09  -0.1];

GainAdjI40=[-1.82 -2.76 -1.82 -2.08 -4.21     0     0 -2.72 -1.72 -2.39 -2.38 -0.85 -4.63];
SaCorAdjI40=[-0.09 -0.08 -0.04 -0.06 -0.07     0     0 -0.07 -0.05 -0.06 -0.06 -0.06  -0.1];


Gain_adjust=GainAdjI0+SaCorAdjI0;


    load('steering_shift.mat');
    dep_trans2(15)=-dep_trans2(1);
    ga2(15)=ga2(1)-0.7;
    
ind_ping=0;
for numfile = 1:nb_files  % boucle sur l'ensemble des fichiers � traiter
    
    hacfilename = filelist(numfile,:);
    FileName = [chemin_ini,hacfilename]
    
%          [detec3,detecsimrad,detecsimraduncomp,posabsTS,anglesimradAthwart,anglesimradAlong,heure_hac,heure_hac_frac,frequency,steering]=ComputeTS(FileName,indexsondeur,threshold,surfaceOffset,bottomOffset);
%         save([chemin_ini,'MBI2_',num2str(indexsondeur),'_',hacfilename(1:end-4)],'detec3','detecsimrad','detecsimraduncomp','posabsTS','anglesimradAthwart','anglesimradAlong','heure_hac','heure_hac_frac','frequency','steering');
%     
    load ([chemin_ini,'MBI2_',num2str(indexsondeur),'_',hacfilename(1:end-4)]);
    
    % mise en liste des d�tections, et positionnement en lat/long
    npings=size(detec3,1);
    
    date = ((heure_hac+heure_hac_frac/10000)/86400 +719529);
%     datestart=datenum(2010,1,18,23,50,0);
%     dateend=datenum(2010,1,19,00,45,00);
    datestart=datenum(2010,1,18,23,49,0);
    dateend=datenum(2010,1,19,01,10,00);
    
    for i=1:npings
        if (date(i)>datestart && date(i)<dateend)
            for ib=1:nbeams
                %             TS{ib}=[TS{ib} ; detec3{i,ib}+2*Gain_adjust(ib)];
                if ~isempty(detecsimrad{i,ib})
                    if (indexsondeur==2 &&nbeams==15)
                        for p=1:length(detecsimrad{i,ib})
                            TSsimrad{ib}=[TSsimrad{ib} ; detecsimrad{i,ib}(p)+delta_TS_steering_shift(ib,anglesimradAthwart{i,ib}(p)*180/pi,anglesimradAlong{i,ib}(p)*180/pi,dep_trans2,dep_long2,freq,a_trans,ga_sim,ga2)];
                            %delta_TS_steering_shift(ib,anglesimradAthwart{i,ib}(p)*180/pi,anglesimradAlong{i,ib}(p)*180/pi,dep_trans2,dep_long2,freq,a_trans,ga_sim,ga2)
                            TSsimradUncomp{ib}=[TSsimradUncomp{ib} ; detecsimraduncomp{i,ib}(p)+delta_TS_steering_shift(ib,anglesimradAthwart{i,ib}(p)*180/pi,anglesimradAlong{i,ib}(p)*180/pi,dep_trans2,dep_long2,freq,a_trans,ga_sim,ga2)];
                        end
                    elseif (indexsondeur==2 &&nbeams==13)
                        TSsimrad{ib}=[TSsimrad{ib} ; detecsimrad{i,ib}'-2*Gain_adjust(ib)];
                        TSsimradUncomp{ib}=[TSsimradUncomp{ib} ; detecsimraduncomp{i,ib}'-2*Gain_adjust(ib)];
                    else
                        TSsimrad{ib}=[TSsimrad{ib} ; detecsimrad{i,ib}'];
                        TSsimradUncomp{ib}=[TSsimradUncomp{ib} ; detecsimraduncomp{i,ib}'];
                    end
                    pos=posabsTS{i,ib};
%                     LatSimrad{ib}=[LatSimrad{ib} ;pos(2,:)'];
%                     LongSimrad{ib}=[LongSimrad{ib} ;pos(1,:)'];
                    DepthSimrad{ib}=[DepthSimrad{ib} ;pos(3,:)'];
                    Date{ib}=[Date{ib} ;repmat(date(i),length(detecsimrad{i,ib}),1)];
                    AngleSimradAthwart{ib}=[AngleSimradAthwart{ib} ;anglesimradAthwart{i,ib}'];
                    AngleSimradAlong{ib}=[AngleSimradAlong{ib} ;anglesimradAlong{i,ib}'];
                end
            end
        end
    end
    
    
end

nbeams=nbeams-2;
startbeam=1;


 steering=(pi/180)*[ -38.6736 -29.9841 -22.5463 -15.9467 -9.9476 -4.3973 0.7788 5.8878 11.3002 17.1513 23.5864 30.8312 39.2774 ];
%steering=(pi/180)*[ 0 0 0 0 0];
% 
 openingAthwart=pi/180*[ 9.51 7.95 6.95 6.26 5.74 5.35 4.99 5.22 5.6 6.1 6.78 7.74 9.23];
 openingAlong=pi/180*[ 7.43 6.89 6.42 6.01 5.66 5.35 4.99 5.19 5.49 5.83 6.21 6.65 7.15 ];
%openingAthwart=pi/180*[ 7 7 7 7 7];
%openingAlong=pi/180*[ 7 7 7 7 7];
  tetaminAth=min(steering-openingAthwart/2);
  tetamaxAth=max(steering+openingAthwart/2);
  tetaminAl=-max(openingAlong/2);
  tetamaxAl=max(openingAlong/2);
  nbanglesAth=60;
  nbanglesAl=5;
            
  angleAthwart=NaN(1,nbanglesAth);
  angleAlong=NaN(1,nbanglesAl);
  
  for i=1:nbanglesAth
      angleat=asin(sin(tetaminAth)+(i)*(sin(tetamaxAth)-sin(tetaminAth))/(nbanglesAth+1));
      angleAthwart(i)=angleat;
  end
  for i=1:nbanglesAl
      angleal=asin(sin(tetaminAl)+(i)*(sin(tetamaxAl)-sin(tetaminAl))/(nbanglesAl+1));
      angleAlong(i)=angleal;
  end
  
  

depth=[10:12:22];
for d=1:length(depth)
    strdepth(d,:)=[num2str(depth(d)) 'm'];
end

for d=1:length(depth)-1
    for i=1:nbeams
        indext=TSsimrad{startbeam+i}<threshold;
        indexg=TSsimrad{startbeam+i}-TSsimradUncomp{startbeam+i}>4;
        indexd=(DepthSimrad{startbeam+i}>depth(d+1) | DepthSimrad{startbeam+i}<depth(d));
        index=indexg|indexd|indext;
        TSsimrad{startbeam+i}(index)=nan;
        AngleSimradAthwart{startbeam+i}(index)=nan;
        AngleSimradAlong{startbeam+i}(index)=nan;
        sum(~index)/sum(~indexd)
        sum(~indexd)*cos(steering(i))/((depth(d+1)-depth(d))*(sin(openingAthwart(i))*sin(openingAlong(i))))
        sum(~index)*cos(steering(i))/((depth(d+1)-depth(d))*(sin(openingAthwart(i))*sin(openingAlong(i))))
        sum(~indexd)
        sum(~index)

    end
end
 
edge=(threshold:2:-30);

% n=NaN(nbeams,length(edge));
% for i=startbeam:nbeams
%     n(i,:)=histc((TS{i}),edge);
%     strlegend{i}=sprintf('%d kHz',(round(frequency(i)/1e3)));
% end
%
% figure
% bar(edge,n');
% palette=jet(nbeams);
% legend(strlegend );
% toto=xlabel('TS from Sv (dB)');
% set(toto,'FontSize',16)


TSmean=nan(nbeams,1);for i=1:nbeams
    indexnan=~isnan(TSsimrad{startbeam+i});
    TSmean(i)=10*log10(mean(10.^(TSsimrad{startbeam+i}(indexnan)/10)));
end
save([chemin_ini,'TSmean_',num2str(indexsondeur)],'TSmean');

TSmeanall=10*log10(mean(10.^(TSmean/10)))
TSstdall=10*log10((mean(10.^(TSmean/10))+std(10.^(TSmean/10)))/mean(10.^(TSmean/10)))

% figure;
% hist(TSall,100);

for d=1:length(depth)-1
    n=NaN(nbeams,length(edge));
    for i=1:nbeams
        indexd=(DepthSimrad{startbeam+i}<=depth(d+1) & DepthSimrad{startbeam+i}>=depth(d));
        indexnan=~isnan(TSsimrad{startbeam+i});
        index=indexnan&indexd;
        if length(TSsimrad{startbeam+i}(index))>0
            n(i,:)=histc((TSsimrad{startbeam+i}(index)),edge);%*cos(steering(i))/((depth(d+1)-depth(d))*(sin(openingAthwart(i))*sin(openingAlong(i))));
        end
        strlegend{i}=sprintf('%d kHz',(round(frequency(startbeam+i)/1e3)));
    end
    
    [~,indexf]=sort(frequency(1+startbeam:nbeams+startbeam));

    
    figure
    bar(edge,n(indexf,:)');
    palette=jet(nbeams);
    legend(   strlegend(indexf) );
    toto=xlabel('TS (dB)');
    set(toto,'FontSize',16)
    toto=ylabel('Count');
    set(toto,'FontSize',16)
%        colormap('HSV');
    xlim([edge(1)-2 edge(end)+2]);
    save([chemin_ini,'TShist_',num2str(indexsondeur)],'n');
    saveas(gcf,[chemin_ini,'TShist_',num2str(indexsondeur),'_',datestr(datestart,'HH-MM'),'_',datestr(dateend,'HH-MM')],'fig')
    saveas(gcf,[chemin_ini,'TShist_',num2str(indexsondeur),'_',datestr(datestart,'HH-MM'),'_',datestr(dateend,'HH-MM')],'jpg')

n=zeros(nbeams,length(depth));
for i=1:nbeams
         indexd=(DepthSimrad{startbeam+i}<=depth(d+1) & DepthSimrad{startbeam+i}>=depth(d));
        indexnan=~isnan(TSsimrad{startbeam+i});
        index=indexnan&indexd;
    if ~isempty(DepthSimrad{startbeam+i}(index))
        n(i,:)=histc((DepthSimrad{startbeam+i}(index)),depth);%*cos(steering(i))/((depth(2)-depth(1))*(sin(openingAthwart(i))*sin(openingAlong(i))));
    end
end

figure
bar(depth,n');
palette=jet(nbeams);
toto=xlabel('Depth (m)');
set(toto,'FontSize',16)

saveas(gcf,[chemin_ini,'NbTargethist_',num2str(indexsondeur),'_',datestr(datestart,'HH-MM'),'_',datestr(dateend,'HH-MM')],'fig')
saveas(gcf,[chemin_ini,'NbTargethist_',num2str(indexsondeur),'_',datestr(datestart,'HH-MM'),'_',datestr(dateend,'HH-MM')],'jpg')

end

for d=1:length(depth)-1
n=zeros(nbeams,12);
for i=1:nbeams
    for j=1:12
        for k=1:length(AngleSimradAthwart{startbeam+i})
            if ( TSsimrad{startbeam+i}(k)-TSsimradUncomp{startbeam+i}(k)>=(j-1) && TSsimrad{startbeam+i}(k)-TSsimradUncomp{startbeam+i}(k)<j)
               n(i,j)=n(i,j)+1; 
            end
        end
    end
    n(i,:)=n(i,:)*cos(steering(i))/((depth(2)-depth(1))*(sin(openingAthwart(i))*sin(openingAlong(i))));
end

figure
bar([1:1:12],n');
palette=jet(nbeams);
toto=xlabel('Attenuation (dB)');
set(toto,'FontSize',16)
saveas(gcf,[chemin_ini,'NbTargethist_',num2str(indexsondeur),'_',datestr(datestart,'HH-MM'),'_',datestr(dateend,'HH-MM')],'fig')
saveas(gcf,[chemin_ini,'NbTargethist_',num2str(indexsondeur),'_',datestr(datestart,'HH-MM'),'_',datestr(dateend,'HH-MM')],'jpg')
end

% nfish=20000;
% 
% for d=1:length(depth)-1
% 
% for i=1:nbeams
%     fsim=nan(1,nfish);
% f=nan(1,20000); 
% f2=nan(1,20000); 
%     indexd=(DepthSimrad{startbeam+i}<=depth(d+1) & DepthSimrad{startbeam+i}>=depth(d));
%     
%     theta_al=AngleSimradAlong{startbeam+i}(indexd)';
%     theta_at=AngleSimradAthwart{startbeam+i}(indexd)';
%     f = -3.0103*((2*(theta_at-steering(i))./openingAthwart(i)).^2+(2*theta_al./openingAlong(i)).^2-0.18*(2*(theta_at-steering(i))./openingAthwart(i)).^2.*(2*theta_al./openingAlong(i)).^2);
%     f2=-(TSsimrad{startbeam+i}(indexd)-TSsimradUncomp{startbeam+i}(indexd))./2;
%     theta_al_sim=(-openingAlong(i)/2+openingAlong(i)*rand(1,nfish))*1.6;
%     theta_at_sim=(-openingAthwart(i)/2+1*openingAthwart(i) *rand(1,nfish))*1.6;
%     fsim= -3.0103*((2*theta_at_sim./openingAthwart(i)).^2+(2*theta_al_sim./openingAlong(i)).^2-0.18*(2*theta_at_sim./openingAthwart(i)).^2.*(2*theta_al_sim./openingAlong(i)).^2);
%     theta_sim=(180/pi)*atan(sqrt(tan(theta_al_sim).^2+tan(theta_at_sim).^2));
% 
% 
% figure
% hist(fsim,[-4:0.5:0]);
% figure
% hist(f,[-4:0.5:0]);
% figure
% hist(f2,[-4:0.5:0]);
% end
% 
% end
% 
% for d=1:length(depth)-1
%     
%     for i=1:nbeams
%         indexd=(DepthSimrad{startbeam+i}<=depth(d+1) & DepthSimrad{startbeam+i}>=depth(d));
%         indexnan=~isnan(TSsimrad{startbeam+i});
%         index=indexnan;
%         theta=(180/pi)*atan(sqrt(tan((AngleSimradAthwart{startbeam+i}(index)-steering(i))).^2+tan(AngleSimradAlong{startbeam+i}(index)).^2));
%         figure
%         hist(theta,20)%,[0:0.5:5]);
%         
%     end
% end


for d=1:length(depth)-1
figure
hold on;
for i=1:nbeams
     indexd=(DepthSimrad{startbeam+i}<=depth(d+1) & DepthSimrad{startbeam+i}>=depth(d));
        indexnan=~isnan(TSsimrad{startbeam+i});
     scatter((180/pi)*AngleSimradAthwart{startbeam+i}(indexd),(180/pi)*(AngleSimradAlong{startbeam+i}(indexd)),1,'b');
%      scatter((DepthSimrad{startbeam+i}(index)./(cos(AngleSimradAthwart{startbeam+i}(index)).*cos(AngleSimradAlong{startbeam+i}(index)))).*tan(AngleSimradAthwart{startbeam+i}(index)),(DepthSimrad{startbeam+i}(index)./(cos(AngleSimradAthwart{startbeam+i}(index)).*cos(AngleSimradAlong{startbeam+i}(index)))).*tan(AngleSimradAlong{startbeam+i}(index)),2,'b');
end


plot((180/pi)*angleAthwart,repmat((180/pi)*angleAlong,length(angleAthwart),1),'r+','MarkerSize',20);

toto=xlabel('Alongship angle (�)');
set(toto,'FontSize',16)
toto=ylabel('Athwartship angle (�)');
set(toto,'FontSize',16)
end

saveas(gcf,[chemin_ini,'TargetPosition_',num2str(indexsondeur),'_',datestr(datestart,'HH-MM'),'_',datestr(dateend,'HH-MM')],'fig')
saveas(gcf,[chemin_ini,'TargetPosition_',num2str(indexsondeur),'_',datestr(datestart,'HH-MM'),'_',datestr(dateend,'HH-MM')],'bmp')

% n=zeros(length(angleAthwart),length(depth)-1);
% for d=1:length(depth)-1
%     for i = 1:nbeams
%         index=find(DepthSimrad{i}<=depth(d+1) & DepthSimrad{i}>=depth(d));
%         if ~isempty(index)
%             n(:,d)=n(:,d)+histc((AngleSimrad{i}(index)*180/pi),angleAthwart,1);
%         end
%     end
% end
% 
% 
% 
% figure
% bar(angleAthwart,n);
% palette=jet(nbeams);
% toto=xlabel('Steering (�)');
% set(toto,'FontSize',16)
% legend(strdepth(1:end-1,:));



% for i=startbeam:nbeams
% figure;
% scatter(DepthSimrad{i},TSsimrad{i});
% end

% Initialize the mean and percentile variables
% sigmavsF = nan(size(TS,1), 1);
% P = nan(size(TSsimrad,1), 2);
%
% for i = 1:size(TS,1)
%   temp = 10.^(TS{i}/10);           % Get sigma for lengths and phi
%   sigmavsF(i) = mean(temp(:));            % Calculate mean
%   P(i,:) = prctile(temp(:), [2.5 97.5]);  % Calculate 95%
% end
%
% TSvsF = 10*log10(sigmavsF);                 % Convert to TS
% L = TSvsF - 10*log10(P(:,1));               % Calculate lower errorbar
% U = 10*log10(P(:,2)) - TSvsF;               % Calculate lower errorbar
%
% % Plot figure with errorbars
% figure
% errorbar( (frequency)./1e3,TSvsF, L, U, 'o','color',[0 0.8 0.8])
% toto=xlabel('Frequency (kHz)');
% set(toto,'FontSize',16)
% toto=ylabel('TS from Sv(dB)');
% set(toto,'FontSize',16)
% toto=ylabel('TS from Sv (dB)');
% set(toto,'FontSize',16)
% set(gca,'FontSize',14)
%
% % Plot figure with errorbars
% figure
% errorbar( (steering)*180/pi,TSvsF, L, U, 'o','color',[0 0.8 0.8])
% toto=xlabel('Steering (�)');
% set(toto,'FontSize',16)
% toto=ylabel('TS from Sv (dB)');
% set(toto,'FontSize',16)
% toto=ylabel('TS from Sv (dB)');
% set(toto,'FontSize',16)
% set(gca,'FontSize',14)
%

% Initialize the mean and percentile variables

TSvsAmean=NaN(length(depth)-1,length(angleAthwart),nbeams,1);
TSvsApct=NaN(length(depth)-1,length(angleAthwart),nbeams,2);
Nb =  NaN(length(depth)-1,length(angleAthwart),nbeams, 1);

for d=1:length(depth)-1
    for a=1:length(angleAthwart)
        for i = 1:nbeams
            indexd=(DepthSimrad{startbeam+i}<=depth(d+1) & DepthSimrad{startbeam+i}>=depth(d));
            if a==1
                dist=(angleAthwart(a)-tetaminAth)/2;
            elseif a==length(angleAthwart)
                dist=(tetamaxAth-angleAthwart(a))/2;
            else
                dist=(angleAthwart(a+1)-angleAthwart(a))/2;
            end          
            indexat=(AngleSimradAthwart{startbeam+i}<=(angleAthwart(a))+dist & AngleSimradAthwart{startbeam+i}>=(angleAthwart(a)-dist));
            indexnan=~isnan(TSsimrad{startbeam+i});
            index=indexd&indexat&indexnan;
            if sum(index)>100
                  temp = 10.^(TSsimrad{startbeam+i}/10);           % Get sigma for lengths and phi
                TSvsAmean(d,a,i) = 10*log10(mean(temp(index)))  ;          % Calculate mean
                TSvsApct(d,a,i,:) = 10*log10(prctile(temp(index), [5 95]));  % Calculate 95%
                Nb(d,a,i)=sum(index);
            end
        end
    end
    
end

col='rbgmck';
mark='o*sdv^';
[value,indexf]=sort(frequency(1+startbeam:nbeams+startbeam));


for f=1:nbeams
    strfrequency(f,:)='000';
    strfrequency(f,end-length(num2str(round(frequency((f+startbeam))'./1e3)))+1:end)=num2str(round(frequency((f+startbeam))'./1e3));
end


for d=1:length(depth)-1
    figure;
    hold on;
    for i = 1:nbeams
        
       plot(angleAthwart((1:end))*180/pi,Nb(d,:,i),[col(1+mod(i,6))]);
        
    end
end
legend(strfrequency(1:nbeams,:));

% set(gca, 'YTickMode', 'auto');
toto=xlabel('Steering (�)');
set(toto,'FontSize',16)
    toto=ylabel('Nb targets');
    set(toto,'FontSize',16)
title(['Mean TS versus angleAthwart for layer ',strdepth(d,:),'m to ',strdepth(d+1,:),'m'])




    
figure;
hold on;
for d=1:length(depth)-1
  for i = 1:nbeams
%     errorbar(angleAthwart(1:end)*180/pi,TSvsAmean(d,:,i),TSvsAmean(d,:,i)-TSvsApct(d,:,i,1),TSvsApct(d,:,i,2)-TSvsAmean(d,:,i),[col(1+mod(i,6)) mark(1+mod(i,6))],'MarkerSize',8);
     errorbar(angleAthwart(1:end)*180/pi,TSvsAmean(d,:,i),TSvsAmean(d,:,i)-TSvsApct(d,:,i,1),TSvsApct(d,:,i,2)-TSvsAmean(d,:,i),'k','MarkerSize',8);

  end
end


for d=1:length(depth)-1
      for i = 1:nbeams
%       plot(angleAthwart(1:end)*180/pi,TSvsAmean(d,:,i),[col(1+mod(i,6))], 'LineWidth',3);
            plot(angleAthwart(1:end)*180/pi,TSvsAmean(d,:,i),'k', 'LineWidth',3);
      end
end

    toto=xlabel('Steering (�)');
    set(toto,'FontSize',16)
    toto=ylabel('TS (dB)');
    set(toto,'FontSize',16)
    
    set(gca,'FontSize',14)
    axis([-50 50 -77 -37]);
%     title(['Mean TS versus steering '])

    grid on;
% legend(strfrequency(:,:));
saveas(gcf,[chemin_ini,'TSversusangleAthwart_',num2str(indexsondeur),'_',datestr(datestart,'HH-MM'),'_',datestr(dateend,'HH-MM')],'fig')
saveas(gcf,[chemin_ini,'TSversusangleAthwart_',num2str(indexsondeur),'_',datestr(datestart,'HH-MM'),'_',datestr(dateend,'HH-MM')],'jpg')



TSvsAmean=NaN(length(depth)-1,length(angleAlong),nbeams,1);
TSvsApct=NaN(length(depth)-1,length(angleAlong),nbeams,2);
Nb =  NaN(length(depth)-1,length(angleAlong),nbeams, 1);
for d=1:length(depth)-1
    for  a=1:length(angleAlong)
         for i = 1:nbeams
             indexd=(DepthSimrad{startbeam+i}<=depth(d+1) & DepthSimrad{startbeam+i}>=depth(d));
             if a==1
                 dist=(angleAlong(a)-tetaminAl)/2;
             elseif a==length(angleAlong)
                 dist=(tetamaxAl-angleAlong(a))/2;
             else
                 dist=(angleAlong(a+1)-angleAlong(a))/2;
             end
             indexal=(AngleSimradAlong{startbeam+i}<=(angleAlong(a)+dist) & AngleSimradAlong{startbeam+i}>=(angleAlong(a)-dist));
             indexnan=~isnan(TSsimrad{startbeam+i});
             index=indexd&indexal&indexnan;
             if sum(index)>100
                 temp = 10.^(TSsimrad{startbeam+i}/10);           % Get sigma for lengths and phi
                 TSvsAmean(d,a,i) = 10*log10(mean(temp(index)))  ;          % Calculate mean
                 TSvsApct(d,a,i,:) = 10*log10(prctile(temp(index), [5 95]));  % Calculate 95%
                 Nb(d,a,i)=sum(index);
             end
         end
    end
end
    
for d=1:length(depth)-1
    figure;
    hold on;
    for i = 1:nbeams
        
       plot(angleAlong((1:end))*180/pi,Nb(d,:,i),[col(1+mod(i,6))]);
        
    end
end
legend(strfrequency(1:nbeams,:));
set(gca, 'YTickMode', 'auto');
toto=xlabel('Steering (�)');
set(toto,'FontSize',16)
title(['Mean TS versus angleAlong for layer ',strdepth(d,:),'m to ',strdepth(d+1,:),'m'])

figure;
hold on;
for d=1:length(depth)-1
    for i=1:nbeams
        errorbar(angleAlong(1:end)*180/pi,TSvsAmean(d,:,i),TSvsAmean(d,:,i)-TSvsApct(d,:,i,1),TSvsApct(d,:,i,2)-TSvsAmean(d,:,i),[col(1+mod(i,6)) mark(1+mod(i,6))],'MarkerSize',8);
    end
end

for d=1:length(depth)-1
    for i=1:nbeams
        plot(angleAlong(1:end)*180/pi,TSvsAmean(d,:,i),[col(1+mod(i,6))], 'LineWidth',3);
    end
end
toto=xlabel('Steering (�)');
set(toto,'FontSize',16)
toto=ylabel('TS (dB)');
set(toto,'FontSize',16)

set(gca,'FontSize',14)
title(['Mean TS versus steering '])
hold on;
grid on;

legend(strfrequency(1:nbeams,:));
saveas(gcf,[chemin_ini,'TSversusangleAlong_',num2str(indexsondeur),'_',datestr(datestart,'HH-MM'),'_',datestr(dateend,'HH-MM')],'fig')
saveas(gcf,[chemin_ini,'TSversusangleAlong_',num2str(indexsondeur),'_',datestr(datestart,'HH-MM'),'_',datestr(dateend,'HH-MM')],'jpg')


TSvsFmean=NaN(length(depth)-1,nbeams,1);
TSvsFpct=NaN(length(depth)-1,nbeams,2);
for d=1:length(depth)-1
        for i = 1:nbeams
            indexd=(DepthSimrad{startbeam+i}<=depth(d+1) & DepthSimrad{startbeam+i}>=depth(d));
            indexnan=~isnan(TSsimrad{startbeam+i});
            index=indexd&indexnan;
            if sum(index)>100
                temp = 10.^(TSsimrad{startbeam+i}/10);           % Get sigma for lengths and phi
                TSvsFmean(d,i) = 10*log10(mean(temp(index)))  ;          % Calculate mean
                TSvsFpct(d,i,:) = 10*log10(prctile(temp(index), [5 95]));  % Calculate 95%
            end
    end
    
end


figure;
for d=1:length(depth)-1
 
    errorbar(steering*180/pi,TSvsFmean(d,:),TSvsFmean(d,:)-TSvsFpct(d,:,1),TSvsFpct(d,:,2)-TSvsFmean(d,:),[col(1+mod(d,6)) mark(1+mod(d,6))],'MarkerSize',8);
    toto=xlabel('Steering (�)');
    set(toto,'FontSize',16)
    toto=ylabel('TS (dB)');
    set(toto,'FontSize',16)
    
    set(gca,'FontSize',14)
    title(['Mean TS versus beam axis steering '])
    hold on;
    grid on;
    
end
for d=1:length(depth)-1
      plot(steering*180/pi,TSvsFmean(d,:),[col(1+mod(d,6))], 'LineWidth',3);
end
legend(strdepth(1:end-1,:));
saveas(gcf,[chemin_ini,'TSversusbeamangle_',num2str(indexsondeur),'_',datestr(datestart,'HH-MM'),'_',datestr(dateend,'HH-MM')],'fig')
saveas(gcf,[chemin_ini,'TSversusbeamangle_',num2str(indexsondeur),'_',datestr(datestart,'HH-MM'),'_',datestr(dateend,'HH-MM')],'jpg')

figure;
for d=1:length(depth)-1
    errorbar((frequency(indexf+startbeam))./1e3,TSvsFmean(d,indexf),TSvsFmean(d,indexf)-TSvsFpct(d,indexf,1),TSvsFpct(d,indexf,2)-TSvsFmean(d,indexf),[col(1+mod(d,6)) mark(1+mod(d,6))],'MarkerSize',8);
    toto=xlabel('Frequency (kHz)');
    set(toto,'FontSize',16)
    toto=ylabel('TS (dB)');
    set(toto,'FontSize',16)
    
    
%     title(['Mean TS versus frequency '])
    hold on;
    grid on;   
end
for d=1:length(depth)-1 
    plot((frequency(indexf+startbeam))./1e3,TSvsFmean(d,indexf),[col(1+mod(d,8))], 'LineWidth',3);
end
    set(gca,'FontSize',14)
    axis([20 220 -77 -37]);
% legend(strdepth(1:end-1,:));
saveas(gcf,[chemin_ini,'TSversusbeamfrequency_',num2str(indexsondeur),'_',datestr(datestart,'HH-MM'),'_',datestr(dateend,'HH-MM')],'fig')
saveas(gcf,[chemin_ini,'TSversusbeamfrequency_',num2str(indexsondeur),'_',datestr(datestart,'HH-MM'),'_',datestr(dateend,'HH-MM')],'jpg')

% for d=1:length(depth)-1
% % Plot figure with errorbars
%     figure
%     errorbar( angle(1:end-1),TSvsF(d,:), L(d,:), U(d,:), 'o')
%     toto=xlabel('Steering (�)');
%     set(toto,'FontSize',16)
%     toto=ylabel('TS simrad (dB)');
%     set(toto,'FontSize',16)
%     toto=ylabel('TS simrad (dB)');
%     set(toto,'FontSize',16)
%     set(gca,'FontSize',14)
% end