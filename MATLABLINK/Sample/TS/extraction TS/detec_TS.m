function [targets_hac,ts_mat,delta]=detec_TS(list_sounder,sv_mat,al_mat,ath_mat,ir,index_sondeur,index_trans,index_beam,TSrange,TSech,TScomp,TSucomp,TSalong,TSathwart,ismultibeam)

affich=0;

%param�tres de d�tection
seuilTS=-60;
maxGainComp=8;
PhaseDev=5;
minEch=0.5;
maxEch=1.8;
MinEchospace=1;

dec_tir=8; %nombre �chantillons du tir
if ismultibeam
    ind_range0=(list_sounder(index_sondeur).m_transducer(index_trans).m_pulseDuration/list_sounder(index_sondeur).m_transducer(index_trans).m_timeSampleInterval)/2; %index de l'�chantillon de range 0
else
    ind_range0=3; %index de l'�chantillon de range 0
end

npings=size(sv_mat,1);

%matrices TS et TS Uncomp
delta=list_sounder(index_sondeur).m_transducer(index_trans).m_timeSampleInterval*list_sounder(index_sondeur).m_soundVelocity/2/10^6;
psi=list_sounder(index_sondeur).m_transducer(index_trans).m_SoftChannel(index_beam).m_beamEquTwoWayAngle;
sa_cor=list_sounder(index_sondeur).m_transducer(index_trans).m_SoftChannel(index_beam).m_beamSACorrection;
sv2ts=10*log10(list_sounder(index_sondeur).m_soundVelocity*list_sounder(index_sondeur).m_transducer(index_trans).m_pulseDuration/2*1e-6)+psi+2*sa_cor;
ouv_at=list_sounder(index_sondeur).m_transducer(index_trans).m_SoftChannel(index_beam).m_beam3dBWidthAthwartRad;
ouv_al=list_sounder(index_sondeur).m_transducer(index_trans).m_SoftChannel(index_beam).m_beam3dBWidthAlongRad;
dec_al=list_sounder(index_sondeur).m_transducer(index_trans).m_SoftChannel(index_beam).m_mainBeamAlongSteeringAngleRad;
dec_at=list_sounder(index_sondeur).m_transducer(index_trans).m_SoftChannel(index_beam).m_mainBeamAthwartSteeringAngleRad;

ts_range=max(delta,((1:size(sv_mat,2))-ind_range0)*delta);
alpha=list_sounder(index_sondeur).m_transducer(index_trans).m_SoftChannel(index_beam).m_absorptionCoef/10000/1000;
tsu_mat= sv_mat+ repmat(20*log10(ts_range),npings,1)+sv2ts;

if ismultibeam
    x=(2*(al_mat).'/ouv_al);
    y=(2*(ath_mat).'/ouv_at);
else
    x=(2*(al_mat-dec_al).'/ouv_al);
    y=(2*(ath_mat-dec_at).'/ouv_at);
end
ts_mat=tsu_mat+ 6.0206*(x.^2+y.^2-0.18*x.^2.*y.^2).';
Plike_mat=tsu_mat-repmat(40*log10(ts_range),npings,1)-2*alpha*repmat(ts_range,npings,1);% niveau d�compens� de la TVG

%index du fond 
indir=find(isnan(ir));
if ~isempty(indir) ir(indir)=size(ts_mat,2)-5;end
ir(find(ir>size(ts_mat,2)))=size(ts_mat,2)-5;



for ip=1:npings
% for ip=34:34
    ip
    targets_hac{ip}.targetRange=[];
    targets_hac{ip}.compensatedTS=[];
    targets_hac{ip}.unCompensatedTS=[];
    targets_hac{ip}.AlongShipAngleRad=[];
    targets_hac{ip}.AthwartShipAngleRad=[];
    targets_hac{ip}.L6dB=[];
    ind=[];
    ind2=[];
    ind3=[];
    ind4=[];
    ind5=[];
    %condition sur niveau, max local, et range (pas dans le tir et avant le fond)
    ind=1+find(ts_mat(ip,2:end-1)>seuilTS & Plike_mat(ip,2:end-1)>=max(Plike_mat(ip,1:end-2),Plike_mat(ip,3:end)) & (2:size(tsu_mat,2)-1)<ir(ip) & (2:size(tsu_mat,2)-1)>dec_tir);
    if ~isempty(ind) 
        %longueur � -6dB
        necho=length(ind);
        clear isup iinf
        for i=1:necho
            u=find(Plike_mat(ip,ind(i):floor(ir(ip)))<Plike_mat(ip,ind(i))-6);
            if ~isempty(u)
                isup(i)=ind(i)-2+min(u);
            else
                isup(i)=ind(i);
            end
            u=find(Plike_mat(ip,1:ind(i))<Plike_mat(ip,ind(i))-6);
            if ~isempty(u)
                iinf(i)=1+max(u);
            else
                iinf(i)=ind(i);
            end
        end
        L6dB=isup-iinf+1;

        %condition longueur
        NechP=list_sounder(index_sondeur).m_transducer(index_trans).m_pulseDuration/list_sounder(index_sondeur).m_transducer(index_trans).m_timeSampleInterval; %nb ech dans pulse
        ind2=find(L6dB>=round(NechP*minEch) & L6dB<=round(NechP*maxEch));
        
        if ~isempty(ind2) 

            %condition compensation
            ind3=find(ts_mat(ip,ind(ind2))-tsu_mat(ip,ind(ind2))<=maxGainComp*2);
            if ~isempty(ind3) 

                %condition phase 
                al_sens=list_sounder(index_sondeur).m_transducer(index_trans).m_SoftChannel(index_beam).m_beamAlongAngleSensitivity;
                ath_sens=list_sounder(index_sondeur).m_transducer(index_trans).m_SoftChannel(index_beam).m_beamAthwartAngleSensitivity;
                clear std_ph_al std_ph_ath
                for i=1:length(ind3)
                    %std_ph_al(i)=std(al_mat(ip,iinf(ind2(ind3(i))):isup(ind2(ind3(i))))*128/pi*al_sens);
                    %std_ph_ath(i)=std(ath_mat(ip,iinf(ind2(ind3(i))):isup(ind2(ind3(i))))*128/pi*ath_sens);
                    std_ph_al(i)=std(al_mat(ip,ind(ind2(ind3(i)))-2:ind(ind2(ind3(i)))+2)*128/pi*al_sens);
                    std_ph_ath(i)=std(ath_mat(ip,ind(ind2(ind3(i)))-2:ind(ind2(ind3(i)))+2)*128/pi*ath_sens);
                end
                ind4=find(std_ph_al<=PhaseDev & std_ph_ath<=PhaseDev);
                
                if ~isempty(ind4) 
                    
                    %calcul range fin
                    ir_fin=[];
                    for i=1:length(ind4)
                        il6dB=(iinf(ind2(ind3(ind4(i)))):isup((ind2(ind3(ind4(i))))));
                        alpha=list_sounder(index_sondeur).m_transducer(index_trans).m_SoftChannel(index_beam).m_absorptionCoef/10000/1000;
                        ir_fin(i)=sum(il6dB.*10.^(Plike_mat(ip,il6dB)/10))/sum(10.^(Plike_mat(ip,il6dB)/10));
                    end
                    %calcul niveau fin
                    i_ini=ind(ind2(ind3(ind4)));
                    ts_fin=ts_mat(ip,i_ini)+40*log10((ir_fin-ind_range0)./(i_ini-ind_range0))+2*alpha*delta*(ir_fin-i_ini);
                    tsu_fin=tsu_mat(ip,i_ini)+40*log10((ir_fin-ind_range0)./(i_ini-ind_range0))+2*alpha*delta*(ir_fin-i_ini);                    
                    
                    %condition proximit�
                    [uu,inds]=sort(ts_fin,'descend'); % on les trie par niveau d�croissant
                    ind5=inds(1);
                    for i=2:length(inds)
                        if min(abs(ir_fin(inds(i))-ir_fin(ind5)))>=MinEchospace*NechP %on ne garde l'�cho que s'il est suffisamment distant des plus forts
                            ind5=[ind5 inds(i)];
                        end
                    end
                    
                    %d�tections finales
                    targets_hac{ip}.targetRange=(ir_fin(ind5)-ind_range0)*delta;
                    targets_hac{ip}.compensatedTS=ts_fin(ind5);
                    targets_hac{ip}.unCompensatedTS=tsu_fin(ind5);
                    targets_hac{ip}.AlongShipAngleRad=al_mat(ip,ind(ind2(ind3(ind4(ind5)))));
                    targets_hac{ip}.AthwartShipAngleRad=ath_mat(ip,ind(ind2(ind3(ind4(ind5)))));
                    targets_hac{ip}.L6dB=L6dB(ind2(ind3(ind4(ind5))));
                end
            end
        end
    end
% end
end


%% affichage d�taill�
if (affich)
    
    
    np=length(TSrange);
    i_det_raw=TSrange{ip}/list_sounder(index_sondeur).m_transducer(index_trans).m_beamsSamplesSpacing+3;
    tsu_raw=TSucomp{ip};
    ts_raw=TScomp{ip};
    al_raw=TSalong{ip};
    at_raw=TSathwart{ip};
    
    str_lgd=[{'niveau'};{'longueur'};{'compensation'};{'phase'};{'prox'}];
    index_fin=targets_hac{ip}.targetRange/delta+ind_range0;
    
    % TS Uncomp
    var=tsu_mat;
    var_raw=tsu_raw;
    stade=0;
    figure(40);hold off;plot(var(ip,1:floor(ir(ip))+5),'-*');grid on;hold on;title(num2str(ip));
    if ~isempty(ind) 
        plot(ind,var(ip,ind),'y*');stade=1;
        if ~isempty(ind2) 
            plot(ind(ind2),var(ip,ind(ind2)),'g*');stade=2;
            if ~isempty(ind3) 
                plot(ind(ind2(ind3)),var(ip,ind(ind2(ind3))),'k*');stade=3;
                if ~isempty(ind4) 
                    plot(ind(ind2(ind3(ind4))),var(ip,ind(ind2(ind3(ind4)))),'m*','LineW',2);stade=4;
                    plot(ind(ind2(ind3(ind4(ind5)))),var(ip,ind(ind2(ind3(ind4(ind5))))),'c*','LineW',4);stade=5;
                end
            end
        end
    end
    
    if np>0 plot(i_det_raw,var_raw,'r^','LineW',2); end
    if stade>=1 legend('tsu',str_lgd{(1:stade)'},'raw'); else legend('tsu','raw'); end
    
    % TS comp
    var=ts_mat;
    var_raw=ts_raw;
    figure(41);hold off;plot(var(ip,1:floor(ir(ip))+5),'-*');grid on;hold on;title(num2str(ip));
    if ~isempty(ind) 
        plot(ind,var(ip,ind),'y*');stade=1;
        if ~isempty(ind2) 
            plot(ind(ind2),var(ip,ind(ind2)),'g*');stade=2;
            if ~isempty(ind3) 
                plot(ind(ind2(ind3)),var(ip,ind(ind2(ind3))),'k*');stade=3;
                if ~isempty(ind4) 
                    plot(ind(ind2(ind3(ind4))),var(ip,ind(ind2(ind3(ind4)))),'m*','LineW',2);stade=4;
                    plot(index_fin,targets_hac{ip}.compensatedTS,'c*','LineW',4);stade=5;
                end
            end
        end
    end
    if np>0 plot(i_det_raw,var_raw,'r^','LineW',2); end
    if stade>=1 legend('ts',str_lgd{(1:stade)'},'raw'); else legend('ts','raw'); end
    if ~isempty(ind4) plot(index_fin,var(ip,ind(ind2(ind3(ind4(ind5))))),'c*','LineW',2);stade=5;end
    
    % Along
    var=al_mat;
    var_raw=al_raw;
    figure(42);hold off;plot(var(ip,1:floor(ir(ip))+5),'-*');grid on;hold on;title(num2str(ip));
    if ~isempty(ind) 
        plot(ind,var(ip,ind),'y*');stade=1;
        if ~isempty(ind2) 
            plot(ind(ind2),var(ip,ind(ind2)),'g*');stade=2;
            if ~isempty(ind3) 
                plot(ind(ind2(ind3)),var(ip,ind(ind2(ind3))),'k*');stade=3;
                if ~isempty(ind4) 
                    plot(ind(ind2(ind3(ind4))),var(ip,ind(ind2(ind3(ind4)))),'m*','LineW',2);stade=4;
                    plot(ind(ind2(ind3(ind4(ind5)))),var(ip,ind(ind2(ind3(ind4(ind5))))),'c*','LineW',4);stade=5;
                end
            end
        end
    end
    if np>0 plot(i_det_raw,var_raw,'r^','LineW',2); end
    if stade>=1 legend('along',str_lgd{(1:stade)'},'raw'); else legend('along','raw'); end
    
    % Athwart
    var=ath_mat;
    var_raw=at_raw;
    figure(43);hold off;plot(var(ip,1:floor(ir(ip))+5),'-*');grid on;hold on;title(num2str(ip));
    if ~isempty(ind) 
        plot(ind,var(ip,ind),'y*');stade=1;
        if ~isempty(ind2) 
            plot(ind(ind2),var(ip,ind(ind2)),'g*');stade=2;
            if ~isempty(ind3) 
                plot(ind(ind2(ind3)),var(ip,ind(ind2(ind3))),'k*');stade=3;
                if ~isempty(ind4) 
                    plot(ind(ind2(ind3(ind4))),var(ip,ind(ind2(ind3(ind4)))),'m*','LineW',2);stade=4;
                    plot(ind(ind2(ind3(ind4(ind5)))),var(ip,ind(ind2(ind3(ind4(ind5))))),'c*','LineW',4);stade=5;
                end
            end
        end
    end
    if np>0 plot(i_det_raw,var_raw,'r^','LineW',2); end
    if stade>=1 legend('athwart',str_lgd{(1:stade)'},'raw'); else legend('athwart','raw'); end
end
