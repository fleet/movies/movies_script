clear all
close all


mydir  = pwd;
idcs   = strfind(mydir,'\')
pathUseful = [mydir(1:idcs(end-1)-1) '\UsefulFunctions'];

addpath(genpath(pathUseful)) 

index_sondeur=1;
index_trans=1;
index_beam=1;

chemin_ini='C:\Users\lberger\Desktop\GUERLEDAN2019\HAC\CW\Etalonnage\';
% chemin_ini='C:\data\ESTECH17\SBES_MBES\ESTECH17\RUN817\';
chemin_cfg='C:\Users\lberger\Desktop\GUERLEDAN2019\HAC\FM\Config_M3D\Calibration\';
% chemin_cfg='C:\data\ESTECH17\SBES_MBES\ESTECH17\RUN817\';

%  chemin_ini='C:\Users\lberger\Desktop\PANDORA\hacLB\0609\';
%  chemin_cfg='C:\Users\lberger\Desktop\PANDORA\Config\';
%  
% datestart =14*3600+30*60+00+(datenum('22-Sept-2017')-datenum('01-Jan-1970'))*24*3600; %debut raw
% dateend =14*3600+50*60+00+(datenum('22-Sept-2017')-datenum('01-Jan-1970'))*24*3600;

datestart =10*3600+10*60+10+(datenum('10-Oct-2019')-datenum('01-Jan-1970'))*24*3600; %debut raw
dateend =14*3600+53*60+00+(datenum('10-Oct-2019')-datenum('01-Jan-1970'))*24*3600;

% datestart =07*3600+34*60+30+(datenum('06-Sept-2019')-datenum('01-Jan-1970'))*24*3600; %debut raw
% dateend =07*3600+34*60+47+(datenum('06-Sept-2019')-datenum('01-Jan-1970'))*24*3600;

[heure_hac_tot,list_sounder,sv_mat,al_mat,ath_mat,ir,TS,TSrange,TSech,TScomp,TSucomp,TSalong,TSathwart,TSposition,TSpositionGPS,TSlabel,TSfreq,heading,vit_knt,pitchRad,rollRad,heave,lat,long]=read_TS_hac(chemin_ini,'',chemin_cfg,datestart,dateend,index_sondeur,index_trans,index_beam,0);



chemin_results=[chemin_ini '\Results_M3D'];

%positionin
Zmax=0;
i=1;
for ip=1:length(TS)
    np=length(TSlabel{ip});
    if np>0
        %calcul des x y z des d�tections courantes
        for idet=1:np
            if (TSlabel{ip}(idet)>0)
                Xarray(i)=TSposition{ip}(1,idet);
                Yarray(i)=TSposition{ip}(2,idet);
                Zarray(i)=TSposition{ip}(3,idet);
                if (Zarray(i)>Zmax)
                    Zmax=Zarray(i);
                end
                TSarray(i)=median(TScomp{ip}(:,idet));
                TSarrayMFR(i,:)=TScomp{ip}(:,idet);
                LatitudeArray(i)=TSpositionGPS{ip}(2,idet);
                LongitudeArray(i)=TSpositionGPS{ip}(1,idet);
                DepthArray(i)=TSpositionGPS{ip}(3,idet);
                TimeArray(i)=(heure_hac_tot(ip)/(24*3600)+datenum('01-Jan-1970'));
                TSlabelArray(i)=TSlabel{ip}(idet);
                TSfreqArray(i,:)=TSfreq{ip}(:,idet);
                TSalongArray(i)=TSalong{ip}(idet);
                  TSathwartArray(i)=TSathwart{ip}(idet);
                i=i+1;
            end
        end
    end
end



%export vers GLOBE;
fileNameprefix=[chemin_ini 'TSpanache_' datestr(datestart/(24*3600)+datenum('01-Jan-1970'),'dd-mmm-yyyy_HH_MM') '_' datestr(dateend/(24*3600)+datenum('01-Jan-1970'),'dd-mmm-yyyy_HH_MM') '_' list_sounder.m_transducer(index_trans).m_transName];
fileNameTS = [fileNameprefix '.csv'];
A=[TimeArray' LatitudeArray' LongitudeArray' DepthArray' TSarray'];
dlmwrite(fileNameTS,A,'delimiter', ',', 'precision', 16);

%affichage
figure;imagesc([1:size(sv_mat,1)],list_sounder.m_transducer(index_trans).m_timeSampleInterval*1e-6*list_sounder.m_soundVelocity/2*[1:size(sv_mat,2)],sv_mat',[-90 -35]) %echogramme Sv
set(gca,'YDir','reverse');
colormap jet
caxis([-90 -35]);
title('Calibrated volume Target Strength');
colorbar;
axis([1 size(sv_mat,1) 0 Zmax])
saveas(gcf,[fileNameprefix '_SvEchogram'],'fig')
saveas(gcf,[fileNameprefix '_SvEchogram'],'png')

figure;imagesc([1:size(sv_mat,1)],list_sounder.m_transducer(index_trans).m_timeSampleInterval*1e-6*list_sounder.m_soundVelocity/2*[1:size(sv_mat,2)],sv_mat',[-90 -35]) %echogramme Sv
colormap gray;
freezeColors;
hold on;
TSlabelunique=[];
for ip=length(TS):-1:1
    for j=1:length(TSlabel{ip})
        %d�tection Simrad track�es
        if (TSlabel{ip}(j)>0)
             scatter(ip,TSrange{ip}(j),10,median(TScomp{ip}(:,j))+calibration_offset);
             %affichage des labels
             ind=find(TSlabelunique==TSlabel{ip}(j));
             if (length(ind)==0)
                 text(ip,TSrange{ip}(j),num2str(TSlabel{ip}(j)),'Color','w');
                 TSlabelunique=[TSlabelunique TSlabel{ip}(j)];
             end
                 
        end
    end
end
% end

set(gca,'YDir','reverse');
colormap jet
caxis([-55 -20]);
title('Beam compensated Target Strength');
colorbar;
axis([1 size(sv_mat,1) 0 Zmax])
saveas(gcf,[fileNameprefix '_TSEchogram'],'fig')
saveas(gcf,[fileNameprefix '_TSEchogram'],'png')

  
figure;
hist(TSarray,50);
meanTS = 10*log10(mean(10.^(TSarray/10)));
legendTS = sprintf('mean TS: %3.1f ', meanTS);
legend(legendTS);
title('Beam compensated Target Strength');
xlabel('TS (dB)');
saveas(gcf,[fileNameprefix '_TSHistogram'],'fig')
saveas(gcf,[fileNameprefix '_TSHistogram'],'png')

step=length(TSfreqArray(1,:))/10;
for f=1:length(TSfreqArray(1,:))
    strfrequency(f,:)='000 kHz';
    strfrequency(f,end-length(num2str(round(TSfreqArray(1,f)./1e3)))-3:end-4)=num2str(round(TSfreqArray(1,f)'./1e3));
end
figure;
hist(TSarrayMFR(:,1:round(step):end),50);
legend(strfrequency(1:round(step):end,:))
xlim([-65 -30])
title('Beam compensated Target Strength');
xlabel('TS (dB)');
saveas(gcf,[fileNameprefix '_TSHistogramMFR'],'fig')
saveas(gcf,[fileNameprefix '_TSHistogramMFR'],'png')

TSdepthnat=[];
DEPTH=[5:1:15];
for i=1:length(DEPTH)-1
    ind=find(Zarray>=DEPTH(i) & Zarray<DEPTH(i+1));
    TSdepthnat(i)=10*log10(mean(10.^(TSarray(ind)/10)));
end

figure;
plot(TSarray,-Zarray,'*')
hold on
plot(TSdepthnat,-(DEPTH(1:end-1)+DEPTH(2:end))/2,'linewidth',5)
grid on
title('Beam compensated Target Strength');
xlabel('TS (dB)');
ylabel('Depth (m)');
saveas(gcf,[fileNameprefix 'meanTS'],'fig')
saveas(gcf,[fileNameprefix 'meanTS'],'png')

% figure;
% plot(TSfreqArray/1000,TSarrayMFR);
% xlabel('Frequency (kHz)');
% ylabel('TS (dB)');
% grid on
% title('Beam compensated Target Strength');

   figure;
      hold on;
       strlegend=[];
       position=[];
       risingspeed=[];
       speed_knt=[];
%        vect=[];
       meanTS=[];
       stdTS=[];
for il=1:length(TSlabelunique)
    ind=find(TSlabelArray==TSlabelunique(il));
%         ind=find(TSlabelArray~=0);
    meanTS(il,:)=10*log10(mean(10.^(TSarrayMFR(ind,:)/10),1));
    for f=1:size(TSarrayMFR,2)
        stdTS(il,f)=calc_std_ts(TSarrayMFR(ind,f));
    end
    
    %    plot(TSfreqArray(ind,:)/1000,TSarrayMFR(:,ind));
    errorbar(TSfreqArray(1,:)/1000,meanTS(il,:),stdTS(il,:),'linewidth',3);
    %     plot(TSfreqArray(ind,:)/1000,10*log10(mean(10.^(TSarrayMFR(:,ind)/10),1)),'linewidth',5);
    str_track(il,:)='0000';
    str_track(il,end-length(num2str(TSlabelunique(il)))+1:end)=num2str(TSlabelunique(il));
    

    %vitesse des tracks
    risingspeed(il)=-(Zarray(ind(1:end-1))-Zarray(ind(2:end)))/(86400*(TimeArray(ind(1:end-1))-TimeArray(ind(2:end))));
    dd=sqrt((Xarray(ind(1:end-1))-Xarray(ind(2:end))).^2+(Yarray(ind(1:end-1))-Yarray(ind(2:end))).^2+(Zarray(ind(1:end-1))-Zarray(ind(2:end))).^2);
    speed_knt(il)= -dd/(86400*(TimeArray(ind(1:end-1))-TimeArray(ind(2:end))))*3600/1852;

    %orientation des tracks
    vect=[Xarray(ind(1:end-1))-Xarray(ind(2:end));Yarray(ind(1:end-1))-Yarray(ind(2:end));Zarray(ind(1:end-1))-Zarray(ind(2:end))];
    vectproj=[Xarray(ind(1:end-1))-Xarray(ind(2:end));Yarray(ind(1:end-1))-Yarray(ind(2:end));zeros(1,length(Zarray(ind))-1)];
    zvect=repmat([0;0;1;],1,size(vect,2));
    xvect=repmat([1;0;0;],1,size(vect,2));
    
    %         tracks{it}.pitch_track= 90-acosd(dot(vect,zvect)/norm(vect));
    pitch(il)=mean(atan2d(vect(3,:),norm(vectproj)));
    %        tracks{it}.yaw_track= acosd(dot(vectproj,xvect)/norm(vectproj));
    %         tracks{it}.yaw_track= acosd(dot(vectproj,xvect)/norm(vectproj))*sign(vectproj(2,:));
    yaw(il)= mean(atan2d(vectproj(2,:),vectproj(1,:)));
    
    posx(il)=mean(Xarray(ind));
    posy(il)=mean(Yarray(ind));
    posz(il)=mean(Zarray(ind));
    vectmean=mean(vect,2);
    vectx(il)=vectmean(1);
    vecty(il)=vectmean(2);
    vectz(il)=vectmean(3);
      
    
end
 legend([str_track]);
   grid on;

xlabel('Freq (kHz)');
ylabel('TS (dB)');
saveas(gcf,[fileNameprefix 'TSTracks'],'fig')
saveas(gcf,[fileNameprefix 'TSTracks'],'png')


for i=1:size(meanTS,2)
    prctileTS(i,:)=prctile(meanTS(:,i),[5 50 95]);
    moyenneTS(i)=10*log10(mean(10.^(meanTS(:,i)/10),1));
end

figure;
plot(TSfreqArray(1,:)/1000,squeeze(prctileTS(:,2)),'*-r')
hold on;
plot(TSfreqArray(1,:)/1000,moyenneTS,'*-k')

plot(TSfreqArray(1,:)/1000,prctileTS(:,1),'*--b')
plot(TSfreqArray(1,:)/1000,prctileTS(:,3),'*--b')
   grid on;
   xlabel('Freq (kHz)');
ylabel(' Mean TS (dB)');
text(speed_knt,mean(meanTS,2),num2str(TSlabelunique'))
saveas(gcf,[fileNameprefix 'TSTracksMean'],'fig')
saveas(gcf,[fileNameprefix 'TSTracksMean'],'png')


figure;
plot(speed_knt,mean(meanTS,2),'*');
   grid on;
   xlabel('Speed (knots)');
ylabel(' Mean TS (dB)');
text(speed_knt,mean(meanTS,2),num2str(TSlabelunique'))
saveas(gcf,[fileNameprefix 'TSTrackSpeed'],'fig')
saveas(gcf,[fileNameprefix 'TSTrackSpeed'],'png')

figure;
plot(pitch,mean(meanTS,2),'*');
meanpitch = mean(pitch);
stdpitch = std(pitch);
legendpitch = sprintf('mean pitch: %3.1f std pitch: %3.1f', meanpitch,stdpitch);
legend(legendpitch);
   grid on;
   xlabel('Pitch (�)');
ylabel(' Mean TS (dB)');
text(pitch,mean(meanTS,2),num2str(TSlabelunique'))
saveas(gcf,[fileNameprefix 'TSTrackPitch'],'fig')
saveas(gcf,[fileNameprefix 'TSTrackPitch'],'png')

% ind=find(TSlabelArray==120);
% writerObj = VideoWriter('bulle.avi');
% writerObj.FrameRate = 1;
% open(writerObj);
% figure;
% grid on;
% hold on;
% % axis tight�
% set(gca,'nextplot','replacechildren');
% set(gcf,'Renderer','zbuffer');
% axis([min(TSfreqArray(1,:)/1000) max(TSfreqArray(1,:)/1000) -70 -40]);
% for i=1:length(ind)
%     plot(TSfreqArray(1,:)/1000,TSarrayMFR(i,:));
%     legend(['Depth: ' num2str(DepthArray(ind(i)))]);
%   frame = getframe;
%    writeVideo(writerObj,frame);
% end
% 
% close(writerObj);

TSdepthnat=[];
TSdepthnatMFR=[];
stdTSdepthnatMFR=[];
DEPTH=[15:1.5:50];
idepth=1;
clear strDepth;
for i=1:length(DEPTH)-1
    ind=find(Zarray>=DEPTH(i) & Zarray<DEPTH(i+1) );
    if (length(ind)>0)
        TSdepthnat(idepth)=10*log10(mean(10.^(TSarray(ind)/10)));
        TSdepthnatMFR(idepth,:)=10*log10(mean(10.^(TSarrayMFR(ind,:)/10),1));
        for f=1:size(TSarrayMFR,2)
            stdTSdepthnatMFR(idepth,f)=calc_std_ts(TSarrayMFR(ind,f));
        end
        strDepth(idepth,:)='000';
        strDepth(idepth,end-length(num2str(round((DEPTH(i)+DEPTH(i+1))/2)))+1:end)=num2str(round((DEPTH(i)+DEPTH(i+1))/2));
        idepth=idepth+1;
    end
end
% 
% figure;
% % plot(TSarray,-Zarray)
% hold on
% % plot(TSfreqArray(1,:)/1000,TSdepthnatMFR,'linewidth',5)
% for idepth=1:size(TSdepthnatMFR,1)
%  errorbar(TSfreqArray(1,:)/1000,TSdepthnatMFR(idepth,:),stdTSdepthnatMFR(idepth,:),'linewidth',3);
% end
% grid on
% title('Beam compensated Target Strength');
% xlabel('Freq (kHz)');
% ylabel('TS (dB)');
% legend(strDepth);
% saveas(gcf,[fileNameprefix 'meanTSMFR'],'fig')
% saveas(gcf,[fileNameprefix 'meanTSMFR'],'png')

r = -linspace(0,Zmax) ;
th = linspace(0,2*pi) ;
[R,T] = meshgrid(r,th) ;
Xbeam = mean(Xarray)+R.*cos(T)*tand(10) ;
Ybeam = mean(Yarray)+R.*sin(T)*tand(10) ;
Zbeam = R ;

figure;
surf(Xbeam,Ybeam,Zbeam,'EdgeColor','none')
alpha 0.2
colormap gray;
freezeColors;
hold on;
scatter3(Xarray,Yarray,-Zarray,10,TSarray)
colormap jet
caxis([-65 -40])
grid on
axis equal
colorbar;
title('Beam compensated Target Strength');
saveas(gcf,[fileNameprefix 'TS3D'],'fig')
saveas(gcf,[fileNameprefix 'TS3D'],'png')

% figure;
% scatter3(LatitudeArray,LongitudeArray,-DepthArray,10,TSarray)
% colormap jet
% caxis([-65 -40])
% grid on
% colorbar;
% title('Beam compensated Target Strength');
% saveas(gcf,[fileNameprefix 'TS3D_GPS'],'fig')
% saveas(gcf,[fileNameprefix 'TS3D_GPS'],'png')

figure;
quiver3(posx,posy,-posz,vectx,vecty,vectz)
colormap jet
grid on
axis equal
title('Direction');
saveas(gcf,[fileNameprefix 'Direction'],'fig')
saveas(gcf,[fileNameprefix 'Direction'],'png')


