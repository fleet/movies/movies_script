clear all
close all
index_sondeur=1;
index_trans=1;
index_beam=1;

% chemin_ini='C:\Temp\HAC\er60_MBES\EROC\RUN196\';
%chemin_ini='C:\Temp\HAC\er60_MBES\EROC\RUN549\';
chemin_ini='C:\Temp\HAC\er60_MBES\EROC\RUN549\';
% chemin_ini='C:\Temp\HAC\er60_MBES\EROC\RUN550\';
%goto =10*3600+56*60+15+(datenum('01-Jun-2014')-datenum('01-Jan-1970'))*24*3600;% detections interessantes
%[heure_hac_tot,list_sounder,sv_mat,al_mat,ath_mat,ir,TS,TSrange,TSech,TScomp,TSucomp,TSalong,TSathwart,heading,vit_knt]=read_TS_hac('L:\PELGAS2014\EROC\HAC_HERMES\er60_MBES\EROC\RUN528\EROC_528_20151110_150710.hac',goto,index_sondeur,index_trans,0);

% goto =11*3600+05*60+35+(datenum('01-Jun-2014')-datenum('01-Jan-1970'))*24*3600;
%  goto =11*3600+08*60+05+(datenum('01-Jun-2014')-datenum('01-Jan-1970'))*24*3600;
% goto =11*3600+10*60+58+(datenum('01-Jun-2014')-datenum('01-Jan-1970'))*24*3600;
% goto =11*3600+31*60+00+(datenum('01-Jun-2014')-datenum('01-Jan-1970'))*24*3600; %debut raw
% goto =11*3600+30*60+14+(datenum('01-Jun-2014')-datenum('01-Jan-1970'))*24*3600; %debut raw
datestart =11*3600+28*60+42+(datenum('01-Jun-2014')-datenum('01-Jan-1970'))*24*3600; %debut raw
dateend =11*3600+29*60+42+(datenum('01-Jun-2014')-datenum('01-Jan-1970'))*24*3600;
% goto =10*3600+56*60+15+(datenum('01-Jun-2014')-datenum('01-Jan-1970'))*24*3600; %debut raw
% goto =17*3600+15*60+44+(datenum('31-May-2014')-datenum('01-Jan-1970'))*24*3600; %debut raw
% goto =17*3600+31*60+17+(datenum('31-May-2014')-datenum('01-Jan-1970'))*24*3600; %debut raw
% goto =13*3600+19*60+48+(datenum('29-May-2014')-datenum('01-Jan-1970'))*24*3600; %debut raw
% goto =14*3600+23*60+40+(datenum('31-May-2015')-datenum('01-Jan-1970'))*24*3600; %debut raw
% goto =08*3600+55*60+19+(datenum('01-Jun-2015')-datenum('01-Jan-1970'))*24*3600; %debut raw
%  [heure_hac_tot,list_sounder,sv_mat,al_mat,ath_mat,ir,TS,TSrange,TSech,TScomp,TSucomp,TSalong,TSathwart,heading,vit_knt,pitchRad,rollRad,heave,lat,long]=read_TS_hac(chemin_ini,[chemin_ini,'EROC_549_20151201_134551.hac'],goto,index_sondeur,index_trans,0);
[heure_hac_tot,list_sounder,sv_mat,al_mat,ath_mat,ir,TS,TSrange,TSech,TScomp,TSucomp,TSalong,TSathwart,TSlabel,heading,vit_knt,pitchRad,rollRad,heave,lat,long]=read_TS_hac(chemin_ini,[chemin_ini,'EROC_549_20151201_135630.hac'],datestart,dateend,index_sondeur,index_trans,index_beam,0);
% [heure_hac_tot,list_sounder,sv_mat,al_mat,ath_mat,ir,TS,TSrange,TSech,TScomp,TSucomp,TSalong,TSathwart,heading,vit_knt]=read_TS_hac(chemin_ini,'EROC_550_20151202_172102.hac',goto,index_sondeur,index_trans,0);
% [heure_hac_tot,list_sounder,sv_mat,al_mat,ath_mat,ir,TS,TSrange,TSech,TScomp,TSucomp,TSalong,TSathwart,heading,vit_knt]=read_TS_hac(chemin_ini,'EROC_196_20151211_160417.hac',goto,index_sondeur,index_trans,0);
%  [heure_hac_tot,list_sounder,sv_mat,al_mat,ath_mat,ir,TS,TSrange,TSech,TScomp,TSucomp,TSalong,TSathwart,heading,vit_knt]=read_TS_hac(chemin_ini,'EROC_196_20151211_161934.hac',goto,index_sondeur,index_trans,0);
%d�tection Ifremer
[targets_hac,ts_mat,delta]=detec_TS(list_sounder,sv_mat,al_mat,ath_mat,ir,index_sondeur,index_trans,index_beam,TSrange,TSech,TScomp,TSucomp,TSalong,TSathwart);

%affichage
% figure(10);hold off;imagesc(ts_mat',[-70 -30]);%echogramme TS
%figure(2);imagesc(al_mat(:,1:600)') %echogramme along angle
%figure(3);imagesc(ath_mat(:,1:600)'); %echogramme athwart angle

%position porteur
npings=length(TS);
if (0)
    load Carto_0_6E;[x_ship y_ship]=latlon2xy(Carto, lat, long);
else %position par vitesse et cap
    clear x_ship y_ship
    x_ship(1)=0;y_ship(1)=0;
    for ip=2:npings
        x_ship(ip)=x_ship(ip-1)+vit_knt(ip)*1852/3600*(heure_hac_tot(ip)-heure_hac_tot(ip-1))*cosd(heading(ip));
        y_ship(ip)=y_ship(ip-1)+vit_knt(ip)*1852/3600*(heure_hac_tot(ip)-heure_hac_tot(ip-1))*sind(heading(ip));
    end
end

%attitude EROC
% load('C:\Temp\HAC\attitudeEROC2014\save_2sec\Immersion_Anchois_chalut31.mat')
% load('C:\Temp\HAC\attitudeEROC2014\save_2sec\Attitude_Anchois_chalut31.mat')
load('C:\Temp\HAC\attitudeEROC2014\save_2sec\Immersion_Anchois_chalut.mat')
load('C:\Temp\HAC\attitudeEROC2014\save_2sec\Attitude_Anchois_chalut.mat')
% load('L:\PELGAS2014\EROC\attitudeEROC2014\save_2sec\Immersion_Anchois_chalut31.mat')
% load('L:\PELGAS2014\EROC\attitudeEROC2014\save_2sec\Attitude_Anchois_chalut31.mat')
% load('L:\PELGAS2014\EROC\attitudeEROC2014\save_2sec\Immersion_Anchois_chalut.mat')
% load('L:\PELGAS2014\EROC\attitudeEROC2014\save_2sec\Attitude_Anchois_chalut.mat')
dateheure=heure_hac_tot/86400 +719529;
immersion=zeros(1,length(heure_hac_tot));
pitch=zeros(1,length(heure_hac_tot));
roll=zeros(1,length(heure_hac_tot));
for i=1:length(heure_hac_tot)
    ind=find(abs(Time-dateheure(i))<=2/86400);
    immersion(i)=Prof(ind(1));
    pitch(i)=IncY(ind(1));
    roll(i)=IncX(ind(1));
end

% 
%  immersion=smooth(immersion,51)';
%   pitch=smooth(pitch,51)';
%    roll=smooth(roll,51)';

SeuilSpeed_knt=5;
ntrous=1;
%[tracks,targets_hac,x_eroc]=track_eroc(targets_hac,vit_knt,heure_hac_tot,SeuilSpeed_knt,ntrous);
% [tracks,targets_hac]=track_det(targets_hac,list_sounder,index_sondeur,index_trans,heure_hac_tot,heading,pitch*pi/180,roll*pi/180,x_ship,y_ship,heave,SeuilSpeed_knt,ntrous);
[tracks,targets_hac]=track_det(targets_hac,list_sounder,index_sondeur,index_trans,heure_hac_tot,heading,zeros(1,length(heading))*pi/180,zeros(1,length(heading))*pi/180,x_ship,y_ship,heave,SeuilSpeed_knt,ntrous);

for ip=1:length(TS)
    targets_sim{ip}.targetRange=TSrange{ip};
    targets_sim{ip}.compensatedTS=TScomp{ip};
    targets_sim{ip}.unCompensatedTS=TSucomp{ip};
    targets_sim{ip}.AlongShipAngleRad=TSalong{ip};
    targets_sim{ip}.AthwartShipAngleRad=TSathwart{ip};
end
%[tracks_sim,targets_sim,x_eroc_sim]=track_eroc(targets_sim,vit_knt,heure_hac_tot,SeuilSpeed_knt,ntrous);
% [tracks_sim,targets_sim]=track_det(targets_sim,list_sounder,index_sondeur,index_trans,heure_hac_tot,heading,pitch*pi/180,roll*pi/180,x_ship,y_ship,heave,SeuilSpeed_knt,ntrous);
[tracks_sim,targets_sim]=track_det(targets_sim,list_sounder,index_sondeur,index_trans,heure_hac_tot,heading,zeros(1,length(heading))*pi/180,zeros(1,length(heading))*pi/180,x_ship,y_ship,heave,SeuilSpeed_knt,ntrous);


%filtrage des tracks
for it=1:length(tracks)
    length_track(it)=length(tracks{it}.ping);
    if (length_track(it))>=2      
        dd=sqrt((tracks{it}.x(2:end)-tracks{it}.x(1:end-1)).^2+(tracks{it}.y(2:end)-tracks{it}.y(1:end-1)).^2+(tracks{it}.z(2:end)-tracks{it}.z(1:end-1)).^2);
        tracks{it}.speed_knt= dd./(heure_hac_tot(tracks{it}.ping(2:end))-heure_hac_tot(tracks{it}.ping(1:end-1)))*3600/1852;
        max_speed_knt(it)=max(tracks{it}.speed_knt);
        median_ts(it)=median(tracks{it}.ts);
        vect=[tracks{it}.x(2:end)-tracks{it}.x(1:end-1);tracks{it}.y(2:end)-tracks{it}.y(1:end-1);tracks{it}.z(2:end)-tracks{it}.z(1:end-1)];
        vectproj=[tracks{it}.x(2:end)-tracks{it}.x(1:end-1);tracks{it}.y(2:end)-tracks{it}.y(1:end-1);zeros(1,length_track(it)-1)];
           zvect=repmat([0;0;1;],1,size(vect,2));
           xvect=repmat([1;0;0;],1,size(vect,2));
           
%         tracks{it}.pitch_track= 90-acosd(dot(vect,zvect)/norm(vect));
        tracks{it}.pitch_track=atan2d(vect(3,:),norm(vectproj));
%        tracks{it}.yaw_track= acosd(dot(vectproj,xvect)/norm(vectproj));
%         tracks{it}.yaw_track= acosd(dot(vectproj,xvect)/norm(vectproj))*sign(vectproj(2,:));
        tracks{it}.yaw_track= atan2d(vectproj(2,:),vectproj(1,:));
        ind=tracks{it}.yaw_track<0;
        tracks{it}.yaw_track(ind)=360+tracks{it}.yaw_track(ind);

            
    end
end
ind_t=find(length_track>=10);


%filtrage des tracks simrad
for it=1:length(tracks_sim)
     length_track_sim(it)=length(tracks_sim{it}.ping);
    if length_track_sim(it)>=2    
        dd=sqrt((tracks_sim{it}.x(2:end)-tracks_sim{it}.x(1:end-1)).^2+(tracks_sim{it}.y(2:end)-tracks_sim{it}.y(1:end-1)).^2+(tracks_sim{it}.z(2:end)-tracks_sim{it}.z(1:end-1)).^2);
        tracks_sim{it}.speed_knt= dd./(heure_hac_tot(tracks_sim{it}.ping(2:end))-heure_hac_tot(tracks_sim{it}.ping(1:end-1)))*3600/1852;
        max_speed_knt(it)=max(tracks_sim{it}.speed_knt);
        median_ts(it)=median(tracks_sim{it}.ts);
        vect=[tracks_sim{it}.x(2:end)-tracks_sim{it}.x(1:end-1);tracks_sim{it}.y(2:end)-tracks_sim{it}.y(1:end-1);tracks_sim{it}.z(2:end)-tracks_sim{it}.z(1:end-1)];
        vectproj=[tracks_sim{it}.x(2:end)-tracks_sim{it}.x(1:end-1);tracks_sim{it}.y(2:end)-tracks_sim{it}.y(1:end-1);zeros(1,length_track_sim(it)-1)];
           zvect=repmat([0;0;1;],1,size(vect,2));
           xvect=repmat([1;0;0;],1,size(vect,2));
%         tracks_sim{it}.pitch_track=  90-acosd(dot(vect,zvect)/norm(vect));
         tracks_sim{it}.pitch_track=atan2d(vect(3,:),norm(vectproj));
%         tracks_sim{it}.yaw_track= acosd(dot(vectproj,xvect)/norm(vectproj));
        tracks_sim{it}.yaw_track= atan2d(vectproj(2,:),vectproj(1,:));
         ind=tracks_sim{it}.yaw_track<0;
        tracks_sim{it}.yaw_track(ind)=360+tracks_sim{it}.yaw_track(ind);
            
    end
end
ind_t_sim=find(length_track_sim>=10);



% affichage tracks filtr�es
col='mrgbykcw';
figure(100);
imagesc(ts_mat',[-70 -30]);%echogramme TS
hold on;
for it=1:length(ind_t)
plot(tracks{ind_t(it)}.ping,tracks{ind_t(it)}.range/delta+3,[col(1+mod(it,length(col))) '*'],'lINeW',4);
text(tracks{ind_t(it)}.ping(1),tracks{ind_t(it)}.range(1)/delta+3,num2str(ind_t(it)),'FontWeight','bold');
end
hold off;

% affichage tracks filtr�es simra
col='mrgbykcw';
figure(110);
imagesc(ts_mat',[-70 -30]);%echogramme TS
hold on;
for it=1:length(ind_t_sim)
plot(tracks_sim{ind_t_sim(it)}.ping,tracks_sim{ind_t_sim(it)}.range/delta+3,[col(1+mod(it,length(col))) '*'],'lINeW',4);
text(tracks_sim{ind_t_sim(it)}.ping(1),tracks_sim{ind_t_sim(it)}.range(1)/delta+3,num2str(ind_t_sim(it)),'FontWeight','bold');
end
hold off;

% %affichage d'une track
if(0)
it=442; %index de la track
it_sim=237;
figure;plot(tracks{it}.x,tracks{it}.y,'-*');xlabel('along m');ylabel('athwart m');grid on; title(['parcours ref longit. eroc track ' num2str(it)]);
hold on;plot(tracks_sim{it_sim}.x,tracks_sim{it_sim}.y,'*');xlabel('along m');ylabel('athwart m');grid on; title(['parcours ref longit. eroc track ' num2str(it_sim)]);
% figure;plot(tracks{it}.x-x_eroc(tracks{it}.ping),tracks{it}.y,'-*');xlabel('along m');ylabel('athwart m');grid on; title(['parcours ref transducteur track ' num2str(it)]);
% hold on;plot(tracks_sim{it_sim}.x-x_eroc_sim(tracks_sim{it_sim}.ping),tracks_sim{it_sim}.y,'*');xlabel('along m');ylabel('athwart m');grid on; title(['parcours ref transducteur track ' num2str(it_sim)]);
figure;plot(tracks{it}.ping,-tracks{it}.z,'-*');xlabel('ping');ylabel('-z m');grid on;title(['-z(m) ref transducteur track ' num2str(it) ])
hold on;plot(tracks_sim{it_sim}.ping,-tracks_sim{it_sim}.z,'*');xlabel('ping');ylabel('-z m');grid on;title(['-z(m) ref transducteur track ' num2str(it_sim) ])
figure;plot(tracks{it}.ping,tracks{it}.ts,'-*');xlabel('ping');ylabel('TS dB');grid on;title(['TS track ' num2str(it) ])
hold on;plot(tracks_sim{it_sim}.ping,tracks_sim{it_sim}.ts,'*');xlabel('ping');ylabel('TS dB');grid on;title(['TS track ' num2str(it_sim) ])
figure;plot(tracks{it}.ping,tracks{it}.speed_knt,'-*');xlabel('ping');ylabel('knt');grid on;title(['speed(knt) track ' num2str(it) ])
hold on;plot(tracks_sim{it_sim}.ping,tracks_sim{it_sim}.speed_knt,'*');xlabel('ping');ylabel('knt');grid on;title(['speed(knt) track ' num2str(it_sim) ])
end

   
datestart1=dateheure(1);
dateend2=dateheure(end);


max_ech=max(round(immersion/list_sounder.m_transducer(index_trans).m_beamsSamplesSpacing));
min_ech=min(round(immersion/list_sounder.m_transducer(index_trans).m_beamsSamplesSpacing));


%r�cup�rations d�tections 

%check TS simrad
ts_sim=[];
tsu_sim=[];
tsu_sim_hac=[];
range_sim=[];
immersion_sim=[];
al_sim=[];
at_sim=[];
al_sim_raw=[];
at_sim_raw=[];
ip_sim=[];
la=[];
for ip=1:length(TS)
np=length(TSech{ip});
if np>0
    ts_sim=[ts_sim TScomp{ip}];
    tsu_sim=[tsu_sim TSucomp{ip}];
    range_sim=[range_sim TSrange{ip}];
     immersion_sim=[immersion_sim TSrange{ip}*0+immersion(ip)];
    al_sim=[al_sim TSalong{ip}];
    at_sim=[at_sim TSathwart{ip}];
    ip_sim=[ip_sim TScomp{ip}*0+ip];
    %tsu_sim_hac=[tsu_sim_hac tsu_mat(ip,ceil(TSrange{ip}/delta)+3)-40*log10(ceil(TSrange{ip}/delta)*delta)+40*log10(TSrange{ip})];
    %al_sim_raw=[al_sim_raw  squeeze(anglesAl_raw(index_trans,ip+1,ceil(TSrange{ip}/delta)+3))'];
    %at_sim_raw=[at_sim_raw  squeeze(anglesAt_raw(index_trans,ip+1,ceil(TSrange{ip}/delta)+3))'];
end
end

%check TS hac
ts_hac=[];
ts_hac_ini=[];
tsu_hac=[];
range_hac=[];
immersion_hac=[];
pitch_hac=[];
roll_hac=[];
al_hac=[];
at_hac=[];
ip_hac=[];
for ip=1:length(targets_hac)
np=length(targets_hac{ip}.targetRange);
if np>0
    ts_hac=[ts_hac targets_hac{ip}.compensatedTS];
    range_hac=[range_hac targets_hac{ip}.targetRange];
    immersion_hac=[immersion_hac targets_hac{ip}.targetRange*0+immersion(ip)];
    pitch_hac=[pitch_hac targets_hac{ip}.targetRange*0+pitch(ip)];
     roll_hac=[roll_hac targets_hac{ip}.targetRange*0+roll(ip)];
    %range_hac_ini=[range_hac_ini TShac_index{ip}];
    ip_hac=[ip_hac targets_hac{ip}.targetRange*0+ip];
    %ts_hac_ini=[ts_hac_ini ts_mat(ip,TShac_index{ip})];
    tsu_hac=[tsu_hac targets_hac{ip}.unCompensatedTS];
    al_hac=[al_hac targets_hac{ip}.AlongShipAngleRad];
    at_hac=[at_hac targets_hac{ip}.AthwartShipAngleRad];
    

end
end

%check TS track
ts_track=[];
tsu_track=[];
range_track=[];
immersion_eroc=[];
immersion_eroc_track_deltamean=[];
immersion_eroc_track_deltastd=[];
pitch_eroc=[];
pitch_eroc_track_deltamean=[];
pitch_eroc_track_deltastd=[];
roll_eroc=[];
roll_eroc_track_deltamean=[];
roll_eroc_track_deltastd=[];
ip_track=[];
al_track=[];
at_track=[];
x_track=[];
y_track=[];
z_track=[];
id_track=[];
speed_track=[];
tilt_track_mean=[];
tilt_track_std=[];
yaw_track_mean=[];
TS_track_mean=[];
TS_track_std=[];
nbtrous_track=[];
for it=1:length(ind_t)
np=length(tracks{ind_t(it)}.ping);
if np>1
    ts_track=[ts_track tracks{ind_t(it)}.ts];
    range_track=[range_track tracks{ind_t(it)}.range];
    immersion_eroc=[immersion_eroc immersion(tracks{ind_t(it)}.ping)];
    immersion_eroc_track_deltamean=[immersion_eroc_track_deltamean immersion(tracks{ind_t(it)}.ping)*0+mean(abs(immersion(tracks{ind_t(it)}.ping)-mean(immersion(tracks{ind_t(it)}.ping))))];
    immersion_eroc_track_deltastd=[immersion_eroc_track_deltastd immersion(tracks{ind_t(it)}.ping)*0+std(abs(immersion(tracks{ind_t(it)}.ping)-mean(immersion(tracks{ind_t(it)}.ping))))];
    pitch_eroc=[pitch_eroc pitch(tracks{ind_t(it)}.ping)];
     pitch_eroc_track_deltamean=[pitch_eroc_track_deltamean pitch(tracks{ind_t(it)}.ping)*0+mean(abs(pitch(tracks{ind_t(it)}.ping)-mean(pitch(tracks{ind_t(it)}.ping))))];
     pitch_eroc_track_deltastd=[pitch_eroc_track_deltastd pitch(tracks{ind_t(it)}.ping)*0+std(abs(pitch(tracks{ind_t(it)}.ping)-mean(pitch(tracks{ind_t(it)}.ping))))];
      pitch_eroc_track_deltamean=[pitch_eroc_track_deltamean pitch(tracks{ind_t(it)}.ping)*0+mean((pitch(tracks{ind_t(it)}.ping)))];
     pitch_eroc_track_deltastd=[pitch_eroc_track_deltastd pitch(tracks{ind_t(it)}.ping)*0+std((pitch(tracks{ind_t(it)}.ping)))];
    roll_eroc=[roll_eroc roll(tracks{ind_t(it)}.ping)];
    roll_eroc_track_deltamean=[roll_eroc_track_deltamean roll(tracks{ind_t(it)}.ping)*0+mean(abs(roll(tracks{ind_t(it)}.ping)-mean(roll(tracks{ind_t(it)}.ping))))];
    roll_eroc_track_deltastd=[roll_eroc_track_deltastd roll(tracks{ind_t(it)}.ping)*0+std(abs(roll(tracks{ind_t(it)}.ping)-mean(roll(tracks{ind_t(it)}.ping))))];
    roll_eroc_track_deltamean=[roll_eroc_track_deltamean roll(tracks{ind_t(it)}.ping)*0+mean((roll(tracks{ind_t(it)}.ping)))];
    roll_eroc_track_deltastd=[roll_eroc_track_deltastd roll(tracks{ind_t(it)}.ping)*0+std((roll(tracks{ind_t(it)}.ping)))];
    ip_track=[ip_track tracks{ind_t(it)}.ping];
    tsu_track=[tsu_track tracks{ind_t(it)}.tsu];
    al_track=[al_track tracks{ind_t(it)}.alongRad];
    at_track=[at_track tracks{ind_t(it)}.athwartRad];
%     x_track=[x_track (tracks{ind_t(it)}.x-x_ship(tracks{ind_t(it)}.ping))];
%     y_track=[y_track (tracks{ind_t(it)}.y-y_ship(tracks{ind_t(it)}.ping))];
        x_track=[x_track (tracks{ind_t(it)}.x)];
    y_track=[y_track (tracks{ind_t(it)}.y)];
    z_track=[z_track (tracks{ind_t(it)}.z-heave(tracks{ind_t(it)}.ping))];
    id_track=[id_track tracks{ind_t(it)}.range*0+ind_t(it)];
    speed_track=[speed_track tracks{ind_t(it)}.range*0+mean(tracks{ind_t(it)}.speed_knt)];
    yaw_track_mean=[yaw_track_mean tracks{ind_t(it)}.range*0+mean(tracks{ind_t(it)}.yaw_track)];
    tilt_track_mean=[tilt_track_mean tracks{ind_t(it)}.range*0+mean(tracks{ind_t(it)}.pitch_track)];
    tilt_track_std=[tilt_track_std tracks{ind_t(it)}.range*0+std(tracks{ind_t(it)}.pitch_track)];
      TS_track_mean=[TS_track_mean tracks{ind_t(it)}.range*0+10*log10(mean(10.^(tracks{ind_t(it)}.ts/10)))];
      TS_track_std=[TS_track_std tracks{ind_t(it)}.range*0+std(tracks{ind_t(it)}.ts)];
      nbtrous_track=[nbtrous_track tracks{ind_t(it)}.range*0+sum(tracks{ind_t(it)}.ping(2:end)-tracks{ind_t(it)}.ping(1:end-1)-1)/length(tracks{ind_t(it)}.ping)];
end
end



%check TS track SImrad
ts_track_sim=[];
tsu_track_sim=[];
range_track_sim=[];
immersion_eroc_sim=[];
pitch_eroc_sim=[];
roll_eroc_sim=[];
ip_track_sim=[];
al_track_sim=[];
at_track_sim=[];
x_track_sim=[];
y_track_sim=[];
z_track_sim=[];
id_track_sim=[];
speed_track_sim=[];
tilt_track_mean_sim=[];
tilt_track_std_sim=[];
yaw_track_mean_sim=[];
TS_track_mean_sim=[];
TS_track_std_sim=[];
nbtrous_track_sim=[];
for it=1:length(ind_t_sim)
np=length(tracks_sim{ind_t_sim(it)}.ping);
if np>1
    ts_track_sim=[ts_track_sim tracks_sim{ind_t_sim(it)}.ts];
    range_track_sim=[range_track_sim tracks_sim{ind_t_sim(it)}.range];
    immersion_eroc_sim=[immersion_eroc_sim tracks_sim{ind_t_sim(it)}.range*0+immersion(tracks_sim{ind_t_sim(it)}.ping)];
    pitch_eroc_sim=[pitch_eroc_sim tracks_sim{ind_t_sim(it)}.range*0+pitch(tracks_sim{ind_t_sim(it)}.ping)];
    roll_eroc_sim=[roll_eroc_sim tracks_sim{ind_t_sim(it)}.range*0+roll(tracks_sim{ind_t_sim(it)}.ping)];
     ip_track_sim=[ip_track_sim tracks_sim{ind_t_sim(it)}.range*0+tracks_sim{ind_t_sim(it)}.ping];
    tsu_track_sim=[tsu_track_sim tracks_sim{ind_t_sim(it)}.tsu];
    al_track_sim=[al_track_sim tracks_sim{ind_t_sim(it)}.alongRad];
    at_track_sim=[at_track_sim tracks_sim{ind_t_sim(it)}.athwartRad];
    x_track_sim=[x_track_sim (tracks_sim{ind_t_sim(it)}.x-x_ship(tracks_sim{ind_t_sim(it)}.ping))];
    y_track_sim=[y_track_sim (tracks_sim{ind_t_sim(it)}.y-y_ship(tracks_sim{ind_t_sim(it)}.ping))];
    z_track_sim=[z_track_sim (tracks_sim{ind_t_sim(it)}.z-heave(tracks_sim{ind_t_sim(it)}.ping))];
    id_track_sim=[id_track_sim tracks_sim{ind_t_sim(it)}.range*0+ind_t_sim(it)];
    speed_track_sim=[speed_track_sim tracks_sim{ind_t_sim(it)}.range*0+mean(tracks_sim{ind_t_sim(it)}.speed_knt)];
    yaw_track_mean_sim=[yaw_track_mean_sim tracks_sim{ind_t_sim(it)}.range*0+mean(tracks_sim{ind_t_sim(it)}.yaw_track)];
    tilt_track_mean_sim=[tilt_track_mean_sim tracks_sim{ind_t_sim(it)}.range*0+mean(tracks_sim{ind_t_sim(it)}.pitch_track)];
     tilt_track_std_sim=[tilt_track_std_sim tracks_sim{ind_t_sim(it)}.range*0+std(tracks_sim{ind_t_sim(it)}.pitch_track)];
      TS_track_mean_sim=[TS_track_mean_sim tracks_sim{ind_t_sim(it)}.range*0+10*log10(mean(10.^(tracks_sim{ind_t_sim(it)}.ts/10)))];
      TS_track_std_sim=[TS_track_std_sim tracks_sim{ind_t_sim(it)}.range*0+std(tracks_sim{ind_t_sim(it)}.ts)];
      nbtrous_track_sim=[nbtrous_track_sim tracks_sim{ind_t_sim(it)}.range*0+sum(tracks_sim{ind_t_sim(it)}.ping(2:end)-tracks_sim{ind_t_sim(it)}.ping(1:end-1)-1)/length(tracks_sim{ind_t_sim(it)}.ping)];
end
end




% on compense l'immersion
% ts_mat2=zeros(size(tsu_mat,1),size(tsu_mat,2)+max_ech);
% for i= 1:size(tsu_mat,1)
%     ts_mat2(i,immersion(i)+1:immersion(i)+size(tsu_mat,2))=ts_mat(i,:);
% end

% det_r=[2870 2950];
% det_r=[2780 2880];
% det_r=[2680 2840];
% det_r=[2690 2860];
det_r=[2480 2680];
% det_r=[2520 2700];

% det_r=[2220 2370];
% det_r=[2280 2440];

% det_r=[125 250];

det_r2=[1 400]; 

ind_sim=find(range_sim/delta+immersion_sim/delta>det_r(1) & range_sim/delta+immersion_sim/delta<det_r(2));
ind_hac=find(range_hac/delta+immersion_hac/delta>det_r(1) & range_hac/delta+immersion_hac/delta<det_r(2));
ind_track=find(range_track/delta+immersion_eroc/delta>det_r(1) & range_track/delta+immersion_eroc/delta<det_r(2)  & nbtrous_track<=0.3 );
ind_track_sim=find(range_track_sim/delta+immersion_eroc_sim/delta>det_r(1) & range_track_sim/delta+immersion_eroc_sim/delta<det_r(2)  & nbtrous_track_sim<=0.3 );
    
Gain_adjust=(17.76-0.66)-(15.5-0.99);
% Gain_adjust=(17.76-0.66)-(15.71-0.99);

Track_ifr=[ts_track(ind_track)+2*Gain_adjust;x_track(ind_track);y_track(ind_track);z_track(ind_track);speed_track(ind_track);tilt_track_mean(ind_track);yaw_track_mean(ind_track)-mean(heading)];
Track_sim=[ts_track_sim(ind_track_sim)+2*Gain_adjust;x_track_sim(ind_track_sim);y_track_sim(ind_track_sim);z_track_sim(ind_track_sim);speed_track_sim(ind_track_sim);tilt_track_mean_sim(ind_track_sim);yaw_track_mean_sim(ind_track_sim)-mean(heading)];
ts_raw_hac=ts_hac(ind_hac)+2*Gain_adjust;
ts_raw_sim=ts_sim(ind_sim)+2*Gain_adjust;
save([chemin_ini,'Result_',num2str(index_sondeur),'_',datestr(datestart1,'HH-MM'),'_',datestr(dateend2,'HH-MM')],'Track_ifr','Track_sim','ts_raw_hac','ts_raw_sim');

ind=find(ts_mat+2*Gain_adjust<-60);
ts_mat2=ts_mat;
ts_mat2(ind)=0;
%affichage
figure;imagesc(ts_mat2'+2*Gain_adjust,[-60 -35]) %echogramme TS
hold on;
%d�tections Ifremer brutes
plot(ip_hac(ind_hac),range_hac(ind_hac)/delta+3,'g*','markersize',1);
%d�tections Simrad brutes
plot(ip_sim(ind_sim),range_sim(ind_sim)/delta+3,'r*','markersize',1);
%d�tections Ifremer track�es
plot(ip_track(ind_track),range_track(ind_track)/delta+3,'go','markersize',5);
%d�tection Simrad track�es
plot(ip_track_sim(ind_track_sim),range_track_sim(ind_track_sim)/delta+3,'rs','markersize',5);
lgnd=legend('TS Ifremer','TS Simrad','TS Ifr track','TS Sim track');
colormap gray
% annotation('textbox', [0.5,0.15,0.1,0.1],...
%     'String',['Immersion: mean ' ,num2str(mean(immersion),'%.1f'),' std ', num2str(std(immersion),'%.1f');'Pitch     : mean ' ,num2str(mean(pitch),'%.1f'),' std ', num2str(std(pitch),'%.1f');'Roll      : mean ' ,num2str(mean(roll),'%.1f'),' std ', num2str(std(roll),'%.1f')],'BackgroundColor','w')

colorbar;
toto=xlabel('Ping number');
set(toto,'FontSize',16)
toto=ylabel('Sample number');
set(toto,'FontSize',16)
set(gca,'FontSize',14)

axis([1 ip_hac(end) det_r2(1) det_r2(2)]);
% colormap jet;
saveas(gcf,[chemin_ini,'Echogram_',num2str(index_sondeur),'_',datestr(datestart1,'HH-MM'),'_',datestr(dateend2,'HH-MM')],'fig')
saveas(gcf,[chemin_ini,'Echogram_',num2str(index_sondeur),'_',datestr(datestart1,'HH-MM'),'_',datestr(dateend2,'HH-MM')],'png')
% 

col='mrgbkc';

 curcolmap = [[1 0 1];[1 0 0];[0 1 0];[0 0 1];[0 0 0];[1 0 0]];
 curmapsize = size(curcolmap,1);

 
figure;
[hAx,hLine1,hLine2] = plotyy(ip_hac,immersion_hac,[ip_hac',ip_hac'],[(pitch_hac)',(roll_hac)']);
legend('Immersion','Pitch','Roll');
toto=xlabel('Ping number');
set(toto,'FontSize',16)
toto=ylabel(hAx(1),'Immersion (m)');
set(toto,'FontSize',16)
toto=ylabel(hAx(2),'Pitch/Roll (�)');
set(toto,'FontSize',16)
set(gca,'FontSize',14)
title(['Attitude variation for EROC'])
set(hAx(1),'YDir','reverse');
hold(hAx(1),'on')
scatter(hAx(1),ip_track(ind_track),z_track(ind_track)+immersion_eroc(ind_track),30,curcolmap(1+mod(id_track(ind_track),curmapsize),:));
 ylim(hAx(1),[min(immersion_eroc(ind_track))-2 max(z_track(ind_track)+immersion_eroc(ind_track))+2]);

mean(unique(immersion_eroc_track_deltamean(ind_track)))
mean(unique(immersion_eroc_track_deltastd(ind_track)))
mean(unique(pitch_eroc_track_deltamean(ind_track)))
mean(unique(pitch_eroc_track_deltastd(ind_track)))
mean(unique(roll_eroc_track_deltamean(ind_track)))
mean(unique(roll_eroc_track_deltastd(ind_track)))

    saveas(gcf,[chemin_ini,'Attitude_',num2str(index_sondeur),'_',datestr(datestart1,'HH-MM'),'_',datestr(dateend2,'HH-MM')],'fig')
    saveas(gcf,[chemin_ini,'Attitude_',num2str(index_sondeur),'_',datestr(datestart1,'HH-MM'),'_',datestr(dateend2,'HH-MM')],'png')
% 


%affichage
figure;

hold on;
scatter(ip_track,range_track/delta+3+immersion_eroc,10,ts_track);
%d�tection Simrad track�es
scatter(ip_track_sim,range_track_sim/delta+3+immersion_eroc_sim,30,ts_track_sim,'s');
lgnd=legend('TS Ifremer','TS Simrad','TS Ifr track','TS Sim track');
axis([1 ip_hac(end) det_r(1) det_r(2)]);
set(gca,'YDir','reverse');

minz = min(id_track(ind_track)); maxz = max(id_track(ind_track));

col='mrgbykcw';

 curcolmap = [[1 0 1];[1 0 0];[0 1 0];[0 0 1];[1 1 0];[0 0 0];[1 0 0]];
 curmapsize = size(curcolmap,1);
 
% %affichage de toutes les tracks
% figure;
% scatter(x_track(ind_track),y_track(ind_track),10,curcolmap(1+mod(id_track(ind_track),curmapsize),:));xlabel('along m');ylabel('athwart m');grid on; 
% 
% 
% figure;
% scatter(x_track(ind_track),-z_track(ind_track),10,curcolmap(1+mod(id_track(ind_track),curmapsize),:));xlabel('ping');ylabel('-z m');grid on;
% 
% figure;
% scatter3(x_track(ind_track),y_track(ind_track),-z_track(ind_track),10,curcolmap(1+mod(id_track(ind_track),curmapsize),:));xlabel('along m');ylabel('athwart m');zlabel('depth m');grid on; 
% 
% 
% figure;
% scatter(immersion_eroc(ind_track)*delta,range_track(ind_track),10,curcolmap(1+mod(id_track(ind_track),curmapsize),:));
% 
% figure;
% scatter(pitch_eroc(ind_track),al_track(ind_track),10,curcolmap(1+mod(id_track(ind_track),curmapsize),:));
% 
% figure;
% scatter(roll_eroc(ind_track),at_track(ind_track),10,curcolmap(1+mod(id_track(ind_track),curmapsize),:));


Al_hac2=al_track(ind_track);
At_hac2=at_track(ind_track);
TS_hac2=ts_track(ind_track)+2*Gain_adjust;
TSU_hac2=tsu_track(ind_track)+2*Gain_adjust;

% Al_hac2=al_hac(ind_hac);
% At_hac2=at_hac(ind_hac);
% TS_hac2=ts_hac(ind_hac)+2*Gain_adjust;
% TSU_hac2=tsu_hac(ind_hac)+2*Gain_adjust;

Al_sim2=al_track_sim(ind_track_sim);
At_sim2=at_track_sim(ind_track_sim);
TS_sim2=ts_track_sim(ind_track_sim)+2*Gain_adjust;
TSU_sim2=tsu_track_sim(ind_track_sim)+2*Gain_adjust;

% Al_sim2=al_sim(ind_sim);
% At_sim2=at_sim(ind_sim);
% TS_sim2=ts_sim(ind_sim)+2*Gain_adjust;
% TSU_sim2=tsu_sim(ind_sim)+2*Gain_adjust;

edge=(-65:0.5:-35);
steering=(pi/180)*[ 0];
openingAthwart=pi/180*[  11];
openingAlong=pi/180*[  11];
tetaminAt=min(steering-openingAthwart/2);
tetamaxAt=max(steering+openingAthwart/2);
tetaminAl=-max(openingAlong/2);
tetamaxAl=max(openingAlong/2);
nbanglesAt=7;
nbanglesAl=7;

angleAthwart=NaN(1,nbanglesAt);
angleAlong=NaN(1,nbanglesAl);



TSmean_hac=10*log10(mean(10.^(TS_hac2/10)));
TSmean_sim=10*log10(mean(10.^(TS_sim2/10)));

TSquantile_hac=10*log10(prctile(10.^(TS_hac2/10), [5 95]));
TSquantile_sim=10*log10(prctile(10.^(TS_sim2/10), [5 95]));

Nb_hac=histc(TS_hac2,edge);
Nb_sim=histc(TS_sim2,edge);


figure
bar_h=bar(edge,[Nb_hac' Nb_sim']);
set(bar_h(1),'facecolor','g') 
set(bar_h(2),'facecolor','r') 
hold on;
toto=xlabel('TS (dB)');
set(toto,'FontSize',16)
toto=ylabel('Count');
set(toto,'FontSize',16)
%        colormap('HSV');
y_lim = [0, max(Nb_hac)];
plot(TSmean_hac*ones(size(y_lim)), y_lim, 'g--','LineWidth',3)
plot(TSmean_sim*ones(size(y_lim)), y_lim, 'r--','LineWidth',3)
xlim([edge(1)-2 edge(end)+2]);
legend('TS Ifr track','TS Sim Track');
annotation('textbox', [0.2,0.8,0.1,0.1],...
    'String',['TSmean Ifremer : ' ,num2str(TSmean_hac,'%.1f');'TSmean Simrad  : ' ,num2str(TSmean_sim,'%.1f')])
    saveas(gcf,[chemin_ini,'TShist_',num2str(index_sondeur),'_',datestr(datestart1,'HH-MM'),'_',datestr(dateend2,'HH-MM')],'fig')
    saveas(gcf,[chemin_ini,'TShist_',num2str(index_sondeur),'_',datestr(datestart1,'HH-MM'),'_',datestr(dateend2,'HH-MM')],'png')

% figure
% h=errorbar([1 2],[TSmean_hac TSmean_sim],[TSmean_hac-TSquantile_hac(1) TSmean_sim-TSquantile_sim(1)],[TSquantile_hac(2)-TSmean_hac TSquantile_sim(2)-TSmean_sim])
% hc = get(h, 'Children')
% set(hc(1),'color','g') 
% set(hc(2),'color','r') 
% 
% 
% 
% toto=xlabel('TS (dB)');
% set(toto,'FontSize',16)
% toto=ylabel('Count');
% set(toto,'FontSize',16)
% 
%     saveas(gcf,[chemin_ini,'TSboxplot_',num2str(index_sondeur),'_',datestr(datestart1,'HH-MM'),'_',datestr(dateend2,'HH-MM')],'fig')
%     saveas(gcf,[chemin_ini,'TSboxplot_',num2str(index_sondeur),'_',datestr(datestart1,'HH-MM'),'_',datestr(dateend2,'HH-MM')],'png')

    
    edge2=(-50:0.2:-40);
Nb_hac_mean=histc(unique(TS_track_mean+2*Gain_adjust),edge2);
Nb_sim_mean=histc(unique(TS_track_mean_sim+2*Gain_adjust),edge2);

figure
bar_h=bar(edge2,[Nb_hac_mean' Nb_sim_mean']);
set(bar_h(1),'facecolor','g') 
set(bar_h(2),'facecolor','r') 
hold on;
toto=xlabel('TS (dB)');
set(toto,'FontSize',16)
toto=ylabel('Count');
set(toto,'FontSize',16)



% figure
% hold on;
% scatter((180/pi)*At_hac2,(180/pi)*Al_hac2,2,'g');
% scatter((180/pi)*At_sim2,(180/pi)*Al_sim2,2,'r');
% plot((180/pi)*angleAthwart,repmat((180/pi)*angleAlong,length(angleAthwart),1),'r+','MarkerSize',20);
% 
% toto=xlabel('Alongship angle (�)');
% set(toto,'FontSize',16)
% toto=ylabel('Athwartship angle (�)');
% set(toto,'FontSize',16)
% 
% saveas(gcf,[chemin_ini,'TargetPosition_',num2str(index_sondeur),'_',datestr(datestart1,'HH-MM'),'_',datestr(dateend2,'HH-MM')],'fig')
% saveas(gcf,[chemin_ini,'TargetPosition_',num2str(index_sondeur),'_',datestr(datestart1,'HH-MM'),'_',datestr(dateend2,'HH-MM')],'png')


n=zeros(2,16);
for j=1:size(n,2)
    for k=1:length(Al_hac2)
        if ( TS_hac2(k)-TSU_hac2(k)>=(j-1) && TS_hac2(k)-TSU_hac2(k)<j)
            n(1,j)=n(1,j)+1;
        end
    end
    for k=1:length(Al_sim2)
        if ( TS_sim2(k)-TSU_sim2(k)>=(j-1) && TS_sim2(k)-TSU_sim2(k)<j)
            n(2,j)=n(2,j)+1;
        end
    end
end

figure
bar_h=bar([1:1:size(n,2)],n');
set(bar_h(1),'facecolor','g') 
set(bar_h(2),'facecolor','r') 

toto=xlabel('TScomp-TSuncomp (dB)');
legend('TS Ifr track','TS Sim track');
set(toto,'FontSize',16)
    saveas(gcf,[chemin_ini,'NbTargethist_',num2str(index_sondeur),'_',datestr(dateheure(1),'HH-MM'),'_',datestr(dateheure(end),'HH-MM')],'fig')
    saveas(gcf,[chemin_ini,'NbTargethist_',num2str(index_sondeur),'_',datestr(dateheure(1),'HH-MM'),'_',datestr(dateheure(end),'HH-MM')],'png')


for i=1:nbanglesAt
    angleat=asin(sin(tetaminAt)+(i)*(sin(tetamaxAt)-sin(tetaminAt))/(nbanglesAt+1));
    angleAthwart(i)=angleat;
end
for i=1:nbanglesAl
    angleal=asin(sin(tetaminAl)+(i)*(sin(tetamaxAl)-sin(tetaminAl))/(nbanglesAl+1));
    angleAlong(i)=angleal;
end

TSvsAmean_hac_Along=NaN(length(angleAlong),1);
TSvsApct_hac_Along=NaN(length(angleAlong),2);
Nb_hac_Along =  NaN(length(angleAlong), 1);

TSvsAmean_sim_Along=NaN(length(angleAlong),1);
TSvsApct_sim_Along=NaN(length(angleAlong),2);
Nb_sim_Along =  NaN(length(angleAlong), 1);

for  a=1:length(angleAlong)
    if a==1
        dist=(angleAlong(a)-tetaminAl)/2;
    elseif a==length(angleAlong)
        dist=(tetamaxAl-angleAlong(a))/2;
    else
        dist=(angleAlong(a+1)-angleAlong(a))/2;
    end
    indexal=(Al_hac2<=(angleAlong(a)+dist) & Al_hac2>=(angleAlong(a)-dist));
    if sum(indexal)>10
        temp = 10.^(TS_hac2/10);           % Get sigma for lengths and phi
        TSvsAmean_hac_Along(a) = 10*log10(mean(temp(indexal)))  ;          % Calculate mean
        TSvsApct_hac_Along(a,:) = 10*log10(prctile(temp(indexal), [5 95]));  % Calculate 95%
        Nb_hac_Along(a)=sum(indexal);
    end
    indexal=(Al_sim2<=(angleAlong(a)+dist) & Al_sim2>=(angleAlong(a)-dist));
    if sum(indexal)>10
        temp = 10.^(TS_sim2/10);           % Get sigma for lengths and phi
        TSvsAmean_sim_Along(a) = 10*log10(mean(temp(indexal)))  ;          % Calculate mean
        TSvsApct_sim_Along(a,:) = 10*log10(prctile(temp(indexal), [5 95]));  % Calculate 95%
        Nb_sim_Along(a)=sum(indexal);
    end
end


figure;
hold on;
plot(angleAlong((1:end))*180/pi,Nb_hac_Along,'g');
plot(angleAlong((1:end))*180/pi,Nb_sim_Along,'r');

set(gca, 'YTickMode', 'auto');
toto=xlabel('Steering (�)');
set(toto,'FontSize',16)
title(['Mean TS versus angleAlong'])

figure;
hold on;

errorbar(angleAlong(1:end)*180/pi,TSvsAmean_hac_Along,TSvsAmean_hac_Along-TSvsApct_hac_Along(:,1),TSvsApct_hac_Along(:,2)-TSvsAmean_hac_Along,'g','MarkerSize',8);
errorbar(angleAlong(1:end)*180/pi,TSvsAmean_sim_Along,TSvsAmean_sim_Along-TSvsApct_sim_Along(:,1),TSvsApct_sim_Along(:,2)-TSvsAmean_sim_Along,'r','MarkerSize',8);

plot(angleAlong(1:end)*180/pi,TSvsAmean_hac_Along,'g', 'LineWidth',3);
plot(angleAlong(1:end)*180/pi,TSvsAmean_sim_Along,'r', 'LineWidth',3);

toto=xlabel('Steering (�)');
set(toto,'FontSize',16)
toto=ylabel('TS (dB)');
set(toto,'FontSize',16)

set(gca,'FontSize',14)
title(['Mean TS versus alongship steering angle'])
grid on;
legend('TS Ifr track','TS Sim track');
axis([-5 5 -55 -35])
saveas(gcf,[chemin_ini,'TSversusangleAlong_',num2str(index_sondeur),'_',datestr(datestart1,'HH-MM'),'_',datestr(dateend2,'HH-MM')],'fig')
saveas(gcf,[chemin_ini,'TSversusangleAlong_',num2str(index_sondeur),'_',datestr(datestart1,'HH-MM'),'_',datestr(dateend2,'HH-MM')],'png')


TSvsAmean_hac_Athwart=NaN(length(angleAthwart),1);
TSvsApct_hac_Athwart=NaN(length(angleAthwart),2);
Nb_hac_Athwart =  NaN(length(angleAthwart), 1);

TSvsAmean_sim_Athwart=NaN(length(angleAthwart),1);
TSvsApct_sim_Athwart=NaN(length(angleAthwart),2);
Nb_sim_Athwart =  NaN(length(angleAthwart), 1);

for  a=1:length(angleAthwart)
    if a==1
        dist=(angleAthwart(a)-tetaminAt)/2;
    elseif a==length(angleAthwart)
        dist=(tetamaxAt-angleAthwart(a))/2;
    else
        dist=(angleAthwart(a+1)-angleAthwart(a))/2;
    end
    indexAt=(At_hac2<=(angleAthwart(a)+dist) & At_hac2>=(angleAthwart(a)-dist));
    if sum(indexAt)>10
        temp = 10.^(TS_hac2/10);           % Get sigma for lengths and phi
        TSvsAmean_hac_Athwart(a) = 10*log10(mean(temp(indexAt)))  ;          % Calculate mean
        TSvsApct_hac_Athwart(a,:) = 10*log10(prctile(temp(indexAt), [5 95]));  % Calculate 95%
        Nb_hac_Athwart(a)=sum(indexAt);
    end
    indexAt=(At_sim2<=(angleAthwart(a)+dist) & At_sim2>=(angleAthwart(a)-dist));
    if sum(indexAt)>10
        temp = 10.^(TS_sim2/10);           % Get sigma for lengths and phi
        TSvsAmean_sim_Athwart(a) = 10*log10(mean(temp(indexAt)))  ;          % Calculate mean
        TSvsApct_sim_Athwart(a,:) = 10*log10(prctile(temp(indexAt), [5 95]));  % Calculate 95%
        Nb_sim_Athwart(a)=sum(indexAt);
    end
end


figure;
hold on;
plot(angleAthwart((1:end))*180/pi,Nb_hac_Athwart,'g');
plot(angleAthwart((1:end))*180/pi,Nb_sim_Athwart,'r');

set(gca, 'YTickMode', 'auto');
toto=xlabel('Steering (�)');
set(toto,'FontSize',16)
title(['Mean TS versus angleAthwart'])

figure;
hold on;

errorbar(angleAthwart(1:end)*180/pi,TSvsAmean_hac_Athwart,TSvsAmean_hac_Athwart-TSvsApct_hac_Athwart(:,1),TSvsApct_hac_Athwart(:,2)-TSvsAmean_hac_Athwart,'g','MarkerSize',8);
errorbar(angleAthwart(1:end)*180/pi,TSvsAmean_sim_Athwart,TSvsAmean_sim_Athwart-TSvsApct_sim_Athwart(:,1),TSvsApct_sim_Athwart(:,2)-TSvsAmean_sim_Athwart,'r','MarkerSize',8);

plot(angleAthwart(1:end)*180/pi,TSvsAmean_hac_Athwart,'g', 'LineWidth',3);
plot(angleAthwart(1:end)*180/pi,TSvsAmean_sim_Athwart,'r', 'LineWidth',3);

toto=xlabel('Steering (�)');
set(toto,'FontSize',16)
toto=ylabel('TS (dB)');
set(toto,'FontSize',16)

set(gca,'FontSize',14)
title(['Mean TS versus Athwartship steering angle'])
grid on;
legend('TS Ifr track','TS Sim track');
axis([-5 5 -55 -35])
saveas(gcf,[chemin_ini,'TSversusangleAthwart_',num2str(index_sondeur),'_',datestr(datestart1,'HH-MM'),'_',datestr(dateend2,'HH-MM')],'fig')
saveas(gcf,[chemin_ini,'TSversusangleAthwart_',num2str(index_sondeur),'_',datestr(datestart1,'HH-MM'),'_',datestr(dateend2,'HH-MM')],'png')

% figure;plot(ip_sim(ind_sim),TS_sim2,'*');hold on;plot(ip_track(ind_track),TS_hac2,'mo');hold off;title('ts')
% figure;plot(range_sim(ind_sim)/delta+3+immersion(ind_sim),TS_sim2,'*');hold on;plot(range_track(ind_track)/delta+3+immersion(ind_track),TS_hac2,'mo');hold off;title('ts')

% figure;
% scatter(ip_track_sim(ind_track_sim),range_track_sim(ind_track_sim)/delta+3+immersion_eroc_sim(ind_track_sim),10,al_track_sim(ind_track_sim));
% set(gca,'YDir','reverse');
% colormap jet
% title('TS Simrad track');
% % caxis([-55 -40]);
% axis([1 ip_hac(end) det_r(1) det_r(2)]);

% figure;
% scatter(ip_track_sim(ind_track_sim),range_track_sim(ind_track_sim)/delta+3+immersion_eroc_sim(ind_track_sim),10,TS_sim2);
% set(gca,'YDir','reverse');
% colormap jet
% title('TS Simrad track');
% caxis([-55 -40]);
% axis([1 ip_hac(end) det_r(1) det_r(2)]);


figure;
scatter(ip_track(ind_track),range_track(ind_track)/delta+3,10,id_track((ind_track)));
set(gca,'YDir','reverse');
colormap jet
title('TS Ifr track');
% caxis([-55 -40]);
axis([1 ip_hac(end) det_r2(1) det_r2(2)]);

figure;
scatter(ip_track(ind_track),range_track(ind_track)/delta+3,10,TS_hac2);
set(gca,'YDir','reverse');
colormap jet
title('TS Ifr track');
caxis([-55 -40]);
axis([1 ip_hac(end) det_r2(1) det_r2(2)]);


idtrack=unique(id_track(ind_track));

for i=1:length(idtrack)
    nb(i)=length(find((id_track(ind_track)==idtrack(i))));
end

figure;
scatter(x_track(ind_track),z_track(ind_track),10,curcolmap(1+mod(id_track(ind_track),curmapsize),:));
set(gca,'YDir','reverse');
% colormap jet
title('Pitch Ifremer (�)');
str1=blanks(50);
str2=blanks(50);
pitchstr=['Pitch: mean ' ,num2str(mean(tilt_track_mean(ind_track)),'%.1f'),' std ', num2str(std(tilt_track_mean(ind_track)),'%.1f')];
trackstr=['Track:',num2str(length(unique(id_track(ind_track))),'%d'),' TotDetect:',num2str(round(sum(nb)),'%d'),' MeanDetect:',num2str(round(mean(nb)),'%d'),' StdDetect:',num2str(round(std(nb)),'%d')];
str1(1:length(pitchstr))=pitchstr;
str2(1:length(trackstr))=trackstr;
annotation('textbox', [0.3,0.8,0.1,0.1],...
    'String',[str1;str2])
% colorbar;
annotation('textbox', [0.5,0.15,0.1,0.1],...
    'String',['Immersion: mean ' ,num2str(mean(immersion_eroc(ind_track)),'%.1f'),' std ', num2str(std(immersion_eroc(ind_track)),'%.1f');'Pitch     : mean ' ,num2str(mean(pitch_eroc(ind_track)),'%.1f'),' std ', num2str(std(pitch_eroc(ind_track)),'%.1f');'Roll      : mean ' ,num2str(mean(roll_eroc(ind_track)),'%.1f'),' std ', num2str(std(roll_eroc(ind_track)),'%.1f')],'BackgroundColor','w')

toto=xlabel('x (m)');
set(toto,'FontSize',16)
toto=ylabel('z (m)');
set(toto,'FontSize',16)
axis('equal');
caxis([-5+mean(tilt_track_mean(ind_track)) 5+mean(tilt_track_mean(ind_track))]);
saveas(gcf,[chemin_ini,'TrackPitch_',num2str(index_sondeur),'_',datestr(datestart1,'HH-MM'),'_',datestr(dateend2,'HH-MM')],'fig')
saveas(gcf,[chemin_ini,'TrackPitch_',num2str(index_sondeur),'_',datestr(datestart1,'HH-MM'),'_',datestr(dateend2,'HH-MM')],'png')

figure;
scatter(x_track(ind_track),y_track(ind_track),10,curcolmap(1+mod(id_track(ind_track),curmapsize),:));
% hold on;
% plot(x_track(ind_track(end)),y_track(ind_track(end)),'rs','Markersize',10);
% set(gca,'YDir','reverse');
% colormap jet
title('Yaw Ifremer (�)');
str1=blanks(50);
str2=blanks(50);
yawstr=['Yaw: mean ' ,num2str(mean(heading)-mean(yaw_track_mean(ind_track)),'%.1f'),' std ', num2str(std(yaw_track_mean(ind_track)),'%.1f')];
trackstr=['Track:',num2str(length(unique(id_track(ind_track))),'%d'),' TotDetect:',num2str(round(sum(nb)),'%d'),' MeanDetect:',num2str(round(mean(nb)),'%d'),' StdDetect:',num2str(round(std(nb)),'%d')];
str1(1:length(yawstr))=yawstr;
str2(1:length(trackstr))=trackstr;
annotation('textbox', [0.3,0.8,0.1,0.1],...
    'String',[str1;str2])
% colorbar;
toto=xlabel('x (m)');
set(toto,'FontSize',16)
toto=ylabel('y (m)');
set(toto,'FontSize',16)
axis('equal');
caxis([-10+mean(heading)-mean(yaw_track_mean(ind_track)) 10+mean(heading)-mean(yaw_track_mean(ind_track))]);
saveas(gcf,[chemin_ini,'TrackYaw_',num2str(index_sondeur),'_',datestr(datestart1,'HH-MM'),'_',datestr(dateend2,'HH-MM')],'fig')
saveas(gcf,[chemin_ini,'TrackYaw_',num2str(index_sondeur),'_',datestr(datestart1,'HH-MM'),'_',datestr(dateend2,'HH-MM')],'png')

idtrack_sim=unique(id_track_sim(ind_track_sim));

for i=1:length(idtrack_sim)
    nb_sim(i)=length(find((id_track_sim(ind_track_sim)==idtrack_sim(i))));
end

figure;
scatter(x_track_sim(ind_track_sim),z_track_sim(ind_track_sim),10,curcolmap(1+mod(id_track_sim(ind_track_sim),curmapsize),:));
set(gca,'YDir','reverse');
% colormap jet
title('Pitch Simrad (�)');
str1=blanks(50);
str2=blanks(50);
pitchstr=['Pitch: mean ' ,num2str(mean(tilt_track_mean_sim(ind_track_sim)),'%.1f'),' std ', num2str(std(tilt_track_mean_sim(ind_track_sim)),'%.1f')];
trackstr=['Track:',num2str(length(unique(id_track_sim(ind_track_sim))),'%d'),' TotDetect:',num2str(round(sum(nb_sim)),'%d'),' MeanDetect:',num2str(round(mean(nb_sim)),'%d'),' StdDetect:',num2str(round(std(nb_sim)),'%d')];
str1(1:length(pitchstr))=pitchstr;
str2(1:length(trackstr))=trackstr;
annotation('textbox', [0.3,0.8,0.1,0.1],...
    'String',[str1;str2])
% colorbar;
toto=xlabel('x (m)');
set(toto,'FontSize',16)
toto=ylabel('z (m)');
set(toto,'FontSize',16)
axis('equal');
caxis([-5+mean(tilt_track_mean_sim(ind_track_sim)) 5+mean(tilt_track_mean_sim(ind_track_sim))]);
saveas(gcf,[chemin_ini,'TrackPitchSim_',num2str(index_sondeur),'_',datestr(datestart1,'HH-MM'),'_',datestr(dateend2,'HH-MM')],'fig')
saveas(gcf,[chemin_ini,'TrackPitchSim_',num2str(index_sondeur),'_',datestr(datestart1,'HH-MM'),'_',datestr(dateend2,'HH-MM')],'png')

figure;
scatter(x_track_sim(ind_track_sim),y_track_sim(ind_track_sim),10,curcolmap(1+mod(id_track_sim(ind_track_sim),curmapsize),:));
% set(gca,'YDir','reverse');
% colormap jet
title('Yaw Simrad (�)');
str1=blanks(50);
str2=blanks(50);
yawstr=['Yaw: mean ' ,num2str(mean(heading)-mean(yaw_track_mean_sim(ind_track_sim)),'%.1f'),' std ', num2str(std(yaw_track_mean_sim(ind_track_sim)),'%.1f')];
trackstr=['Track:',num2str(length(unique(id_track_sim(ind_track_sim))),'%d'),' TotDetect:',num2str(round(sum(nb_sim)),'%d'),' MeanDetect:',num2str(round(mean(nb_sim)),'%d'),' StdDetect:',num2str(round(std(nb_sim)),'%d')];
str1(1:length(yawstr))=yawstr;
str2(1:length(trackstr))=trackstr;
annotation('textbox', [0.3,0.8,0.1,0.1],...
    'String',[str1;str2])
% colorbar;
toto=xlabel('x (m)');
set(toto,'FontSize',16)
toto=ylabel('y (m)');
set(toto,'FontSize',16)
axis('equal');
caxis([-10+mean(heading)-mean(yaw_track_mean_sim(ind_track_sim)) 10+mean(heading)-mean(yaw_track_mean_sim(ind_track_sim))]);
saveas(gcf,[chemin_ini,'TrackYawSim_',num2str(index_sondeur),'_',datestr(datestart1,'HH-MM'),'_',datestr(dateend2,'HH-MM')],'fig')
saveas(gcf,[chemin_ini,'TrackYawSim_',num2str(index_sondeur),'_',datestr(datestart1,'HH-MM'),'_',datestr(dateend2,'HH-MM')],'png')
% 
% figure;
% plot(tilt_track_mean(ind_track),TS_track_mean(ind_track)+2*Gain_adjust,'g*');
% hold on;
% plot(tilt_track_mean_sim(ind_track_sim),TS_track_mean(ind_track_sim)+2*Gain_adjust,'r*');
% 
% figure;
% plot(yaw_track_mean(ind_track),TS_track_mean(ind_track)+2*Gain_adjust,'g*');
% hold on;
% plot(yaw_track_mean_sim(ind_track_sim),TS_track_mean(ind_track_sim)+2*Gain_adjust,'r*');


figure;
plot(tilt_track_std(ind_track),TS_track_mean(ind_track)+2*Gain_adjust,'g*');
hold on;
plot(tilt_track_std_sim(ind_track_sim),TS_track_mean_sim(ind_track_sim)+2*Gain_adjust,'r*');
 set(gca,'FontSize',14)
xlabel('Tilt track std (�)');
ylabel('Median track TS (dB)');
saveas(gcf,[chemin_ini,'TrackTSvspitchsd_',num2str(index_sondeur),'_',datestr(datestart1,'HH-MM'),'_',datestr(dateend2,'HH-MM')],'fig')
saveas(gcf,[chemin_ini,'TrackTSvspitchsd_',num2str(index_sondeur),'_',datestr(datestart1,'HH-MM'),'_',datestr(dateend2,'HH-MM')],'png')

figure;
plot(tilt_track_mean(ind_track),tilt_track_std(ind_track),'g*');
hold on;
plot(tilt_track_mean_sim(ind_track_sim),tilt_track_std_sim(ind_track_sim),'r*');
 set(gca,'FontSize',14)
xlabel('Tilt track mean(�)');
ylabel('Tilt track std(�)');
saveas(gcf,[chemin_ini,'TrackAttitude_',num2str(index_sondeur),'_',datestr(datestart1,'HH-MM'),'_',datestr(dateend2,'HH-MM')],'fig')
saveas(gcf,[chemin_ini,'TrackAttitude_',num2str(index_sondeur),'_',datestr(datestart1,'HH-MM'),'_',datestr(dateend2,'HH-MM')],'png')

figure;
plot(tilt_track_std(ind_track),TS_track_std(ind_track),'g*');
hold on;
plot(tilt_track_std_sim(ind_track_sim),TS_track_std_sim(ind_track_sim),'r*');
 set(gca,'FontSize',14)
%  text(tilt_track_std(ind_track),TS_track_std(ind_track),num2str(id_track(ind_track)),'FontWeight','bold');
xlabel('Tilt track std(�)');
ylabel('TS track std(�)');
saveas(gcf,[chemin_ini,'TrackAttitude_',num2str(index_sondeur),'_',datestr(datestart1,'HH-MM'),'_',datestr(dateend2,'HH-MM')],'fig')
saveas(gcf,[chemin_ini,'TrackAttitude_',num2str(index_sondeur),'_',datestr(datestart1,'HH-MM'),'_',datestr(dateend2,'HH-MM')],'png')

edge=[min(tilt_track_mean(ind_track)):0.5:max(tilt_track_mean(ind_track))];
Tilt_hac=histc(unique(tilt_track_mean(ind_track)),edge);
Tilt_sim=histc(unique(tilt_track_mean_sim(ind_track_sim)),edge);


figure
bar_h=bar(edge,[Tilt_hac' Tilt_sim']);
set(bar_h(1),'facecolor','g') 
set(bar_h(2),'facecolor','r') 
hold on;
toto=xlabel('Angle (�)');
set(toto,'FontSize',16)
toto=ylabel('Count');
set(toto,'FontSize',16)
%        colormap('HSV');
y_lim = [0, max(Tilt_hac)];
plot(mean(tilt_track_mean(ind_track))*ones(size(y_lim)), y_lim, 'g--','LineWidth',3)
plot(mean(tilt_track_mean_sim(ind_track_sim))*ones(size(y_lim)), y_lim, 'r--','LineWidth',3)
xlim([edge(1)-2 edge(end)+2]);
legend('Pitch Ifr track','Pitch Sim Track');
   saveas(gcf,[chemin_ini,'Pitchhist_',num2str(index_sondeur),'_',datestr(datestart1,'HH-MM'),'_',datestr(dateend2,'HH-MM')],'fig')
    saveas(gcf,[chemin_ini,'Pitchhist_',num2str(index_sondeur),'_',datestr(datestart1,'HH-MM'),'_',datestr(dateend2,'HH-MM')],'png')

    
edge=[min(yaw_track_mean(ind_track)):0.5:max(yaw_track_mean(ind_track))];
yaw_hac=histc(unique(yaw_track_mean(ind_track)),edge);
yaw_sim=histc(unique(yaw_track_mean_sim(ind_track_sim)),edge);


figure
bar_h=bar(edge,[yaw_hac' yaw_sim']);
set(bar_h(1),'facecolor','g') 
set(bar_h(2),'facecolor','r') 
hold on;
toto=xlabel('Angle (�)');
set(toto,'FontSize',16)
toto=ylabel('Count');
set(toto,'FontSize',16)
%        colormap('HSV');
y_lim = [0, max(yaw_hac)];
plot(mean(yaw_track_mean(ind_track))*ones(size(y_lim)), y_lim, 'g--','LineWidth',3)
plot(mean(yaw_track_mean_sim(ind_track_sim))*ones(size(y_lim)), y_lim, 'r--','LineWidth',3)
xlim([edge(1)-2 edge(end)+2]);
legend('Heading Ifr track','Heading Sim Track');
   saveas(gcf,[chemin_ini,'Yawhhist_',num2str(index_sondeur),'_',datestr(datestart1,'HH-MM'),'_',datestr(dateend2,'HH-MM')],'fig')
    saveas(gcf,[chemin_ini,'Yawhist_',num2str(index_sondeur),'_',datestr(datestart1,'HH-MM'),'_',datestr(dateend2,'HH-MM')],'png')

    
if (0) %�tude de fusions de tracks
    %fusion de tracks
    list_it=[123 128 129]; %index des tracks � fusionner
    
    track_fus.x=[];
    track_fus.y=[];
    track_fus.z=[];
    track_fus.ts=[];
    track_fus.tsu=[];
    track_fus.range=[];
    track_fus.alongRad=[];
    track_fus.athwartRad=[];
    track_fus.ping=[];
    track_fus.label=[];
    for it=1:length(list_it)
        track_fus.ping=[track_fus.ping tracks{list_it(it)}.ping];
        track_fus.x=[track_fus.x tracks{list_it(it)}.x];
        track_fus.y=[track_fus.y tracks{list_it(it)}.y];
        track_fus.z=[track_fus.z tracks{list_it(it)}.z];
        track_fus.ts=[track_fus.ts tracks{list_it(it)}.ts];
        track_fus.tsu=[track_fus.tsu tracks{list_it(it)}.tsu];
        track_fus.range=[track_fus.range tracks{list_it(it)}.range];
        track_fus.alongRad=[track_fus.alongRad tracks{list_it(it)}.alongRad];
        track_fus.athwartRad=[track_fus.athwartRad tracks{list_it(it)}.athwartRad];
        track_fus.label=[track_fus.label tracks{list_it(it)}.x*0+list_it(it)];
    end
    dd=sqrt((track_fus.x(2:end)-track_fus.x(1:end-1)).^2+(track_fus.y(2:end)-track_fus.y(1:end-1)).^2+(track_fus.z(2:end)-track_fus.z(1:end-1)).^2);
    track_fus.speed_knt=[0 dd./(heure_hac_tot(track_fus.ping(2:end))-heure_hac_tot(track_fus.ping(1:end-1)))']*3600/1852;


    col='mrgbykcw';
    figure(1);plot(track_fus.x,track_fus.y,'*');xlabel('along m');ylabel('athwart m');grid on; title(['track fusionn�e ' 'ref parcours longit. eroc' ]);
    figure(2);plot(track_fus.x-x_eroc(track_fus.ping),track_fus.y,'*');xlabel('along m');ylabel('athwart m');grid on; title(['track fusionn�e ref transducteur' ]);
    figure(3);plot(track_fus.ping,track_fus.z,'*');xlabel('ping');ylabel('z m');grid on;title(['track fusionn�e z' ])
    figure(4);plot(track_fus.ping,track_fus.ts,'*');xlabel('ping');ylabel('TS dB');grid on;title(['TS track fusionn�e' ])
    figure(5);plot(track_fus.ping,track_fus.speed_knt,'*');xlabel('ping');ylabel('knt');grid on;title(['speed(knt) track fusionn�e' ])

end
