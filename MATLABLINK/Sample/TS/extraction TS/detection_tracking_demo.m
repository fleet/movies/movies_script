clear
dbstop if error
% index sondeur et transducteur � traiter (cf structure
% list_sounder lue dans le hac)
index_sondeur=1;
index_trans=1;
index_beam=1;
ismultibeam=0;

%% lecture du hac et des d�tections Simrad
%------------------------------------------
% r�gler la config et le nombre de
% chunk � lire dans la routine read_TS_hac.m

% goto �ventuel
chemin_ini='C:\data\IBTS10\tsme70\';
datestart =00*3600+18*60+50+(datenum('19-Jan-2010')-datenum('01-Jan-1970'))*24*3600;
dateend =00*3600+20*60+05+(datenum('19-Jan-2010')-datenum('01-Jan-1970'))*24*3600;
[heure_hac_tot,list_sounder,sv_mat,al_mat,ath_mat,ir,TS,TSrange,TSech,TScomp,TSucomp,TSalong,TSathwart,TSlabel,heading,vit_knt,pitchRad,rollRad,heave,lat,long]=read_TS_hac(chemin_ini,[chemin_ini,'IBTS10j_224_20120824_102155.hac'],datestart,dateend,index_sondeur,index_trans,index_beam,0);

%% d�tection de cibles simples
%------------------------------------------
% r�gler les param�tres d'extraction en d�but de code
[targets_hac,ts_mat]=detec_TS(list_sounder,sv_mat,al_mat,ath_mat,ir,index_sondeur,index_trans,index_beam,TSrange,TSech,TScomp,TSucomp,TSalong,TSathwart,ismultibeam);


% affichage d�tections
%---------------------
figure(1000);hold off;imagesc(ts_mat',[-110 -20]);title('TS compens�')%echogramme TS
delta=list_sounder(index_sondeur).m_transducer(index_trans).m_timeSampleInterval*list_sounder(index_sondeur).m_soundVelocity/2/10^6;

%d�tections Simrad
for ip=1:length(TS)
    np=length(TSech{ip});
    if np>0
    hold on;plot(ip*ones(1,np),TSrange{ip}/delta+3,'mo','lINeW',2);
    end
end

%d�tections Ifremer
for ip=1:length(targets_hac)
    np=length(targets_hac{ip}.targetRange);
    if np>0
    hold on;plot(ip*ones(1,np),targets_hac{ip}.targetRange/delta+3,'k*','lINeW',2);
    end
end
title('TS compens�, d�tection Simrad (mo) et Ifremer (k*)')

%% pistage
%------------
SeuilSpeed_knt=10;
ntrous=1;

if (0) % positions x y g�ographiques � partir des lat/long
    load Carto_0_6E;[x_ship y_ship]=latlon2xy(Carto, lat, long);
else %position par vitesse et cap
    clear x_ship y_ship
    x_ship(1)=0;y_ship(1)=0;
    for ip=2:size(sv_mat,1);
        x_ship(ip)=x_ship(ip-1)+vit_knt(ip)*1852/3600*(heure_hac_tot(ip)-heure_hac_tot(ip-1))*cosd(heading(ip));
        y_ship(ip)=y_ship(ip-1)+vit_knt(ip)*1852/3600*(heure_hac_tot(ip)-heure_hac_tot(ip-1))*sind(heading(ip));
    end
end

[tracks,targets_hac]=track_det(targets_hac,list_sounder,index_sondeur,index_trans,heure_hac_tot,heading,pitchRad,rollRad,x_ship,y_ship,heave,SeuilSpeed_knt,ntrous);

% affichage tracks 
%-----------------
col='mrgbykcw';
figure(1001);hold off;imagesc(ts_mat',[-110 -20]);title('TS compens�, d�tections Ifremer labelis�es')%echogramme TS
hold on;
for it=1:length(tracks)
plot(tracks{it}.ping,tracks{it}.range/delta+3,[col(1+mod(it,length(col))) 'o'],'lINeW',2);
text(tracks{it}.ping(1),tracks{it}.range(1)/delta+3,num2str(it));
end
hold off;

%filtrage des tracks
%------------------
for it=1:length(tracks)
    length_track(it)=length(tracks{it}.ping);
    dd=sqrt((tracks{it}.x(2:end)-tracks{it}.x(1:end-1)).^2+(tracks{it}.y(2:end)-tracks{it}.y(1:end-1)).^2+(tracks{it}.z(2:end)-tracks{it}.z(1:end-1)).^2);
    tracks{it}.speed_knt=[0 dd./(heure_hac_tot(tracks{it}.ping(2:end))-heure_hac_tot(tracks{it}.ping(1:end-1)))]*3600/1852;
    max_speed_knt(it)=max(tracks{it}.speed_knt);
    median_ts(it)=median(tracks{it}.ts);
end
ind_t=find(length_track>=2); % pistes de longeur sup�rieure � 10 d�tections

% affichage tracks filtr�es
figure(1002);hold off;imagesc(ts_mat',[-110 -20]);title('TS compens�, pistes > 10 det')%echogramme TS
hold on;
for it=1:length(ind_t)
plot(tracks{ind_t(it)}.ping,tracks{ind_t(it)}.range/delta+3,[col(1+mod(it,length(col))) '*'],'lINeW',4);
text(tracks{ind_t(it)}.ping(1),tracks{ind_t(it)}.range(1)/delta+3,num2str(ind_t(it)),'FontWeight','bold');
end
hold off;
