clear all
 close all


mydir  = pwd;
idcs   = strfind(mydir,'\')
pathUseful = [mydir(1:idcs(end-1)-1) '\UsefulFunctions'];

addpath(genpath(pathUseful)) 

index_sondeur=1;
index_trans=1;
index_beam=1;

chemin_ini='L:\MAYOBS\MAYOBS4\EK80\DONNEES\HAC\MD223_MAYOBS4\RUN001\';
chemin_cfg='L:\MAYOBS\MAYOBS4\EK80\DONNEES\HAC\MD223_MAYOBS4\Config\TSAnlaysis\';


datestart =10*3600+25*60+00+(datenum('20-July-2019')-datenum('01-Jan-1970'))*24*3600; %debut raw
dateend =10*3600+37*60+00+(datenum('20-July-2019')-datenum('01-Jan-1970'))*24*3600;

[heure_hac_tot,list_sounder,sv_mat,al_mat,ath_mat,ir,TS,TSrange,TSech,TScomp,TSucomp,TSalong,TSathwart,TSposition,TSpositionGPS,TSlabel,TSfreq,heading,vit_knt,pitchRad,rollRad,heave,lat,long]=read_TS_hac(chemin_ini,'MD223_MAYOBS4_001_20190720_102830.hac',chemin_cfg,datestart,dateend,index_sondeur,index_trans,index_beam,0);

% datestart =05*3600+40*60+00+(datenum('18-May-2019')-datenum('01-Jan-1970'))*24*3600; %debut raw
% dateend =05*3600+45*60+00+(datenum('18-May-2019')-datenum('01-Jan-1970'))*24*3600;
% 
% [heure_hac_tot,list_sounder,sv_mat,al_mat,ath_mat,ir,TS,TSrange,TSech,TScomp,TSucomp,TSalong,TSathwart,TSposition,TSpositionGPS,TSlabel,TSfreq,heading,vit_knt,pitchRad,rollRad,heave,lat,long]=read_TS_hac(chemin_ini,[chemin_ini,'CW\MAYOBS_056_20190518_051624.hac'],[chemin_ini,'Config\TSAnlaysis'],datestart,dateend,index_sondeur,index_trans,index_beam,0);

% datestart =14*3600+00*60+00+(datenum('11-May-2019')-datenum('01-Jan-1970'))*24*3600; %debut raw
% dateend =14*3600+14*60+00+(datenum('11-May-2019')-datenum('01-Jan-1970'))*24*3600;
% 
% [heure_hac_tot,list_sounder,sv_mat,al_mat,ath_mat,ir,TS,TSrange,TSech,TScomp,TSucomp,TSalong,TSathwart,TSposition,TSpositionGPS,TSlabel,TSfreq,heading,vit_knt,pitchRad,rollRad,heave,lat,long]=read_TS_hac(chemin_ini,[chemin_ini,'CW\MAYOBS_056_20190511_130744.hac'],[chemin_ini,'Config\TSAnlaysis'],datestart,dateend,index_sondeur,index_trans,index_beam,0);
% 
% datestart =14*3600+55*60+00+(datenum('11-May-2019')-datenum('01-Jan-1970'))*24*3600; %debut raw
% dateend =15*3600+00*60+00+(datenum('11-May-2019')-datenum('01-Jan-1970'))*24*3600;
% 
% [heure_hac_tot,list_sounder,sv_mat,al_mat,ath_mat,ir,TS,TSrange,TSech,TScomp,TSucomp,TSalong,TSathwart,TSposition,TSpositionGPS,TSlabel,TSfreq,heading,vit_knt,pitchRad,rollRad,heave,lat,long]=read_TS_hac(chemin_ini,[chemin_ini,'CW\MAYOBS_056_20190511_143014.hac'],[chemin_ini,'Config\TSAnlaysis'],datestart,dateend,index_sondeur,index_trans,index_beam,0);
% 
% datestart =18*3600+49*60+00+(datenum('11-May-2019')-datenum('01-Jan-1970'))*24*3600; %debut raw
% dateend =18*3600+57*60+00+(datenum('11-May-2019')-datenum('01-Jan-1970'))*24*3600;
% 
% [heure_hac_tot,list_sounder,sv_mat,al_mat,ath_mat,ir,TS,TSrange,TSech,TScomp,TSucomp,TSalong,TSathwart,TSposition,TSpositionGPS,TSlabel,TSfreq,heading,vit_knt,pitchRad,rollRad,heave,lat,long]=read_TS_hac(chemin_ini,[chemin_ini,'CW\MAYOBS_056_20190511_172556.hac'],[chemin_ini,'Config\TSAnlaysis'],datestart,dateend,index_sondeur,index_trans,index_beam,0);





% datestart =16*3600+38*60+00+(datenum('14-Jun-2019')-datenum('01-Jan-1970'))*24*3600; %debut raw
% dateend =16*3600+46*60+00+(datenum('14-Jun-2019')-datenum('01-Jan-1970'))*24*3600;
% 
% [heure_hac_tot,list_sounder,sv_mat,al_mat,ath_mat,ir,TS,TSrange,TSech,TScomp,TSucomp,TSalong,TSathwart,TSposition,TSpositionGPS,TSlabel,TSfreq,heading,vit_knt,pitchRad,rollRad,heave,lat,long]=read_TS_hac(chemin_ini,[chemin_ini,'FM\MAYOBS_055_20190614_163508.hac'],[chemin_ini,'Config\TSAnlaysis'],datestart,dateend,index_sondeur,index_trans,index_beam,0);


% datestart =17*3600+21*60+00+(datenum('14-Jun-2019')-datenum('01-Jan-1970'))*24*3600; %debut raw
% dateend =17*3600+29*60+00+(datenum('14-Jun-2019')-datenum('01-Jan-1970'))*24*3600;
% 
% [heure_hac_tot,list_sounder,sv_mat,al_mat,ath_mat,ir,TS,TSrange,TSech,TScomp,TSucomp,TSalong,TSathwart,TSposition,TSpositionGPS,TSlabel,TSfreq,heading,vit_knt,pitchRad,rollRad,heave,lat,long]=read_TS_hac(chemin_ini,[chemin_ini,'FM\MAYOBS_055_20190614_163508.hac'],[chemin_ini,'Config\TSAnlaysis'],datestart,dateend,index_sondeur,index_trans,index_beam,0);

% datestart =01*3600+08*60+00+(datenum('13-Jun-2019')-datenum('01-Jan-1970'))*24*3600; %debut raw
% dateend =01*3600+44*60+00+(datenum('13-Jun-2019')-datenum('01-Jan-1970'))*24*3600;
% 
% [heure_hac_tot,list_sounder,sv_mat,al_mat,ath_mat,ir,TS,TSrange,TSech,TScomp,TSucomp,TSalong,TSathwart,TSposition,TSpositionGPS,TSlabel,TSfreq,heading,vit_knt,pitchRad,rollRad,heave,lat,long]=read_TS_hac(chemin_ini,[chemin_ini,'MAYOBS_055_20190613_004451.hac'],datestart,dateend,index_sondeur,index_trans,index_beam,0);

chemin_results='L:\MAYOBS\Results_M3D';

%positionin
Zmax=0;
X=[];
Y=[];
Z=[];
TSmedian=[];
Latitude=[];Longitude=[];
Depth=[];
TimeEcho=[];
for ip=1:length(TS)
    np=length(TSlabel{ip});
    if np>0      
        %calcul des x y z des d�tections courantes
        for idet=1:np
            X{ip}(idet)=TSposition{ip}(1,idet);
            Y{ip}(idet)=TSposition{ip}(2,idet);
            Z{ip}(idet)=TSposition{ip}(3,idet);         
            if (Z{ip}(idet)>Zmax)
                Zmax=Z{ip}(idet);
            end
             TSmedian{ip}(idet)=median(TScomp{ip}(:,idet));
             Latitude{ip}(idet)=TSpositionGPS{ip}(2,idet);
             Longitude{ip}(idet)=TSpositionGPS{ip}(1,idet);
             Depth{ip}(idet)=TSpositionGPS{ip}(3,idet);
             TimeEcho{ip}(idet)=(heure_hac_tot(ip)/(24*3600)+datenum('01-Jan-1970'));
        end
    end
end
        

Xarray=cell2mat(X);
Yarray=cell2mat(Y);
Zarray=cell2mat(Z);
LatitudeArray=cell2mat(Latitude);
LongitudeArray=cell2mat(Longitude);
DepthArray=cell2mat(Depth);
TSarray=cell2mat(TSmedian);
TimeArray=cell2mat(TimeEcho);

%export vers GLOBE;
fileNameprefix=[chemin_ini 'TSpanache_' datestr(datestart/(24*3600)+datenum('01-Jan-1970'),'dd-mmm-yyyy_HH_MM') '_' datestr(dateend/(24*3600)+datenum('01-Jan-1970'),'dd-mmm-yyyy_HH_MM') '_70kHz'];
fileNameTS = [fileNameprefix '.csv'];
A=[TimeArray' LatitudeArray' LongitudeArray' DepthArray' TSarray'];
dlmwrite(fileNameTS,A,'delimiter', ',', 'precision', 16);

%affichage
figure;imagesc([1:size(sv_mat,1)],list_sounder.m_transducer(index_trans).m_timeSampleInterval*1e-6*list_sounder.m_soundVelocity/2*[1:size(sv_mat,2)],sv_mat',[-90 -35]) %echogramme Sv
set(gca,'YDir','reverse');
colormap jet
caxis([-90 -35]);
title('Calibrated volume Target Strength');
colorbar;
axis([1 size(sv_mat,1) 0 Zmax])
saveas(gcf,[fileNameprefix '_SvEchogram'],'fig')
saveas(gcf,[fileNameprefix '_SvEchogram'],'png')


figure;imagesc([1:size(sv_mat,1)],list_sounder.m_transducer(index_trans).m_timeSampleInterval*1e-6*list_sounder.m_soundVelocity/2*[1:size(sv_mat,2)],sv_mat',[-90 -35]) %echogramme Sv
colormap gray;
freezeColors;
hold on;
for ip=1:length(TS)-2
    for j=1:length(TSlabel{ip})
        %d�tection Simrad track�es
%         if (TSlabel{ip}(j)>0)
             scatter(ip,TSrange{ip}(j),10,median(TScomp{ip}(:,j)));
%         end
    end
end
set(gca,'YDir','reverse');
colormap jet
caxis([-40 0]);
title('Beam compensated Target Strength');
colorbar;
axis([1 size(sv_mat,1) 0 Zmax])
saveas(gcf,[fileNameprefix '_TSEchogram'],'fig')
saveas(gcf,[fileNameprefix '_TSEchogram'],'png')

TSdepthnat=[];
DEPTH=[400:150:Zmax];
for i=1:length(DEPTH)-1
    ind=find(Zarray>=DEPTH(i) & Zarray<DEPTH(i+1));
    TSdepthnat(i)=10*log10(mean(10.^(TSarray(ind)/10)));
end
   

figure;
plot(TSarray,-Zarray,'*')
hold on
plot(TSdepthnat,-(DEPTH(1:end-1)+DEPTH(2:end))/2,'linewidth',5)
grid on
title('Beam compensated Target Strength');
xlabel('TS (dB)');
ylabel('Depth (m)');
saveas(gcf,[fileNameprefix 'meanTS'],'fig')
saveas(gcf,[fileNameprefix 'meanTS'],'png')

r = -linspace(0,Zmax) ;
th = linspace(0,2*pi) ;
[R,T] = meshgrid(r,th) ;
Xbeam = mean(Xarray)+R.*cos(T)*tand(10) ;
Ybeam = mean(Yarray)+R.*sin(T)*tand(10) ;
Zbeam = R ;

figure;
surf(Xbeam,Ybeam,Zbeam,'EdgeColor','none')
alpha 0.2
colormap gray;
freezeColors;
hold on;
scatter3(Xarray,Yarray,-Zarray,10,TSarray)
colormap jet
caxis([-30 0])
grid on
axis equal
colorbar;
title('Beam compensated Target Strength');
saveas(gcf,[fileNameprefix 'TS3D'],'fig')
saveas(gcf,[fileNameprefix 'TS3D'],'png')

figure;
scatter3(LatitudeArray,LongitudeArray,-DepthArray,10,TSarray)
colormap jet
caxis([-30 0])
grid on
colorbar;
title('Beam compensated Target Strength');
saveas(gcf,[fileNameprefix 'TS3D_GPS'],'fig')
saveas(gcf,[fileNameprefix 'TS3D_GPS'],'png')
