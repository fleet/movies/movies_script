function [heure_hac_tot,list_sounder,sv_mat,al_mat,ath_mat,ir,TS,TSrange,TSech,TScomp,TSucomp,TSalong,TSathwart,TSposition,TSpositionGPS,TSlabel,TSfreq,heading,vit_knt,pitchRad,rollRad,heave,lat,long]=read_TS_hac(FileDir,FileName,DirConfig,datestart,dateend,index_sondeur,index_trans,index_beam,hac_er)

%hac_er : 0 si hac hermes, 1 si hac eroc er60 (mic-mac d'angle)
% r�gler le nombre de chunk � lire L60

if (0) %definition config � la main
%noyau lecture
ParameterKernel= moKernelParameter();
ParameterDef=moLoadKernelParameter(ParameterKernel);

ParameterDef.m_bAutoLengthEnable=1;
ParameterDef.m_MaxRange = 10;
ParameterDef.m_bAutoDepthEnable=1;
ParameterDef.m_bIgnorePhaseData=0;

moSaveKernelParameter(ParameterKernel,ParameterDef);
clear ParameterDef;

ParameterR= moReaderParameter();
ParameterDef=moLoadReaderParameter(ParameterR);

ParameterDef.m_chunkNumberOfPingFan=100;

moSaveReaderParameter(ParameterR,ParameterDef);

else
    chemin_config_reverse = strrep(DirConfig, '\', '/');
    moLoadConfig(chemin_config_reverse);
end

%lecture hac
%%%%%%%%%%%%%%%
if datestart>-1
    
    ParameterR= moReaderParameter();
    ParameterDef=moLoadReaderParameter(ParameterR);
    taille_chunk=ParameterDef.m_chunkNumberOfPingFan;
    ParameterDef.m_chunkNumberOfPingFan=2;
    moSaveReaderParameter(ParameterR,ParameterDef);
    %lit premier chunk
    if length(FileName)==0
        moOpenHac(FileDir);
    else
          moOpenHac([FileDir FileName]);
    end
    
    moGoTo(datestart);
    moReadChunk(); %lit un ping suppl�mentaire
    
    %vide la m�moire des pings pr�c�dents
    nb_pings=moGetNumberOfPingFan();
    for ip=nb_pings-1:-1:1
        MX= moGetPingFan(ip-1);
        moRemovePing(MX.m_pingId);
    end
    
    ParameterDef.m_chunkNumberOfPingFan=taille_chunk;
    moSaveReaderParameter(ParameterR,ParameterDef);
else
    moOpenHac(FileName);
end

FileStatus= moGetFileStatus;
ilec=1;
while FileStatus.m_StreamClosed < 1
% while FileStatus.m_StreamClosed < 1 & ilec<=9
    moReadChunk();
    FileStatus= moGetFileStatus();
    ilec=ilec+1;
end;
fprintf('End of File');

%presentation des sondeurs
%%%%%%%%%%%%%%%%%%%%%%%%%%
list_sounder=moGetSounderList;
nb_snd=size(list_sounder,2);
disp(' ');
disp(['nb sondeurs = ' num2str(nb_snd)]);
for isdr = 1:nb_snd
    nb_transduc=list_sounder(isdr).m_numberOfTransducer;
    if (list_sounder(isdr).m_SounderId<10)
        ind_list(isdr)=list_sounder(isdr).m_SounderId;
        disp(['sondeur ' num2str(isdr) ':    ' 'index: ' num2str(list_sounder(isdr).m_SounderId) '   nb trans:' num2str(nb_transduc)]);
        for itr=1:nb_transduc
            for ibeam=1:list_sounder(isdr).m_transducer(itr).m_numberOfSoftChannel
                disp(['   trans ' num2str(itr) ':    ' 'nom: ' list_sounder(isdr).m_transducer(itr).m_transName '   freq: ' num2str(list_sounder(isdr).m_transducer(itr).m_SoftChannel(ibeam).m_acousticFrequency/1000) ' kHz']);
            end
        end
    else
        disp('Pb sondeur Id');
    end
end

%index_sondeur=1;
%index_trans=2;


detecsimrad=cell(moGetNumberOfPingFan(),list_sounder(index_sondeur).m_numberOfTransducer,list_sounder(index_sondeur).m_transducer.m_numberOfSoftChannel);
detecsimraduncomp=cell(moGetNumberOfPingFan(),list_sounder(index_sondeur).m_numberOfTransducer,list_sounder(index_sondeur).m_transducer.m_numberOfSoftChannel);
posabsTS=cell(moGetNumberOfPingFan(),list_sounder(index_sondeur).m_numberOfTransducer,list_sounder(index_sondeur).m_transducer.m_numberOfSoftChannel);
anglesimradAthwart=cell(moGetNumberOfPingFan(),list_sounder(index_sondeur).m_numberOfTransducer,list_sounder(index_sondeur).m_transducer.m_numberOfSoftChannel);
anglesimradAlong=cell(moGetNumberOfPingFan(),list_sounder(index_sondeur).m_numberOfTransducer,list_sounder(index_sondeur).m_transducer.m_numberOfSoftChannel);

delta=list_sounder(index_sondeur).m_transducer(index_trans).m_timeSampleInterval*list_sounder(index_sondeur).m_soundVelocity/2/10^6;
psi=list_sounder(index_sondeur).m_transducer(index_trans).m_SoftChannel.m_beamEquTwoWayAngle;
sa_cor=list_sounder(index_sondeur).m_transducer(index_trans).m_SoftChannel.m_beamSACorrection;
sv2ts=10*log10(list_sounder(index_sondeur).m_soundVelocity*list_sounder(index_sondeur).m_transducer(index_trans).m_pulseDuration/2*1e-6)+psi+2*sa_cor;
ouv_at=list_sounder(index_sondeur).m_transducer(index_trans).m_SoftChannel.m_beam3dBWidthAthwartRad;
ouv_al=list_sounder(index_sondeur).m_transducer(index_trans).m_SoftChannel.m_beam3dBWidthAlongRad;


iping=0;
        
for index= 0:moGetNumberOfPingFan()-1  %boucle sur les pings
           
    MX= moGetPingFan(index);
    SounderDesc =  moGetFanSounder(MX.m_pingId);
               
    if SounderDesc.m_SounderId == list_sounder(index_sondeur).m_SounderId

        if MX.m_meanTimeCPU+MX.m_meanTimeFraction/10000>dateend
            return;
        end
        iping=iping+1
        
        
        for transducerIndex=index_trans:index_trans
            heure_hac_tot(iping)=MX.beam_data(transducerIndex).m_timeCPU+MX.beam_data(transducerIndex).m_timeFraction/10000;
            heading(iping)=MX.beam_data(transducerIndex).navigationAttribute.m_headingRad*180/pi;
            pitchRad(iping)=MX.beam_data(transducerIndex).navigationAttitude.pitchRad;
            rollRad(iping)=MX.beam_data(transducerIndex).navigationAttitude.rollRad;
            heave(iping)=MX.beam_data(transducerIndex).navigationAttitude.sensorHeaveMeter;
            vit_knt(iping)=MX.beam_data(transducerIndex).navigationAttribute.m_speedMeter*3600/1852;
            long(iping)=MX.beam_data(transducerIndex).navigationPosition.m_longitudeDeg;
            lat(iping)=MX.beam_data(transducerIndex).navigationPosition.m_lattitudeDeg;
            for beamIndex=index_beam:index_beam
                datalist = moGetPolarMatrix(MX.m_pingId,transducerIndex-1);
                
                tsu_mat(iping,1:size(datalist.m_Amplitude,1))= datalist.m_Amplitude(:,beamIndex)/100+ 20*log10(max(delta,(-2:length(datalist.m_Amplitude(:,beamIndex))-3))*delta).'+sv2ts;
                sv_mat(iping,1:size(datalist.m_Amplitude,1))=datalist.m_Amplitude(:,beamIndex)/100;
                
                %lecture angles
                if (hac_er==0)
                    al_mat(iping,1:size(datalist.m_Amplitude,1))=datalist.m_AlongAngle(:,beamIndex);
                    ath_mat(iping,1:size(datalist.m_Amplitude,1))=datalist.m_AthwartAngle(:,beamIndex);
                else
                    %attention, tr�s sp�cial pour EROC (inversion
                    %along/athwart, et application int�gr�e du 180/128 (� compenser) pour convertir �
                    %partir du angle sensitivity)
                    al_mat(iping,1:size(datalist.m_Amplitude,1))=datalist.m_AthwartAngle(:,beamIndex)*128/180;
                    ath_mat(iping,1:size(datalist.m_Amplitude,1))=datalist.m_AlongAngle(:,beamIndex)*128/180;
                end
                %                 x=(2*al_mat(iping,:).'/ouv_al);
                %                 y=(2*ath_mat(iping,:).'/ouv_at);
                %                 ts_mat(iping,1:size(datalist.m_Amplitude,1))=  tsu_mat(iping,1:size(datalist.m_Amplitude,1))+ 6.0206*(x.^2+y.^2-0.18*x.^2.*y.^2).';
                %
                ir(iping)=nan;
                if (MX.beam_data(index_trans).m_bottomWasFound)
                    ir(iping)=MX.beam_data(index_trans).m_bottomRange/delta;
                end
                
                if (SounderDesc.m_transducer(index_trans).m_pulseShape==2)
                    targets=moGetSingleTargetFanFM(MX.m_pingId);
                else
                    targets=moGetSingleTargetFan(MX.m_pingId);
                end
                    
                TS{iping}=targets;
                TSrange{iping}=[];
                TSech{iping}=[];
                TScomp{iping}=[];
                TSucomp{iping}=[];
                TSalong{iping}=[];
                TSathwart{iping}=[];
                TSposition{iping}=[];
                TSpositionGPS{iping}=[];
                TSlabel{iping}=[];
                TSfreq{iping}=[];
                for i=1:size(targets.m_splitBeamData,2)
                    if targets.m_splitBeamData(i).m_parentSTId==list_sounder(index_sondeur).m_transducer(index_trans).m_SoftChannel(index_beam).m_softChannelId
                        nbcibles=size(targets.m_splitBeamData(i).m_targetList,2);
                        for p=1:nbcibles
                            TSrange{iping}=[TSrange{iping} targets.m_splitBeamData(i).m_targetList(p).m_targetRange];
                            TSech{iping}=[TSech{iping} round(targets.m_splitBeamData(i).m_targetList(p).m_targetRange/delta)];
                            TScomp{iping}=[TScomp{iping} targets.m_splitBeamData(i).m_targetList(p).m_compensatedTS];
                            TSucomp{iping}=[TSucomp{iping} targets.m_splitBeamData(i).m_targetList(p).m_unCompensatedTS];
                            TSalong{iping}=[TSalong{iping} targets.m_splitBeamData(i).m_targetList(p).m_AlongShipAngleRad];
                            TSathwart{iping}=[TSathwart{iping} targets.m_splitBeamData(i).m_targetList(p).m_AthwartShipAngleRad];
                            TSposition{iping}=[TSposition{iping} targets.m_splitBeamData(i).m_targetList(p).m_positionWorldCoord];
                            TSpositionGPS{iping}=[TSpositionGPS{iping} targets.m_splitBeamData(i).m_targetList(p).m_positionGeoCoord];
                            TSlabel{iping}=[TSlabel{iping} targets.m_splitBeamData(i).m_targetList(p).m_trackLabel];
                            if (SounderDesc.m_transducer(index_trans).m_pulseShape==2)
                                TSfreq{iping}=[TSfreq{iping} targets.m_splitBeamData(i).m_targetList(p).m_freq];
                            else
                                TSfreq{iping}=[TSfreq{iping} SounderDesc.m_transducer(index_trans).m_SoftChannel.m_acousticFrequency];
                            end
                        end
                    end
                end
            end
        end
    end
end

