function [tracks,targets_hac]=track_det(targets_hac,list_sounder,index_sondeur,index_trans,heure_hac_tot,heading,pitchRad,rollRad,x_ship,y_ship,heave,SeuilSpeed_knt,ntrous)

SeuilSpeed=SeuilSpeed_knt*1852/3600;

% on avance jusqu'au premier ping avec des d�tections
ip=1;
np=length(targets_hac{ip}.targetRange);
while(np==0)
    targets_hac{ip}.label=[];
    targets_hac{ip}.x=[];
    targets_hac{ip}.y=[];
    targets_hac{ip}.z=[];
    ip=ip+1;
    np=length(targets_hac{ip}.targetRange);
end

% on initie les tracks avec les d�tections du premier ping qui en pr�sente
targets_hac{ip}.label=(1:np);
clear lx ly lz
for idet=1:np
    [PosRel,PosAbsTrans]= ComputeWorldPositionER60_3(list_sounder(index_sondeur).m_transducer(index_trans),heading(ip)*pi/180,pitchRad(ip),rollRad(ip),heave(ip), targets_hac{ip}.targetRange(idet), targets_hac{ip}.AlongShipAngleRad(idet), targets_hac{ip}.AthwartShipAngleRad(idet));
    x=PosRel(1)+PosAbsTrans(1)+x_ship(ip);
    y=PosRel(2)+PosAbsTrans(2)+y_ship(ip);
    z=PosRel(3)+PosAbsTrans(3);
    tracks{idet}.ping=1;
    tracks{idet}.ts=targets_hac{ip}.compensatedTS(idet);
    tracks{idet}.tsu=targets_hac{ip}.unCompensatedTS(idet);
    tracks{idet}.range=targets_hac{ip}.targetRange(idet);
    tracks{idet}.alongRad=targets_hac{ip}.AlongShipAngleRad(idet);
    tracks{idet}.athwartRad=targets_hac{ip}.AthwartShipAngleRad(idet);
    tracks{idet}.x=x;
    tracks{idet}.y=y;
    tracks{idet}.z=z;
    tracks_end(idet)=ip; %dernier ping d'une track
    lx(idet)=x;
    ly(idet)=y;
    lz(idet)=z;
end
targets_hac{ip}.x=lx;
targets_hac{ip}.y=ly;
targets_hac{ip}.z=lz;
ip0=ip;
    
%tracking
for ip=ip0+1:length(targets_hac)
    np=length(targets_hac{ip}.targetRange);
    if np>0
        targets_hac{ip}.label(1:np)=0;
        
        %calcul des x y z des d�tections courantes
        clear x y z
        for idet=1:np
            [PosRel,PosAbsTrans]= ComputeWorldPositionER60_3(list_sounder(index_sondeur).m_transducer(index_trans),heading(ip)*pi/180,pitchRad(ip),rollRad(ip),heave(ip), targets_hac{ip}.targetRange(idet), targets_hac{ip}.AlongShipAngleRad(idet), targets_hac{ip}.AthwartShipAngleRad(idet));
            x(idet)=PosRel(1)+PosAbsTrans(1)+x_ship(ip);
            y(idet)=PosRel(2)+PosAbsTrans(2)+y_ship(ip);
            z(idet)=PosRel(3)+PosAbsTrans(3);
        end
        targets_hac{ip}.x=x;
        targets_hac{ip}.y=y;
        targets_hac{ip}.z=z;
        
        for itrou=0:ntrous %on examine la prolongation des tracks des pings pr�c�dents, � rebours (priorit� au ping r�cent)
            list_tracks=find(tracks_end==ip-1-itrou); % tracks envisag�es
            ntrack=length(list_tracks);
            ind_det=find(targets_hac{ip}.label==0); %d�tections non affect�es
            ndetlib=length(ind_det);
            clear dmin itm d
            if ntrack>0 & ndetlib>0
                
                %distance des tracks aux d�tections du ping
                for it=1:ntrack
                    d(it,:)=sqrt((x(ind_det)-tracks{list_tracks(it)}.x(end)).^2+(y(ind_det)-tracks{list_tracks(it)}.y(end)).^2+(z(ind_det)-tracks{list_tracks(it)}.z(end)).^2);
                end
                
                %association par crit�re de proximit�
                dSeuil=SeuilSpeed*(heure_hac_tot(ip)-heure_hac_tot(ip-1-itrou));
                [dmin,idetmin]=min(d,[],2); %d�tection la plus proche de chaque track
                [dbest,itrackbest]=min(dmin,[],1); %couple track-det le plus proche
                while dbest<=dSeuil
                    label=list_tracks(itrackbest);
                    idetbest=idetmin(itrackbest);
                    targets_hac{ip}.label(ind_det(idetbest))=label;
                    % on �limine la d�tection affect�e 
                    d(:,idetbest)=Inf;
                    % on �limine la track des choix 
                    d(itrackbest,:)=Inf;
                    [dmin,idetmin]=min(d,[],2); %d�tection la plus proche de chaque track
                    [dbest,itrackbest]=min(dmin,[],1); %couple track-det le plus proche
                end
               
               
            end
        end
            
        % on renseigne les tracks et en initie pour les d�tections non affect�es
        for idet=1:np
            label=targets_hac{ip}.label(idet);
            if label>0
                tracks{label}.ping=[tracks{label}.ping ip];
                tracks{label}.ts=[tracks{label}.ts targets_hac{ip}.compensatedTS(idet)];
                tracks{label}.tsu=[tracks{label}.tsu targets_hac{ip}.unCompensatedTS(idet)];
                tracks{label}.range=[tracks{label}.range targets_hac{ip}.targetRange(idet)];
                tracks{label}.alongRad=[tracks{label}.alongRad targets_hac{ip}.AlongShipAngleRad(idet)];
                tracks{label}.athwartRad=[tracks{label}.athwartRad targets_hac{ip}.AthwartShipAngleRad(idet)];
                tracks{label}.x=[tracks{label}.x x(idet)];
                tracks{label}.y=[tracks{label}.y y(idet)];
                tracks{label}.z=[tracks{label}.z z(idet)];
                tracks_end(label)=ip;
            else %nouvelle track
                label=size(tracks,2)+1;
                targets_hac{ip}.label(idet)=label;
                tracks{label}.ping=ip;
                tracks{label}.ts=targets_hac{ip}.compensatedTS(idet);
                tracks{label}.tsu=targets_hac{ip}.unCompensatedTS(idet);
                tracks{label}.range=targets_hac{ip}.targetRange(idet);
                tracks{label}.alongRad=targets_hac{ip}.AlongShipAngleRad(idet);
                tracks{label}.athwartRad=targets_hac{ip}.AthwartShipAngleRad(idet);
                tracks{label}.x=x(idet);
                tracks{label}.y=y(idet);
                tracks{label}.z=z(idet);
                tracks_end(label)=ip;
            end
        end
            
    else
        targets_hac{ip}.label=[];
        targets_hac{ip}.x=[];
        targets_hac{ip}.y=[];
        targets_hac{ip}.z=[];
    end
end


