function [tracks,targets_hac,x_eroc]=track_eroc(targets_hac,vit_knt,heure_hac_tot,SeuilSpeed_knt,ntrous)
clear tracks tracks_end

%SeuilSpeed_knt=4;
%ntrous=3;

SeuilSpeed=SeuilSpeed_knt*1852/3600;

ip=1;
x_eroc=0;
np=length(targets_hac{ip}.targetRange);
while(np==0)
    targets_hac{ip}.label=[];
    ip=ip+1;
    x_eroc(ip)=x_eroc(ip-1)+vit_knt(ip)*1852/3600*(heure_hac_tot(ip)-heure_hac_tot(ip-1));
    np=length(targets_hac{ip}.targetRange);
end


targets_hac{ip}.label=(1:np);
for idet=1:np
    tracks{idet}.ping=1;
    tracks{idet}.ts=targets_hac{ip}.compensatedTS(idet);
    tracks{idet}.tsu=targets_hac{ip}.unCompensatedTS(idet);
    tracks{idet}.range=targets_hac{ip}.targetRange(idet);
    tracks{idet}.alongRad=targets_hac{ip}.AlongShipAngleRad(idet);
    tracks{idet}.athwartRad=targets_hac{ip}.AthwartShipAngleRad(idet);
    tracks{idet}.x=tracks{idet}.range*sin(tracks{idet}.alongRad)+x_eroc(ip);
    tracks{idet}.y=tracks{idet}.range*sin(tracks{idet}.athwartRad);
    tracks{idet}.z=tracks{idet}.range*cos(tracks{idet}.alongRad)*cos(tracks{idet}.athwartRad);
    tracks_end(idet)=ip; %dernier ping d'une track
end
    

for ip=2:length(targets_hac)
    x_eroc(ip)=x_eroc(ip-1)+vit_knt(ip)*1852/3600*(heure_hac_tot(ip)-heure_hac_tot(ip-1));
    np=length(targets_hac{ip}.targetRange);
    if np>0
        targets_hac{ip}.label(1:np)=0;
        
        %calcul des x y z des d�tections courantes
        x=targets_hac{ip}.targetRange.*sin(targets_hac{ip}.AlongShipAngleRad)+x_eroc(ip);
        y=targets_hac{ip}.targetRange.*sin(targets_hac{ip}.AthwartShipAngleRad);
        z=targets_hac{ip}.targetRange.*cos(targets_hac{ip}.AlongShipAngleRad).*cos(targets_hac{ip}.AthwartShipAngleRad);
        
        ntrack=length(targets_hac{ip-1}.targetRange); %nb de tracks au ping pr�c�dent
        clear dmin itm d
        detp_label=zeros(1,np); %label des tracks associ�es aux d�tections courantes
        if ntrack>0
            for it=1:ntrack
                %distance de la track it aux d�tections du ping
                d(it,:)=sqrt((x-tracks{targets_hac{ip-1}.label(it)}.x(end)).^2+(y-tracks{targets_hac{ip-1}.label(it)}.y(end)).^2+(z-tracks{targets_hac{ip-1}.label(it)}.z(end)).^2);
                [dmin(it),itm(it)]=min(d(it,:));
            end

            % on trie par distance croissante
            [dmin_sort,indtrs]=sort(dmin,'ascend');
            dSeuil=SeuilSpeed*(heure_hac_tot(ip)-heure_hac_tot(ip-1));
            for it=1:ntrack
                if dmin(indtrs(it))<=dSeuil 
                    label=targets_hac{ip-1}.label(indtrs(it));
                    targets_hac{ip}.label(itm(indtrs(it)))=label;
                    tracks_end(label)=ip;
                    detp_label(itm(indtrs(it)))=label;
                    if it<ntrack
                        % on recalcule les distances restantes
                        d(:,itm(indtrs(it)))=Inf;
                        [dmin(indtrs(it+1:end)),itm(indtrs(it+1:end))]=min(d(indtrs(it+1:end),:),[],2);
                        [dmin_sort(indtrs(it+1:end)),inds_indtrs]=sort(dmin(indtrs(it+1:end)),'ascend');
                        indtrs(it+1:end)=indtrs(it+inds_indtrs);
                    end
                end
            end
        end
        
        % s'il reste des d�tections non affect�es, on envisage les tracks des
        % ntrous pings pr�c�dents
        ind_det=find(detp_label==0); %d�tections non affect�es
        ndetlib=length(ind_det);
        if ndetlib>0
            list_tracks=find(tracks_end<ip-1 & tracks_end>=ip-1-ntrous);
            %on recommence
            ntrack=length(list_tracks); %nb de tracks
            clear dmin itm d
            if ntrack>0
                for it=1:ntrack
                    %distance de la track it aux d�tections du ping
                    d(it,:)=sqrt((x(ind_det)-tracks{list_tracks(it)}.x(end)).^2+(y(ind_det)-tracks{list_tracks(it)}.y(end)).^2+(z(ind_det)-tracks{list_tracks(it)}.z(end)).^2);
                    [dmin(it),itm(it)]=min(d(it,:));
                end

                % on trie par distance croissante
                [dmin_sort,indtrs]=sort(dmin,'ascend');
                for it=1:ntrack
                    dSeuil2=SeuilSpeed*(heure_hac_tot(ip)-heure_hac_tot(tracks_end(list_tracks(indtrs(it)))));
                    if dmin(indtrs(it))<=dSeuil2
                        label=list_tracks(indtrs(it));
                        targets_hac{ip}.label(ind_det(itm(indtrs(it))))=label;
                        tracks_end(label)=ip;
                        if it<ntrack
                            % on recalcule les distances restantes
                            d(:,itm(indtrs(it)))=Inf;
                            [dmin(indtrs(it+1:end)),itm(indtrs(it+1:end))]=min(d(indtrs(it+1:end),:),[],2);
                            [dmin_sort(indtrs(it+1:end)),inds_indtrs]=sort(dmin(indtrs(it+1:end)),'ascend');
                            indtrs(it+1:end)=indtrs(it+inds_indtrs);
                        end
                    end
                end
            end
        end
        
            
        % on renseigne les tracks et en initie pour les d�tections non affect�es
        for idet=1:np
            label=targets_hac{ip}.label(idet);
            if label>0
                tracks{label}.ping=[tracks{label}.ping ip];
                tracks{label}.ts=[tracks{label}.ts targets_hac{ip}.compensatedTS(idet)];
                tracks{label}.tsu=[tracks{label}.tsu targets_hac{ip}.unCompensatedTS(idet)];
                tracks{label}.range=[tracks{label}.range targets_hac{ip}.targetRange(idet)];
                tracks{label}.alongRad=[tracks{label}.alongRad targets_hac{ip}.AlongShipAngleRad(idet)];
                tracks{label}.athwartRad=[tracks{label}.athwartRad targets_hac{ip}.AthwartShipAngleRad(idet)];
                tracks{label}.x=[tracks{label}.x tracks{label}.range(end)*sin(tracks{label}.alongRad(end))+x_eroc(ip)];
                tracks{label}.y=[tracks{label}.y tracks{label}.range(end)*sin(tracks{label}.athwartRad(end))];
                tracks{label}.z=[tracks{label}.z tracks{label}.range(end)*cos(tracks{label}.alongRad(end))*cos(tracks{label}.athwartRad(end))];
            else %nouvelle track
                label=size(tracks,2)+1;
                targets_hac{ip}.label(idet)=label;
                tracks{label}.ping=[ip];
                tracks{label}.ts=[targets_hac{ip}.compensatedTS(idet)];
                tracks{label}.tsu=[targets_hac{ip}.unCompensatedTS(idet)];
                tracks{label}.range=[targets_hac{ip}.targetRange(idet)];
                tracks{label}.alongRad=[targets_hac{ip}.AlongShipAngleRad(idet)];
                tracks{label}.athwartRad=[targets_hac{ip}.AthwartShipAngleRad(idet)];
                tracks{label}.x=[tracks{label}.range(end)*sin(tracks{label}.alongRad(end))+x_eroc(ip)];
                tracks{label}.y=[tracks{label}.range(end)*sin(tracks{label}.athwartRad(end))];
                tracks{label}.z=[tracks{label}.range(end)*cos(tracks{label}.alongRad(end))*cos(tracks{label}.athwartRad(end))];
                tracks_end(label)=ip;
            end
        end
            
    else
        targets_hac{ip}.label=[];
    end
end


