m = rand(10,2); %Generate data

x = m(:,1); %split into x and y
y = m(:,2);

topEdge = 1; % define limits
botEdge = 0; % define limits
numBins = 2; % define number of bins

binEdges = linspace(botEdge, topEdge, numBins+1);
[h,whichBin] = histc(x, binEdges);

for i = 1:numBins
    flagBinMembers = (whichBin == i);
    binMembers     = y(flagBinMembers);
    binMean(i)     = mean(binMembers);
end


