% SampleHacChangeSound Velocity is a script that modify the sound velocity
% used by M3D to compute sample intervall

clear all;

chemin_config='S:\PELGAS11\SMFH\config-movies3D\base\autolength\'
chemin_config_reverse = strrep(chemin_config, '\', '/');

moLoadConfig(chemin_config_reverse);

chemin_ini='R:\PELGAS11\HacBrutPG11\avec_problemes\RUN003\'
%chemin_ini='Y:\HacBrutPG11_Hermes_ER60\RUN003\'

save_path='R:\PELGAS11\HacBrutPG11\RUN003\'

filelist = ls([chemin_ini,'*.hac']);  % ensemble des fichiers
nb_files = size(filelist,1);  % nombre de fichiers

for l=1:nb_files

    FileName=filelist(l,:)

    moOpenHac(strcat(chemin_ini,FileName));
    
    FileStatus= moGetFileStatus;
    while FileStatus.m_StreamClosed < 1
        moReadChunk();
        FileStatus= moGetFileStatus();
    end;
 
    SounderList=moGetSounderList();
    for k= SounderList(1,:)
        moSetSounderSoundVelocity(k.m_SounderId,1511);
    end
   
    % now we ask to write what's in memory
    moStartWrite(save_path, 'ER60_OUTPUT2_');
    % stop Writting
    moStopWrite();
end    
    

