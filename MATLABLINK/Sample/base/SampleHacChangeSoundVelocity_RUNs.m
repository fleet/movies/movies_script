% SampleHacChangeSound Velocity_RUNs: modifies the sound speed in .hac
% files located in 'RUN' folders. Sound speed is used by M3D to
% compute sample interval.
% Mean sound speed per run is computed based on the hull thermo-salinometer measurements 
% stored in casino file. 
%--------------------------------------------------------------
% Author: Mathieu Doray (mathieu.doray@ifremer.fr)
% Based on orginal code by Laurent Berger, 16/05/2011
% Created: 18/05/11
% Last update: 18/05/11

clear all;

%% Configuration and data in/out paths
chemin_config='S:\PELGAS11\SMFH\config-movies3D\base\autolength\'
chemin_config_reverse = strrep(chemin_config, '\', '/');

moLoadConfig(chemin_config_reverse);

%% Filter and compute mean celerity per run

path_cas='M:\PELGAS11\Casino\pelgas11.csv'

%Open complete Casino .csv export and extract data
[stime,sdates,dates,hours,datetime,lat,lon,vitf,vits,roul,tang,prof,venta,dira,cap,dirc,ventv,dirv,pilon,temperature,celerity,salinity,Chla]=import_ALL_casino (path_cas);
%filter out erroneous (small) values
cel_min=quantile(celerity,.06)
cel_sum=quantile(celerity,[.025 .25 .50 .75 .975])
max(celerity)
min(celerity)
sel=celerity>cel_min;

celeritys=celerity(sel,:);
sdates2=sdates(sel,:);
ndates=sort(cellstr(unique(sdates2,'rows')));
stimes=stime(sel,:);
dates2=dates(sel,:);
hours2=hours(sel,:);

sndspeed_RUN.day=ndates
sndspeed_RUN.run=0:size(sndspeed_RUN.day,1)
sndspeed_RUN.mean=grpstats(celeritys,dates2);
sndspeed_RUN.sd=grpstats(celeritys,dates2,'sem');

%% Display daily sound speed profiles
% Convert dates from 'string' format into cell array to allow for strcmp
% comparison

cdates=cellstr(sdates2);
class(cdates)

%All runs
%--------------
for nr=1:size(sndspeed_RUN.day,1) 
    figure;
    plot(stimes(strcmp(char(sndspeed_RUN.day(nr,:)),cdates),:),celeritys(strcmp(char(sndspeed_RUN.day(nr,:)),cdates),:));
    datetick('x',15)
    title(['Sound speed, run',num2str(sndspeed_RUN.run(:,nr))]);
end    

%1st leg
%--------------
for nr=2:14
    figure;
    plot(stimes(strcmp(char(sndspeed_RUN.day(nr,:)),cdates),:),celeritys(strcmp(char(sndspeed_RUN.day(nr,:)),cdates),:));
    datetick('x',15)
    title(['Sound speed, run',num2str(sndspeed_RUN.run(:,nr)),', ',char(sndspeed_RUN.day(nr,:))]);
end  

%% Change sound speed in hac files

runs=[1];

for (nr=runs)
    for ir=1:length(runs)
      str_run(ir,:)='000';
      str_run(ir,end-length(num2str(runs(ir)))+1:end)=num2str(runs(ir));
    end
    chemin_ini=['Y:\HacBrutPG11_Hermes\RUN',str_run(nr,:),'\']
    save_path=['Y:\HacBrutPG11_Hermes_celcor\RUN',str_run(nr,:),'\']
    if ~exist(save_path,'dir')
          mkdir(save_path)
    end
       
    filelist = ls([chemin_ini,'*.hac']);  % ensemble des fichiers
    nb_files = size(filelist,1);  % nombre de fichiers
    
    for l=1:nb_files
    
    %Mean sound speed for current .hac file
      FileName=filelist(l,:)
      FileName2=filelist(l+1,:)
      fhour1=[FileName(:,23:24),':',FileName(:,25:26),':',FileName(:,27:28)];  
      fhour2=[FileName2(:,23:24),':',FileName2(:,25:26),':',FileName2(:,27:28)];
      hsel=(datenum(hours2,'HH:MM:SS')>=datenum(fhour1,'HH:MM:SS'))&(datenum(hours2,'HH:MM:SS')<datenum(fhour2,'HH:MM:SS'));
      mean_sndspeed_hac=mean(celeritys(hsel,:))
    %Mean sound speed during the whole run
      mean_sndspeed_run=sndspeed_RUN.mean(sndspeed_RUN.run==nr,:);
      
      moOpenHac(strcat(chemin_ini,FileName));
        
        FileStatus= moGetFileStatus;
        while FileStatus.m_StreamClosed < 1
            moReadChunk();
        FileStatus= moGetFileStatus();
        end;
 
    %Change sound speed to surface sound speed recorded during current .hac file
    
    SounderList=moGetSounderList();
    for k= SounderList(1,:)
        moSetSounderSoundVelocity(k.m_SounderId,mean_sndspeed_hac);
    end
   
    % now we ask to write what's in memory
    moStartWrite(save_path, 'celcor_');
    % stop Writting
    moStopWrite();
    end
end
    

