% SampleHacWrite is a sample demoonstrating how to use write algorithm
%
% Use this simple implementation to create your own .m processes
%

clear all;
FileName='Y:/HacBrutPG11_Hermes/RUN002/PELGAS11_002_20110427_000428.hac';
save.path='Y:\HacBrutPG11_Hermes_ER60\'

moOpenHac(FileName);
clear FileName;

SounderList=moGetSounderList();
% printf sounder Id data
for k= SounderList(1,:)
    if k.m_isMultiBeam==1||k.m_SounderId==3
        fprintf('remove sounder %d\n',k.m_SounderId);
        moDeleteSounder(k.m_SounderId);
    end
end


% now we ask to write what's in memory
moStartWrite(save.path, 'ER60_OUTPUT2_');

% all pingFan already in memory are now written 
%new ones read are also written. These are written after all matlab
%algorithm are called
FileStatus= moGetFileStatus;
while FileStatus.m_StreamClosed < 1
    moReadChunk();
    FileStatus= moGetFileStatus();
end;

% stop Write 
moStopWrite();

files = dir(save.path)
filename=char(files.name)

moOpenHac(strcat(save.path,filename(3,:)));
SounderList=moGetSounderList();
SounderList(1,:)
SounderList.m_SounderId



