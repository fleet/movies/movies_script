% SampleHacWriteOnlyER60 removes data from specified sounder(s) from .hac
% files
%--------------------------------------------------------------------
clear all;

for nrun=3:3


%% Read config and data

%Read config (Edit/kernel parameter/nb frame='auto length' in M3D)
chemin_config='F:\MoviesTestDataset\ConfigsM3D\remME70\'
chemin_config_reverse = strrep(chemin_config, '\', '/');
moLoadConfig(chemin_config_reverse);

%Data in/out path
chemin_ini=['F:\MoviesTestDataset\RUN00',num2str(nrun),'\']
save_path=['F:\MoviesTestDataset\ER60only\RUN00',num2str(nrun),'\']
  if ~exist(save_path,'dir')
       mkdir(save_path)
  end

%File list
filelist = ls([chemin_ini,'*.hac']);  % ensemble des fichiers
nb_files = size(filelist,1);  % nombre de fichiers

%Eventually, file selection
%Ni=strmatch('ER60_OUTPUT2_PELGAS11_002_20110427_054620',filelist)
% Ni=strmatch('PELGAS11_003_20110428_115816.hac',filelist)
% Ni=strmatch('PELGAS11_003_20110428_155204.hac',filelist)
% Ni=strmatch('PELGAS11_002_20110427_044017.hac',filelist)
% Ni=strmatch('PELGAS11_002_20110427_181713.hac',filelist)
Ni=1

for l=Ni:nb_files

    FileName=filelist(l,:)

    moOpenHac(strcat(chemin_ini,FileName));
        
    FileStatus= moGetFileStatus;
    while FileStatus.m_StreamClosed < 1
        moReadChunk();
        FileStatus= moGetFileStatus();
    end;
    
    SounderList=moGetSounderList();
    % printf sounder Id data
    for k= SounderList(1,:)
        if k.m_isMultiBeam==1||k.m_SounderId==3
            fprintf('remove sounder %d\n',k.m_SounderId);
            moDeleteSounder(k.m_SounderId);
        end
    end

    % now we ask to write what's in memory
    moStartWrite(save_path, 'ER60_ONLY_');
    % stop Writting
    moStopWrite();
end    
    
end