% SampleHacWrite is a sample demoonstrating how to use write algorithm
%
% Use this simple implementation to create your own .m processes
%
clear all;

chemin_config='F:\Projects\PELGAS\2011\SMFH\config-movies3D\remME70\'
chemin_config_reverse = strrep(chemin_config, '\', '/');

moLoadConfig(chemin_config_reverse);

%chemin_ini='F:/Correction_fond/RUN002/hermes/'
%save_path='F:/Correction_fond/RUN002/hermes/'
%chemin_ini='R:\PELGAS11\HacBrutPG11\RUN002\'
%
chemin_ini='I:\Pelgas14\Hac-Hermes\ANEshl\'
save_path='Y:/HacBrutPG11_Hermes_ER60/RUN002/'
  if ~exist(save_path,'dir')
       mkdir(save_path)
  end

filelist = ls([chemin_ini,'*.hac']);  % ensemble des fichiers
nb_files = size(filelist,1);  % nombre de fichiers

%Ni=strmatch('ER60_OUTPUT2_PELGAS11_002_20110427_054620',filelist)
% Ni=strmatch('PELGAS11_003_20110428_115816.hac',filelist)
% Ni=strmatch('PELGAS11_003_20110428_155204.hac',filelist)
% Ni=strmatch('PELGAS11_002_20110427_044017.hac',filelist)
% Ni=strmatch('PELGAS11_002_20110427_181713.hac',filelist)
Ni=1

for l=Ni:nb_files

    filename=filelist(l,:)

    moOpenHac(strcat(chemin_ini,filename));
    
    %recup_params_sondeur; marche p� why??
    
    FileStatus= moGetFileStatus;
    while FileStatus.m_StreamClosed < 1
        moReadChunk();
        FileStatus= moGetFileStatus();
    end;
    
    while moReadChunk();
        FileStatus= moGetFileStatus();
    end;
    
    SounderList=moGetSounderList();
    % printf sounder Id data
    for k= SounderList(1,:)
        if k.m_isMultiBeam==1||k.m_SounderId==3
            fprintf('remove sounder %d\n',k.m_SounderId);
            moDeleteSounder(k.m_SounderId);
        end
    end

    % now we ask to write what's in memory
    moStartWrite(save_path, 'ER60_OUTPUT2_');
    % stop Writting
    moStopWrite();
end    
    
