% SampleHac_FewPingsOnly is a script demonstrating Movies3D .hac files read/write 
% capabilities
%
% The objective is to extract only a given number of pings from a (large)
% hac file and write them into a "short" new file
% The number of ping to extract is defined as the "nb frame" in the M3D
% configuration (Edit/kernel parameter/nb frame in M3D) 
%
clear all;

%% Set data and configuration paths

%Load configuration for reading .hac file: the number of pings to read is
%defined by the "nb frame" parameter in M3D 'edit/kernel parameters' menu. 
%----------------------
chemin_config='S:\PELGAS11\SMFH\config-movies3D\base\ShortenIt_25\'
chemin_config_reverse = strrep(chemin_config, '\', '/');

moLoadConfig(chemin_config_reverse);

%Data path
%----------------------
%input
%chemin_ini='Y:/HacBrutPG11_Hermes/RUN002/'
chemin_ini='R:\PELGAS11\HacCorPG11\RUN002\'
%output
save_path='Y:/HacBrutPG11_Hermes_samples/RUN002/'
  %create output path if none
  if ~exist(save_path,'dir')
       mkdir(save_path)
  end

%file list 
filelist = ls([chemin_ini,'*.hac']);  % ensemble des fichiers
nb_files = size(filelist,1);  % nombre de fichiers

%Eventually, 1 file selection
Ni=strmatch('ER60_OUTPUT2_ER60_OUTPUT2_PELGAS11_002_20110427_050017',filelist)
%Ni=strmatch('ER60_OUTPUT2_PELGAS11_002_20110427_054620',filelist)
% Ni=strmatch('PELGAS11_003_20110428_115816.hac',filelist)
% Ni=strmatch('PELGAS11_003_20110428_155204.hac',filelist)
% Ni=strmatch('PELGAS11_002_20110427_044017.hac',filelist)
% Ni=strmatch('PELGAS11_002_20110427_181713.hac',filelist)

%Eitherway, process all files
%Ni=1

%% Read file
 
%Parameters of interest
    num_ping_fichier.total = 1;
    num_ping_fichier.mono = 0;
    num_ping_fichier.multi = 0;
    num_ping_fichier.horiz = 0;
    index=0;

%Sounder definition
    SounderId.mono=1;
    SounderId.multi=2;
    SounderId.horiz=3;
%Open file    
    filename=filelist(Ni,:)
    moOpenHac(strcat(chemin_ini,filename));
%Get file status (openned/end of file)   
    FileStatus= moGetFileStatus;
    
%Read current ping    
    while FileStatus.m_StreamClosed < 1
        %read "chunk"
        moReadChunk();
        
%         %get ping data
%         PingFanData = moGetPingFan(index);
%         
%         is_mono = (PingFanData.m_sounderId == SounderId.mono);
%         is_multi = (PingFanData.m_sounderId == SounderId.multi);
%         is_horiz = (PingFanData.m_sounderId == SounderId.horiz);
%         
%         index=index+1
%         
%         %display ping no. for each sounder
%         if is_mono
%             fprintf('Ping mono n� %g\n',num_ping_fichier.mono);
%             num_ping_fichier.mono=num_ping_fichier.mono+1;
%         elseif is_multi
%             fprintf('Ping SMFH n� %g\n',num_ping_fichier.multi);
%             num_ping_fichier.multi=num_ping_fichier.multi+1;
%         else
%             fprintf('Ping SMFH n� %g\n',num_ping_fichier.horiz);
%             num_ping_fichier.horiz=num_ping_fichier.horiz+1;
%         end
      
        FileStatus= moGetFileStatus();  
    end;   

%% Write pings in memory (i.e. up to the "nb frame" in the M3D kernel parameter menu) into new file     
    
    % now we ask to write what's in memory
    moStartWrite(save_path,'FirstPings_');
    % stop Writting
    moStopWrite();

    
