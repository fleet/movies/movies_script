function PosAbs= ComputeWorldPositionME70_2(Delay, transFaceAlongAngleOffsetRad,transFaceAthwarAngleOffsetRad, PingFan, Transducer, SoftChannel , Range, AlongTSAngle, AthwartTSAngle)
	
m_MatrixNav=CreateRotationMatrix(PingFan.navigationAttribute.m_headingRad,PingFan.navigationAttitude.pitchRad,PingFan.navigationAttitude.rollRad );
m_MatrixHeadingNav=CreateRotationMatrix(PingFan.navigationAttribute.m_headingRad,0,0 );
m_TranslationNav=([0,0,PingFan.navigationAttitude.sensorHeaveMeter] )';

m_transducerTranslation=[Transducer.m_pPlatform.along_ship_offset;Transducer.m_pPlatform.athwart_ship_offset;Transducer.m_pPlatform.depth_offset  ];

m_transducerRotation=CreateRotationMatrix(Transducer.m_transRotationAngleRad,transFaceAlongAngleOffsetRad,transFaceAthwarAngleOffsetRad);


temp=(Range+Delay);
temp= temp * (cos(AlongTSAngle)*cos(AthwartTSAngle));

CoordSoftChannel=[temp*tan(AlongTSAngle);temp*tan(AthwartTSAngle);temp];

SoftRotation=CreateRotationMatrix(0,SoftChannel.m_mainBeamAlongSteeringAngleRad,-SoftChannel.m_mainBeamAthwartSteeringAngleRad);


PosRel= m_MatrixNav*m_transducerTranslation+m_TranslationNav+m_MatrixHeadingNav*m_transducerRotation*SoftRotation*CoordSoftChannel;

%PosGeo = [PingFan.navigationPosition.m_lattitudeDeg;PingFan.navigationPosition.m_longitudeDeg;0];
%PosAbs(1,1) = PosGeo(1)+PosRel(1)/60.0/1852.0;
%PosAbs(2,1) = PosGeo(2)+PosRel(2)/60.0/1852.0/cos(PosGeo(1)*3.14159/180.0);
%PosAbs(3,1) = PosRel(3);
%PosGeo = [PingFan.navigationPosition.m_lattitudeDeg*60*1852;PingFan.navigationPosition.m_longitudeDeg*60*1852*cosd(PingFan.navigationPosition.m_lattitudeDeg);0];


%PosAbs=PosRel+PosGeo;
PosAbs=PosRel;