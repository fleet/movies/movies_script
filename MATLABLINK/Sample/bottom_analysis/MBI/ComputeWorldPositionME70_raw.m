function PosAbs= ComputeWorldPositionME70_raw(latitudeDeg,longitudeDeg,headingRad,pitchRad,rollRad, sensorHeaveMeter, mainBeamAlongSteeringAngleRad,mainBeamAthwartSteeringAngleRad,Range, AlongTSAngle, AthwartTSAngle)

Delay=0;

transFaceAlongAngleOffsetRad=0.0026;
transFaceAthwarAngleOffsetRad=0.003;

m_MatrixNav=CreateRotationMatrix(headingRad,pitchRad,rollRad);
m_MatrixHeadingNav=CreateRotationMatrix(headingRad,0,0 );
%m_TranslationNav=([0,0,sensorHeaveMeter] )';
m_TranslationNav=([0,0,sensorHeaveMeter+26.73*tan(pitchRad)] )';

along_ship_offset=26.6300;
athwart_ship_offset=1.0500;
depth_offset=7.0700;
   
along_ship_offset=26.7300;
athwart_ship_offset=0.00;
depth_offset=7.0700;
   
m_transducerTranslation=[along_ship_offset;athwart_ship_offset;depth_offset  ];

m_transRotationAngleRad=0;
m_transducerRotation=CreateRotationMatrix(m_transRotationAngleRad,transFaceAlongAngleOffsetRad,transFaceAthwarAngleOffsetRad);


temp=(Range+Delay);
temp= temp * (cos(AlongTSAngle)*cos(AthwartTSAngle));

CoordSoftChannel=[temp*tan(AlongTSAngle);temp*tan(AthwartTSAngle);temp];

SoftRotation=CreateRotationMatrix(0,mainBeamAlongSteeringAngleRad,-mainBeamAthwartSteeringAngleRad);


PosRel= m_MatrixNav*m_transducerTranslation+m_TranslationNav+m_MatrixHeadingNav*m_transducerRotation*SoftRotation*CoordSoftChannel;
% PosGeo = [PingFan.navigationPosition.m_lattitudeDeg;PingFan.navigationPosition.m_longitudeDeg;0];
% PosAbs(1) = PosGeo(1)+PosRel(1)/60.0/1852.0;
% PosAbs(2) = PosGeo(2)+PosRel(2)/60.0/1852.0/cos(PosGeo(1)*3.14159/180.0);
% PosAbs(3) = PosRel(3);
%PosGeo = [latitudeDeg*60*1852;longitudeDeg*60*1852*cosd(latitudeDeg);0];


%PosAbs=PosRel+PosGeo;
PosAbs=PosRel;