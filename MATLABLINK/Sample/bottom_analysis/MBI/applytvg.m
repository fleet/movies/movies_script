function outsampledata = applytvg(config,insampledata)

% Applies TVG to raw sample power data
%
% Output structure 
%
% outsampledata.svrange
% outsampledata.sv [dB]
% outsampledata.sprange
% outsampledata.sp [dB]
%
% Simrad, Lars Nonboe Andersen, 9/5-05

channel = insampledata.channel;

configtransducer = config.transducer(channel);
pulselengthtable = configtransducer.pulselengthtable;
gaintable = configtransducer.gaintable;
sacorrectiontable = configtransducer.sacorrectiontable;
psi = configtransducer.equivalentbeamangle;

soundvelocity = insampledata.soundvelocity;
absorptioncoefficient = insampledata.absorptioncoefficient;
sampleinterval = insampledata.sampleinterval;
transmitpower = insampledata.transmitpower;
frequency = insampledata.frequency;
pulselength = insampledata.pulselength;
count = insampledata.count;
power = insampledata.power;

pulselengthindex = find(pulselengthtable==pulselength);
if ~isempty(pulselengthindex)
    gain = gaintable(pulselengthindex);
    sacorrection = sacorrectiontable(pulselengthindex);
else
    gain = configtransducer.gain;
    sacorrection = 0;
end

samplespace = soundvelocity.*sampleinterval/2;

lambda = soundvelocity/frequency;

% Sv values
svcenterrange = soundvelocity.*pulselength/4;
svrange = (0:count-1)'*samplespace - svcenterrange;
svtvgrange = svrange;
svtvgrange(find(svtvgrange<samplespace)) = samplespace;

tvg20 = 20*log10(svtvgrange) + 2*absorptioncoefficient*svtvgrange;
idxtvg20less0 = find(tvg20<0);
tvg20(idxtvg20less0) = 0;

svconst = 10*log10(transmitpower*lambda^2*soundvelocity*pulselength/(32*pi^2)) + 2*(gain+sacorrection) + psi;
sv = power + tvg20 - svconst;

% Sp values
sprange = (0:count-1)'*samplespace;
sptvgrange = sprange;
sptvgrange(find(sptvgrange<samplespace)) = samplespace;

tvg40 = 40*log10(sptvgrange) + 2*absorptioncoefficient*sptvgrange;
idxtvg40less0 = find(tvg40<0);
tvg40(idxtvg40less0) = 0;

spconst = 10*log10(transmitpower*lambda^2/(16*pi^2)) + 2*gain;
sp = power + tvg40 - spconst;

outsampledata.svrange = svrange;
outsampledata.sv = sv;
outsampledata.sprange = sprange;
outsampledata.sp = sp;