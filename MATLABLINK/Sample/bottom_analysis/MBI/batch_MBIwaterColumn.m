chemin_ini='C:\Users\mdoray\Documents\Data\MoviesTestDataSet\RUN003\sel\';
chemin_save='';

filelist = ls([chemin_ini,'*.hac']);  % ensemble des fichiers
nb_files = size(filelist,1);  % nombre de fichiers

latlong=0;

load Carto; %carto sonarscope relue en xml et enregistr�e en .mat

for numfile = 1:nb_files  % boucle sur l'ensemble des fichiers � traiter

    hacfilename = filelist(numfile,:);
    FileName = [chemin_ini,hacfilename]
    [detec3,lat,long,heading,vit,heure_hac,heure_hac_frac]=test_MBI2(FileName);
    save(['MBI2_',hacfilename(1:end-4)],'detec3','lat','long','heading','vit','heure_hac','heure_hac_frac');

%load (['MBI2_',hacfilename(1:end-4)]);

    % mise en liste des d�tections, et positionnement en lat/long
    npings=size(detec3,1);
    nbeams=size(detec3,2);

    detec2=[];
    step=100;
    for ip1=1:npings/step+1
        detec22=[];
        for ip2=1:step
            ip=ip2+(ip1-1)*step
            if (ip<=npings)
                for ib=1:nbeams
                    %detec22=[detec22 ; [detec3{ip,ib} hm*ones(size(detec3{ip,ib},1),1)]];
                    u=detec3{ip,ib};
                    [posabs(1) posabs(2)]=latlon2xy(Carto, lat(ip), long(ip));
                    if (latlong==1)
                        [latd, longd] = xy2latlon(Carto, posabs(1)+u(:,2), posabs(2)+u(:,1));
                        detec22=[detec22 ; [latd longd u(:,3:4)]];
                    else % on reste en xy
                        detec22=[detec22 ; [posabs(1)+u(:,2) posabs(2)+u(:,1) u(:,3:4)]];
                    end
                end
            end
        end
        detec2=[detec2; detec22];
    end
    ind2=find(detec2(:,3)>40 & detec2(:,3)<150 & detec2(:,4)>-40);  % s�lection des d�tections en profondeur et niveau

    %affichage nuage de d�tections
    figure(7);plot3(detec2(ind2,1),detec2(ind2,2),detec2(ind2,3),'*');grid on

    if (latlong==0) %affichage maillage xy
        [x2,y2,z2]=my_grid(detec2(ind2,1).',detec2(ind2,2).',detec2(ind2,3).');
        [x3,y3]=meshgrid(x2,y2);
        figure(5);surf(x3-mean(x2),y3-mean(y2),-z2.');
    end

    if (~strcmp(chemin_save,'')) %enregistrement pour lecture sonarscope
        clear d3
        %d3(:,1)=detec2(:,1)/(60*1852);
        %d3(:,2)=detec2(:,2)./(cosd(d3(:,1))*60*1852);
        d3(:,1)=detec2(ind2,1);
        d3(:,2)=detec2(ind2,2);
        d3(:,3)=-detec2(ind2,3);
        Filename=hacfilename(1:end-4);
        dlmwrite([chemin_save Filename '.xyz'],d3(:,1:3),'delimiter',' ','precision',17);
    end
end