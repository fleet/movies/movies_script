function [dir_trans,teta_trans,dir_long,teta_long]=directivite1(teta_trans_rel,teta_long_rel,psi,phi,tree,ibeam,typ_bathy,typ2,typ_dir,typ_norm,nom_conf)

%%%%%%%%%%%%%%%%
%% type norme %%       
%%%%%%%%%%%%%%%%
% 
% typ_norm='max';   %normalise a la valeur maximum
% typ_norm='axe';  %normalise a la valeur de l'axe
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %% type directivite elementaire %
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% typ_dir='cos';
% %typ_dir='no_cos';
% typ_dir='sim_th';  % terme crois� long/trans dans la directivit� capteur,
% attention la directivite delivree n'en tient donc pas compte
% %typ_dir='sim_calc';  % terme crois� long/trans dans la directivit� capteur
% attention la directivite delivree n'en tient donc pas compte


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% dimensions               %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Trans_capteur_elem = 6.7/1000;
Long_capteur_elem = 14.2 /1000;

delta_Long = 15*0.001;
delta_Trans = 7.5*0.001;

         %Trans_capteur_elem = delta_Trans ;
         %Long_capteur_elem = delta_Long ;
         
c=tree.CommonParameters.SoundVelocity;
%c=1490;
lambda=c/tree.Beams.Beam(ibeam).ParameterData.Frequency;
k=2*pi/lambda;

teta_trans=psi+teta_trans_rel; % transversal en degr�s
%teta_trans=psi+(-70:dteta:70); % transversal en degr�s
teta_long=phi+teta_long_rel; % longitudinal en degr�s

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  directivit� �l�mentaire capteur  %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if (~strcmp(typ_dir,'sim_calc'))
    if (strcmp(typ_dir,'cos'))
        h_capt_trans=sin(k*Trans_capteur_elem./2*sind(teta_trans)).*cosd(teta_trans)./(k*Trans_capteur_elem./2*sind(teta_trans));
        h_capt_long=sin(k*Long_capteur_elem./2*sind(teta_long)).*cosd(teta_long)./(k*Long_capteur_elem./2*sind(teta_long));
    elseif(strcmp(typ_dir,'no_cos') | strcmp(typ_dir,'sim_th'))
        h_capt_trans=sin(k*Trans_capteur_elem./2*sind(teta_trans))./(k*Trans_capteur_elem./2*sind(teta_trans));
        h_capt_long=sin(k*Long_capteur_elem./2*sind(teta_long))./(k*Long_capteur_elem./2*sind(teta_long));
    end
    h_capt_trans(find(isnan(h_capt_trans)))=1;
    h_capt_long(find(isnan(h_capt_long)))=1;
else 
     Trans_capteur_elem = delta_Trans ;
     Long_capteur_elem = delta_Long ;

     delta_uy=sind(teta_trans)-sind(psi);
     
     qy=pi*Trans_capteur_elem/lambda;

     uy_bm=sind(psi);
     ux_bm=sind(phi);
     uz_bm=sqrt(1-ux_bm.^2-uy_bm.^2);

     if (abs(qy*uy_bm)>0.01)
        delta_B_el_trans=0.0339*256*delta_uy*(qy*(1./(tan(qy*uy_bm))-1./(qy*uy_bm))-uy_bm./(uz_bm.^2));
     else
        delta_B_el_trans=0.0339*256*delta_uy*(-uy_bm*(1/uz_bm.^2+(qy.^2)/3));
     end
     
     delta_ux=sind(teta_long)-sind(phi);

     qx=pi*Long_capteur_elem/lambda;

     if (abs(qx*ux_bm)>0.01)
        delta_B_el_long=0.0339*256*delta_ux*(qx*(1./(tan(qx*ux_bm))-1./(qx*ux_bm))-ux_bm./(uz_bm.^2));
     else
        delta_B_el_long=0.0339*256*delta_ux*(-ux_bm*(1/uz_bm.^2+(qx.^2)/3));
     end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% ponderation des voies     %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

nb_capt_trans=40;
pond_trans=ones(1,nb_capt_trans);
nb_capt_long=20;
pond_long=ones(1,nb_capt_long);

if (strcmp(typ2,'bathy'))
    pond_transTx=[0.00240764,0.0215298,0.0590394,0.113495,0.182803,0.264302,0.354858,0.450991,0.549009,0.645142,0.735698,0.817197,0.886505,0.940961,0.97847,0.997592,1,1,1,1,1,1,1,1,0.997592,0.97847,0.940961,0.886505,0.817197,0.735698,0.645142,0.549009,0.450991,0.354858,0.264302,0.182803,0.113495,0.0590394,0.0215298,0.00240764];
    pond_transRx=[0.560914,0.496778,0.421975,0.394205,0.427967,0.493815,0.55618,0.60452,0.649664,0.700551,0.752992,0.798896,0.837704,0.874409,0.910257,0.940856,0.963049,0.979017,0.9918,1,1,0.9918,0.979017,0.963049,0.940856,0.910257,0.874409,0.837704,0.798896,0.752992,0.700551,0.649664,0.60452,0.55618,0.493815,0.427967,0.394205,0.421975,0.496778,0.560914];
    pond_trans=pond_transRx;
%     if (strcmp(typ_bathy,'Tx'))
%         pond_trans=pond_transTx;
%     elseif (strcmp(typ_bathy,'Rx'))
%         pond_trans=pond_transRx;
%     end
    pond_long=[0.775142,0.743332,0.707633,0.703139,0.746584,0.825217,0.907497,0.965839,0.992975,1,1,0.992975,0.965839,0.907497,0.825217,0.746584,0.703139,0.707633,0.743332,0.775142];
elseif (strcmp(typ2,'halie'))
    if (strcmp(nom_conf,'21'))
        %init (21?)
        pond_trans=[0.111994,0.092027,0.126928,0.167993,0.21512,0.267984,0.326026,0.388457,0.454268,0.522255,0.591046,0.659147,0.724988,0.786976,0.843552,0.893248,0.934743,0.966911,0.988867,1,1,0.988867,0.966911,0.934743,0.893248,0.843552,0.786976,0.724988,0.659147,0.591046,0.522255,0.454268,0.388457,0.326026,0.267984,0.21512,0.167993,0.126928,0.092027,0.111994];
        pond_long=[0.112676,0.161491,0.258945,0.376379,0.507236,0.642138,0.769867,0.878735,0.95813,1,1,0.95813,0.878735,0.769867,0.642138,0.507236,0.376379,0.258945,0.161491,0.112676];
   elseif (strcmp(nom_conf,'15'))
        %IBTS09 15
        pond_trans=[0.000361,0.001257,0.002729,0.005336,0.009906,0.017648,0.030237,0.049844,0.079065,0.120686,0.177267,0.250554,0.340784,0.446025,0.561749,0.680814,0.793994,0.891063,0.962283,1,1,0.962283,0.891063,0.793994,0.680814,0.561749,0.446025,0.340784,0.250554,0.177267,0.120686,0.079065,0.049844,0.030237,0.017648,0.009906,0.005336,0.002729,0.001257,0.000361];
        pond_long=[0.000775,0.003902,0.013481,0.039576,0.099579,0.214839,0.397437,0.630426,0.857455,1,1,0.857455,0.630426,0.397437,0.214839,0.099579,0.039576,0.013481,0.003902,0.000775];
    elseif (strcmp(nom_conf,'noaa'))
        pond_trans=[0.275726,0.159943,0.202764,0.250225,0.301908,0.357256,0.415573,0.476042,0.537736,0.599644,0.660689,0.719764,0.775752,0.827562,0.874157,0.914587,0.948011,0.973723,0.991178,1,1,0.991178,0.973723,0.948011,0.914587,0.874157,0.827562,0.775752,0.719764,0.660689,0.599644,0.537736,0.476042,0.415573,0.357256,0.301908,0.250225,0.202764,0.159943,0.275726];
        pond_long=[0.203518,0.224049,0.327902,0.4451,0.569078,0.691737,0.804216,0.897814,0.964945,1,1,0.964945,0.897814,0.804216,0.691737,0.569078,0.4451,0.327902,0.224049,0.203518];
    end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% formation de voie           %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%ym=(-(nb_capt_trans-1)/2:(nb_capt_trans-1)/2)*Trans_capteur_elem;
ym=(-(nb_capt_trans-1)/2:(nb_capt_trans-1)/2)*delta_Trans;
sum_nb_c_trans=zeros(1,length(teta_trans));  % somme des capteurs dephases/ponderes 
for i_c=1:nb_capt_trans
    sum_nb_c_trans=sum_nb_c_trans+exp(sqrt(-1)*k*sin(teta_trans*pi/180)*ym(i_c))...
        *exp(-sqrt(-1)*k*sin(psi*pi/180)*ym(i_c))*pond_trans(i_c);
end
if (strcmp(typ_dir,'sim_calc'))
    pond_trans=pond_trans/sum(pond_trans);
    B_beam_trans=20*log10(cos(k*delta_uy.'*ym)*pond_trans.').';
end

ym=(-(nb_capt_long-1)/2:(nb_capt_long-1)/2)*delta_Long;
sum_nb_c_long=zeros(1,length(teta_long));
for i_c=1:nb_capt_long
    sum_nb_c_long=sum_nb_c_long+exp(sqrt(-1)*k*sin(teta_long*pi/180)*ym(i_c))...
        *exp(-sqrt(-1)*k*sin(phi*pi/180)*ym(i_c))*pond_long(i_c);
end
if (strcmp(typ_dir,'sim_calc'))
    pond_long=pond_long/sum(pond_long);
    B_beam_long=20*log10(cos(k*delta_ux.'*ym)*pond_long.').';
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% normalisation               %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if (~strcmp(typ_dir,'sim_calc') & ~strcmp(typ_dir,'sim_th'))
    if (strcmp(typ_norm,'axe'))        
        norm_pond_trans=h_capt_trans(ceil(length(teta_trans)/2))*sum(pond_trans);  % valeur dans l'axe
        norm_pond_long=h_capt_long(ceil(length(teta_long)/2))*sum(pond_long);
        h_trans=h_capt_trans/norm_pond_trans.*sum_nb_c_trans; %normalisation initiale a la valeur de pointage
        h_long=h_capt_long/norm_pond_long.*sum_nb_c_long; %normalisation initiale a la valeur de pointage
    elseif (strcmp(typ_norm,'max'))
        h_trans=h_capt_trans.*sum_nb_c_trans;
        h_trans=h_trans/max(abs(h_trans));%normalisation au max (pour garder le sens de l'angle equivalent)
        h_long=h_capt_long.*sum_nb_c_long;
        h_long=h_long/max(abs(h_long));%normalisation au max (pour garder le sens de l'angle equivalent)
    end
elseif strcmp(typ_dir,'sim_th') %traitement croise long/trans
    h_trans=h_capt_trans.*sum_nb_c_trans;
    h_long=h_capt_long.*sum_nb_c_long;
    if (strcmp(typ_norm,'axe'))
        norm_axe=h_trans(ceil(length(teta_trans)/2))*h_long(ceil(length(teta_long)/2))*sqrt(1-sind(psi)^2-sind(phi)^2);
        h_trans=h_trans/sqrt(norm_axe);
        h_long=h_long/sqrt(norm_axe);
    elseif (strcmp(typ_norm,'max'))
        croise=zeros(length(teta_trans),length(teta_long));
        for il=1:length(teta_long)
                croise(:,il)=sqrt(1-sind(teta_trans).^2-sind(teta_long(il))^2);
        end
        norm_max=max(max(abs((h_trans.'*h_long).*croise)));
        h_trans=h_trans/sqrt(norm_max);
        h_long=h_long/sqrt(norm_max);
    end
elseif strcmp(typ_dir,'sim_calc') 
    h_trans=10.^((B_beam_trans+delta_B_el_trans)/20);
    h_long=10.^((B_beam_long+delta_B_el_long)/20);
end

if (strcmp(typ2,'bathy'))   %directivit� long utilis�e seule aussi
    norm_long_only=h_long(ceil(length(teta_long)/2))
    h_long=h_long/norm_long_only;
    h_trans=h_trans*norm_long_only;
end 
%niv=max(h_capt_trans.*sum_nb_c);

dir_trans=h_trans;
dir_long=h_long;

% cas particulier de bathy ou on voudrait prendre le Tx en compte
if (strcmp(typ_bathy,'TR') & strcmp(typ2,'bathy'))
    if (strcmp(typ_norm,'max'))
        dir_trans=h_trans.*h_capt_trans/max(abs(h_trans.*h_capt_trans));
    elseif (strcmp(typ_norm,'axe'))
        dir_trans=h_trans.*h_capt_trans/abs(h_capt_trans((1+size(h_capt_trans,2))/2));
    end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% parametres                  %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if (strcmp(typ_dir,'sim_th'))
    dirt=dir_trans*dir_long(ceil(length(teta_long)/2)).*sqrt(1-sind(phi)^2-sind(teta_trans).^2);
    dirl=dir_long*dir_trans(ceil(length(teta_trans)/2)).*sqrt(1-sind(psi)^2-sind(teta_long).^2);
else
    dirt=dir_trans;
    dirl=dir_long;
end


% ouvertures � -3dB
i3dBmin=min(find(20*log10(abs(dirt))>-3))-1;
i3dBmax=max(find(20*log10(abs(dirt))>-3))+1;

ouv_3dB_trans=teta_trans(i3dBmax)-teta_trans(i3dBmin)
%tree.Beams.Beam(ibeam).ParameterData.BeamWidthAthwartship

i3dBmin=min(find(20*log10(abs(dirl))>-3))-1;
i3dBmax=max(find(20*log10(abs(dirl))>-3))+1;

ouv_3dB_long=teta_long(i3dBmax)-teta_long(i3dBmin)

im=find(abs(dir_trans)==max(abs(dir_trans)));
jm=find(abs(dir_long)==max(abs(dir_long)));

dep_natur_trans=teta_trans(im)-psi;
if (strcmp(typ_dir,'sim_th'))
    if (~strcmp(typ_norm,'max')) %sinon croise existe deja
        croise=zeros(length(teta_trans),length(teta_long));
        for il=1:length(teta_long)
                croise(:,il)=sqrt(1-sind(teta_trans).^2-sind(teta_long(il))^2);
        end
    end
    [im,jm]=find(abs((dir_trans.'*dir_long).*croise)==max(max(abs((dir_trans.'*dir_long).*croise))));
    dep_natur_trans=teta_trans(im)-psi;
end

% ouvertures � -3dB par rapport au max
if (strcmp(typ_dir,'sim_th'))
    dirt=abs((dir_trans(:)*dir_long(jm)).*croise(:,jm));
    dirl=abs((dir_trans(im)*dir_long(:).').*croise(im,:));
else
    dirt=dir_trans*dir_long(jm);
    dirl=dir_trans(im)*dir_long;
end

i3dBmin=min(find(20*log10(abs(dirt)/max(abs(dirt)))>-3))-1;
i3dBmax=max(find(20*log10(abs(dirt)/max(abs(dirt)))>-3))+1;

ouv_3dB_trans_max=teta_trans(i3dBmax)-teta_trans(i3dBmin)
%tree.Beams.Beam(ibeam).ParameterData.BeamWidthAthwartship

i3dBmin=min(find(20*log10(abs(dirl)/max(abs(dirl)))>-3))-1;
i3dBmax=max(find(20*log10(abs(dirl)/max(abs(dirl)))>-3))+1;

ouv_3dB_long_max=teta_long(i3dBmax)-teta_long(i3dBmin)

tree.Beams.Beam(ibeam).ParameterData.BeamWidthAthwartship
tree.Beams.Beam(ibeam).ParameterData.BeamWidthAlongship

% maximum
if (strcmp(typ_dir,'sim_th'))
    max_niv=20*log10(max(max(abs((dir_trans.'*dir_long).*croise))));
else
    max_niv=20*log10(abs(dir_trans(im)*dir_long(jm)));
end
