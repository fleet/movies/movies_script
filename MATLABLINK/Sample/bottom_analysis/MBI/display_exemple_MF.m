load MBI2_AOEM-D20101012-T133027_raw
load('Carto');
npings=size(detec3,1);
nbeams=size(detec3,2);


%create detec2, list of all detections, with for each of them: lat, long,depth, sv, ping index
detec2=[];
step=50;
for ip1=1:npings/step+1  %loop on pings
    detec22=[];
    for ip2=1:step
        ip=ip2+(ip1-1)*step
        if (ip<=npings)
            for ib=1:nbeams
                u=detec3{ip,ib}; 
                if (~isempty(u))
                    [posabs(1) posabs(2)]=latlon2xy(Carto, lat(ip), long(ip));
                    [latd, longd] = xy2latlon(Carto, posabs(1)+u(:,2), posabs(2)+u(:,1));
                    detec22=[detec22 ; [latd longd u(:,3:4) ip*ones(size(u,1),1)]];
                end
            end
        end
    end
    detec2=[detec2; detec22];
end


%DISPLAY


%choose pings to display
npings_min=1;
%npings_max=700;
npings_max=npings;
%choose depth limits
dmin=20;
dmax=250;
%chosse sv range
sv_max=-10;
sv_min=-60;

%select detections according to depth , sv values and ping index
ind2=find(detec2(:,3)>dmin & detec2(:,3)<dmax & detec2(:,4)<sv_max & detec2(:,4)>sv_min & detec2(:,5)>=npings_min & detec2(:,5)<=npings_max);

figure(10);plot3(detec2(ind2,1),detec2(ind2,2),-detec2(ind2,3),'*');grid on;
xlabel('latitude in decimal degrees');ylabel('longitude in decimal degrees');zlabel('depth')


%display with colors
col='rmygbc';
nst=length(col); %6
%seuil_st=[-10 -40 -55 -50 -52 -56 -58]% sv thresholds for color coding
seuil_st=[-10 -20 -30 -40 -45 -55 -58]% sv thresholds for color coding

figure(2);hold off
for i=nst:-1:1
ind2=find(detec2(:,3)>dmin & detec2(:,3)<dmax & detec2(:,4)>=seuil_st(i+1) & detec2(:,4)<seuil_st(i) & detec2(:,5)>=npings_min & detec2(:,5)<=npings_max);
figure(2);plot3(detec2(ind2,1),detec2(ind2,2),-detec2(ind2,3),[col(i) '*']);grid on;hold on
end
hold off;xlabel('latitude in decimal degrees');ylabel('longitude in decimal degrees');zlabel('depth')


%display along latitude
figure(3);hold off
for i=nst:-1:1
ind2=find(detec2(:,3)>dmin & detec2(:,3)<dmax & detec2(:,4)>=seuil_st(i+1) & detec2(:,4)<seuil_st(i) & detec2(:,5)>=npings_min & detec2(:,5)<=npings_max);
figure(3);plot(detec2(ind2,1),-detec2(ind2,3),[col(i) '*']);grid on;hold on
end
hold off;xlabel('latitude in decimal degrees');ylabel('depth')

%display along longitude
figure(4);hold off
for i=nst:-1:1
ind2=find(detec2(:,3)>dmin & detec2(:,3)<dmax & detec2(:,4)>=seuil_st(i+1) & detec2(:,4)<seuil_st(i) & detec2(:,5)>=npings_min & detec2(:,5)<=npings_max);
figure(4);plot(detec2(ind2,2),-detec2(ind2,3),[col(i) '*']);grid on;hold on
end
hold off;xlabel('longitude in decimal degrees');ylabel('depth')