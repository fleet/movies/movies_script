function [x2,y2,z2]=my_grid(x,y,z)

dx=5;
dy=5;
xmin=min(x);
xmax=max(x);
ymin=min(y);
ymax=max(y);

x2=(xmin:dx:xmax+dx);
y2=(ymin:dy:ymax+dy);

grid=cell(size(x2,2),size(y2,2));
for i=1:size(z,2)
    i2=1+round((x(i)-xmin)/dx);
    j2=1+round((y(i)-ymin)/dy);
    grid{i2,j2}=[grid{i2,j2} z(i)];
end

z2=nan(size(x2,2),size(y2,2));
for i=1:size(x2,2)
    for j=1:size(y2,2)
        z2(i,j)=median(grid{i,j});
    end
end