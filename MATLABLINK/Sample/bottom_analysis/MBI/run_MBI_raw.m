chemin_ini='L:\PELGAS10\ME70_raw\RUN003\';

fname = 'PELGAS10-D20100428-T184422.raw';
FileName = [chemin_ini,fname]

if (0) %premiere lecture
    [detec3,lat,long,heading,heure]=test_MBI2_raw(FileName);
    save(['MBI2_',fname(1:end-4),'_raw'],'detec3','lat','long','heading','heure');
else
    %load (['MBI2_',fname(1:end-4),'_raw']);
end


latlong=0;
ping2ping=0;

load Carto; %carto sonarscope relue en xml et enregistr�e en .mat

maree=0; %correction de maree
heures_etal=[0 00; 5 49; 12 13; 18 00 ]; %conquet 19 03 2010
h_mer=[1.33;6.64;1.39;6.51];

refl=1; %traitement de la r�flectivit�
nom_conf='21'; %nom de la configuration
typ_bathy='Rx';
typ_mode='halie';
%typ_mode='bathy';

%%%%%%%%%%reflectivit�
%%%%%%%%%%%%%%%%
%% type norme %%       
%%%%%%%%%%%%%%%%
% 
 typ_norm='max';   %normalise a la valeur maximum
% typ_norm='axe';  %normalise a la valeur de l'axe
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %% type directivite elementaire %
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
 typ_dir_elem='cos';
% %typ_dir_elem='no_cos';
% typ_dir_elem='sim_th';  % terme crois� long/trans dans la directivit� capteur,
% attention la directivite delivree n'en tient donc pas compte
% %typ_dir_elem='sim_calc';  % terme crois� long/trans dans la directivit� capteur
% attention la directivite delivree n'en tient donc pas compte

if (refl==1) %construction des directivit�s de chaque voie
    load(['tree_' nom_conf]);
    nbeams=tree.CommonParameters.BeamCount;
    T=tree.CommonParameters.PulseLength;
    c=tree.CommonParameters.SoundVelocity;

    
    for ib=1:nbeams
        a_trans(ib)=tree.Beams.Beam(ib).ParameterData.DirectionY;
        ouv_t_sim(ib)=tree.Beams.Beam(ib).ParameterData.BeamWidthAthwartship;
        ouv_l_sim(ib)=tree.Beams.Beam(ib).ParameterData.BeamWidthAlongship;
        psi(ib)=tree.Beams.Beam(ib).ParameterData.EquivalentBeamAngle;
        sa_corr(ib)=tree.Beams.Beam(ib).ParameterData.SaCorrection+tree.Beams.Beam(ib).Results.SACorrectionAdjustment;
    end
    Teff=T*10.^(sa_corr/5);
    
    dteta=0.05;
    teta_trans_rel=(-max(abs(ouv_t_sim)):dteta:max(abs(ouv_t_sim))); % transversal en degr�s
    teta_long_rel=(-max(abs(ouv_l_sim)):dteta:max(abs(ouv_l_sim))); % longitudinal en degr�s
    dir_transm=zeros(nbeams,length(teta_trans_rel));
    teta_transm=zeros(nbeams,length(teta_trans_rel));
    dir_longm=zeros(nbeams,length(teta_long_rel));
    teta_longm=zeros(nbeams,length(teta_long_rel));
    
    for ib=1:nbeams
        [dir_transm(ib,:),teta_transm(ib,:),dir_longm(ib,:),teta_longm(ib,:)]=directivite1(teta_trans_rel,teta_long_rel,a_trans(ib),0,tree,ib,typ_bathy,typ_mode,typ_dir_elem,typ_norm,nom_conf);
    end
        
end


% mise en liste des d�tections, et positionnement en lat/long
npings=size(detec3,1);
nbeams=size(detec3,2);

detec2=[];
step=50;
for ip1=1:npings/step+1
    detec22=[];
    for ip2=1:step
        ip=ip2+(ip1-1)*step
        if (ip<=npings)
            if maree==1
                [hh,mm,ss]=read_day_time2(heure_hac(ip)+heure_hac_frac(ip)/10000);
                hm=hmaree(hh,mm,heures_etal,h_mer);
            end
            for ib=1:nbeams
                %detec22=[detec22 ; [detec3{ip,ib} hm*ones(size(detec3{ip,ib},1),1)]];
                u=detec3{ip,ib};
                if (~isempty(u))
                [posabs(1) posabs(2)]=latlon2xy(Carto, lat(ip), long(ip));

                if (refl==1)
                    alr=u(:,6)*180/pi;
                    atr=u(:,7)*180/pi;
                    teta_trans=atr+a_trans(ib);
                    teta_long=alr+0;
                    dir_transl=dir_transm(ib,:);
                    dir_longl=dir_longm(ib,:);
                    [dir_trans]=interp1(teta_trans_rel,dir_transl,atr);
                    [dir_long]=interp1(teta_long_rel,dir_longl,alr);

                     %lobe naturel
                    if (strcmp(typ_mode,'bathy'))
                        if strcmp(typ_dir_elem(1:3),'sim')
                            lLobe0=20*log10(abs(dir_trans).*(abs(dir_long).^2).*sqrt(1-sind(teta_trans).^2-sind(teta_long).^2).*abs(cosd(teta_long)));
                            %lLobe0=20*log10(abs(dir_trans(i_t)).*(abs(dir_long(i_l)).^2).*abs(cosd(teta_trans(i_t)).*cosd(teta_long(i_l)).^2));
                        else
                            lLobe0=20*log10(abs(dir_trans).*(abs(dir_long).^2));
                        end
                    elseif(strcmp(typ_mode,'halie'))
                        if strcmp(typ_dir_elem(1:3),'sim')
                            lLobe0=40*log10(abs(dir_trans.*dir_long.*sqrt(1-sind(teta_trans).^2-sind(teta_long).^2)));
                        else
                            lLobe0=20*log10(abs(dir_trans.^2).*(abs(dir_long).^2));
                        end
                    end

                    area=ouv_l_sim(ib)*pi/180.*u(:,8).*min(c*Teff(ib)/2./abs(sind(teta_trans)),ouv_t_sim(ib)*pi/180.*u(:,8)./cosd(teta_trans));
                    cor_sv2bs=psi(ib)+20*log10(u(:,8))+10*log10(c*Teff(ib)/2)-10*log10(area)-lLobe0;

                    cor_sv2bs2=psi(ib)+10*log10(u(:,8))-10*log10(ouv_l_sim(ib)*pi/180)+10*log10(abs(sind(teta_trans)))-lLobe0;

                end

                if (latlong==1)
                    [latd, longd] = xy2latlon(Carto, posabs(1)+u(:,2), posabs(2)+u(:,1));
                    det_ad=[latd longd u(:,3:4)];
                elseif (ping2ping==1) % affichage ping � ping
                    det_ad=[sqrt(u(:,2).^2+u(:,1).^2).*sign(u(:,1)) ip+0*u(:,1) u(:,3:4)];
                else % on reste en xy
                    det_ad=[posabs(1)+u(:,2) posabs(2)+u(:,1) u(:,3:4)];
                end
                if maree==1
                    det_ad=[det_ad hm*ones(size(detec3{ip,ib},1),1)];
                elseif refl==1
                    det_ad=[det_ad cor_sv2bs];
                end
                detec22=[detec22 ; det_ad];
                end
            end
        end
    end
    detec2=[detec2; detec22];
end

%affichage nuage de d�tections
figure(7);plot3(detec2(:,1),detec2(:,2),-detec2(:,3),'*');grid on
%ind2=find(detec2(:,3)>20 & detec2(:,3)<300 & detec2(:,4)>-50);  % s�lection des d�tections en profondeur et niveau
ind2=find(detec2(:,3)>170 & detec2(:,3)<300 & detec2(:,4)>-50);  % s�lection des d�tections en profondeur et niveau
figure(8);plot3(detec2(ind2,1),detec2(ind2,2),-detec2(ind2,3),'*');grid on

if (latlong==0) %affichage maillage xy
    [x2,y2,z2]=my_grid(detec2(ind2,1).',detec2(ind2,2).',detec2(ind2,3).');
    [x3,y3]=meshgrid(x2,y2);
    figure(5);surf(x3-mean(x2),y3-mean(y2),-z2.');
end

if (0) %enregistrement pour lecture sonarscope
    clear d3
    %d3(:,1)=detec2(:,1)/(60*1852);
    %d3(:,2)=detec2(:,2)./(cosd(d3(:,1))*60*1852);
    d3(:,1)=detec2(:,1);
    d3(:,2)=detec2(:,2);
    ad_fic='';
    if (maree==1)
        d3(:,3)=-detec2(:,3)- detec2(:,5);
    else
        d3(:,3)=-detec2(:,3);
    end
    if (refl==1)
        d3(:,3)=10.^((detec2(:,4)+ detec2(:,5))/10);
        ad_fic='bs_';
    end
    Filename=fname(1:end-4);

    dlmwrite(['C:\SonarScope-R2014b-32Bits-2011-02-28\IfremerToolboxResources\raw_' ad_fic Filename '_' num2str(latlong) '.xyz'],d3(ind2,1:3),'delimiter',' ','precision',17);
end
