%function [detec,lat,long,heure_hac,heure_hac_frac,roll_hac,heave_hac,heave_hac2,at_hac,al_hac,res]=test_MBI2
function [detec,lat,long,heading,vit,heure_hac,heure_hac_frac]=test_MBI2(FileName)
%detec est un cell array npings x nbeams o� chaque cellule est une matrice
%ndetections x y sv


%implantation MBI directe avec lecture hac et fonctions movies

%nom et chemin fichier
if(0)
chemin_ini='K:\SMFH\EXACHA10\HAC HERMES\EXACHA10\RUN001\';
nom_fic='EXACHA10_001_20100319_171559.hac';
 FileName = [chemin_ini,nom_fic]
end

if (0) %definition config � la main
%noyau lecture
ParameterKernel= moKernelParameter();
ParameterDef=moLoadKernelParameter(ParameterKernel);

ParameterDef.m_bAutoLengthEnable=1;
ParameterDef.m_MaxRange = 150;
ParameterDef.m_bAutoDepthEnable=1;
ParameterDef.m_bIgnorePhaseData=0;

moSaveKernelParameter(ParameterKernel,ParameterDef);
clear ParameterDef;

ParameterR= moReaderParameter();
ParameterDef=moLoadReaderParameter(ParameterR);

ParameterDef.m_chunkNumberOfPingFan=50;

moSaveReaderParameter(ParameterR,ParameterDef);
else
    moLoadConfig('./config_pas_interp/');
    
    ParameterKernel= moKernelParameter();
    ParameterDef=moLoadKernelParameter(ParameterKernel);
    ParameterDef.m_bIgnorePhaseData=0;

    moSaveKernelParameter(ParameterKernel,ParameterDef);
    clear ParameterDef;

end

%lecture hac
%%%%%%%%%%%%%%%
moOpenHac(FileName);

FileStatus= moGetFileStatus;
ilec=1;
while FileStatus.m_StreamClosed < 1
%while FileStatus.m_StreamClosed < 1 & ilec<=3
    moReadChunk();
    FileStatus= moGetFileStatus();
    ilec=ilec+1;
end;
fprintf('End of File');

%presentation des sondeurs
%%%%%%%%%%%%%%%%%%%%%%%%%%
list_sounder=moGetSounderList;
nb_snd=size(list_sounder,2);
disp(' ');
disp(['nb sondeurs = ' num2str(nb_snd)]);
for isdr = 1:nb_snd
    nb_transduc=list_sounder(isdr).m_numberOfTransducer;
    if (list_sounder(isdr).m_SounderId<10)
        ind_list(isdr)=list_sounder(isdr).m_SounderId;
        disp(['sondeur ' num2str(isdr) ':    ' 'index: ' num2str(list_sounder(isdr).m_SounderId) '   nb trans:' num2str(nb_transduc)]);
        for itr=1:nb_transduc
            for ibeam=1:list_sounder(isdr).m_transducer(itr).m_numberOfSoftChannel
                disp(['   trans ' num2str(itr) ':    ' 'nom: ' list_sounder(isdr).m_transducer(itr).m_transName '   freq: ' num2str(list_sounder(isdr).m_transducer(itr).m_SoftChannel(ibeam).m_acousticFrequency/1000) ' kHz']);
            end
        end
    else
        disp('Pb sondeur Id');
    end
end



%a renseigner
index_sondeur=2;
seuil=-60;


detec=cell(moGetNumberOfPingFan(),list_sounder(ind_list(index_sondeur)).m_transducer.m_numberOfSoftChannel);


        heure_hac=[];
        heure_hac_frac=[];
        roll_hac=[];
        heave_hac=[];
        heave_hac2=[];
        at_hac=[];
        al_hac=[];
        lat=[];
        long=[];
        heading=[];
        vit=[];
        
        ind_ping=0;
        
for index= 0:moGetNumberOfPingFan()-1  %boucle sur les pings
    index
    MX= moGetPingFan(index);
    %if (MX.m_pingId >0)
    SounderDesc =  moGetFanSounder(MX.m_pingId);
    if SounderDesc.m_SounderId == list_sounder(index_sondeur).m_SounderId
        ind_ping=ind_ping+1;
         heure_hac(ind_ping)=MX.m_meanTimeCPU;
         heure_hac_frac(ind_ping)=MX.m_meanTimeFraction;
%          roll_hac(ind_ping)=MX.navigationAttitude.rollRad*180/pi;
%          heave_hac(ind_ping)=MX.navigationAttitude.sensorHeaveMeter;
%          heave_hac2(ind_ping)=MX.beam_data(1,1).m_compensateheave;
        lat(ind_ping)=MX.navigationPosition.m_lattitudeDeg;
        long(ind_ping)=MX.navigationPosition.m_longitudeDeg;
        heading(ind_ping)=MX.navigationAttribute.m_headingRad*180/pi;
        vit(ind_ping)=MX.navigationAttribute.m_speedMeter*3600/1852;
        for transducerIndex=1:SounderDesc.m_numberOfTransducer
        %for transducerIndex=1:1
            datalist = moGetPolarMatrix(MX.m_pingId,transducerIndex-1);
            sv_mat=datalist.m_Amplitude/100;
            Ath_mat=datalist.m_AthwartAngle;
%            at_hac(:,end+1,:)=Ath_mat.';
            Al_mat=datalist.m_AlongAngle;
%            al_hac(:,end+1,:)=Al_mat.';
            
            for ib=1:SounderDesc.m_transducer(1,transducerIndex).m_numberOfSoftChannel  %boucle sur les voies
                %BeamData=MX.beam_data(1,transducerIndex);   
                steer_at=SounderDesc.m_transducer(transducerIndex).m_SoftChannel(1,ib).m_mainBeamAthwartSteeringAngleRad;
                ouv_at=SounderDesc.m_transducer(transducerIndex).m_SoftChannel(1,ib).m_beam3dBWidthAthwartRad;
                ouv_al=SounderDesc.m_transducer(transducerIndex).m_SoftChannel(1,ib).m_beam3dBWidthAlongRad;
%                 Ath_l_cor=Al_mat(:,ib)*ouv_at/ouv_al*cos(steer_at)+steer_at-steer_at;
%                 Ath_l_cor=asin(sin(steer_at)+sin(Ath_l_cor))-steer_at; %correction TWeber
%                 Al_l_cor=(Ath_mat(:,ib)-steer_at)*ouv_al/ouv_at/cos(steer_at);

                %correction enregistrement angles switch� dans les hac
%                 Ath_l_cor=asin(sin(steer_at)+cos(steer_at)/cos(0)*ouv_at/ouv_al*(sin(Al_mat(:,ib))-sin(0)));
%                 Ath_l_cor=Ath_l_cor-steer_at;
%                 Al_l_cor=Ath_mat(:,ib)*ouv_al/ouv_at;
                %Ath_l_cor=asin(sin(steer_at)+cos(steer_at)*(Ath_mat(:,ib)-steer_at))-steer_at;  %correction TWeber
                
                %detection au-dessus seuil (et pas au max de la quantification de phase)
                ind_ech=find(squeeze(sv_mat(:,ib))>seuil & abs(squeeze(Ath_mat(:,ib)))<ouv_at*3/4*120/128 & abs(squeeze(Al_mat(:,ib)))<ouv_al*3/4*120/128); 
                ndet=size(ind_ech,1);
                if (ndet>0)
                    %figure(10);plot(squeeze(sv_mat(:,ib)),'-*');grid on;hold on;plot(ind_ech,squeeze(sv_mat(ind_ech,ib)),'r*');hold off;figure(20);plot(squeeze(Ath_l_cor)*180/pi,'-*');grid on;hold on;plot(ind_ech,Ath_l_cor(ind_ech)*180/pi,'r*');hold off;figure(200);plot(squeeze(Al_l_cor)*180/pi,'-*');grid on;hold on;plot(ind_ech,Al_l_cor(ind_ech)*180/pi,'r*');hold off
                    %pause;
                    for idet=1:ndet
                        %PosAbs= ComputeWorldPositionME70_2(0,SounderDesc.m_transducer.m_transFaceAlongAngleOffsetRad,SounderDesc.m_transducer.m_transFaceAthwarAngleOffsetRad, MX, SounderDesc.m_transducer, SounderDesc.m_transducer.m_SoftChannel(1,ib) , (ind_ech(idet)-1)*SounderDesc.m_transducer.m_beamsSamplesSpacing, 0, Ath_l_cor(ind_ech(idet)));
                        PosAbs= ComputeWorldPositionME70_2(0,SounderDesc.m_transducer(transducerIndex).m_transFaceAlongAngleOffsetRad,SounderDesc.m_transducer(transducerIndex).m_transFaceAthwarAngleOffsetRad, MX, SounderDesc.m_transducer(transducerIndex), SounderDesc.m_transducer(transducerIndex).m_SoftChannel(1,ib) , (ind_ech(idet)-1)*SounderDesc.m_transducer(transducerIndex).m_beamsSamplesSpacing, Al_mat(ind_ech(idet),ib), Ath_mat(ind_ech(idet),ib));
                        detec{ind_ping,ib}=[detec{ind_ping,ib}; [PosAbs.' sv_mat(ind_ech(idet),ib)]];             
                    end
                end
            end
        end
    end
end

detec=detec(1:ind_ping,:);
