%function [detec,lat,long,heading,heure,heure_ac,heavel,rolll,pitchl,res]=test_MBI2_raw(fname)
function [detec,lat,long,heading,heure]=test_MBI2_raw(fname)

% Reading SMS raw data file
% Simrad, Lars Nonboe Andersen, 12/9-05

if (0)
    fname ='K:\SMFH\EXACHA10\RAW ME70\EXACHA10-D20100319-T165553.raw'; %
    fname ='K:\SMFH\EXACHA10\RAW ME70\EXACHA10-D20100319-T163357.raw'; %
end

headerlength = 12; % Bytes in datagram header

pingno = 0;
pingno2 = 0;
nrefbeams = 0;
pingtime=0;
lastpingtime=0;
pingrate = [];

fid = fopen(fname,'r');
if (fid==-1)
    error('Could not open file');
else
    % Read configuration datagram
    length = fread(fid,1,'int32');
    dgheader = readdgheader(fid);
    configheader = readconfigheader(fid);
    for i=1:configheader.transducercount,
        configtransducer(i) = readconfigtransducer(fid);
        if ~isempty(findstr(configtransducer(i).channelid,'Reference'))
            nrefbeams = nrefbeams+1;
        end
    end
    config = struct('header',configheader,'transducer',configtransducer);
    config.header.nrefbeams = nrefbeams;
    length = fread(fid,1,'int32');
    nbpings=2000;
    detec=cell(nbpings,configheader.transducercount);
    
    indexpos=1;
    % Read NMEA, Annotation, or Sample datagram
    while (1)
    %while (pingno2<200)
        length = fread(fid,1,'int32');
        if (feof(fid))
            break
        end
        dgheader = readdgheader(fid);

        
        
        switch (dgheader.datagramtype)
        case 'CON1' % SMS extra configuration datagram
            text = readtextdata(fid,length-headerlength);
            disp('CON1');
        case 'NME0' % NMEA datagram
            text = readtextdata(fid,length-headerlength);
            if text.text(1:6)=='$INGLL' | text.text(1:6)=='$GPGLL'
             %if text.text(1:6)=='$INGGA'
%                 lat(indexpos)=str2num(text.text(8:18));
 %                long(indexpos)=str2num(text.text(22:30));
                 lat(pingno2)=str2num(text.text(8:9))+str2num(text.text(10:17))/60;
                 long(pingno2)=-(str2num(text.text(22:23))+str2num(text.text(24:31))/60);
                 heure(pingno2)=str2num(text.text(35:43));
%                 lat(pingno2)=str2num(text.text(19:28))/100;
%                 long(pingno2)=str2num(text.text(32:42))/100;
             end
             if text.text(1:6)=='$INHDT' | text.text(1:6)=='$GPHDT'
                heading(pingno2)=str2num(text.text(8:13));
             end
 %           disp('NME0');
        case 'TAG0' % Annotation datagram
            text = readtextdata(fid,length-headerlength);
            disp('TAG0');
        case 'RAW0' % Sample datagram
            sampledata = readsampledata(fid);
            %disp('RAW0');
           
            % WRITE YOUR OWN CODE HERE TO PROCESS AND/OR DISPLAY SAMPLE DATA
             channel = sampledata.channel;
      
                 tvgsampledata = applytvg(config,sampledata);
                 tvgdata(channel) = tvgsampledata;
%                 Attitude(1,pingno+1) = sampledata.heave;
%                 Attitude(2,pingno+1) = sampledata.roll;
%                 Attitude(3,pingno+1) = sampledata.pitch;
%                 Attitude(4,pingno+1) =  dgheader.datetime/ 10000000;
%                 pingno=pingno+1;
                
                if (sampledata.channel==1) 
                    pingno2=pingno2+1
                    if rem(pingno2,nbpings)==0
                        detec(pingno2+1:pingno2+nbpings,1:configheader.transducercount)=cell(nbpings,configheader.transducercount);
                    end
                end
                
                
                if (sampledata.channel==11) 
                res(pingno2,1:min(1000,size(tvgsampledata.sv,1)))=tvgsampledata.sv(1:min(1000,size(tvgsampledata.sv,1)));
                end
                
                steer_at=config.transducer(channel).diry;
                %pas de correction de c�l�rit�/calib
                anglesAt_data=asin(sind(steer_at)+sampledata.athwartship*cosd(steer_at)*0.75*config.transducer(channel).beamwidthathwartship/128*pi/180)-steer_at*pi/180;
                anglesAl_data=asin(sind(0)+sampledata.alongship*cosd(0)*0.75*config.transducer(channel).beamwidthalongship/128*pi/180)-0;
                sv_data=tvgsampledata.sv;
                sv_range=tvgsampledata.svrange;

                heave=sampledata.heave;
                roll = sampledata.roll;
                pitch = sampledata.pitch;
                heure_ac(pingno2)=dgheader.datetime;
                
                heavel(pingno2)=sampledata.heave;
                rolll(pingno2) = sampledata.roll;
                pitchl(pingno2) = sampledata.pitch;

                %MBI
                seuil=-50;
                ind_ech=find(sv_data>seuil & abs(sampledata.athwartship)<127 & abs(sampledata.alongship)<127); %detection au-dessus seuil
                ndet=size(ind_ech,1);
                if (ndet>0 & exist('heading') & exist('lat'))
                %figure(10);plot(squeeze(sv_mat(:,ib)),'-*');grid on;hold on;plot(ind_ech,squeeze(sv_mat(ind_ech,ib)),'r*');hold off;figure(20);plot(squeeze(Ath_l_cor)*180/pi,'-*');grid on;hold on;plot(ind_ech,Ath_l_cor(ind_ech)*180/pi,'r*');hold off;figure(200);plot(squeeze(Al_l_cor)*180/pi,'-*');grid on;hold on;plot(ind_ech,Al_l_cor(ind_ech)*180/pi,'r*');hold off
                %pause;
                    for idet=1:ndet
                        %PosAbs= ComputeWorldPositionME70_raw(lat(end),long(end),heading(end)*pi/180,pitch*pi/180,roll*pi/180, heave,0,steer_at*pi/180,sv_range(ind_ech(idet)), anglesAl_data(ind_ech(idet)), anglesAt_data(ind_ech(idet)));
                        
                        PosAbs= ComputeWorldPositionME70_raw(lat(end),long(end),heading(end)*pi/180,pitch*pi/180,roll*pi/180, heave,0,steer_at*pi/180,sv_range(ind_ech(idet)), anglesAl_data(ind_ech(idet)), anglesAt_data(ind_ech(idet)));
                        detec{pingno2,channel}=[detec{pingno2,channel}; [PosAbs.' sv_data(ind_ech(idet)) channel anglesAl_data(ind_ech(idet)) anglesAt_data(ind_ech(idet)) sv_range(ind_ech(idet))]];             
                    end
            end
                    
                    
        otherwise
            %error(strcat('Unknown datagram ''',dgheader.datagramtype,''' in file'));
        end
        length = fread(fid,1,'int32');

    end
    fclose(fid);
end

detec=detec(1:pingno2,:);
if (lat(1)==0)
    i0=min(find(lat~=0));
    lat(1)=lat(i0);
    long(1)=long(i0);
    heure(1)=heure(i0);
end
for ip=2:pingno2
    if (ip<=size(lat,2))
        if (lat(ip)==0) 
            lat(ip)=lat(ip-1);
            long(ip)=long(ip-1);
            heure(ip)=heure(ip-1);
        end
    else
        lat(ip)=lat(ip-1);
        long(ip)=long(ip-1);
        heure(ip)=heure(ip-1);
    end
end

disp('Finished reading file');

    
