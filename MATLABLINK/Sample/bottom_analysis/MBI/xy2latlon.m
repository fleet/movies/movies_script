% Passage de coordonn�es cartographiques � coordonn�es g�ographiques
%
% SYNTAX :
%   [lat, lon] = xy2latlon(Carto, x, y)
%
% INPUT PARAMETERS :
%   Carto : Instance de cartographie
%   x  : Description du parametre (m).
%   y  : Description du parametre (m).
%
% OUTPUT PARAMETERS :
%   lat   : Latitudes (deg d�cimaux)
%   lon   : Longitudes (deg d�cimaux)
%
% EXAMPLES :
%   Carto = cl_carto('Ellipsoide.Type', 12, 'Projection.Type', 4)
%   [lat, lon] = xy2latlon(Carto, 700000, 6600000)
%   [x, y] = latlon2xy(Carto, lat, lon)
%
%   [lat, lon] = xy2latlon(Carto, x, y)
%   [lat, lon] = xy2latlon(Carto, 1160000, 4313000)
%
% SEE ALSO   : cl_carto xy2latlon Authors
% REFERENCES : CARAIBES(R)
% AUTHORS    : JMA
% VERSION    : $Id: entete.m,v 1.2 2003/11/12 12:46:11 augustin Exp $
%-------------------------------------------------------------------------------

function [lat, lon] = xy2latlon(this, x, y)

E  = double(this.Ellipsoide.Excentricite);
E2 = E ^ 2;
A  = double(this.Ellipsoide.DemiGrandAxe);
B  = A * sqrt(1-E2);

deg2rad = pi / 180;
rad2deg = 180 / pi;

try
    lat = NaN(size(x));
    lon = NaN(size(x));
catch %#ok<CTCH> % Test
    lat = cl_memmapfile('Value', NaN, 'Size', size(x), 'Format', 'double');
    lon = cl_memmapfile('Value', NaN, 'Size', size(x), 'Format', 'double');
end

switch this.Projection.Type
    case 2 % Mercator
        X0 = double(this.Projection.Mercator.X0);
        Y0 = double(this.Projection.Mercator.Y0);

        G0   = double(this.Projection.Mercator.long_merid_orig) * deg2rad;
        L0   = double(this.Projection.Mercator.lat_ech_cons)    * deg2rad;
        SL   = sin(L0) ^ 2;
        NORM = sqrt((1-E2*SL) / (1-SL));
        for i=1:size(x,1)
            T    = tanh((y(i,:)-Y0) * (NORM / A / 2));
            S    = 2 * T ./ (1 + T.^ 2);
            B1   = E2 * (1 + E2 + E2.^2) / 2;
            B3   = -E2 ^ 2 * (1 + 2.625 * E2) / 3;
            B5   = E2 ^ 3 * 13 / 30;
            B    = B1 * S + B3 * S .^ 3 + B5 *S .^ 5;

            L = 2 * atan2(B + T, 1 + B .* T);
            G = (x(i,:)-X0) * NORM / A + G0;

            lon(i,:) = G * rad2deg;
            lat(i,:) = L * rad2deg;
        end

    case 3  % UTM
        X0 = double(this.Projection.UTM.X0);
        Y0 = double(this.Projection.UTM.Y0);

        G0    = ((double(this.Projection.UTM.Fuseau)-30)*6 - 3) * deg2rad;
        EP2   = E2 / (1 - E2);
        C     = A / sqrt(1 - E2);
        
        Algo = 1;
        
        if Algo == 1
            ALPHA = 0.75 * EP2;
            BETA  = 0.9375 * EP2^2;
            GAMMA = 0.546875 * EP2^3;
        else
            ALPHA = 3/2 * EP2;      % (3/2) / 2 = 0.75
            BETA  = 15/8 * EP2^2;   % (15/8) / 2 = 0.9375
            GAMMA = 35/16 * EP2^3;  % (35/16) / 2 = 1.09375   (35/16) / 4 = 0.546875
            ZETA  = 315/128 * EP2^4;
        end


        for i=1:size(x,1)
            W0    = (y(i,:)-Y0) / 0.9996;
            LAT   = W0 / A^2 * B;
            cosLAT = cos(LAT);
            sinLAT = sin(LAT);
            sinLATxcosLAT = sinLAT .* cosLAT;
            N     = C ./ sqrt(1 + EP2 * cosLAT .^ 2);
            
            if Algo == 1
                A1    = LAT + sinLATxcosLAT;
                A2    = 0.75 * A1 + 0.5 * sinLAT .* (cosLAT .^ 3);
                A3    = (5 * A2 + 2 * sinLAT .* (cosLAT .^ 5)) / 3;
                S     = C * (LAT - ALPHA*A1 + BETA*A2 - GAMMA*A3);
                W2    = (x(i,:)-X0) ./ N / 0.9996;
                W3    = EP2 * cosLAT .^ 2 .* W2 .^ 2;
                XI    = W2 .* (1 - W3 / 6);
            else
                A1    = (LAT + sinLATxcosLAT) / 2.;
                A2    = (sinLAT .* (cosLAT .^ 3) + 3 * A1) / 4.;
                A3    = (sinLAT .* (cosLAT .^ 5) + 5 * A2) / 6.;
                A4    = (sinLAT .* (cosLAT .^ 7) + 7 * A3) / 8.;
                S     = C * (LAT - ALPHA*A1 + BETA*A2 - GAMMA*A3 + ZETA*A4);
                l_x     = (x(i,:) - X0) / 0.9996;
                W3    = EP2 * (cosLAT .^ 2) ./ 6 ./ N.^3;
                W4    = EP2 * sinLATxcosLAT .* (3 + 4 * EP2 * cosLAT .^ 2) ./ 48 ./ N.^4;
                XI    = l_x ./ N - l_x.^3 .* W3 + l_x.^4. .* W4;
            end
            
            ETA   = (W0-S) .* (1 - W3 / 2) ./ N;
            LatPlusEta = LAT+ETA;
            cosLatPlusEta = cos(LatPlusEta);
            DG    = atan2(sinh(XI), cosLatPlusEta);
            DL    = atan2(cos(DG) .* sin(LatPlusEta), cosLatPlusEta)-LAT;
            M2   = (1 + EP2 * cosLAT .^ 2) .* (1 - 1.5 * EP2 * sinLATxcosLAT .* DL);
            lat(i,:) = (LAT + M2 .* DL) * rad2deg ; %- 0.00158
            lon(i,:) = (DG + G0) * rad2deg;
%{
            [convergence, scaleFactor] = get_convergence(this, lat(i,:), lon(i,:));
%}
        end

    case 4 % Lambert
% this.Projection.Lambert.Y0 = 6602157;
% this.Projection.Lambert.scale_factor = 0.999051;
% this.Projection.Lambert.first_paral = 46.519432;
% this.Projection.Lambert.second_paral = 46.519432;      

        L1 = double(this.Projection.Lambert.first_paral)     * deg2rad;
        L2 = double(this.Projection.Lambert.second_paral)    * deg2rad;
        G0 = double(this.Projection.Lambert.long_merid_orig) * deg2rad;
        X0 = double(this.Projection.Lambert.X0);
        Y0 = double(this.Projection.Lambert.Y0);
        RK = double(this.Projection.Lambert.scale_factor);
        
% RK = 0.9987       
        if L1 == L2
            L12  = L1;
            ISO1 = tan(L1/2 + pi/4) * ((1 - E*sin(L1)) / (1 + E*sin(L1))) ^ (E/2);
            N1 = A / sqrt(1 - E2*sin(L1)^2);
            N    = 1;
            C    = N1 * cos(L1) / N * ISO1^N;
            R0   = A / sqrt(1 - E2*sin(L12)^2) / sin(L12) * cos(L12) * RK;
        else
            N1 = A / sqrt(1 - E2*sin(L1)^2);
            N2 = A / sqrt(1 - E2*sin(L2)^2);
            ISO1 = tan(L1/2 + pi/4) * ((1 - E*sin(L1)) / (1 + E*sin(L1))) ^ (E/2);
            ISO2 = tan(L2/2 + pi/4) * ((1 - E*sin(L2)) / (1 + E*sin(L2))) ^ (E/2);
            L12  =(L1 + L2) / 2;
            R0   = A / sqrt(1 - E2*sin(L12)^2) / sin(L12) * cos(L12) * RK;
            N    = log((N2 * cos(L2)) / (N1 * cos(L1))) / log(ISO1/ISO2);
            C    = N1 * cos(L1) / N * ISO1^N;
        end

        B1 =  E2 * (1 + E2 + E2^2)  / 2;
        B3 = -E2^2 * (1 + 2.625*E2) / 3;
        B5 =  E2^3 * 13/30;

        for i=1:size(x,1)
            Deno = R0 - y(i,:) + Y0;
            R = abs(C ./ Deno .* cos(atan((x(i,:) - X0) ./ Deno))) .^ (1/N);
            T = (R-1)./(R+1);
            S = 2*T ./ (1 + T.^2);
            B = B1*S + B3*S.^3 + B5*S.^5;

            L = 2 * atan2(B+T, 1+B.*T);
            G = atan((x(i,:) - X0) ./ Deno) / N + G0;

            lon(i,:) = G * rad2deg;
            lat(i,:) = L * rad2deg;
        end
        
%{
if 1 % (type >= CIB_CCO_LAMBERT_FRANCE_93 && type <= CIB_CCO_LAMBERT_FRANCE_IV_EXTENDED)
    scale = RK;
else
    l_latitude0 = (L1 + L2) / 2;
    sinLat = sin(l_latitude0);
    scale = cos(l_latitude0) / sqrt(1. - E2* sinLat * sinLat);
end
      
l_lat  = (L1 + L2) / 2.0;
uRAY0 = GetNormal(l_lat, A, E2) / sin(l_lat) * cos(l_lat) * scale;

lx = x - X0;
ly = uRAY0 - y + Y0;

%  Calcul de la longitude en fonction de l'abscisse X

l_iso  = GetLatIsometric(L1, E);
l_norm = GetNormal(L1, A, E2);
    
if L1 == L2
    uN = sin(L1);
    uC = uRAY0 * l_iso .^ uN;
else
    l_cos = cos(L1);
    uN = log(GetNormal(L2, A, E2) / l_norm * cos(L2) / l_cos) / log(l_iso / GetLatIsometric(L2, E));
    uC = l_norm * l_cos  / uN * l_iso ^ uN;
end

if (ly == 0.)
    lgAM = 0;
else
    lgAM = atan(lx ./ ly);
end

lon = ((lgAM / uN)) + G0;

sub = (lon < - pi);
lon(sub) = lon(sub) + 2*pi;
sub = (lon > pi);
lon(sub) = lon(sub) - 2*pi;
lon = lon * (180/pi);

% Calcul de la laltitude en fonction de l'ordonnee Y

lat = abs(uC ./ ly .* cos(lgAM)) .^ (1 / uN);

luT  = (lat - 1) ./ (lat + 1);
luS  = 2 * luT ./ (1 + (luT .* luT));
uB1 = E2 * (1 + E2 + l_lat) / 2;
uB3 = -l_lat * (1. + (21.0 / 8.0 * E2)) / 3;
uB5 = 13 / 30 * l_lat * E2;
       
luB  = (uB1 .* luS) + (uB3 .* luS.^3) + (uB5 .* luS.^5);
lat = 2 * atan2(luB + luT, 1. + (luB .* luT));
lat = lat * (180/pi);
%}

    case 5 % stereographic polaire
        L0 = double(this.Projection.Stereo.lat_ech_cons);
        G0 = double(this.Projection.Stereo.long_merid_horiz);
        %         H0=this.Projection.Stereo.hemisphere;
        S = E * sin(abs(L0));
        U = A * (((1-S)/(1+S))^E/(1-S^2))^(1/2)*cos(abs(L0))/tan((pi/4)-(abs(L0)/2));
        for i=1:size(x,1)
            C2 = (x(i,:).^2+y(i,:).^2).^(1/2)./U;
            T = (C2-1)./(C2+1);
            B1 = E2*(1+E2+E2^2)/2;
            B3 = -E2^2*(1+2.625*E2)/3;
            B5 = E2^3*13/30;
            B  = B1*S+B3*S^3+B5*S^5;
            LAT = abs(2*atan((B+T) ./ (1+B*T)));

            if strcmp(this.Projection.Stereo.hemisphere, 'N')
                L = LAT;
            else
                L = -LAT;
            end

            G = G0 - atan2(y(i,:), x(i,:));

            lon(i,:) = G * rad2deg;
            lat(i,:) = L * rad2deg;
        end
        
    case 6 % Cylindrique Equidistant
        my_warndlg('Projection Cylindrique Equidistant pas encore programme dans cl_carto/xy2latlon', 1)
        lat = [];
        lon = [];
    otherwise
        disp('Pas fait')
end

function N = GetNormal(myLatitude, A, E2)
sinLat = sin(myLatitude);
l_temp = 1 - (E2 * sinLat * sinLat);
N = A / sqrt(l_temp);

function l_iso  = GetLatIsometric(l_rLat, E)
l_dummy = (l_rLat / 2 + pi/4);
l_uX    = E;
l_uE    = l_uX * sin(l_rLat);
l_temp = (1 - l_uE) / (1 + l_uE) ^ (l_uX / 2);
l_iso = (sin(l_dummy) / cos(l_dummy) * l_temp);
