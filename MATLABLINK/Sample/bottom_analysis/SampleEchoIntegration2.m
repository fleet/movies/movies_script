function SampleEchoIntegration2(chemin_hac,chemin_save,chemin_config,varargin)
% � partir de SampleEchoIntegration.m, ajout de la lecture du pilonnement,
% profondeur d�tect�e, bool�en bottom_found, liste des fr�quences ER60 (pas toujours enregistr�es dans le m�me ordre)
%
% r�alise en enregistre les donn�es d'�cho-int�gration (Sa et Volume par couche et par ping, instants des pings echo-int�gr�s, latitude, longitude, profondeur moyenne de chaque couche par ping, pilonnement, profondeur d�tect�e, bool�en bottom_found, instants des pings des donn�es acoustiques)
% chemin_hac:       chemin du dossier contenant les .hac � traiter
% chemin_save:      chemin du dossier o� sauvegrader les r�sultats .mat
% chemin_config:    chemin du dossier contenant les fichiers de config .xml
%                   g�n�r�s par movies
% varargin:         entr�e optionnelle du nom de fichier hac � traiter (si 
%                   on d�sire n'en traiter qu'un, sinon tous les fichiers 
%                   du dossier chemin_hac seront trait�s)


%% Parametres de configuration

% Chargement de la bonne configuration
% Attention : a charger avant le OpenHAC
% sinon on a une config qui change en cours de
% route
chemin_hac_reverse = strrep(chemin_hac, '\', '/');
chemin_config_reverse = strrep(chemin_config, '\', '/');

%moLoadConfig(chemin_config_reverse);
% il semble qu'on ne puisse faire un LoadConfig qu'une fois par session
% matlab
persistent config_deja_chargee

if isempty(config_deja_chargee)
   moLoadConfig(chemin_config_reverse);
   config_deja_chargee=1;
end

% Taille des blocs
taille_bloc = 3000;

ParameterKernel= moKernelParameter();
ParameterDef=moLoadKernelParameter(ParameterKernel);

ParameterDef.m_nbPingFanMax=taille_bloc;
ParameterDef.m_bAutoLengthEnable=0;
ParameterDef.m_bAutoDepthEnable=1;
ParameterDef.m_MaxRange = 200;
ParameterDef.m_bIgnorePhaseData=1;

moSaveKernelParameter(ParameterKernel,ParameterDef);
clear ParameterKernel;
clear ParameterDef;

ParameterR= moReaderParameter();
ParameterDef=moLoadReaderParameter(ParameterR);

ParameterDef.m_chunkNumberOfPingFan=taille_bloc;

moSaveReaderParameter(ParameterR,ParameterDef);
clear ParameterR;
clear ParameterDef;


%% �cho-int�gration 

% liste des fichiers � traiter
if nargin==3
    filelist = ls([chemin_hac,'*.hac']);  % ensemble des fichiers
    nb_files = size(filelist,1);  % nombre de fichiers
else
    filelist(1,:) = varargin{1};
    nb_files = 1;
end


%for numfile = 1:nb_files  % boucle sur l'ensemble des fichiers � traiter
for numfile = 1:1  % boucle sur l'ensemble des fichiers � traiter
    
    hacfilename = filelist(numfile,:);
    
    FileName = [chemin_hac,hacfilename]

    moOpenHac(FileName);

    num_bloc=1;

    FileStatus= moGetFileStatus;
    
    while FileStatus.m_StreamClosed < 1
        hacfilename
        display(['bloc numero ',num2str(num_bloc)])

        % init matrices
        time_ER60 = [];
        time_ME70 = [];
        Sa_surfME70 =[];
        Sa_botME70=[];
        Sa_surfER60=[];
        Sa_botER60=[];
        Sv_surfME70=[];
        Sv_botME70=[];
        Sv_surfER60=[];
        Sv_botER60=[];
        Lat_surfME70=[];
        Long_surfME70=[];
        Depth_surfME70=[];
        Lat_surfER60=[];
        Long_surfER60=[];
        Depth_surfER60=[];
        Lat_botME70=[];
        Long_botME70=[];
        Depth_botME70=[];
        Lat_botER60=[];
        Long_botER60=[];
        Depth_botER60=[];
        Volume_surfME70=[];
        Volume_surfER60=[];
        Volume_botME70=[];
        Volume_botER60=[];
        AllData=[];


        EchoIntegrModule = moEchoIntegration();
        moSetEchoIntegrationEnabled(EchoIntegrModule);
        
        moReadChunk();

        AllData=moGetOutPutList(EchoIntegrModule);

        % r�cup�ration de la d�finition des ESU
        ESUParam = moGetESUDefinition();

        % r�cup�ration de la d�finition des couches
        LayersDef = moGetLayersDefinition(EchoIntegrModule);

        % r�cup�ration des volumes de chaque couche (pour 1 ping)
        LayersVolumes = moGetLayersVolumes(EchoIntegrModule);
        sounderNb = size(LayersVolumes(1).Sounders,2);
        indexSounderME70=0;
        indexSounderER60=0;
        for indexSounder=1:sounderNb
            if size(LayersVolumes(1).Sounders(indexSounder).Channels,2) > 6
                indexSounderME70=indexSounder;
            else
                indexSounderER60=indexSounder;
            end
        end

        clear EchoIntegrModule;



        indexESUME70 = 1;
        indexESUER60 = 1;
        nbResults = size(AllData,2);
        for indexResults=1:nbResults
            if size(AllData(1,indexResults).m_Sa,2)>6

                time_ME70(indexESUME70) = AllData(1,indexResults).m_time;
                nbLayer= size(AllData(1,indexResults).m_Sa,1);
                indexLayerSurface = 1;
                indexLayerBottom = 1;

                for indexLayer=1:nbLayer
                    if LayersDef.LayerType(indexLayer)==0
                        Sa_surfME70(indexESUME70,indexLayerSurface,:)=max(0,AllData(1,indexResults).m_Sa(indexLayer,:));
                        Sv_surfME70(indexESUME70,indexLayerSurface,:)=max(-100,AllData(1,indexResults).m_Sv(indexLayer,:));
                        Lat_surfME70(indexESUME70,indexLayerSurface,:)=AllData(1,indexResults).m_latitude(indexLayer,:);
                        Long_surfME70(indexESUME70,indexLayerSurface,:)=AllData(1,indexResults).m_longitude(indexLayer,:);
                        Depth_surfME70(indexESUME70,indexLayerSurface,:)=AllData(1,indexResults).m_Depth(indexLayer,:);
                        Volume_surfME70(indexESUME70,indexLayerSurface,:)=LayersVolumes(indexLayer).Sounders(indexSounderME70).Channels(:);
                        indexLayerSurface = indexLayerSurface+1;
                    else
                        Sa_botME70(indexESUME70,indexLayerBottom,:)=max(0,AllData(1,indexResults).m_Sa(indexLayer,:));
                        Sv_botME70(indexESUME70,indexLayerBottom,:)=max(-100,AllData(1,indexResults).m_Sv(indexLayer,:));
                        Lat_botME70(indexESUME70,indexLayerBottom,:)=AllData(1,indexResults).m_latitude(indexLayer,:);
                        Long_botME70(indexESUME70,indexLayerBottom,:)=AllData(1,indexResults).m_longitude(indexLayer,:);
                        Depth_botME70(indexESUME70,indexLayerBottom,:)=AllData(1,indexResults).m_Depth(indexLayer,:);
                        Volume_botME70(indexESUME70,indexLayerBottom,:)=LayersVolumes(indexLayer).Sounders(indexSounderME70).Channels(:);
                        indexLayerBottom = indexLayerBottom+1;
                    end;

                end;


                indexESUME70=indexESUME70+1;
                
            elseif size(AllData(1,indexResults).m_Sa,2)>1  %filtre le netsonde
            %else
                time_ER60(indexESUER60) = AllData(1,indexResults).m_time;

                nbLayer= size(AllData(1,indexResults).m_Sa,1);
                indexLayerSurface = 1;
                indexLayerBottom = 1;

                for indexLayer=1:nbLayer
                    if LayersDef.LayerType(indexLayer)==0
                        Sa_surfER60(indexESUER60,indexLayerSurface,:)=max(0,AllData(1,indexResults).m_Sa(indexLayer,:));
                        Sv_surfER60(indexESUER60,indexLayerSurface,:)=max(-100,AllData(1,indexResults).m_Sv(indexLayer,:));
                        Lat_surfER60(indexESUER60,indexLayerSurface,:)=AllData(1,indexResults).m_latitude(indexLayer,:);
                        Long_surfER60(indexESUER60,indexLayerSurface,:)=AllData(1,indexResults).m_longitude(indexLayer,:);
                        Depth_surfER60(indexESUER60,indexLayerSurface,:)=AllData(1,indexResults).m_Depth(indexLayer,:);
                        Volume_surfER60(indexESUER60,indexLayerSurface,:)=LayersVolumes(indexLayer).Sounders(indexSounderER60).Channels(:);
                        indexLayerSurface = indexLayerSurface+1;
                    else
                        Sa_botER60(indexESUER60,indexLayerBottom,:)=max(0,AllData(1,indexResults).m_Sa(indexLayer,:));
                        Sv_botER60(indexESUER60,indexLayerBottom,:)=max(-100,AllData(1,indexResults).m_Sv(indexLayer,:));
                        Lat_botER60(indexESUER60,indexLayerBottom,:)=AllData(1,indexResults).m_latitude(indexLayer,:);
                        Long_botER60(indexESUER60,indexLayerBottom,:)=AllData(1,indexResults).m_longitude(indexLayer,:);
                        Depth_botER60(indexESUER60,indexLayerBottom,:)=AllData(1,indexResults).m_Depth(indexLayer,:);
                        Volume_botER60(indexESUER60,indexLayerBottom,:)=LayersVolumes(indexLayer).Sounders(indexSounderER60).Channels(:);
                        indexLayerBottom = indexLayerBottom+1;
                    end;
                end;

                indexESUER60=indexESUER60+1;
            end                            
        end;
        
        % presentation des sondeurs
        list_sounder=moGetSounderList;
        nb_snd=length(list_sounder);
        for isdr = 1:nb_snd
            nb_transduc=list_sounder(isdr).m_numberOfTransducer;
            if nb_transduc==5 % rep�re les ER60
                SounderId=list_sounder(isdr).m_SounderId;
                ind_snd=isdr;
                for itr=1:nb_transduc
                    list_freq(itr)=list_sounder(isdr).m_transducer(itr).m_SoftChannel.m_acousticFrequency;
                end
                break;
            end
        end
        
        
        %donn�es compl�mentaires ER60
        heave_m=[];
        heave_comp_m=[];
        dpth_m=[];
        lat=[];
        long=[];
        ping_id=[];
        bottom_found=[];
        time_ping=[];
        for index= 0:moGetNumberOfPingFan()-1
                MX= moGetPingFan(index);
                SounderDesc =  moGetFanSounder(MX.m_pingId);
                if SounderDesc.m_SounderId == SounderId
                    for i_trans=1:5
                      dpth_m(end+floor(1/i_trans),i_trans)=MX.beam_data(i_trans).m_bottomRange;
                      bottom_found(end+floor(1/i_trans),i_trans)=MX.beam_data(i_trans).m_bottomWasFound;
                      heave_comp_m(end+floor(1/i_trans),i_trans)=MX.beam_data(i_trans).m_compensateHeaveMovies;
                    end
                      heave_m(end+1)=MX.navigationAttitude.sensorHeaveMeter;
                      lat(end+1)=MX.navigationPosition.m_lattitudeDeg;
                      long(end+1)=MX.navigationPosition.m_longitudeDeg;
                      ping_id(end+1)=MX.m_pingId;
                      time_ping(end+1)=MX.m_meanTimeCPU+MX.m_meanTimeFraction/10000;
                end
        end
        
        
        % enregistrement
        if ~exist(chemin_save,'dir')
                mkdir(chemin_save)
        end

        save ([chemin_save, hacfilename(1:findstr('.',hacfilename)-1),'_', num2str(num_bloc),'_EI'],'time_ER60','time_ME70','Sa_surfME70', 'Sa_botME70', 'Sa_surfER60', 'Sa_botER60', 'Sv_surfME70', 'Sv_botME70', 'Sv_surfER60', 'Sv_botER60','Lat_surfME70', 'Long_surfME70', 'Depth_surfME70','Lat_surfER60','Long_surfER60','Depth_surfER60','Lat_botME70', 'Long_botME70', 'Depth_botME70','Lat_botER60','Long_botER60','Depth_botER60','Volume_surfER60','Volume_botER60','Volume_surfME70','Volume_botME70','ind_snd','SounderId','list_freq','dpth_m','bottom_found','heave_comp_m','heave_m','lat','long','ping_id','time_ping') ;
        clear time_ER60 time_ME70 Sa_surfME70 Sa_botME70 Sa_surfER60 Sa_botER60 Sv_surfME70 Sv_botME70 Sv_surfER60 Sv_botER60 Lat_surfME70 Long_surfME70 Depth_surfME70 Lat_surfER60 Long_surfER60 Depth_surfER60 Lat_botME70 Long_botME70 Depth_botME70 Lat_botER60 Long_botER60 Depth_botER60
        clear ind_snd SounderId list_freq dpth_m bottom_found heave_comp_m heave_m lat long ping_id time_ping
        clear AllData;
        clear Volume_surfER60 Volume_botER60 Volume_surfME70 Volume_botME70;

        num_bloc=num_bloc+1;
        FileStatus= moGetFileStatus();
        
    end
end;
