%nom_chemin='C:\detection_fond_pelgas\PELGAS09';
%nom_fic='PELGAS09_008_20090503_064421';
if (0) %appel standalone
    num_run='005';

    freq_ER60=[18 38 70 120 200]*1000;
    i_freq=2;
    typ=1;
end

clear llist_freq ldpth lheave lbottom_found ldpth_ini lbottom_found_ini Sa_tot nb_pg D1 ltime_ER60 i_pf nb_pf

if (typ==0)
    typ_name='Brut';
    ap_dos='brut';
    ap='';
    disp('resultats brut');
elseif(typ==1)
    typ_name='Cor';
    ap_dos='cor';
    ap='_1';
    disp('resultats corrige');
else
end

chemin_save=['.\PELGAS09' ap_dos '\RUN' num_run '\EIlay' typ_name '\'];
%chemin_save=['D:\lebouffant\SMFH\detection_fond_pelgas\PELGAS09' ap_dos '\RUN' num_run '\EIlay' typ_name '\'];
%chemin_save=['Z:\PELGAS09' ap_dos '\RUN' num_run '\EIlay' typ_name '\'];

filelist = ls([chemin_save,'*.mat']);  % ensemble des fichiers
nb_files = size(filelist,1);  % nombre de fichiers

nb_pf=[];
i_pf=[];
Sa_tot=[];
Sa_tot_filtre=[];
lD1=[];
ltime_ER60=[];
for numfile = 1:nb_files  % boucle sur l'ensemble des fichiers � traiter

    if (0) %recharge les index sondeurs
        clear ind_snd list_freq ;
        FileName = ['L:\PELGAS09\HAC-MOVIES\Hac' typ_name 'Pel09\RUN' num_run '\' filelist(numfile,1:end-9) '.HAC'];
        moOpenHac(FileName);
        list_sounder=moGetSounderList;
        nb_snd=length(list_sounder);
        for isdr = 1:nb_snd
            nb_transduc=list_sounder(isdr).m_numberOfTransducer;
            if (nb_transduc==5)
                ind_snd=isdr;
                for itr=1:nb_transduc
                    list_freq(itr)=list_sounder(isdr).m_transducer(itr).m_SoftChannel.m_acousticFrequency;
                end
            end
        end
        i_trans=find(freq_ER60(i_freq)==list_freq);
        
        load([chemin_save filelist(numfile,:)]);
        save ([chemin_save filelist(numfile,:)],'time_ER60','time_ME70','Sa_surfME70', 'Sa_botME70', 'Sa_surfER60', 'Sa_botER60', 'Sv_surfME70', 'Sv_botME70', 'Sv_surfER60', 'Sv_botER60','Lat_surfME70', 'Long_surfME70', 'Depth_surfME70','Lat_surfER60','Long_surfER60','Depth_surfER60','Lat_botME70', 'Long_botME70', 'Depth_botME70','Lat_botER60','Long_botER60','Depth_botER60','Volume_surfER60','Volume_botER60','Volume_surfME70','Volume_botME70','ind_snd','list_freq') ;
       
    end


    load([chemin_save filelist(numfile,:)]);
    i_trans=find(freq_ER60(i_freq)==list_freq);
    li_trans(numfile)=i_trans;
    llist_freq(numfile,:)=list_freq;
    ldpth{numfile}=dpth_m;
    lheave{numfile}=heave_m;
    lheave_comp{numfile}=heave_comp_m;
    lbottom_found{numfile}=bottom_found;
    
        
    % calcul du Sa par seuils
    % rappel: chaque couche de fond fait 10cm
    sat=[];
    for i_couche=1:10
        sat(i_couche,:)=Sa_surfER60(:,1,i_trans)-sum(Sa_botER60(:,1:i_couche,i_trans),2);
    end
    Sa_tot{numfile}=sat;
    
    %Sa_tot_filtre{numfile}=sat(:,find(Depth_botER60(:,1,i_trans)>10 & Depth_botER60(:,1,i_trans)<1000));
    

    % calcul du nombre de prises de fond
    seuil_pf=10.^(5:-1:3);
    for i_couche=1:10
        for i_seuil_niv=1:length(seuil_pf)
            i_pf{numfile,i_couche,i_seuil_niv}=find(sat(i_couche,:)>seuil_pf(i_seuil_niv) & ...
                squeeze(Depth_botER60(:,1,i_trans)).'>10 & squeeze(Depth_botER60(:,1,i_trans)).'<1000);
            nb_pf(numfile,i_couche,i_seuil_niv)=sum(sat(i_couche,:)>seuil_pf(i_seuil_niv) & squeeze(Depth_botER60(:,1,i_trans)).'>10 & squeeze(Depth_botER60(:,1,i_trans)).'<1000,2);
        end
    end

    %nombre de pings du fichier
    nb_pg(numfile)=size(Sa_tot,2);
    
    %profondeur 1ere couche
    lD1{numfile}=squeeze(Depth_botER60(:,1,:));

    %recuperation du temps pour calage
    ltime_ER60{numfile}=time_ER60;
    
    
    % chargement du haclab si le corrige en provient
    if typ==1 & isequal(filelist(numfile,end-10:end),'_1_1_EI.mat')
        chemin_save2=['.\PELGAS09lab\RUN' num_run '\EIlayLab\'];
        load([chemin_save2 filelist(numfile,:)]);
        ldpth_ini{numfile}=dpth_m;
        lbottom_found_ini{numfile}=bottom_found;
    end
    
end
