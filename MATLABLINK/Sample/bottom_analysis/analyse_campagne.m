
runs=[(1:14) (16:23) (26:29)];


blk_l_cor{2}=[2]; %pas de roll/pitch/heave. Fond d�tect� dans la cde 
blk_l_cor{4}=8;
blk_l_cor{6}=[3 26];
blk_l_cor{7}=[29 33 34];
blk_l_cor{10}=[35];
blk_l_cor{13}=[46];
blk_l_cor{19}=[10 28 37 38];
blk_l_cor{20}=[16 17 20];
blk_l_cor{21}=[4 28];
blk_l_cor{22}=[5];
blk_l_cor{23}=[8];
blk_l_cor{29}=[7 16 29];

nb_hac=0;
nb_hac_ini=0;
nb_sde=0;
nb_sde_cor=0;
for ir=1:length(runs)
    
    irun=runs(ir);

    ind_garde=[];

    prop_filtre=prop_filtre_run{irun};
    nb_pf00=nb_pf00_run{irun};
    med_ddpth=med_dz_run{irun};

    for ific=1:length(ind_fic_com1_run{irun}) 
        % filtrage de erreurs de c�l�rit� sur le brut et des fichiers
        % corrig�s avec pf
        if (prop_filtre(ific)<0.3 & nb_pf00(ific)<200 & abs(med_ddpth(ific))<0.4 & isempty(find(blk_l_cor{irun}==ific)))
            ind_garde(end+1)=ific;
        end
    end
    nb_hac=nb_hac+length(ind_garde);
    nb_hac_ini=nb_hac_ini+length(ind_fic_com1_run{irun});
    ind_garde_run{irun}=ind_garde;
    
    ns=nb_sde_run{irun};
    nsc=nb_sde_cor_run{irun};
    nb_sde_run2(irun)=sum(ns(ind_garde));
    nb_sde_cor_run2(irun)=sum(nsc(ind_garde));
end
nb_hac_ini
nb_hac

%%%%%%%%%%%%%%%%% proportion de sondes corrig�es
for ir=1:length(runs)
    irun=runs(ir);
    prop_cor(irun)=nb_sde_cor_run2(irun)/nb_sde_run2(irun);
end

figure(1);plot(prop_cor,'-*','LineW',2);grid on
xlabel('runs');ylabel('proportion de sondes corrig�es');title(['pour une proportion corrig�e totale de ' num2str(sum(nb_sde_cor_run2)/sum(nb_sde_run2))])

%%%%%%%%%%%%%%%%% Sa
dif_concat=[];
ind_concat=[];
sa0_concat=[];
sa1_concat=[];
for ir=1:length(runs)
    irun=runs(ir);

    mean_sa0=mean_sa0_run{irun};
    mean_sa1=mean_sa1_run{irun};
    mean2_sa0(irun,:)=mean(mean_sa0(ind_garde_run{irun},:),1);
    mean2_sa1(irun,:)=mean(mean_sa1(ind_garde_run{irun},:),1);
    mean_dif_sa(irun,:)=mean(mean_sa0(ind_garde_run{irun},:)-repmat(mean_sa1(ind_garde_run{irun},3),1,10));
    mean_abs_dif_sa(irun,:)=mean(abs(mean_sa0(ind_garde_run{irun},:)-repmat(mean_sa1(ind_garde_run{irun},3),1,10)));
    sa0_concat=[sa0_concat;mean_sa0(ind_garde_run{irun},:)];
    sa1_concat=[sa1_concat;mean_sa1(ind_garde_run{irun},:)];
    dif_concat=[dif_concat ; mean_sa0(ind_garde_run{irun},:)-repmat(mean_sa1(ind_garde_run{irun},3),1,10)];
    ind_concat=[ind_concat irun*ones(1,length(ind_garde_run{irun}))];
end

figure(2);plot(mean2_sa0(:,3),'-*','LineW',2);hold on;plot(mean2_sa1(:,3),'r-*','LineW',2);hold off;grid on
xlabel('runs');title('Sa moyen')

figure(3);plot(mean_dif_sa(:,3),'-*','LineW',2);grid on
xlabel('runs');title('diff�rence de Sa moyen')

figure(4);plot(mean_abs_dif_sa(:,3),'-*','LineW',2);grid on
xlabel('runs');title('abs de diff�rence de Sa moyen')

dif_concat_s=sort(dif_concat(:,3));

figure(5);plot(dif_concat_s,'LineW',2);grid on
xlabel('hac');title('diff�rence de Sa moyen sur tous les hac')

figure(6);plot(dif_concat_s,(1:length(dif_concat_s))/length(dif_concat_s),'-*');grid on
figure(7);plot(dif_concat(:,3),'-*','LineW',2);grid on
xlabel('hac');title('diff�rence de Sa moyen par hac')

figure(8);plot(sa0_concat(:,3),'-*','LineW',2);hold on;plot(sa1_concat(:,3),'r-*','LineW',2);hold off;grid on
xlabel('hac');title('Sa moyen par hac')