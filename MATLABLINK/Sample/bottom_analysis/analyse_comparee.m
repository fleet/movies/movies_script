irun=28;

disp(['run' num2str(irun)])
freq_ER60=[18 38 70 120 200]*1000;
i_freq=2;

num_run='000';
num_run(end-length(num2str(irun))+1:end)=num2str(irun);


% brut
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
typ=0;
typ_name='Brut';
ap_dos='brut';
ap='';

%recueille les index des fichiers trait�s
chemin_save=['.\PELGAS09' ap_dos '\RUN' num_run '\EIlay' typ_name '\'];
%chemin_save=['Z:\PELGAS09' ap_dos '\RUN' num_run '\EIlay' typ_name '\'];
filelist = ls([chemin_save,'*.mat']);  % ensemble des fichiers
filtre_fic_pelgas;

analyse;

nb_pf0=nb_pf;
Sa_tot0=Sa_tot;
Sa_tot_filtre0=Sa_tot_filtre;
ind_fic_traite0=ind_fic_traite;
filelist0=filelist;
li_trans0=li_trans;
lD10=lD1;
time0=ltime_ER60;
ldpth0=ldpth;
lheave0=lheave;
lheave_comp0=lheave_comp;
list_freq0=llist_freq;
lbottom_found0=lbottom_found;

% corrige
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
typ=1;
typ_name='Cor';
ap_dos='cor';
ap='_1';

%recueille les index des fichiers trait�s
chemin_save=['.\PELGAS09' ap_dos '\RUN' num_run '\EIlay' typ_name '\'];
%chemin_save=['Z:\PELGAS09' ap_dos '\RUN' num_run '\EIlay' typ_name '\'];
filelist = ls([chemin_save,'*.mat']);  % ensemble des fichiers
filtre_fic_pelgas;

clear ldpth_ini;

analyse;

nb_pf1=nb_pf;
Sa_tot1=Sa_tot;
Sa_tot_filtre1=Sa_tot_filtre;
ind_fic_traite1=ind_fic_traite;
filelist1=filelist;
li_trans1=li_trans;
lD11=lD1;
time1=ltime_ER60;
ldpth1=ldpth;
lheave1=lheave;
lheave_comp1=lheave_comp;
llist_freq1=llist_freq;
lbottom_found1=lbottom_found;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% affichage pf
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
i_seuil=1;
ind_fic_com0=[];
ind_fic_com1=[];
for ific=1:length(ind_fic_traite0)
    if ~isempty(find(str2num(filelist0(ind_fic_traite0(ific),23:28))==str2num(filelist1(ind_fic_traite1,23:28))))
        ind_fic_com0(end+1)=ind_fic_traite0(ific);
        ind_fic_com1(end+1)=ind_fic_traite1(find(str2num(filelist0(ind_fic_traite0(ific),23:28))==str2num(filelist1(ind_fic_traite1,23:28))));
    end
end

%%%% recap nb_pf
%figure(1);plot(nb_pf0(ind_fic_com0,:,i_seuil));hold off;grid on
%title(['seuil ' num2str(i_seuil) ' r�sultat brut run' num_run]);


%figure(2);plot(nb_pf1(ind_fic_com1,:,i_seuil));hold off;grid on
%hold on;plot(nb_pf1(ind_fic_com,10,i_seuil),'r');hold off;grid on
%title(['seuil ' num2str(i_seuil) ' r�sultat cor run' num_run]);


%%%% nb_pf brut vraies fct offset
%figure(3);plot((nb_pf0(ind_fic_com,:,i_seuil)-repmat(nb_pf1(ind_fic_com,1,i_seuil),1,10)).','*');hold off;grid on
%title(['seuil ' num2str(i_seuil) ' nombre de vraies pf fct de l offset']);

%%%% nb_pf brut vraies fct offset
figure(3);plot((nb_pf0(ind_fic_com0,:,i_seuil)-repmat(nb_pf1(ind_fic_com1,1,i_seuil),1,10)),'-*','LineW',2);hold off;grid on
title([' nombre de vraies pf pr chq offset']);
xlabel('fichiers hac')
ylabel('nb pf')

%%%% distrib vraies nb_pf brut fct offset
for i_couche=1:10
    nb_uns=nb_pf0(ind_fic_com0,i_couche,i_seuil)-nb_pf1(ind_fic_com1,1,i_seuil);
    nb_s=sort(nb_uns);
    length_nb_s=length(nb_s);
    min_nb_pf(i_couche)=nb_s(1);
    max_nb_pf(i_couche)=nb_s(end);
    med_nb_pf(i_couche)=median(nb_s);
    quant_20(i_couche)=nb_s(ceil(0.1*length_nb_s));
    quant_80(i_couche)=nb_s(ceil(0.9*length_nb_s));
end
figure(4);hold off;plot(min_nb_pf,'k*','LineW',2);grid on
hold on;plot(max_nb_pf,'k*','LineW',2);plot(med_nb_pf,'rd','LineW',2);plot(quant_20,'r^','LineW',2);plot(quant_80,'rv','LineW',2);hold off
title(['(min, 10%, med, 90%, max) nb pf\_br-pf\_cor1 fct de l offset, sur ' num2str(length(ind_fic_com0)) ' fichiers hac']);
xlabel('offset');ylabel('nombre de pf')

%%%% nombre de fichiers avec vraies pf
disp('nombre de fichiers bruts avec pf')
nb_fic_pf=sum((nb_pf0(ind_fic_com0,:,i_seuil)-repmat(nb_pf1(ind_fic_com1,1,i_seuil),1,10)>0),1)

disp('nombre total de fichiers brut')
nb_fic=length(ind_fic_com0)

%nb tot de pf en fct du seuil
disp('nombre total de pf en fonction du seuil')
sum(max(0,nb_pf0(ind_fic_com0,:,i_seuil)-repmat(nb_pf1(ind_fic_com1,1,i_seuil),1,10)))

nb_p=[];
for ific=1:length(ind_fic_com1) nb_p(ific)=length(Sa_tot1{ind_fic_com1(ific)});end
disp('nombre total de pings')
sum(nb_p)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% affichage Sa
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%% calage des Sa
lind_sab2c=[];
for ific=1:length(ind_fic_com0)
    %le brut reste la ref
    ind_sab2c=[];
    t0=time0{ind_fic_com0(ific)};
    t1=time1{ind_fic_com1(ific)};
    
    %calage initial
    if t0(1)>=t1(1)
        ind1=find(t1==t0(1));
        if isempty(ind1) disp('*** Pb calage t1 ******');end
        ind_sab2c(1)=ind1;
        iti=2;
        ind1=ind1+1;
    else
        iti=find(t0==t1(1));
        if isempty(iti) 
            iti=find(t0==t1(2));
            if isempty(iti) disp('*** Pb calage t1 bis ******');end
            ind_sab2c(1:iti)=2;
            iti=iti+1;
            ind1=3;
        else
            ind_sab2c(1:iti)=1;
            iti=iti+1;
            ind1=2;
        end
    end
    %calage suite
    for it=iti:length(t0)
       if (ind1<=length(t1))
            if (t0(it)==t1(ind1))
                ind_sab2c(it)=ind1;
                ind1=ind1+1;
            elseif (t0(it)>t1(ind1))
                for ip=1:10
                    if ind1+ip>length(t1)
                        ind1=ind1+ip;
                        break;
                    elseif (t0(it)==t1(ind1+ip))
                        ind_sab2c(it)=ind1+ip;
                        ind1=ind1+ip+1;
                        break;
                    end
                    if (ip==10) disp(['********* Pb calage ping ' num2str(it)]);end
                end
            else
                ind_sab2c(it)=ind1-1;
            end
       end
    end
    lind_sab2c{ific}=ind_sab2c;
end



dsa_hac=[];
mean_sa0=[];
mean_sa1=[];
part_sa_eq1p=[];
% on prend comme ref le Sa_cor couche3
for ific=1:length(ind_fic_com0)
    D10=lD10{ind_fic_com0(ific)};
    D11=lD11{ind_fic_com1(ific)};
    sat0=Sa_tot0{ind_fic_com0(ific)};
    sat1=Sa_tot1{ind_fic_com1(ific)};
    ind_sab2c=lind_sab2c{ific};
    heave_comp=lheave_comp0{ind_fic_com0(ific)};
    heave_comp=heave_comp(:,li_trans0(ind_fic_com0(ific)));
    i0_filtre=find(D10(1:length(ind_sab2c),li_trans0(ind_fic_com0(ific)))>10 & D10(1:length(ind_sab2c),li_trans0(ind_fic_com0(ific)))<1000);

    % rajout filtrage des heave importants
    i2=find(sat0(10,i0_filtre)>10^6);
    %excl=[];
    i0_filtreb=i0_filtre;
    for i3=1:length(i2)
        if ~isempty(find(heave_comp(max(1,i0_filtre(i2(i3))-30):min(i0_filtre(i2(i3))+30,length(heave_comp)))>3))
            %excl(end+1)=i0_filtre(i2(i3));
            i0_filtreb=i0_filtreb(find(i0_filtreb~=i0_filtre(i2(i3))));
        end
    end
    i0_filtre=i0_filtreb;
    
    ind_sab2c_sav=ind_sab2c;
    ind_sab2c=ind_sab2c(i0_filtre);
    dsa_hac(ific,1:10,1:length(ind_sab2c))=(1+sat0(:,i0_filtre))./repmat((1+sat1(3,ind_sab2c)),10,1); %rapport des sa
    %mean_sa0(ific,:)=mean(sat0(:,1:length(ind_sab2c)),2).'; %sa sur le hac
    mean_sa0(ific,:)=mean(sat0(:,i0_filtre),2).'; %sa sur le hac
    mean_sa1(ific,:)=mean(sat1(:,ind_sab2c),2).'; %sa sur le cor
    for ic=1:10
        rap_s=sort(squeeze(dsa_hac(ific,ic,1:length(ind_sab2c))));
        %part_sa_eq1p(ific,ic)=(min(find(rap_s>1.01))-length(find(rap_s<1.01))+1)/length(rap_s);
        part_sa_eq1p(ific,ic)=(length(rap_s)-length(find(rap_s>1.03))-length(find(rap_s<1-0.03)))/length(rap_s);
    end
    %dsa_hac(ific,1:10,1:length(ind_sab2c))=sat0(:,1:length(ind_sab2c))-repmat((sat1(1,ind_sab2c)),10,1);
    %dsa_hac{ific}=sat0(:,1:length(ind_sab2c))-repmat((sat1(1,ind_sab2c)),10,1);
    prop_filtre(ific)=(length(ind_sab2c_sav)-length(ind_sab2c))/length(ind_sab2c_sav);
end

disp('nombre total de pings filtres')
sum(nb_p.*(1-prop_filtre))

disp('nombre �quivalent de fichiers hac trait�s')
sum(1-prop_filtre)

%figure(10);plot(mean_sa0./repmat(mean_sa1(:,3),1,10));grid on
%title('rapport brut/cor-offset3 des sa-offset moy')
figure(11);plot(mean_sa1(:,3),'-*','LineW',2);hold on;plot(mean_sa0,'LineW',2);hold off;grid on
title('sa moyen');xlabel('fichiers hac')
legend('sa-cor3 moy','sa-brut moy pour chq offset')
figure(12);plot(part_sa_eq1p,'*-','LineW',2);grid on;
legend
title('proportion des sa-brut diff�rant de moins de 3% de sa-cor-offset3');xlabel('fichiers hac')
figure(14);plot(mean_sa0-repmat(mean_sa1(:,3),1,10),'*-','LineW',2);hold off;grid on
title('diff�rence sa moyen brut - sa-cor3 moyen');xlabel('fichiers hac')

figure(13);plot(prop_filtre,'-*','LineW',2);title('proportion de donn�es filtr�es'); grid on
xlabel('fichiers hac')
% figure(12);plot(part_sa_eq1p(:,2));hold on;plot(sum_sa0(:,2)./sum_sa1(:,1),'r');hold off;grid on
% legend('proportion des sa-brut-offset2 diff�rant de moins de 1% de sa-cor-offset1','rapport brut/cor des sa-offset2/offset1 somm�s')
%figure(10);plot(dsa_hac(:,1,:),'*');
% 
% %rapport des Sa somm�s sur les hac
% disp('rapport des Sa somm�s sur les hac')
% sum(sum_sa0,1)/sum(sum_sa1(:,3))
% sum(sum_sa1(:,3))

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% affichage dif depth
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%calage
clear stat_ddpth nb_sde_cor nb_sde;
ddpth_concat=[];
for ific=1:length(ind_fic_com0)
    dpth_m0=ldpth0{ind_fic_com0(ific)};
    dpth_m1=ldpth1{ind_fic_com1(ific)};
    heave_m0=lheave0{ind_fic_com0(ific)};
    heave_m1=lheave1{ind_fic_com1(ific)};
    bottom_found0=lbottom_found0{ind_fic_com0(ific)};
    
    dif_l=size(dpth_m0,1)-size(dpth_m1,1);
    clear ind_transf
    if (dif_l<0)
        ind0=min(find(abs(heave_m1-heave_m0(1))<0.02));
        ind_transf(1)=ind0;
        for it=2:length(heave_m0)
            if abs(heave_m1(ind0+1)-heave_m0(it))<0.02
                ind_transf(it)=ind0+1;
                ind0=ind0+1;
            elseif abs(heave_m1(ind0+2)-heave_m0(it))<0.02
                ind_transf(it)=ind0+2;
                ind0=ind0+2;
            else
                disp('********** Pb calage dpth 1');
            end
        end
        dpth_m1=dpth_m1(ind_transf,:);
        bottom_found1=bottom_found1(ind_transf,:);
    elseif (dif_l>0)
        ind0=min(find(abs(heave_m0-heave_m1(1))<0.02));
        ind_transf(1)=ind0;
        for it=2:length(heave_m1)
            if abs(heave_m0(ind0+1)-heave_m1(it))<0.02
                ind_transf(it)=ind0+1;
                ind0=ind0+1;
            elseif abs(heave_m0(ind0+2)-heave_m1(it))<0.02
                ind_transf(it)=ind0+2;
                ind0=ind0+2;
            elseif abs(heave_m0(ind0+3)-heave_m1(it))<0.02
                ind_transf(it)=ind0+3;
                ind0=ind0+3;
            else
                disp('********** Pb calage dpth 2');
            end
        end
        dpth_m0=dpth_m0(ind_transf,:);
        bottom_found0=bottom_found0(ind_transf,:);
    elseif (dif_l==0)
        it=size(dpth_m0,1);
        while it>0
            if (abs(heave_m0(it)-heave_m1(it))<0.02)
            elseif (it>1)
                if (abs(heave_m0(it-1)-heave_m1(it))<0.02)
                    dpth_m0=[dpth_m0(1:it-1,:);dpth_m0(it+1:end,:)];
                    heave_m0=[heave_m0(1:it-1),heave_m0(it+1:end)];
                    dpth_m1=dpth_m1(2:end,:);
                    heave_m1=heave_m1(2:end);
                    bottom_found0=[bottom_found0(1:it-1), bottom_found0(it+1:end)];
                elseif (abs(heave_m0(it)-heave_m1(it-1))<0.02)
                    dpth_m1=[dpth_m1(1:it-1,:);dpth_m1(it+1:end,:)];
                    heave_m1=[heave_m1(1:it-1),heave_m1(it+1:end)];
                    dpth_m0=dpth_m0(2:end,:);
                    heave_m0=heave_m0(2:end);
                    bottom_found0=bottom_found0(2:end);
                else
                    dpth_m0=[dpth_m0(1:it-1,:);dpth_m0(it+1:end,:)];
                    heave_m0=[heave_m0(1:it-1),heave_m0(it+1:end)];
                    bottom_found0=[bottom_found0(1:it-1), bottom_found0(it+1:end)];
                    dpth_m1=[dpth_m1(1:it-1,:);dpth_m1(it+1:end,:)];
                    heave_m1=[heave_m1(1:it-1),heave_m1(it+1:end)];
                    disp('******** Pb calage 3');
                end
            end
            it=it-1;
        end
    end
   
    if (length(dpth_m0)==0)
        'lu'
    end
    
    d_dpth=dpth_m0(:,li_trans0(ind_fic_com0(ific)))-dpth_m1(:,li_trans1(ind_fic_com1(ific)));
    d_dpth_s=sort(d_dpth(find(bottom_found0(:,li_trans0(ind_fic_com0(ific)))==1)));
    ddpth_concat=[ddpth_concat; d_dpth_s];
    if ~isempty(d_dpth_s)
        stat_ddpth(ific,1)=d_dpth_s(1);%min
        stat_ddpth(ific,2)=d_dpth_s(ceil(0.1*length(d_dpth_s)));%20%
        stat_ddpth(ific,3)=d_dpth_s(ceil(0.5*length(d_dpth_s)));%med
        stat_ddpth(ific,4)=d_dpth_s(ceil(0.9*length(d_dpth_s)));%80%
        stat_ddpth(ific,5)=d_dpth_s(end);%max
    end
    
    nb_sde_cor(ific)=length(find(d_dpth_s~=0));
    nb_sde(ific)=length(d_dpth_s);
end

figure(20);plot(stat_ddpth,'-*','LineW',2);title('min,10%,median,90% et max des diff�rence de sondes brut/cor');grid on
xlabel('fichiers hac');ylabel('prof\_brut-prof\_cor')

ddpth_concat_s=sort(ddpth_concat);
figure(21);plot(ddpth_concat_s,'-','LineW',2);title('diff�rence de sondes brut/cor');grid on;xlabel('pings');ylabel('prof\_brut-prof\_brut')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% affichage dif depth ini/cor
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%calage
if exist('ldpth_ini')
    clear stat_ddpth_ini nb_sde_cor nb_sde ;
    ddpth_concat=[];
    
    for ific=1:length(ind_fic_com1)
        dpth_m_ini=ldpth_ini{ind_fic_com1(ific)};
        dpth_m1=ldpth1{ind_fic_com1(ific)};
        bottom_found_ini=lbottom_found_ini{ind_fic_com1(ific)};
        
        dif_l=size(dpth_m_ini,1)-size(dpth_m1,1);
        
        if (dif_l~=0)
           disp('********** Pb calage dpth 4');
        end

        d_dpth=dpth_m_ini(:,li_trans1(ind_fic_com1(ific)))-dpth_m1(:,li_trans1(ind_fic_com1(ific)));
        d_dpth_s=sort(d_dpth(find(bottom_found_ini(:,li_trans1(ind_fic_com1(ific)))==1)));
        ddpth_concat=[ddpth_concat ; d_dpth_s];
        if ~isempty(d_dpth_s)
            stat_ddpth_ini(ific,1)=d_dpth_s(1);%min
            stat_ddpth_ini(ific,2)=d_dpth_s(ceil(0.1*length(d_dpth_s)));%20%
            stat_ddpth_ini(ific,3)=d_dpth_s(ceil(0.5*length(d_dpth_s)));%med
            stat_ddpth_ini(ific,4)=d_dpth_s(ceil(0.9*length(d_dpth_s)));%80%
            stat_ddpt_inih(ific,5)=d_dpth_s(end);%max
        end
        nb_sde_cor(ific)=length(find(d_dpth_s~=0));
        nb_sde(ific)=length(d_dpth_s);
    end

    figure(22);plot(stat_ddpth_ini,'*-','LineW',2);title('min,10%,median,90% et max des diff�rence de sondes haclab/cor');grid on
    xlabel('fichiers hac');ylabel('prof\_haclab-prof\_cor')
    disp('corrig� � partir du haclab')

    ddpth_concat_s=sort(ddpth_concat);
    figure(23);plot(ddpth_concat_s,'-','LineW',2);title('diff�rence de sondes haclab/cor');grid on;xlabel('pings');ylabel('prof\_haclab-prof\_brut')
else
    disp('corrig� � partir du brut')
end

%nb sondes corrig�es
disp('proportion de sondes corrig�es')
sum(nb_sde_cor)/sum(nb_sde)


if (0)
    ific=1;
sat0=Sa_tot0{ind_fic_com0(ific)};
sat1=Sa_tot1{ind_fic_com1(ific)};
ind_sab2c=lind_sab2c{ific};
figure(50);plot(squeeze(sat0(1,1:length(ind_sab2c))).');hold on;plot(squeeze(sat1(1,ind_sab2c)).','k');hold off

nom_fic=filelist0(ind_fic_com0(1),1:28)
end

