nom_fic='PELGAS09_009_20090504_042810';  %fichier de base � analyser

%% traitement du fichier hac corrig�
if (str2num(num_run)>4 & str2num(num_run)~=26 & str2num(num_run)~=23 & str2num(num_run)~=27)
    ap='_1';
else
    ap='';
end
nom_fic_cor=[nom_fic ap '.hac'];
chemin_save=['.\PELGAS09cor\RUN' str_run(ir,:) '\EIlayCor'];
chemin_hac=['K:\PELGAS09\HAC-MOVIES\HacCorPel09\RUN' str_run(ir,:) '\'];

i_freq=2;
%freq_ER60=[18 38 70 120 200]*1000;

[Sa_totCor,Sa_surfER60Cor,Sa_botER60Cor,Sv_botER60Cor,Depth_botER60Cor,time_ER60Cor,seuil_pf,i_pfCor,nb_pfCor,...
    utCor,uCor,ind_u2svCor,ind_sv2uCor,time_pingCor,dpth_echCor,heave_comp_echCor,min_echCor,bt_compCor,bt_simCor,beamsSamplesSpacing,freq_ER60]...
    =detail_pf(0,i_freq,nom_fic_cor,chemin_hac,chemin_save);

%% traitement du fichier hac brut
ap='';
nom_fic_brut=[nom_fic ap '.hac'];
chemin_save=['.\PELGAS09brut\RUN' str_run(ir,:) '\EIlayBrut\'];
chemin_hac=['K:\PELGAS09\HAC-MOVIES\HacBrutPel09\RUN' str_run(ir,:) '\'];

i_freq=2;
%freq_ER60=[18 38 70 120 200]*1000;

[Sa_tot,Sa_surfER60,Sa_botER60,Sv_botER60,Depth_botER60,time_ER60,seuil_pf,i_pf,nb_pf,...
    ut,u,ind_u2sv,ind_sv2u,time_ping,dpth_ech,heave_comp_ech,min_ech,bt_comp,bt_sim,beamsSamplesSpacing,freq_ER60]...
    =detail_pf(0,i_freq,nom_fic_brut,chemin_hac,chemin_save);


%% il faut recaler le u brut et corrige...

if size(u,2)>size(uCor,2)
    dec_cor=size(u,2)-size(uCor,2);
    ad_cor=zeros(1,dec_cor);
elseif size(u,2)==size(uCor,2) %brut et corrige sont de meme taille (corrige issu du brut et pas du haclab?)
    dec_cor=0;
    ad_cor=[];
end
if (sum(time_ping(1+dec_cor:end)-time_pingCor)~=0)
    disp('*****************Pb calage u uCor********************'); 
end


%% %%%%%%%%%%%%%%%%%%%%
% principe d'affichage des couches: si le centre du pixel est sous la
% ligne, il est affect� � la couche
if (1) %affichage
ifig=50;
figure(ifig);DrawEchoGram_brut(ut,0,-70);colorbar;grid on;
title([num2str(freq_ER60(i_freq)/1000) ' kHz'])
figure(ifig);hold on;plot(dpth_ech+1.5+heave_comp_ech-min_ech,'k','LineW',2);hold off

% on affiche la couche voulue
i_couche=3;
figure(ifig);hold on;plot(dpth_ech+1.5+heave_comp_ech-min_ech-i_couche*0.1/beamsSamplesSpacing,'c','LineW',2);hold off

% on affiche les PF pour le seuil voulu
i_seuil_niv=1;
figure(ifig);hold on;plot(ind_sv2u(i_pf{i_couche,i_seuil_niv}),(1:length(i_pf{i_couche,i_seuil_niv}))*0+bt_comp(ind_sv2u(i_pf{i_couche,i_seuil_niv}))-10,'k*','LineW',10);hold off

%%%%%%%%%corrige

figure(ifig);hold on;plot([ad_cor,dpth_echCor]+1.5+heave_comp_ech-min_ech,'m','LineW',2);hold off

% on affiche la couche voulue
figure(ifig);hold on;plot([ad_cor,dpth_echCor]+1.5+heave_comp_ech-min_ech-i_couche*0.1/beamsSamplesSpacing,'c','LineW',2);hold off

% on affiche les PF pour le seuil voulu
figure(ifig);hold on;plot(ind_sv2uCor(i_pfCor{i_couche,i_seuil_niv})+dec_cor,(1:length(i_pfCor{i_couche,i_seuil_niv}))*0+bt_compCor(ind_sv2uCor(i_pfCor{i_couche,i_seuil_niv})+dec_cor)-10,'m*','LineW',5);hold off
xlabel(['seuil ' num2str(i_seuil_niv) ' couche ' num2str(i_couche)]);

end

%% exemple checking Sv/fond
% pixel d'index d'abcisse 775 � l'affichage, 38kHz 
if (0) 
    Sv_botER60Cor(ind_u2svCor(775-dec_cor),:,2)
    Sv_botER60(ind_u2sv(775),:,2)
end