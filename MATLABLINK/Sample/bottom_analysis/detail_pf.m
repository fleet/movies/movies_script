function [Sa_tot,Sa_surfER60,Sa_botER60,Sv_botER60,Depth_botER60,time_ER60,seuil_pf,i_pf,nb_pf,...
    ut,u,ind_u2sv,ind_sv2u,time_ping,dpth_ech,heave_comp_ech,min_ech,bt_comp,bt_sim,beamsSamplesSpacing,freq_ER60]...
    =detail_pf(standalone,i_freq,nom_fic,chemin_hac,chemin_save)

ifig=50;
freq_ER60=[18 38 70 120 200]*1000;

if (standalone==1) %seul
    disp('appel de detail en standalone');
else
    disp('appel de detail par comparatif');
end

disp(' ');
disp(['detail de ' nom_fic ' avec la frequence ' num2str(i_freq)]);
disp(' ');
disp(' ');
pause(2);
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%charge les Sv
load([chemin_save '\' nom_fic(1:end-4) '_1' '_EI']);
i_trans=find(freq_ER60(i_freq)==list_freq);

% calcul du Sa par seuils
% rappel: chaque couche de fond fait 10cm
clear Sa_tot;
for i_seuil=1:10
    Sa_tot(i_seuil,:)=Sa_surfER60(:,1,i_trans)-sum(Sa_botER60(:,1:i_seuil,i_trans),2);
end

% calcul du nombre de prises de fond
clear i_pf nb_pf;
seuil_pf=10.^(5:-1:3); % trois seuils de PF
for i_seuil=1:10
    for i_seuil_niv=1:length(seuil_pf)
        i_pf{i_seuil,i_seuil_niv}=find(Sa_tot(i_seuil,:)>seuil_pf(i_seuil_niv) & ...
                squeeze(Depth_botER60(:,1,i_trans)).'>10 & squeeze(Depth_botER60(:,1,i_trans)).'<1000);
        nb_pf(i_seuil,i_seuil_niv)=sum(Sa_tot(i_seuil,:)>seuil_pf(i_seuil_niv) & squeeze(Depth_botER60(:,1,i_trans)).'>10 & squeeze(Depth_botER60(:,1,i_trans)).'<1000,2);
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%charge les donn�es acoustiques

ParameterKernel= moKernelParameter();
ParameterDef=moLoadKernelParameter(ParameterKernel);

ParameterDef.m_bAutoLengthEnable=1;
ParameterDef.m_MaxRange = 150;
ParameterDef.m_bAutoDepthEnable=1;
ParameterDef.m_bIgnorePhaseData=0;

moSaveKernelParameter(ParameterKernel,ParameterDef);
clear ParameterDef;

ParameterR= moReaderParameter();
ParameterDef=moLoadReaderParameter(ParameterR);

ParameterDef.m_chunkNumberOfPingFan=50;

moSaveReaderParameter(ParameterR,ParameterDef);

FileName = [chemin_hac '\' nom_fic];

moOpenHac(FileName);

% il faudrait d�sactiver l'echo-integration qui fait perdre du temps
%EchoIntegrModule = moEchoIntegration();
%moSetEchoIntegrationDisabled(EchoIntegrModule);

FileStatus= moGetFileStatus;
while FileStatus.m_StreamClosed < 1
    moReadChunk();
    FileStatus= moGetFileStatus();
end;
fprintf('End of File');


%presentation des sondeurs
list_sounder=moGetSounderList;
nb_snd=length(list_sounder);
disp(' ');
disp(['nb sondeurs = ' num2str(nb_snd)]);
for isdr = 1:nb_snd
    nb_transduc=list_sounder(isdr).m_numberOfTransducer;
    if (nb_transduc==5) index_sondeur=list_sounder(isdr).m_SounderId; end
    if (list_sounder(isdr).m_SounderId<10)
        ind_list(list_sounder(isdr).m_SounderId)=isdr;
        disp(['sondeur ' num2str(isdr) ':    ' 'index: ' num2str(list_sounder(isdr).m_SounderId) '   nb trans:' num2str(nb_transduc)]);
        for itr=1:nb_transduc
            disp(['   trans ' num2str(itr) ':    ' 'nom: ' list_sounder(isdr).m_transducer(itr).m_transName '   freq: ' num2str(list_sounder(isdr).m_transducer(itr).m_SoftChannel.m_acousticFrequency/1000) ' kHz']);
        end
    else
        disp('sondeur Id bizarre');
    end
end

for i=1:list_sounder(ind_list(index_sondeur)).m_numberOfTransducer
    if freq_ER60(i_freq)==list_sounder(ind_list(index_sondeur)).m_transducer(i).m_SoftChannel.m_acousticFrequency
        index_trans=i
    end
end

u=[];
bt_sim=[];
heave_m=[];
heave_comp_m=[];
dpth_ech=[];
lat=[];
long=[];
ping_id=[];
bt_found=[];
time_ping=[];
for index= 0:moGetNumberOfPingFan()-1
        MX= moGetPingFan(index);
        SounderDesc =  moGetFanSounder(MX.m_pingId);
        if SounderDesc.m_SounderId == index_sondeur
              SounderDesc_ref=SounderDesc;
              datalist = moGetPolarMatrix(MX.m_pingId,index_trans-1);
              u=[u datalist.m_Amplitude/100];
              bt_sim= [bt_sim,floor( MX.beam_data(index_trans).m_bottomRange/SounderDesc.m_transducer(index_trans).m_beamsSamplesSpacing)];
              dpth_ech=[dpth_ech,MX.beam_data(index_trans).m_bottomRange/SounderDesc.m_transducer(index_trans).m_beamsSamplesSpacing];
              bt_found(end+1)=MX.beam_data(index_trans).m_bottomWasFound;
              heave_m(end+1)=MX.navigationAttitude.sensorHeaveMeter;
              heave_comp_m(end+1)=MX.beam_data(index_trans).m_compensateHeaveMovies;
              lat(end+1)=MX.navigationPosition.m_lattitudeDeg;
              long(end+1)=MX.navigationPosition.m_longitudeDeg;
              ping_id(end+1)=MX.m_pingId;
              time_ping(end+1)=MX.m_meanTimeCPU+MX.m_meanTimeFraction/10000;
        end
end

SounderDesc=SounderDesc_ref;
heave_ech=round(heave_m/SounderDesc.m_transducer(1).m_beamsSamplesSpacing);
heave_comp_ech=round(heave_comp_m/SounderDesc.m_transducer(index_trans).m_beamsSamplesSpacing);

% on compense le pilonnement a l'affichage
max_ech=max(heave_comp_ech);
min_ech=min(heave_comp_ech);
u2=zeros(size(u,1)-min_ech,size(u,2));
for i= 1:size(u,2)
    u2(heave_comp_ech(i)-min_ech+1:heave_comp_ech(i)-min_ech+size(u,1),i)=u(:,i);
end

% on enl�ve les pings manquants dans l'EI (pourquoi en manque-t-il???)
ind_sv2u=zeros(1,size(time_ER60,2));
ind_u2sv=zeros(1,size(u,2));
ind_u2sv(1)=1;
ind_sv2u(1)=1;

for i=1:size(u,2)
    i0=[];
    i0=find(time_ER60==time_ping(i));
    if (isempty(i0))
        ind_u2sv(i)=ind_u2sv(max(1,i-1));
    else
        ind_u2sv(i)=i0;
        ind_sv2u(i0)=i;
    end
end
for i=1:size(time_ER60,2)
    if (ind_sv2u(i)==0)
        ind_sv2u(i)=ind_sv2u(max(1,i-1));
    end
end

%matrice de travail
ut=u2;
beamsSamplesSpacing=SounderDesc.m_transducer(index_trans).m_beamsSamplesSpacing;

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% affichage

figure(ifig);DrawEchoGram_brut(ut,0,-70);colorbar;grid on;
title([num2str(freq_ER60(i_freq)/1000) ' kHz'])
figure(ifig);hold on;plot(dpth_ech+1.5+heave_comp_ech-min_ech,'k','LineW',2);hold off

% on affiche la couche voulue
i_couche=2;
figure(ifig);hold on;plot(dpth_ech+1.5+heave_comp_ech-min_ech-i_couche*0.1/beamsSamplesSpacing,'c','LineW',2);hold off

% on affiche les PF pour le seuil voulu
i_seuil_niv=1;
bt_comp=bt_sim+heave_comp_ech-min_ech;
figure(ifig);hold on;plot(ind_sv2u(i_pf{i_couche,i_seuil_niv}),(1:length(i_pf{i_couche,i_seuil_niv}))*0+bt_comp(ind_sv2u(i_pf{i_couche,i_seuil_niv}))-10,'k*','LineW',5);hold off
xlabel(['seuil ' num2str(seuil_pf(i_seuil_niv)) ' couche ' num2str(i_couche)]);
