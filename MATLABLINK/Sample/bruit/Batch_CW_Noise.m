clear
close all
mydir  = pwd;
idcs   = strfind(mydir,'\')
pathUseful = [mydir(1:idcs(end)-1) '\EK80\Codes Lectures de Base'];

addpath(genpath(pathUseful)) 

typ='EK80';
FileName=[];
FileName{end+1}='C:\Users\lberger\Desktop\Essais Antéa\Noise\ANTEATEST-D20220315-T163124.raw'; % 10 noeuds
FileName{end+1}='C:\Users\lberger\Desktop\Essais Antéa\Noise\ANTEATEST-D20220315-T164411.raw'; % 8 noeuds
FileName{end+1}='C:\Users\lberger\Desktop\Essais Antéa\Noise\ANTEATEST-D20220315-T164748.raw'; % 6 noeuds
FileName{end+1}='C:\Users\lberger\Desktop\Essais Antéa\Noise\ANTEATEST-D20220315-T165814.raw'; % 4 noeuds
FileName{end+1}='C:\Users\lberger\Desktop\Essais Antéa\Noise\ANTEATEST-D20220315-T170545.raw'; % 2 noeuds
FileName{end+1}='C:\Users\lberger\Desktop\Essais Antéa\Noise\ANTEATEST-D20220315-T171016.raw'; % 0 noeuds



pas=[8 6.5 4.7 3 0];
vit_fd=[10 8 6 4 2 0];

for i=1:length(FileName)
    [Pel_sim_ping{i},Pel_ping{i},Pel2Pac,P_an(i,:),Pel(i,:),Pel_sim(i,:),portee(i,:),param]=calc_bruit_portee_mono_CW(FileName{i},typ) ;
end

figure;plot(pas,Pel)