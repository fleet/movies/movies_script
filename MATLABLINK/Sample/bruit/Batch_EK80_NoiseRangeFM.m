mydir  = pwd;
idcs   = strfind(mydir,'\')
pathEK80 = [mydir(1:idcs(end)) 'EK80\Codes Lectures de Base'];

addpath(genpath(pathEK80)) 

[RawData] = ReadEK80Data;


df=1000; %Hz
Sv_thr0=-70; %dB
TrList_File='C:\Users\lberger\Desktop\TrList_calibration_apres_correction.xml';%calibration file
depth_transducteur=7;

nchannel=size(RawData.Data.PingData,1);
freq_tot=[];
Sv_tot=[];
for ichannel=3:nchannel
    [freq_cal, gain_cal]=read_cal_channel_EK80(RawData,TrList_File,ichannel);
    [dsp_noise_ini,freq,DSP_AN,range_Sv,ifreqB,Sv,r]=EK80_NoiseRangeFM(RawData,ichannel,df,Sv_thr0,freq_cal, gain_cal);
    %[dsp_noise_ini,freq,DSP_AN,range_Sv,ifreqB,Sv,r]=EK80_NoiseRangeFM(RawData,ichannel,df,Sv_thr0);    
    freq_tot=[freq_tot freq];
    Sv_tot=[Sv_tot Sv];
    figure(10);plot(freq(ifreqB)/1000,10*log10(dsp_noise_ini(ifreqB)));hold on
    figure(11);plot(freq(ifreqB)/1000,-range_Sv(ifreqB),'b');hold on;
    figure(12);imagesc(freq/1000,r+depth_transducteur,Sv);hold on;[c, h] = contour(freq/1000,r+depth_transducteur,Sv,[-75 -65 -55],'w'); clabel(c, h);
end
figure(10);hold off
grid on;xlabel('freq kHz');ylabel('dB');title('Noise Power Spectral Density dB ref 1W/Hz')
figure(11);hold off;
grid on;xlabel('freq kHz');ylabel('range m');title(['range where noise induces Sv(f)=' num2str(median(Sv_thr0)) 'dB'])


figure(12);hold off;caxis([-100 -20]);colorbar;xlim([45 450])
xlabel('kHz');ylabel('range m');title('Sv(f) dB');