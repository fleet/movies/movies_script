function [dsp_noise_ini,freq,DSP_AN,range_Sv,ifreqB,Sv,r]=EK80_NoiseRangeFM(RawData,itr,ichannel,df,Sv_thr0,varargin)

% varargin:freq_cal,gain_cal: optionnel, pour renseigner des gains d'�talonnage (dB) en fonction de la fr�quence (Hz)

nchannel=size(RawData.Data.PingData,1);
ichannelPD=max(itr,ichannel); %bidouille

if strcmp(RawData.Data.ConfigurationData.Transceivers(itr).TransceiverType,'GPT')
    uiwait(msgbox('GPT channel: stop processing spectrum. Choose WBT channel'));
    return;
end

%d�termination �mission s�quentielle ou simultan�e
if nchannel==1
    seq=0; 
elseif isempty(RawData.Data.PingData(1,1).SampleData) | isempty(RawData.Data.PingData(2,1).SampleData)
    seq=1;
else
    seq=0;
end
if seq==0
    ipdec=1;
    ip0=1;
else
    ipdec=nchannel;
    ip0=1;
    while isempty(RawData.Data.PingData(ichannelPD,ip0).SampleData)
        ip0=ip0+1;
    end
end

if (~RawData.Data.PingData(ichannelPD,ip0).ParameterData.pulseForm) %CW
    uiwait(msgbox('CW mode. Stop processing'));
    return;
end

%param�tres EK80
ipref=1;
nSectors = size(RawData.Data.PingData(ichannelPD,ip0+(ipref-1)*ipdec).SampleData.complexSamples,2);
nominalTransducerImpedance  = 75;
wbtImpedanceRx              = 5e3;
fe=1/RawData.Data.PingData(ichannelPD,ip0+(ipref-1)*ipdec).ParameterData.sampleInterval;


f1=RawData.Data.PingData(ichannelPD,ip0+(ipref-1)*ipdec).ParameterData.frequencyStart;
f2=RawData.Data.PingData(ichannelPD,ip0+(ipref-1)*ipdec).ParameterData.frequencyStop;
T=RawData.Data.PingData(ichannelPD,ip0+(ipref-1)*ipdec).ParameterData.pulseLength;% pulse parameters...
if (0) %compute maximum range with other pulse parameters 
    %f1=260*1000;
    %f2=280*1000;
    T=2048e-6;
end
B=f2-f1;
soundSpeed=RawData.Data.PingData(ichannelPD,ip0+(ipref-1)*ipdec).EnvironmentData.soundSpeed;

if (0) %set Nfft
    Nfft=1024;
    df=fe/Nfft;
else %set df
    Nfft=round(fe/df);
    df=fe/Nfft;
end
Tfft=Nfft/fe;
figure(9);
if (0)
    % determination of reception time (conservative)
    sig=20*log10(abs(RawData.Data.PingData(ichannelPD,ip0).SampleData.complexSamples(1:end,1)));
    medianS=median(sig);
    sig2=filter(ones(1,100)/100,1,sig);
    ech1=2*(99+min(find(sig2<medianS)));
    ech2=size(RawData.Data.PingData(ichannelPD,ip0).SampleData.complexSamples,2);
    p1=1;
    np=floor(size(RawData.Data.PingData,2)/ipdec);
    p2=p1+np-1;
else
    np=floor(size(RawData.Data.PingData,2)/ipdec);
    for ip=1:np
        nech=size(RawData.Data.PingData(ichannelPD,ip0+(ip-1)*ipdec).SampleData.complexSamples,1);
        im(ip,1:nech)=20*log10(abs(RawData.Data.PingData(ichannelPD,ip0+(ip-1)*ipdec).SampleData.complexSamples(1:nech,1)));
    end
    figure(9);imagesc(im.');xlabel('ping');ylabel('echantillons');title(['energie dB quadrant 1 '  RawData(1).Data.ConfigurationData.Transceivers(itr).Channels(ichannel).ChannelIdShort ])
    %range d'evaluation
    ech1=input('index du 1er �chantillon � prendre en compte:');
    ech2=input('index du dernier �chantillon � prendre en compte:');
    
    %index de pings
    p1=input('index du 1er ping � prendre en compte:');
    p2=input('index du dernier ping � prendre en compte:');
    np=p2-p1+1;
end


% raw noise spectrum
fft_all2=zeros(Nfft,np);

for ip=1:np
    data4=RawData.Data.PingData(ichannelPD,ip0+(p1+ip-2)*ipdec).SampleData.complexSamples(ech1:ech2,:);
    amp_raw=sum(data4,2)*(wbtImpedanceRx+nominalTransducerImpedance)/wbtImpedanceRx/(2*sqrt(2*nominalTransducerImpedance*nSectors));
    amp_raw_buf=buffer(amp_raw,Nfft);
    fft_all=fft(amp_raw_buf);
    fft_all2(:,ip)=mean(abs(fft_all(:,1:end-1).^2),2);
end
dsp_noise_ini=mean(fft_all2,2)/(fe^2*Tfft);


%frequency index
f2per=rem(f2,fe);
f1per=rem(f1,fe);
if f1per<f2per
    nper=floor(f1/fe);
    freq=(0:Nfft-1)/Nfft*fe+nper*fe;
else
    freqper=(0:Nfft-1)/Nfft*fe;
    ind_out=find(freqper>f2per & freqper<f1per);
    if ~isempty(freqper)
        iout0=ind_out(ceil(length(ind_out)/2));
    else
        iout0=max(freqper<f1per)+1;
    end
    nper=floor(f1/fe);
    freq=[freqper(1:iout0-1)+(nper+1)*fe freqper(iout0:end)+nper*fe];
end

%frequency reordering
[freq,indordf]=sort(freq);
nfreq=Nfft;
dsp_noise_ini=dsp_noise_ini(indordf);


figure(1);plot(freq/1000,10*log10(dsp_noise_ini));grid on;ylim([-220 -150]);xlim([0 500])
xlabel('kHz');ylabel('dB ref 1W');title('Noise Power Spectral Density dB ref 1W/Hz')

ifreqB=find(freq>=f1 & freq <=f2);
figure(1);hold on;plot(freq(ifreqB)/1000,10*log10(dsp_noise_ini(ifreqB)),'r');hold off

%Electric Noise
P_noise_ini=10*log10(sum(dsp_noise_ini(ifreqB))*df)
P_noise_MF=P_noise_ini-10*log10(B*T);
dsp_noise_MF=dsp_noise_ini/(B*T);


%Noise CW
list_fr=[18 38 70 120 200 333]*1000;
%f0=RawData.Data.PingData(ichannelPD,ip0+(ipref-1)*ipdec).ParameterData.frequencyCenter;
f0=str2num(RawData.Data.ConfigurationData.Transceivers(itr).Channels(ichannel).Transducer.Frequency);
fCW=list_fr(find(abs(f0-list_fr)==min(abs(f0-list_fr))));
BCW=1500; %Hz
ifreqCW=find(freq>=fCW-BCW/2 & freq <=fCW+BCW/2);
NoiseCW=10*log10(sum(dsp_noise_ini(ifreqCW))*df)

figure(1);hold on;plot(freq(ifreqCW)/1000,10*log10(dsp_noise_ini(ifreqCW)),'g');hold off

% EK80 parameters
transmitPower=RawData.Data.PingData(ichannelPD,ip0+(ipref-1)*ipdec).ParameterData.transmitPower; %W
soundSpeed=RawData.Data.PingData(ichannelPD,ip0+(ipref-1)*ipdec).EnvironmentData.soundSpeed;
clear absorptionCoefficients
for i=1:length(freq)
    absorptionCoefficients(i) = EstimateAbsorptionCoefficients(RawData.Data.PingData(ichannelPD,ip0+(ipref-1)*ipdec).EnvironmentData,freq(i));
end
lambda=soundSpeed./freq;
if nargin==5
    %%%%%%%%%%%%%%%%%%% l'estimation effectu�e ci-dessous des gain(f) � appliquer devra �tre remplac�e par les gain(f) lus dans le fichier d'�talonnage (interpol�s aux fr�quences freq)
%     gainTable       = str2num(RawData.Data.ConfigurationData.Transceivers(ichannel).Channels.Transducer.Gain);
%     gain = gainTable(end)+20*log10(freq/str2num(RawData.Data.ConfigurationData.Transceivers(ichannel).Channels.Transducer.Frequency));
    gainTable       = str2num(RawData.Data.ConfigurationData.Transceivers(itr).Channels(ichannel).Transducer.Gain);
    gain = gainTable(end)+20*log10(freq/str2num(RawData.Data.ConfigurationData.Transceivers(itr).Channels(ichannel).Transducer.Frequency));

else
    freq_cal=varargin{1};
    gain_cal=varargin{2};
    
    %gain moyen pour la resolution demandee
    df_cal=median(freq_cal(2:end)-freq_cal(1:end-1))/2;
    nfilt=round(df/df_cal);
    
    if nfilt>0
        freq_interp=(freq_cal(1):df_cal:freq_cal(end));
        if freq_interp(end)<freq_cal(end) freq_interp(end+1)=freq_cal(end); end
        gain_cal_interp=interp1(freq_cal,gain_cal,freq_interp); %interpole dans les stop bands
    
        gain_df=(filter(ones(1,nfilt)/nfilt,1,10.^(gain_cal_interp/10)));
        freq_gain_df=freq_interp-df_cal*(nfilt-1)/2;
        gain = 10*log10(interp1(freq_gain_df,gain_df,freq));  
    else
        gain = 10*log10(interp1(freq_cal,10.^(gain_cal/10),freq));  
    end

    %figure;hold on;plot(freq_cal/1000,gain_cal);plot(freq/1000,gain);title('u')
end
effectivePulselLength =Nfft/fe;
%psi=str2num(RawData.Data.ConfigurationData.Transceivers(ichannel).Channels.Transducer.EquivalentBeamAngle)-20*log10(freq/str2num(RawData.Data.ConfigurationData.Transceivers(ichannel).Channels.Transducer.Frequency));
psi=str2num(RawData.Data.ConfigurationData.Transceivers(itr).Channels(ichannel).Transducer.EquivalentBeamAngle)-20*log10(freq/str2num(RawData.Data.ConfigurationData.Transceivers(itr).Channels(ichannel).Transducer.Frequency));
dsp_Pt=nan(1,nfreq);
if 0
    dsp_Pt(ifreqB)=transmitPower/B*T;
else
    [transmitSignal] = CreateTransmitSignal(RawData.Data.PingData(ichannelPD,ip0));
    filt=transmitSignal/norm(transmitSignal)^2;
    MFfilt=xcorr(filt,filt); %filtre MF
    imed=ceil(length(MFfilt)/2);
    fft_filt=fft(MFfilt(max(1,imed-floor((Nfft-1)/2)):min(length(MFfilt),imed+floor(Nfft/2))),Nfft);
    fft_filt=sqrt(abs(fft_filt)).';
    dsp_Pt= transmitPower*(fft_filt(indordf)*norm(transmitSignal)^2).^2/(fe^2*T); 
end
    
r=(1:1:2000);

%% Equivalent ambiant noise
lambda=soundSpeed./freq;
% % A ACTUALISER AVEC GAIN DE CALIBRATION SI DISPONIBLE
% gainTable       = str2num(RawData.Data.ConfigurationData.Transceivers(ichannel).Channels.Transducer.Gain);
% %frequencyNom=str2num(RawData.Data.ConfigurationData.Transceivers(ichannel).Channels.Transducer.Frequency);
% %gain = gainTable(end)+20*log10(freq/frequencyNom);
% %psi=str2num(RawData.Data.ConfigurationData.Transceivers(ichannel).Channels.Transducer.EquivalentBeamAngle)-20*log10(freq/frequencyNom);
% gain = gainTable(end);
%psi=str2num(RawData.Data.ConfigurationData.Transceivers(ichannel).Channels.Transducer.EquivalentBeamAngle);
psi=str2num(RawData.Data.ConfigurationData.Transceivers(itr).Channels(ichannel).Transducer.EquivalentBeamAngle);
DI=-psi+10*log10(41000/5800);

%Pel2Pac(i) =- 10*log10(lambda(i).^2/(4*pi)) - 10*log10(param.Brx(i)) -param.Gain(i) + param.DI(i) +181.8; %CW mono
Pel2Pac =- 10*log10(lambda.^2/(4*pi)) - gain + DI +181.8;
DSP_AN=10*log10(dsp_noise_ini)+Pel2Pac.'; 

figure(2);plot(freq/1000,DSP_AN);grid on;ylim([-20 70]);xlim([0 500])
xlabel('kHz');ylabel('dB');title('Equivalent Ambiant Noise Spectral Density dB ref 1�Pa/Hz')


%% Single target range
%%%%%%%%%%%%%%%%%%%%
TS_thr=zeros(1,nfreq);
TS_thr(:)=-100;
TS_thr_MF=-100;

% Single target spectrum
TS=nan(length(r),nfreq);
for i=1:length(r)
    TS(i,:)=10*log10(dsp_noise_ini*Tfft/T).' +40*log10(r(i))+2*absorptionCoefficients*r(i)-10*log10(dsp_Pt.*lambda.^2/(16*pi^2))-2*gain;
end
%TS=TS-5*log10(B*T); %optimal spectral analysis foir noise reduction
figure(5);imagesc(freq(ifreqB)/1000,r,TS(:,ifreqB));caxis([-90 -20]);colorbar
xlabel('kHz');ylabel('range m');title('TS(f) dB')
hold on;[c, h] = contour(freq(ifreqB)/1000,r,TS(:,ifreqB),[-60 -50 -40],'k'); clabel(c, h);hold off

% spectrum range limit
range_TS=nan(1,nfreq);
for i=1:length(ifreqB)
    indTS=find(TS(:,ifreqB(i))<=TS_thr(ifreqB(i)));
    if ~isempty(indTS)
       range_TS(ifreqB(i))=r(max(indTS));
    end
end
figure(6);plot(freq(ifreqB)/1000,range_TS(ifreqB));grid
xlabel('kHz');ylabel('range m');title(['range where noise induces TS(f)=' num2str(median(TS_thr)) 'dB'])

% MF range limit
%TS_MF=10*log10(mean(10.^(TS(:,ifreqB)/10),2))-5*log10(B*T); %keep -5 for the moment (i.e -10/ini) /BT (MF and compression (compensation of mean over T))
TS_MF=10*log10(mean(10.^(TS(:,ifreqB)/10),2))-10*log10(B*T); 
range_TS_MF=r(max(find(TS_MF<=TS_thr_MF)));
figure(7);plot(r,TS_MF);xlabel('range m');ylabel('TS compressed');grid on;title(['range where noise induces TS=' num2str(TS_thr_MF) 'dB =' num2str(round(range_TS_MF)) 'm'])

%% Volume backscatter 
% %%%%%%%%%%%%%%%%%%%
Sv_thr=zeros(1,nfreq);
Sv_thr(:)=Sv_thr0;
Sv_thr_MF=-100;

Sv=nan(length(r),nfreq);
for i=1:length(r)
    Sv(i,:)=10*log10(dsp_noise_ini*Tfft/T).' +20*log10(r(i))+2*absorptionCoefficients*r(i)-10*log10(dsp_Pt.*lambda.^2*soundSpeed/(32*pi^2))-2*gain-10*log10(effectivePulselLength)-psi;
end
figure(8);imagesc(freq(ifreqB)/1000,r,Sv(:,ifreqB));caxis([-100 -20]);colorbar
xlabel('kHz');ylabel('range m');title('Sv(f) dB')
hold on;[c, h] = contour(freq(ifreqB)/1000,r,Sv(:,ifreqB),[-70 -60 -50],'k'); clabel(c, h);hold off

range_Sv=nan(1,nfreq);
for i=1:length(ifreqB)
    indSv=find(Sv(:,ifreqB(i))<=Sv_thr(ifreqB(i)));
    if ~isempty(indSv)
        range_Sv(ifreqB(i))=r(max(indSv));
    end
end
figure(3);plot(freq(ifreqB)/1000,-range_Sv(ifreqB));grid
xlabel('kHz');ylabel('range m');title(['range where noise induces Sv(f)=' num2str(median(Sv_thr)) 'dB'])

Sv_MF=10*log10(mean(10.^(Sv(:,ifreqB)/10),2));
range_Sv_MF=r(max(find(Sv_MF<=Sv_thr_MF)));
figure(4);plot(r,Sv_MF);xlabel('range m');ylabel('Sv compressed');grid on;title(['range where noise induces Sv=' num2str(Sv_thr_MF) 'dB =' num2str(round(range_Sv_MF)) 'm'])

