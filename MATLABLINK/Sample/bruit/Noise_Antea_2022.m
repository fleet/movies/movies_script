clear all;
close all;

%essais du 15/03/2022


typ='EK80';
FileName=[];
FileName{end+1}='C:\Users\lberger\Desktop\Essais Ant�a\Noise\ANTEATEST-D20220315-T163124.raw'; % 10 noeuds
FileName{end+1}='C:\Users\lberger\Desktop\Essais Ant�a\Noise\ANTEATEST-D20220315-T164411.raw'; % 8 noeuds
FileName{end+1}='C:\Users\lberger\Desktop\Essais Ant�a\Noise\ANTEATEST-D20220315-T164748.raw'; % 6 noeuds
FileName{end+1}='C:\Users\lberger\Desktop\Essais Ant�a\Noise\ANTEATEST-D20220315-T165814.raw'; % 4 noeuds
FileName{end+1}='C:\Users\lberger\Desktop\Essais Ant�a\Noise\ANTEATEST-D20220315-T170545.raw'; % 2 noeuds
FileName{end+1}='C:\Users\lberger\Desktop\Essais Ant�a\Noise\ANTEATEST-D20220315-T171016.raw'; % 0 noeuds



pas=[8 6.5 4.7 3 1.4 0];
vit_fd=[10 8 6 4 2 0];

for i=1:length(FileName)
    [RawData] = ReadEK80Data(FileName{i});
    
  [Pel_sim_ping,Pel_ping,Pel2Pac,P_an,Pel_18(i),Pel_sim,portee18(i),param]=EK80_NoiseRange_CW(RawData,1,1,-70);
[Pel_sim_ping,Pel_ping,Pel2Pac,P_an,Pel_38(i),Pel_sim,portee38(i),param]=EK80_NoiseRange_CW(RawData,2,1,-70);
[Pel_sim_ping,Pel_ping,Pel2Pac,P_an,Pel_70(i),Pel_sim,portee70(i),param]=EK80_NoiseRange_CW(RawData,3,1,-70);
[Pel_sim_ping,Pel_ping,Pel2Pac,P_an,Pel_120(i),Pel_sim,portee120(i),param]=EK80_NoiseRange_CW(RawData,4,1,-70);
[Pel_sim_ping,Pel_ping,Pel2Pac,P_an,Pel_200(i),Pel_sim,portee200(i),param]=EK80_NoiseRange_CW(RawData,5,1,-70);


end

figure;
plot(vit_fd,Pel_18)
hold on;
plot(vit_fd,Pel_38)
plot(vit_fd,Pel_70)
plot(vit_fd,Pel_120)
plot(vit_fd,Pel_200)
grid on;
legend('18kHz','38kHz','70kHz','120kHz','200kHz');
xlabel('Vessel speed (knts)');ylabel('Noise level dB/W');title('Self noise')