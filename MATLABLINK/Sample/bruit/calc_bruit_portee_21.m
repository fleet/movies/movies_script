% code permettant de lire un fichier ME70 (de pr�f�rence passif) et effectuant l'�valuation
% de bruit (similaire simrad et moyenn� en valeurs naturelles) et de port�e
% 
% Renseigner le nom de fichier et le nombre de pings � lire lignes 13 et 14
% Apr�s lecture, des images de donn�es s'afficheront pour demander de
% renseigner au clavier les plages de pings et d'�chantillons � analyser

%pour utiliser ce code pour une configuration ME70 autre que la 21
%faisceaux, passer la condition � 0 ligne 57 (calcul Pel2Pac)

%% lecture du .raw � utiliser
% obtention pr�alable de la matrice Pr(nb_ping,nb_voies,nb_ech)
clear
npings_max=300;
fname='K:\ESTECH17\bruit propre\ME70\ESTECH17-D20170924-T164505.raw';
[Pr,heure,configtransducer,param]=readraw_Pr(fname,npings_max);


%% param�tres
c=param.c;

nbeams=length(param.fr);
freq=param.fr;

%range et pings d'�valuation
if 0 % on rentre les s�lections de pings/�chantillons � la main
    %range d'evaluation
    ech1=500;
    ech2=2000;

    %index de pings
    p1=60;
    %p2=min(100,size(Pr,1)-1);
    p2=160;
else %entr�e interactive
    %range d'evaluation
    figure(1);imagesc(squeeze(Pr(1,:,:)).');colorbar;title('Pr ping 1, renseigner la plage d echantillons � utiliser (keybord)'); xlabel('voies');ylabel('�chantillons')
    ech1=input('index du 1er �chantillon � prendre en compte:');
    ech2=input('index du dernier �chantillon � prendre en compte:');
    
    %index de pings
    figure(2);imagesc(squeeze(Pr(:,1,:)).');colorbar;title('Pr voie 1, renseigner la plage de  pings � utiliser (keybord)'); xlabel('pings');ylabel('�chantillons')
    p1=input('index du 1er ping � prendre en compte:');
    p2=input('index du dernier ping � prendre en compte:');
end

[hh,mm,ss]=read_day_time_raw(heure(p1));
disp(['ping deb: ' num2str(hh) ':' num2str(mm) ':' num2str(ss)]);
[hh,mm,ss]=read_day_time_raw(heure(p2));
disp(['ping fin: ' num2str(hh) ':' num2str(mm) ':' num2str(ss)]);
disp(' ')

%% param�tres de conversion Pel/Pac
% formule Pac= Pel_noise  - 10.log(lambda2/4pi) - 10.log(Breceiver) - 10.log(eta_el) (+181.8)
% T=1024 us pour tout le monde

cref=1503;

if (0)
    %param�tres �valu�s sur la config 21
    Pel2Pac=[193.8 194.3 194.8 195.4 195.6 196 196.5 197 197.3 197.3 198.1 197.7 197.2 197 196.6 196.6 196 195.4 195 194.6 194.1]-20*log10(c/cref);
else
    %mod�le Simrad
%     nnom=0.55;
%     fnom=95000;
%     n1=-0.62*((fnom+abs(freq-fnom))/fnom).^2+1.62;
%     eta_el=nnom*n1;
    eta_el=0.55;
    lambda=c./freq;
    Pel2Pac =- 10*log10(lambda.^2/(4*pi)) - 10*log10(param.Brx) - 10*log10(eta_el)+181.8;
end


%% Estimation du bruit �lectrique et du bruit ambiant �quivalent
Pel_sim=mean(mean(squeeze(Pr(p1:p2,:,ech1:ech2)),3)); %moyenne sur les dB
Pel=10*log10(mean(mean(squeeze(10.^(Pr(p1:p2,:,ech1:ech2)/10)),3))); %moyenne correcte

figure(2);imagesc(squeeze(Pr(2,:,:)).');colorbar;title('Pr ping 2')

figure(3);hold off;plot(Pel_sim,'LineW',2);title('Pel dB ref 1W, smd estimation')
figure(3);hold on;plot(Pel,'r','LineW',2);hold off;grid on;title('Pel dB ref 1W')
legend('Pel dB ref 1W, smd estimation','Pel dB ref 1W')

figure(4);plot(Pel_sim+Pel2Pac,'-*');hold on;plot(Pel+Pel2Pac,'r-*');hold off;grid on;title('AN dB ref uPa / sqrt(Hz)')
legend('AN dB ref uPa / sqrt(Hz), smd estimation','AN dB ref uPa / sqrt(Hz)')
P_an=Pel+Pel2Pac;

for ifreq=1:nbeams
    disp(['Pel ' num2str(freq(ifreq)/1000) 'kHz: ' num2str(Pel(ifreq)) 'dB ref 1W, Noise ' num2str(freq(ifreq)/1000) 'kHz: ' num2str(P_an(ifreq)) 'dB ref uPa / sqrt(Hz)']);
end
disp(' ')


%% calcul de port�e
sv_ref=-70;

G=param.Gain;
%G(lifreq)=28;
SaCorr=param.SaCorr;
%SaCorr(lifreq)=-0.45;
psi=param.psi;
T=param.T;
Pt=param.Pt;
alpha=param.alpha;

for ib=1:nbeams
    angle(ib)=configtransducer(ib).diry;
end

%DI=[25 28 28 28 28 28 28 36.6 30.7];

lambda=c./freq;
rl=(1:10000);
for ifreq=1:nbeams
    sv_moy=Pel(ifreq)+20*log10(rl)+2*alpha(ifreq)*rl-10*log10(Pt(ifreq)*lambda(ifreq).^2*c/(32*pi^2))-2*G(ifreq)-10*log10(T(ifreq))-2*SaCorr(ifreq)-psi(ifreq);
    %figure;plot(rl,sv_moy);grid on;xlabel('range');ylabel('dB');title('sv moyen du au bruit en fct du range')
    portee(ifreq)=rl(min(find(sv_moy>=-70)));
    disp(['portee ' num2str(freq(ifreq)/1000) 'kHz: ' num2str(portee(ifreq)) 'm, prof:' num2str(round(portee(ifreq)*cosd(angle(ifreq)))) 'm, pour sv_moy=' num2str(sv_ref) ,'dB'])
end
disp(' ');
figure(6);plot(portee,'LineW',2);title(['portee par voie (bruit moyenn� en naturel), sv ref =' num2str(sv_ref) 'dB']);xlabel('voies');ylabel('port�e en m�tres');grid on
figure(7);plot(-round(portee.*cosd(angle)),'LineW',2);title(['profondeur par voie (bruit moyenn� en naturel), sv ref =' num2str(sv_ref) 'dB']);xlabel('voies');ylabel('profondeur en m�tres');grid on

%% evaluation par rapport au bruit de mer

%parametre knudsen
for ifreq=1:nbeams
    B0=P_an(ifreq)+17*log10(freq(ifreq)/1000);
    br_th=25+20*log10(freq(ifreq)/100000);
    disp([num2str(freq(ifreq)/1000) 'kHz: ' ' B0 ' num2str(B0) 'dB, br th ' num2str(br_th) 'dB'])
end

% Etat de mer (SS)
% 0         0.5      1       2       3       4       5       6
% Echelle Beaufort
% 0         1        2       3       4       5       6       7
% B0 
% 44.5      50      55      61.5     64.5    66.5    68.5    70

B0=[44.5      50      55      61.5     64.5    66.5    68.5    70];
SS=[0         0.5      1       2       3       4       5       6];
freq_kn=(60000:10:150000);
for iss=1:8
    br_kn(iss,:)=B0(iss)-17*log10(freq_kn/1000);
end

freq_th=(100000:100:150000);
br_th=25+20*log10(freq_th/100000);
figure(5);plot(freq_kn/1000,br_kn);hold on;plot(freq_th/1000,br_th,'--');hold off;grid on;
legend('knudsen SS0','knudsen SS0.5','knudsen SS1','knudsen SS2','knudsen SS3','knudsen SS4','knudsen SS5','knudsen SS6','bruit thermique')
figure(5);hold on;plot(freq/1000,P_an,'*','LineW',2);hold off
xlabel('fr�quence en kHz');ylabel('dB ref 1uPa / sqrt(Hz)');title('bruit ambiant et bruit sondeur')