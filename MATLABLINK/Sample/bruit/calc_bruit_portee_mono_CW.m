function [Pel_sim_ping,Pel_ping,Pel2Pac,P_an,Pel,Pel_sim,portee,param]=calc_bruit_portee_mono_CW(FileName,typ) 
% code permettant de lire un fichier EK80/EK60(de pr�f�rence passif) et effectuant l'�valuation
% de bruit (similaire simrad et moyenn� en valeurs naturelles) et de port�e

%actualiser:
% type EK80/EK60
% fichier et nombre de pings � lire

% Apr�s lecture, des images de donn�es s'afficheront pour demander de
% renseigner au clavier les plages de pings et d'�chantillons � analyser

% actualiser au besoin:
% conversion Pel/Pac: �ventuellement le facteur de conversion si le type de sondeur change
% calcul de port�e : sv_ref 
% 
if (0)
    close all;
    clear all;
    %typ='EK80';
    %FileName='F:\Campagnes\ESSTECH17_Thalassa\EK80\RAW\bruit propre\ESTECH17-D20170924-T155020.raw'; %CW
    %FileName='F:\Campagnes\ESSTECH17_Thalassa\EK80\RAW\bruit propre\ESTECH17-D20170924-T160315.raw'; %FM
    % FileName='F:\Campagnes\ESSTECH18_Thalassa\EK80\NoiseLvele_coque\ESSTECH2-D20180908-T103920.raw'; %0knt
    % FileName='F:\Campagnes\Thalassa2018\EK80\NoiseLvele_coque\ESSTECH2-D20180908-T102207.raw'; %2knt
    %  FileName='F:\Campagnes\Thalassa2018\EK80\NoiseLvele_coque\ESSTECH2-D20180908-T100147.raw'; %4knt
    %  FileName='F:\Campagnes\ESSTECH18_Thalassa\EK80\NoiseLvele_coque\ESSTECH2-D20180908-T094005.raw'; %6knt
    %  FileName='F:\Campagnes\ESSTECH18_Thalassa\EK80\NoiseLvele_coque\ESSTECH2-D20180908-T091726.raw'; %8knt
    %  FileName='F:\Campagnes\ESSTECH18_Thalassa\EK80\NoiseLvele_coque\ESSTECH2-D20180908-T084847.raw'; %10knt
    typ='EK80';
    %FileName='K:\TELEPRESENCE\Transfert mesure bruit\RAW\EK80\TELEPRESENCE\TELEPRESENCE-D20181206-T091731.raw'; % 85
    %FileName='K:\TELEPRESENCE\Transfert mesure bruit\RAW\EK80\TELEPRESENCE\TELEPRESENCE-D20181206-T092327.raw'; % 77
    FileName='K:\TELEPRESENCE\Transfert mesure bruit\RAW\EK80\TELEPRESENCE\TELEPRESENCE-D20181206-T093405.raw'; % 63
    %FileName='K:\TELEPRESENCE\Transfert mesure bruit\RAW\EK80\TELEPRESENCE\TELEPRESENCE-D20181206-T095052.raw'; % 45
    %FileName='K:\TELEPRESENCE\Transfert mesure bruit\RAW\EK80\TELEPRESENCE\TELEPRESENCE-D20181206-T100031.raw'; % 30
end

%% lecture du .raw � utiliser, obtention pr�alable de la matrice Pr(nb_ping,nb_ek60,nb_ech)

npings_max=500;

if strcmp(typ,'EK80')
    % Read EK80 Raw data files
    [RawData] = ReadEK80Data(FileName);

     nchannel=size(RawData.Data.PingData,1);

        for ichannel = 1:nchannel % channel index   

                 [BasicProcessedData] = BasicProcessData(RawData,ichannel);

                 empty_elems = arrayfun(@(s) all(structfun(@isempty,s)), BasicProcessedData(1).ProcessedSampleData); % virer les cellules vides (1 ping sur 6)
                 BasicProcessedData(1).ProcessedSampleData = BasicProcessedData(1).ProcessedSampleData(:);     
                 rawChannel(ichannel,:)=RawData.Data.PingData(ichannel,:);

                 npings=size(BasicProcessedData(1).ProcessedSampleData,1);

                 for z = 1:npings
                     heure(z)=rawChannel(ichannel,z).time;
                     Pr(z,ichannel,1:length(squeeze(BasicProcessedData(1).ProcessedSampleData(z).power))) = 10*log10(squeeze(BasicProcessedData(1).ProcessedSampleData(z).power))';
                     Sp_range(z,ichannel,1:length(squeeze(BasicProcessedData(1).ProcessedSampleData(z).power))) = BasicProcessedData(1).ProcessedSampleData(z).svRange;
                 end

        end

    % param�tres
    param.c=RawData.Data.PingData(1,1).EnvironmentData.soundSpeed;

    for i=1:nchannel
        param.fr(i)=RawData.Data.PingData(i,1).ParameterData.frequencyUsed;
        param.T(i)=RawData.Data.PingData(i,1).ParameterData.pulseLength;
        str_lgd{i}=[num2str(param.fr(i)/1000) 'kHz'];
        gainTable       = str2num(RawData.Data.ConfigurationData.Transceivers(i).Channels.Transducer.Gain);
        pulseLengthTable = str2num(RawData.Data.ConfigurationData.Transceivers(i).Channels.PulseDuration); %OK for CW
        if (~RawData.Data.PingData(i,1).ParameterData.pulseForm)
            % CW
            pulseLengthIndex = dsearchn(pulseLengthTable,param.T(i));
            param.Gain(i) = gainTable(pulseLengthIndex);
            
            PingData=RawData.Data.PingData(i,1);
            SampleData      = PingData.SampleData;
            %complexSamples = SampleData.complexSamples;
            PingData.ParameterData.frequencyStart=str2num(RawData.Data.ConfigurationData.Transceivers(i).Channels.Transducer.Frequency);
            PingData.ParameterData.frequencyStop=str2num(RawData.Data.ConfigurationData.Transceivers(i).Channels.Transducer.Frequency);
%             transmitSignal = CreateTransmitSignal(PingData);
%             transmitSignalPower = abs(transmitSignal).^2;
%             effectivePulselLength  = PingData.ParameterData.sampleInterval * sum(transmitSignalPower) / max(transmitSignalPower); % nlb: SaCorr added after
%             param.effectivePulselLength(i)=effectivePulselLength;
            param.effectivePulselLength(i)=BasicProcessedData.ChannelData.Transducer.effectivePulselLength;
            SaCorrTable       = str2num(RawData.Data.ConfigurationData.Transceivers(i).Channels.Transducer.SaCorrection);
            param.SaCorr(i)=SaCorrTable(pulseLengthIndex);
        else
            % FM
           param.Gain(i) = gainTable(end);
           param.effectivePulselLength(i)=param.T(i);
           param.SaCorr(i)=0;
           uiwait(msgbox(['Warning: echosounder ' num2str(param.fr(i)/1000) 'kHz in FM mode'],'FM case','modal'));
        end
        param.psi(i)=str2num(RawData.Data.ConfigurationData.Transceivers(i).Channels.Transducer.EquivalentBeamAngle);
        param.DI(i)=-param.psi(i)+10*log10(41000/5800);
        param.Pt(i)=RawData.Data.PingData(i,1).ParameterData.transmitPower;
        absorptionCoefficients = EstimateAbsorptionCoefficients(rawChannel(i,1).EnvironmentData,rawChannel(i,1).ParameterData.frequencyUsed);
        param.alpha(i)=absorptionCoefficients;
        param.sampleinterval_m(i)=Sp_range(1,i,2)-Sp_range(1,i,1);
        lambda=param.c./param.fr;
        param.Brx(i)=1./param.T(i)*2;
    end
else %EK60
    [Pr,heure,configtransducer,param]=readraw_Pr(FileName,npings_max);
    nchannel=size(Pr,2);
    npings=size(Pr,1);
    param.DI=-param.psi+10*log10(41000/5800);
    lambda=param.c./param.fr;
    param.sampleinterval_m= param.sampleinterval*param.c./2;
    param.effectivePulselLength=param.T; %nlb SaCorr adeed after
    for i=1:nchannel
        str_lgd{i}=[num2str(param.fr(i)/1000) 'kHz'];
        Sp_range(1,i,:)=(1:size(Pr,3))*param.sampleinterval_m(i);
    end
end
%% plage de donn�es � analyser

[~,ind]=find(Sp_range(1,1,:)>0); %on cherche le range max sur la fr�quence 1
for i=1:nchannel
    echm=floor((max(ind)-1)*param.sampleinterval_m(1)/param.sampleinterval_m(i));
    figure;imagesc((1:1:npings),squeeze(Sp_range(1,i,1:echm))',squeeze(Pr(:,i,1:echm)));title(['Pr ' num2str(param.fr(i)/1000) 'kHz. V�rifier ou entrer au clavier les �chantillons/pings � analyser']);colorbar
    xlabel('pings');ylabel('profondeur')
    caxis([-180 -130])
end


if (0) % on rentre les s�lections de pings/�chantillons � la main
    %range d'evaluation
    ech1=200;
    ech2=2000;

    %index de pings
    p1=10;
    p2=min(100,npings);
else %entr�e interactive
    %range d'evaluation
    ech1=input('Range du 1er �chantillon � prendre en compte:');
    ech2=input('Range du dernier �chantillon � prendre en compte:');
    
    %index de pings
    p1=input('index du 1er ping � prendre en compte:');
    p2=input('index du dernier ping � prendre en compte:');
end

for i=1:nchannel
    echd1(i)=floor(ech1/param.sampleinterval_m(i));
    echd2(i)=floor(ech2/param.sampleinterval_m(i));
end

if strcmp(typ,'EK80')
    disp(['ping deb: ' datestr(heure(p1))]);
    disp(['ping fin: ' datestr(heure(p2))]);
    disp(' ')
else
    [hh,mm,ss]=read_day_time_raw(heure(p1));
    disp(['ping deb: ' num2str(hh) ':' num2str(mm) ':' num2str(ss)]);
    [hh,mm,ss]=read_day_time_raw(heure(p2));
    disp(['ping fin: ' num2str(hh) ':' num2str(mm) ':' num2str(ss)]);
    disp(' ')
end


%% param�tres de conversion Pel/Pac 
% formule Pac= Pel_noise  - 10.log(lambda2/4pi) - 10.log(Breceiver) - 10.log(eta_el)	(+181.8)
% ou Pac= Pel_noise  - 10.log(lambda2/4pi) - 10.log(Breceiver) - Gain + DI	(+181.8)

if (0)
    % T=1024 us pour tout le monde
    % 18    38-B 70 120 200 333-7C 120
    cref=1517; % c�l�rit� du fichier o� on a fait l'analyse du facteur de conversion
    Breceiver_ref=[1.57 2.43 2.86 3.03 3.09 3.11 3.54]*1000;
    fr_conv=[18 38 70 120 200 333 121]*1000;
    
    Pel2Pac=[185.4 189.7 194.5 198.8 203.2 207.6 199]-20*log10(param.c/cref);
    % Pel2Pac=[185.4 189.7 194.5 194.5 203.2 207.6 199]-20*log10(c/cref); %256us 120thalia
    % Pel2Pac=[185.4 189.7 194.5 196.5 203.2 207.6 199]-20*log10(c/cref); %512us 120thalia

    Pel2PacInt=Pel2Pac+10*log10(Breceiver_ref);
    clear ind_fr2Pr;
    for i=1:nchannel
        ind_fr2Pr(i)=find(param.fr==fr_conv(i));
    end
    Pel2PacInt=Pel2PacInt(ind_fr2Pr);
    Pel2Pac=Pel2PacInt-10*log10(param.Brx);
elseif 0
    eta_el=0.55;
    for i=1:nchannel
        Pel2Pac(i) =- 10*log10(lambda(i).^2/(4*pi)) - 10*log10(param.Brx(i)) - 10*log10(eta_el)+181.8;
    end
else
    for i=1:nchannel
        Pel2Pac(i) =- 10*log10(lambda(i).^2/(4*pi)) - 10*log10(param.Brx(i)) - param.Gain(i) + param.DI(i) +181.8;
    end
end

%% Pel

for i=1:nchannel
    Pel_sim_ping(:,i)=mean(Pr(p1:p2,i,echd1(i):echd2(i)),3); %moyenne sur les dB
    Pel_ping(:,i)=10*log10(mean(10.^(Pr(p1:p2,i,echd1(i):echd2(i))/10),3)); %moyenne correcte
end

figure(10);hold on;
map=colormap(hsv(nchannel));
for i=1:nchannel
    plot(Pel_sim_ping(:,i),'LineW',2,'Color',map(i,:));
end
for i=1:nchannel
    plot(Pel_ping(:,i),'--','LineW',2,'Color',map(i,:));
end
grid on;
legend(str_lgd);
title('Pel dB ref 1W, smd estimation, Pel dB ref 1W (dashed)')
xlabel('pings');ylabel('dB')

Pel_sim=mean(Pel_sim_ping,1);
Pel=10*log10(mean(10.^(Pel_ping/10),1));

for ifreq=1:nchannel
%     disp(['Pel ' num2str(param.fr(ifreq)/1000) 'kHz: ' num2str(Pel(ifreq)) 'dB ref 1W']);
%     disp(['Pel Simrad ' num2str(param.fr(ifreq)/1000) 'kHz: ' num2str(Pel_sim(ifreq)) 'dB ref 1W']);
    disp(['Pel ' num2str(param.fr(ifreq)/1000) 'kHz: ' num2str(Pel(ifreq)) 'dB ref 1W, Pel Simrad ' num2str(param.fr(ifreq)/1000) 'kHz: ' num2str(Pel_sim(ifreq)) 'dB ref 1W']);
end
disp(' ')


%% calcul de port�e
sv_ref=-70; %dB
%DI=[25 28 28 28 28 28 28 36.6 30.7];

rl=(1:10000);
for ifreq=1:nchannel
    sv_moy=Pel(ifreq)+20*log10(rl)+2*param.alpha(ifreq)*rl-10*log10(param.Pt(ifreq)*lambda(ifreq).^2*param.c/(32*pi^2))-2*param.Gain(ifreq)-10*log10(param.effectivePulselLength(ifreq))-2*param.SaCorr(ifreq)-param.psi(ifreq);
    %figure;plot(rl,sv_moy);grid on;xlabel('range');ylabel('dB');title('sv moyen du au bruit en fct du range')
    portee(ifreq)=rl(min(find(sv_moy>=sv_ref)));
    disp(['portee ' num2str(param.fr(ifreq)/1000) 'kHz: ' num2str(portee(ifreq)) 'm, pour sv_moy=' num2str(sv_ref) ,'dB'])
end
disp(' ');

%% Pac 

figure(11);
hold on;
for i=1:nchannel
plot(Pel_sim_ping(:,i)+Pel2Pac(i),'-','LineW',2,'Color',map(i,:));
end
for i=1:nchannel
plot(Pel_ping(:,i)+Pel2Pac(i),'--','LineW',2,'Color',map(i,:));
end
grid on;
title('AN dB ref uPa / sqrt(Hz), smd estimation, AN dB ref uPa / sqrt(Hz) (dashed)')
legend(str_lgd);
xlabel('pings');ylabel('dB')

P_an=Pel+Pel2Pac;


for ifreq=1:nchannel
    disp(['Noise ' num2str(param.fr(ifreq)/1000) 'kHz: ' num2str(P_an(ifreq)) 'dB ref uPa / sqrt(Hz), Noise Simrad ' num2str(param.fr(ifreq)/1000) 'kHz: ' num2str(Pel_sim(ifreq)+Pel2Pac(ifreq)) 'dB ref uPa / sqrt(Hz)']);
end
disp(' ')

%% evaluation par rapport � l'�tat de mer

%parametre knudsen
for ifreq=1:nchannel
    B0=P_an(ifreq)+17*log10(param.fr(ifreq)/1000);
    br_th=25+20*log10(param.fr(ifreq)/100000);
    disp([num2str(param.fr(ifreq)/1000) 'kHz: ' ' B0 ' num2str(B0) 'dB, br th ' num2str(br_th) 'dB'])
end

% Etat de mer (SS)
% 0         0.5      1       2       3       4       5       6
% Echelle Beaufort
% 0         1        2       3       4       5       6       7
% B0 
% 44.5      50      55      61.5     64.5    66.5    68.5    70

B0=[44.5      50      55      61.5     64.5    66.5    68.5    70];
SS=[0         0.5      1       2       3       4       5       6];
freq_kn=(10000:10:200000);
for iss=1:8
    br_kn(iss,:)=B0(iss)-17*log10(freq_kn/1000);
end

freq_th=(100000:1000:400000);
br_th=25+20*log10(freq_th/100000);
figure(12);semilogx(freq_kn/1000,br_kn);hold on;semilogx(freq_th/1000,br_th,'--');hold off;grid on;
legend('knudsen SS0','knudsen SS0.5','knudsen SS1','knudsen SS2','knudsen SS3','knudsen SS4','knudsen SS5','knudsen SS6','bruit thermique')
figure(12);hold on;plot(param.fr/1000,P_an,'*','LineW',2);hold off
xlabel('fr�quence en kHz');ylabel('dB ref 1uPa / sqrt(Hz)');title('bruit ambiant et bruit sondeur')

%% recap param
disp(' ')
disp(['fr=' num2str(param.fr/1000)]);
disp(['T=' num2str(param.T*10^6)]);
disp(['G=' num2str(param.Gain)]);
disp(['SaCorr=' num2str(param.SaCorr)]);
disp(['psi=' num2str(param.psi)]);
disp(['Pt=' num2str(param.Pt)]);
disp(['alpha=' num2str(param.alpha)]);
disp(['Brx=' num2str(param.Brx)]);
disp(['c=' num2str(param.c)]);