%% Lance la lecture des heures de ping de tous les fichiers contenus dans un repertoire donne 
% actualiser les variables chemin_hac (o� se trouevnt les RUN) et
% chemin_save_mat (o� sauver les �rsultats matlab de lecture)
% ainsi que les num�ros (m) de runs � traiter

    clear all;
    
    %chargement config
    chemin_config = '.\config_lec_rec\';
    chemin_config_reverse = strrep(chemin_config, '\', '/');
    moLoadConfig(chemin_config_reverse);
    
    config=1;
        
for m=[13:13] %num�ros de runs
    
    if m<10
        RUN=strcat('RUN00',num2str(m))
    else
        RUN=strcat('RUN0',num2str(m))
    end;
    
    chemin_hac=['C:\data\PG15\',RUN,'\'];
    chemin_save_mat=['C:\data\PG15\',RUN,'\matlab\'];

    if ~exist(chemin_save_mat,'dir')
            mkdir(chemin_save_mat)
    end
    
    filelist = ls([chemin_hac,'*.hac']);  % ensemble des fichiers
    nb_files = size(filelist,1);  % nombre de fichiers

    for numfile = 1:nb_files  % boucle sur l'ensemble des fichiers � traiter
    %for numfile = 50:52  % boucle sur l'ensemble des fichiers � traiter
    %for numfile = 25:25
        hacfilename = filelist(numfile,:);
        [heure_hac_tot,ind_ping_hac,list_sounder]=lec_rec(chemin_hac,hacfilename,chemin_config,config);
        save([chemin_save_mat hacfilename(1:end-4)],'heure_hac_tot','ind_ping_hac','list_sounder');
    end

end;
