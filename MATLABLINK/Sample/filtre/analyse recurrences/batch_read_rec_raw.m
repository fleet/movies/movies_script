%% Lit les heures de pings de tous les fichiers raw contenus dans un repertoire donne 
% renseigner les chemins chemin_raw (de lecture) et chemin_save_mat (de
% sauvegarde des r�sultats)
% n�cessite l'acc�s au r�pertoire des routines de lecture des raw

chemin_raw='L:\ESSPROP\ER60\raw\';
chemin_save_mat=['L:\ESSPROP\ER60\raw\' 'matlab\'];

if ~exist(chemin_save_mat,'dir')
        mkdir(chemin_save_mat)
end

filelist_raw = ls([chemin_raw,'*.raw']);  % ensemble des fichiers
nb_files = size(filelist_raw,1);  % nombre de fichiers

%for numfile = 1:nb_files  % boucle sur l'ensemble des fichiers � traiter
for numfile = 80:80  % boucle sur l'ensemble des fichiers � traiter
    filename = filelist_raw(numfile,:)
    fname=[chemin_raw filename];
    [heure]=readraw_heure_raw(fname,1,50000);
    save([chemin_save_mat filename(1:end-4)],'heure');
    
    if (1)%affichage
        figure(1);plot(heure(2:end)-heure(1:end-1));grid on
        ylim([0 1])
        title(filename(1:end-4),'interpreter','none');
    end
end

