function [heure_hac_tot,ind_ping_hac,list_sounder]=lec_rec(chemin_hac,hacfilename,chemin_config,config)

%nom et chemin fichier
if(0)
chemin_ini='K:\SMFH\EXACHA10\HAC HERMES\EXACHA10\RUN001\';
nom_fic='EXACHA10_001_20100319_171559.hac';
 FileName = [chemin_ini,nom_fic]
end


FileName = [chemin_hac,hacfilename];
    
%% param�tres de lecture du hac
if (config==0) % config d�j� charg�e ou pas
    if (0) %config � la main
        ParameterKernel= moKernelParameter();
        ParameterDef=moLoadKernelParameter(ParameterKernel);

        ParameterDef.m_MaxRange = 200;
        ParameterDef.m_bAutoDepthEnable=0;
        ParameterDef.m_bAutoLengthEnable=1;
        ParameterDef.m_bIgnorePhaseData=1;
        ParameterDef.m_bIgnorePingsWithNoNavigation=0;
        moSaveKernelParameter(ParameterKernel,ParameterDef);
        clear ParameterDef;

        ParameterR= moReaderParameter();
        ParameterDef=moLoadReaderParameter(ParameterR);

        ParameterDef.m_chunkNumberOfPingFan=50;

        moSaveReaderParameter(ParameterR,ParameterDef);
    else %charge config
        moLoadConfig(strrep(chemin_config, '\', '/'));

        ParameterKernel= moKernelParameter();
        ParameterDef=moLoadKernelParameter(ParameterKernel);
        ParameterDef.m_bIgnorePhaseData=0;
        ParameterDef.m_bAutoLengthEnable=1;

        moSaveKernelParameter(ParameterKernel,ParameterDef);
        clear ParameterDef;

    end
end

%lecture hac
%%%%%%%%%%%%%%%
moOpenHac(FileName);

FileStatus= moGetFileStatus;
ilec=0;
nb_pings_prec=0;

while FileStatus.m_StreamClosed < 1 | ilec==0
%while FileStatus.m_StreamClosed < 1 & ilec<=3
    moReadChunk();
    FileStatus= moGetFileStatus();
    ilec=ilec+1;


    if (ilec<=1) %premier ping lu, on pr�sente les sondeurs et choisit celui � traiter


        %presentation des sondeurs
        list_sounder=moGetSounderList;
        nb_snd=length(list_sounder);
        disp(' ');
        disp('pr�sentation des sondeurs');
        disp('-------------------------');
        disp(' ');
        disp(['nb sondeurs = ' num2str(nb_snd)]);
        for isdr = 1:nb_snd
            nb_transduc=list_sounder(isdr).m_numberOfTransducer;
            if (list_sounder(isdr).m_SounderId<10)
%                ind_list(list_sounder(isdr).m_SounderId)=isdr;
                ind_list(isdr)=list_sounder(isdr).m_SounderId;                
                %disp(['sondeur num�ro ' num2str(isdr) ':    ' '(index: ' num2str(list_sounder(isdr).m_SounderId) ')   nb trans:' num2str(nb_transduc)]);
                disp(['sondeur num�ro ' num2str(isdr) ':    '  '   nb transducteurs:' num2str(nb_transduc)]);
                for itr=1:nb_transduc
                    nb_soft_chnel(isdr,itr)=list_sounder(isdr).m_transducer(itr).m_numberOfSoftChannel;
                    disp(['   transducteur num�ro ' num2str(itr) ':    ' 'nom: ' list_sounder(isdr).m_transducer(itr).m_transName '   nb soft_chnl:' num2str(nb_soft_chnel(isdr,itr))]);
                    for isf=1:nb_soft_chnel(isdr,itr)
                        disp(['       soft_chn ' num2str(isf) ':    ' '   freq: ' num2str(list_sounder(isdr).m_transducer(itr).m_SoftChannel(isf).m_acousticFrequency/1000) ' kHz']);
                    end
                end
            else
                disp('Pb sondeur Id');
            end
        end
        
        heure_hac_tot=[];
        ind_ping_hac=[];
        ind_ping=zeros(1,nb_snd);
    end
    
    nb_pings=moGetNumberOfPingFan();

        
    for index= nb_pings_prec:nb_pings-1  %boucle sur les pings
        MX= moGetPingFan(index);
        SounderDesc =  moGetFanSounder(MX.m_pingId);
        num_sdr=find(ind_list==SounderDesc.m_SounderId);
        ind_ping(num_sdr)=ind_ping(num_sdr)+1;
        heure_hac_tot(num_sdr,ind_ping(num_sdr))=MX.m_meanTimeCPU+MX.m_meanTimeFraction/10000;
        %ind_ping_hac(num_sdr,ind_ping(num_sdr))=index;
        ind_ping_hac(num_sdr,ind_ping(num_sdr))=MX.m_pingId;
    end
    nb_pings_prec=nb_pings;
end;
fprintf('End of File');

%% ecriture hac    
%moStartWrite(chemin_save,'filtre_pstes_rec_');
%moStopWrite;

%% affichage optionnel
if (1) %r�currences 
    col='bgr';
    rec=heure_hac_tot(:,2:end)-heure_hac_tot(:,1:end-1);
    figure(20);hold off
    str_lgd=[];
    for isdr=1:min(nb_snd,size(heure_hac_tot,1))
        nb_p=min(find(heure_hac_tot(isdr,:)==0))-1;
        if isempty(nb_p) nb_p=size(heure_hac_tot,2);end
        if nb_p>0
            figure(20);plot(heure_hac_tot(isdr,2:nb_p),rec(isdr,1:nb_p-1),[col(isdr) '-']);hold on;
            str_lgd{end+1}=list_sounder(isdr).m_transducer(1).m_transName;
        end
    end
    xlabel('heure');ylabel('recurrence');grid on;
    title(hacfilename,'interpreter','none');
    legend(str_lgd)
end
