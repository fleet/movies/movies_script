function [heure]=readraw_heure_raw(fname,ip1,npingmax)

% Reading SMS raw data file
% Simrad, Lars Nonboe Andersen, 12/9-05

% ip1: 1er ping � lire
%clear all
%close all

%fname ='L:\PELGAS10\ME70_raw\RUN003\PELGAS10-D20100428-T092407.raw'; %Name of
%file to read 

headerlength = 12; % Bytes in datagram header

%pingno = 0;
pingno2 = 0;
pingno3 = 0;
nrefbeams = 0;
pingtime=0;
lastpingtime=0;
pingrate = [];

fid = fopen(fname,'r');
if (fid==-1)
    error('Could not open file');
else
    % Read configuration datagram
    length = fread(fid,1,'int32');
    dgheader = readdgheader(fid);
    configheader = readconfigheader(fid);
    for i=1:configheader.transducercount,
        configtransducer(i) = readconfigtransducer(fid);
        if ~isempty(findstr(configtransducer(i).channelid,'Reference'))
            nrefbeams = nrefbeams+1;
        end
    end
    config = struct('header',configheader,'transducer',configtransducer);
    config.header.nrefbeams = nrefbeams;
    length = fread(fid,1,'int32');
    
    indexpos=1;
    % Read NMEA, Annotation, or Sample datagram
    while (1)
        length = fread(fid,1,'int32');
        if (feof(fid))
            break
        end
        dgheader = readdgheader(fid);

        
        
        switch (dgheader.datagramtype)
        case 'CON1' % SMS extra configuration datagram
            text = readtextdata(fid,length-headerlength);
            disp('CON1');
        case 'NME0' % NMEA datagram
            text = readtextdata(fid,length-headerlength);
            if text.text(1:6)=='$INGLL'
                %lat(indexpos)=str2num(text.text(8:17)); trame gga ou gll
                %long(indexpos)=str2num(text.text(21:31));
                %lat(indexpos)=str2num(text.text(8:9))+str2num(text.text(10:17))/60; %degres decimaux
                %long(indexpos)=str2num(text.text(21:23))+str2num(text.text(24:31))/60;
                %long(indexpos)=str2num(text.text(22:30));
                indexpos=indexpos+1;
            end
 %           disp('NME0');
        case 'TAG0' % Annotation datagram
            text = readtextdata(fid,length-headerlength);
            disp('TAG0');
        case 'RAW0' % Sample datagram
            sampledata = readsampledata(fid);
            %disp('RAW0');
           if (sampledata.mode==3)
            delta=sampledata.soundvelocity*sampledata.sampleinterval/2;
            % WRITE YOUR OWN CODE HERE TO PROCESS AND/OR DISPLAY SAMPLE DATA
             channel = sampledata.channel;
             
                 tvgsampledata = applytvg(config,sampledata);
                 tvgdata(channel) = tvgsampledata;
                 
                 
%                if pingno2>=1
%                 Attitude(1,pingno2+1) = sampledata.heave;
%                 Attitude(2,pingno2+1) = sampledata.roll;
%                 Attitude(3,pingno2+1) = sampledata.pitch;
%                 Attitude(4,pingno2+1) =  dgheader.datetime/ 10000000;
%                %pingno=pingno+1;
%                end
                
                if (sampledata.channel==1) 
                    pingno3=pingno3+1; 
                end
                
                if pingno3>=ip1
                    pingno2=pingno3-ip1+1;
                end
                
                if pingno2>npingmax
                    break;
                end
                
                if pingno2>=1
                 %sv_data(pingno2,channel,1:size(tvgsampledata.sv,1))=tvgsampledata.sv.';
%                 sv_range(channel,pingno2,1:size(tvgsampledata.sv,1))=tvgsampledata.svrange;
                 %sp_data(pingno2,channel,1:size(tvgsampledata.sv,1))=tvgsampledata.sp.';
%                 sp_range(channel,pingno2,1:size(tvgsampledata.sv,1))=tvgsampledata.sprange;
%                 Pr(pingno2,sampledata.channel,1:size(sampledata.power,1))=sampledata.power.';
%                 heave(pingno2)= sampledata.heave;
                 heure(pingno2)=dgheader.datetime/ 10000000;
%                 
%                  steer_at=config.transducer(channel).diry;
%                  steer_al=config.transducer(channel).dirx;
% %                 
% %                 %angles ref antenne
%                  anglesAt_data(channel,pingno2,1:size(tvgsampledata.sv,1))=asin(sind(steer_at)+sampledata.athwartship*cosd(steer_at)*0.75*config.transducer(channel).beamwidthathwartship/128*pi/180);
%                  anglesAl_data(channel,pingno2,1:size(tvgsampledata.sv,1))=asin(sind(steer_al)+sampledata.alongship*cosd(steer_al)*0.75*config.transducer(channel).beamwidthalongship/128*pi/180);
% %                 
                %angles dans la voie
                %anglesAt_data(channel,pingno2,1:size(tvgsampledata.sv,1))=asin(sind(steer_at)+sampledata.athwartship*cosd(steer_at)*0.75*config.transducer(channel).beamwidthathwartship/128*pi/180)-steer_at*pi/180;
                %anglesAl_data(channel,pingno2,1:size(tvgsampledata.sv,1))=asin(sind(steer_al)+sampledata.alongship*cosd(steer_al)*0.75*config.transducer(channel).beamwidthalongship/128*pi/180)-steer_al*pi/180;
                end
           
                
           end

        otherwise
            %error(strcat('Unknown datagram ''',dgheader.datagramtype,''' in file'));
        end
        length = fread(fid,1,'int32');

    end
    fclose(fid);
end



disp('Finished reading file');

    
