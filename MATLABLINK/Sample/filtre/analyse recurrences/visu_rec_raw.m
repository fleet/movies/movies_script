% routine permettant de visualiser le r�sum� des r�currences et des pings
% filtr�s des raw d'un r�pertoire


chemin_save_mat=['L:\ESSPROP\ER60\raw\' 'matlab\'];

filelist = ls([chemin_save_mat,'*.mat']);  % ensemble des fichiers
nb_files = size(filelist,1);  % nombre de fichiers

heure_cat=[];
cat=0; %concatenation (si l'on souhaite visualiser le r�sultat des fichiers concat�n�s)

for numfile = 1:nb_files  % boucle sur l'ensemble des fichiers � traiter
%for numfile = 1:3  % boucle sur l'ensemble des fichiers � traiter
    filename = filelist(numfile,:);
    load([chemin_save_mat filename]);
    
    figure(1);plot(heure(2:end)/(24*3600)+datenum('01-Jan-1601'),heure(2:end)-heure(1:end-1),'LineW',2);grid on
    datetickzoom
    ylim([0 1])
    title(filename(1:end-4),'interpreter','none');
    xlabel('ping');ylabel('recurrence');grid on;
        
        pause;
        %' '
    if (cat==1) %concatenation
        heure_cat=[heure_cat heure];
    end
        
end

if (cat==1)
    figure(2);plot(heure_cat(2:end)/(24*3600)+datenum('01-Jan-1601'),heure_cat(2:end)-heure_cat(1:end-1));grid on
     datetickzoom
     ylim([0,2]);
end
