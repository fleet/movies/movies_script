% routine permettant de visualiser le r�sum� des r�currences des hac d'un r�pertoire
% figure 20: r�currences de chaque sondeur
% figure 21: diff�rence de temps entre chaque ping du sondeur isdr et les
% pings pr�c�dent et suivant (ref timestamp) du sondeur isdr_ref
% figure 22: diff�rence de temps entre chaque ping du sondeur isdr et les
% pings pr�c�dent et suivant (ref ordre d'archivage) du sondeur isdr_ref

m=4; %num�ro du RUN � analyser
if m<10
    RUN=strcat('RUN00',num2str(m))
else
    RUN=strcat('RUN0',num2str(m))
end;

chemin_save_mat=['C:\data\PG16\',RUN,'\matlab\'];

filelist = ls([chemin_save_mat,'*.mat']);  % ensemble des fichiers
nb_files = size(filelist,1);  % nombre de fichiers

for numfile = 1:nb_files  % boucle sur l'ensemble des fichiers � traiter
%for numfile = 1:3  % boucle sur l'ensemble des fichiers � traiter
    hacfilename = filelist(numfile,:)
    load([chemin_save_mat hacfilename(1:end-4)]);
    nb_snd=size(heure_hac_tot,1);
    
    col='bgr';
    rec=heure_hac_tot(:,2:end)-heure_hac_tot(:,1:end-1);
    
    %visu rec
    figure(20);hold off
    str_lgd=[];
    h0=heure_hac_tot(1,1);
    for isdr=1:nb_snd
        nb_p=min(find(heure_hac_tot(isdr,:)==0))-1;
        if isempty(nb_p) nb_p=size(heure_hac_tot,2);end
        if nb_p>0
            figure(20);plot(heure_hac_tot(isdr,2:nb_p)/(24*3600)+datenum('01-Jan-1970'),rec(isdr,1:nb_p-1),[col(isdr) '-']);hold on;
            str_lgd{end+1}=list_sounder(isdr).m_transducer(1).m_transName;
        end
    end
    xlabel('heure');ylabel('recurrence');grid on;
    datetickzoom;
    title(hacfilename,'interpreter','none');
    legend(str_lgd)
    ylim([0 2])
        
    %visu dif temps pings sondeurs
    isdr_ref=1;
    isdr=2;
    if nb_snd>=2
        nb_p=min(find(heure_hac_tot(isdr,:)==0))-1;
        nb_p_ref=min(find(heure_hac_tot(isdr_ref,:)==0))-1;
        if isempty(nb_p) nb_p=size(heure_hac_tot,2);end
        if isempty(nb_p_ref) nb_p_ref=size(heure_hac_tot,2);end
        if nb_p*nb_p_ref<=max(nb_p,nb_p_ref) % moins dun ping sur un des deux sondeurs
            figure(21);hold off;plot(0,0);
            title(hacfilename,'interpreter','none');
            legend('un seul sondeur ping')
        else
            nb_p=min(nb_p,max(find(heure_hac_tot(isdr,1:nb_p)<heure_hac_tot(isdr_ref,nb_p_ref))));
            ip0=min(find(heure_hac_tot(isdr,:)>=heure_hac_tot(isdr_ref,1)));
            clear delta1 delta2
            str_lgd=[];
            for ip=ip0:nb_p
                ip_inf=max(find(heure_hac_tot(isdr_ref,:)~=0 & heure_hac_tot(isdr_ref,:)<=heure_hac_tot(isdr,ip)));
                delta1(ip-ip0+1)=heure_hac_tot(isdr,ip)-heure_hac_tot(isdr_ref,ip_inf);
                delta2(ip-ip0+1)=-(heure_hac_tot(isdr,ip)-heure_hac_tot(isdr_ref,ip_inf+1));
            end
            figure(21);hold off
            figure(21);plot(heure_hac_tot(isdr,ip0:nb_p)/(24*3600)+datenum('01-Jan-1970'),delta1,'LineW',2);hold on;
            str_lgd{1}=['delta ' list_sounder(isdr).m_transducer(1).m_transName '-' list_sounder(isdr_ref).m_transducer(1).m_transName];
            figure(21);plot(heure_hac_tot(isdr,ip0:nb_p)/(24*3600)+datenum('01-Jan-1970'),delta2,'r','LineW',2);hold on;
            str_lgd{2}=['delta ' list_sounder(isdr_ref).m_transducer(1).m_transName '-' list_sounder(isdr).m_transducer(1).m_transName];
            xlabel('heure');ylabel('delta ping en secondes');grid on;

            title(hacfilename,'interpreter','none');
            legend(str_lgd)
            ylim([0 2])
            datetickzoom;
        end
    else
        figure(21);hold off;plot(0,0);
        title(hacfilename,'interpreter','none');
        legend('un seul sondeur ping')
    end
    
    % visu dif temps pings sondeurs / ordre d'arriv�e
    if nb_snd>=2
        nb_p=size(heure_hac_tot,2);
        clear delta1 delta2
        str_lgd=[];
        for ip=1:nb_p
            ip1=max(find(ind_ping_hac(isdr_ref,:)>0 & ind_ping_hac(isdr_ref,:)<ind_ping_hac(isdr,ip)));
            if ~isempty(ip1) & heure_hac_tot(isdr,ip)*heure_hac_tot(isdr_ref,ip1)>0
                delta1(ip)=heure_hac_tot(isdr,ip)-heure_hac_tot(isdr_ref,ip1);
                if ip1+1<nb_p & heure_hac_tot(isdr_ref,ip1+1)~=0
                    delta2(ip)=heure_hac_tot(isdr_ref,ip1+1)-heure_hac_tot(isdr,ip);
                else
                    delta2(ip)=nan;
                end
            else
                delta1(ip)=nan;
                delta2(ip)=nan;
            end
        end
        heure_hac_tot_nan=heure_hac_tot;
        heure_hac_tot_nan(find(heure_hac_tot==0))=nan;
        figure(22);hold off
        figure(22);plot(heure_hac_tot_nan(isdr,:)/(24*3600)+datenum('01-Jan-1970'),delta1,'LineW',2);hold on;
        str_lgd{1}=['delta ' list_sounder(isdr).m_transducer(1).m_transName '-' list_sounder(isdr_ref).m_transducer(1).m_transName];
        figure(22);plot(heure_hac_tot_nan(isdr,:)/(24*3600)+datenum('01-Jan-1970'),delta2,'r','LineW',2);hold on;
        str_lgd{2}=['delta ' list_sounder(isdr_ref).m_transducer(1).m_transName '-' list_sounder(isdr).m_transducer(1).m_transName];
        xlabel('ping');ylabel('delta ping archivage en secondes');grid on;

        title(hacfilename,'interpreter','none');
        legend(str_lgd)
        datetickzoom;
    end
    
    pause;
end


