%% Lance le d�parasitage de tous les fichiers contenus dans un repertoire donne 

    clear all;
    
    %chargement config
    chemin_config = '.\config_filtre\';
    chemin_config_reverse = strrep(chemin_config, '\', '/');
    moLoadConfig(chemin_config_reverse);
    
    config=1;
        
for m=[5]
    
    if m<10
        RUN=strcat('RUN00',num2str(m))
    else
        RUN=strcat('RUN0',num2str(m))
    end;
    
%     chemin_hac=['L:\PELGAS11\',RUN,'\'];
%     chemin_save=['C:\data\test_parasites\pstes_rec\',RUN,'\'];
%     chemin_save_mat=['C:\data\test_parasites\pstes_rec\',RUN,'\matlab\'];
    chemin_hac=['C:\data\PG16\\',RUN,'\'];
    chemin_save=['C:\data\PG16\pstes_rec\PG16\',RUN,'\'];
    chemin_save_mat=['C:\data\PG16\pstes_rec\PG16\',RUN,'\matlab\'];
    
    if ~exist(chemin_save,'dir')
            mkdir(chemin_save)
    end
    if ~exist(chemin_save_mat,'dir')
            mkdir(chemin_save_mat)
    end
    
    filelist = ls([chemin_hac,'*.hac']);  % ensemble des fichiers
    nb_files = size(filelist,1);  % nombre de fichiers

    for numfile = 1:nb_files  % boucle sur l'ensemble des fichiers � traiter
    %for numfile = 25:25
        hacfilename = filelist(numfile,:);
         [heure_hac_tot,ind_ping_hac,saut_rec,ind_sav, isdr_ref, filt_sdr,list_sounder]=filtre_saut_rec(chemin_hac,hacfilename,chemin_save,chemin_config,config);
        save([chemin_save_mat hacfilename(1:end-4)],'heure_hac_tot','ind_ping_hac','saut_rec','ind_sav', 'isdr_ref', 'filt_sdr','list_sounder');
    end

end;
