function [heure_hac_tot,ind_ping_hac,saut_rec,ind_sav, isdr_ref, filt_sdr,list_sounder]=filtre_saut_rec(chemin_hac,hacfilename,chemin_save,chemin_config,config)

%nom et chemin fichier
if(0)
chemin_ini='K:\SMFH\EXACHA10\HAC HERMES\EXACHA10\RUN001\';
nom_fic='EXACHA10_001_20100319_171559.hac';
 FileName = [chemin_ini,nom_fic]
end


FileName = [chemin_hac,hacfilename];
    
%% param�tres de lecture du hac
if (config==0) % config d�j� charg�e ou pas
    if (0) %config � la main
        ParameterKernel= moKernelParameter();
        ParameterDef=moLoadKernelParameter(ParameterKernel);

        ParameterDef.m_MaxRange = 200;
        ParameterDef.m_bAutoDepthEnable=0;
        ParameterDef.m_bAutoLengthEnable=1;
        ParameterDef.m_bIgnorePhaseData=1;
        ParameterDef.m_bIgnorePingsWithNoNavigation=0;
        moSaveKernelParameter(ParameterKernel,ParameterDef);
        clear ParameterDef;

        ParameterR= moReaderParameter();
        ParameterDef=moLoadReaderParameter(ParameterR);

        ParameterDef.m_chunkNumberOfPingFan=50;

        moSaveReaderParameter(ParameterR,ParameterDef);
    else %charge config
        moLoadConfig(strrep(chemin_config, '\', '/'));

        ParameterKernel= moKernelParameter();
        ParameterDef=moLoadKernelParameter(ParameterKernel);
        ParameterDef.m_bIgnorePhaseData=0;
        ParameterDef.m_bAutoLengthEnable=1;

        moSaveKernelParameter(ParameterKernel,ParameterDef);
        clear ParameterDef;

    end
end

%lecture hac
%%%%%%%%%%%%%%%
moOpenHac(FileName);

FileStatus= moGetFileStatus;
ilec=0;
nb_pings_prec=0;
filt_sdr=[];

%param�tres anomalies
trou=0;
fen=3;
seuil=0.05;

while FileStatus.m_StreamClosed < 1
%while FileStatus.m_StreamClosed < 1 & ilec<=3
    moReadChunk();
    FileStatus= moGetFileStatus();
    ilec=ilec+1;


    if (ilec==1) %premier ping lu, on pr�sente les sondeurs et choisit celui � traiter


        %presentation des sondeurs
        list_sounder=moGetSounderList;
        nb_snd=length(list_sounder);
        disp(' ');
        disp('pr�sentation des sondeurs');
        disp('-------------------------');
        disp(' ');
        disp(['nb sondeurs = ' num2str(nb_snd)]);
        for isdr = 1:nb_snd
            nb_transduc=list_sounder(isdr).m_numberOfTransducer;
            if (list_sounder(isdr).m_SounderId<10)
%                ind_list(list_sounder(isdr).m_SounderId)=isdr;
                ind_list(isdr)=list_sounder(isdr).m_SounderId;                
                %disp(['sondeur num�ro ' num2str(isdr) ':    ' '(index: ' num2str(list_sounder(isdr).m_SounderId) ')   nb trans:' num2str(nb_transduc)]);
                disp(['sondeur num�ro ' num2str(isdr) ':    '  '   nb transducteurs:' num2str(nb_transduc)]);
                for itr=1:nb_transduc
                    nb_soft_chnel(isdr,itr)=list_sounder(isdr).m_transducer(itr).m_numberOfSoftChannel;
                    disp(['   transducteur num�ro ' num2str(itr) ':    ' 'nom: ' list_sounder(isdr).m_transducer(itr).m_transName '   nb soft_chnl:' num2str(nb_soft_chnel(isdr,itr))]);
                    for isf=1:nb_soft_chnel(isdr,itr)
                        disp(['       soft_chn ' num2str(isf) ':    ' '   freq: ' num2str(list_sounder(isdr).m_transducer(itr).m_SoftChannel(isf).m_acousticFrequency/1000) ' kHz']);
                        if (list_sounder(isdr).m_transducer(itr).m_SoftChannel(isf).m_acousticFrequency==38000)
                            isdr_ref=isdr;  % selection du 38  maitre
                        end
                        if (nb_soft_chnel(isdr,itr)>10 || ~isempty(strfind(list_sounder(isdr).m_transducer(itr).m_transName,'120-2.5x10')))
                            filt_sdr(end+1)=isdr;  % selection du ME70 et/ou du 120 horizontal
                        end
                    end
                end
            else
                disp('Pb sondeur Id');
            end
        end
        
        if ~exist('isdr_ref') disp('Pb, pas de 38kHz maitre?'); return; end
        
        heure_hac_tot=[];
        ind_ping=zeros(1,nb_snd);
        saut_rec=[];    
        for isdr=1:nb_snd
            ind_sav{isdr}=[];
        end
    end
    
    nb_pings=moGetNumberOfPingFan();

        
    for index= nb_pings_prec:nb_pings-1  %boucle sur les pings
        MX= moGetPingFan(index);
        SounderDesc =  moGetFanSounder(MX.m_pingId);
        num_sdr=find(ind_list==SounderDesc.m_SounderId);
        ind_ping(num_sdr)=ind_ping(num_sdr)+1;
        heure_hac_tot(num_sdr,ind_ping(num_sdr))=MX.m_meanTimeCPU+MX.m_meanTimeFraction/10000;
        %ind_ping_hac(num_sdr,ind_ping(num_sdr))=index;
        ind_ping_hac(num_sdr,ind_ping(num_sdr))=MX.m_pingId;
        
        
        % detection d'anomalie
        if num_sdr==2 & ind_ping(num_sdr)>=2*(fen+trou)+1
            iping=ind_ping(num_sdr)-trou-fen; % ping � tester
            rec=heure_hac_tot(num_sdr,iping)-heure_hac_tot(num_sdr,iping-1);
            moy1=(heure_hac_tot(num_sdr,iping-trou-1)-heure_hac_tot(num_sdr,iping-trou-fen))/(fen-1);
            moy2=(heure_hac_tot(num_sdr,iping+trou+fen)-heure_hac_tot(num_sdr,iping+trou+1))/(fen-1);
            saut=min(abs(rec-moy1),abs(rec-moy2));
            if saut>seuil
                saut_rec(end+1)=iping;
                for isdr=1:min(nb_snd,size(heure_hac_tot,1))
                    ind_er=find(heure_hac_tot(isdr,:)<heure_hac_tot(isdr_ref,saut_rec(end)+1) & heure_hac_tot(isdr,:)>=heure_hac_tot(isdr_ref,saut_rec(end)));
                    ind_sav{isdr}=[ind_sav{isdr} ind_er];
                    %effacement des pings pour les sondeurs � filtrer
                    if ~isempty(find(isdr==filt_sdr))
                        for ier=1:length(ind_er)
                            pingId=ind_ping_hac(isdr,ind_er(ier));
                            for itr=1:list_sounder(isdr).m_numberOfTransducer
                                datalist = moGetPolarMatrix(pingId,itr-1);
                                datalist.m_Amplitude=datalist.m_Amplitude*0-120*100;
                                moSetPolarMatrix(pingId,itr-1,datalist);
                                %moSetPingFan(MX_cur.m_pingId,MX_cur);
                            end
                        end
                    end
                end
            end
        end
    end
    nb_pings_prec=nb_pings;
end;
fprintf('End of File');

%% ecriture hac    
moStartWrite(chemin_save,'filtre_pstes_rec_');
moStopWrite;

%% affichage optionnel
if (1) %r�currences et filtrage
    col='bgr';
    rec=heure_hac_tot(:,2:end)-heure_hac_tot(:,1:end-1);
    figure(20);hold off
    str_lgd=[];
    if ~isempty(saut_rec)
        figure(20);plot(heure_hac_tot(isdr_ref,saut_rec),rec(isdr_ref,saut_rec-1),'mo','LineW',7);hold on;grid on
        str_lgd{end+1}='sauts d�tect�s';
    end
    for isdr=1:min(nb_snd,size(heure_hac_tot,1))
        nb_p=min(find(heure_hac_tot(isdr,:)==0))-1;
        if isempty(nb_p) nb_p=size(heure_hac_tot,2);end
        if nb_p>0
            figure(20);plot(heure_hac_tot(isdr,2:nb_p),rec(isdr,1:nb_p-1),[col(isdr) '-']);hold on;
            ind_er=ind_sav{isdr};
            figure(20);hold on;plot(heure_hac_tot(isdr,ind_er),rec(isdr,max(1,ind_er-1)),[col(isdr) 'o'],'LineW',3)
            str_lgd{end+1}=list_sounder(isdr).m_transducer(1).m_transName;
            if ~isempty(ind_er) str_lgd{end+1}='pings filtr�s (si ME70/120h)';end
        end
    end
    xlabel('heure');ylabel('recurrence');grid on;
    legend(str_lgd)
end
if (0) % synchro sondeurs et filtrage
    
    isdr=2; %choix du sondeur a comparer au maitre
    
    nb_p=min(find(heure_hac_tot(isdr,:)==0))-1;
    nb_p_ref=min(find(heure_hac_tot(isdr_ref,:)==0))-1;
    if isempty(nb_p) nb_p=size(heure_hac_tot,2);end
    if isempty(nb_p_ref) nb_p_ref=size(heure_hac_tot,2);end
    nb_p=min(nb_p,max(find(heure_hac_tot(isdr,1:nb_p)<heure_hac_tot(isdr_ref,nb_p_ref))));
    ip0=min(find(heure_hac_tot(isdr,:)>=heure_hac_tot(isdr_ref,1)));
    clear delta1 delta2
    str_lgd=[];
    for ip=ip0:nb_p
        ip_inf=max(find(heure_hac_tot(isdr_ref,:)~=0 & heure_hac_tot(isdr_ref,:)<=heure_hac_tot(isdr,ip)));
        delta1(ip-ip0+1)=heure_hac_tot(isdr,ip)-heure_hac_tot(isdr_ref,ip_inf);
        delta2(ip-ip0+1)=-(heure_hac_tot(isdr,ip)-heure_hac_tot(isdr_ref,ip_inf+1));
    end
    figure(21);hold off
    figure(21);plot(heure_hac_tot(isdr,ip0:nb_p),delta1,'LineW',2);hold on;
    str_lgd{1}=['delta ' list_sounder(isdr).m_transducer(1).m_transName '-ma�tre'];
    figure(21);plot(heure_hac_tot(isdr,ip0:nb_p),delta2,'r','LineW',2);hold on;
    str_lgd{2}=['delta ma�tre - ' list_sounder(isdr).m_transducer(1).m_transName];
    ind_er=ind_sav{isdr};
    figure(21);hold on;plot(heure_hac_tot(isdr,ind_er),min(delta1(ind_er-ip0+1),delta2(ind_er-ip0+1)),'mo','LineW',3)
    if ~isempty(ind_er) str_lgd{3}=['ping filtr�s ' list_sounder(isdr).m_transducer(1).m_transName];end
    xlabel('heure');ylabel('delta ping en secondes');grid on;
    legend(str_lgd);
    
end