% routine permettant de visualiser le r�sum� des r�currences et des pings
% filtr�s des hac d'un r�pertoire

%la datation des sondeurs �tant erron�e, c'est l'ordre des pings dans les
%hac qui est pris comme r�f�rence pour comparer les instants de 2 sdrs

m=6; %num�ro du RUN � analyser
if m<10
    RUN=strcat('RUN00',num2str(m))
else
    RUN=strcat('RUN0',num2str(m))
end;

%chemin_hac=['L:\PELGAS11\',RUN,'\'];
%chemin_save=['C:\data\test_parasites\pstes_rec\',RUN,'\'];
%chemin_save_mat=['C:\data\test_parasites\pstes_rec\',RUN,'\matlab\'];

filelist = ls([chemin_save_mat,'*.mat']);  % ensemble des fichiers
nb_files = size(filelist,1);  % nombre de fichiers

for numfile = 1:nb_files  % boucle sur l'ensemble des fichiers � traiter
    hacfilename = filelist(numfile,:);
    load([chemin_save_mat hacfilename(1:end-4)]);
    nb_snd=size(heure_hac_tot,1);
    
    isdr=2; %choix du sondeur a comparer au maitre
    
    if (isdr<=nb_snd)
        nb_p=min(find(heure_hac_tot(isdr,:)==0))-1;
        nb_p_ref=min(find(heure_hac_tot(isdr_ref,:)==0))-1;
        if isempty(nb_p) nb_p=size(heure_hac_tot,2);end
        if isempty(nb_p_ref) nb_p_ref=size(heure_hac_tot,2);end
        nb_p=min(nb_p,max(find(ind_ping_hac(isdr,1:nb_p)<ind_ping_hac(isdr_ref,nb_p_ref))));
        ip0=min(find(ind_ping_hac(isdr,:)>=ind_ping_hac(isdr_ref,1)));
        clear delta1 delta2
        str_lgd=[];
        for ip=ip0:nb_p
            ip_inf=max(find(ind_ping_hac(isdr_ref,:)~=0 & ind_ping_hac(isdr_ref,:)<=ind_ping_hac(isdr,ip)));
            delta1(ip-ip0+1)=heure_hac_tot(isdr,ip)-heure_hac_tot(isdr_ref,ip_inf);
            delta2(ip-ip0+1)=-(heure_hac_tot(isdr,ip)-heure_hac_tot(isdr_ref,ip_inf+1));
        end
        figure(21);hold off
        figure(21);plot((ip0:nb_p),delta1,'LineW',2);hold on;
        str_lgd{1}=['delta ' list_sounder(isdr).m_transducer(1).m_transName '-ma�tre'];
        figure(21);plot((ip0:nb_p),delta2,'r','LineW',2);hold on;
        str_lgd{2}=['delta ma�tre - ' list_sounder(isdr).m_transducer(1).m_transName];
        ind_er=ind_sav{isdr};
        figure(21);hold on;plot(ind_er,min(delta1(ind_er-ip0+1),delta2(ind_er-ip0+1)),'mo','LineW',3)
        if ~isempty(ind_er) str_lgd{3}=['ping filtr�s ' list_sounder(isdr).m_transducer(1).m_transName];end
        xlabel('ping');ylabel('delta ping en secondes');grid on;
        legend(str_lgd);
        title([hacfilename ' n�' num2str(numfile)])
        
        % en fct de l'heure de ping
         col='bgr';
        rec=heure_hac_tot(:,2:end)-heure_hac_tot(:,1:end-1);
        u=heure_hac_tot;
        heure_hac_tot=heure_hac_tot-heure_hac_tot(1,1);
        figure(20);hold off
        str_lgd=[];
        if ~isempty(saut_rec)
            figure(20);plot(heure_hac_tot(isdr_ref,saut_rec),rec(isdr_ref,saut_rec-1),'mo','LineW',7);hold on;grid on
            str_lgd{end+1}='sauts d�tect�s';
        end
        for isdr=1:min(nb_snd,size(heure_hac_tot,1))
            nb_p=min(find(heure_hac_tot(isdr,:)+u(1,1)==0))-1;
            if isempty(nb_p) nb_p=size(heure_hac_tot,2);end
            if nb_p>0
                figure(20);plot(heure_hac_tot(isdr,2:nb_p),rec(isdr,1:nb_p-1),[col(isdr) '-']);hold on;
                ind_er=ind_sav{isdr};
                figure(20);hold on;plot(heure_hac_tot(isdr,ind_er),rec(isdr,max(1,ind_er-1)),[col(isdr) 'o'],'LineW',3)
                str_lgd{end+1}=list_sounder(isdr).m_transducer(1).m_transName;
                if ~isempty(ind_er) str_lgd{end+1}='pings filtr�s (si ME70/120h)';end
            end
        end
        figure(20);ylim([-1 2]);
        xlabel('secondes');ylabel('recurrence');grid on;
        legend(str_lgd)
        heure_hac_tot=u;
        
        % en fct du n� de ping
        col='bgr';
        rec=heure_hac_tot(:,2:end)-heure_hac_tot(:,1:end-1);
        figure(22);hold off
        str_lgd=[];
        if ~isempty(saut_rec)
            figure(22);plot(ind_ping_hac(isdr_ref,saut_rec),rec(isdr_ref,saut_rec-1),'mo','LineW',7);hold on;grid on
            str_lgd{end+1}='sauts d�tect�s';
        end
        for isdr=1:min(nb_snd,size(heure_hac_tot,1))
            nb_p=min(find(heure_hac_tot(isdr,:)==0))-1;
            if isempty(nb_p) nb_p=size(heure_hac_tot,2);end
            if nb_p>0
                figure(22);plot(ind_ping_hac(isdr,2:nb_p),rec(isdr,1:nb_p-1),[col(isdr) '-']);hold on;
                ind_er=ind_sav{isdr};
                figure(22);hold on;plot(ind_ping_hac(isdr,ind_er),rec(isdr,max(1,ind_er-1)),[col(isdr) 'o'],'LineW',3)
                str_lgd{end+1}=list_sounder(isdr).m_transducer(1).m_transName;
                if ~isempty(ind_er) str_lgd{end+1}='pings filtr�s (si ME70/120h)';end
            end
        end
        figure(22);ylim([-1 2]);
        xlabel('ping hac');ylabel('recurrence');grid on;
        legend(str_lgd)
        
        pause;
        %' '
    end
end