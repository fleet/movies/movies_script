% Filter 2 : Filtering the pings with over-detection.
% Function to use when filter 2 is chosen.
% -----------------------------------------------------------------------
% Inputs:
%   -> data: 3 dimensional matrix (pings x depths x frequencies)
%   -> threshold: filtering threshold on the pings arithmeric sum over
%   depths. 
% -----------------------------------------------------------------------
% Outputs:
%   -> Echog: Filtered echogram (saved in jpeg format)
%   -> FilteredData: pings with detections under the threshold 
%   -> ResidualData: pings with detections over the threshold
% -----------------------------------------------------------------------
% B.REMOND 15/10/2012                           Last review : 08/02/2013
% -----------------------------------------------------------------------
% -----------------------------------------------------------------------

function [Echog,FilteredData,ResidualData] = F2WP (data,threshold)

global fq THRESH run NbFreq depth

for i = 1:NbFreq

    datafreq = data (:,:,i);    
    Sumdatasv = sum (10.^(datafreq./10),2);

    % filtering
    FilterWP = Sumdatasv < threshold;
    
    % apply the filter on all frequencies
    FilteredData = data (FilterWP,:,:);
    ResidualData = data (~FilterWP,:,:);
    
    % Filtered median profils calculation 
    FilteredProfils = [depth',squeeze(median(FilteredData,1))];

    % Filtering quality criterion (% number of ping deleted)
    FilterQuality = 100* size(ResidualData,1)/size(data,1);

    
end

fifi=figure (2);
set(fifi, 'Units','Normalized','OuterPosition',[0 0 1 1]);
nfig = [1 2 3 5 6 7];
for i = 1:NbFreq
    subplot (2,4,nfig(i))
        DrawEchoGram_brut (FilteredData(:,:,i)',-45,-90)
        colorbar
        colorbar ('location','southoutside')
        title (['Filtered echogram RUN',num2str(run),' ',num2str(fq(i)),'kHz'])
end
subplot (2,4,[4 8])
plot (FilteredProfils(:,2:7), -FilteredProfils(:,1), 'LineWidth',2)
title (sprintf(['Filtered median profils \n Quality:',num2str(FilterQuality),' percent of pings removed']))
legend ('18','38','70','120','200','333')
xlim([-100 -40]) 


% result
Echog = figure (2)
FilteredData;
ResidualData;

end
 
