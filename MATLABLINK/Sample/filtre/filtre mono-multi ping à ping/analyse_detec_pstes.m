
%chemin_save=['C:\data\Campagnes\CLASS08\filtre\RUN001\ME70\'];
%chemin_save=['C:\data\Campagnes\demo\IBTS10\filtre\ME70\'];
chemin_save=['C:\data\Campagnes\demo\hac_a_filtrer\filtre\EK5001\RUN001\'];

filelist = dir([chemin_save,'*.mat']);  % ensemble des fichiers
nb_files = size(filelist,1);  % nombre de fichiers

%remise dans l'ordre des fichiers si n�cessaire
if (0)
    clear date
    for numfile = 1:nb_files
        date(numfile)=filelist(numfile).datenum;
    end
    [d,ind]=sort(date);
    filelist=filelist(ind);
end

lname_sdr=[];
lname_hac=[];
ldif_sa=[];
sa_filtre_cat=[];
time_cat=[];
ltime=[];
time_deb=[];
sa_cat=[];

prem=1;
nb_f=0;
for numfile = 1:nb_files  % boucle sur l'ensemble des fichiers � traiter
%for numfile = 1:5  % boucle sur l'ensemble des fichiers � traiter
%numfile
    nb_f=nb_f+1;
    load([chemin_save filelist(numfile).name]);
    
     if (prem==1)
           name_ref=name_sdr;
           disp(['sondeur filtr�: ' name_ref]);
           prem=0;
     elseif strcmp(name_ref,name_sdr)==0
         disp(['chgt sdr filtr� ' filelist(numfile).name ' :' name_sdr])
     end
     
     lname_sdr{nb_f}=name_sdr;
     lname_hac{nb_f}=filelist(numfile).name(1:end-4);
     ldif_sa{nb_f}=dif_sa;
     ltime{nb_f}=time;
     sa_filtre_cat=[sa_filtre_cat sa_filtre];
     time_cat=[time_cat time];
     sa_cat=[sa_cat sa];
     
     if ~isempty(time)
        time_deb(nb_f)=time(1);
     elseif ~isempty(time_deb)
        time_deb(nb_f)=time_deb(nb_f-1);
     end
     
     
end

ideb=min(find(time_deb>0));
if (~isempty(ideb))
    date=time_cat/(24*3600)+datenum('01-Jan-1970');
    dif=sa_cat-sa_filtre_cat;
    figure(1);plot(date,dif,'-*');datetickzoom('x',13);grid on
    title(['diff�rence de sa, sondeur filtr� ' name_ref ', total=' num2str(sum(dif))]);
    
    figure(2);plot(date,sa_cat,'-*');datetickzoom('x',13);grid on
    figure(2);hold on;plot(date,sa_filtre_cat,'r-*');datetickzoom('x',13);grid on;hold off
    title(['sa, sondeur ' name_ref ', total=' num2str(sum(sa_cat))]);
    legend('fichier initial','fichier filtr�')
    
    ind=find(sa_cat==0);
    rap=(sa_cat-sa_filtre_cat)./sa_cat;
    rap(ind)=0;
    figure(3);plot(date,rap,'-*');datetickzoom('x',13);grid on
    title(['proportion de sa filtr�, sondeur filtr� ' name_ref ', total=' num2str(100*sum(dif)/sum(sa_cat)) '%']);
    
    if (0) %d�tail sur un fichier
        dateh=ltime_pstes{1}/(24*3600)+datenum('01-Jan-1970');
        figure(4);plot(dateh(2:end),24*3600*(dateh(2:end)-dateh(1:end-1)),'-*');datetickzoom('x',13);grid on
        title('dur�e entre 2 pings parasit�s')
    end
else
    disp('pas de parasites')
end