%% Lance le d�parasitage de tous les fichiers contenus dans un repertoire donne 
% renseigner le chemin du r�pertoire � filtrer et le r�pertoire o�
% sauvegarder les hac filtr�s

    clear all;
    
    %chargement config
    chemin_config = '.\config_filtre_directory\';
    chemin_config_reverse = strrep(chemin_config, '\', '/');
    moLoadConfig(chemin_config_reverse);
    config=1;

    chemin_hac_dir='C:\data\parasites\fichier_ini\';
    chemin_hac_dir='C:\data\algues\';
    chemin_save='C:\data\parasites\filtre_dir\';
    
    if ~exist(chemin_save,'dir')
            mkdir(chemin_save)
    end
        
    choix_sdr=0;


    if choix_sdr==0 %sondeur a filtrer pas encore choisi

        if ~exist(chemin_hac_dir,'dir')
            disp('le r�pertoire n existe pas');
            return
        end
        
        moOpenHac(chemin_hac_dir);
        %moOpenHac(FileName);
        
        %presentation des sondeurs
        list_sounder=moGetSounderList;
        nb_snd=length(list_sounder);
        disp(' ');
        disp('pr�sentation des sondeurs');
        disp('-------------------------');
        disp(' ');
        disp(['nb sondeurs = ' num2str(nb_snd)]);
        for isdr = 1:nb_snd
            nb_transduc=list_sounder(isdr).m_numberOfTransducer;
            if (list_sounder(isdr).m_SounderId<10)
%                ind_list(list_sounder(isdr).m_SounderId)=isdr;
                ind_list(isdr)=list_sounder(isdr).m_SounderId;                
                %disp(['sondeur num�ro ' num2str(isdr) ':    ' '(index: ' num2str(list_sounder(isdr).m_SounderId) ')   nb trans:' num2str(nb_transduc)]);
                disp(['sondeur num�ro ' num2str(isdr) ':    '  '   nb transducteurs:' num2str(nb_transduc)]);
                for itr=1:nb_transduc
                    nb_soft_chnel(isdr,itr)=list_sounder(isdr).m_transducer(itr).m_numberOfSoftChannel;
                    disp(['   transducteur num�ro ' num2str(itr) ':    ' 'nom: ' list_sounder(isdr).m_transducer(itr).m_transName '   nb soft_chnl:' num2str(nb_soft_chnel(isdr,itr))]);
                    for isf=1:nb_soft_chnel(isdr,itr)
                        disp(['       soft_chn ' num2str(isf) ':    ' '   freq: ' num2str(list_sounder(isdr).m_transducer(itr).m_SoftChannel(isf).m_acousticFrequency/1000) ' kHz']);

                    end
                end
            else
                disp('Pb sondeur Id');
            end
        end
        choix_sdr=1;
        disp(' ');
        disp(' ');
        num_sondeur=input('>>> num�ro du sondeur � filtrer: ');
        num_trans=input('>>> num�ro du transducteur � filtrer: ');

        if (isempty(num_sondeur) || isempty(num_trans) || ~isnumeric(num_sondeur) || ~isnumeric(num_trans))
            disp(' ');
            disp('ERREUR: de saisie des num�ros');
            return;    
        elseif (num_sondeur>nb_snd)
            disp(' ');
            disp('ERREUR: le num�ro du sondeur � filtrer n existe pas. Corriger le num�ro');
            return;
        elseif(num_trans>list_sounder(num_sondeur).m_numberOfTransducer)
            disp(' ');
            disp('ERREUR: l index du transducteur � filtrer n existe pas. Corriger l index');
            return;
        end
    end

    moLoadConfig(chemin_config_reverse);%mieux vaut recharger, au cas o� il y aurait eu des fichiers de config dans le r�pertoire � lire
    exec_filtre_parasites_p2p_fan_hac_directory(chemin_hac_dir,chemin_save,chemin_config,config,num_sondeur,num_trans);


