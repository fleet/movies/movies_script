function exec_filtre_parasites_p2p_fan_hac_directory(chemin_hac_dir,chemin_save,chemin_config,config,num_sondeur,num_trans)
%function exec_filtre_parasites_p2p_fan_hac_directory
% chemin_hac : r�pertoire contenant le fichier hac � filtrer
% hacfilename : nom du fichier hac � filtrer
% chemin_save : r�pertoire o� sauver le fichier hac filtr�
% chemin_config : r�pertoire contenant la configuration movies3D � utiliser (un r�pertoire exemple \config_filtre\ est joint). 
% config : bool�en (0 ou 1) indiquant si on a d�j� charg� la configuration movies3D sous matlab (je ne sais pas pourquoi on ne peut pas le faire 2 fois de suite). 
% num_sondeur: num�ro du sondeur contenant le transducteur � filtrer
% num_trans: num�ro du transducteur � filtrer


% 
% %% sondeur � filtrer � renseigner
% num_sondeur=1;
% num_trans=5;

%% chargement des param�tres du filtre
seuil_rsb=5;    %dB seuil de d�tection de parasite (rsb)
seuil_sv=-150; %dB seuil de d�tection de parasite (sv)
l_parasite=4; %ech longueur minimum de parasite recherch�
l_erase=13; %ech longueur d'effacement du parasite d�tect�

% cas multifaisceau (param�tres inutilis�s en monofaisceau)

% d�calage des faisceaux � l'�mission (nb d'�chantillons)
tx_dec=[41 33 33 25 25 17 17 9 9 1 1 1 1 9 9 17 17 25 25 33 33]-1; %21 fscx gpe4
%tx_dec=[ones(1,42)*8 ones(1,45)*4 ones(1,57)*0 ones(1,57)*2 ones(1,45)*6 ones(1,42)*10]; %EM302
tol=10; % tol�rance en nombre d'�chantillons sur la simultan�it� d'apparition des parasites sur toutes les voies
prop=0.25; % proportion de voies devant pr�senter simultan�ment un parasite pour qu'il soit retenu

%% fichier � lire
if (0)    
    chemin_config = '.\config_filtre2\';
    chemin_config_reverse = strrep(chemin_config, '\', '/');
    moLoadConfig(chemin_config_reverse);
    config=1;

    chemin_hac_dir='E:\data\test_parasites\ME70b\';
    chemin_save='E:\data\test_parasites\ME70b\filtre2\';
    
    
    if ~exist(chemin_save,'dir')
            mkdir(chemin_save)
    end
    if ~exist(chemin_hac_dir,'dir')
        disp('le r�pertoire n existe pas');
        return
    end
    moOpenHac(chemin_hac_dir);
    
    num_sondeur=2;
    num_trans=1;
    
end

%FileName = [chemin_hac,hacfilename];
    
%% param�tres de lecture du hac
ParameterR= moReaderParameter();
ParameterDef=moLoadReaderParameter(ParameterR);
nchunk=ParameterDef.m_chunkNumberOfPingFan;
list_sounder=moGetSounderList;
ParameterDef.m_chunkSounderId=list_sounder(num_sondeur).m_SounderId; %impose de prendre le sondeur cible pour le goto
moSaveReaderParameter(ParameterR,ParameterDef);

% ParameterKernel= moKernelParameter();
% ParameterDef=moLoadKernelParameter(ParameterKernel);
% ParameterDef.m_bAutoLengthEnable=0;
% ParameterDef.m_nbPingFanMax=nchunk+10;
% moSaveKernelParameter(ParameterKernel,ParameterDef);
% clear ParameterDef;
        
%moOpenHac(FileName);

%% lecture et filtrage
prem_lec=1;
iping=0;
FileStatus= moGetFileStatus;
nb_pings_prec=0;
ilec=0;
scan=1;
sa_filtre=[];
sa=[];
time=[];

while FileStatus.m_StreamClosed < 1 || ilec==0
%while (ilec <= 5 & FileStatus.m_StreamClosed < 1)
    ilec=ilec+1;
    
    %% lecture %%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    disp('debut readchunk')
    moReadChunk();
    disp('fin readchunk')
    FileStatus= moGetFileStatus();
    
    
    %vide la m�moire des pings pr�c�dents �ventuellement erron�s
    if ilec>1
        MXu= moGetPingFan(0);
        moRemovePing(MXu.m_pingId);%enl�ve de toute mani�re le premier ping (le dernier du chunk pr�c�dent dont les donn�es ont �t� �cras�es)
        MXu= moGetPingFan(0);
        while MXu.m_pingHacId<MX_next.m_pingHacId
             moRemovePing(MXu.m_pingId);
             MXu= moGetPingFan(0);
        end
        if MXu.m_pingHacId~=MX_next.m_pingHacId % v�rifie qu'on a bien repris sur le dernier ping du chunck pr�c�dent, le Goto ne marche pas sur le fichier TASA de J�r�mie 
            disp('!!!!!!!!!!!!!!!!!! Probl�me de Goto');
            pause(1);
        end
        MX_next.m_pingId=MXu.m_pingId; % il faut bien prendre l'Id du MX_next nouvellement lu (pas celui de l'�cras�)
    end
    
    if (prem_lec==1) %premier ping lu, on pr�sente les sondeurs et choisit celui � traiter
        prem_lec=0;
        
        %presentation des sondeurs
        list_sounder=moGetSounderList;
        nb_snd=length(list_sounder);
        disp(' ');
        disp('pr�sentation des sondeurs');
        disp('-------------------------');
        disp(' ');
        disp(['nb sondeurs = ' num2str(nb_snd)]);
        for isdr = 1:nb_snd
            nb_transduc=list_sounder(isdr).m_numberOfTransducer;
            if (list_sounder(isdr).m_SounderId<10)
%                ind_list(list_sounder(isdr).m_SounderId)=isdr;
                ind_list(isdr)=list_sounder(isdr).m_SounderId;                
                %disp(['sondeur num�ro ' num2str(isdr) ':    ' '(index: ' num2str(list_sounder(isdr).m_SounderId) ')   nb trans:' num2str(nb_transduc)]);
                disp(['sondeur num�ro ' num2str(isdr) ':    '  '   nb transducteurs:' num2str(nb_transduc)]);
                for itr=1:nb_transduc
                    nb_soft_chnel(isdr,itr)=list_sounder(isdr).m_transducer(itr).m_numberOfSoftChannel;
                    disp(['   transducteur num�ro ' num2str(itr) ':    ' 'nom: ' list_sounder(isdr).m_transducer(itr).m_transName '   nb soft_chnl:' num2str(nb_soft_chnel(isdr,itr))]);
                    for isf=1:nb_soft_chnel(isdr,itr)
                        disp(['       soft_chn ' num2str(isf) ':    ' '   freq: ' num2str(list_sounder(isdr).m_transducer(itr).m_SoftChannel(isf).m_acousticFrequency/1000) ' kHz']);

                    end
                end
            else
                disp('Pb sondeur Id');
            end
        end
%         
%         
%         %a renseigner
%         num_sondeur=1;
%         num_trans=2;
                
        if (num_sondeur>nb_snd)
            disp(' ');
            disp('ERREUR: le num�ro du sondeur � filtrer n existe pas. Corriger le num�ro');
            return;
        elseif(num_trans>list_sounder(num_sondeur).m_numberOfTransducer)
            disp(' ');
            disp('ERREUR: l index du transducteur � filtrer n existe pas. Corriger l index');
            return;
        end
        
              
        disp(' ');
        disp(['(choix actuel du sondeur � filtrer: ' list_sounder(num_sondeur).m_transducer(num_trans).m_transName ')']);
        
        disp(' ');
        disp('routine en pause, v�rifier les choix sondeur/transducteur');
        
         %pause; % pause � commenter (surtout en batch)
    end
    
    
    dec_tir=ceil(3*list_sounder(num_sondeur).m_transducer(num_trans).m_pulseDuration/list_sounder(num_sondeur).m_transducer(num_trans).m_timeSampleInterval);
    
    nb_pings=moGetNumberOfPingFan();
    
    if FileStatus.m_StreamClosed==1 % il faudra veiller � traiter le dernier ping du fichier
        nb_pings=nb_pings+1;
    end
    
    for index= 0:nb_pings-1
    %for index= nb_pings_prec:nb_pings-1
    %for index=1:nb_pings-1
        if FileStatus.m_StreamClosed==1 & index==nb_pings-1 %traitement dernier ping du fichier
            MX=MX_cur;
        else
            MX= moGetPingFan(index);
        end
       
        SounderDesc =  moGetFanSounder(MX.m_pingId);
        % lecture et cr�ation de la matrice de donn�es sv_data � filtrer
        if SounderDesc.m_SounderId == ind_list(num_sondeur)
            datalist = moGetPolarMatrix(MX.m_pingId,num_trans-1);
            sv_data=(datalist.m_Amplitude/100).';
            iping=iping+1;

            %% filtrage %%%%%%%%%%%%%%%%%%%
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            nbeams=size(sv_data,1);
            nech=size(sv_data,2);
            
            if nbeams<2 | prop==0 % modification des param�tres en monofaisceau
                tx_dec=zeros(1,nbeams);
                tol=1;
            end
            
            %compensation du recalage temporel des donn�es en fonction des instants
            % d'emission par voie
            %svTxComp=zeros(nbeams,nech-max(tx_dec));
            svTxComp=zeros(nbeams,nech);
            for ib=1:nbeams
                svTxComp(ib,1:nech-(max(tx_dec)-tx_dec(ib)))=squeeze(sv_data(ib,1+max(tx_dec)-tx_dec(ib):end)).';
            end
            
            %recalage en profondeur en fonction du pilonnement
            for ib=1:nbeams
                steer_ath(ib)=SounderDesc.m_transducer(num_trans).m_SoftChannel(1,ib).m_mainBeamAthwartSteeringAngleRad;
            end
            heave=MX.navigationAttitude.sensorHeaveMeter; 
            dec_heave=-round(heave./cos(steer_ath)/SounderDesc.m_transducer(num_trans).m_beamsSamplesSpacing);%inversion de signe...(04/03/15)
            
            svHeaveComp=zeros(nbeams,nech-min(dec_heave));
            if (heave<=0)%inversion de signe...(04/03/15)
                for ib=1:nbeams
                    svHeaveComp(ib,1:nech-dec_heave(ib))=squeeze(svTxComp(ib,1+dec_heave(ib):end)).';
                end
            else
                for ib=1:nbeams
                    svHeaveComp(ib,1-dec_heave(ib):nech-dec_heave(ib))=squeeze(svTxComp(ib,:)).';
                end
            end
            
            %stockage des donn�es ping pr�c�dent/courant/suivant
            if (iping==1)
                svHeaveComp_prev=svHeaveComp;  %donn�es ping pr�c�dent
                svHeaveComp_cur=svHeaveComp;  % donn�es ping trait�
                svHeaveComp_next=svHeaveComp; %donn�es ping suivant
                MX_cur= MX;
                MX_next= MX;
                MX_prev= MX;
                sv_data_cur=sv_data;
                sv_data_next=sv_data;
                dec_heave_cur=dec_heave;
                dec_heave_prev=dec_heave;
                dec_heave_next=dec_heave;
                heave_cur=heave;
                heave_next=heave;
                svTxComp_cur=svTxComp;
                svTxComp_next=svTxComp;
            elseif (iping==2) %cas particulier pour filtrer le premier ping (compar� deux fois au 2e)
                svHeaveComp_prev=svHeaveComp;  %donn�es ping pr�c�dent
                svHeaveComp_cur=svHeaveComp_next;  % donn�es ping trait�
                svHeaveComp_next=svHeaveComp; %donn�es ping suivant
                MX_cur= MX_next;
                MX_prev= MX;
                MX_next= MX;
                sv_data_cur=sv_data_next;
                sv_data_next=sv_data;
                dec_heave_cur=dec_heave_next;
                dec_heave_next=dec_heave;
                dec_heave_prev=dec_heave;
                heave_cur=heave_next;
                heave_next=heave;
                svTxComp_cur=svTxComp_next;
                svTxComp_next=svTxComp;
            else
                svHeaveComp_prev=svHeaveComp_cur;  %donn�es ping pr�c�dent
                svHeaveComp_cur=svHeaveComp_next;  % donn�es ping trait�
                svHeaveComp_next=svHeaveComp; %donn�es ping suivant
                MX_prev= MX_cur;
                MX_cur= MX_next;
                MX_next= MX;
                sv_data_cur=sv_data_next;
                sv_data_next=sv_data;
                dec_heave_prev=dec_heave_cur;
                dec_heave_cur=dec_heave_next;
                dec_heave_next=dec_heave;
                heave_cur=heave_next;
                heave_next=heave;
                svTxComp_cur=svTxComp_next;
                svTxComp_next=svTxComp;
            end

            
            %range mini
            if MX_cur.m_maxRangeWasFound
                nrange_cur=floor(min(MX_cur.beam_data(num_trans).m_bottomRange)/SounderDesc.m_transducer(num_trans).m_beamsSamplesSpacing-max(0,dec_heave_cur));
            else
                nrange_cur=size(svHeaveComp_cur,2);
            end
            if MX_prev.m_maxRangeWasFound
                nrange_prev=floor(min(MX_prev.beam_data(num_trans).m_bottomRange)/SounderDesc.m_transducer(num_trans).m_beamsSamplesSpacing-max(0,dec_heave_prev));
            else
                nrange_prev=size(svHeaveComp_prev,2);
            end
            if MX_next.m_maxRangeWasFound
                nrange_next=floor(min(MX_next.beam_data(num_trans).m_bottomRange)/SounderDesc.m_transducer(num_trans).m_beamsSamplesSpacing-max(0,dec_heave_next));
            else
                nrange_next=size(svHeaveComp_next,2);
            end
            
            %on met tout le monde � la m�me taille
            %nech_min=min([size(svHeaveComp_prev,2),size(svHeaveComp_cur,2),size(svHeaveComp_next,2)]);
            nech_min=min([nrange_prev,nrange_cur,nrange_next]);
            svCom_prev=svHeaveComp_prev(:,1:nech_min);    % 
            svCom_cur=svHeaveComp_cur(:,1:nech_min);  % donn�es de travail
            svCom_next=svHeaveComp_next(:,1:nech_min);    % 
                   

            %filtrage lui-meme
            dif_prev=svCom_cur-svCom_prev;
            dif_next=svCom_cur-svCom_next;
            dif_max=min(dif_prev,dif_next);
            is_det=(dif_max>seuil_rsb & svCom_cur>seuil_sv); %matrice des detections potentielles (echantillons sup�rieurs de seuil_rsb � leurs voisins)
            is_det2=erode(double(is_det),ones(1,l_parasite)); % on garde les detections de longueur au moins egale � l_parasite
            is_det3=dilate(is_det2,ones(1,tol)); % on dilate de la tol�rance
            sum_fan=sum(is_det3,1);
            r_det=find(sum_fan>=max(1,prop*nbeams));% on garde les parasites present sur une partie de la fauchee
            is_det2(:,:)=0;
            is_det2(:,r_det)=1;
            is_det3=dilate(is_det2,ones(1,l_erase)); % on efface le parasite sur une longueur l_erase
            ind_det=find(is_det3);
            sv_temp=svCom_cur;
            svCom_cur(ind_det)=10*log10((10.^(svCom_prev(ind_det)/10)+10.^(svCom_next(ind_det)/10))/2); % on remplace les parasites par la moyenne des ech avant/apres
            
            if MX_cur.m_maxRangeWasFound
                nrange=floor(min(MX_cur.beam_data(num_trans).m_bottomRange)/SounderDesc.m_transducer(num_trans).m_beamsSamplesSpacing);
            else
                nrange=size(sv_temp,2);
            end
            
            sa(iping)=sum(sum(10.^(sv_temp(:,max(1,dec_tir-dec_heave_cur):min(nrange-dec_heave_cur+1,size(sv_temp,2)))/10)))*SounderDesc.m_transducer(num_trans).m_beamsSamplesSpacing*4*pi*1852^2;       
            sa_filtre(iping)=sum(sum(10.^(svCom_cur(:,max(1,dec_tir-dec_heave_cur):min(nrange-dec_heave_cur+1,size(sv_temp,2)))/10)))*SounderDesc.m_transducer(num_trans).m_beamsSamplesSpacing*4*pi*1852^2;       
            
            
            time(iping)=MX_cur.m_meanTimeCPU+MX_cur.m_meanTimeFraction/10000;
            
            %d�compensation du pilonnement
            svTxComp_new=svTxComp_cur;
            if (heave_cur<=0)%inversion de signe...(04/03/15)
                for ib=1:nbeams
                    svTxComp_new(ib,1+dec_heave_cur(ib):nech_min+dec_heave_cur(ib))=svCom_cur(ib,:).';
                end
            else
                for ib=1:nbeams
                    svTxComp_new(ib,1:nech_min+dec_heave_cur(ib))=svCom_cur(ib,1-dec_heave_cur(ib):end).';
                end
            end

            % recalage temporel des donn�es en fonction des instants
            % d'emission par voie
            n4=size(svTxComp_new,2);
            datalist = moGetPolarMatrix(MX_cur.m_pingId,num_trans-1);
            sv_data_new=(datalist.m_Amplitude/100).'; %donn�es finales, relecture car pfs chgt taille
            lgth_data_new=size(sv_data_new,2);
     
            for ib=1:nbeams
               sv_data_new(ib,1+max(tx_dec)-tx_dec(ib):n4+max(tx_dec)-tx_dec(ib))=svTxComp_new(ib,:);
            end
            sv_data_new=sv_data_new(:,1:lgth_data_new);
            
            %% changement donn�es m�moire
            datalist = moGetPolarMatrix(MX_cur.m_pingId,num_trans-1);
            datalist.m_Amplitude=(sv_data_new*100).';      
            moSetPolarMatrix(MX_cur.m_pingId,num_trans-1,datalist);
            moSetPingFan(MX_cur.m_pingId,MX_cur);
            
            %% affichage optionnel
            %if (iping-1>=225)
            if (0)
                ech=[-80 -40];
                %ech=[-150 -10];
                figure(10);subplot(1,4,1);imagesc(svHeaveComp_prev(:,1:nech_min).',ech);title('prev')
                figure(10);subplot(1,4,2);imagesc(svHeaveComp_cur(:,1:nech_min).',ech);title('cur')
                figure(10);subplot(1,4,3);imagesc(svHeaveComp_next(:,1:nech_min).',ech);title('next')
                figure(20);imagesc(is_det3.');title('parasites')
                figure(10);subplot(1,4,4);imagesc(svCom_cur.',ech);colorbar;title('new cur')
                %figure(40);imagesc(svHeaveComp_cur(:,1:nech_min).'-svCom_cur.');title(['ping ' num2str(iping)]);colorbar;title('dif');
                figure(50);imagesc(sv_data_cur.'-sv_data_new.',[-20 20]);title(['ping ' num2str(iping) ', dif sv complet']);colorbar;
                figure(11);imagesc(svHeaveComp_cur(:,1:600).',ech);title('cur')
                figure(12);imagesc(svCom_cur(:,1:600).',ech);title('cur')
                
                pause;
            end
            if(0)
                if exist('sv_mat_compheave')
                    sv_mat_compheave(end+1,1:length(svHeaveComp_cur))=svHeaveComp_cur;
                    sv_mat(end+1,1:length(sv_data))=sv_data;
                    heave_rec(end+1)=heave_cur;
                else
                    sv_mat_compheave(1,:)=svHeaveComp_cur;
                    sv_mat(1,1:length(sv_data))=sv_data;
                    heave_rec=heave_cur;
                end
            end
        end
    end
    

    %% ecriture hac        

    %nom du hac
    MXu=moGetPingFan(0);
    [hh,mm,ss]=read_day_time_hac(MXu.m_meanTimeCPU+MXu.m_meanTimeFraction/10000);
    name_sdr=list_sounder(num_sondeur).m_transducer(num_trans).m_transName;
    hhs=['0' num2str(hh)]; hhs=hhs(end-1:end);
    mms=['0' num2str(mm)]; mms=mms(end-1:end);
    sss=['0' num2str(floor(ss))]; sss=sss(end-1:end);
    
    
    % retire le dernier ping pour ne pas l'�crire (pas filtr�)    
    moRemovePing(MX_next.m_pingId);
    
    moStartWrite(chemin_save,['filtre_pstes_' name_sdr '_' hhs mms sss '_']);
    moStopWrite;
    
    nb_pings_prec=moGetNumberOfPingFan();
    
    %vide la m�moire sauf un ping
    if nb_pings_prec>1
        for index= 0:max(0,nb_pings_prec-2)
             MX= moGetPingFan(0);
             moRemovePing(MX.m_pingId);
        end
    end
    %va au dernier ping lu
    moGoTo(MX_next.m_meanTimeCPU+MX_next.m_meanTimeFraction/10000);
    
end

if ilec>0
    name_sdr=list_sounder(num_sondeur).m_transducer(num_trans).m_transName;
else
    name_sdr='';
end


% %% sauvegarde param�tres filtre
% if (scan==0)
%     %save([chemin_save '\param_filtre'],'seuil_rsb','seuil_sv','l_parasite','l_erase','tx_dec','tol','prop');
% else
%     nb_pings_tot=nb_pings_prec;
%     nb_pings_sdr=iping;
% %    save([chemin_save '\scan_pstes_' name_sdr '_' hacfilename(1:end-4)],'seuil_rsb','seuil_sv','l_parasite','l_erase','tx_dec','tol','prop','time','sa_filtre','name_sdr','sa');
% end

fprintf('End of File');

disp(' ');
disp(' ');
if ilec==0
    disp('pas de donn�es pour ce hac');
else
    disp(['rappel : choix du sondeur filtr� : ' list_sounder(num_sondeur).m_transducer(num_trans).m_transName ]);
end
        