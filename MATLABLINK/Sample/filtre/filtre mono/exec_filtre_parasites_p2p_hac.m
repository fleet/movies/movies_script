%function filtre_parasites

%% chargement des param�tres du filtre
seuil_rsb=3;%dB
seuil_sv=-150; %dB
l_parasite=7; %ech
l_erase=13;%ech

%% fichier � lire
chemin_ini='K:\Basin\DonneesJRoty\1iere-maree-2011\HAC\';
chemin_sav='K:\Basin\DonneesJRoty\1iere-maree-2011\HAC_filtre\';
% FileName = [chemin_ini,'D20110120-T112611.hac'];
% chemin_ini='L:\IBTS10\HAC\RUN002\';
% FileName = [chemin_ini,'IBTS10_002_20100115_164629.hac'];
% chemin_sav=chemin_ini;
%chemin_ini='Q:\SMFH\matlab\parasites\pelgas11\FirstPings_PELGAS11_006_20110501_154920\';
%FileName = [chemin_ini,'FirstPings_PELGAS11_006_20110501_154920.hac'];
%chemin_ini='D:\lebouffant\PELGAS10\pelgas10\hac\';
%FileName = [chemin_ini,'PELGAS10_003_20100428_061111.hac'];
%chemin_ini='D:\lebouffant\data\IBTS10\';
%FileName = [chemin_ini,'IBTS10_002_20100115_164629.hac'];

%% param�tres de lecture du hac
if (0)
    ParameterKernel= moKernelParameter();
    ParameterDef=moLoadKernelParameter(ParameterKernel);
    
    ParameterDef.m_bAutoLengthEnable=1;
    ParameterDef.m_MaxRange = 150;
    ParameterDef.m_bAutoDepthEnable=0;
    ParameterDef.m_bAutoLengthEnable=0;
    ParameterDef.m_bIgnorePhaseData=1;
    ParameterDef.m_bIgnorePingsWithNoNavigation=0;
    moSaveKernelParameter(ParameterKernel,ParameterDef);
    clear ParameterDef;
    
    ParameterR= moReaderParameter();
    ParameterDef=moLoadReaderParameter(ParameterR);
    
    ParameterDef.m_chunkNumberOfPingFan=10000;
    
    moSaveReaderParameter(ParameterR,ParameterDef);
else
    moLoadConfig(strrep(chemin_sav, '\', '/'));
    %
    %     ParameterKernel= moKernelParameter();
    %     ParameterDef=moLoadKernelParameter(ParameterKernel);
    %     ParameterDef.m_bIgnorePhaseData=0;
    %
    %     moSaveKernelParameter(ParameterKernel,ParameterDef);
    %     clear ParameterDef;
    
end

filelist = ls([chemin_ini,'*.hac']);  % ensemble des fichiers
nb_files = size(filelist,1);  % nombre de fichiers

for numfile = 1:nb_files  % boucle sur l'ensemble des fichiers � traiter
    
    hacfilename = filelist(numfile,:);
    FileName = [chemin_ini,hacfilename];
    
    moOpenHac(FileName);
    
    %% lecture et filtrage
    prem_lec=1;
    iping=0;
    FileStatus= moGetFileStatus;
    nb_pings_prec=0;
    ilec=0;
    while FileStatus.m_StreamClosed < 1
        %while (ilec < 9 & FileStatus.m_StreamClosed < 1)
        ilec=ilec+1;
        %% lecture %%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        disp('debut readchunk')
        moReadChunk();
        disp('fin readchunk')
        FileStatus= moGetFileStatus();
        
        if (prem_lec==1) %premier ping lu, on pr�sente les sondeurs et choisit celui � traiter
            prem_lec=0;
            
            %presentation des sondeurs
            list_sounder=moGetSounderList;
            nb_snd=length(list_sounder);
            disp(' ');
            disp(['nb sondeurs = ' num2str(nb_snd)]);
            for isdr = 1:nb_snd
                nb_transduc=list_sounder(isdr).m_numberOfTransducer;
                if (list_sounder(isdr).m_SounderId<10)
                    % ind_list(list_sounder(isdr).m_SounderId)=isdr;
                    disp(['sondeur ' num2str(isdr) ':    ' 'index: ' num2str(list_sounder(isdr).m_SounderId) '   nb trans:' num2str(nb_transduc)]);
                    for itr=1:nb_transduc
                        nb_soft_chnel(isdr,itr)=list_sounder(isdr).m_transducer(itr).m_numberOfSoftChannel;
                        disp(['   trans ' num2str(itr) ':    ' 'nom: ' list_sounder(isdr).m_transducer(itr).m_transName '   nb soft_chnl:' num2str(nb_soft_chnel(isdr,itr))]);
                        for isf=1:nb_soft_chnel(isdr,itr)
                            disp(['       soft_chn ' num2str(isf) ':    ' '   freq: ' num2str(list_sounder(isdr).m_transducer(itr).m_SoftChannel(isf).m_acousticFrequency/1000) ' kHz']);
                            
                        end
                    end
                else
                    disp('Pb sondeur Id');
                end
            end
            
            disp('');
            disp('routine en pause, v�rifier les choix sondeur/transducteur');
            
            %pause;
            
            %a renseigner
            index_sondeur=0;
            index_trans=1;
            
        end
        
        
        
        nb_pings=moGetNumberOfPingFan();
        %nb_pings=100;
        
        for index= nb_pings_prec:nb_pings-1
            MX= moGetPingFan(index);
            SounderDesc =  moGetFanSounder(MX.m_pingId);
            % lecture et cr�ation de la matrice de donn�es sv_data � filtrer
            if SounderDesc.m_SounderId == index_sondeur
                datalist = moGetPolarMatrix(MX.m_pingId,index_trans-1);
                sv_data=(datalist.m_Amplitude/100).';
                
                iping=iping+1;
                
                
                %% filtrage %%%%%%%%%%%%%%%%%%%
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                nbeams=size(sv_data,1);
                nech=size(sv_data,2);
                
                %recalage en profondeur en fonction du pilonnement
                for ib=1:nbeams
                    steer_ath(ib)=SounderDesc.m_transducer(index_trans).m_SoftChannel(1,ib).m_mainBeamAthwartSteeringAngleRad;
                end
                heave=MX.navigationAttitude.sensorHeaveMeter;
                dec_heave=round(heave./cos(steer_ath)/SounderDesc.m_transducer(index_trans).m_beamsSamplesSpacing);
                sv2=zeros(nbeams,nech-min(dec_heave));
                if (heave>=0)
                    for ib=1:nbeams
                        sv2(ib,1:nech-dec_heave(ib))=squeeze(sv_data(ib,1+dec_heave(ib):end)).';
                    end
                else
                    for ib=1:nbeams
                        %sv2(ib,1-dec_heave(ib):end)=squeeze(sv_data(ib,1:nech2+dec_heave(ib))).';
                        sv2(ib,1-dec_heave(ib):nech-dec_heave(ib))=squeeze(sv_data(ib,:)).';
                    end
                end
                
                if (iping==1)
                    sv2_prev=sv2;  %donn�es ping pr�c�dent
                    sv2_cur=sv2;  % donn�es ping trait�
                    sv2_next=sv2; %donn�es ping suivant
                    MX_cur= moGetPingFan(index);
                    MX_next= moGetPingFan(index);
                    sv_data_cur=sv_data;
                    sv_data_next=sv_data;
                    dec_heave_cur=dec_heave;
                    dec_heave_next=dec_heave;
                    heave_cur=heave;
                    heave_next=heave;
                elseif(iping==2)
                    sv2_prev=sv2_prev;  %donn�es ping pr�c�dent
                    sv2_cur=sv2_prev;  % donn�es ping trait�
                    sv2_next=sv2; %donn�es ping suivant
                    MX_cur= MX_next;
                    MX_next= moGetPingFan(index);
                    sv_data_cur=sv_data_next;
                    sv_data_next=sv_data;
                    dec_heave_cur=dec_heave_next;
                    dec_heave_next=dec_heave;
                    heave_cur=heave_next;
                    heave_next=heave;
                else %iping>=3
                    sv2_prev=sv2_cur;  %donn�es ping pr�c�dent
                    sv2_cur=sv2_next;  % donn�es ping trait�
                    sv2_next=sv2; %donn�es ping suivant
                    MX_cur= MX_next;
                    MX_next= moGetPingFan(index);
                    sv_data_cur=sv_data_next;
                    sv_data_next=sv_data;
                    dec_heave_cur=dec_heave_next;
                    dec_heave_next=dec_heave;
                    heave_cur=heave_next;
                    heave_next=heave;
                end
                
                %on met tout le monde � la m�me taille
                nech_min=min([size(sv2_prev,2),size(sv2_cur,2),size(sv2_next,2)]);
                sv3_prev=sv2_prev(:,1:nech_min);    %
                sv3_cur=sv2_cur(:,1:nech_min);  % donn�es de travail
                sv3_next=sv2_next(:,1:nech_min);    %
                
                sv4=sv_data_cur; %donn�es finales
                
                
                %filtrage lui-meme
                dif_prev=sv3_cur-sv3_prev;
                dif_next=sv3_cur-sv3_next;
                dif_max=min(dif_prev,dif_next);
                is_det=(dif_max>seuil_rsb & sv3_cur>seuil_sv); %matrice des detections potentielles (echantillons sup�rieurs de seuil_rsb � leurs voisins)
                is_det2=erode(double(is_det),ones(1,l_parasite)); % on garde les detections de longueur au moins egale � l_parasite
                is_det3=dilate(is_det2,ones(1,l_erase)); % on efface le parasite sur une longueur l_erase
                ind_det=find(is_det3);
                sv3_cur(ind_det)=(sv3_prev(ind_det)+sv3_next(ind_det))/2; % on remplace les parasites par la moyenne des ech avant/apres (moy dB, pas bien mais bon)
                
                %d�compensation du pilonnement
                if (heave_cur>=0)
                    for ib=1:nbeams
                        sv4(ib,1+dec_heave_cur(ib):nech_min+dec_heave_cur(ib))=sv3_cur(ib,:).';
                    end
                    sv4=sv4(:,1:size(sv_data_cur,2));
                else
                    for ib=1:nbeams
                        sv4(ib,1:nech_min+dec_heave_cur(ib))=sv3_cur(ib,1-dec_heave_cur(ib):end).';
                    end
                    sv4=sv4(:,1:size(sv_data_cur,2));
                end
                
                %% changement donn�es m�moire
                datalist = moGetPolarMatrix(MX_cur.m_pingId,index_trans-1);
                datalist.m_Amplitude=(sv4*100).';
                moSetPolarMatrix(MX_cur.m_pingId,index_trans-1,datalist);
                moSetPingFan(MX_cur.m_pingId,MX_cur);
                
                %%affichage optionnel
                %if (iping>=42)
                if (0)
                    figure(10);subplot(1,4,1);imagesc(sv2_prev(:,1:nech_min).',[-80 -40]);title('prev')
                    figure(10);subplot(1,4,2);imagesc(sv2_cur(:,1:nech_min).',[-80 -40]);title('cur')
                    figure(10);subplot(1,4,3);imagesc(sv2_next(:,1:nech_min).',[-80 -40]);title('next')
                    figure(20);imagesc(is_det3.');title('parasites')
                    figure(10);subplot(1,4,4);imagesc(sv3_cur.',[-80 -40]);colorbar;title('new cur')
                    %figure(40);imagesc(sv2_cur(:,1:nech_min).'-sv3_cur.');title(['ping ' num2str(iping)]);colorbar;title('dif');
                    figure(50);imagesc(sv_data_cur.'-sv4.',[-20 20]);title(['ping ' num2str(iping) ', dif sv complet']);colorbar;
                    
                    pause;
                end
                
            end
        end
        nb_pings_prec=nb_pings;
        
    end
    
    %% ecriture hac
    moStartWrite(chemin_sav,'test_');
    moStopWrite;
    
    fprintf('End of File');
    
end


