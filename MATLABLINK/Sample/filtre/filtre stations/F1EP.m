% Filter 1 : Filtering the pings with under-detection.
% Function to use when filter 1 is chosen.
% -----------------------------------------------------------------------
% Inputs:
%   -> data: 3 dimensional matrix (pings x depths x frequencies)
%   -> threshold: filtering threshold on the standardised residuals
%   calculated on pings variances values.
% -----------------------------------------------------------------------
% Outputs:
%   -> Echog: Filtered echogram (saved in jpeg format) and filtered median
%   profils
%   -> FilteredData: pings with detections over the threshold 
%   -> ResidualData: pings with detections under the threshold
% -----------------------------------------------------------------------
% B.REMOND 11/10/2012                           Last review : 08/02/2013
% -----------------------------------------------------------------------
% -----------------------------------------------------------------------
 
function [Echog,FilteredData,ResidualData] = F1EP (data,threshold)

global fq THRESH run NbFreq depth

for i = 1:NbFreq
    
    datafreq = data (:,:,i);

    % Variance calculation for each ping
    Meandata = 10*log10(mean (mean (10.^(datafreq./10))));   
    Vardata = (1/size(datafreq,1))* sum ((datafreq' - Meandata).^2);

    t = 1:size (Vardata,2);

    % -100 dB lines removing        
    Selection = var(datafreq) ~= 0;
    zmax = length (datafreq(Selection));


    % In-Control interval selection on the variances signal
    % Raw Echogram
    fifi=figure(1);
    set(fifi, 'Units','Normalized','OuterPosition',[0 0 1 1]);
    
    subplot (2,1,1)
        DrawEchoGram_brut (datafreq(:,1:zmax)',-45,-90)
        colorbar
        colorbar ('location','southoutside')
        title (['RUN',num2str(run),' ',num2str(fq(i)),'kHz',' Raw Echogram '])   

    % Variances signal    
    subplot (2,1,2)
        plot (t,Vardata)
        title ('Select a "control" interval without Empty Ping (2 clicks)')
        ylabel ('Variance per ping')
        xlabel ('time')
        limIC = ginput(2);


    % Selected "In-Control" interval
    intervalIC = Vardata (limIC (1,1):limIC (2,1));    
    meanIC = mean (intervalIC);

    % Comparison between in-control interval mean and pings variances
    Diff = Vardata - meanIC;
    % Differencies standardization
    Diffstd = (Diff - nanmean (Diff)) ./ nanstd (Diff);      
    
    % Filtering
    FilterEP = Diffstd < threshold;
    FilterEPvar = Vardata (FilterEP);
    FilterEPt = 1:size (FilterEPvar,1);
    
    % applied the filter on all frequencies
    FilteredData = data (FilterEP,:,:);
    residuals = data (~FilterEP,:,:);
end
close

% Filtered median profils calculation 
FilteredProfils = [depth',squeeze(median(FilteredData,1))];

% Filtering quality criterion (% number of ping deleted)
FilterQuality = 100* size(residuals,1)/size(data,1);


% Filtered echograms, filtered median profils and quality criterion
fifi=figure (2);
set(fifi, 'Units','Normalized','OuterPosition',[0 0 1 1]);
nfig = [1 2 3 5 6 7];
for i = 1:NbFreq
    subplot (2,4,nfig(i))
        DrawEchoGram_brut (FilteredData(:,:,i)',-45,-90)
        colorbar
        colorbar ('location','southoutside')
        title (['Filtered echogram RUN',num2str(run),' ',num2str(fq(i)),'kHz'])
end
subplot (2,4,[4 8])
plot (FilteredProfils(:,2:7), -FilteredProfils(:,1), 'LineWidth',2)
title (sprintf(['Filtered median profils \n Quality:',num2str(FilterQuality),' percent of pings removed']))
legend ('18','38','70','120','200','333')
xlim([-100 -40]) 

% result
Echog = figure (2)
FilteredData = FilteredData;
ResidualData = residuals;
end

    

    
    



