function Smb2hac(fid2,datatype,index)
%**************************************************************************
% Ecriture des fichiers hac
%**************************************************************************
warning('off')
global WC;
global Position;
global NbFaisceaux;
global Beamwidth;
global LS;
global Name;
%   NbFaisceaux=2;

switch(datatype)

    %**************************************************************************
    % 0
    %**************************************************************************
    case 65535
        % tuple 65535 HAC signature
        StartCode = 172;
        TupleSize = 14;
        TupleType = 65535;
        HACIdentifier = 44204;
        HACVersion = 160;
        AcqSoftVersion = 100;
        AcqSoftIdentifier = 4278234284;
        TupleAttribute = 0;
        BackLink = 24;

        fprintf('Ecriture HAC 65535 --> Signature de début de fichier HAC \n');
        fwrite(fid2, StartCode,'uint32');
        fwrite(fid2, TupleSize,'uint32');
        fwrite(fid2, TupleType,'uint16');
        fwrite(fid2, HACIdentifier,'uint16');
        fwrite(fid2, HACVersion,'uint16');
        fwrite(fid2, AcqSoftVersion,'uint16');
        fwrite(fid2, AcqSoftIdentifier,'uint32');
        fwrite(fid2, TupleAttribute,'int32');
        fwrite(fid2, BackLink,'uint32');

        %**************************************************************************
        % 20 GPS en GLL ou RMC
        %**************************************************************************
    case 20
        % tuple 20 standard position
        TupleSize = 26;
        TupleType = 20;
        TimeFraction = Position.time_fraction(index);
        TimeCPU = Position.time_cpu(index);                                           %heure locale en seconde depuis le 1er janvier 1970
        GPSTime = Position.time_cpu(index);                             % heure GPS en seconde depuis le 1er janvier 1970
        PositSystem = 1;
        Space = 0;
        Latitude = Position.latitude(index)*1000000;                                      %-90 à +90
        Longitude = Position.longitude(index)*1000000;                                    % de -180 à +180
        TupleAttribute = 0;
        BackLink = 36;

        fprintf('Ecriture HAC 20    --> GPS \n');
        fwrite(fid2, TupleSize,'uint32');
        fwrite(fid2, TupleType,'uint16');
        fwrite(fid2, TimeFraction,'uint16');
        fwrite(fid2, TimeCPU,'uint32');
        fwrite(fid2, GPSTime,'uint32');
        fwrite(fid2, PositSystem,'uint16');
        fwrite(fid2, Space,'uint16');
        fwrite(fid2, Latitude,'int32');
        fwrite(fid2, Longitude,'int32');
        fwrite(fid2, TupleAttribute,'int32');
        fwrite(fid2, BackLink,'uint32');

        %**************************************************************************
        % 30 HEADING
        %**************************************************************************
    case 30
        % tuple 30 Navigation Tuple (doc séparée....)
        TupleSize = 18;
        TupleType = 30;
        TimeFraction = Position.time_fraction(index);
        TimeCPU = Position.time_cpu(index);                                          % heure locale en seconde depuis le 1er janvier 1970
        NavSystem = 0;
        Heading = Position.heading(index)*10;
        NavigationSpeed	= Position.speed(index)*1000;
        Space = 0;
        TupleAttribute = 0;
        BackLink = 28;

        fprintf('Ecriture HAC 30    --> HEADING \n');
        fwrite(fid2, TupleSize,'uint32');
        fwrite(fid2, TupleType,'uint16');
        fwrite(fid2, TimeFraction,'uint16');
        fwrite(fid2, TimeCPU,'uint32');
        fwrite(fid2, NavSystem,'uint16');
        fwrite(fid2, Heading,'int16');
        fwrite(fid2, NavigationSpeed,'int16');
        fwrite(fid2, Space,'uint16');
        fwrite(fid2, TupleAttribute,'int32');
        fwrite(fid2, BackLink,'uint32');

        %**************************************************************************
        % 220 SONDEUR
        %**************************************************************************
    case 220
        % Tuple 220 MBES Echo sounder
        TupleSize = 174;
        TupleType = 220;
        NumberSWChannels = NbFaisceaux;
        EchosounderDocIdentifier = 2; %999; %2; %999;
        TransducerName = Name(1:5);                                                   % InfoPing.TransducerName;
        TransceiverSoftVersion = blanks(30);
        SoundSpeed = WC.soundSpeed(index)*10;
        TriggerMode = 65535;                                                       % not available
        PingInterval = 0.0;                                                        % not available
        PulseForm = 1;                                                             % CW
        PulseDuration = 200;                                    % en us
        TimeSampleInterval = (1/WC.sampling(index))*1000000;                          % time between each sample en us
        FrequencyBeamSpacing = 65535;                                              % not available
        FrequencySpaceShape = 65535;                                               % not available
        TransceiverPowerPercentage = 1;                                            % de 0 à 1
        TransducerInstallationDepth = 1*10000;
        PlatformIdentifier = 65535;
        TransducerShape = 1;                                                       % 1 = circular transducer, 2 = rectangular
        TransducerFaceAlongshipAngleOffset = 0*10000;                              % -180 à +180°
        TransducerFaceAthwartshipAngleOffset = 0*10000; %-InfoPing.PanelInclinaison*10000;   % -180 à +180°
        TransducerRotationAngle = 0*10000;                                         % -180 à +180°
        Remarks = blanks(40);                                                      % 40 characters max
        TupleAttribute = 0;
        Backlink = 184;

        fprintf('Ecriture HAC 220   --> Définiton du sondeur \n');
        fwrite(fid2, TupleSize,'uint32');
        fwrite(fid2, TupleType,'uint16');
        fwrite(fid2, NumberSWChannels,'uint16');
        fwrite(fid2, EchosounderDocIdentifier,'uint32');

        fwrite(fid2, TransducerName,'char');
        fwrite(fid2, 0,'char');
        fwrite(fid2, blanks(44),'char');

        fwrite(fid2, TransceiverSoftVersion,'char');
        fwrite(fid2, SoundSpeed,'uint16');
        fwrite(fid2, TriggerMode,'uint16');
        fwrite(fid2, PingInterval,'uint16');
        fwrite(fid2, PulseForm,'uint16');
        fwrite(fid2, PulseDuration,'uint32');
        fwrite(fid2, TimeSampleInterval,'uint32');
        fwrite(fid2, FrequencyBeamSpacing,'uint16');
        fwrite(fid2, FrequencySpaceShape,'uint16');
        fwrite(fid2, TransceiverPowerPercentage,'uint32');
        fwrite(fid2, TransducerInstallationDepth,'uint32');
        fwrite(fid2, PlatformIdentifier,'uint16');
        fwrite(fid2, TransducerShape,'uint16');
        fwrite(fid2, TransducerFaceAlongshipAngleOffset,'int32');
        fwrite(fid2, TransducerFaceAthwartshipAngleOffset,'int32');
        fwrite(fid2, TransducerRotationAngle,'int32');
        fwrite(fid2, Remarks,'char');
        fwrite(fid2, TupleAttribute,'int32');
        fwrite(fid2, Backlink,'uint32');

        %**************************************************************************
        % 2200 / 10100 / 41 
        %**************************************************************************
    case 2200
  
        
        for i = 1:NbFaisceaux
            TupleSize = 178;
            TupleType = 2200;
            SoftwareChannelIdentifier = i;                          % numéro du faisceau  - à itérer de 1 à 128
            EchosounderDocIdentifier = 2; %999; %2; %999;                         % Identifiant sondeur
            FrequencyChannelName = blanks(48);                      % 48 characters
            DataType = 1;                                           % 1 = Electrical power, 2 = Sv
            BeamType = 0;
            AcousticFrequency = WC.frequency(index,i);
            StartSample = 0;                                        % 0 = no offset
            % définition du dépointage des faisceaux
            AlongshipSteeringAngle  = WC.angles_along(index,i)*10000;                                                               % (à la vertical du bateau) -180 à +180°
            AthwartshipSteeringAngle = WC.angles_athwart(index,i)*10000 ;          %
            AbsorptionCoefficient = 0;                              % en 0.0001 dB/km Absorption of sound in the propagation medium
            Bandwidth = 0;             %InfoPing.SampleRate; % en Hz
            TransmissionPower = 000000 ;                            %Transmit power referred to the transducer terminals - W
            BeamAlongshipAngleSensitivity = 000000;                 % Electrical phase angle in degrees for one mechanical angle in the alongship (fore-aft) direction.
            BeamAthwartshipAngleSensitivity = 000000;
            BeamAlongship3dBBeamWidth = Beamwidth.along*10000;           % Half power (3dB) beam width of the transducer in the alongship direction.
            BeamAthwartship3dBBeamWidth = (Beamwidth.athwart/cos(WC.angles_athwart(index,i)*pi/180))*10000;
            BeamEquivalentTwoWayBeamAngle = 0;     % Equivalent two way beam opening solid angle. MacLennan and Simmonds, Fisheries Acoustics1992, section 2.3.
            BeamGain = 000000;                                      %Transducer gain used in power budget calculations for calculation of TS.
            BeamSACorrection = 0;
            BottomDetectionMinimumDepth = 0.0000;
            BottomDetectionMaximumDepth = 0.0000;
            BottomDetectionMinimumLevel = 0;
            AlongshipTXRXWeightingIdentifier = 65535; %1;
            AthwartshipTXRXWeightingIdentifier = 65535; %2;
            SplitbeamAlongshipRXWeightingIdentifier = 65535; %7;
            SplitbeamAthwartshipRXWeightingIdentifier = 65535; %8;
            Remarks = blanks(40);                                   % 40 characters
            TupleAttribute = 0;
            Backlink = 188;

            fwrite(fid2, TupleSize,'uint32');
            fwrite(fid2, TupleType,'uint16');
            fwrite(fid2, SoftwareChannelIdentifier,'uint16');
            fwrite(fid2, EchosounderDocIdentifier,'uint32');
            fwrite(fid2, FrequencyChannelName,'char');
            fwrite(fid2, DataType,'uint16');
            fwrite(fid2, BeamType,'uint16');
            fwrite(fid2, AcousticFrequency,'uint32');
            fwrite(fid2, StartSample,'uint32');
            fwrite(fid2, AlongshipSteeringAngle,'int32');
            fwrite(fid2, AthwartshipSteeringAngle,'int32');
            fwrite(fid2, AbsorptionCoefficient,'uint32');
            fwrite(fid2, Bandwidth,'uint32');
            fwrite(fid2, TransmissionPower,'uint32');
            fwrite(fid2, BeamAlongshipAngleSensitivity,'uint32');
            fwrite(fid2, BeamAthwartshipAngleSensitivity,'uint32');
            fwrite(fid2, BeamAlongship3dBBeamWidth,'uint32');
            fwrite(fid2, BeamAthwartship3dBBeamWidth,'uint32');
            fwrite(fid2, BeamEquivalentTwoWayBeamAngle,'int32');
            fwrite(fid2, BeamGain,'uint32');
            fwrite(fid2, BeamSACorrection,'int32');
            fwrite(fid2, BottomDetectionMinimumDepth,'uint32');
            fwrite(fid2, BottomDetectionMaximumDepth,'uint32');
            fwrite(fid2, BottomDetectionMinimumLevel,'int32');
            fwrite(fid2, AlongshipTXRXWeightingIdentifier,'uint16');
            fwrite(fid2, AthwartshipTXRXWeightingIdentifier,'uint16');
            fwrite(fid2, SplitbeamAlongshipRXWeightingIdentifier,'uint16');
            fwrite(fid2, SplitbeamAthwartshipRXWeightingIdentifier,'uint16');
            fwrite(fid2, Remarks,'char');
            fwrite(fid2, TupleAttribute,'int32');
            fwrite(fid2, Backlink,'uint32');
        end
    case 41
        %------------------------------------------------------------------
        % Tuple 10100 General threshold
        fprintf('Ecriture HAC 10100 --> Paramètres du seuillage pour chaque faisceau (non définis pour SM20) \n');
        for i = 1:NbFaisceaux
            TupleSize = 34;
            TupleType = 10100;
            TimeFraction = WC.time_fraction(index);                                %0.0000;
            TimeCPU = WC.time_cpu(index);                                     %InfoPing.startdatatime+InfoPing.datatime; %heure locale en seconde depuis le 1er janvier 1970
            SoftwareChannelIdentifier = i;                                       % numéro du faisceau  - à itérer de 1 à 128
            TVGMaxRange = 6553.5*10;                                                % le max
            TVGMinRange = 6553.5*10;
            TVTEvaluationMode = 0;
            TVTEvaluationInterval = 65535;
            TVTEvaluationNoPings = 65535;
            TVTEvaluationStartingTVTPingNum = 4294967295;
            TVToffsetparameter = -2147.483648*1000000;                                   %Seuil darchivage = not available
            TVTAmplificationParameter = 0;
            TupleAttribute = 0;
            Backlink = 44;

            fwrite(fid2, TupleSize,'uint32');
            fwrite(fid2, TupleType,'uint16');
            fwrite(fid2, TimeFraction,'uint16');
            fwrite(fid2, TimeCPU,'uint32');
            fwrite(fid2, SoftwareChannelIdentifier,'uint16');
            fwrite(fid2, TVGMaxRange,'uint16');
            fwrite(fid2, TVGMinRange,'uint16');
            fwrite(fid2, TVTEvaluationMode,'uint16');
            fwrite(fid2, TVTEvaluationInterval,'uint16');
            fwrite(fid2, TVTEvaluationNoPings,'uint16');
            fwrite(fid2, TVTEvaluationStartingTVTPingNum,'uint32');
            fwrite(fid2, TVToffsetparameter,'int32');
            fwrite(fid2, TVTAmplificationParameter,'uint32');
            fwrite(fid2, TupleAttribute,'int32');
            fwrite(fid2, Backlink,'uint32');
        end

        %------------------------------------------------------------------
        % Tuple 41 attitude sondeur pour chaque faisceau
        fprintf('Ecriture HAC 41    --> Coordonnées de la plateforme attitudes sondeurs pour chaque faisceau (non définis pour SM20) \n');
        TupleSize = 54;
        TupleType = 41;
        TimeFraction = WC.time_fraction(index);                                    %0.0000;
        TimeCPU = WC.time_cpu(index);                                         %InfoPing.startdatatime + InfoPing.datatime;
        AttitudeSensorId = 1;
        PlatformType = 0;
        AlongOffset = 0;
        AthwartOffset = 0;
        ElevationOffset = 0;
        Remarks = blanks(30);
        Space = 0;
        TupleAttribute = 0;
        Backlink = 64;
        for i = 1:NbFaisceaux
            TransceiverChannelNumber = i;
            fwrite(fid2, TupleSize,'uint32');
            fwrite(fid2, TupleType,'uint16');
            fwrite(fid2, TimeFraction,'uint16');
            fwrite(fid2, TimeCPU,'uint32');
            fwrite(fid2, AttitudeSensorId,'uint16');
            fwrite(fid2, TransceiverChannelNumber,'uint16');
            fwrite(fid2, PlatformType,'uint16');
            fwrite(fid2, AlongOffset,'int16');
            fwrite(fid2, AthwartOffset,'int16');
            fwrite(fid2, ElevationOffset,'int16');
            fwrite(fid2, Remarks,'char');
            fwrite(fid2, Space,'uint16');
            fwrite(fid2, TupleAttribute,'int32');
            fwrite(fid2, Backlink,'uint32');
        end

        %**************************************************************************
        % 10140 / 10040
        %**************************************************************************
    case 10000

        %------------------------------------------------------------------
        % Tuple 10140 attitude sensor tuple pour chaque ping
        TupleSize = 22;
        TupleType = 10140;
        TimeFraction = WC.time_fraction(index);                                    %0.0000;
        TimeCPU = WC.time_cpu(index);                                         %InfoPing.startdatatime + InfoPing.datatime;
        AttitudeSensorId = 1;
        Pitch = 0;
        Roll = 0;
        Heave = WC.heave(index);
        Yaw = 0;
        Space = 0;
        TupleAttribute = 0;
        Backlink = 32;

        fwrite(fid2, TupleSize,'uint32');
        fwrite(fid2, TupleType,'uint16');
        fwrite(fid2, TimeFraction,'uint16');
        fwrite(fid2, TimeCPU,'uint32');
        fwrite(fid2, AttitudeSensorId,'uint16');
        fwrite(fid2, Pitch,'int16');
        fwrite(fid2, Roll,'int16');
        fwrite(fid2, Heave,'int16');
        fwrite(fid2, Yaw,'int16');
        fwrite(fid2, Space,'uint16');
        fwrite(fid2, TupleAttribute,'int32');
        fwrite(fid2, Backlink,'uint32');

        %------------------------------------------------------------------
        % Tuple 10040 Ping C-16 compressed 16 bytes format pour chaque
        % faisceau

        
        fprintf('Ecriture HAC 10040    --> Ping \n');
        TupleType = 10040;
        TimeFraction = WC.time_fraction(index);                   %0.0000;
        TimeCPU = WC.time_cpu(index);                        %InfoPing.startdatatime + InfoPing.datatime;
        TransceiverMode = 0;
        PingNumber = WC.ping(index);
       
         % nombre d'échantillons
        TupleAttribute = 0;
        
        for i = 1:NbFaisceaux   %InfoPing.faisceaux
    
            fwrite(fid2, zeros(14,1), 'int16');
            
            nbBlanc=0;     
            nbsample=0;
            for j = 1:WC.samples(index,i)
                SampleValue = WC.amplitudes(index,i,j); 
                if ( SampleValue <= -64)%|| SampleValue<(max(WC.amplitudes(index,:,j))-LS))
                    nbBlanc=nbBlanc+1;
                else
                    if ( nbBlanc > 0)
                        fwrite(fid2, nbBlanc - 1,'bit15');
                        fwrite(fid2, 1,'ubit1');
                        nbsample= nbsample+1;

                        nbBlanc = 0;
                    end

                    fwrite(fid2, (SampleValue-10*log(j*WC.soundSpeed(index)/(2*WC.sampling(index))))*100,'bit15');
%                      fwrite(fid2, (SampleValue)*100,'bit15');
                    fwrite(fid2, 0,'ubit1');
                    nbsample= nbsample+1;
                end
            end
            
            if ( nbBlanc > 0)
                fwrite(fid2, nbBlanc - 1,'bit15');
                fwrite(fid2, 1,'ubit1');

                nbsample= nbsample+1;
            end
            
            if((nbsample/2-round(nbsample/2))~=0)
                fwrite(fid2, 0,'bit15');
                fwrite(fid2, 1,'ubit1');
                nbsample= nbsample+1;
            end

           
            
            TupleSize = 26 + 2*nbsample;
            
            fseek(fid2, -28-2*nbsample, 'cof');
          
            SoftChannelId = i;
            fwrite(fid2, TupleSize,'uint32');
            fwrite(fid2, TupleType,'uint16');
            fwrite(fid2, TimeFraction,'uint16');
            fwrite(fid2, TimeCPU,'uint32');
            fwrite(fid2, SoftChannelId,'uint16');
            fwrite(fid2, TransceiverMode,'uint16');
            fwrite(fid2, PingNumber,'uint32');
            if WC.rangebottom(index,i)==0
                fwrite(fid2, 2147483647,'int32');
%                 fwrite(fid2, ((24/cos(WC.angles_athwart(index,i)*pi/180))*1000),'int32');
            else
                fwrite(fid2, (WC.rangebottom(index,i)*WC.soundSpeed(index)/(2*WC.sampling(index)))*1000,'int32');
%                 fwrite(fid2, ((24/cos(WC.angles_athwart(index,i)*pi/180))*1000),'int32');
                
            end
            
            fwrite(fid2, nbsample,'uint32');
            
            fseek(fid2, 2*nbsample, 'cof');
            
            Backlink = TupleSize+10;
            
           
            fwrite(fid2, TupleAttribute,'int32');
            fwrite(fid2, Backlink,'uint32');
        end
        
        %**************************************************************************
        % 65534
        %**************************************************************************
    case 65534
        % tuple 65534 End of file
        TupleSize = 12;
        TupleType = 65534;
        TimeFraction = WC.time_fraction(index);                    %0.0000;
        TimeCPU = WC.time_cpu(index);                         %InfoPing.startdatatime + InfoPing.datatime;
        ClosingMode = 1;
        TupleAttribure = 0;
        BackLink = 22;

        fprintf('Ecriture HAC 65534 --> Signature de fin de fichier HAC \n');
        fwrite(fid2, TupleSize,'uint32');
        fwrite(fid2, TupleType,'uint16');
        fwrite(fid2, TimeFraction,'uint16');
        fwrite(fid2, TimeCPU,'uint32');
        fwrite(fid2, ClosingMode,'uint16');
        fwrite(fid2, TupleAttribure,'int32');
        fwrite(fid2, BackLink,'uint32');

        % *************************************************************************
end
warning('on')
