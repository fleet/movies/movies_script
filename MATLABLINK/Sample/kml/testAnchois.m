function testAnchois
nomDirData = 'C:\data\EM302\MARMESONET\20091120_HAC\';
% nomFic = 'seq_1000_1390_0306_20091113_160950_Lesuroit_1_school.mat';
% nomDirData = 'C:\nasa world wind\MARMESONET\';
nomDirOut = [nomDirData,'\kml'];
if ~exist(nomDirOut,'dir')
            mkdir(nomDirOut)
end

filelist=ls([nomDirData,'\*.mat']);
nb_files = size(filelist,1);


dB= createDbMap( -120, -90);
rgB= createColorMap();
opacity=createOpacityMap();

for numfile = 1:nb_files

    [pathstr, name, ext, versn] = fileparts(filelist(numfile,:));

    var = load(fullfile(nomDirData, filelist(numfile,:)));
    for i=1:length(var.schools_ME70)
        nomFicKML = fullfile(nomDirOut,[name num2str(i) '_polygon.kml']);
        pppp = var.schools_ME70(i);
         if( length(pppp.contours)>0 && pppp.MVBS_weighted<-105)
            M = zeros(size(pppp.contours,2),3);
            nbfacettes=pppp.contours(size(pppp.contours,2)).m_facet+1;
            K=zeros(nbfacettes,nbfacettes);
            indextri=0;
            indexfac=0;
            for j=1:size(pppp.contours,2)
                M(j,1) = pppp.contours(j).m_latitude;
                M(j,2) = pppp.contours(j).m_longitude;
                M(j,3) = -pppp.contours(j).m_depth;
                if (pppp.contours(j).m_facet+1)>indexfac
                    indextri=1;
                    indexfac=pppp.contours(j).m_facet+1;
                end
                K(indexfac,indextri)=j;
                indextri=indextri+1;
            end

            indexdB=find(dB(:,2)>=pppp.MVBS_weighted);
            writeKMLBanc(num2str(i),nomFicKML, M,K,pppp.time,[num2str(dec2hex(floor(255*opacity(indexdB(1))),2),'%2c'),num2str(dec2hex(255*rgB(indexdB(1),1),2),'%2c'),num2str(dec2hex(255*rgB(indexdB(1),2),2),'%2c'),num2str(dec2hex(255*rgB(indexdB(1),3),2),'%2c')]);

        end

    end
end
end

function db = createDbMap(minValue, maxValue)
% palette movies
inc=(minValue-maxValue)/16;
   db=[
       [1 -200];
     [2 -2500];	 
     [3 -2750];
     [4 -3000];
     [5 -3250];
     [6 -3500];
     [7 -3750];
     [8 -4000];
     [9 -4250];
     [10 -4500];
     [11 -4750];
     [12 -5000];
     [13 -5250];
     [14 -5500];
     [15 -5750];
     [16 -6000];
     [17 -6250];
     [18 -18000];
     ];
   db= [
     [1 minValue-inc];
     [2 minValue-2*inc];	 
     [3 minValue-3*inc];
     [4 minValue-4*inc];
     [5 minValue-5*inc];
     [6 minValue-6*inc];
     [7 minValue-7*inc];
     [8 minValue-8*inc];
     [9 minValue-9*inc];
     [10 minValue-10*inc];
     [11 minValue-11*inc];
     [12 minValue-12*inc];
     [13 minValue-13*inc];
     [14 minValue-14*inc];
     [15 minValue-15*inc];
     [16 minValue-16*inc];
     [17 maxValue-1];
     [18 -18000];
     ];
 
end
function rgb = createColorMap()
% palette movies
   rgb= [
         [0 0 0 ];     % 0x00000000, // noir
		 [128 0 0];	  % 0x00800000, // rouge fonce
		 [172 0 0];	  % 0x00ac0000,
		 [208 0 0];	  % 0x00d00000, // rouge
         [240 0 0];    % 0x00f00000, // orange
		[255 0 0];	  %  0x00ff0000, // jaune fonce
		[255 121 121]; % 0x00ff7979, // jaune
		[255 128 64];  % 0x00ff8040, // jaune clair
		[255 128 0];	  %  0x00ff8000, // vert clair
		[255 255 0];	  % 0x00ffff00, // vert
		[255 255 128]; % 0x00ffff80, // vert
		[0 255 0];	  % 0x0000ff00, // vert
		[125 255 128]; % 0x0080ff80,
		[0 128 192];	  % 0x000080c0, // bleu
		[0 128 255];	  % 0x000080ff, // bleu clair
		[128 255 255]; % 0x0080ffff,
		[192 192 192]; % 0x00C0C0C0, // gris clair
		[255 255 255]; %  0x00FFFFFF  /
        ];
   rgb = rgb / 255.;
end

function opacity = createOpacityMap()
% palette movies
   opacity= [
         1;0.85;0.85;0.8;0.75;0.7;0.6;0.6;0.5;0.4;0.4;0.3;0.2;0.2;0.15;0.1;0.1;0
        ];
        opacity=sort(opacity);
end


