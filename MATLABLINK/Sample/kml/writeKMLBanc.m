% Fonction writeKMLBanc
% EXAMPLE:
% writeKMLBanc('example_Polygon.kml', MPoints, KFacettes, ...)
% ----------------------------------------------------------------------------

function writeKMLBanc(index,nomFicKml, M, K, Time,color)

%% Temps
format = 'yyyy-mm-ddTHH:MM:SSZ';




fid = fopen(nomFicKml, 'w+', 'native', 'UTF-8');
if fid == -1
    str = sprintf(Lang('Impossible to cr�er Fichier KML %s', ...
        'Impossible to create KML config file %s'), nomFicXml);
    my_warndlg(str, 1)
    return
end

fprintf(fid, '<?xml version="1.0" encoding="utf-8"?>\n');
fprintf(fid, '<!--Fichier cree depuis la fonction writeKMLPolygon -->\n');
fprintf(fid, '<!--Auteur : GLU 29/01/2010-->\n');
fprintf(fid, '<kml xmlns="http://www.opengis.net/kml/2.2">\n');
fprintf(fid, '<Document>\n');
% fprintf(fid, '\t<Visibility>1</Visibility>\n');
% fprintf(fid, '\t<DocumentSource>ADU Blablabla</DocumentSource>\n');

    
dateStr =  datestr(Time, format);
fprintf(fid, '\t<Placemark>\n');
% Ajout Time Stamp
fprintf(fid, '\t\t<TimeStamp>\n');
fprintf(fid, '\t\t\t<when>%s</when>\n', dateStr);
fprintf(fid, '\t\t</TimeStamp>\n');

fprintf(fid, '\t\t<name>%s</name>\n',index);
fprintf(fid, '\t\t<Style id="examplePolyStyle">\n');
fprintf(fid, '\t\t\t<PolyStyle>\n');
fprintf(fid, '\t\t\t\t<color>%s</color>	 <!-- abgr -->\n',color);
fprintf(fid, '\t\t\t\t<fill>1</fill>\n');
fprintf(fid, '\t\t\t\t<outline>0</outline>\n');
% fprintf(fid, '\t\t\t\t<scale>10</scale>\n');
fprintf(fid, '\t\t\t</PolyStyle>\n');
fprintf(fid, '\t\t</Style>\n');
fprintf(fid, '\t\t<MultiGeometry>\n');
for j=1:size(K,1)
    index=find(K(j,:)==0);
    if index
        if (index(1)>3)
            for k=1:index(1)-3
                fprintf(fid, '\t\t\t<Polygon>\n');  
                fprintf(fid, '\t\t\t\t<altitudeMode>absolute</altitudeMode>\n');
                fprintf(fid, '\t\t\t\t<outerBoundaryIs>\n');
                fprintf(fid, '\t\t\t\t\t<LinearRing>\n');
                fprintf(fid, '\t\t\t\t\t\t<coordinates>\n');
                if rem(k, 2) == 0
                    fprintf(fid, '%15.12f,%15.12f,%15.12f\n', M(K(j,k+1),2),M(K(j,k+1),1),M(K(j,k+1),3));
                    fprintf(fid, '%15.12f,%15.12f,%15.12f\n', M(K(j,k),2),M(K(j,k),1),M(K(j,k),3));
                    fprintf(fid, '%15.12f,%15.12f,%15.12f\n', M(K(j,k+2),2),M(K(j,k+2),1),M(K(j,k+2),3));
%                     fprintf(fid, '%15.12f,%15.12f,%15.12f\n', M(K(j,k+1),2),M(K(j,k+1),1),M(K(j,k+1),3));
                else
                    fprintf(fid, '%15.12f,%15.12f,%15.12f\n', M(K(j,k),2),M(K(j,k),1),M(K(j,k),3));
                    fprintf(fid, '%15.12f,%15.12f,%15.12f\n', M(K(j,k+1),2),M(K(j,k+1),1),M(K(j,k+1),3));
                    fprintf(fid, '%15.12f,%15.12f,%15.12f\n', M(K(j,k+2),2),M(K(j,k+2),1),M(K(j,k+2),3));
%                     fprintf(fid, '%15.12f,%15.12f,%15.12f\n', M(K(j,k),2),M(K(j,k),1),M(K(j,k),3));
                end
                
                fprintf(fid, '\t\t\t\t\t\t</coordinates>\n');
                fprintf(fid, '\t\t\t\t\t</LinearRing>\n');
                fprintf(fid, '\t\t\t\t</outerBoundaryIs>\n');
                fprintf(fid, '\t\t\t</Polygon>\n');
            end
        end
    end


end
fprintf(fid, '\t\t</MultiGeometry>\n');
fprintf(fid, '\t</Placemark>\n');
fprintf(fid, '</Document>\n');
fprintf(fid, '</kml>\n');

fclose(fid);