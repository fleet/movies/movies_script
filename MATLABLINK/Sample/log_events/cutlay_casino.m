%Function to select ESUs corresponding to specific Casino sequences
%-----------------------------------------------------------
%   mathieu.doray@ifremer.fr, 2009/09/07, revised 2010/12/10
%
%Inputs:
%-------
%   C:          a casino output file
%   timeER60:   ESU time stamps (e.g. from EI results)
%   bps:        casino codes of sequences breakpoints to be selected
%   istarts     casino codes of sequences starts to be selected
%
%       e.g. to select only on-transect and inter-transect sequences (i.e. to exclude trawl hauls), use:
%           bps=char('DEBURAD','REPRAD','INTERAD','STOPRAD','FINRAD')
%           istarts=char('DEBURAD','REPRAD','INTERAD')
%       OR to select only on-transect sequences (excluding trawl hauls and inter-transects), use:
%           bps=char('DEBURAD','REPRAD','STOPRAD','FINRAD')
%           istarts=char('DEBURAD','REPRAD')
%       OR to select only trawl hauls sequences, use:
%           bps=char('DFIL','CULAB')
%           istarts=char('DFIL')
%       OR to select everything:
%           bps=char('ALL')
%           istarts=c('ALL')
%Outputs:
%-------
%   sdates3: time of casino events in string format 
%   stimes3: time of casino events in numbers of days since origin (0000/01/01 00:00:00)
%   sevts3: selected casino events
%   rad_self: boolean, same size as "timeER60", value: "do the ESUs belong to
%   selected Casino sequences?"
%   also prints out the number and percentage of ESUs excluded (0) and kept
%   (1) and a time:time plot with selected sequences

%Method:
%-------
%ESUs are selected according to their time stamps in numbers of days since
%origin (0000/01/01 00:00:00), which are compared to casino events time
%stamps, using a common categorical label: "ct"

function [sdates3,stimes3,sevts3,rad_self] = cutlay_casino(C,timeER60,bps,istarts)


%% Events file cleaning

%select non-null events in a cell array of cells
Cs={C{1,1}(~strcmp(C{1,11},'')) C{1,2}(~strcmp(C{1,11},'')) C{1,11}(~strcmp(C{1,11},''))}

dates=char(Cs{1,1});
hours=char(Cs{1,2});
evts=(Cs{1,3});

% remove '"' from events codes 
for j=1:size(evts,1)
    if (size(strfind(evts(j,:), '"'),2)>=1)
        evts(j,:)=strtok(evts(j,:), '"');
    end
end 

% converts string times to numeric time (nb of days since 0000)

daten1=datenum(dates,'dd/mm/yy');
datev1=datevec(dates,'dd/mm/yy');
daten2=datenum(hours,'HH:MM:SS');
datev2=datevec(hours,'HH:MM:SS');
datev2(:,1:3)=datev1(:,1:3);
dates2=datestr(datev2);
stime=datenum(dates2,'dd-mmm-yyyy HH:MM:SS',0000);

%% EI times conversion

t1=timeER60;
t1n=datestr(t1);

%plot(stime,stime,'-s')
%datetick('x',13)
%hold on
%plot(t1,t1,'-sr','LineWidth',2)

%% Select data in common time range
t1min=min(t1);
t1max=max(t1);
stmin=min(stime);
stmax=max(stime);

tmin=max([t1min stmin]); 
tmax=min([t1max stmax]); 

%select casino events in common time range
sevts=evts(stime>=tmin&stime<=tmax,:);
sdates=dates2(stime>=tmin&stime<=tmax,:);
stimes=stime(stime>=(tmin)&stime<=(tmax),:);

%select EI files in common time range
rad_sel1=t1>=tmin&t1<=tmax;
t1s=t1(rad_sel1);

%figure
%plot(stimes,stimes,'-s')
%datetick('x',13)
%hold on
%plot(t1s,t1s,'--rs','LineWidth',2)
%hold on
%plot([tmin tmax],[tmin tmax],'s','MarkerEdgeColor','k','MarkerFaceColor','g','MarkerSize',20)

%% Breakpoints check in common time range 
%check that events list starts with first bp values and ends with last bp values (remove others) 

if (~strcmp(bps,'ALL'));
    n=size(bps,1);
    N=size(sevts,1);
    j=0;
    completeb_sel=true(N,1);
    firstb_OK=0;
    lastb_OK=0;

    while (firstb_OK==0)||(lastb_OK==0);
        if (firstb_OK==0);
            if (strncmp(sevts(j+1,:),bps(1,:),size(strtok(bps(1,:)),2)));
                firstb_OK=1;
            else;
                completeb_sel(j+1,1)=0;
            end;
        end;
        if (lastb_OK==0);
            if (strncmp(sevts(N-j,:),bps(n,:),size(strtok(bps(n,:)),2)));
                lastb_OK=1;
            else;
                completeb_sel(N-j,1)=0;
            end;
        end;    
        j=j+1;
    end;

    sdates2=sdates(completeb_sel,:);
    stimes2=stimes(completeb_sel,:);
    sevts2=sevts(completeb_sel);
else
    sdates2=sdates;
    stimes2=stimes;
    sevts2=sevts;
    bps=unique(sevts2);
end;

%selection of breakpoints-> labels of breakpoints sequences are stored in "radevt_sel"

for j=1:size(bps);
        radevt_seli=strncmp(sevts2,bps(j,:),size(strtok(bps(j,:)),2));
    if j==1;
        radevt_sel=radevt_seli;
    else
        radevt_sel=radevt_sel|radevt_seli;
    end;    
end;

%check no. of breakpoints
%tevts=tabulate(sevts2(radevt_sel));
%if (~tevts{1,3}==100/size(bps,1));
%    alert='Missing breakpoints'
%else;
%    alert='Correct number of breakpoints'
%end;

%only selected breakpoints

sdates3=sdates2(radevt_sel,:);
stimes3=stimes2(radevt_sel,:);
sevts3=sevts2(radevt_sel);

%% Select EI times in new common time range

tmin2=min(stimes3); 
tmax2=max(stimes3); 

%select EI files in common time range
rad_sel2=(t1>=tmin2&t1<=tmax2);
%t1s2=t1s(t1s>=tmin2&t1s<=tmax2);
t1s2=t1(rad_sel2);

%% creates categorical variable "ct" to allocate each ESU to one casino sequence
%ct has 1 value per casino sequence (= sequence
%between 2 casino breakpoints (stored in "bpts"), e.g. between "DEBRAD" and "FINRAD")

%figure
%plot(stimes3,stimes3,'s','MarkerEdgeColor','k','MarkerFaceColor','g','MarkerSize',20)
%datetick('x',13)
%hold on
%plot(t1s2,t1s2,'rs')
%text(stimes3,stimes3,sevts3,'HorizontalAlignment','left')
 
t1f=datestr(t1s2);
stimes3plus=vertcat(stimes3,stimes3(size(stimes3,1),:)+1);
N=size(stimes3plus,1);
gap=1;
newseq= 1:gap:(N+gap);

labels = num2str(newseq(1,1:(size(newseq,2)-2))');
bpts= ordinal(t1s2,labels,[],stimes3plus);
bpts2= ordinal(stimes3,labels,[],stimes3plus);

ct=reshape(unique(bpts),[],1);
ct2=reshape(bpts2,[],1);

%selection of starting breakpoints-> 
%labels of starting breakpoints sequences are stored in "radevt_sel"

if (~strcmp(istarts,'ALL'));
    N=size(istarts,1);
    for j=1:N;
        is_radi=strncmp(sevts3,istarts(j,:),size(strtok(istarts(j,:)),2));
        if j==1;
            is_rad=is_radi;
        else
            is_rad=is_rad|is_radi;
        end;    
    end;
else
    is_rad=repmat(1,size(sdates3,1),1)
end;

tcat=dataset({char(t1f),'tf'},{reshape(bpts,[],1),'ct'});
Ctab=dataset({char(sdates3),'dates'},{sevts3,'evt'},{ct2,'ct'},{is_rad,'is_rad'});
Ctab=dataset({sdates3,'dates'},{sevts3,'evt'},{ct2,'ct'},{is_rad,'is_rad'});

% results are stored in the Ctab dataset; Ctab is merged with tcat:

tcat2=join(tcat,Ctab,'ct');

%test if ESU belongs to selected sequences using "is_rad": boolean, 
%FALSE if ESU belongs to selected sequences 

is_rad=logical(tcat2.is_rad);
is_rad=reshape(is_rad,1,[]);

t1r=t1s2(is_rad);
size(t1f);
t1f(is_rad,:);
% tabulate(is_rad)

rad_self=ismember(t1,t1r);

figure
plot(t1,t1,'bs')
datetick('x',0)
hold on
plot(t1r,t1r,'rs')
plot(stimes3,stimes3,'s','MarkerEdgeColor','k','MarkerFaceColor','g','MarkerSize',20)
text(stimes3,stimes3,sevts3,'HorizontalAlignment','left')
legend('Original ESDUs','Selected ESDUs','Selected breakpoints',2);

end



