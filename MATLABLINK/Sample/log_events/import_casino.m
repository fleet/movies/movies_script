%% function to import first fields of .csv casino files located at "path_evt"
%----------------------------------------
% WARNING1! .csv file separator: ';' with point as decimal separator
% (casino default = ','....)
% WARNING2! Remove all extra "'" in strings before importing

function [C,Cs,evts,datetime,juliantime] = import_casino(path_evt,allstring)
%% creating a string which specifies how to read the values of the casino file
%casino secret composition...

str = [repmat('%s',1,16) repmat('%f',1,17) '%s' repmat('%f',1,27) repmat('%s',1,10) repmat('%f',1,153) '%*[^\n]'];

if (allstring)
    str=repmat('%s',1,224);
end
    
%% opening the file with the pre-defined string str
fid = fopen(path_evt);
%
[C] = textscan(fid,str,'delimiter',';','HeaderLines',1);
%[C] = textscan(fid,str,'HeaderLines',1);
fclose(fid);

%% Events file cleaning

%select non-null events in a cell array of cells
Cs={C{1,1}(~strcmp(C{1,11},'')) C{1,2}(~strcmp(C{1,11},'')) C{1,11}(~strcmp(C{1,11},''))}

dates=char(Cs{1,1});
hours=char(Cs{1,2});
evts=(Cs{1,3});

% remove '"' from events codes 
for j=1:size(evts,1)
    if (size(strfind(evts(j,:), '"'),2)>=1)
        evts(j,:)=strtok(evts(j,:), '"');
    end
end 

%% converts string times to numeric time (nb of days since 0000)

daten1=datenum(dates,'dd/mm/yy');
datev1=datevec(dates,'dd/mm/yy');
daten2=datenum(hours,'HH:MM:SS');
datev2=datevec(hours,'HH:MM:SS');
datev2(:,1:3)=datev1(:,1:3);
datetime=datestr(datev2);
juliantime=datenum(datetime,'dd-mmm-yyyy HH:MM:SS',0000);

end


