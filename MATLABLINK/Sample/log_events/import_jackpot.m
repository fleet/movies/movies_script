%% Fonction pour importer et formater les champs utiles du fichier jackpot
%----------------------------------------
function [run,radiale,transect,DateStart,DateEnd,TimeStart,TimeEnd] = import_jackpot(path_evt,rads)
%% creating a string which specifies how to read the values of the jackpot file
    str=repmat('%s',1,224);
    
%% opening the file with the pre-defined string str
fid = fopen(path_evt);
%
[C] = textscan(fid,str,'delimiter',';','HeaderLines',1);
%[C] = textscan(fid,str,'HeaderLines',1);
fclose(fid);

%% extrait les informations utiles de jackpot
runs0=C{1,2};
radiale0=C{1,10};
%radiale0=str2num(radiale0(:,2:3));
date0=C{1,1};
time0=C{1,3};
codeAction=C{1,7};
%transects0=C{1,13}(strcmp(C{1,7},'DEBURAD')|strcmp(C{1,7},'REPRAD'));

    run=[];
    DateStart=[];
    TimeStart=[];
    DateEnd=[];
    TimeEnd=[];
    radiale=[];
    transect=[];

for r=1:length(rads)
    radi=rads(1,r);
    if radi<10
        rad=strcat('R0',num2str(radi));
    else
        rad=strcat('R',num2str(radi));
    end    
    runs1=str2double(runs0(strcmp(radiale0,rad),1));
    date1=char(date0(strcmp(radiale0,rad),1));
    time1=char(time0(strcmp(radiale0,rad),1));
    transect1=codeAction(strcmp(radiale0,rad),1);
    ranki=find(strcmp(transect1,'DEBURAD')|strcmp(transect1,'REPRAD'));
    rankj=find(strcmp(transect1,'FINRAD')|strcmp(transect1,'STOPRAD'));
    nbtransect=size(ranki,1);
    for tr=1:size(ranki,1)
        run(size(run,1)+1,1)=runs1(ranki(tr,1),1);
        if r==1&tr==1
            DateStart=date1(ranki(tr,1),:);
            TimeStart=time1(ranki(tr,1),:);
            DateEnd=date1(rankj(tr,1),:);
            TimeEnd=time1(rankj(tr,1),:);
        else
            DateStart=vertcat(DateStart,date1(ranki(tr,1),:));
            DateEnd=vertcat(DateEnd,date1(rankj(tr,1),:));
            TimeStart=vertcat(TimeStart,time1(ranki(tr,1),:));
            TimeEnd=vertcat(TimeEnd,time1(rankj(tr,1),:));            
        end
        radiale(size(radiale,1)+1,1)=r;
        transect(size(transect,1)+1,1)=tr;
    end
end 
end


