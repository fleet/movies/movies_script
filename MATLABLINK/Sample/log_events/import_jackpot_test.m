%% import jackpot file
%----------------------------------
clear all;

%path of jackpot file
path_evt='T:\PELGAS2015\Casino\PELGAS2015_jackpot.csv';
%import
rads=[1:14];

[run,radiale,transect,DateStart,DateEnd,TimeStart,TimeEnd]=import_jackpot(path_evt,rads);

path_export='T:\PELGAS2015\Acoustique\Segmentation\Jackpot4Matlab.mat';

save(path_export,'run','radiale','transect','DateStart','DateEnd','TimeStart','TimeEnd')


