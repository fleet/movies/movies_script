%% import casino file
%----------------------------------

%path of Casino file
path_evt='E:\PELGAS09\Casino\pelgas09_acou.csv';
%import
C=import_casino(path_evt);

%% import result files from EIlay located at:
%----------------------------------
chemin_hacbrut='G:\PELGAS09\HAC-Mplus\HacCorPel09\RUN005\';

D1_depth_bottom_ER60=[];
D1_Sa_botER60 = [];
D1_Sa_surfER60 = [];
D1_timeER60= [];

filelist = ls([chemin_hacbrut,'*.mat']);  % ensemble des fichiers
nb_files = size(filelist,1);  % nombre de fichiers

nblER60=0;

for numfile = 1:nb_files  % boucle sur

    matfilename = filelist(numfile,:);   
    FileName = [chemin_hacbrut,matfilename(1:findstr('.',matfilename)-1),'.mat']
    load(FileName);
    D1_timeER60(nblER60+1:nblER60+size(Depth_botER60,1)) = time_ER60;
    D1_depth_bottom_ER60(nblER60+1:nblER60+size(Depth_botER60,1),:,:) = Depth_botER60;
    D1_Sa_botER60(nblER60+1:nblER60+size(Sa_botER60,1),:,:) = Sa_botER60;
    D1_Sa_surfER60(nblER60+1:nblER60+size(Sa_surfER60,1),:,:) = Sa_surfER60;
    nblER60=size(D1_depth_bottom_ER60,1);
    clear Depth_botER60 Sa_botER60;
end;

%% Casino sequences selection with 'cutlay_casino' function

NoSeq=char('COM','INTERAD')

[sdate_rad,stime1,evt1,is_rad1]=cutlay_casino(C,D1_timeER60,NoSeq);

%visual check
%--------------
%   time~time plot 
%   red squares: casino breakpoints
%   blue dots: selected ESUs
%   broken red line: other ESUs

t1=D1_timeER60/86400+719529;
t1min=min(t1);
t1max=max(t1);
t1r=t1(is_rad1);
stime2s=stime1(stime1>=(t1min)&stime1<=(t1max),:);

close all
figure
hold on
plot(stime2s,stime2s,'--rs','LineWidth',2)
datetick('x',13)
plot(t1r,t1r,'db')


