function TS = KRM_model
% KRM_MODEL     Calculate TS of fish using Helmholts Kirchoff model
%   KRM_MODEL calculates the Target Strength (TS) of a given fish shape for
%   user-specified values of fish length, orientation, and frequency.  The
%   user may also specify parameters of soundspeed and density used in the
%   model calculations.  The program outputs TS for the given
%   parameters, along with figures and a .txt file containing the results.
%
%   FISH SHAPE
%       The shape of the fish must be defined in a Matlab .mat file
%       consisting of the following variables, where units are in meters:
%           x_b --> Nx1 vector defining the length of the fish body.
%           x_sb --> Mx1 vector defining the length of the fish
%               swimbladder.
%           z_b --> Nx2 array where the 1st and 2nd columns define the
%               upper and lower coordinates of the fish body, respectively.
%           z_sb --> Mx2 array where the 1st and 2nd columns define the
%               upper and lower coordinates of the fish swimbladder,
%               respectively.
%           w_b = Nx1 vector defining the width of the fish body from the
%               dorsal aspect.
%           w_sb = Mx1 vector defining the width of the fish swimbladder
%               from the dorsal aspect.
%
%       Depending on what the user inputs for lengths to use for the model,
%       the variables describing the fish shape are then scaled to those
%       lengths.  Lastly, the shape vectors are then interpolated so that
%       there is a 0.1mm cylinder resolution.
%
%   PROCESSING PARAMETERS
%       The program will prompt the user for values defining the
%       soundspeeds (m/s) and densities (kg/m^3) of the fish body,
%       swimbladder, and surrounding water.  Each input may only contain a
%       single value.
%
%       A prompt will also be given for the user to select ranges of fish
%       lengths (m), orientations (degrees), and frequencies (Hz) for use
%       in the model:
%           Fish Lengths --> A numeric array giving the fish lengths, in
%               meters, for the program to process.  Any Matlab command can
%               be used here, so long as the result is a numeric array
%               (e.g. normal or uniform distribution).
%
%           Frequencies --> A numeric array giving the frequencies, in Hz,
%               for the program to process.
%
%           Incidence Angles --> A numeric array giving the incidence
%               angles, in degrees, for the program to process.  A dorsal
%               incident orientation equates to 90 degrees.  Typically, the
%               KRM model is only valid for orientations between 65 and 115
%               degrees.
%
%           Save filename --> A string defining the filename to use for
%               saving the results in .txt files.
%
%   OUTPUT
%       The program returns an MxNxP matrix with M lengths, N frequencies,
%       and P orientations.
%
%       The program also plots figures of the defined fish and swimbladder
%       contour, TS vs. fish length, TS vs. frequency, and TS vs. incidence
%       angle.  For each plot of TS, the TS will be averaged out over the
%       other two parameters.  For example, when plotting TS vs. fish
%       length, the TS for each length will be calculated by averaging the
%       TS for all combinations of that length at all frequencies and
%       incidence angles.
%
%       Additionally, comma-separated-value .txt files will be created for
%       TS vs. length, TS vs. frequency, and TS vs. incidence angle.  As in
%       the figures, the TS calculated for one parameters is the average TS
%       over every combination of the other two parameters.  The .txt
%       files will be saved in the working directory and appended with
%       'TSvs[L | F | Phi]'.
%
%   NOTES
%       Equations and methods used in this program were taken from
%       'Acoustic models of fish: The Atlantic cod (Gadus morhua)' by Clay
%       and Horne, 1994.  Please refer to this paper for any questions
%       regarding parameter definitions or orientation clarification.

% Created by Josiah Renfree
% February 24, 2011
% Modified by Isabelle Leblond 
% may 2011

% Make sure there are no inputs
if nargin ~= 0
  error('This function does not accept input variables')
end


%% Get fish body and swimbladder shape

% Initialize variables
x_b = []; x_sb = []; z_b = []; z_sb = []; w_b = []; w_sb = [];

% Prompt user for file that describes fish shape
[filename, pathname] = uigetfile( {'*.mat', 'MAT files (*.mat)'; ...
  '*.*',  'All Files (*.*)'}, 'Pick fish shape file');

% Check to see if user pressed cancel
if isempty(filename)
  error('User pressed cancel')
end

% Load data from that file
load(fullfile(pathname, filename))

% Check that the correct variables exist
if sum(cellfun(@isempty, {x_b x_sb z_b z_sb w_b w_sb}))
  error('MAT file did not contain the correct fish shape variables.')
end


%% Plot fish shape
figure

% Plot lateral view
subplot(2,1,1)
hold on

% Plot fish body
plot(x_b, z_b(:,1), 'k', x_b, z_b(:,2), 'k')            % Upper and lower
plot([x_b(1) x_b(1)], [z_b(1,1) z_b(1,2)], 'k')         % Left end
plot([x_b(end) x_b(end)], [z_b(end,1) z_b(end,2)], 'k') % Right end

% Plot fish bladder
plot(x_sb, z_sb(:,1), 'k', x_sb, z_sb(:,2), 'k')        % Upper and lower
plot([x_sb(1) x_sb(1)], [z_sb(1,1) z_sb(1,2)], 'k')     % Left end
plot([x_sb(end) x_sb(end)], [z_sb(end,1) z_sb(end,2)], 'k') % Right end

axis equal              % make axes equal
title('Lateral view')
ylabel('Height (m)')

% Plot dorsal view
subplot(2,1,2)
hold on

% Plot fish body
plot(x_b, w_b/2, 'k', x_b, -w_b/2, 'k')                 % Upper and lower
plot([x_b(1) x_b(1)], [-w_b(1)/2 w_b(1)/2], 'k')        % Left end
plot([x_b(end) x_b(end)], [-w_b(end)/2 w_b(end)/2], 'k')	% Right end

% Plot fish bladder
plot(x_sb, w_sb/2, 'k', x_sb, -w_sb/2, 'k')             % Upper and lower
plot([x_sb(1) x_sb(1)], [-w_sb(1)/2 w_sb(1)/2], 'k')    % Left end
plot([x_sb(end) x_sb(end)], [-w_sb(end)/2 w_sb(end)/2], 'k')    % Right end

axis equal
title('Dorsal view')
ylabel('Width (m)')
xlabel('Length (m)')


%% Get fish and water parameters

% Prompt user for fish parameters
prompt = {'Fish body density (kg/m^3):', ...
  'Fish body soundspeed (m/s):', ...
  'Swimbladder density (kg/m^3):', ...
  'Swimbladder soundspeed (m/s):', ...
  'Water density (kg/m^3):', ...
  'Water soundspeed (m/s):'};
name = 'Inputs for fish and water parameters';
numlines = 1;
defaultanswer = {'1070', '1570', '1.24', '345', '1030', '1490'};
% defaultanswer = {'1.24', '345', '1.24', '345', '1030', '1490'}; % !! pour bulles uniquement !

answer = inputdlg(prompt,name,numlines,defaultanswer);

% If user pressed cancel
if isempty(answer)
  error('User pressed cancel')
end

% Parse out inputs to variables
p_b = str2double(answer{1});    % fish body density (kg/m^3)
c_b = str2double(answer{2});    % fish body soundspeed (m/s)
p_c = str2double(answer{3});    % swimbladder density (kg/m^3)
c_c = str2double(answer{4});    % swimbladder soundspeed (m/s)
p_w = str2double(answer{5});    % water density (kg/m^3)
c_w = str2double(answer{6});    % water soundspeed (m/s)

% Calculate static values that do not change with frequency, length, or phi
R_bc = (p_c*c_c - p_b*c_b) / (p_c*c_c + p_b*c_b);
R_wb = (p_b*c_b - p_w*c_w) / (p_b*c_b + p_w*c_w);
TwbTbw = 1 - R_wb.^2;


%% Prompt user for processing parameters
prompt = {'Fish length(s) (m):', ...
  'Frequency(ies) (Hz):', ...
  'Incidence Angle(s):', ...
  'Saveas filename:'};
name = 'Inputs for processing parameters';
numlines = 1;
 defaultanswer = {'.15', '[200]*1e3', '[60:0.1:120]', datestr(now, 'ddmmmyyyy')};
% defaultanswer = {'[1:0.1:10].*1e-3', '[18:200]*1e3', '90', datestr(now, 'ddmmmyyyy')};
% defaultanswer = {'[5:0.1:10].*1e-2', '[18:200]*1e3', '90', datestr(now, 'ddmmmyyyy')};
% taille=sort(normrnd(20e-2,1e-2,1,1000));
% frequency='[72662 78839 85016 91193 97370 103546 109723 117338 112812 106635 100458 94281 88104 81928 75751]';
% steering=sort(normrnd(90,10,1,1000));
% defaultanswer = {strcat('[',num2str(taille(1:100:end)),']'), (frequency), strcat('[',num2str(steering(1:100:end)),']'), datestr(now, 'ddmmmyyyy')};
answer = inputdlg(prompt,name,numlines,defaultanswer);

lengths = eval(answer{1});          % Fish lengths (m)
freqs = eval(answer{2});            % Frequencies (Hz)
phi = eval(answer{3});              % Incidence angles (degrees)
filesave = answer{4};               % Save filename

% Check to see if orientations are between 65 and 115 degrees
if sum(phi < 65 | phi > 115)
  uiwait(warndlg({'KRM model is only reliable for orientations' ...
    'between 65 and 115 degrees.'}, 'Unreliable Orientation', 'modal'))
end

% Check variables to ensure correct dimensions
if isequal(size(lengths,2),1)       % Check lengths
  lengths = lengths';             % Transpose if needed
end

if isequal(size(freqs,2),1)         % Check frequencies
  freqs = freqs';
end

if isequal(size(phi,2),1)           % Check incidence angles
  phi = phi';
end

% If a vector is given for sound speed, then give error
if length(c_w) > 1
  error('Sound speed must be a singular value');
end


%% Begin processing data

h = waitbar(0, 'Processing data...');   % Create waitbar
count = 0;                              % Count variable for waitbar

% Initialize scattering length variables for fish body and swimbladder
softL = zeros(length(lengths), length(freqs), length(phi));
fluidL = zeros(length(lengths), length(freqs), length(phi));

% Cycle through lengths
for i = 1:length(lengths)
  
  % Scale the body and bladder shape by the current fish length
  x_b2 = x_b .* lengths(i)/(x_b(end)-x_b(1));
  x_sb2 = x_sb .* lengths(i)/(x_b(end)-x_b(1));
  z_b2 = z_b .* lengths(i)/(x_b(end)-x_b(1));
  z_sb2 = z_sb .* lengths(i)/(x_b(end)-x_b(1));
  w_b2 = w_b .* lengths(i)/(x_b(end)-x_b(1));
  w_sb2 = w_sb .* lengths(i)/(x_b(end)-x_b(1));
  
  % Interpolate data to 0.1mm cylinder resolution
  x_b3 = linspace(x_b2(1), x_b2(end), 1 + lengths(i)/.001); % modification : interpolation with 1/10 mm 
  x_sb3 = linspace(x_sb2(1), x_sb2(end), 1 + lengths(i)/.001);
  z_b3 = interp1(x_b2, z_b2, x_b3, [], 'extrap');
  z_sb3 = interp1(x_sb2, z_sb2, x_sb3, [], 'extrap');
  w_b3 = interp1(x_b2, w_b2, x_b3, [], 'extrap');
  w_sb3 = interp1(x_sb2, w_sb2, x_sb3, [], 'extrap');
  
  
%% Plot fish shape
figure

% Plot lateral view
subplot(2,1,1)
hold on

% Plot fish body
plot(x_b, z_b(:,1), 'k', x_b, z_b(:,2), 'k')            % Upper and lower
plot([x_b(1) x_b(1)], [z_b(1,1) z_b(1,2)], 'k')         % Left end
plot([x_b(end) x_b(end)], [z_b(end,1) z_b(end,2)], 'k') % Right end

% Plot fish bladder
plot(x_sb3, z_sb3(:,1), 'k', x_sb3, z_sb3(:,2), 'k')        % Upper and lower
plot([x_sb3(1) x_sb3(1)], [z_sb3(1,1) z_sb3(1,2)], 'k')     % Left end
plot([x_sb3(end) x_sb3(end)], [z_sb3(end,1) z_sb3(end,2)], 'k') % Right end

axis equal              % make axes equal
title('Lateral view')
ylabel('Height (m)')

% Plot dorsal view
subplot(2,1,2)
hold on

% Plot fish body
plot(x_b, w_b/2, 'k', x_b, -w_b/2, 'k')                 % Upper and lower
plot([x_b(1) x_b(1)], [-w_b(1)/2 w_b(1)/2], 'k')        % Left end
plot([x_b(end) x_b(end)], [-w_b(end)/2 w_b(end)/2], 'k')	% Right end

% Plot fish bladder
plot(x_sb3, w_sb3/2, 'k', x_sb3, -w_sb3/2, 'k')             % Upper and lower
plot([x_sb3(1) x_sb3(1)], [-w_sb3(1)/2 w_sb3(1)/2], 'k')    % Left end
plot([x_sb3(end) x_sb3(end)], [-w_sb3(end)/2 w_sb3(end)/2], 'k')    % Right end

axis equal
title('Dorsal view')
ylabel('Width (m)')
xlabel('Length (m)')
  
  % Calculate variables that only change with length
  a_s = (w_sb3(1:end-1) + w_sb3(2:end)) ./ 4;
  a_b = (w_b3(1:end-1) + w_b3(2:end)) ./ 4;
  
  % Cycle through frequencies
  for j = 1:length(freqs)
    
    % Calculate variables that change with frequency
    k = 2*pi*freqs(j)/c_w;
    k_b = 2*pi*freqs(j)/c_b;
    A_sb = k*a_s ./ (k*a_s + 0.083);
    Psi_p = k*a_s ./ (40+k*a_s) - 1.05;
    Psi_b = -pi.*k_b.*((z_b3(1:end-1,1)+z_b3(2:end,1))./2) ./ ...
      (2.*(k_b.*((z_b3(1:end-1,1)+z_b3(2:end,1))./2)+0.4));
    
    % Cycle through each incidence angle
    for m = 1:length(phi)
      
      % Calculate variables that change with angle
      v_temp = repmat(x_sb3'*cosd(phi(m)),1,2) + z_sb3*sind(phi(m));
      v_s = (v_temp(1:end-1,1) + v_temp(2:end,1)) / 2;
      
      v_temp = repmat(x_b3'*cosd(phi(m)),1,2) + z_b3*sind(phi(m));
      v_b = (v_temp(1:end-1,:) + v_temp(2:end,:)) ./ 2;
      
      delta_u_s = (x_sb3(2:end) - x_sb3(1:end-1)) .* sind(phi(m));
      delta_u_b = (x_b3(2:end) - x_b3(1:end-1)) .* sind(phi(m));
      
      % Form function for the fish bladder
      buff = 0;
      for n = 1:length(x_sb3)-1
        buff = buff + ...
          (A_sb(n).*((k.*a_s(n)+1).*sind(phi(m))).^0.5 .* ...
          exp(-1i.*(2.*k.*v_s(n) + Psi_p(n))).*delta_u_s(n));
      end
      
      % Calculate scattering length of the swimbladder
      softL(i,j,m) = -1i.*(R_bc.*(1-R_wb.^2)./(2*sqrt(pi))) .* buff;
      
      % Form function for the fish body
      buff = 0;
      for n = 1:length(x_b3)-1
        buff = buff + ((k.*a_b(n)).^0.5 .* delta_u_b(n) .* ...
          (exp(-1i.*2.*k.*v_b(n,1)) - TwbTbw .* ...
          exp(-1i.*2.*k.*v_b(n,1) + ...
          1i.*2.*k_b.*(v_b(n,1)-v_b(n,2)) + 1i.*Psi_b(n))));
      end
      
      % Calculate scattering length of the fish body
      fluidL(i,j,m) = -1i .* (R_wb./(2*sqrt(pi))) .* buff;
      
      % Update waitbar
      count = count+1;            % Increment count
      
      % Want waitbar to only update 100 times
      if mod(count, floor((length(lengths) * length(freqs) * ...
          length(phi))/100)) == 0
        waitbar(count/(length(lengths) * length(freqs) * ...
          length(phi)), h)
        ph1 = findobj(h, 'type', 'patch');
        set(ph1, 'FaceColor', [0 0.8 0.8], 'EdgeColor', [0 0.8 0.8]);
        
      end
    end
  end
end
close(h)

TS = 20*log10(abs(softL+fluidL));	% Calculate TS
TS = 20*log10(abs(softL));	% Calculate TS
sigma = 10.^(TS/10);                % Calculate scattering cross-section

% If user provided filename, then save data
if ~isempty(filesave)
  save(filesave, 'TS', 'sigma', 'fluidL', 'softL', 'lengths', ...
    'freqs', 'phi', 'c_b', 'c_c', 'c_w', 'p_b', 'p_c', 'p_w', ...
    'w_b', 'w_sb', 'x_b', 'x_sb', 'z_b', 'z_sb', 'filesave')
end


%% Format results
% The results will be displayed as graphs as well as made into tables using
% a csv-style .txt file.  For the three input parameters (incidence angle,
% frequency, and fish length), TS will be calculated for each parameter by
% averaging the TS over the other two parameters.
%
% For example, if plotting TS vs. frequency, for each frequency, TS will be
% calculated for every combination of incidence angle and fish length, then
% averaged out to obtain a single TS value.

%%%%%%%%%%%%%%%%%%%% Plot TS vs. length

% Initialize the mean and percentile variables
sigmavsL = nan(size(TS,1), 1);
P = nan(size(TS,1), 2);

% Cycle through lengths
for i = 1:size(TS,1)
  temp = squeeze(sigma(i,:,:));           % Get sigma for freq and phi
  sigmavsL(i) = mean(temp(:));            % Calculate mean
  P(i,:) = prctile(temp(:), [2.5 97.5]);  % Calculate 95%
end

TSvsL = 10*log10(sigmavsL);                 % Convert to TS
L = TSvsL - 10*log10(P(:,1));               % Calculate lower errorbar
U = 10*log10(P(:,2)) - TSvsL;               % Calculate lower errorbar


% Plot figure with errorbars
figure
errorbar(lengths, TSvsL, L, U, 'o','color',[0 0.8 0.8])
toto=xlabel('Fish length (m)');
set(toto,'FontName','Comic sans MS','FontSize',16)
toto=ylabel('TS (dB)');
set(toto,'FontName','Comic sans MS','FontSize',16)
toto=ylabel('TS (dB)');
set(toto,'FontName','Comic sans MS','FontSize',16)
set(gca,'FontSize',14)

% Create .txt file for TS vs. Length
fid_TSvsL = fopen(strcat(filesave, '_TSvsL.txt'), 'w+t');   % Create file
fprintf(fid_TSvsL, '%s,%s\n', 'Length(mm)', 'TS(dB)');      % Add header
fprintf(fid_TSvsL, '%f,%f\n', [lengths; TSvsL']);           % Add data
fclose(fid_TSvsL);                                          % Close file


%%%%%%%%%%%%%%%%%%%% Plot TS vs. frequency

% Initialize the mean and percentile variables
sigmavsF = nan(size(TS,2), 1);
P = nan(size(TS,2), 2);

% Cycle through frequencies
for i = 1:size(TS,2)
  temp = squeeze(sigma(:,i,:));           % Get sigma for lengths and phi
  sigmavsF(i) = mean(temp(:));            % Calculate mean
  P(i,:) = prctile(temp(:), [2.5 97.5]);  % Calculate 95%
end

TSvsF = 10*log10(sigmavsF);                 % Convert to TS
L = TSvsF - 10*log10(P(:,1));               % Calculate lower errorbar
U = 10*log10(P(:,2)) - TSvsF;               % Calculate lower errorbar

% Plot figure with errorbars
figure
errorbar(freqs/1e3, TSvsF, L, U, 'o','color',[0 0.8 0.8])
toto=xlabel('Frequency (kHz)');
set(toto,'FontName','Comic sans MS','FontSize',16)
toto=ylabel('TS (dB)');
set(toto,'FontName','Comic sans MS','FontSize',16)
toto=ylabel('TS (dB)');
set(toto,'FontName','Comic sans MS','FontSize',16)
set(gca,'FontSize',14)

% Create .txt file for TS vs. Frequency
fid_TSvsF = fopen(strcat(filesave, '_TSvsF.txt'), 'w+t');   % Create file
fprintf(fid_TSvsF, '%s,%s\n', 'Frequency(kHz)', 'TS(dB)');  % Add header
fprintf(fid_TSvsF, '%f,%f\n', [freqs; TSvsF']);             % Add data
fclose(fid_TSvsF);                                          % Close file


%%%%%%%%%%%%%%%%%%%% Plot TS vs. incidence angle

% Initialize the mean and percentile variables
sigmavsPhi = nan(size(TS,3), 1);
P = nan(size(TS,3), 2);

% Cycle through angles
for i = 1:size(TS,3)
  temp = squeeze(sigma(:,:,i));           % Get sigma for L and freq
  sigmavsPhi(i) = mean(temp(:));          % Calculate mean
  P(i,:) = prctile(temp(:), [2.5 97.5]);  % Calculate 95%
end

TSvsPhi = 10*log10(sigmavsPhi);             % Convert to TS
L = TSvsPhi - 10*log10(P(:,1));             % Calculate lower errorbar
U = 10*log10(P(:,2)) - TSvsPhi;             % Calculate lower errorbar

% Plot figure with errorbars
figure
errorbar(phi, TSvsPhi, L, U, 'o','color',[0 0.8 0.8])
toto=xlabel('Incidence Angle (\circ)');
set(toto,'FontName','Comic sans MS','FontSize',16)
toto=ylabel('TS (dB)');
set(toto,'FontName','Comic sans MS','FontSize',16)
toto=ylabel('TS (dB)');
set(toto,'FontName','Comic sans MS','FontSize',16)
set(gca,'FontSize',14)

% Create .txt file for TS vs. Incidence angle
fid_TSvsPhi = fopen(strcat(filesave, '_TSvsPhi.txt'), 'w+t');   % Create file
fprintf(fid_TSvsPhi, '%s,%s\n', 'Phi(degrees)', 'TS(dB)');      % Add header
fprintf(fid_TSvsPhi, '%f,%f\n', [180*phi/pi; TSvsPhi']);        % Add data
fclose(fid_TSvsPhi);                                            % Close file


%%%%%%%%%%%%%%%%%%%% Plot reduced TS vs. length/wavelength

% Initialize the mean and percentile variables
sigmarvsLr = nan(size(TS,1)*size(TS,2), 1);
P = nan(size(TS,1)*size(TS,2), 2);
Lr=nan(size(TS,1)*size(TS,2),1);

% Cycle through length
for i = 1:size(TS,1)
  %Cycle through frequency
  for j=1:size(TS,2)
    temp = squeeze(sigma(i,j,:))./(lengths(i)*lengths(i));           % Get sigma for L and freq
    sigmarvsLr((i-1)*size(TS,2)+j) = mean(temp(:));          % Calculate mean
    P((i-1)*size(TS,2)+j,:) = prctile(temp(:), [2.5 97.5]);  % Calculate 95%
    Lr((i-1)*size(TS,2)+j)=lengths(i)*freqs(j)/c_w;
  end
end

TSrvsLr = 10*log10(sigmarvsLr);             % Convert to TS
L = TSrvsLr - 10*log10(P(:,1));             % Calculate lower errorbar
U = 10*log10(P(:,2)) - TSrvsLr;             % Calculate lower errorbar

% Plot figure with errorbars
figure
errorbar(Lr', TSrvsLr, L, U, 'o','color',[0 0.8 0.8])
toto=xlabel('L/lambda');
set(toto,'FontName','Comic sans MS','FontSize',16)
toto=ylabel('Reduced TS (dB)');
set(toto,'FontName','Comic sans MS','FontSize',16)
toto=ylabel('TS (dB)');
set(toto,'FontName','Comic sans MS','FontSize',16)
set(gca,'FontSize',14)

% Create .txt file for reducd TS vs. length/wavelength
fid_TSrvsLr = fopen(strcat(filesave, '_TSrvsLr.txt'), 'w+t');   % Create file
fprintf(fid_TSrvsLr, '%s,%s\n', 'L/lambda', 'ReducedTS(dB)');      % Add header
fprintf(fid_TSrvsLr, '%f,%f\n', [Lr; TSrvsLr]);        % Add data
fclose(fid_TSrvsLr);
