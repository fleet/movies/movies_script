% V1.00
clear *; close all;
dbstop if error
addpath(genpath(pwd)) % add path for subfolder 'my_functions' and for input data (swimbladder *.stl)
cd(fileparts(mfilename('fullpath'))); % set the current Matlab folder = script directory

% f : fish
% sb : fish swimbladder

%% Inputs

% Soft swimbladder
g  = 0.00129; % relative mass density
c0 = 1483;    % sound velocity (m/s) outside the cylinder
c1 = 340;     % sound velocity (m/s) inside the cylinder
h = c1/c0;
L_b_sb = [11 12 13]%(6:1:14)*2; % ratio of the swimbladder length and the swimbladder radius
i_Flat2Plot = 1;
Lfish2Lsb = 0.387; % ratio of the swimbladder length and the fish length : empirical parameter estimated from Loic report
Nang = 1%000; % number of fish for normal distributions

%% Modelled TS : over potential lengths (8cm - 25cm), potential frequencies (18kHz - 200kHz), potential tilt angles (15� - 165�)
% fix flattening : a/b = 8
% Stanton1988 modal series
% nTS = function(L,f,ti) or nTS = function(ka,ab,ti)

% Potential values of L, f and theta :
% length(L) dependency
Lf_all = (15:1:24)*1e-2; % fish length (m)
Lsb_all = Lfish2Lsb * Lf_all; % swimbladder length (m)
Nflat = length(L_b_sb);
L0 = mean(Lsb_all); % average swimbladder length (m)
% frequency(f) dependency
f_all = (0.1:1:200)*1e3; % from 18kHz to 200kHz
% tilt angle(ti) dependency
% first hypothesis : the fishes are swimming in the crosstrack vessel direction
ti_all = [90; 85; 70; 45]'%40:1:140; % beam angle crosstrack the vessel : tilt angle (�)
Nti = length(ti_all);
% kL and kb potential values (assuming ab_sb constant) :
kL_all = (2*pi/c0) * Lsb_all.' * f_all; % wave number x swimbladder length

str1 = sprintf('kL min value : %2.1f \nkL max value : %2.1f \n',min(kL_all(:)),max(kL_all(:)));
disp(str1);

figure
imagesc(Lf_all*1e2, f_all*1e-3, kL_all); colorbar;
set(gca,'YDir','normal')
xlabel('Fish length (cm)'); ylabel('Frequency (kHz)'); title('kL values according to swimbladder length and frequency');

% Value of fixed parameters
kL_all = min(kL_all(:)):0.001:max(kL_all(:)); % wave number x swimbladder length
NkL = length(kL_all);
kb_all = kL_all ./ (L_b_sb(1)); % wave number x swimbladder diameter

% List of acoustic models for fluid cylinder of finite length
list_models = [1,5]%, 5, 4, 6]; % description inside the function cylinder_model_ka
str_model = {'Stanton1988 modal series', 'Stanton1988 HF', 'Ye1997 f_{end}', 'Do1990 HF'};
Nmodel = length(list_models);
% Concatenate results from all acoustic models in variable nsigma_allModels
nsigma_allModels = zeros(NkL, Nflat, Nti, Nmodel); % absolute value WHEN TO DO THIS COMPUTATION??
% reduced (ie. length-normalised) scattering function  :
% dimension 1 : dependency on frequency  : kL
% dimension 2 : dependency on flattening : ab
% dimension 3 : dependency on tilt angle : ti
% dimension 4 : type of acoustic model used
for i_model = 1:Nmodel
    nsigma_allModels(:,:,:,i_model) = cylinder_model_ka( kL_all, L_b_sb, ti_all, g, h, list_models(i_model) );
end


%% ka dependency
% for finner results : average over theta normal distribution
% ka_all, ab_sb, moyenne_distrib_n

% 4 angle normal distributions are used :
m_anglen = [90, 85, 70, 45];
angles = repmat(m_anglen, Nang, 1); % angle normal distributions mean (�)
std_anglen = 15;%15; % angle normal distributions standard deviation (�)
anglen = angles + std_anglen*randn(size(angles)); % angle normal distributions (�)
% reduced scattering function according to ka, averaged on each angle distribution : 
nsigma_kL_anglemean = zeros(NkL, Nflat, size(angles,2), Nmodel);

for i_kL = 1:NkL % loop on each ka value
    for i_model = 1:Nmodel % loop on each scattering model
        for i_flat = 1:Nflat
        nsigma_law_tmp = nsigma_allModels(i_kL, i_flat, :, i_model); % modelled scattering function according to tilt angle : 1 x Nflat x Nti x 1
        nsigma_law_tmp_r = reshape(nsigma_law_tmp, 1, Nti); % rescalling : Nflat x Nti
        % interpolation between angle distribution and modelled scattering function :
        nsigma_anglen = interp1(ti_all, nsigma_law_tmp_r, anglen, 'nearest', 'extrap');
        % average on each angle distribution :
        nsigma_kL_anglemean(i_kL,i_flat,:,i_model) = mean(nsigma_anglen,1);
        end
    end
end

% reduced target strength according to ka, averaged on each angle distribution :
nTS_ka_anglemean  = 10*log10(nsigma_kL_anglemean); 

scrsz = get(groot,'ScreenSize');
figure('Position',scrsz)
for i_model = 1:Nmodel
subplot(2,2,i_model)
semilogx(kL_all, nTS_ka_anglemean(:,i_Flat2Plot,1,i_model), 'k-' ); % Solid line
hold on; grid on; ylim([-80 10]);
semilogx(kL_all, nTS_ka_anglemean(:,i_Flat2Plot,2,i_model), 'k-.' ); % Dashed line
semilogx(kL_all, nTS_ka_anglemean(:,i_Flat2Plot,3,i_model), 'k--' ); % Dotted line
semilogx(kL_all, nTS_ka_anglemean(:,i_Flat2Plot,4,i_model), 'k:'); % Dash-dot line
xlabel('kL'); ylabel('length-normalised nTS (dB)'); title(strcat('nTS - ',str_model{i_model},' - flattening L/b=',num2str(L_b_sb(i_Flat2Plot))));
legend(strcat('angle normal distribution : mean=',num2str(m_anglen(1)),'� - std=',num2str(std_anglen),'�'), ...
    strcat('angle normal distribution : mean=',num2str(m_anglen(2)),'� - std=',num2str(std_anglen),'�'), ...
    strcat('angle normal distribution : mean=',num2str(m_anglen(3)),'� - std=',num2str(std_anglen),'�'), ...
    strcat('angle normal distribution : mean=',num2str(m_anglen(4)),'� - std=',num2str(std_anglen),'�'), ...
    'Location','southwest');
set(gca,'PlotBoxAspectRatio',[2 1 1])
% set(gca,'YAxisLocation','left')
set(gca,'box','off') % remove upper XTicks and right YTicks
end
% set(gcf,'PaperPositionMode','auto')
% print -deps -painters MyFigure
% print -deps2 -painters MyFigure2

%% theta dependency
% for finner results : average over ka normal distribution
% moyenne_distrib_n, ab_sb, ti_all

kLs = repmat([4; 12; 25; 50], 1, Nang);
f_kLs = kLs * c0 / (2*pi*L0);
std_L0 = 1e-2;
std_kLs = (2*pi/c0) * f_kLs * std_L0;
kLn = kLs + std_kLs.*randn(size(kLs)); % angle normal distribution

nsigma_angle_kamean = zeros(size(kLs,1), length(ti_all), size(nsigma_allModels,4)); % ka, mean incident angle

for i_ti = 1:length(ti_all) % loop on each ka value
    for i_model = 1:Nmodel % loop on each cylinder model
        nsigma_law_tmp = nsigma_allModels(:, 3, i_ti, i_model); % TS law according to ka
        nsigma_law_tmp = squeeze(nsigma_law_tmp);
        nsigma_kan = interp1(kL_all, nsigma_law_tmp, kLn, 'nearest', 'extrap');
        nsigma_angle_kamean(:,i_ti,i_model) = mean(nsigma_kan,2);
    end
end

nTS_angle_kLmean  = 10*log10(nsigma_angle_kamean); % nTS : length-normalised TS

figure('Position',scrsz)
for i_model = 1:Nmodel
subplot(2,2,i_model)
hold on; grid on; ylim([-80 10]);
plot(ti_all, nTS_angle_kLmean(1,:,i_model), 'k-');  % Solid line
plot(ti_all, nTS_angle_kLmean(2,:,i_model), 'k--'); % Dashed line
plot(ti_all, nTS_angle_kLmean(3,:,i_model), 'k:');  % Dotted line
plot(ti_all, nTS_angle_kLmean(4,:,i_model), 'k-.'); % Dash-dot line
xlabel('tilt angle (�)'); ylabel('length-normalised nTS (dB)'); title(strcat('nTS - ',str_model{i_model},' - flattening L/b=',num2str(L_b_sb(3))));
legend(strcat('ka normal distribution : mean=',num2str(kLs(1)),' - std=',num2str(std_kLs(1,1))), ...
    strcat('kL normal distribution : mean=',num2str(kLs(2)),' - std=',num2str(std_kLs(2,1))), ...
    strcat('kL normal distribution : mean=',num2str(kLs(3)),' - std=',num2str(std_kLs(3,1))), ...
    strcat('kL normal distribution : mean=',num2str(kLs(4)),' - std=',num2str(std_kLs(4,1))), ...
    'Location','southwest');
end

%% flattening dependency

kLs = repmat([4; 12; 25; 50], 1, Nang);
f_kLs = kLs * c0 / (2*pi*L0);
std_L0 = 1e-2;
std_kLs = (2*pi/c0) * f_kLs * std_L0;
kLn = kLs + std_kLs.*randn(size(kLs)); % angle normal distribution

nsigma_angle_kamean = zeros(size(kLs,1), length(ti_all), size(nsigma_allModels,4)); % ka, mean incident angle

for i_ti = 1:length(ti_all) % loop on each ka value
    for i_model = 1:Nmodel % loop on each cylinder model
        nsigma_law_tmp = nsigma_allModels(:, 3, i_ti, i_model); % TS law according to ka
        nsigma_law_tmp = squeeze(nsigma_law_tmp);
        nsigma_kan = interp1(kL_all, nsigma_law_tmp, kLn, 'nearest', 'extrap');
        nsigma_angle_kamean(:,i_ti,i_model) = mean(nsigma_kan,2);
    end
end

nTS_angle_kLmean  = 10*log10(nsigma_angle_kamean); % nTS : length-normalised TS

figure('Position',scrsz)
for i_model = 1:Nmodel
subplot(2,2,i_model)
hold on; grid on; ylim([-80 10]);
plot(ti_all, nTS_angle_kLmean(1,:,i_model), 'k-');  % Solid line
plot(ti_all, nTS_angle_kLmean(2,:,i_model), 'k--'); % Dashed line
plot(ti_all, nTS_angle_kLmean(3,:,i_model), 'k:');  % Dotted line
plot(ti_all, nTS_angle_kLmean(4,:,i_model), 'k-.'); % Dash-dot line
xlabel('tilt angle (�)'); ylabel('length-normalised nTS (dB)'); title(strcat('nTS - ',str_model{i_model},' - flattening L/b=',num2str(L_b_sb(9))));
legend(strcat('ka normal distribution : mean=',num2str(kLs(1)),' - std=',num2str(std_kLs(1,1))), ...
    strcat('kL normal distribution : mean=',num2str(kLs(2)),' - std=',num2str(std_kLs(2,1))), ...
    strcat('kL normal distribution : mean=',num2str(kLs(3)),' - std=',num2str(std_kLs(3,1))), ...
    strcat('kL normal distribution : mean=',num2str(kLs(4)),' - std=',num2str(std_kLs(4,1))), ...
    'Location','southwest');
end