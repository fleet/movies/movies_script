% V1.00
clear *; close all;
dbstop if error
addpath(genpath(pwd)) % add path for subfolder 'my_functions' and for input data (swimbladder *.stl)
cd(fileparts(mfilename('fullpath'))); % set the current Matlab folder = script directory


scrsz = get(groot,'ScreenSize');
% Change default axes fonts.
set(0,'DefaultAxesFontName', 'Arial')
set(0,'DefaultAxesFontSize', 14)
% Change default text fonts.
set(0,'DefaultTextFontname', 'Arial')
set(0,'DefaultTextFontSize', 16)
% Change default line width.
set(0,'defaultlinelinewidth', 1)


% f : fish
% sb : fish swimbladder

%% Inputs

% Fluid swimbladder
g_fluid = 0.00129; % relative mass density
c0 = 1500;    % sound velocity (m/s) outside the cylinder
c1 = 340;     % sound velocity (m/s) inside the cylinder
h_fluid = c1/c0;

% Fish parameters (adulte sardines)
% ratio of the swim bladder semi-length and radius :
% flats = [11 12 13]; % array of numbers
flats = [1 10 100]; % array of numbers
flats_LineStyle = {'-','--','-.'}; % cell of strings
flats_cellstr = cellfun(@num2str, num2cell(flats), 'UniformOutput', false);
flats_cellstr_complete = strcat('ab=',flats_cellstr);
flats_Legend = {'ab=','ab=','ab='};
cellfun(@num2str, num2cell(flats), 'UniformOutput', false)
Lfish2Lsb = 0.387; % ratio of the swimbladder length and the fish length
freqs = (18:200)*1e3; % frequency (Hz)
angle = 90; % normal incidence (�)
Lfish = (15:24)*1e-2; % fish length (m)
Lbladder = Lfish2Lsb*Lfish; % bladder length (m)
% ka value computation :
ka_min = 2*pi/c0*freqs(1)*Lbladder(1)/2;
ka_max = 2*pi/c0*freqs(end)*Lbladder(end)/2;
dka    = 1e-3;
% ka     = ka_min:dka:ka_max; % potential ka values
ka     = dka:dka:50;

Nka = length(ka);
Nflats = length(flats);
Nangle = length(angle);

%%
% Modelled TS : over potential lengths (15cm - 24cm), potential frequencies
% (18kHz - 200kHz), normal incidence (90�)
% Model used : Stanton1988 modal series
% nTS = function(2*ka,2*ab,angle)

tic
nsigma_St88_fluid = cylinder_model_ka( 2*ka, 2*flats, angle, g_fluid, h_fluid, 1); % Nka x Nflats x Nangle
toc
nTS_St88_fluid = 10*log10(nsigma_St88_fluid);
tic
nsigma_St88_soft = cylinder_model_ka( 2*ka, 2*flats, angle, 0, 0, 7); % Nka x Nflats x Nangle
toc
nTS_St88_soft = 10*log10(nsigma_St88_soft);

%% plot for different flattenings :
figure('Position',scrsz); % ylim([-80 10]);
for i_flat = 1:Nflats
    semilogx(ka, nTS_St88_fluid(:,i_flat,1), strcat('k',flats_LineStyle{i_flat}));
    hold on;
end
% ka edges for our adulte sardines :
line([ka_min ka_min],[-100 20],'LineStyle',':','Color','k'); text(ka_min,-50,'ka min');
line([ka_max ka_max],[-100 20],'LineStyle',':','Color','k'); text(ka_max,-50,'ka max');

grid on; xlabel('ka'); ylabel('length-normalised nTS (dB)'); title('Flattening dependancy');
legend(flats_cellstr_complete)

%% Comparison between fluid and soft cylinder :
figure('Position',scrsz);
semilogx(ka, nTS_St88_fluid(:,2,1), 'k-');
hold on; grid on;
semilogx(ka, nTS_St88_soft(:,2,1), 'k:');

% ka edges for our adulte sardines :
line([ka_min ka_min],[-100 20],'LineStyle',':','Color','k'); text(ka_min,-50,'ka min');
line([ka_max ka_max],[-100 20],'LineStyle',':','Color','k'); text(ka_max,-50,'ka max');

xlabel('ka'); ylabel('length-normalised nTS (dB)'); title('Fluid vs. Soft cylinder');
legend('Fluid', 'Soft')

%% Plot TS as a function of ka and flattening
ka_3Dplot     = ka_min:0.1:ka_max;
flats_3Dplot = 10:0.1:14;
angles_3Dplot = 40:0.5:140;
Nka3D = length(ka_3Dplot);
Nflats3D = length(flats_3Dplot);
Nangles3D = length(angles_3Dplot);

% nTS computation
nsigma_St88_soft_3Dplot = cylinder_model_ka( 2*ka_3Dplot, 2*flats_3Dplot, angles_3Dplot, 0, 0, 7); % Nka3D x Nflats3D x Nangles3D
nTS_St88_soft_3Dplot = 10*log10(nsigma_St88_soft_3Dplot);

figure('Position',scrsz);
subplot 121
imagesc(flats_3Dplot,ka_3Dplot,nTS_St88_soft_3Dplot(:,:,100))
xlabel('flattening'); ylabel('ka'); title('nTS 90� [dB] - Flattening and ka dependancy'); colorbar;

% mean TS over a tilt distribution
Nfish = 1000;
m_PDF = 90;
std_PDF = 10;
angles_PDF = m_PDF + std_PDF * randn(1, Nfish); % probality density distribution (PDF)
% Truncate at 95% the PDF :
angles_PDF = angles_PDF(angles_PDF >=  m_PDF - 3*std_PDF & angles_PDF <= m_PDF + 3*std_PDF);

% Mean over angle distribution for each fish
nTS_St88_soft_3Dplot_PDFaveraged = zeros(Nka3D,Nflats3D);
for i_ka = 1:Nka3D % ka selection
    for i_flat = 1:Nflats3D % flattening selection
        nTS_tmp = nTS_St88_soft_3Dplot(i_ka, i_flat, :);
        nTS_tmp = squeeze(nTS_tmp); % dim : Nangles3D
        nTS_anglen_dataset_tmp = interp1(angles_3Dplot, nTS_tmp, angles_PDF, 'nearest', 'extrap');
        
        nTS_tiltAveraged_dataset_tmp = 10*log10(mean(10.^(nTS_anglen_dataset_tmp/10)));
        nTS_St88_soft_3Dplot_PDFaveraged(i_ka,i_flat) = nTS_tiltAveraged_dataset_tmp;
    end
end

subplot 122
imagesc(flats_3Dplot,ka_3Dplot,nTS_St88_soft_3Dplot_PDFaveraged)
xlabel('flattening'); ylabel('ka'); title('nTS averaged [dB] - Flattening and ka dependancy'); colorbar;


