%%
% elseif d == 2 % simulated data
%     
% % Theta dependancy
% figure; hold on; grid on; ylim([-100 0]);
% for i_model = models_ON
% for i_fish = 1:Nfish % loop on fishes
% nTS_tmp = nTS_dataset(i_fish, i_model,:,:);
% nTS_tmp = squeeze(nTS_tmp);
% plot(tif_MyFrame, nTS_tmp(i_freq4ThetaPlot,:),'LineStyle',mark_{i_model}) 
% end
% end
% 
% title(strcat('nTS versus tilt angle - Tomofish sardines - f=',num2str(freqs(i_freq4ThetaPlot)*1e-3),'kHz'));
% xlabel('Tilt angle [�]'); ylabel('length-normalised TS [dB]');
% legend(fish_legend);
% 
% % bladder tilt vertical line plot
% for i_fish = 1:Nfish
%     y_lim = [-100, 0];
% plot((bladder_tilt(i_fish))*ones(size(y_lim)), y_lim, '-')
% end
% 
% for i_model = 1:Nmodel
% nTS_tmp = nTS_dataset(:,i_model,:,:);
% nTS_tmp_fishAveraged = 10*log10(mean(10.^(nTS_tmp/10),1));
% nTS_tmp_fishAveraged = squeeze(nTS_tmp_fishAveraged);
% if i_model == 2 % KRM
% plot(tif_MyFrame, nTS_tmp_fishAveraged(i_freq4ThetaPlot,:),'LineStyle',mark_{i_model}) 
% else % Stanton88 or PSMS
% plot(tif_MyFrame+mean(bladder_tilt), nTS_tmp_fishAveraged(i_freq4ThetaPlot,:),'LineStyle',mark_{i_model})
% end
% end
% title(strcat('nTS versus tilt angle - simulated sardines - f=',num2str(freqs(i_freq4ThetaPlot)*1e-3),'kHz'));
% xlabel('Tilt angle [�]'); ylabel('mean length-normalised TS [dB]');
% 
% % ka dependancy
% figure; hold on; grid on;
% for i_model = 1:Nmodel
% x_axis = mean(kL_distrib,1)/2; % signification ????
% nTS_tmp = nTS_tiltAveraged_dataset(:,i_model,:); % dim : Nfish*1*Nfreq
% nTS_tmp_tiltAveraged_fishAveraged = 10*log10(mean(10.^(nTS_tmp/10),1)); 
% nTS_tmp_tiltAveraged_fishAveraged = squeeze(nTS_tmp_tiltAveraged_fishAveraged); % dim : Nfreq
% plot(x_axis, nTS_tmp_tiltAveraged_fishAveraged,'LineStyle',mark_{i_model});
% end
% title(strcat('tilt-averaged nTS versus ka - simulated sardines - Tilt distribution (m=-2.6�, std=16�)'));
% xlabel('ka with a the semi-length'); ylabel('tilt-averaged fish-averaged length-normalised TS [dB]');
% 
% % ka dependancy
% figure; 
% for i_model = models_ON
% hold on; grid on;
% for i_fish = 1:Nfish % loop on fishes
% plot(kL_distrib(i_fish,:)/2, squeeze(nTS_tiltAveraged_dataset(i_fish,i_model,:)),'LineStyle',mark_{i_model},'color',c(i_fish,:))
% end
% end
% title(strcat('tilt-averaged nTS versus ka - Tomofish sardines - Tilt distribution (m=4.4�, std=16�)'));
% xlabel('ka with a the semi-length'); ylabel('tilt-averaged length-normalised TS [dB]');
% legend(fish_legend);
%     
% end

%% stop the end of the script:
return

%%

%
frequency = { [   71.9430   76.4490   80.9560   85.4620   89.9690 ...
    94.4760   98.9820  103.4890  107.9950  112.5020  118.0570  ...
    114.7550  110.2490  105.7420  101.2350   96.7290   92.2220   ...
    87.7160   83.2090 78.7020  74.1960]*1e3;
    [18 38 70 120]*1e3};
Nbeam = {length(frequency{1}) ; length(frequency{2})};

steering = {[-42.20577 -35.92659 -30.43624 -25.51571 -21.02889 ...
    -16.88417 -13.01624 -9.376566 -5.927816 -2.640473 0.495642 ...
    3.601663 6.827438 10.21141 13.78197 17.57491 21.63671 26.02967 ...
    30.84084 36.19885 42.30876] * (pi/180);
    0*ones(1,Nbeam{2})};

BW = {3.5*1e3*ones(1,Nbeam{1});
    3*1e3*ones(1,Nbeam{2})};

for i_echosounder = 1:length(Nbeam)

yaw=(360*rand(1,Nang));
% yaw=(0*ones(1,Nang));
% yaw(1:round(Nang/2))=(normrnd(-45,std_ta,1,round(Nang/2)));
% yaw(1+round(Nang/2):Nang)=(normrnd(45,std_ta,1,Nang-round(Nang/2)));
yaw(1:round(Nang/2))= -45 + std_ta*randn(1,round(Nang/2));
yaw(1+round(Nang/2):Nang)= 45 + std_ta*randn(1,Nang-round(Nang/2));

roll=0*ones(1,Nang);
tilt=anglen;

% ka dependancy
nTS_tiltAveraged_dataset_apparentangle = zeros(Nfish,Nmodel,Nbeam{i_echosounder});

for i_beam = 1:Nbeam{i_echosounder}
    if i_model > 1
        tilt = tilt + bladder_tilt(i_fish);
    else
        tilt = tilt;
    end
    
    apparenttilt=zeros(length(tilt),1);
    %     apparentyaw=zeros(length(yaw),1);
    apparentroll=zeros(length(roll),1);
    for i=1:Nang
        Rfish=CreateRotationMatrix(yaw(i)*(pi/180),(tilt(i)-90)*(pi/180),roll(i)*(pi/180));
        Rbeam=CreateRotationMatrix(0*pi/180,0,- steering{i_echosounder}(i_beam));
        Vbeam=Rbeam*[0;0;1;];
        Vfish=-Rfish*Vbeam;
        %                 apparenttilt1(i)=atan2(Vfish(2),Vfish(1))*180/pi;
        %                 apparentroll1(i)=sign(sin(atan2(Vfish(2),Vfish(1))))*atan2((Vfish(1)^2+Vfish(2)^2)^0.5,Vfish(3))*180/pi;
        % %                             apparentroll(i)=atan2((Vfish(1)^2+Vfish(2)^2)^0.5,Vfish(3))*180/pi;
        apparenttilt(i)=(acos(dot(Vbeam,Rfish*[1;0;0;])))*180/pi;
        apparentroll(i)=acos(dot(Vbeam,cross(Rfish*[1;0;0;],[0;0;1])))*180/pi;
    end
    
    % Incidence angles (degrees)
    apparentphi(i_beam,:) = mod(apparenttilt,180);              % Incidence angles (degrees)
    apparentpsi(i_beam,:)=mod(apparentroll,180);
    
  
    
    i_freq= find(abs(freqs-frequency{i_echosounder}(i_beam))<=BW{i_echosounder}(i_beam)/2);
    for i_fish = 1:Nfish % loop on fishes
        for i_model = 1:Nmodel
            nTS_tmp = nTS_dataset(i_fish, i_model,:,:); % dim : freq * angle
            nTS_tmp = squeeze(nTS_tmp);
            %
%             nTS_tmp = nTS_dataset{i_fish, i_model};
            nTS_anglen_dataset_tmp = interp1(tif_MyFrame, nTS_tmp(i_freq(1),:), apparentphi(i_beam,:), 'nearest','extrap');
            nTS_tiltAveraged_dataset_tmp = 10*log10(mean(10.^(nTS_anglen_dataset_tmp/10)));
            nTS_tiltAveraged_dataset_apparentangle(i_fish,i_model,i_beam) = nTS_tiltAveraged_dataset_tmp;
        end
    end
end

if d == 1 || d == 2 % real data
figure; 
for i_model = 1:Nmodel
hold on; grid on;
for i_fish = 1:Nfish % loop on fishes
if i_echosounder == 1
plot(steering{i_echosounder}*180/pi, 20*log10(Ln(i_fish))+squeeze(nTS_tiltAveraged_dataset_apparentangle(i_fish,i_model,:)),'LineStyle',mark_{i_model})
xlabel('steering angle (�)');
elseif i_echosounder == 2
plot(frequency{i_echosounder}*1e-3, 20*log10(Ln(i_fish))+squeeze(nTS_tiltAveraged_dataset_apparentangle(i_fish,i_model,:)),'LineStyle',mark_{i_model})
xlabel('Frequency (kHz)');
end
end
end
title(strcat('tilt-averaged nTS versus ka - Tomofish sardines - Tilt distribution (m=4.4�, std=16�)'));
ylabel('tilt-averaged length-normalised TS [dB]');
legend(fish_legend);

elseif d == 3

figure; 
for i_model = 1:Nmodel
hold on; grid on;
nTS_tmp = nTS_tiltAveraged_dataset_apparentangle(:,i_model,:);
TS_tmp_tiltAveraged_dataset = 10*log10(repmat(Ln.^2,1,1,Nbeam{i_echosounder}).*10.^(nTS_tmp/10));  % nTS to TS
TS_tmp_fishAveraged_tiltAveraged_dataset = 10*log10(mean(10.^(TS_tmp_tiltAveraged_dataset/10),1)); % average over fishes
TS_tmp_fishAveraged_tiltAveraged_dataset = squeeze(TS_tmp_fishAveraged_tiltAveraged_dataset);
if i_echosounder == 1
plot(steering{i_echosounder}*180/pi, TS_tmp_fishAveraged_tiltAveraged_dataset,'LineStyle',mark_{i_model})
xlabel('steering angle (�)');
elseif i_echosounder == 2
plot(frequency{i_echosounder}*1e-3, TS_tmp_fishAveraged_tiltAveraged_dataset,'LineStyle',mark_{i_model})
xlabel('Frequency (kHz)');
end
end
title(strcat('tilt-averaged nTS versus ka - simulated sardines - Tilt distribution (m=4.4�, std=16�)'));
ylabel('tilt-averaged fish-averaged length-normalised TS [dB]');
    
end
end


%% %% TS law with Do&Surti1990 method

% Nfish_TSlaw = 15; % number of fishes
% freq_TSlaw = [18, 38, 70, 120, 200]*1e3; % frequencies (Hz)
% Nfreq_TSlaw = length(freq_TSlaw); % number of frequencies
% anglen_TSlaw = anglen; % we use previous tilt angle distribution (�)
% kL_TSlaw = zeros(Nfish_TSlaw, Nfreq_TSlaw);
% TSmean_TSlaw = zeros(Nfish_TSlaw, Nfreq_TSlaw);
% b20_St88 = zeros(Nfreq_TSlaw,1);
% 
% % fish length distribution parameters :
% m_Lfish = 16*1e-2; % tilt angle mean (�)
% std_Lfish = 1*1e-2; % tilt angle std (�)
% Lfish_TSlaw = m_Lfish + std_Lfish * randn(1, Nfish_TSlaw); % probality density distribution (PDF)
% % Truncate at 95% the PDF :
% Lfish_TSlaw = Lfish_TSlaw(Lfish_TSlaw >=  m_Lfish - 3*std_Lfish & Lfish_TSlaw <= m_Lfish + 3*std_Lfish);
% Lbladder_TSlaw = 0.3 * Lfish_TSlaw;
% 
% % bladder flattening distribution parameters :
% m_abfish = 10; % tilt angle mean (�)
% std_abfish = 0.2; % tilt angle std (�)
% abfish_TSlaw = m_abfish + std_abfish * randn(1, Nfish_TSlaw); % probality density distribution (PDF)
% % Truncate at 95% the PDF :
% abfish_TSlaw = abfish_TSlaw(abfish_TSlaw >=  m_abfish - 3*std_abfish & abfish_TSlaw <= m_abfish + 3*std_abfish);

TS_tiltAveraged_dataset = 10 * log10(10.^(nTS_tiltAveraged_dataset/10).*repmat(Ln.^2,1,Nmodel,Nfreq));

b20_DoSurtiMethod = zeros(Nmodel,Nfreq);

for i_model = 1:Nmodel
for i_freq = 1:Nfreq
b20_DoSurtiMethod(i_model,i_freq) = polyfit(zeros(Nfish,1),TS_tiltAveraged_dataset(:,i_model,i_freq)-20*log10(Ln_fish_cm),0);
end
end

figure % model 1
plot(freqs*1e-3, b20_DoSurtiMethod(1,:),'x-'); grid on;
title('b_{20} - Do&Surti method - Stanton acoustic model'); xlabel('Frequency (kHz)'); ylabel('b_{20} [dB]');
