clear all;
close all;

% nTSmean=zeros(3,7,2000);
% nTSstd=zeros(3,7,2000);


m_ta   = [0]; % angle mean (�), <0 according to our orientation of reference
std_ta = 5; % angle std (�)

% nTS_hybrid = zeros(length(m_ta),2000);

i_model_KRM         = 1;
i_model_straightcyl = 2;
i_model_bentcyl     = 3;
i_model_ell         = 4;
i_model_deformedcyl = 5;
i_model_straightcyl_end_eff = 6;
i_model_Ye          = 7;
model_desc = {'KRM'; 'Straight Cylinder'; 'Bent cylinder'; 'Ellipsoid'; 'Deformed cylinder'; 'Straight cylinder with end effect'; 'Ye low frequency model'};
mark_ = {'--','-','--','-.',':','--',':'}; % LineStyle for each model : Stanton88, PSMS, KRM
facecolor_ = {'k',[0 0 1]*0.8,[1 0 0]*0.8,[0 1 0]*0.8,[1 1 1]*0.3,'c','m'}; % LineStyle for each model : Stanton88, PSMS, KRM
% Choice of scattering models :
models_ON = [i_model_straightcyl, i_model_ell, i_model_Ye]; % i_model_KRM, i_model_straightcyl, i_model_bentcyl, i_model_ell, i_model_deformedcyl, i_model_straightcyl_end_eff


for i=1:length(m_ta)
    
load(['in/nTS_panache_',num2str(round(m_ta(i))),'_',num2str(round(std_ta)),'.mat']);

nTSmean(i,:,:)=nTSmean_fishAll_dataset;
nTSstd(i,:,:)=nTSstd_fishAll_dataset;

ind=find(kaeq_distrib_plot/2>0.05);
nTS_hybrid(i,1:ind(1))=squeeze(nTSmean(i,i_model_Ye,1:ind(1)));
nTS_hybrid(i,ind(1):ind(end))=squeeze(nTSmean(i,i_model_ell,ind(1):ind(end)));


end


figure; hold on; grid on;
for i_nage = 1:length(m_ta)
plot(kaeq_distrib_plot/2,abs(nTS_hybrid(i_nage,:)-squeeze(nTSmean(i_nage,i_model_Ye,:))'),'kx', 'LineStyle',mark_{i_nage}, 'LineWidth', 2, 'Color',facecolor_{i_nage});
end
set(gca,'XScale','log')


for i_nage = 1:length(m_ta)
figure; hold on; grid on;
for i_model = models_ON
plot(kaeq_distrib_plot/2,squeeze(nTSmean(i_nage,i_model,:)),'kx', 'LineStyle',mark_{i_nage}, 'LineWidth', 2, 'Color',facecolor_{i_model});
end
set(gca,'XScale','log')
end



