function nf_BS = PSM_model( freqs, b, a, ti, c_b, c_g, g )
%f_BS : computes the reduced scattering function for a cylinder of finite length L0

%   INPUT
% - L0    : cylinder length (m)
% - L_b0  : the dimensionless ratio for longitudinal length (L) by cross-sectional radius (b)
% - f0    : acoustic wave frequency (Hz)
% - c_b   : surrounding sound velocity
% - c_G   : gas sound velocity
% - g     : mass density ratio
% - ti0   : incident angle (�) (convention detailled in figure 1 from Ye1997)
% 
%   OUTPUT
%           - sigma_BS  : reduced (ie. length-normalised) backscattering cross section function


%% Parameters computation
nf_BS=zeros(length(freqs),length(ti));
for i=1:length(freqs)
    for j=1:length(ti)
        %         if j<=(length(ti)-1)/2+1
        if g>0
            F=psms(freqs(i), c_b, b,a, ti(j),c_g,g);
        else
            F=psms(freqs(i), c_b, b,a, ti(j));
            nf_BS(i,j) =F(end);
        end
%         else
%              nf_BS(i,j)= nf_BS(i,length(ti)-j+1);
%         end
    end
    freqs(i)
end

end





