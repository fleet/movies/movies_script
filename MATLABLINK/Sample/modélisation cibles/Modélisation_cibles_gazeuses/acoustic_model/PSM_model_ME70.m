function KRM_model_ME70
% KRM_MODEL     Calculate TS of fish using Helmholts Kirchoff model
%   KRM_MODEL calculates the Target Strength (TS) of a given fish shape for
%   user-specified values of fish length, orientation, and frequency.  The
%   user may also specify parameters of soundspeed and density used in the
%   model calculations.  The program outputs TS for the given
%   parameters, along with figures and a .txt file containing the results.
%
%   FISH SHAPE
%       The shape of the fish must be defined in a Matlab .mat file
%       consisting of the following variables, where units are in meters:
%           x_b --> Nx1 vector defining the length of the fish body.
%           x_sb --> Mx1 vector defining the length of the fish
%               swimbladder.
%           z_b --> Nx2 array where the 1st and 2nd columns define the
%               upper and lower coordinates of the fish body, respectively.
%           z_sb --> Mx2 array where the 1st and 2nd columns define the
%               upper and lower coordinates of the fish swimbladder,
%               respectively.
%           w_b = Nx1 vector defining the width of the fish body from the
%               dorsal aspect.
%           w_sb = Mx1 vector defining the width of the fish swimbladder
%               from the dorsal aspect.
%
%       Depending on what the user inputs for lengths to use for the model,
%       the variables describing the fish shape are then scaled to those
%       lengths.  Lastly, the shape vectors are then interpolated so that
%       there is a 1mm cylinder resolution.
%
%   PROCESSING PARAMETERS
%       The program will prompt the user for values defining the
%       soundspeeds (m/s) and densities (kg/m^3) of the fish body,
%       swimbladder, and surrounding water.  Each input may only contain a
%       single value.
%
%       A prompt will also be given for the user to select ranges of fish
%       lengths (m), orientations (degrees), and frequencies (Hz) for use
%       in the model:
%           Fish Lengths --> A numeric array giving the fish lengths, in
%               meters, for the program to process.  Any Matlab command can
%               be used here, so long as the result is a numeric array
%               (e.g. normal or uniform distribution).
%
%           Frequencies --> A numeric array giving the frequencies, in Hz,
%               for the program to process.
%
%           Incidence Angles --> A numeric array giving the incidence
%               angles, in degrees, for the program to process.  A dorsal
%               incident orientation equates to 90 degrees.  Typically, the
%               KRM model is only valid for orientations between 65 and 115
%               degrees.
%
%           Save filename --> A string defining the filename to use for
%               saving the results in .txt files.
%
%   OUTPUT
%       The program returns an MxNxP matrix with M lengths, N frequencies,
%       and P orientations.
%
%       The program also plots figures of the defined fish and swimbladder
%       contour, TS vs. fish length, TS vs. frequency, and TS vs. incidence
%       angle.  For each plot of TS, the TS will be averaged out over the
%       other two parameters.  For example, when plotting TS vs. fish
%       length, the TS for each length will be calculated by averaging the
%       TS for all combinations of that length at all frequencies and
%       incidence angles.
%
%       Additionally, comma-separated-value .txt files will be created for
%       TS vs. length, TS vs. frequency, and TS vs. incidence angle.  As in
%       the figures, the TS calculated for one parameters is the average TS
%       over every combination of the other two parameters.  The .txt
%       files will be saved in the working directory and appended with
%       'TSvs[L | F | Phi]'.
%
%   NOTES
%       Equations and methods used in this program were taken from
%       'Acoustic models of fish: The Atlantic cod (Gadus morhua)' by Clay
%       and Horne, 1994.  Please refer to this paper for any questions
%       regarding parameter definitions or orientation clarification.

% Created by Josiah Renfree
% February 24, 2011
% Modified by Laurent Berger
% march 2012
% 
% Make sure there are no inputs
% if nargin ~= 0
%     error('This function does not accept input variables')
% end
% 

%http://mathworld.wolfram.com/ProlateSpheroidalWaveFunction.html?affilliate=1

% Get fish body and swimbladder shape

% Initialize variables
x_b = []; x_sb = []; z_b = []; z_sb = []; w_b = []; w_sb = [];

% Prompt user for file that describes fish shape
[filename, pathname] = uigetfile( {'*.mat', 'MAT files (*.mat)'; ...
    '*.*',  'All Files (*.*)'}, 'Pick fish shape file');

% Check to see if user pressed cancel
if isempty(filename)
    error('User pressed cancel')
end

% Load data from that file
load(fullfile(pathname, filename))

% Check that the correct variables exist
if sum(cellfun(@isempty, {x_b x_sb z_b z_sb w_b w_sb}))
    error('MAT file did not contain the correct fish shape variables.')
end

if (strcmp(filename,'herring.mat') || strcmp(filename,'herring2.mat') || strcmp(filename,'herring_10m.mat') || strcmp(filename,'herring_20m.mat') || strcmp(filename,'herring_40m.mat'))
    x_b=x_b/1000;
    x_sb=x_sb/1000;
    z_b=z_b/1000;
    z_sb=z_sb/1000;
    w_b=w_b/1000;
    w_sb=w_sb/1000;
end

%% Plot fish shape
figure

% Plot lateral view
subplot(2,1,1)
hold on

% Plot fish body
plot(x_b, z_b(:,1), 'k', x_b, z_b(:,2), 'k')            % Upper and lower
plot([x_b(1) x_b(1)], [z_b(1,1) z_b(1,2)], 'k')         % Left end
plot([x_b(end) x_b(end)], [z_b(end,1) z_b(end,2)], 'k') % Right end

% Plot fish bladder
plot(x_sb, z_sb(:,1), 'k', x_sb, z_sb(:,2), 'k')        % Upper and lower
plot([x_sb(1) x_sb(1)], [z_sb(1,1) z_sb(1,2)], 'k')     % Left end
plot([x_sb(end) x_sb(end)], [z_sb(end,1) z_sb(end,2)], 'k') % Right end


% approximate with ellipse fish bladder in lateral view
a_sb=(z_sb(end/2,1)-z_sb(end/2,2))/4+(w_sb(end/2))/4;
b_sb=(x_sb(end)-x_sb(1))/2;

cx=mean(x_sb);
cz=mean((z_sb(:,1)+z_sb(:,2))/2);

teta=atan(((z_sb(end,1)+z_sb(end,2))/2-(z_sb(1,1)+z_sb(1,2))/2)/(x_sb(end)-x_sb(1)));
% teta=-7*pi/180

z_sb(:,1)=cz+sin(teta)*(x_sb-cx)+cos(teta)*(a_sb*sqrt(abs(1-(x_sb-cx).^2/b_sb^2)));
z_sb(:,2)=cz+sin(teta)*(x_sb-cx)-cos(teta)*(a_sb*sqrt(abs(1-(x_sb-cx).^2/b_sb^2)));

% approximate with ellipse fish body in lateral view
a_b=(z_b(end/2,1)-z_b(end/2,2))/4+(w_b(end/2))/4;
b_b=(x_b(end)-x_b(1))/2;

cx=mean(x_b);
cz=mean((z_b(:,1)+z_b(:,2))/2);

teta=atan(((z_b(end,1)+z_b(end,2))/2-(z_b(1,1)+z_b(1,2))/2)/(x_b(end)-x_b(1)));
% teta=0*pi/180

z_b(:,1)=cz+sin(teta)*(x_b-cx)+cos(teta)*(a_b*sqrt(abs(1-(x_b-cx).^2/b_b^2)));
z_b(:,2)=cz+sin(teta)*(x_b-cx)-cos(teta)*(a_b*sqrt(abs(1-(x_b-cx).^2/b_b^2)));


% rotate swimbladder
% cx=mean(x_sb);
% cz=mean((z_sb(:,1)+z_sb(:,2))/2);
% 
% teta=5*pi/180
% 
% z_sb(:,1)=cz+sin(teta)*(x_sb-cx)+cos(teta)*z_sb(:,1);
% z_sb(:,2)=cz+sin(teta)*(x_sb-cx)+cos(teta)*z_sb(:,2);

% Plot fish body
plot(x_b, z_b(:,1), 'r', x_b, z_b(:,2), 'r')            % Upper and lower
plot([x_b(1) x_b(1)], [z_b(1,1) z_b(1,2)], 'r')         % Left end
plot([x_b(end) x_b(end)], [z_b(end,1) z_b(end,2)], 'r') % Right end

% Plot fish bladder
plot(x_sb, z_sb(:,1), 'r', x_sb, z_sb(:,2), 'r')        % Upper and lower
plot([x_sb(1) x_sb(1)], [z_sb(1,1) z_sb(1,2)], 'r')     % Left end
plot([x_sb(end) x_sb(end)], [z_sb(end,1) z_sb(end,2)], 'r') % Right end

axis equal              % make axes equal
title('Lateral view')
ylabel('Height (m)')

% Plot dorsal view
subplot(2,1,2)
hold on

% Plot fish body
plot(x_b, w_b/2, 'k', x_b, -w_b/2, 'k')                 % Upper and lower
plot([x_b(1) x_b(1)], [-w_b(1)/2 w_b(1)/2], 'k')        % Left end
plot([x_b(end) x_b(end)], [-w_b(end)/2 w_b(end)/2], 'k')	% Right end

% Plot fish bladder
plot(x_sb, w_sb/2, 'k', x_sb, -w_sb/2, 'k')             % Upper and lower
plot([x_sb(1) x_sb(1)], [-w_sb(1)/2 w_sb(1)/2], 'k')    % Left end
plot([x_sb(end) x_sb(end)], [-w_sb(end)/2 w_sb(end)/2], 'k')    % Right end

% approximate with ellipse fish bladder in dorsal view


cx=mean(x_sb);

w_sb(:,1)=2*(a_sb*sqrt(abs(1-(x_sb-cx).^2/b_sb^2)));

% approximate with ellipse fish body in dorsal view


cx=mean(x_b);

w_b(:,1)=2*(a_b*sqrt(abs(1-(x_b-cx).^2/b_b^2)));

% Plot fish body
plot(x_b, w_b/2, 'r', x_b, -w_b/2, 'r')                 % Upper and lower
plot([x_b(1) x_b(1)], [-w_b(1)/2 w_b(1)/2], 'r')        % Left end
plot([x_b(end) x_b(end)], [-w_b(end)/2 w_b(end)/2], 'r')	% Right end

% Plot fish bladder
plot(x_sb, w_sb/2, 'r', x_sb, -w_sb/2, 'r')             % Upper and lower
plot([x_sb(1) x_sb(1)], [-w_sb(1)/2 w_sb(1)/2], 'r')    % Left end
plot([x_sb(end) x_sb(end)], [-w_sb(end)/2 w_sb(end)/2], 'r')    % Right end

axis equal
title('Dorsal view')
ylabel('Width (m)')
xlabel('Length (m)')

span = 3; % Size of the averaging window
window = ones(span,1)/span;
z_b(:,1) = convn( z_b(:,1),window,'same');
z_b(:,2) = convn( z_b(:,2),window,'same');
z_sb(:,1) = convn( z_sb(:,1),window,'same');
z_sb(:,2) = convn( z_sb(:,2),window,'same');
w_b = convn( w_b,window,'same');
w_sb = convn( w_sb,window,'same');



nbangles=128;

nbPoints1=length(x_b);
xb=NaN(nbPoints1,nbangles);
yb=NaN(nbPoints1,nbangles);
zb=NaN(nbPoints1,nbangles);
teta=NaN(1,nbangles);
for i=1:nbPoints1
    xb(i,:)=x_b(i)*ones(1,length(teta));
    for j=1:nbangles
        c=(z_b(i,1)+z_b(i,2))/2;
        teta(j)=-pi+(j-1)*2*pi/(nbangles-1);
        if w_b(i)/2<=(z_b(i,1)-z_b(i,2))/2
            a=w_b(i)/2;
            b=(z_b(i,1)-z_b(i,2))/2;
        else
            a=(z_b(i,1)-z_b(i,2))/2;
            b=w_b(i)/2;
        end
        if a>0 && b>0
            yb(i,j)=(b/(1-(1-(b^2/a^2))*(cos(teta(j)))^2)^0.5)*cos(teta(j));
            zb(i,j)=c+(b/(1-(1-(b^2/a^2))*(cos(teta(j)))^2)^0.5)*sin(teta(j));
        else
            yb(i,j)=0;
            zb(i,j)=c;
        end
    end
end

nbPoints2=length(x_sb);
xsb=NaN(nbPoints2,nbangles);
ysb=NaN(nbPoints2,nbangles);
zsb=NaN(nbPoints2,nbangles);
teta=NaN(1,nbangles);
for i=1:nbPoints2
    xsb(i,:)=x_sb(i)*ones(1,length(teta));
    for j=1:nbangles
        c=(z_sb(i,1)+z_sb(i,2))/2;
        teta(j)=-pi+(j-1)*2*pi/(nbangles-1);
        if w_sb(i)/2<(z_sb(i,1)-z_sb(i,2))/2
            a=w_sb(i)/2;
            b=(z_sb(i,1)-z_sb(i,2))/2;
        else
            a=(z_sb(i,1)-z_sb(i,2))/2;
            b=w_sb(i)/2;
        end
        if a>0 && b>0
            ysb(i,j)=(b/(1-(1-(b^2/a^2))*(cos(teta(j)))^2)^0.5)*cos(teta(j));
            zsb(i,j)=c+(b/(1-(1-(b^2/a^2))*(cos(teta(j)))^2)^0.5)*sin(teta(j));
        else
            ysb(i,j)=0;
            zsb(i,j)=c;
        end
    end
end

step=5;
figure;
mesh(xb(1:step:end,1:step:end),yb(1:step:end,1:step:end),zb(1:step:end,1:step:end),'EdgeColor','black');
hold on
mesh(xsb(1:step:end,1:step:end),ysb(1:step:end,1:step:end),zsb(1:step:end,1:step:end),'EdgeColor','red');
alpha(.4)
axis([0 0.2 -0.1 0.1 -0.1 0.1]);
% axis equal;
grid off;



T=[reshape(xsb,1,[]);reshape(ysb,1,[]);reshape(zsb,1,[])];
[~,Vsb]=convhulln(T');
U=[reshape(xsb,1,[]);reshape(ysb,1,[])];
[~,Asb]=convhulln(U');
T=[reshape(xb,1,[]);reshape(yb,1,[]);reshape(zb,1,[])];
[~,Vb]=convhulln(T');
U=[reshape(xb,1,[]);reshape(yb,1,[])];
[~,Ab]=convhulln(U');

Vsb
Asb

Vsb/Vb*100
Asb/Ab*100

% 


%% Get fish and water parameters

% Prompt user for fish parameters
prompt = {'Fish body density (kg/m^3):', ...
    'Fish body soundspeed (m/s):', ...
    'Swimbladder density (kg/m^3):', ...
    'Swimbladder soundspeed (m/s):', ...
    'Water density (kg/m^3):', ...
    'Water soundspeed (m/s):'};
name = 'Inputs for fish and water parameters';
numlines = 1;
% defaultanswer = {'1070', '1570', '1.24', '345', '1030', '1480'}; % Demer
defaultanswer = {'1000', '1500', '1.24', '345', '1000', '1500'}; %Fassler
% defaultanswer = {'1.24', '345', '1.24', '345', '1030', '1490'}; % !! pour bulles uniquement !

answer = inputdlg(prompt,name,numlines,defaultanswer);

% If user pressed cancel
if isempty(answer)
    error('User pressed cancel')
end

% Parse out inputs to variables
p_b = str2double(answer{1});    % fish body density (kg/m^3)
c_b = str2double(answer{2});    % fish body soundspeed (m/s)
p_c = str2double(answer{3});    % swimbladder density (kg/m^3)
c_c = str2double(answer{4});    % swimbladder soundspeed (m/s)
p_w = str2double(answer{5});    % water density (kg/m^3)
c_w = str2double(answer{6});    % water soundspeed (m/s)


depth=0;
p_c=p_c*(1+0.1*depth);

freqs = logspace(log10(10000),log10(333000),50);
% freqs=[65000:1000:125000];
% freqs=[35000:1000:41000];
% freqs=38000;
phi=[40:2:140];
psi=90;
% lengths=([1:1:35])*1e-2;
lengths=10*1e-2;
% lengths=10*1e-2;
TS=zeros(length(psi),length(freqs),length(lengths),length(phi));

filesave = datestr(now, 'ddmmmyyyy_HHSS');               % Save filename
% % filesave='';





% If a vector is given for sound speed, then give error
if length(c_w) > 1
    error('Sound speed must be a singular value');
end


% Begin processing data
h = waitbar(0, 'Processing data...');   % Create waitbar
count = 0;                              % Count variable for waitbar

% Initialize scattering length variables for fish body and swimbladder
softL = zeros(length(psi),length(lengths), length(freqs), length(phi));
softLr = zeros(length(psi),length(lengths), length(freqs), length(phi));
fluidL = zeros(length(psi),length(lengths), length(freqs), length(phi));

ka=nan(length(lengths), length(freqs));
kb=nan(length(lengths), length(freqs));
toto=0;
% Cycle through lengths
for i = 1:length(lengths)
    
    % Scale the body and bladder shape by the current fish length
    x_b2 = x_b .* lengths(i)/(x_b(end)-x_b(1));
    x_sb2 = x_sb .* lengths(i)/(x_b(end)-x_b(1));
    z_b2 = z_b .* lengths(i)/(x_b(end)-x_b(1));
    z_sb2 = z_sb .* lengths(i)/(x_b(end)-x_b(1));
    w_b2 = w_b .* lengths(i)/(x_b(end)-x_b(1));
    w_sb2 = w_sb .* lengths(i)/(x_b(end)-x_b(1));
    
    b_b=(z_b2(end/2,1)-z_b2(end/2,2))/4+(w_b2(end/2))/4;
    a_b=(x_b2(end)-x_b2(1))/2;

    teta_b=atand(((z_b(end,1)+z_b(end,2))/2-(z_b(1,1)+z_b(1,2))/2)/(x_b(end)-x_b(1)));
    
    b_sb=(z_sb2(end/2,1)-z_sb2(end/2,2))/4+(w_sb2(end/2))/4;
    a_sb=(x_sb2(end)-x_sb2(1))/2;
    a_sb/b_sb
    a_sb=0.05;
    b_sb=0.01;
    
    teta_sb=atand(((z_sb(end,1)+z_sb(end,2))/2-(z_sb(1,1)+z_sb(1,2))/2)/(x_sb(end)-x_sb(1)));
    teta_sb=0;
    
%     b_sb=b_sb*0.55*5;
    
    % Cycle through frequencies
    for j = 1:length(freqs)
        
%         position1=find(abs(ka-(2*pi*freqs(j)/c_b)*a_sb)<0.1);
        position2=find(abs(kb-(2*pi*freqs(j)/c_b)*b_sb)<0.0001);
%         if ~isempty(position1) && ~isempty(position2)
            if  ~isempty(position2)
            %#transform the index in the 1D view to 2 indices, given the size of A
%             [k1,l1] = ind2sub(size(ka),position1);
            [k2,l2] = ind2sub(size(kb),position2);
            softL(:,i,j,:) =2*a_sb*softLr(:,min(k2),min(l2),:);
             ka(i,j) = ka(min(k2),min(l2));
             kb(i,j) = kb(min(k2),min(l2));
            
        else
            % Cycle through each incidence angle
            for m = 1:length(phi)
                
%                 % Calculate scattering length of the fish body
%                 F=psms(freqs(j), c_w,a_b, b_b, phi(m)-teta_b, c_b, p_b/p_w);
%                 fluidL(:,i,j,m) = 2*a_b*F(end);
%                 
% % %                 Calculate scattering length of the swimbladder
%                 F=psms(freqs(j), c_b, a_sb,b_sb, phi(m)-teta_sb, c_c, p_c/p_b);
                 F=psms(freqs(j), c_b, a_sb,b_sb, phi(m)-teta_sb);
                softLr(:,i,j,m) =F(end);
                softL(:,i,j,m) =2*a_sb*F(end);
                

            end
             toto=toto+1
             ka(i,j) = (2*pi*freqs(j)/c_b)*a_sb;
             kb(i,j) = (2*pi*freqs(j)/c_b)*b_sb;
        end
        

        
        % Update waitbar
        count = count+1            % Increment count
        
        % Want waitbar to only update 100 times
        if mod(count, floor((length(lengths) * length(freqs) * ...
                length(phi))/100)) == 0
            waitbar(count/(length(lengths) * length(freqs) * ...
                length(phi)), h)
            ph1 = findobj(h, 'type', 'patch');
            set(ph1, 'FaceColor', [0 0.8 0.8], 'EdgeColor', [0 0.8 0.8]);
            
        end
    end
end

close(h)

figure
plot(sort(reshape(ka,[],1)))

 figure
plot(sort(reshape(kb,[],1)))

% If user provided filename, then save data
if ~isempty(filesave)
    save(filesave, 'softL', 'fluidL', 'lengths', ...
        'freqs', 'phi', 'psi', 'c_b', 'c_c', 'c_w', 'p_b', 'p_c', 'p_w', ...
        'w_b', 'w_sb', 'x_b', 'x_sb', 'z_b', 'z_sb', 'filesave')
end

