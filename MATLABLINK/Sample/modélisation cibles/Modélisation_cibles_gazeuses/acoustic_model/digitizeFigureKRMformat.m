% digitize figure

clear *; close all; clc;
dbstop if error
addpath(genpath(pwd)) % add path for subfolder 'my_functions' and for input data (swimbladder *.stl)
cd(fileparts(mfilename('fullpath'))); % set the current Matlab folder = script directory

%% Fish bladder geometry
load('shape_fig11_Reeder2004.mat'); % unit : meter

% save('shape_fig11_Reeder2004.mat','up_xz','down_xz');

x_all = [xz_down(:,1);xz_up(:,1)];
x_min = min(x_all);
x_max = max(x_all);
dx = 1e-4; % 0.1 mm
x = x_min:dx:x_max;

% Interpolation
z_up = interp1(xz_up(:,1),xz_up(:,2),x,'linear','extrap');
z_down = interp1(xz_down(:,1),xz_down(:,2),x,'linear','extrap');

% Add 2 points : head and tail
z_tail = 0;
z_head = mean([z_up(end), z_down(end)]);
z_up = [z_tail, z_up, z_head];
z_down = [z_tail, z_down, z_head];
x = [x(1)-dx, x, x(end)+dx];

figure; subplot(2,1,1); hold on;
plot(x*1e3,z_up*1e3,'k')
plot(x*1e3,z_down*1e3,'k')
text(x(1)*1e3,z_tail,' \leftarrow tail')
text(x(end)*1e3,z_head,' \leftarrow head')
title('Shape #2 - Figure 12 from Reeder2004'); xlabel('mm'); ylabel('mm');

%%
x = x';
z_down = z_down';
z_up = z_up';

x_sb = x; % x coordinates of the slice
x_b  = x_sb;
w_sb = (z_up - z_down); % thickness of the slice
w_b  = w_sb;
z_sb = [z_down, z_up]; % z boundaries of the slice
z_b  = z_sb;

save('in/ImageDigitized/shape_Reeder2004_fig11.mat', 'x_sb', 'x_b', 'w_sb', 'w_b', 'z_sb', 'z_b'); % Data export

% The coordinate (0,0) from shape#2 (figure 12 Reeder2004) is the tail
% according to caption to figure 11 from Reeder2004. In the Clay&Horne1994
% convention, the coordinate (0,0) is the head. So, we have to reverse the
% x-axis in order to set the coordinate (0,0) for the head :
w_sb = flipud(w_sb); % thickness of the slice (must stay coherent with x_coord)
w_b  = w_sb;
z_sb = flipud(z_sb); % z boundaries of the slice (must stay coherent with x_coord)
z_b  = z_sb;

% !!! Be careful, we have overwritten old data.

save('in/ImageDigitized/shape_Reeder2004_fig11_KRMformat.mat', 'x_sb', 'x_b', 'w_sb', 'w_b', 'z_sb', 'z_b'); % Data export

subplot(2,1,2); hold on;
plot(x_sb*1e3,z_sb(:,1)*1e3,'k')
plot(x_sb*1e3,z_sb(:,2)*1e3,'k')
text(x_sb(1)*1e3,mean(z_sb(1,:)),' \leftarrow head')
text(x_sb(end)*1e3,mean(z_sb(end,:)),' \leftarrow tail')
title('Shape #2 - Clay&Horne94 axis convention'); xlabel('mm'); ylabel('mm');

% % Limit the number of digits:
% Ndigit = 4; % 0.1mm
% x_b = limit_digit( x_b, Ndigit );
% w_b = limit_digit( w_b, Ndigit );
% z_b = limit_digit( z_b, Ndigit );
% x_sb = limit_digit( x_sb, Ndigit );
% w_sb = limit_digit( w_sb, Ndigit );
% z_sb = limit_digit( z_sb, Ndigit );




