% V1.00
% clear *; close all;
dbstop if error
addpath(genpath(pwd)) % add path for subfolder 'my_functions' and for input data (swimbladder *.stl)
cd(fileparts(mfilename('fullpath'))); % set the current Matlab folder = script directory

% f : fish
% sb : fish swimbladder

%% Inputs

% Soft swimbladder
g  = 0;%0.00129; % relative mass density
c0 = 1483;    % sound velocity (m/s) outside the cylinder
c1 = 0;%340;     % sound velocity (m/s) inside the cylinder
h = c1/c0;
f = [18 38 70 120 200] * 1e3; % frequency (Hz)
% f = [38 ] * 1e3; % frequency (Hz)

  m_anglen = -0;
    std_anglen = 15; % angle normal distributions standard deviation (�)
    
i_Flat2Plot = 1;
Lstyle = {'k-','k--','k:'}; % line style for each acoustic model

d = dir;
str = {'longueur', 'aplatissement', 'angle', 'courbure'};
[par,v] = listdlg('PromptString','Etude de sensibilit� � :',...
    'SelectionMode','single',...
    'ListString',str);
DlgChoice2 = questdlg('Utiliser des donn�es d�j� simul�es ?', ...
    'Chargement de donn�es existantes', ...
    'Oui','Non','Non');

switch DlgChoice2
    case 'Oui'
        computation = 0;
    case 'Non'
        computation = 1;
end


%% Geometric descriptors

% Build lists for each sensitivity study :
Lsb0 = 6.6 * 1e-2; % swimbladder length (m)
Lsb_list = {(4:0.2:9)*1e-2, Lsb0, Lsb0, Lsb0};
L_b_sb0 = 2 * 12.4; % swimbladder flattening (cylinder)
L_b_sb_list = {L_b_sb0, 2*(10:0.5:50), L_b_sb0, L_b_sb0};
L_b_sb_ell0 = 2 * 9.4; % swimbladder flattening (ellipsoid)
L_b_sb_ell_list = {L_b_sb_ell0, 2*(10:0.5:50), L_b_sb_ell0, []};
ti_2_list = {(0:0.5:50)',(0:0.5:50)',(0:0.5:50)',(0:0.5:50)'};
gamma_max0 = 6.8; % swimbladder curvature angle (�)
gamma_max_list = {gamma_max0, gamma_max0, gamma_max0, 0:0.1:12};

% Current sensitivity study :
Lsb        = Lsb_list{par};
L_b_sb     = L_b_sb_list{par};
L_b_sb_ell = L_b_sb_ell_list{par};
ti_2       = ti_2_list{par};
ti_1       = 90 - ti_2;
gamma_max  = gamma_max_list{par};


%% Computation
kL_all = (2*pi/c0) * Lsb.' * f; % wave number x swimbladder length

i_ti_normal = floor(length(ti_2)/2);

NkL = length(kL_all);
Nflat = length(L_b_sb);
Nti = length(ti_2);
Ngamma = length(gamma_max);
NanglePDF = 1000; % PDF : probability density function
Nmodel = 2; % set the value to 1,2 or 3 according to the number of models studied

% List of acoustic models for fluid cylinder of finite length
str_model = {'Cylindre droit', 'Cylindre courb�', 'Ellipsoide'};

if (computation && par < 4)
    % Concatenate results from all acoustic models in variable nsigma_allModels
    nsigma_allModels = zeros(NkL, Nflat, Nti, Nmodel); % absolute value WHEN TO DO THIS COMPUTATION??
    % reduced (ie. length-normalised) scattering function  :
    % dimension 1 : dependency on frequency  : kL
    % dimension 2 : dependency on flattening : ab
    % dimension 3 : dependency on tilt angle : ti
    % dimension 4 : type of acoustic model used
    
    % add soft straight cylinder
    nsigma_allModels(:,:,:,1) = cylinder_model_ka( kL_all, L_b_sb, ti_1, g, h, 7 );
    
    % add bent cylinder :
    nsigma_allModels(:,:,:,2) = bentcylinder_model( kL_all, L_b_sb, ti_2, g, h, gamma_max );
    
    % add ellipsoid :
    % Length sensitivity
    if par == 1 && Nmodel > 2
        for i_L = 1:length(Lsb)
            nf_PSM_OneLength = PSM_model( f, Lsb(i_L)/2, Lsb(i_L)/L_b_sb_ell, ti_1, c0 ); % dim: Nfreq x Ntif
            nsigma_PSM_OneLength = abs(nf_PSM_OneLength).^2;
            nsigma_allModels(i_L,:,:,3) = nsigma_PSM_OneLength;
        end
        
        % Flattening sensitivity
    elseif par == 2 && Nmodel > 2
        for i_L = 1:length(L_b_sb_ell)
            nf_PSM_OneLength = PSM_model( f, Lsb/2, Lsb/L_b_sb_ell(i_L), ti_1, c0 ); % dim: Nfreq x Ntif
            nsigma_PSM_OneLength = abs(nf_PSM_OneLength).^2;
            nsigma_allModels(1,i_L,:,3) = nsigma_PSM_OneLength;
        end
        
        % Angle sensitivity
    elseif par == 3 && Nmodel > 2
        nf_PSM_OneLength = PSM_model( f, Lsb/2, Lsb/L_b_sb_ell, ti_1, c0 ); % dim: Nfreq x Ntif
        nsigma_PSM_OneLength = abs(nf_PSM_OneLength).^2;
        nsigma_allModels(1,1,:,3) = nsigma_PSM_OneLength;
    end
    
    % add negative angle values :
    nsigma_allModels = cat(3, flip(nsigma_allModels(:,:,2:end,:),3), nsigma_allModels);
    
    % average over angle normal distribtion
    nsigma_allModels_PDF = zeros(NkL,Nflat,1,Nmodel);
  
    anglen = m_anglen + std_anglen*randn(NanglePDF,1); % angle normal distributions (�)
    
    if par ~= 3 % mean with angles PDF except for angle sensitivity study
        for i_kL = 1:NkL % loop on each ka value
            for i_model = 1:Nmodel % loop on each scattering model
                for i_flat = 1:Nflat
                    nsigma_tmp = nsigma_allModels(i_kL, i_flat, :, i_model); % modelled scattering function according to tilt angle : 1 x Nflat x Nti x 1
                    nsigma_tmp2 = reshape(nsigma_tmp, 1, 2*Nti-1); % rescalling : Nflat x Nti
                    % interpolation between angle distribution and modelled scattering function :
                    nsigma_PDF_tmp = interp1((-50:0.5:50)', nsigma_tmp2', anglen, 'nearest', 'extrap');
                    % average on each angle distribution :
                    nsigma_allModels_PDF(i_kL,i_flat,1,i_model) = mean(nsigma_PDF_tmp,1);
                end
            end
        end
    end
    
    if (par == 1 || par == 2)
        %         TS = 10*log10(nsigma_allModels .* repmat(Lsb',1,Nflat,1,Nmodel));
        TS = 10*log10(nsigma_allModels_PDF .* repmat(Lsb'.^2,length(f),Nflat,1,Nmodel));
    elseif par == 3 % angle sensitivity, no PDF
        nTS = 10*log10(nsigma_allModels);
    else
        nTS = 10*log10(nsigma_allModels_PDF);
    end

end

%% length sensitivity
if (par == 1)
    % save('TS_sensitivity_length_120kHz_abMean_gammaMean_L4_02_9cm.mat','TS');
    
    if computation
        figure;
        hold on;
        title(strcat('Longueurs :'));
        xlabel('Longueur vessie [cm]'); ylabel('TS [dB]');
        for i_model = 1:Nmodel
        plot(Lsb*1e2,TS(:,1,1,i_model),Lstyle{i_model});
        end
        legend(str_model,'Location','southeast');
        

        
    else
        
        TS38 = load('TS_sensitivity_length_38kHz_abMean_gammaMean_L4_02_9cm.mat');
        TS120 = load('TS_sensitivity_length_120kHz_abMean_gammaMean_L4_02_9cm.mat');
        TS38 = TS38.TS;
        TS120 = TS120.TS;
        
        figure;
        ax = subplot(2,1,1); hold on; ylim([-28 -20]); set(ax,'YGrid','on');
        title('f = 38kHz');
        xlabel('Longueur vessie [cm]'); ylabel('TS [dB]');
        plot(Lsb*1e2,TS38(:,1,1,1),'k-');
        plot(Lsb*1e2,TS38(:,1,1,2),'k--');
        plot(Lsb*1e2,TS38(:,1,1,3),'k:');
        plot(Lsb([9 18])*1e2,TS38([9 18],1,1,1),'k*');
        plot(Lsb([9 18])*1e2,TS38([9 18],1,1,2),'k*');
        plot(Lsb([9 18])*1e2,TS38([9 18],1,1,3),'k*');
        legend(str_model,'Location','northwest');
        ax = subplot(2,1,2); hold on; ylim([-26 -18]); set(ax,'YGrid','on');
        title('f = 120kHz');
        xlabel('Longueur vessie [cm]'); ylabel('TS [dB]');
        plot(Lsb*1e2,TS120(:,1,1,1),'k-');
        plot(Lsb*1e2,TS120(:,1,1,2),'k--');
        plot(Lsb*1e2,TS120(:,1,1,3),'k:');
        plot(Lsb([9 18])*1e2,TS120([9 18],1,1,1),'k*');
        plot(Lsb([9 18])*1e2,TS120([9 18],1,1,2),'k*');
        plot(Lsb([9 18])*1e2,TS120([9 18],1,1,3),'k*');
        legend(str_model,'Location','northwest');
    end
    
    %% flattening sensitivity
elseif (par == 2)
    % save('nTS_sensitivity_flattening_38kHz_LMean_gammaMean_ab10_05_25.mat','nTS');
    
    if computation
               figure;
        hold on;
        title(strcat('Aplatissement :'));
        xlabel('Aplatissement '); ylabel('TS [dB]');
        for i_model = 1:Nmodel
        plot(L_b_sb/2,TS(1,:,1,i_model),Lstyle{i_model});
        end
        legend(str_model,'Location','southeast');
%         figure; hold on;
%         xlabel('Aplatissement'); ylabel('nTS [dB]');
%         for i_model = 1:Nmodel
%             plot(L_b_sb/2,nTS(1,:,1,i_model),Lstyle{i_model});
% %             plot(L_b_sb([3 6])/2,nTS(1,[3 6],1,1),'k*');
%         end
%         legend(str_model,'Location','southeast');

    c=colormap(hsv(length(m_ta)));
               figure;
             hold on; grid on;
%         title(strcat('Aplatissement :'));
        xlabel('Aplatissement '); ylabel('TS [dB]');
        for i_freq = 1:length(f)
        plot(L_b_sb/2,TS(i_freq,:,1,2),'-','color',c(i_freq,:), 'LineWidth', 3);
        end
        legend('18 kHz' , '38 kHz', '70 kHz', '120 kHz', '200 kHz');
        
    else
        nTS38 = load('nTS_sensitivity_flattening_38kHz_LMean_gammaMean_ab105_05_14.mat');
        % nTS38 = load('nTS_sensitivity_flattening_38kHz_LMean_gammaMean_ab10_05_25.mat');
        nTS38 = nTS38.nTS;
        nTS120 = load('nTS_sensitivity_flattening_120kHz_LMean_gammaMean_ab105_05_14.mat');
        % nTS120 = load('nTS_sensitivity_flattening_120kHz_LMean_gammaMean_ab10_05_25.mat');
        nTS120 = nTS120.nTS;
        
        figure;
        ax = subplot(2,1,1); hold on; ylim([-15 -11]); set(ax,'YGrid','on');
        title('f = 38kHz');
        xlabel('Aplatissement'); ylabel('nTS [dB]');
        plot(L_b_sb/2,nTS38(1,:,1,1),'k-');
        plot(L_b_sb/2,nTS38(1,:,1,2),'k--');
        plot(L_b_sb/2,nTS38(1,:,1,3),'k:');
        plot(L_b_sb([3 6])/2,nTS38(1,[3 6],1,1),'k*');
        plot(L_b_sb([3 6])/2,nTS38(1,[3 6],1,2),'k*');
        plot(L_b_sb([3 6])/2,nTS38(1,[3 6],1,3),'k*');
        legend(str_model,'Location','northeast');
        ax = subplot(2,1,2); hold on; ylim([-12 -8]); set(ax,'YGrid','on');
        title('f = 120kHz');
        xlabel('Aplatissement'); ylabel('nTS [dB]');
        plot(L_b_sb/2,nTS120(1,:,1,1),'k-');
        plot(L_b_sb/2,nTS120(1,:,1,2),'k--');
        plot(L_b_sb/2,nTS120(1,:,1,3),'k:');
        plot(L_b_sb([3 6])/2,nTS120(1,[3 6],1,1),'k*');
        plot(L_b_sb([3 6])/2,nTS120(1,[3 6],1,2),'k*');
        plot(L_b_sb([3 6])/2,nTS120(1,[3 6],1,3),'k*');
        legend(str_model,'Location','northeast');
    end
    
    
    %% angle sensitivity
    
elseif (par == 3)
    
    ti_3 = [flip(-ti_2(2:end)) ; ti_2];
    if computation
        figure; hold on;
        xlabel('Angle'); ylabel('nTS [dB]');
        for i_model = 1:Nmodel
            plot(ti_3,squeeze(nTS(1,1,:,i_model)),Lstyle{i_model});
        end
        legend(str_model,'Location','southeast');
        
    else
    % save('nTS_sensitivity_angle_120kHz_LMean_gammaMean_abMean_Angle0_05_50.mat','nTS');
    nTS38 = load('nTS_sensitivity_angle_38kHz_LMean_gammaMean_abMean_Angle0_05_50.mat');
    nTS38 = nTS38.nTS;
    nTS120 = load('nTS_sensitivity_angle_120kHz_LMean_gammaMean_abMean_Angle0_05_50.mat');
    nTS120 = nTS120.nTS;
    
    figure
    subplot 211; hold on;
    plot(ti_3,[squeeze(nTS38(1,1,end:-1:2,1)); squeeze(nTS38(1,1,:,1))],'k-');
    plot(ti_3,[squeeze(nTS38(1,1,end:-1:2,2)); squeeze(nTS38(1,1,:,2))],'k--');
    plot(ti_3,[squeeze(nTS38(1,1,end:-1:2,3)); squeeze(nTS38(1,1,:,3))],'k:');
    title('f = 38kHz'); xlabel('Angle d''incidence [�]'); ylabel('nTS [dB]'); ylim([-50 0]);
    legend(str_model,'Location','northeast');
    subplot 212; hold on;
    plot(ti_3,[squeeze(nTS120(1,1,end:-1:2,1)); squeeze(nTS120(1,1,:,1))],'k-');
    plot(ti_3,[squeeze(nTS120(1,1,end:-1:2,2)); squeeze(nTS120(1,1,:,2))],'k--');
    plot(ti_3,[squeeze(nTS120(1,1,end:-1:2,3)); squeeze(nTS120(1,1,:,3))],'k:');
    title('f = 120kHz'); xlabel('Angle d''incidence [�]'); ylabel('nTS [dB]'); ylim([-50 0]);
    legend(str_model,'Location','northeast');
    end
    
    %% curvature sensitivity
elseif (par == 4)
    % this sensitivity study uses only normal incidence
    
    
    nsigma_allModels = zeros(NkL, Nflat, Nti, Ngamma);
    for i_gamma = 1:Ngamma
        nsigma_allModels(:,:,:,i_gamma) = bentcylinder_model( kL_all, L_b_sb, ti_2, g, h, gamma_max(i_gamma) );
    end
    
        % add negative angle values :
    nsigma_allModels = cat(3, flip(nsigma_allModels(:,:,2:end,:),3), nsigma_allModels);
    
    % average over angle normal distribtion
    nsigma_allModels_PDF = zeros(NkL,Ngamma);
    anglen = m_anglen + std_anglen*randn(NanglePDF,1); % angle normal distributions (�)
    
    for i_kL = 1:NkL % loop on each ka value
           
                for i_gamma = 1:Ngamma
                    nsigma_tmp = nsigma_allModels(i_kL, 1, :, i_gamma); % modelled scattering function according to tilt angle : 1 x Nflat x Nti x 1
                    nsigma_tmp2 = reshape(nsigma_tmp, 1, 2*Nti-1); % rescalling : Nflat x Nti
                    % interpolation between angle distribution and modelled scattering function :
                    nsigma_PDF_tmp = interp1((-50:0.5:50)', nsigma_tmp2', anglen, 'nearest', 'extrap');
                    % average on each angle distribution :
                    nsigma_allModels_PDF(i_kL,i_gamma) = mean(nsigma_PDF_tmp,1);
                end
   
    end
        
    nTS = 10*log10(nsigma_allModels_PDF);
    
     figure; hold on;
        xlabel('Courbure [�]'); ylabel('nTS [dB]');
    plot(gamma_max,squeeze(nTS(1,:)),'k-');  % 38kHz
    plot(gamma_max,squeeze(nTS(2,:)),'k--'); % 70kHz
    plot(gamma_max,squeeze(nTS(3,:)),'k-.'); % 120kHz
    plot(gamma_max,squeeze(nTS(4,:)),'k:');  % 200kHz
%             plot(L_b_sb([3 6])/2,nTS(1,[3 6],1,1),'k*');

    set(gca,'YGrid','on');
    legend('f = 38kHz','f = 70kHz','f = 120kHz','f = 200kHz','Location','northeast');
    
%     figure; hold on;
%     xlabel('Courbure [�]'); ylabel('nTS [dB]');
%     plot(gamma_max,squeeze(nTS(1,1,101,:)),'k-');  % 38kHz
%     plot(gamma_max,squeeze(nTS(2,1,101,:)),'k--'); % 70kHz
%     plot(gamma_max,squeeze(nTS(3,1,101,:)),'k-.'); % 120kHz
%     plot(gamma_max,squeeze(nTS(4,1,101,:)),'k:');  % 200kHz
%     plot(gamma_max([33 98]),squeeze(nTS(1,1,101,[33 98])),'k*');
%     plot(gamma_max([33 98]),squeeze(nTS(2,1,101,[33 98])),'k*');
%     plot(gamma_max([33 98]),squeeze(nTS(3,1,101,[33 98])),'k*');
%     plot(gamma_max([33 98]),squeeze(nTS(4,1,101,[33 98])),'k*');
%     set(gca,'YGrid','on');
%     legend('f = 38kHz','f = 70kHz','f = 120kHz','f = 200kHz','Location','northeast');
%     
%     figure;
%     subplot 211
%     hold on; % 38kHz
%     i_f = 1;
%     plot(ti_2,squeeze(nTS(i_f,1,:,3)),'k-');
%     plot(ti_2,squeeze(nTS(i_f,1,:,9)),'k--');
%     plot(ti_2,squeeze(nTS(i_f,1,:,17)),'k-.');
%     plot(ti_2,squeeze(nTS(i_f,1,:,25)),'k:');
%     title('f = 38kHz'); xlabel('Angle d''incidence [�]'); ylabel('nTS [dB]');
%     legend(strcat('\gamma = ',num2str(gamma_max(3)),'�'), ...
%         strcat('\gamma = ',num2str(gamma_max(9)), '�'), ...
%         strcat('\gamma = ',num2str(gamma_max(17)),'�'), ...
%         strcat('\gamma = ',num2str(gamma_max(25)),'�'),'Location','northeast');
%     
%     subplot 212; hold on; % 120kHz
%     i_f = 3;
%     plot(ti_2,squeeze(nTS(i_f,1,:,3)),'k-');
%     plot(ti_2,squeeze(nTS(i_f,1,:,9)),'k--');
%     plot(ti_2,squeeze(nTS(i_f,1,:,17)),'k-.');
%     plot(ti_2,squeeze(nTS(i_f,1,:,25)),'k:');
%     title('f = 120kHz'); xlabel('Angle d''incidence [�]'); ylabel('nTS [dB]');
%     legend(strcat('\gamma = ',num2str(gamma_max(3)),'�'), ...
%         strcat('\gamma = ',num2str(gamma_max(9)),'�'), ...
%         strcat('\gamma = ',num2str(gamma_max(17)),'�'), ...
%         strcat('\gamma = ',num2str(gamma_max(25)),'�'),'Location','northeast');
    
end
