% V1.00
clear *; %close all;
dbstop if error
addpath(genpath(pwd)) % add path for subfolder 'my_functions' and for input data (swimbladder *.stl)
cd(fileparts(mfilename('fullpath'))); % set the current Matlab folder = script directory

krm_file_path = 'in/InputDataKRM/'; % path of input geometry files for KRM script

%% Parameters
% Environment
g  = 0.00129; % relative mass density
c0 = 1483;    % sound speed (m/s) outside the cylinder
c1 = 340;     % sound speed (m/s) inside the cylinder
h  = c1/c0;   % relative sound speed
% Sound wave
freqs = (18:2:120)*1e3; % echosounder frequency (Hz)
Nfreq   = length(freqs);
ti_list = 40:2:140; % angle of incidence (�)

%% Load of fish dataset
% Dataset : Tomofish sardines from La Rochelle
fishDataSet = importdata('AqLR2_Sardine_caracteristics.txt');
fishDataSet = fishDataSet.data.';
fish_index = fishDataSet(1,:);
Ln = fishDataSet(3,:)*1e-2; % bladder length (m)
as = fishDataSet(8,:)*1e-2; % bladder radius (m) (Constant area)
as2= fishDataSet(9,:)*1e-2; % bladder radius (m) (Constant area)

bladder_tilt = fishDataSet(7,:); % degree

Ln_fish_cm = fishDataSet(2,:); % fish length (cm)
Nfish = size(fishDataSet,2); % number of fish in dataset
Lb_distrib = Ln ./ as; % 2*flattening
Lb_distrib2 = Ln ./ as2; 

%% nTS computation for Tomofish data
% for each fish at different frequencies

Nfish=8;

Nmodel = 3; % number of scattering models used
nTS_dataset = cell(Nfish,Nmodel); % nTS for each fish according to frequency and tilt angle
fish_legend = cell(Nfish,1);
kL_distrib = zeros(Nfish,Nfreq);



for i_fish = 1:Nfish % loop on fishes
% Legend writing :
i_fish_str = num2str(fish_index(i_fish));
Lb_fish_str = num2str(round(Lb_distrib(i_fish)/2*10)/10); % 1 decimal
Lb_fish_str2 = num2str(round(Lb_distrib2(i_fish)/2*10)/10); % 1 decimal
Lb_fish_str3 = num2str(round(Ln_fish_cm(i_fish)*100)/100); % 1 decimal
fish_legend{i_fish,1} = strcat('Sardine n�',i_fish_str,' - L/acyl=',Lb_fish_str,' - L/aell=',Lb_fish_str2,' - Lf=',Lb_fish_str3);

% Model 1 : KRM model (for one frequency) :
TS_KRM_mean = zeros(1, Nfish);

% krm_file = strcat('2014-11-13_AqLR2_Sardine',num2str(fish_index(i_fish)),'_KRMgeometry');
% sigma_KRM_OneFish = KRM_model_bladderOnly( freqs, ti_list, c0, c1, g, krm_file_path, krm_file ); % dim: Nfreq x 1 x length(ti_list)
% nTS_KRM_OneFish = 10*log10(sigma_KRM_OneFish ./ Ln(i_fish)^2); % dim: Nfreq x 1 x length(ti_list)
% nTS_KRM_OneFish = squeeze(nTS_KRM_OneFish); % dim : Nfreq x length(ti_list)
% nTS_dataset{i_fish, 1} = nTS_KRM_OneFish;

% Cylinder models
kL_distrib(i_fish,:) = (2*pi/c0)*Ln(i_fish)*freqs; 

% Model 2 : fluid cylinder of finite length without end effects (modal series Stanton88)
% One fish : all frequencies x 1 flattening x all angles
% nf_St88_OneFish = cylinder_model_ka( kL_distrib(i_fish,:), Lb_distrib(i_fish), ti_list, g, h, 1 ); % dim: Nfreq x 1 x length(ti_list)
% nTS_St88_OneFish = 20*log10(abs(nf_St88_OneFish));
% nTS_St88_OneFish = squeeze(nTS_St88_OneFish); % dim : Nfreq x length(ti_list)
% nTS_dataset{i_fish, 2} = nTS_St88_OneFish;

% % Model 3 : fluid cylinder of finite length with end effects (modal series Stanton88 + f_end Ye1997)
% nf_fend_OneFish = cylinder_model_ka( kL_distrib(i_fish,:), Lb_distrib(i_fish), ti_list, g, h, 4 ); % dim: Nfreq x 1 x length(ti_list)
% nTS_fend_OneFish = 20*log10(abs(nf_St88_OneFish + nf_fend_OneFish));
% nTS_fend_OneFish = squeeze(nTS_fend_OneFish); % dim : Nfreq x length(ti_list)
% nTS_dataset{i_fish, 2} = nTS_fend_OneFish;

% % Model 3 : PSMS model
% nf_PSM_OneFish = PSM_model( freqs, Ln(i_fish)/2, as2(i_fish), ti_list, c0 ); % dim: Nfreq x 1 x length(ti_list)
% nTS_PSM_OneFish = 20*log10(abs( nf_PSM_OneFish));
% nTS_PSM_OneFish = squeeze(nTS_PSM_OneFish); % dim : Nfreq x length(ti_list)
% nTS_dataset{i_fish, 3} = nTS_PSM_OneFish;


end

% % save('in/TS_model3','nTS_dataset');



load('in/TS_model3');

%% Tilt angle normal distribution

% tilt distribution parameters :
Nang = 2000; % number of tilt angles
% Values from Olsen1971 :
m_ta = 90 + 4.4; % tilt angle mean (�)
m_ta = 90 + 30; % tilt angle mean (�)
std_ta = 15; % tilt angle std (�)
ota_fsb = -7; % offset tilt angle of the swimbladder relative to the fish body (�)
anglen = m_ta + std_ta * randn(1, Nang); % probality density distribution (PDF)
% Truncate at 95% the PDF :
anglen = anglen(anglen >=  m_ta - 3*std_ta & anglen <= m_ta + 3*std_ta);

% figure
% nbins = 100;
% histogram(anglen,nbins,'Normalization','probability'); 
% title('Tilt angle distribution'); xlabel('Tilt angle (�)'); ylabel('relative number of observations')

%% Plot of ka dependancy and theta dependancy for each fish

myColorOrder = [
    0    0.4470    0.7410;
    0.8500    0.3250    0.0980;
    0.9290    0.6940    0.1250;
    0.4940    0.1840    0.5560;
    0.4660    0.6740    0.1880;
    0.3010    0.7450    0.9330;
    0.6350    0.0780    0.1840;
    0    0    0];
set(groot, 'defaultAxesColorOrder', myColorOrder);

% Theta dependancy
i_f38kHz = find(freqs == 120000);
mark_ = {':','-','--'}; % LineStyle for KRM, cylinder

figure; 
for i_model = 1:Nmodel
hold on; grid on;
for i_fish = 1:Nfish % loop on fishes
nTS_tmp = nTS_dataset{i_fish, i_model};
if i_model == 1
plot(ti_list, nTS_tmp(i_f38kHz,:),'LineStyle',mark_{i_model})
else
plot(ti_list+bladder_tilt(i_fish), nTS_tmp(i_f38kHz,:),'LineStyle',mark_{i_model})
end
end
end
title(strcat('nTS versus tilt angle - Tomofish sardines - f=',num2str(freqs(i_f38kHz)*1e-3),'kHz'));
xlabel('Tilt angle [�]'); ylabel('length-normalised TS [dB]');
legend(fish_legend);

% ka dependancy
nTS_tiltAveraged_dataset = zeros(Nfish,Nmodel,Nfreq);
for i_fish = 1:Nfish % loop on fishes
for i_model = 1:Nmodel
for i_freq = 1:Nfreq
    if i_model > 1
        anglen_tmp = anglen + bladder_tilt(i_fish);
    else
        anglen_tmp = anglen;
    end
    nTS_tmp = nTS_dataset{i_fish, i_model};
    nTS_anglen_dataset_tmp = interp1(ti_list, nTS_tmp(i_freq,:), anglen_tmp, 'nearest','extrap');
    nTS_tiltAveraged_dataset_tmp = 10*log10(mean(10.^(nTS_anglen_dataset_tmp/10)));
    nTS_tiltAveraged_dataset(i_fish,i_model,i_freq) = nTS_tiltAveraged_dataset_tmp;
end
end
end

c=colormap(hsv(Nmodel));
figure; 
for i_model = 1:Nmodel
hold on; grid on;
for i_fish = 1:Nfish % loop on fishes
nTS_tmp = nTS_dataset{i_fish, i_model};
h(i_model)=plot(kL_distrib(i_fish,:)/2, squeeze(nTS_tiltAveraged_dataset(i_fish,i_model,:)),'LineStyle',mark_{i_model},'color',c(i_model,:),'LineWidth',3)
end
end
% title(strcat('orientation-averaged nTS versus ka - Tomofish sardines - Orientation distribution (m=-2.6�, std=16�)'));
xlabel('ka with a the semi-length'); ylabel('orientation-averaged length-normalised TS [dB]');
% legend(fish_legend);
legend(h(1:3),{'KRM','Cylinder modal series','Ellipsoid modal series'});
axis([0 20 -25 -15]);

steering=(pi/180)*[-42.20577 -35.92659 -30.43624 -25.51571 -21.02889 -16.88417 -13.01624 -9.376566 -5.927816 -2.640473 0.495642 3.601663 6.827438 10.21141 13.78197 17.57491 21.63671 26.02967 30.84084 36.19885 42.30876];
frequency=1000*[   71.9430   76.4490   80.9560   85.4620   89.9690   94.4760   98.9820  103.4890  107.9950  112.5020  118.0570  114.7550  110.2490  105.7420  101.2350   96.7290   92.2220   87.7160   83.2090 78.7020  74.1960];
Nbeam=length(steering);

BW=3500*ones(1,length(steering));

Nang=length(anglen);

yaw=(360*rand(1,Nang));
% yaw=(90*ones(1,Nang));
% yaw(1:round(Nang/2))=(normrnd(-30,std_ta,1,round(Nang/2)));
% yaw(1+round(Nang/2):Nang)=(normrnd(30,std_ta,1,Nang-round(Nang/2)));

roll=0*ones(1,Nang);

tilt=anglen-90;



% ka dependancy
nTS_tiltAveraged_dataset = zeros(Nfish,Nmodel,Nbeam);

for i_beam = 1:Nbeam
    if i_model > 1
        tilt2 = tilt + bladder_tilt(i_fish);
    else
        tilt2 = tilt;
    end
    
    apparenttilt=zeros(length(tilt),1);
    %     apparentyaw=zeros(length(yaw),1);
    apparentroll=zeros(length(roll),1);
    for i=1:Nang
        Rfish=CreateRotationMatrix(yaw(i)*(pi/180),tilt2(i)*(pi/180),roll(i)*(pi/180));
        Rbeam=CreateRotationMatrix(0*pi/180,0,- steering(i_beam));
        Vbeam=Rbeam*[0;0;1;];
        Vfish=-Rfish*Vbeam;
        %                 apparenttilt1(i)=atan2(Vfish(2),Vfish(1))*180/pi;
        %                 apparentroll1(i)=sign(sin(atan2(Vfish(2),Vfish(1))))*atan2((Vfish(1)^2+Vfish(2)^2)^0.5,Vfish(3))*180/pi;
        % %                             apparentroll(i)=atan2((Vfish(1)^2+Vfish(2)^2)^0.5,Vfish(3))*180/pi;
        apparenttilt(i)=(acos(dot(Vbeam,Rfish*[1;0;0;])))*180/pi;
        apparentroll(i)=acos(dot(Vbeam,cross(Rfish*[1;0;0;],[0;0;1])))*180/pi;
    end
    
    % Incidence angles (degrees)
    apparentphi(i_beam,:) = mod(apparenttilt,180);              % Incidence angles (degrees)
    apparentpsi(i_beam,:)=mod(apparentroll,180);
    
  
    
    i_freq= find(abs(freqs-frequency(i_beam))<=BW(i_beam)/2);
    for i_fish = 1:Nfish % loop on fishes
        for i_model = 1:Nmodel
              nTS_tmp = nTS_dataset{i_fish, i_model};
            nTS_anglen_dataset_tmp = interp1(ti_list, nTS_tmp(i_freq(1),:), apparentphi(i_beam,:), 'nearest','extrap');
            nTS_tiltAveraged_dataset_tmp = 10*log10(mean(10.^(nTS_anglen_dataset_tmp/10)));
            nTS_tiltAveraged_dataset(i_fish,i_model,i_beam) = nTS_tiltAveraged_dataset_tmp;
        end
    end
end

c=colormap(hsv(Nfish));
figure; 
for i_model = 1:Nmodel
hold on; grid on;
for i_fish = 1:Nfish % loop on fishes
nTS_tmp = nTS_dataset{i_fish, i_model};
plot(steering*180/pi, 20*log10(Ln(i_fish))+squeeze(nTS_tiltAveraged_dataset(i_fish,i_model,:)),'LineStyle',mark_{i_model},'color',c(i_fish,:),'LineWidth',3)
end
end
title(strcat('tilt-averaged nTS versus ka - Tomofish sardines - Tilt distribution (m=-2.6�, std=16�)'));
xlabel('ka with a the semi-length'); ylabel('tilt-averaged length-normalised TS [dB]');
legend(fish_legend);

 frequency=[18 38 70 120]*1e3;
BW=3*1e3*ones(length(frequency),1);
steering=0*ones(length(frequency),1);
Nbeam=length(steering);

Nang=length(anglen);

yaw=(360*rand(1,Nang));
% yaw=(0*ones(1,Nang));
% yaw(1:round(Nang/2))=(normrnd(-30,std_ta,1,round(Nang/2)));
% yaw(1+round(Nang/2):Nang)=(normrnd(30,std_ta,1,Nang-round(Nang/2)));

roll=0*ones(1,Nang);

tilt=anglen;


% ka dependancy
nTS_tiltAveraged_dataset = zeros(Nfish,Nmodel,Nbeam);

for i_beam = 1:Nbeam
    if i_model > 1
        tilt = tilt + bladder_tilt(i_fish);
    else
        tilt = tilt;
    end
    
    apparenttilt=zeros(length(tilt),1);
    %     apparentyaw=zeros(length(yaw),1);
    apparentroll=zeros(length(roll),1);
    for i=1:Nang
        Rfish=CreateRotationMatrix(yaw(i)*(pi/180),(tilt(i)-90)*(pi/180),roll(i)*(pi/180));
        Rbeam=CreateRotationMatrix(0*pi/180,0,- steering(i_beam));
        Vbeam=Rbeam*[0;0;1;];
        Vfish=-Rfish*Vbeam;
        %                 apparenttilt1(i)=atan2(Vfish(2),Vfish(1))*180/pi;
        %                 apparentroll1(i)=sign(sin(atan2(Vfish(2),Vfish(1))))*atan2((Vfish(1)^2+Vfish(2)^2)^0.5,Vfish(3))*180/pi;
        % %                             apparentroll(i)=atan2((Vfish(1)^2+Vfish(2)^2)^0.5,Vfish(3))*180/pi;
        apparenttilt(i)=(acos(dot(Vbeam,Rfish*[1;0;0;])))*180/pi;
        apparentroll(i)=acos(dot(Vbeam,cross(Rfish*[1;0;0;],[0;0;1])))*180/pi;
    end
    
    % Incidence angles (degrees)
    apparentphi(i_beam,:) = mod(apparenttilt,180);              % Incidence angles (degrees)
    apparentpsi(i_beam,:)=mod(apparentroll,180);
    
   
    
    i_freq= find(abs(freqs-frequency(i_beam))<=BW(i_beam)/2);
    for i_fish = 1:Nfish % loop on fishes
        for i_model = 1:Nmodel
             nTS_tmp = nTS_dataset{i_fish, i_model};
            nTS_anglen_dataset_tmp = interp1(ti_list, nTS_tmp(i_freq(1),:), apparentphi(i_beam,:), 'nearest','extrap');
            nTS_tiltAveraged_dataset_tmp = 10*log10(mean(10.^(nTS_anglen_dataset_tmp/10)));
            nTS_tiltAveraged_dataset(i_fish,i_model,i_beam) = nTS_tiltAveraged_dataset_tmp;
        end
    end
end

c=colormap(hsv(Nfish));
figure; 
for i_model = 1:Nmodel
hold on; grid on;
for i_fish = 1:Nfish % loop on fishes
nTS_tmp = nTS_dataset{i_fish, i_model};
plot(frequency, 20*log10(Ln(i_fish))+squeeze(nTS_tiltAveraged_dataset(i_fish,i_model,:)),'LineStyle',mark_{i_model},'color',c(i_fish,:),'LineWidth',3)
end
end
title(strcat('tilt-averaged nTS versus ka - Tomofish sardines - Tilt distribution (m=-2.6�, std=16�)'));
xlabel('ka with a the semi-length'); ylabel('tilt-averaged length-normalised TS [dB]');
legend(fish_legend);


%% %% TS law with Do&Surti1990 method

Nfish_TSlaw = 15; % number of fishes
freq_TSlaw = [18, 38, 70, 120, 200]*1e3; % frequencies (Hz)
Nfreq_TSlaw = length(freq_TSlaw); % number of frequencies
anglen_TSlaw = anglen; % we use previous tilt angle distribution (�)
kL_TSlaw = zeros(Nfish_TSlaw, Nfreq_TSlaw);
TSmean_TSlaw = zeros(Nfish_TSlaw, Nfreq_TSlaw);
b20_St88 = zeros(Nfreq_TSlaw,1);

% fish length distribution parameters :
m_Lfish = 16*1e-2; % tilt angle mean (�)
std_Lfish = 1*1e-2; % tilt angle std (�)
Lfish_TSlaw = m_Lfish + std_Lfish * randn(1, Nfish_TSlaw); % probality density distribution (PDF)
% Truncate at 95% the PDF :
% Lfish_TSlaw = Lfish_TSlaw(Lfish_TSlaw >=  m_Lfish - 3*std_Lfish & Lfish_TSlaw <= m_Lfish + 3*std_Lfish);
Lbladder_TSlaw = 0.3 * Lfish_TSlaw;

% bladder flattening distribution parameters :
m_abfish = 10; % tilt angle mean (�)
std_abfish = 0.2; % tilt angle std (�)
abfish_TSlaw = m_abfish + std_abfish * randn(1, Nfish_TSlaw); % probality density distribution (PDF)
% Truncate at 95% the PDF :
% abfish_TSlaw = abfish_TSlaw(abfish_TSlaw >=  m_abfish - 3*std_abfish & abfish_TSlaw <= m_abfish + 3*std_abfish);

% mean TS computation
for i_fish = 1:Nfish_TSlaw
    kL_TSlaw(i_fish,:) = 2*pi*freq_TSlaw*Lbladder_TSlaw(i_fish)/c0;
    nf_TSlaw = cylinder_model_ka( kL_TSlaw(i_fish,:), 2*abfish_TSlaw(i_fish), anglen_TSlaw, g, h, 1 ); % dim: Nfreq x 1 x length(anglen_TSlaw)
    nf_mean_TSlaw = mean(nf_TSlaw, 3);% mean according to tilt angle distribution
    nf_mean_TSlaw = squeeze(nf_mean_TSlaw).';
    TSmean_TSlaw(i_fish,:) = 20 * log10(abs(nf_mean_TSlaw * Lbladder_TSlaw(i_fish)));
end

disp('Do&Surti b20 computation method :');
for i_freq = 1:Nfreq_TSlaw
b20_St88(i_freq) = polyfit(zeros(1,Nfish_TSlaw),TSmean_TSlaw(:,i_freq).'-20*log10(Lfish_TSlaw * 1e2),0);

str2 = sprintf('Frequency %2.0f kHz - b20 = %2.1f [dB]', freq_TSlaw(i_freq)*1e-3, b20_St88(i_freq));
disp(str2);
end

figure
plot(freq_TSlaw*1e-3, b20_St88,'x--'); grid on;
title('b_{20} - Do&Surti method - Stanton acoustic model'); xlabel('Frequency (kHz)'); ylabel('b_{20} [dB]');





