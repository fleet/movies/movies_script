clear *

c0 = 1483;
c1 = 1557;
g = 1.043;
dka = 1e-2;
ka0 = dka:dka:10;
a = 1e-2; % sphere radius (m)
aplatissement=1;
a0=a*(1/aplatissement)^(2/3);
f0 = c0*ka0 ./ (2*pi*a0);
ti0 = [0 10];
SphereType = 'fluid_sphere';

sigma_sphere = sphere_model_ka( f0, a0, ti0, g, c0, c1, SphereType );
nsigma_sphere = sigma_sphere/(pi*a0^2);
nTS_sphere = 10*log(nsigma_sphere);
figure; semilogx(ka0,nTS_sphere(:,1,1)); axis tight; grid on;
figure; semilogx(f0/1e3,10*log10(sigma_sphere(:,1,1))); axis tight;  grid on;
figure; plot(f0/1e3,10*log10(sigma_sphere(:,1,1))); axis tight;  grid on;