function GenerateBentCylinderForKRM(P, L, R, gamma_max, beta, dL, ExptPath, ExptFileName)

% (X,Y,Z) is a direct frame
% - the cylinder is bented in (Z,X) plan
% - if the cylinder is straight, its revolution axis is X-axis
% - you can add a tilt angle in (Z,X) plan
% - the angle convention is : angle value is positive from Z to X.

% the parameter you choose are :
% - P  : cylinder center position (m) in (X,Y,Z) frame
% - L  : bent cylinder length (m) > 0
% - R  : cylinder radius (m) > 0
% - gamma_max : cylinder maximum curvature angle (�) > 0
% - beta : cylinder tilt angle (�)
% - dL : elementary cylinder slice length (m)
% - ExptPath : path where you want to export the shape geometry file
% - ExptFileName : shape geometry file name

% Export Format : KRM convention :
% - 

% Check P variable format :
if length(P) ~= 3
    error('P must be a vector of dimension 3, expressing the bent cylinder center coordinates in a (X,Y,Z) direct frame.')
end
if size(P,1) ~= 3 % if P is a row vector, change it in a column vector
    P = reshape(P,3,1);
end

gamma_max = gamma_max * pi/180; % degrees to radians
beta_str = num2str(beta);
beta = beta * pi/180; % degrees to radians


rc = L/(2*gamma_max); % curvature radius (m), formula 24 from Stanton1989a
Ns = ceil(L/dL); % number of elementary slices
gamma_s = linspace(-gamma_max,gamma_max,Ns); % slice angle vector (rad)

% slice center coordinates in (X,Y,Z) frame (bent cylinder center is
% 0,0,0):
x_s = rc * sin(gamma_s); % row vector (m)
z_s = -(rc - x_s./tan(gamma_s)); % line vector (m)
z_s(isnan(z_s)) = 0; % debug if gamma_s=0
y_s = zeros(size(gamma_s)); % row vector (m)
Xs = [x_s; y_s; z_s]; % slice center coordinates

% if there is a tilt angle
Rot = [cos(beta) 0 sin(beta); 0 1 0; -sin(beta) 0 cos(beta)]; % rotation matrix
Xs_tilt = Rot*Xs;



% figure; grid on; hold on; axis equal;
% set(gca,'YDir','reverse')
% plot(Xs(1,:),Xs(3,:),'b') % (X,Z) plan view, before tilt
% plot(Xs_tilt(1,:),Xs_tilt(3,:),'r') % (X,Z) plan view, after tilt
% title(strcat('Bent cylinder view before and after tilt(',beta_str,'�)'));
% xlabel('x-axis (m)'); ylabel('z-axis (m)');
% legend('before tilt','after tilt')

% transfer from origin (X,Y,Z) frame to KRM frame :
Xs_KRMframe = Tang2009coord2KRMcoord(Xs_tilt); 
P_KRMframe = Tang2009coord2KRMcoord(P);

% Bent cylinder center is now P :
Xs_final_KRMframe = Xs_KRMframe + repmat(P_KRMframe,1,Ns);


% Conversion for KRM view :
x_sb = Xs_final_KRMframe(1,:)';
x_b = x_sb;
w_sb = 2*R*ones(Ns,1);
w_b = w_sb;
z_sb = repmat(Xs_final_KRMframe(3,:),2,1)'+repmat([-R R],Ns,1);
z_b = z_sb;

% PlotKRMSliceView( x_b, w_b, z_b, x_sb, w_sb, z_sb )

% Export the geometry :
save(strcat(ExptPath,ExptFileName,'.mat'), 'x_sb', 'x_b', 'w_sb', 'w_b', 'z_sb', 'z_b'); % Data export

end
