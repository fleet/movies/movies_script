function myDataset = GenerateDataset( Nf, param_Lf, param_Lf_Lsb, param_sbFlat, param_sbTilt, param_sbGamma )
% generate a dataset of fish with parameters (fish length, bladder length,
% bladder flattening) with random values following probality density distribution

% INPUT :
%   Nf : number of fishes in the dataset
%   param_Lf : distribution parameters for fish length : [mean, std]
%     - unit must be meter (m) for fish length
%   param_Lf_Lsb : parameters for fish bladder / fish length ratio : [mean, std]
%   param_sbFlat : parameters for swimbladder flattening : [mean, std]

% INPUT :
%   myDataset array :
%     - each row is one fish
%     - 1st column : fish index
%     - 2nd column : fish length (m)
%     - 3rd column : bladder length (m)
%     - 4th column : bladder flattening

validateattributes(Nf,{'numeric'},{'nonempty', 'nonnegative', 'numel',1});
validateattributes(param_Lf,{'numeric'},{'nonempty', 'nonnegative'});
validateattributes(param_Lf_Lsb,{'numeric'},{'nonempty', 'nonnegative'});
validateattributes(param_sbFlat,{'numeric'},{'nonempty', 'nonnegative'});
validateattributes(param_sbTilt,{'numeric'},{'nonempty'});
validateattributes(param_sbGamma,{'numeric'},{'nonempty'});

% normal distributions (PDF) truncated at 95% for fish length, 
% bladder length and bladder flattening 

% fish length distribution parameters :
if length(param_Lf) == 1 % constant value
    Lf = param_Lf(1) * ones(Nf, 1);
else % normal distribution
    m_Lf = param_Lf(1); % fish length mean (m)
    std_Lf = param_Lf(2); % fish length std (m)
    Lf = m_Lf + std_Lf * randn(Nf, 1); % probality density distribution (PDF)
end

% bladder length/fish length ratio distribution parameters :
if length(param_Lf_Lsb) == 1 % constant value
    Lf_Lsb = param_Lf_Lsb(1) * ones(Nf, 1);
else % normal distribution
    m_Lf_Lsb = param_Lf_Lsb(1); % fish length mean (m)
    std_Lf_Lsb = param_Lf_Lsb(2); % fish length std (m)
    Lf_Lsb = m_Lf_Lsb + std_Lf_Lsb * randn(Nf, 1); % probality density distribution (PDF)
end

% bladder flattening distribution parameters :
if length(param_sbFlat) == 1 % constant value
    sbFlat = param_sbFlat(1) * ones(Nf, 1);
else % normal distribution
    m_sbFlat = param_sbFlat(1); % fish length mean (m)
    std_sbFlat = param_sbFlat(2); % fish length std (m)
    sbFlat = m_sbFlat + std_sbFlat * randn(Nf, 1); % probality density distribution (PDF)
end

% bladder tilt distribution parameters :
if length(param_sbTilt) == 1 % constant value
    sbTilt = param_sbTilt(1) * ones(Nf, 1);
else % normal distribution
    m_sbTilt = param_sbTilt(1); % fish length mean (m)
    std_sbTilt = param_sbTilt(2); % fish length std (m)
    sbTilt = m_sbTilt + std_sbTilt * randn(Nf, 1); % probality density distribution (PDF)
end

% bladder curvature distribution parameters :
if length(param_sbGamma) == 1 % constant value
    sbGamma = param_sbGamma(1) * ones(Nf, 1);
else % normal distribution
    m_sbGamma = param_sbGamma(1); % fish length mean (m)
    std_sbGamma = param_sbGamma(2); % fish length std (m)
    sbGamma = m_sbGamma + std_sbGamma * randn(Nf, 1); % probality density distribution (PDF)
end

% Truncate at 95% the PDF :
cond1 = (Lf >=  m_Lf - 3*std_Lf & Lf <= m_Lf + 3*std_Lf);
cond2 = (Lf_Lsb >=  m_Lf_Lsb - 3*std_Lf_Lsb & Lf_Lsb <= m_Lf_Lsb + 3*std_Lf_Lsb);
cond3 = (sbFlat >=  m_sbFlat - 3*std_sbFlat & sbFlat <= m_sbFlat + 3*std_sbFlat);
cond4 = (sbTilt >=  m_sbTilt - 3*std_sbTilt & sbTilt <= m_sbTilt + 3*std_sbTilt);
cond5 = (sbGamma >=  m_sbGamma - 3*std_sbGamma & sbGamma <= m_sbGamma + 3*std_sbGamma);
all_cond = (cond1 & cond2 & cond3 & cond4 & cond5);
Lf = Lf(all_cond);
Lf_Lsb = Lf_Lsb(all_cond);
sbFlat = sbFlat(all_cond);
sbTilt = sbTilt(all_cond);
sbGamma = sbGamma(all_cond);

Lsb = Lf ./ Lf_Lsb; % bladder length (m)

Nf_final = size(Lsb, 1);

myDataset = [(1:Nf_final)', Lf, Lsb, sbFlat, sbTilt, sbGamma]; % fish index, fish length, bladder length, bladder flattening, bladder tilt


end

