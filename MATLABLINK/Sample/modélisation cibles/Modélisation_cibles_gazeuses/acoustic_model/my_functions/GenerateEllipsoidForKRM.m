function GenerateEllipsoidForKRM(P, a, b, beta, dL, ExptPath, ExptFileName)

% Check P variable format :
if length(P) ~= 3
    error('P must be a vector of dimension 3, expressing the bent cylinder center coordinates in a (X,Y,Z) direct frame.')
end
if size(P,1) ~= 3 % if P is a row vector, change it in a column vector
    P = reshape(P,3,1);
end

Ns = ceil(2*a/dL); % number of elementary slices
% ab = a/b; % flattening
beta_str = num2str(beta);
beta = beta * pi/180; % degrees to radians

% slice center coordinates in (X,Y,Z) frame (bent cylinder center is
% 0,0,0):

x_s = linspace(-a,a,Ns); % x coordinates of the slice
y_s = zeros(size(x_s)); % row vector (m)
R = b*sqrt(abs(1-x_s.^2/a^2));
z_s = [-R; R]; % z boundaries of the slice
Xs_d = [x_s; y_s; z_s(1,:)]; % slice down-part coordinates
Xs_u = [x_s; y_s; z_s(2,:)]; % slice up-part coordinates

% if there is a tilt angle
Rot = [cos(beta) 0 sin(beta); 0 1 0; -sin(beta) 0 cos(beta)]; % rotation matrix
Xs_d_tilt = Rot*Xs_d;
Xs_u_tilt = Rot*Xs_u;

% figure; grid on; hold on; axis equal;
% set(gca,'YDir','reverse')
% plot(Xs_d(1,:),Xs_d(3,:),'b') % (X,Z) plan view, before tilt
% plot(Xs_d_tilt(1,:),Xs_d_tilt(3,:),'r') % (X,Z) plan view, after tilt
% plot(Xs_u(1,:),Xs_u(3,:),'b') % (X,Z) plan view, before tilt
% plot(Xs_u_tilt(1,:),Xs_u_tilt(3,:),'r') % (X,Z) plan view, after tilt
% title(strcat('Bent cylinder view before and after tilt(',beta_str,'�)'));
% xlabel('x-axis (m)'); ylabel('z-axis (m)');
% legend('before tilt','after tilt')

% transfer from origin (X,Y,Z) frame to KRM frame :
Xs_d_KRMframe = Tang2009coord2KRMcoord(Xs_d_tilt); 
Xs_u_KRMframe = Tang2009coord2KRMcoord(Xs_u_tilt); 
P_KRMframe = Tang2009coord2KRMcoord(P);

% Bent cylinder center is now P :
Xs_d_final_KRMframe = Xs_d_KRMframe + repmat(P_KRMframe,1,Ns);
Xs_u_final_KRMframe = Xs_u_KRMframe + repmat(P_KRMframe,1,Ns);


% Conversion for KRM view :
x_sb = Xs_d_final_KRMframe(1,:)';
x_b = x_sb;
w_sb = 2*R';
w_b = w_sb;
z_sb = [Xs_d_final_KRMframe(3,:)', Xs_u_final_KRMframe(3,:)'];
z_b = z_sb;

% PlotKRMSliceView( x_b, w_b, z_b, x_sb, w_sb, z_sb )

% Export the geometry :
save(strcat(ExptPath,ExptFileName,'.mat'), 'x_sb', 'x_b', 'w_sb', 'w_b', 'z_sb', 'z_b'); % Data export


end