function GenerateStraightCylinderForKRM(P, L, l, beta, dL, file_path, file_name)

% Check P variable format :
if length(P) ~= 3
    error('P must be a vector of dimension 3, expressing the bent cylinder center coordinates in a (X,Y,Z) direct frame.')
end
if size(P,1) ~= 3 % if P is a row vector, change it in a column vector
    P = reshape(P,3,1);
end

Ns = ceil(L/dL); % number of elementary cylinders
X = linspace(-L/2,L/2,Ns); % slice angle vector (rad)

Z = repmat([-l; 0],1,Ns);
XZd_0 = [X; Z(1,:)]; % bottom cylinder coordinates in origin frame
XZu_0 = [X; Z(2,:)]; % top cylinder coordinates in origin frame

beta = -beta*pi/180; % tilt angle (rad)
Rot = [cos(beta) -sin(beta); sin(beta) cos(beta)];

XZd_1 = Rot*XZd_0; % bottom cylinder coordinates in new frame
XZu_1 = Rot*XZu_0; % top cylinder coordinates in new frame

% Now, straight cylinder center is P
X1 = XZu_1(1,:)+P(1);
Zd_1 = XZd_1(2,:)+P(3);
Zu_1 = XZu_1(2,:)+P(3);

x_sb = X1';
x_b = x_sb;
w_sb = l*ones(size(x_sb));
w_b = w_sb;
z_sb = [Zd_1; Zu_1]';
z_b = z_sb;

% figure; hold on;
% h1 = plot(X*1e3, Z(1,:)*1e3, 'b');
% plot(X*1e3, Z(2,:)*1e3, 'b')
% plot([X(1) X(1)]*1e3, [Z(1,1) Z(2,1)]*1e3, 'b')         % Left end
% plot([X(end) X(end)]*1e3, [Z(1,end) Z(2,end)]*1e3, 'b') % Right end
% h2 = plot(x_sb*1e3, z_sb(:,1)*1e3, 'r');
% plot(x_sb*1e3, z_sb(:,2)*1e3, 'r')
% plot([x_sb(1) x_sb(1)]*1e3, [z_sb(1,1) z_sb(1,2)]*1e3, 'r')         % Left end
% plot([x_sb(end) x_sb(end)]*1e3, [z_sb(end,1) z_sb(end,2)]*1e3, 'r') % Right end
% xlabel('X - Length (mm)'); ylabel('Z - Height (mm)'); axis equal
% legend([h1 h2], {'normal cylinder', 'tilted cylinder'})

save(strcat(file_path,file_name,'.mat'), 'x_sb', 'x_b', 'w_sb', 'w_b', 'z_sb', 'z_b'); % Data export

end
