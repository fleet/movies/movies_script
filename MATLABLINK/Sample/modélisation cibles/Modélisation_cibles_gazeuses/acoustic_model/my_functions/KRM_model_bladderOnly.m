function soft_sigma = KRM_model_bladderOnly( f, ti, c0, c1, g, pathname, filename )

% filename with real units 

validateattributes(f,{'numeric'},{'nonempty', 'nonnegative'});
validateattributes(ti,{'numeric'},{'nonempty'});
validateattributes(c0,{'numeric'},{'nonempty', 'nonnegative', 'numel',1});
validateattributes(c1,{'numeric'},{'nonempty', 'nonnegative', 'numel',1});
validateattributes(g,{'numeric'},{'nonempty', 'nonnegative', 'numel',1});
% validateattributes(pathname,{'char'},{'nonempty'});
validateattributes(filename,{'char'},{'nonempty'});

dbstop if error; 

%% Get fish body and swimbladder shape

% Initialize variables
x_b = []; x_sb = []; z_b = []; z_sb = []; w_b = []; w_sb = [];

% Load data from that file
load(fullfile(pathname, filename))

% Check that the correct variables exist
% if sum(cellfun(@isempty, {x_b x_sb z_b z_sb w_b w_sb}))
%    error('MAT file did not contain the correct fish shape variables.')
% end

%% Get fish and water parameters

% Parse out inputs to variables
% p_b = str2double(answer{1});    % fish body density (kg/m^3)
% c_b = c0; % str2double(answer{2});    % fish body soundspeed (m/s)
% p_c = str2double(answer{3});    % swimbladder density (kg/m^3)
% c_c = c1; % str2double(answer{4});    % swimbladder soundspeed (m/s)
% p_w = str2double(answer{5});    % water density (kg/m^3)
c_w = c0; % str2double(answer{6});    % water soundspeed (m/s)

h = c1 / c0;

% Calculate static values that do not change with frequency, length, or phi
R_bc = (g*h - 1) / (g*h + 1);
R_wb = 0; % (p_b*c_b - p_w*c_w) / (p_b*c_b + p_w*c_w);
% TwbTbw = 1 - R_wb.^2;

% Lfish = x_b(end) - x_b(1); % fish body length in m
Lsb = x_sb(end) - x_sb(1); % fish bladder length in m

%% KRM frame origin must be the fish head :
x_head = x_sb(1);
z_head = mean(z_sb(1,:));
x_sb = x_sb - x_head;
z_sb = z_sb - z_head;

% PlotKRMSliceView( x_b, w_b, z_b, x_sb, w_sb, z_sb ) % check bladder orientation

%% Processing parameters

freqs = f;       % Frequencies (Hz)
phi = ti;        % Incidence angles (�)

% Check to see if orientations are between 65 and 115 degrees
% if sum(ti < 65 | ti > 115)
%   uiwait(warndlg({'KRM model is only reliable for orientations' ...
%     'between 65 and 115 degrees.'}, 'Unreliable Orientation', 'modal'))
% end

% Check variables to ensure correct dimensions
if isequal(size(freqs,2),1)         % Check frequencies
  freqs = freqs';
end

if isequal(size(phi,2),1)           % Check incidence angles
  phi = phi';
end

%% Begin processing data

% h = waitbar(0, 'Processing data...');   % Create waitbar
% count = 0;                              % Count variable for waitbar

phi = phi * pi / 180; % degrees to radians
% Initialize scattering length variables for fish body and swimbladder
% softL  = zeros(length(freqs), length(phi));
% fluidL = zeros(length(freqs), length(phi));


% Cycle through lengths

% Scale the body and bladder shape by the current fish length
% x_b2 = x_b .* lengths(i)/(x_b(end)-x_b(1));
% x_sb2 = x_sb .* lengths(i)/(x_b(end)-x_b(1));
% z_b2 = z_b .* lengths(i)/(x_b(end)-x_b(1));
% z_sb2 = z_sb .* lengths(i)/(x_b(end)-x_b(1));
% w_b2 = w_b .* lengths(i)/(x_b(end)-x_b(1));
% w_sb2 = w_sb .* lengths(i)/(x_b(end)-x_b(1));

% Interpolate data to 0.05mm cylinder resolution (Macaulay2013, paragraph 2
% page 3)
bool_interp = false;
if bool_interp
res = 50e-6; % 0.05mm
% x_b3 = linspace(x_b(1), x_b(end), 1 + Lsb/1e-4)'; % modification : interpolation with 1/10 mm for fish bladder
x_sb3 = linspace(x_sb(1), x_sb(end), 1 + Lsb/res)';
% z_b3 = interp1(x_b, z_b, x_b3, 'linear', 'extrap');
z_sb3 = interp1(x_sb, z_sb, x_sb3, 'linear', 'extrap');
% w_b3 = interp1(x_b, w_b, x_b3, 'linear', 'extrap');
w_sb3 = interp1(x_sb, w_sb, x_sb3, 'linear', 'extrap');
else
x_sb3 = x_sb; z_sb3 = z_sb; w_sb3 = w_sb;
end

% z_bmax3 = z_b3(:,1);
% z_bmin3 = z_b3(:,2);
z_sbmax3 = z_sb3(:,1);
z_sbmin3 = z_sb3(:,2);

i_slice = 1:length(x_sb3);

[Slice, F, PHI ] = ndgrid(i_slice', freqs, phi); % slice, f, theta

idx_s = Slice(1:end-1,:,:);
F = F(1:end-1,:,:);
PHIr = PHI(1:end-1,:,:);

% Slices coordinates : 4D Matrix format

% x_b4 = repmat(x_b3,1,length(freqs),length(phi));
x_sb4 = repmat(x_sb3,1,length(freqs),length(phi));
% z_bmax4 = repmat(z_bmax3,1,length(freqs),length(phi));
% z_bmin4 = repmat(z_bmin3,1,length(freqs),length(phi));
z_sbmax4 = repmat(z_sbmax3,1,length(freqs),length(phi));
z_sbmin4 = repmat(z_sbmin3,1,length(freqs),length(phi));
% w_b4 = repmat(w_b3,1,length(freqs),length(phi));
w_sb4 = repmat(w_sb3,1,length(freqs),length(phi));


a_s = (w_sb4(idx_s) + w_sb4(idx_s+1)) ./ 4; % equa 12 Clay94
% a_b = (w_b4(idx_s) + w_b4(idx_s+1)) ./ 4;

% Calculate variables that change with frequency (Clay94)
k = 2*pi*F ./ c_w;
% k_b = 2*pi*F ./ c_b;

A_sb = k.*a_s ./ (k.*a_s + 0.083); % formula 10
Psi_p = k.*a_s ./ (40+k.*a_s) - 1.05; % formula 10
% Psi_b = -pi.*k_b.*((z_bmax4(idx_s)+z_bmax4(idx_s+1))./2) ./ ... % formula 15
%   (2.*(k_b.*((z_bmax4(idx_s)+z_bmax4(idx_s+1))./2)+0.4));

% Calculate variables that change with angle
vs_u = x_sb4.*cos(PHI) + z_sbmax4.*sin(PHI); % keep only zmax (formula 5)
v_s = (vs_u(1:end-1,:,:) + vs_u(2:end,:,:)) / 2;
% figure; imagesc(squeeze(v_s)); title('vs'); colorbar;
vs_tmp = -1i*exp(-1i*(2*k.*v_s + Psi_p));
vs_tmp = squeeze(vs_tmp);
% figure; 
% subplot 211
% imagesc(abs(squeeze(vs_tmp))); title('vs'); colorbar;
% subplot 212
% plot(abs(sum(vs_tmp,1))); title('vs sum'); colorbar;

% vb_tempzmax = x_b4.*cos(PHI) + z_bmax4.*sin(PHI); % keep zmin and zmax
% vb_tempzmin = x_b4.*cos(PHI) + z_bmin4.*sin(PHI); % keep zmin and zmax

% v_b1 = (vb_tempzmax(1:end-1,:,:) + vb_tempzmax(2:end,:,:)) ./ 2;
% v_b2 = (vb_tempzmin(1:end-1,:,:) + vb_tempzmin(2:end,:,:)) ./ 2;

delta_u_s = (x_sb4(2:end,:,:) - x_sb4(1:end-1,:,:)) .* sin(PHIr);
% delta_u_b = (x_b4(2:end,:,:) - x_b4(1:end-1,:,:)) .* sin(PHIr);

% figure; imagesc(squeeze(abs(2*k.*v_s))); title('exp'); colorbar;

% Form function for the fish bladder

buff = A_sb.*((k.*a_s+1).*sin(PHIr)).^0.5 .* exp(-1i*(2*k.*v_s + Psi_p )).*delta_u_s ;
% delta = k .* delta_u_s .* cos(PHIr);
% sinc_delta = sin(delta) ./ delta;
% sinc_delta(delta == 0) = 1;
% buff = buff .* sinc_delta;
% figure; imagesc(squeeze(abs(buff))); title('somme');
% Sum over all slices
buff = sum(buff,1);

% Calculate scattering length of the swimbladder
softL = -1i*R_bc*(1-R_wb^2) ./ (2*sqrt(pi)) .* buff; % 1 x freq x phi

% backscattering cross section of the swimbladder
soft_sigma = abs(softL).^2;

% nsoft_sigma = soft_sigma ./ Lsb^2; % length-normalised backscattering cross section

% % Form function for the fish body
% buff = ((k.*a_b).^0.5 .* delta_u_b .* ...
%   (exp(-1i.*2.*k.*v_b1) - TwbTbw .* ...
%   exp(-1i.*2.*k.*v_b1 + ...
%   1i.*2.*k_b.*(v_b1-v_b2) + 1i.*Psi_b)));
% buff = sum(buff,1);
% 
% % Calculate scattering length of the fish body
% fluidL(ii,:,:) = -1i .* (R_wb./(2*sqrt(pi))) .* buff;
  

% TS = 20*log10(abs(softL));	% Calculate TS
% TS = squeeze(TS);
% sigma = 10.^(TS./10);                % Calculate scattering cross-section


%% Format results
% The results will be displayed as graphs as well as made into tables using
% a csv-style .txt file.  For the three input parameters (incidence angle,
% frequency, and fish length), TS will be calculated for each parameter by
% averaging the TS over the other two parameters.
%
% For example, if plotting TS vs. frequency, for each frequency, TS will be
% calculated for every combination of incidence angle and fish length, then
% averaged out to obtain a single TS value.

%%%%%%%%%%%%%%%%%%%% Plot TS vs. length
if (0)
% Initialize the mean and percentile variables
sigmavsL = nan(size(TS,1), 1);
P = nan(size(TS,1), 2);

% Cycle through lengths
for i = 1:size(TS,1)
  temp = squeeze(sigma(i,:,:));           % Get sigma for freq and phi
  sigmavsL(i) = mean(temp(:));            % Calculate mean
  temp_sort = sort(temp(:)); % sort vector temp
  
  ne_200quantile = size(temp_sort,1) ./ 200; % number of elements in each 200-quantile
  idx_l = ceil(5*ne_200quantile);
  idx_h = floor(195*ne_200quantile);
  P(i,:) = [temp_sort(idx_l), temp_sort(idx_h)];
end

TSvsL = 10*log10(sigmavsL);                 % Convert to TS
L = TSvsL - 10*log10(P(:,1));               % Calculate lower errorbar
U = 10*log10(P(:,2)) - TSvsL;               % Calculate lower errorbar


% Plot figure with errorbars
figure
errorbar(lengths*1e2, TSvsL, L, U,'Marker', '.','MarkerSize',30,'LineStyle','-','LineWidth',1,'color','k')
xlabel('Fish length (cm)'); ylabel('TS (dB)'); title('TS vs. length');


% Create .txt file for TS vs. Length
fid_TSvsL = fopen(strcat(filesave, '_TSvsL.txt'), 'w+t');   % Create file
fprintf(fid_TSvsL, '%s,%s\n', 'Length(mm)', 'TS(dB)');      % Add header
fprintf(fid_TSvsL, '%f,%f\n', [lengths; TSvsL']);           % Add data
fclose(fid_TSvsL);                                          % Close file


%%%%%%%%%%%%%%%%%%%% Plot TS vs. frequency

% Initialize the mean and percentile variables
sigmavsF = nan(size(TS,2), 1);
P = nan(size(TS,2), 2);

% Cycle through frequencies
for i = 1:size(TS,2)
  temp = squeeze(sigma(:,i,:));           % Get sigma for lengths and phi
  sigmavsF(i) = mean(temp(:));            % Calculate mean
  temp_sort = sort(temp(:)); % sort vector temp
  
  ne_200quantile = size(temp_sort,1) ./ 200; % number of elements in each 200-quantile
  idx_l = ceil(5*ne_200quantile);
  idx_h = floor(195*ne_200quantile);
  P(i,:) = [temp_sort(idx_l), temp_sort(idx_h)];
end

TSvsF = 10*log10(sigmavsF);                 % Convert to TS
L = TSvsF - 10*log10(P(:,1));               % Calculate lower errorbar
U = 10*log10(P(:,2)) - TSvsF;               % Calculate lower errorbar

% Plot figure with errorbars
figure
errorbar(freqs/1e3, TSvsF, L, U,'Marker', '.','MarkerSize',30,'LineStyle','-','LineWidth',1,'color','k')
xlabel('Frequency (kHz)'); ylabel('TS (dB)'); title('TS vs. frequency');

% Create .txt file for TS vs. Frequency
fid_TSvsF = fopen(strcat(filesave, '_TSvsF.txt'), 'w+t');   % Create file
fprintf(fid_TSvsF, '%s,%s\n', 'Frequency(kHz)', 'TS(dB)');  % Add header
fprintf(fid_TSvsF, '%f,%f\n', [freqs; TSvsF']);             % Add data
fclose(fid_TSvsF);                                          % Close file


%%%%%%%%%%%%%%%%%%%% Plot TS vs. incidence angle

% Initialize the mean and percentile variables
sigmavsPhi = nan(size(TS,3), 1);
P = nan(size(TS,3), 2);

% Cycle through angles
for i = 1:size(TS,3)
  temp = squeeze(sigma(:,:,i));           % Get sigma for L and freq
  sigmavsPhi(i) = mean(temp(:));          % Calculate mean
  temp_sort = sort(temp(:)); % sort vector temp
  
  ne_200quantile = size(temp_sort,1) ./ 200; % number of elements in each 200-quantile
  idx_l = ceil(5*ne_200quantile);
  idx_h = floor(195*ne_200quantile);
  P(i,:) = [temp_sort(idx_l), temp_sort(idx_h)];
end

TSvsPhi = 10*log10(sigmavsPhi);             % Convert to TS
L = TSvsPhi - 10*log10(P(:,1));             % Calculate lower errorbar
U = 10*log10(P(:,2)) - TSvsPhi;             % Calculate lower errorbar

% Plot figure with errorbars
figure
errorbar(phi*180/pi, TSvsPhi, L, U,'Marker', '.','MarkerSize',30,'LineStyle','-','LineWidth',1,'color','k')
xlabel('Tilt angle (�)'); ylabel('TS (dB)'); title('TS vs. tilt angle');

% Create .txt file for TS vs. Incidence angle
fid_TSvsPhi = fopen(strcat(filesave, '_TSvsPhi.txt'), 'w+t');   % Create file
fprintf(fid_TSvsPhi, '%s,%s\n', 'Phi(degrees)', 'TS(dB)');      % Add header
fprintf(fid_TSvsPhi, '%f,%f\n', [180*phi/pi; TSvsPhi']);        % Add data
fclose(fid_TSvsPhi);                                            % Close file


%%%%%%%%%%%%%%%%%%%% Plot reduced TS vs. length/wavelength

% Initialize the mean and percentile variables
sigmarvsLr = nan(size(TS,1)*size(TS,2), 1);
P = nan(size(TS,1)*size(TS,2), 2);
kL=nan(size(TS,1)*size(TS,2),1);

% Cycle through length
for i = 1:size(TS,1)
  %Cycle through frequency
  for j=1:size(TS,2)
%     temp = squeeze(sigma(i,j,:))./(lengths(i)*lengths(i));           % Get sigma for L and freq
    temp = sigma(i,j,:) ./ (lengths(i)^2);           % Get normalised sigma for L and freq
    sigmarvsLr((i-1)*size(TS,2)+j) = mean(temp(:));          % Calculate mean
    temp_sort = sort(temp(:)); % sort vector temp
    ne_200quantile = size(temp_sort,1) ./ 200; % number of elements in each 200-quantile
    idx_l = ceil(5*ne_200quantile);
    idx_h = floor(195*ne_200quantile);
    P((i-1)*size(TS,2)+j,:) = [temp_sort(idx_l), temp_sort(idx_h)];
    kL((i-1)*size(TS,2)+j)=2*pi*lengths(i)*freqs(j)/c_w;
  end
end

TSrvskL = 10*log10(sigmarvsLr);             % Convert to TS
L = TSrvskL - 10*log10(P(:,1));             % Calculate lower errorbar
U = 10*log10(P(:,2)) - TSrvskL;             % Calculate lower errorbar

% Plot figure with errorbars
figure
errorbar(kL', TSrvskL, L, U,'Marker', '.','MarkerSize',30,'LineStyle','-','LineWidth',1,'color','k')
xlabel('kL'); ylabel('TS (dB)'); title('TS vs. kL');

% Create .txt file for reducd TS vs. length/wavelength
fid_TSrvsLr = fopen(strcat(filesave, '_TSrvsLr.txt'), 'w+t');   % Create file
fprintf(fid_TSrvsLr, '%s,%s\n', 'L/lambda', 'ReducedTS(dB)');      % Add header
fprintf(fid_TSrvsLr, '%f,%f\n', [kL; TSrvskL]);        % Add data
fclose(fid_TSrvsLr);

end
