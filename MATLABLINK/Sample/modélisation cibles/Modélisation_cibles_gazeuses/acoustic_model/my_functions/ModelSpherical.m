%--------------------------------------------------------------------------
% ModelSpherical.m :
%   Modelled TS ( dB and arithmetic) and reduced TS for spherical
%   High pass model
%--------------------------------------------------------------------------
% Input  :
%   material = given material (text)
%   fx       = vector of frequencies
%   tabSize  = vector of sizes
%   g        = density
%   h        = speed
%   cmoy     = sound celerity in sea water

% Output :
%   TS       = Target Strength in dB
%   TSreduce = Reduced target strength
%   TAij     = Target Strenght in arithmetic values
%--------------------------------------------------------------------------
% References :
%   Stanton
%--------------------------------------------------------------------------
% Version written for Matlab 6.5 Release 13 for PC
%--------------------------------------------------------------------------
% Original : 18/02/03 J.Balle-Beganton 
% Modified :
%--------------------------------------------------------------------------

function [TS, TSreduce, TAij] = ModelSpherical(material,fx,tabSize,g,h,cmoy)

if strcmp(material,'rgfx')
    % ----- For gazeous material : g,h towards infinity
    R = 1;
    alphaPiS = -5/6;
    alphaPiC = -3/2; 
else
    alphaPiS = (1 - g*h^2) / (3*g*h^2) + (1 - g) / (1 + 2*g) ;
    alphaPiC = (1 - g*h^2) / (2*g*h^2) + (1 - g) / (1 + g) ;
    R = (g*h - 1) / (g*h + 1) ;
end

for ifreq = 1:length(fx)
    for ia = 1:length(tabSize)
        f = fx(ifreq);   % f in Hz
        k = 2 * pi * f / cmoy ;
        ka = k * tabSize(ia); 
 
        if strcmp(material,'flui') 
            F = 40 .* ((ka).^(-0.4)) ;
            G = [1 - 0.8 .* exp(-2.5 .* ((ka - 2.25).^2))] ;
        elseif strcmp(material,'gaze') 
            F = 1 + 0.5 .* (ka).^(-0.75) ;
            G = 1 + 85 .* exp(-500000 .* ((ka - 0.0135).^2)) ;
        else 
            F = 15 .* (ka).^(-1.9) ;
            G = 1 ; 
        end

        sigmaBSreduce = [(ka^4) * (alphaPiS^2) * (G/pi) ] /...
                    [1 + (4*(ka^4) * (alphaPiS^2)) / (F * R^2)] ;
        sigmaBS = sigmaBSreduce * pi * tabSize(ia)^2;
    
        TS(ifreq,ia) = 10*log10(sigmaBS) ;
        TSreduce(ifreq,ia) = 10*log10(sigmaBSreduce) ;
        TAij(ifreq,ia) = sigmaBS ;
    end
end
