function PlotKRMSliceView( x_b, w_b, z_b, x_sb, w_sb, z_sb )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

figure

% convert m into mm
x_b = x_b*1e3;
w_b = w_b*1e3;
z_b = z_b*1e3;
x_sb = x_sb*1e3;
w_sb = w_sb*1e3;
z_sb = z_sb*1e3;

% Plot lateral view
subplot(2,1,1)
hold on

% Plot fish body
if ~isempty(x_b)
plot(x_b, z_b(:,1), 'k', x_b, z_b(:,2), 'k')            % Upper and lower
plot([x_b(1) x_b(1)], [z_b(1,1) z_b(1,2)], 'k')         % Left end
plot([x_b(end) x_b(end)], [z_b(end,1) z_b(end,2)], 'k') % Right end
text(x_b(1),mean([z_b(1,1) z_b(1,2)]),' \leftarrow fish head')
text(x_b(end),mean([z_b(end,1) z_b(end,2)]),' \leftarrow fish tail')
end

% Plot fish bladder
plot(x_sb, z_sb(:,1), 'k', x_sb, z_sb(:,2), 'k')        % Upper and lower
plot([x_sb(1) x_sb(1)], [z_sb(1,1) z_sb(1,2)], 'k')     % Left end
plot([x_sb(end) x_sb(end)], [z_sb(end,1) z_sb(end,2)], 'k') % Right end

axis equal; axis tight;              % make axes equal
title('Lateral view')
ylabel('Height (mm)')
xlabel('Length (mm)')

% Plot dorsal view
subplot(2,1,2)
hold on

% Plot fish body
if ~isempty(x_b)
plot(x_b, w_b/2, 'k', x_b, -w_b/2, 'k')                 % Upper and lower
plot([x_b(1) x_b(1)], [-w_b(1)/2 w_b(1)/2], 'k')        % Left end
plot([x_b(end) x_b(end)], [-w_b(end)/2 w_b(end)/2], 'k')	% Right end
end

% Plot fish bladder
plot(x_sb, w_sb/2, 'k', x_sb, -w_sb/2, 'k')             % Upper and lower
plot([x_sb(1) x_sb(1)], [-w_sb(1)/2 w_sb(1)/2], 'k')    % Left end
plot([x_sb(end) x_sb(end)], [-w_sb(end)/2 w_sb(end)/2], 'k')    % Right end

axis equal; axis tight; 
title('Dorsal view')
ylabel('Width (mm)')
xlabel('Length (mm)')


end

