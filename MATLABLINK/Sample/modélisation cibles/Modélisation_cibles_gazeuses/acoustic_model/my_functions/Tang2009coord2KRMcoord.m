function X_KRM = Tang2009coord2KRMcoord( X_Tg09 )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
% X_Tg09 : X,Y,Z coordinates in Tang2009 conventional frame
% X_KRM  : X,Y,Z coordinates in Clay&Horne1994 conventional frame
% X format : 3 rows * N columns

X_KRM_decreasingX = -eye(3)*X_Tg09; % transfer from origin (X,Y,Z) frame to KRM frame 
X_KRM = fliplr(X_KRM_decreasingX); % x-coordinate value is increasing
% now, X_KRM(1) is the min x-value and X_KRM(end) is the max x-value


end

