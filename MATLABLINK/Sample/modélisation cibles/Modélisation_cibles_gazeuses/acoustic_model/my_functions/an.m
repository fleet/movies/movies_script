function An = an(x0,x1,g,n,SphereType)
% modal term bm for a fluid sphere

% x0=k0*a
% x1=k1*a
% def = 1 : bm definition from Stanton 1988, 1989
% def = 2 : bm definition from Ye 1997


% epsilon is the Neumann factor, a rather obtuse way of saying this:
if n == 0
    eps = 1;
else
    eps = 2;
end

% Bessel functions
Jn_x0 = besselj(n,x0); % n-th order Bessel function if the first kind
%     Jn_x0_save{n+1} = Jn_x0;
Jnn_x0 = besselj(n+1,x0);
%     Jnn_x0_save{n+1} = Jnn_x0;
Jnnn_x0 = 2*(n+1) .* Jnn_x0 ./ x0 - Jn_x0; % fin page 6 : http://g.boutte.free.fr/cours/Bessel.pdf

Nn_x0 = bessely(n,x0); % n-th order Bessel function of the second kind
Nnn_x0 = bessely(n+1,x0);
NN_x0 = n*Nn_x0./x0 - Nnn_x0;

JJ_x0 = n*Jn_x0./x0 - Jnn_x0;
%     JJJ_x0 = Jnnn_x0 - (2*n-1)*Jnn_x0./x0 + (n*(n-1)./(x0.^2)).*Jn_x0;
JJJ_x0 = (n./x0.^2).*JJ_x0 - (n./x0.^2).*Jn_x0 - ((n+1)./x0).*Jnn_x0 + Jnnn_x0;
Jn_x1 = besselj(n,x1); % n-th order Bessel function of the first kind
JJ_x1 = n*Jn_x1./x1 - besselj(n+1,x1);

%     J_x2 = besselj(n,x2); % n-th order Bessel function of the first kind
%     JJ_x2 = n*J_x2./x2 - besselj(n+1,x2);
% Hankel functions
H_x0  = besselh(n,1,x0); % Hankel function of the first kind
HH_x0 = n*H_x0./x0 - besselh(n+1,1,x0);

% An computation :
switch SphereType
    case 'fluid_sphere' % formula 11 from Sage1979
        An_num = g*x0.*Jn_x1.*JJ_x0  - x1.*JJ_x1.*Jn_x0;
        An_den = g.*x0.*Jn_x1.*HH_x0 - x1.*JJ_x1.*H_x0;
        An = -An_num./An_den;
    case 'soft_sphere' % formula 12b from Sage1979
        An = -Jn_x0./H_x0;
    case 'rigid_sphere' % formula 12a from Sage1979
        An = -JJ_x0./HH_x0;
    otherwise
        error('myApp:argChk', 'The parameter ''SphereType'' value must be ''fluid_sphere'' or ''soft_sphere'' or ''rigid_sphere''.')
end
        
        
end

