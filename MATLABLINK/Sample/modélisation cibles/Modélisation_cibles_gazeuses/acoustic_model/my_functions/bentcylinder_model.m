function nsigma_BS = bentcylinder_model( kL0, L_b0, ti0, g, h, gamma_max, n_max )


%% Parameters computation
% angles convention detailled in figure 1 from Ye1997 and in the last paragraph of page 4

L0 = 1; % arbitral swimbladder length for computation : 1m
% As nTS depends only upon ka, ab and tilt angle ; L0 value does not matter

gamma_max = gamma_max * pi/180;
ti0 = ti0 * pi/180; % incident angle in radians
[kL_w, L_b, ti] = ndgrid(kL0, L_b0, ti0); % L x f x theta

% kL_w : wave number (m^-1) in water * cylinder length
% kb_w : wave number (m^-1) in water * cylinder radius
% kb_c : wave number (m^-1) in cylinder * cylinder radius

kb_w = kL_w ./ L_b; % coefficient 2 comes from diameter=2*radius
if h == 0 % soft cylinder
    kb_c = zeros(size(kb_w));
else
    kb_c = kb_w / h; % wave number (m^-1) in cylinder * cylinder radius
end


x0 = kb_w; % outside the cylinder with incidence direction
x1 = kb_c; % inside the cylinder with incidence direction

% Approximate limits on the summations (TO ADJUST)
if not(exist('n_max','var'))
n_max = 30; %ceil(2*k0*a);
end

% Terms in the summation smaller than 'tol' will cause the iteration to
% stop.
tol = -200; % [dB] 
tsx = tol+1;

nb_gamma = 50;
gamma = linspace(-gamma_max,gamma_max,nb_gamma); % gamma angle value in radians
dgamma = gamma(2)-gamma(1);
dr_pos = abs(dgamma*L0/(2*gamma_max)); % length normalised !

T1 = 0; % integral

for i_gamma = 1:nb_gamma
    
    ga = gamma(i_gamma); % current gamme angle value in radians
    T2 = 0; % modal serie
    n = -1; % infinite sum index
    while (n < n_max) && (tsx > tol) % modal serie computation for i_gamma
        n = n+1;
        % bn computation(i_gamma,n)
        % you have to correct the wavenumbers for bn 
        bn_corr = abs(sin(ti)*sin(ga)+cos(ti)*cos(ga));
        Bn = bn(x0.*bn_corr,x1.*bn_corr,g,h,n,1);
%         figure;plot(squeeze(abs(Bn(70,1,:))))
        
        % t1 computation(i_gamma,m)
%         exp_term = 1i*kL_w/gamma_max*(1-cos(gamma(i_gamma)));
%         exp_term = -1i*kL_w/gamma_max.*((1-cos(ga))*cos(ti-pi/2)+tan(ga)*sin(ti-pi/2));
        exp_term = 1i*kL_w/gamma_max.*(sin(ti)*sin(ga)+cos(ti)*(cos(ga)-1));
        T3 = Bn*((-1)^n).*exp(exp_term); % formula 26 from Stanton1989
        tsx = max(20*log10(abs(T3(:)))); 
        % on incremente MS(i_gamma)
        T2 = T2 + T3;
    end
    tsx = tol+1;
    % T2 is now the full modal serie for i_gamma
    T1 = T1 + T2*dr_pos;
    
end
% T1 is now the full integral (complex)

% str1 = sprintf('Model : Modal serie bentcylinder (Stanton1989) \nNumber of iterations : %i', n+1);
% disp(str1);

f_BS = -1i/pi*T1; % formula 8 from Stanton 1989
nf_BS = f_BS / L0;
nsigma_BS = abs(nf_BS).^2;


% nTS = 10*log10(nsigma_BS);
% nTS = squeeze(nTS);
% 
% figure
% plot(nTS(:,1)); grid on;



end



