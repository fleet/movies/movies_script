function Bn = bn(x0,x1,g,h,n,def)
% modal term bm for a fluid cylinder

% def = 1 : bm definition from Stanton 1988, 1989
% def = 2 : bm definition from Ye 1997
% def = 3 : bm definition from Stanton 1988 - adapted for a soft cylinder

% epsilon is the Neumann factor, a rather obtuse way of saying this:
if n == 0
    eps = 1;
else
    eps = 2;
end

if h == 0 % soft cylinder
def = 3;
end

%% Bessel and Hankel functions computation :

if def == 1 || def == 2 % fluid cylinder
    % Bessel functions
    Jn_x0 = besselj(n,x0); % n-th order Bessel function if the first kind
    %     Jn_x0_save{n+1} = Jn_x0;
    Jnn_x0 = besselj(n+1,x0);
    %     Jnn_x0_save{n+1} = Jnn_x0;
    Jnnn_x0 = 2*(n+1) .* Jnn_x0 ./ x0 - Jn_x0; % fin page 6 : http://g.boutte.free.fr/cours/Bessel.pdf
    
    Nn_x0 = bessely(n,x0); % n-th order Bessel function of the second kind
    Nnn_x0 = bessely(n+1,x0);
    NN_x0 = n*Nn_x0./x0 - Nnn_x0;
    
    JJ_x0 = n*Jn_x0./x0 - Jnn_x0;
    %     JJJ_x0 = Jnnn_x0 - (2*n-1)*Jnn_x0./x0 + (n*(n-1)./(x0.^2)).*Jn_x0;
    JJJ_x0 = (n./x0.^2).*JJ_x0 - (n./x0.^2).*Jn_x0 - ((n+1)./x0).*Jnn_x0 + Jnnn_x0;
    Jn_x1 = besselj(n,x1); % n-th order Bessel function of the first kind
    JJ_x1 = n*Jn_x1./x1 - besselj(n+1,x1);
    
    %     J_x2 = besselj(n,x2); % n-th order Bessel function of the first kind
    %     JJ_x2 = n*J_x2./x2 - besselj(n+1,x2);
    % Hankel functions
    H_x0  = besselh(n,1,x0); % Hankel function of the first kind
    HH_x0 = n*H_x0./x0 - besselh(n+1,1,x0);
    
    if def == 1 % fluid cylinder
        Cn_num = (JJ_x1.*Nn_x0)./(Jn_x1.*JJ_x0)-g*h*(NN_x0./JJ_x0);
        Cn_den = (JJ_x1.*Jn_x0)./(Jn_x1.*JJ_x0)-g*h;
        Cn = Cn_num ./ Cn_den; % formula 12 from Stanton1988
        Bn = -eps ./ (1+1i*Cn);
    elseif def == 2 % fluid cylinder
        beta = g*h*Jn_x1./JJ_x1; % formula 11 from Ye1997
        Bn_num = -eps*(Jn_x0-beta.*JJJ_x0);
        Bn_den = H_x0-beta.*HH_x0;
        Bn = Bn_num ./ Bn_den; % formula 11 from Ye1997
    end
    
elseif def == 3 % soft cylinder
    % Bessel functions
    Jn_x0 = besselj(n,x0); % n-th order Bessel function if the first kind
    H_x0  = besselh(n,1,x0); % Hankel function of the first kind
    Bn_num = -eps*Jn_x0;
    Bn_den = H_x0;
    Bn = Bn_num ./ Bn_den; % formula 11 from Ye1997
    
else
    error('myApp:argChk', 'The parameter ''def'' value must be 1 or 2 or 3.')
end


end

