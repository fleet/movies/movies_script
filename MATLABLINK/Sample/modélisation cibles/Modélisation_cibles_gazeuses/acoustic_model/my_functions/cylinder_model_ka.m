function nsigma_BS = cylinder_model_ka( kL0, L_b0, ti0, g, h, i_model )
%cylinder_model_ka : computes the reduced scattering function for a cylinder of finite length L0

% Asumptions :
% - we consider the scattering function of a fluid finite straight cylinder
% - "a differential element of the cylinder scatters sound as though it were part of an infinite cylinder of the same radius" [cf. Ye1997]
% - scattering function is given by Kirchhoff integral theorem
% - integrate the volume flow along the length of the cylinder
% - the rigorous solution is a modal serie

%   INPUT
% - L0    : reference cylinder length (m)
% - kL0   : 2*pi*f*L0/c
% - L_b0  : the dimensionless ratio for longitudinal length (L) by cross-sectional radius (b)
% - ti0   : incident angle (�) (convention detailled in figure 1 from Ye1997)
% - g     : mass density dimensionless ratio
%                         g=rho_c/rho_w with :
%                             - rho_c the mass density insider the cylinder
%                             - rho_w the mass density outside the cylinder
% - h     : relative sound velocity
% - i_model : integer value from 1 to 6:
%     1 : Stanton1988 modal serie
%         - formula 28 from Stanton1988
%         - hypothesis :
%             - no absorption, dispersion or nonlinearities
%                in the cylinder or surrounding medium
%             - fluid cylinder (does not support shear waves)
%             - does not take into account end effects 
%                (considered as not important), so internal field
%                can be approximated by the internal field solution
%                of an infinite cylinder
%     2 : Ye1997 modal serie
%         - formula 15 from Ye1997
%     3 : Ye1997 modal serie + Ye1997 scattering end effect
%         - formula 15 from Ye1997 + formula 18 from Ye1997
%         - take into account the body contribution AND the end effect 
%     4 : Stanton1988 modal serie + Ye1997 scattering end effect
%         - formula 28 from Stanton1988 + formula 18 from Ye1997
%         - take into account the body contribution AND the end effect
%     5 : Stanton1988 HF formula
%         - formula 37 from Stanton1988
%         - high frequencies (k*sin(ti)*b >> 1) = solid cylinder
%     6 : Do&Surti1990 HF formula
%         - formula 8 from Do&Surti1990 (sin^2 should be a sin)
%         - high frequencies (k*sin(ti)*b >> 1)
%     7 : Soft cylinder modal series

%   OUTPUT
%           - nsigma_BS  : reduced (ie. length-normalised) backscattering cross section function


%% Parameters computation
% angles convention detailled in figure 1 from Ye1997 and in the last paragraph of page 4
% b0 = (L0./2) ./ ab0; % cylinder radius (m)

L0 = 1; % arbitral swimbladder length for computation : 1m
% As nTS depends only upon ka, ab and tilt angle ; L0 value does not matter
model_desc = {'Modal serie (Stanton1988)', ...
    'Modal serie (Ye1997)', ...
    'Modal serie (Stanton1988) with end effect (Ye1997)', ...
    'Modal serie (Ye1997) with end effect (Ye1997)', ...
    'High frequencies (Stanton1988)', ...
    'High frequencies (Do&Surti1990)'};

ti0 = ti0 * pi/180; % incident angle in radians
[kL_w, L_b, ti] = ndgrid(kL0, L_b0, ti0); % kL x L/b x theta
kL_w = reshape(kL_w, length(kL0), length(L_b0), length(ti0));

% kL_w : wave number (m^-1) in water * cylinder length
% kb_w : wave number (m^-1) in water * cylinder radius
% kb_c : wave number (m^-1) in cylinder * cylinder radius

kb_w = kL_w ./ L_b; % coefficient 2 comes from diameter=2*radius

if h == 0 % soft cylinder
kb_c = zeros(size(kb_w));
else
kb_c = kb_w / h;
end

% L = L0 * ones(size(kL_w));
% f = kL_w * c0 ./ (2*pi*L0);
% b = L ./ ab;%(2*ab);

ts = pi - ti; % scattering angle in radians
ps = pi;  % azimuthal angle in radians

% wave number (m^-1)
% k0 = 2*pi*f ./ c0; % outside the cylinder
% k1 = 2*pi*f ./ c1; % inside the cylinder

x0 = kb_w .* sin(ti); % outside the cylinder with incidence direction
x1 = kb_c .* sin(ti); % inside the cylinder with incidence direction
% x2 = k0 .* b .* sin(ts); % outside the cylinder with scattering direction

% local parameter : formula 13 from Ye1997
delta = kL_w .*(cos(ts)-cos(ti)) ./ 2;
sinc_ = sin(delta) ./ delta;
sinc_(delta == 0) = 1; % the sinc(x) equals to 1 when x=0

% Approximate limits on the summations (TO ADJUST)
m_max = 20; %ceil(2*k0*a);
if i_model ==8
    m_max = 3;
end

% Terms in the summation smaller than 'tol' will cause the iteration to
% stop.
tol = -200; % [dB] 

f_BS = 0.0;
nsigma_BS = []; % reduced (ie. length-normalised) backscattering cross section function
n = -1; % infinite sum index
tsx = tol+1;

while (tsx > tol) && (n < m_max) && any(i_model == [1 2 3 4 7 8])
    n = n+1; % modal serie counter
    
    if any(i_model == [1 3 7 8]) % Terms used in publication Stanton1988 :
        bn_type = 1;
    elseif any(i_model == [2 4]) % Terms used in publication Ye1997 :
        bn_type = 2;
    end

%     % Terms used in publication Stanton1988 :
%     if any(i_model == [1 3])
%         if soft_cylinder
%             bn_type = 3;
%         else
%             bn_type = 1;
%         end
%     % Terms used in publication Ye1997 :
%     elseif any(i_model == [2 4])
%         bn_type = 2;
%     end
    Bn = bn(x0,x1,g,h,n,bn_type);
    
%     Fn = (pi*k0.*b ./ (2*1i)) .* (HH_x0.*J_x2.*sin(ti) - H_x0.*JJ_x2.*sin(ts)); % formula 14 from Ye1997
    
    % Formula 28 from Stanton1988 equivalent to formula 15 from Ye1997 :
    t_sum = (-1i*L0/pi).*sinc_.*Bn*cos(n*ps);
    
    f_BS = f_BS + t_sum;

    tsx = max(20*log10(abs(t_sum(:) ))); % FORMULE A CONFIRMER

end

% if i_model < 5 % show the number of modal serie terms
%     str1 = sprintf('Model : %s \nNumber of iterations : %i', model_desc{i_model}, n+1);
%     disp(str1);
% end

if any(i_model == [3 4]) % add end effect
    p0 = atan2(sin(ts)*sin(ps) ./ (sin(ti)-sin(ts)*cos(ps)),1);
    k00b = kb_w.*abs((sin(ti)-sin(ts)*cos(ps)) ./ cos(p0));
    
    % end effect scattering function
    f_end = ((g*h-1)/(g*h+1))*(-0.5*1i*kb_w.*L0./L_b).*(cos(ts)-cos(ti)).* ...
        exp(-1i*0.5*kL_w.*(cos(ts)-cos(ti))).*besselj(1,k00b)./(k00b);
    % global scattering function
    f_BS = f_BS + f_end;

elseif any(i_model == [5 6]) % high frequencies
    delta_ = kL_w.*cos(ti);
    sinc_ = sin(delta_) ./ delta_;
    sinc_(delta_ == 0) = 1;
    if i_model == 5 % HF (Stanton1988)
        sigma_BS = kb_w.*sin(ti)    *L0^2 .* sinc_.^2 / (4*pi); % formula 37 from Stanton1988
    elseif i_model == 6 % HF (Do&Surti1990)
        sigma_BS = kb_w.*sin(ti).^2 *L0^2 .* sinc_.^2 / (4*pi) ; % formula 7 from Do&Surti1990
    end
	nsigma_BS = sigma_BS ./ L0^2;
end

if isempty(nsigma_BS) % for models 1,2,3,4
    nsigma_BS = abs(f_BS ./ L0).^2;
end

end

