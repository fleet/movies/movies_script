function nsigma_BS = deformedcylinder_model( f, ti, c0, c1, g, pathname, filename )

%% Parameters computation
% angles convention detailled in figure 1 from Ye1997 and in the last paragraph of page 4

ti = ti * pi/180;
h = c1/c0;

% Initialize variables
x_sb = []; z_sb = []; w_sb = []; 

% Load data from that file
load(fullfile(pathname, filename))

% PlotKRMSliceView( [], [], [], x_sb, w_sb, z_sb ) % check initial bladder orientation


% Get the correct x-axis and z-axis orientation :
x_sb = flipud(x_sb); % change x-axis orientation
% w_sb = flipud(w_sb); % change x-axis orientation
z_sb = -z_sb; % change z-axis orientation

% New variables
X_raw = x_sb';
Zu_raw = z_sb(:,1)';
Zd_raw = z_sb(:,2)';
Z_raw = (Zu_raw+Zd_raw)/2; % mean according to z-dimension
W_raw = w_sb'; 

% Shape center computation :
Csb_x = mean(X_raw); % x-coordinate swimbladder center
Csb_z = Z_raw(round(end/2)); % z-coordinate swimbladder center (mean according to z-dimension in bladder central slice)
% Shape coordinates offset, the origin is now the shape center :
X_shifted = X_raw - Csb_x;   % x-origin offset
Z_shifted = Z_raw - Csb_z; % z-origin offset

% Now we consider each elementary cylinders and compute their center
% coordinates and radius :
Nec = length(X_shifted)-1; % number of elements (cylinders)
b_ec = (W_raw(1:Nec) + W_raw(2:Nec+1)) / 4; % radius of each elementary cylinder
X_ec = (X_shifted(1:Nec) + X_shifted(2:Nec+1)) / 2; % x-coordinate of each elementary cylinder
Z_ec = (Z_shifted(1:Nec) + Z_shifted(2:Nec+1)) / 2; % mean according to z-dimension
% PlotKRMSliceView( [], [], [], X_shifted', W_raw', [Zd_raw-Csb_z; Zu_raw-Csb_z]' ) % check final bladder orientation

% vectors for each slice :
ri = [cos(ti); sin(ti); zeros(size(ti))];
r_pos = [Z_ec; X_ec; zeros(size(X_ec))]; % non-normalised vector
dr_pos = sqrt((Z_shifted(2:end)-Z_shifted(1:end-1)).^2 + (X_shifted(2:end)-X_shifted(1:end-1)).^2);
L_shape = sum(dr_pos); % shape length (m)
dr_pos = repmat(dr_pos',1,length(f),length(ti));
% r_tan computation and normalisation :
r_tan = [Z_ec(3:end)-Z_ec(1:end-2); X_ec(3:end)-X_ec(1:end-2); zeros(size(Z_ec(1:end-2)))]; % r_tan vector for each slice, non-normalised
r_tan_0 = [Z_ec(2)-Z_ec(1); X_ec(2)-X_ec(1); 0];
r_tan_n = [Z_ec(end)-Z_ec(end-1); X_ec(end)-X_ec(end-1); 0];
r_tan = [r_tan_0, r_tan, r_tan_n];

for i_rtan = 1:size(r_tan,2) % normalisation of r_tan vector
    r_tan(:,i_rtan) = r_tan(:,i_rtan) ./ norm(r_tan(:,i_rtan), 2);
end

% bn_corr computation slice by slice :
bn_corr = zeros(Nec,1,length(ti));
for i_slice = 1:Nec
    for i_ti = 1:length(ti)
    bn_corr(i_slice,1,i_ti) = norm(cross(ri(:,i_ti),r_tan(:,i_slice)),2); % be careful because ri and r_tan dimensions are different
    end
end
bn_corr = repmat(bn_corr,1,length(f),1);

% ri * r_pos vector product computation, slice by slice :
ri_r_pos_product = zeros(Nec,1,length(ti));
for i_slice = 1:Nec
    for i_ti = 1:length(ti)
    ri_r_pos_product(i_slice,1,i_ti) = ri(:,i_ti)'*r_pos(:,i_slice); % be careful because ri and r_tan dimensions are different
    end
end
ri_r_pos_product = repmat(ri_r_pos_product,1,length(f),1);

k_w = 2*pi/c0*f; % wave number (m^-1) in water
kb_w = b_ec'*k_w; % wave number (m^-1) in water * cylinder radius
kb_w = repmat(kb_w,1,1,length(ti));
if h == 0 % soft cylinder
    kb_c = zeros(size(kb_w));
else
    kb_c = kb_w / h; % wave number (m^-1) in cylinder * cylinder radius
end


[Slice, F, PHI ] = ndgrid(1:Nec, f, ti); % slice, f, ti

k_w_extend = 2*pi/c0*F;

L0 = 1; % arbitral swimbladder length for computation : 1m
% As nTS depends only upon ka, ab and tilt angle ; L0 value does not matter


% Approximate limits on the summations (TO ADJUST)

n_max = 30; %ceil(2*k0*a);


% Terms in the summation smaller than 'tol' will cause the iteration to
% stop.
tol = -200; % [dB] 
tsx = tol+1;


full_modal_serie = zeros(size(k_w_extend)); % dim : slice*f*ti

n = -1; % infinite sum index
while (n < n_max) && (tsx > tol) % modal serie computation for i_gamma
    n = n+1;
    % bn computation(i_gamma,n)
    % you have to correct the wavenumbers for bn 
    Bn = bn(kb_w.*bn_corr,kb_c.*bn_corr,g,h,n,1);

    exp_term = 1i*2*k_w_extend.*ri_r_pos_product;
    modal_serie_Nterm = ((-1)^n)*Bn.*exp(exp_term); % formula 26 from Stanton1989
    tsx = max(20*log10(abs(modal_serie_Nterm(:)))); 
    % on incremente MS(i_gamma)
    full_modal_serie = full_modal_serie + modal_serie_Nterm;
end

AllElementSum = sum(full_modal_serie.*dr_pos,1); % dim : f*ti

% T1 is now the full integral (complex)

f_BS = -1i/pi*AllElementSum; % formula 8 from Stanton 1989
nf_BS = f_BS/L_shape;
nsigma_BS = abs(nf_BS).^2;


% nTS = 10*log10(nsigma_BS);
% nTS = squeeze(nTS);
% 
% figure
% plot(nTS(:,1)); grid on;



end



