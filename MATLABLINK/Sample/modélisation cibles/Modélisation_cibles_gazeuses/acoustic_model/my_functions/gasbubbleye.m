% Ye formula for gas bubble resonance, from Kloser et al. 2002
%  gasbubbleye.m
% ------------------------------------------------------------
%   Modelled TS ( dB and arithmetic) and reduced TS gas bubble model
%--------------------------------------------------------------------------
% Input  :
%   fx       = vector of frequencies
%   tabSize  = vector of sizes
%   D        = depth of the bladder in meter

% Output :
%   TS       = Target Strength in dB
%   TSreduce = Reduced target strength
%   TAij     = Target Strenght in arithmetic values
%--------------------------------------------------------------------------
% References :
%   Ye et al.
%--------------------------------------------------------------------------
% Version written for Matlab 11  for PC
%--------------------------------------------------------------------------
% Original : 15/10/2013 B. Remond
%--------------------------------------------------------------------------

function [TS, TSreduce, TAij] = gasbubbleye(kL0, ti0,f0,L_b0,D,elongation,rhoWater,cWater,gamma,eta,tau)

if nargin<10
   eta=2.4*1e-5*10^(247.8/(10+273.15-140)); %eau � 10�C
   tau=75*10^-3; %eau � 10�C
end

        ro = rhoWater ; % water density in g.m-3
c = cWater;
P = (1 + 0.1*D) * 1e5 ; % pressure at the D depth 


ti0 = ti0 * pi/180; % incident angle in radians
[kL_w, L_b, ti] = ndgrid(kL0, L_b0, ti0); % kL x L/b x theta
[f, L_b, ti] = ndgrid(f0, L_b0, ti0); % kL x L/b x theta

% kL_w = reshape(kL_w, length(kL0), length(L_b0), length(ti0));


% for ifreq = 1:length(fx)    % boucle sur les fr�quences
%     for ia = 1:length(tabSize)  % boucle sur les tailles


        aes = L_b; % tabSizeM in meters is the input
        eratio = 1./elongation ; % ratio of the minor semi-axis to the major semi-axis
        epsilon=(1-eratio.^2).^(1/2);
        %         aes=aes*(1/(1+0.1*D))^(1/3);
        
        
        L = 2 * ((aes.^3)./(eratio.^2)).^(1/3) ;
        k=(2*pi*f/c);
        
        if (epsilon~=0)
            f0 = (1./(2*pi.*aes.*eratio.^(1/3))) .* ((3*gamma*P+(2*tau./aes).*(3*gamma-1))/ro).^(1/2) .* ((log((1 + epsilon)./eratio)./epsilon).^(-1/2)); % spherical resonant frequency at a
            CsteQ = eratio.^(-2/3).*((log((1 + epsilon)./eratio)./epsilon)).^(-1);
        else
            f0 =  (1./(2*pi*aes)) * ((3*gamma*P)/ro)^(1/2) ;
            CsteQ = 1;
        end
        %ajout du damping thermique et du � la viscosit� ignor� dans le paier
        %de Ye 1997 car n�gligeable aux tailles de bulles consid�r�es
        %le calcul de ce damping vient du papier de Ainslie et Leighton 2011
        % 'Review of scattering and extinction cross-sections, damping
        % factors, and resonance frequencies of a spherical gas bubble'
        % les param�tres physiques de diffusivit� thermique et de viscosit�
        % sont � passer en param�tres
        Dp=1.9e-5; % thermal diffusivity
        lth=sqrt(Dp./(2*c*k)); %thermal diffusion length eq8
        X=aes./lth; %thermal diffusion ratio eq14
        G=gamma./(1-(((1+1i).*X/2./tanh((1+1i).*X/2)-1).*(6*1i*(gamma-1)./X.^2))); %polytropic complex index eq16
        betath_omega=3*(P+2*tau./aes).*imag(G)./(2*ro*c^2*(k.*aes).^2); %eq92
%         eta=2.4*1e-5*10^(247.8/(15+273.15-140)); %shear viscosity
        betavis_omega=2*eta./(ro*c*aes.*(k.*aes)); %eq91
        beta_omega=betath_omega+betavis_omega;
%         beta_omega=0;
% X = sprintf('%.2f %.2f %.2f',betath_omega,betavis_omega,CsteQ*k*aes);
% disp(X)

        Cstedenom = 4;%(2/(eratio^(2/3)))^2 ;
    
        delta_ = kL_w.*cos(ti);
        sinc_ = sin(delta_) ./ delta_;
        sinc_(delta_ == 0) = 1;
        sinc_=1;
    
        TAij = ((CsteQ./Cstedenom).*(2*aes).^2)./((((f0./f).^2 - 1-2*beta_omega.*k.*aes).^2 +(CsteQ.*(f0./f).^2.*k.*aes+2*beta_omega).^2)).*( sinc_.^2);
        sigmaBSreduce = TAij ./ (L).^2;
        TS = 10 * log10(TAij) ;
        TSreduce = 10*log10(sigmaBSreduce) ;
  
%         clear f aeq f0 fp a sigmaBSreduce ;
        
%     end
% end


end

