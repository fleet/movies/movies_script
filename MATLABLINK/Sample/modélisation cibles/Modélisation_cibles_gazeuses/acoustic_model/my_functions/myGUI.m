
function myGUI
h.f = figure; % Create figure
h.bg = uibuttongroup(h.f,'Title','Check the model(s) you want to use:',... % Create button group
            'Position',[.1 .2 .8 .6]);
% bg = uipanel(f,'Title','My Panel',...
%              'Position',[.25 .1 .5 .8]);
h.rb(1) = uicontrol(h.bg,'style','checkbox','units','normalized',...
                'position',[.1 .10 .3 .2],'string','Straight cylinder');
h.rb(2) = uicontrol(h.bg,'style','checkbox','units','normalized',...
                'position',[.1 .8 .3 .2],'string','Bent cylinder');    
h.rb(3) = uicontrol(h.bg,'style','checkbox','units','normalized',...
                'position',[.1 .6 .3 .2],'string','KRM');    
h.rb(4) = uicontrol(h.bg,'style','checkbox','units','normalized',...
                'position',[.1 .4 .3 .2],'string','Ellipsoid');    
% Create OK pushbutton   
h.p = uicontrol(h.f,'style','pushbutton','units','normalized',...
                'position',[.4 .2 .3 .2],'string','OK',...
                'callback',@p_call);

%     % Pushbutton callback
function p_call(varargin)
    vals = get(h.rb,'Value');
    checked = find([vals{:}]);
    if isempty(checked)
        checked = 'none';
    end
    disp(checked)
end
end