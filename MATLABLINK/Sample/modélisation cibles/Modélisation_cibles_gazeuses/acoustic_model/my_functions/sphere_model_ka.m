function sigma_BS = sphere_model_ka( f0, a0, ti0, g, c0, c1, SphereType )



%% Parameters computation
% angles convention detailled in figure 1 from Ye1997 and in the last paragraph of page 4

ti0 = ti0 * pi/180; % incident angle in radians

% General formula : formula 13b and 13c from Sage1979
% Fluid Sphere (formula 11 from Sage1979)
% Solid Sphere (formula 12a from Sage1979)
% Soft Sphere (formula 12b from Sage1979)

[f, a, ti] = ndgrid(f0, a0, ti0); % kL x L/b x theta

% wave number (m^-1)
k0 = 2*pi*f ./ c0; % outside the cylinder
k1 = 2*pi*f ./ c1; % inside the cylinder

x0 = k0.*a; % outside the cylinder with incidence direction
x1 = k1.*a; % inside the cylinder with incidence direction

% Approximate limits on the summations (TO ADJUST)
m_max = 30; 

% Terms in the summation smaller than 'tol' will cause the iteration to
% stop.
tol = -200; % [dB] 

f_BS = 0.0;
n = -1; % infinite sum index
tsx = tol+1;

while (tsx > tol) && (n < m_max)
    n = n+1; % modal serie counter
    
    An = an(x0,x1,g,n,SphereType);
    Pn = legendre(n,cos(ti));
    if n > 0 
        Pn = Pn(n,:,:);
        Pn = reshape(Pn, size(f));
    end

    % Formula 28 from Stanton1988 equivalent to formula 15 from Ye1997 :
    t_sum = (1./k0).*(2*n+1).*An.*Pn;
    
    f_BS = f_BS + t_sum;

    tsx = max(20*log10(abs(t_sum(:) ))); % FORMULE A CONFIRMER

end

sigma_BS = abs(f_BS).^2;

end

