close all
clear *

% Soft swimbladder
g  = 0.00129; % relative mass density
c0 = 1483;    % sound velocity (m/s) outside the cylinder
c1 = 340;     % sound velocity (m/s) inside the cylinder
h = c1/c0;



% Modelled TS : over potential lengths (8cm - 25cm), potential frequencies (18kHz - 200kHz), potential tilt angles (15� - 165�)
L_b_sb = (12:13)*2; % ratio of the swimbladder length and the swimbladder radius
Lfish2Lsb = 0.387; % ratio of the swimbladder length and the fish length : empirical parameter estimated from Loic report

% Potential values of L, f and theta :
% length(L) dependency
Lf_all = (8:0.1:25)*1e-2; % fish length (m)
Lsb_all = Lfish2Lsb * Lf_all; % swimbladder length (m)

% frequency(f) dependency
f_all = (18:1:200)*1e3; % from 18kHz to 200kHz
% tilt angle(ti) dependency
% first hypothesis : the fishes are swimming in the crosstrack vessel direction
ti_all = 90:91; % beam angle crosstrack the vessel : tilt angle (�)

% kL and kb potential values (assuming ab_sb constant) :
kL_all = (2*pi/c0) * Lsb_all.' * f_all; % wave number x swimbladder length

% Value of fixed parameters
kL_all = round(min(kL_all(:))):0.01:round(max(kL_all(:))); % wave number x swimbladder length


% INPUTS
kL0  = kL_all;
L_b0 = L_b_sb;
ti0  = ti_all;
model = 1; % Stanton1988

%f_BS : computes the reduced scattering function for a cylinder of finite length L0

%   INPUT
% - L0    : cylinder length (m)
% - L_b0  : the dimensionless ratio for longitudinal length (L) by cross-sectional radius (b)
% - f0    : acoustic wave frequency (Hz)
% - h     : relative sound velocity
% - g     : mass density dimensionless ratio
%                         g=rho_c/rho_w with :
%                             - rho_c the mass density insider the cylinder
%                             - rho_w the mass density outside the cylinder
% - ti0   : incident angle (�) (convention detailled in figure 1 from Ye1997)
% - model : integer value from 1 to 5:
%     1 : Stanton1988 scattering function for fluid cylinder of finite length
%         - formula 28 from Stanton1988 | formula 15 from Ye1997
%         - hypothesis :
%             - no absorption, dispersion or nonlinearities
%                in the cylinder or surrounding medium
%             - fluid cylinder (does not support shear waves)
%             - does not take into account end effects 
%                (considered as not important), so internal field
%                can be approximated by the internal field solution
%                of an infinite cylinder
%     2 : Stanton1988 scattering function for solid cylinder of finite length
%         - formula 37 from Stanton1988
%         - high frequencies (k*sin(ti)*b >> 1)
%     3 : Ye1997 formula 12 : model series
%     4 : Ye1997 scattering end effects function for fluid cylinder of finite length
%         - formula 18 from Stanton1988
%         - only the end effect, not the body contribution
%     5 : Do&Surti1990 scattering function for solid cylinder of finite length
%         - formula 8 from Surti1990 (sin^2 should be a sin)
%         - high frequencies (k*sin(ti)*b >> 1)
%   OUTPUT
%           - sigma_BS  : reduced (ie. length-normalised) backscattering cross section function


%% Parameters computation
% angles convention detailled in figure 1 from Ye1997 and in the last paragraph of page 4
% b0 = (L0./2) ./ ab0; % cylinder radius (m)

L0 = 1; % arbitral swimbladder length for computation : 1m
% As nTS depends only upon ka, ab and tilt angle ; L0 value does not matter
model_str = {'Fluid cylinder of finite length - modal serie - Stanton1988', ...
    'Rigid cylinder of finite length - cardinal sinus - Stanton1988', ...
    'Fluid cylinder of finite length - modal serie - Ye1997', ...
    'Fluid cylinder of finite length - fend - Ye1997', ...
    'Fluid cylinder of finite length - cardinal sinus - DoSurti1990'};

ti0 = ti0 * pi/180; % incident angle in radians
[kL_w, L_b, ti] = ndgrid(kL0, L_b0, ti0); % L x f x theta
idx_kL47 = find(kL0 == 47);

% kL_w : wave number (m^-1) in water * cylinder length
% kb_w : wave number (m^-1) in water * cylinder radius
% kb_c : wave number (m^-1) in cylinder * cylinder radius

kb_w = kL_w ./ L_b; % coefficient 2 comes from diameter=2*radius
kb_c = kb_w / h;


% L = L0 * ones(size(kL_w));
% f = kL_w * c0 ./ (2*pi*L0);
% b = L ./ ab;%(2*ab);

ts = pi - ti; % scattering angle in radians
ps = pi;  % azimuthal angle in radians

% wave number (m^-1)
% k0 = 2*pi*f ./ c0; % outside the cylinder
% k1 = 2*pi*f ./ c1; % inside the cylinder


x0 = kb_w .* sin(ti); % outside the cylinder with incidence direction
x1 = kb_c .* sin(ti); % inside the cylinder with incidence direction
% x2 = k0 .* b .* sin(ts); % outside the cylinder with scattering direction

% local parameter : formula 13 from Ye1997
delta = kL_w .*(cos(ts)-cos(ti)) ./ 2;
sinc_ = sin(delta) ./ delta;
sinc_(delta == 0) = 1; % the sinc(x) equals to 1 when x=0

% Approximate limits on the summations (TO ADJUST)
m_max = 6; %ceil(2*k0*a);

% Terms in the summation smaller than 'tol' will cause the iteration to
% stop.
tol = -200; % [dB] 

nf_BS = 0.0;
n = -1; % infinite sum index
tsx = tol+1;

kL_save = [];
angles_save = [];

i_theta90 = 1;
i_Lb24 = 1;

while (tsx > tol) && (n < m_max) && any(model == [1 3])
    n = n+1;
    % epsilon is the Neumann factor, a rather obtuse way of saying this:
    if n == 0
        eps = 1;
    else
        eps = 2;
    end

    % Bessel functions
    J_x0 = besselj(n,x0); % n-th order Bessel function if the first kind
%     Jn_x0_save{n+1} = Jn_x0;
    Jnn_x0 = besselj(n+1,x0);
%     Jnn_x0_save{n+1} = Jnn_x0;
    Jnnn_x0 = 2*(n+1) .* Jnn_x0 ./ x0 - J_x0; % fin page 6 : http://g.boutte.free.fr/cours/Bessel.pdf
    
    Nn_x0 = bessely(n,x0); % n-th order Bessel function of the second kind
    Nnn_x0 = bessely(n+1,x0);
    NN_x0 = n*Nn_x0./x0 - Nnn_x0;
    
    JJ_x0 = n*J_x0./x0 - Jnn_x0;
%     JJJ_x0 = Jnnn_x0 - (2*n-1)*Jnn_x0./x0 + (n*(n-1)./(x0.^2)).*Jn_x0;
    JJJ_x0 = (n./x0.^2).*JJ_x0 - (n./x0.^2).*J_x0 - ((n+1)./x0).*Jnn_x0 + Jnnn_x0; 
    J_x1 = besselj(n,x1); % n-th order Bessel function of the first kind
    JJ_x1 = n*J_x1./x1 - besselj(n+1,x1);
    
%     J_x2 = besselj(n,x2); % n-th order Bessel function of the first kind
%     JJ_x2 = n*J_x2./x2 - besselj(n+1,x2);
    % Hankel functions
    H_x0  = besselh(n,1,x0); % Hankel function of the first kind
    HH_x0 = n*H_x0./x0 - besselh(n+1,1,x0);
    
    % Terms used in publication Stanton1988 :
    if model == 1
        Cn_num = (JJ_x1.*Nn_x0)./(J_x1.*JJ_x0)-g*h*(NN_x0./JJ_x0);
        Cn_den = (JJ_x1.*J_x0)./(J_x1.*JJ_x0)-g*h;
        Cn = Cn_num ./ Cn_den; % formula 12 from Stanton1988
        Bn = -eps*1i^n ./ (1+1i*Cn);
    
    % Terms used in publication Ye1997 :
    elseif model == 3
        beta_ = g*h*J_x1./JJ_x1; % formula 11 from Ye1997
        Bn_num = -eps*1i^n*(J_x0-beta_.*JJJ_x0);
        Bn_den = H_x0-beta_.*HH_x0;
        Bn = Bn_num ./ Bn_den; % formula 11 from Ye1997
    end
    
%     figure(1)
%     plot(squeeze(real(Bn(1,1,1:50)))); hold on;
%     plot(squeeze(imag(Bn(1,1,1:50))));
    
%     seuil = 1e-40;
%     beta_save(n+1,1:7) = beta_(idx_kL47-3:idx_kL47+3,1,75).';
%     if any(abs(J_x1(:)) < seuil) && any(abs(JJ_x1(:)) < seuil)
%         disp(strcat('beta, 0/0, mode:',num2str(n)));
%         kL_with_artefacts = kL_w(abs(J_x1) < seuil & abs(JJ_x1) < seuil);
%         kL_with_artefacts = kL_with_artefacts(kL_with_artefacts > 0);
%         kL_save = [kL_save, kL_with_artefacts];
%         angles_with_artefacts = ti(abs(J_x1) < seuil & abs(JJ_x1) < seuil);
%         angles_with_artefacts = angles_with_artefacts(angles_with_artefacts > 0);
%         angles_save = [angles_save, angles_with_artefacts];
%     elseif any(abs(JJ_x1(:)) < seuil)
%         disp(strcat('beta, X/0, mode:',num2str(n)));
%     end
%     if any(abs(Bn_num(:)) < seuil) && any(abs(Bn_den(:)) < seuil)
%         disp(strcat('Bn, 0/0, mode:',num2str(n)));
%     elseif any(abs(Bn_den(:)) < seuil)
%         disp(strcat('Bn, X/0, mode:',num2str(n)));
%     end
    
    % Model choice :
    t_sum = (-1i*L0/pi).*sinc_.*Bn.*(-1i)^n*cos(n*ps); % formula 15 from Ye1997 : used for model 1
    
    
    figure; hold on; grid on;
%     subplot 211
    
%     title(strcat('Resonance kL values for mode n�',num2str(n),' (theta=90�, ab=12) - formula 11 from Ye1997')); 
    title('R�sonances du cylindre fluide (n=0, ab=12, incidence normale)')
    plot(kL0/2,J_x1(:,i_Lb24,i_theta90), 'k:', 'LineWidth', 1);
    plot(kL0/2,JJ_x1(:,i_Lb24,i_theta90), 'k--', 'LineWidth', 1);
    plot(kL0/2,abs(Bn(:,i_Lb24,i_theta90)),'k-', 'LineWidth', 1);
    plot(kL0/2, 0*kL0,'Color',0.7*[1 1 1],'LineWidth', 1)
    xlabel('ka');
    legend(strcat('J_{',num2str(n),'}(k_1b sin(90�))'),strcat('J_{',num2str(n),'}''(k_1b sin(90�))'),strcat('|B_{',num2str(n),'}(k_1b sin(90�))|'),'Orientation','horizontal')
    set(gca,'PlotBoxAspectRatio',[4 1 1])
%     subplot 212
%     figure(500); hold on;
%     plot(kL0,10*log10(abs(t_sum(:,i_Lb24,i_theta75)).^2/L0^2), '-'); grid on;
%     xlabel('kL'); ylabel('n^{th} term in dB');
%     title('n^{th} term of modal serie (formula 15 from Ye1997)');
    
    
    nf_BS = nf_BS + t_sum;
    


    tsx = max(20*log10(abs(t_sum(:) ))); % FORMULE A CONFIRMER

end

figure(600); hold on;
plot(kL0,10*log10(abs(nf_BS(:,i_Lb24,i_theta90)).^2/L0^2), '-'); grid on;
xlabel('kL'); ylabel('full modal serie nTS in dB');
title('full modal serie (formula 15 from Ye1997)');


% figure; hold on;
% for ii = 1:7
% plot(beta_save(:,ii));
% end
% legend('1','2','3','4','5','6','7')
% figure
% histogram(kL_save,25)
% figure
% histogram(angles_save*180/pi,25)
%  




