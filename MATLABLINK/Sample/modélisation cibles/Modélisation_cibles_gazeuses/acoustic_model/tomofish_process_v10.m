
% V7.00
% updates :
% - angle convention according to a direct frame : positive from Z to X and negative from X to Z
% - allow you to work with one frequency
% - plot at fullscreen size
% V8.00
% - deformed cylinder model is validated
% - deformed cylinder model is validated

clear *; close all;
dbstop if error
addpath(genpath(pwd)) % add path for subfolder 'my_functions' and for input data (swimbladder *.stl)
cd(fileparts(mfilename('fullpath'))); % set the current Matlab folder = script directory

scrsz = get(groot,'ScreenSize');
% Change default axes fonts.
set(0,'DefaultAxesFontName', 'Arial')
set(0,'DefaultAxesFontSize', 14)
% Change default text fonts.
set(0,'DefaultTextFontname', 'Arial')
set(0,'DefaultTextFontSize', 16)
% Change default line width.
set(0,'defaultlinelinewidth', 1)


%% Prompt user for fish parameters
prompt = {'Relative mass density:', ...
    'Water soundspeed (m/s):', ...
    'Swimbladder soundspeed (m/s):', ...
    'Depth (m)',...
    'Frequencies (kHz):', ...
    'Incidence angles (�):',...
    'Viscosit� (Pa.s):',...
    'Tension superficielle (N/m):'};
name = 'Inputs for fish and water parameters';
numlines = 1;
% defaultanswer = {'0', '1483', '0', '0', 'logspace(log10(0.1),log10(500),1000)', '-50:1:50','10','10'};
% defaultanswer = {'0', '1483', '0', '0', 'logspace(log10(10),log10(120),1000)', '-50:2:50','0','0'};
% defaultanswer = {'0', '1483', '0', '[10:1:200]', '-50:0.5:50'};
% defaultanswer = {'0', '1483', '0', 'logspace(log10(0.01),log10(200),100)', '-50:2:50'};
%  defaultanswer = {'0.00129', '1483', '340', '0', 'logspace(log10(0.01),log10(500),1000)', '-50:1:50','10','0'};
 defaultanswer = {'1.043', '1483', '1557', '15', 'logspace(log10(10),log10(5000),1000)', '-50:0.1:50','0.0013','75e-3'};
%  defaultanswer = {'0', '1483', '0', '50', '[18 38 70:2:120 200]', '-50:1:50','0.5','75e-3'};

answer = inputdlg(prompt,name,numlines,defaultanswer);

% If user pressed cancel
if isempty(answer)
    error('User pressed cancel')
end

% Parse out inputs to variables
g = str2double(answer{1});    % relative mass density
c0 = str2double(answer{2});    % water soundspeed (m/s)
c1 = str2double(answer{3});    % swimbladder soundspeed (m/s)
D=str2double(answer{4});
freqs = eval(answer{5});    % echosounder frequencies (kHz)
freqs = freqs * 1e3; % Hz
% fish body tilt according to vertical axis (acoustic wave incidence = 0�) :
tif_MyFrame = eval(answer{6});
eta=str2double(answer{7});    % viscosit� (Pa.s)
tau=str2double(answer{8});    % tension superficielle (N/m)
% Transfer into local fish frame for TS computation :
tif_KRM_Frame  = 90 + tif_MyFrame;
tif_St88_Frame = 90 - tif_MyFrame;
tif_Fu88_Frame = 90 - tif_MyFrame;

g=g*(1+0.1*D);
h  = c1/c0;   % relative sound speed
Nfreq = length(freqs);
Ntif  = length(tif_MyFrame);


% Choice of the dataset
i_dataset = menu('Which set of data use ?','Sardines from Aquarium La Rochelle2 (13/11/2014)', ...
    'Herrings from La Manche - January 2013 - depth < 30m', ...
    'Sardines from Pelgas2014 (juillet 2014)', ...
    'Sardines from Kanadeven (02/10/2014)', ...
    'Sardines from Kanadeven (06/10/2014)', ...
    'Sardines from Pelgas2014 (15/06/2015)', ...
    'Inflated sardines', ...
    'Deflated sardines', ...
     'Anchois from Aquarium La Rochelle2 (24/02/2016)', ...
     'Partially deflated sardines', ...
     'All sardines', ...
    'Simulated fishes'    );
datasetFileName = {'AqLR2_caracteristics3.txt', 'Manche2013_caracteristics.txt', 'Pelgas14Juillet2014_caracteristics.txt','Pelgas142octobre2014_caracteristics.txt','Pelgas146octobre2014_caracteristics.txt','Pelgas14Juin2015_caracteristics.txt','sardinegonfle_caracteristics.txt','sardinedegonfle2_caracteristics.txt','AqLR3_caracteristics_g.txt','sardinedegonfle3_caracteristics.txt','Sardineall_caracteristics.txt', 'VirtualFish_caracteristics.txt'};
KRMFileNamePrefix = {'sardine_AqLR2_','herring_Manche2013_','sardine_Pelgas14Juillet2014','sardine_Pelgas142octobre2014','sardine_Pelgas146octobre2014','sardine_Pelgas14Juin2015','sardinegonfle_','sardinedegonfle_','anchois_AqLR3_','sardinemigongle_','sardineall_',''};
KRMFilePath = 'in/InputDataKRM/'; % path of input geometry files for KRM script
close;

%% Load of fish dataset

if i_dataset == 0 % if the user close the multiple-choice dialog box,
    error('User pressed cancel') % the script is stopped
elseif i_dataset < 12
    % Dataset : Tomofish sardines from La Rochelle
    fishDataSet = importdata(datasetFileName{i_dataset});
    fishDataSet = fishDataSet.data.';
    fish_index = fishDataSet(1,:);
    Ln = fishDataSet(3,:)*1e-2; % bladder length (m)
    as = fishDataSet(11,:)*1e-2; % bladder radius (m) (Constant area)
    as2= fishDataSet(12,:)*1e-2; % bladder radius (m) (Constant area)
    asbis=fishDataSet(14,:)*1e-2; % bladder radius (m) (mean radius)
    
    surface= fishDataSet(4,:)*1e-4;
    volume= fishDataSet(5,:)*1e-6;
    
    bladder_tilt_up = fishDataSet(7,:); % bladder tilt (�), >0
    bladder_tilt_c = fishDataSet(8,:); % bladder tilt (�), >0
    bladder_tilt = bladder_tilt_up; % bladder tilt (�), >0
    
    Ln_fish_cm  = fishDataSet(2,:); % fish length (cm)
    Nfish = size(fishDataSet,2); % number of fish in dataset
    Lb_distrib  = Ln ./ as; % 2*flattening
    Lb_distrib2 = Ln ./ as2;
    Lb_distrib3=sqrt(((4/3)*pi*Ln.^3)./volume); % 2*flattening ellipsoid with same volume
    
    % bent bladder parameters
    Ln_bc = fishDataSet(10,:)*1e-2; % bent bladder length (m)
    gamma_max = abs(fishDataSet(9,:)); % 2*ones(1,Nfish)
    
%     masse=fishDataSet(15,:); % masse (g)
    
    disp('ok');
    
elseif i_dataset == 12
    % Dataset parameters :
    Nfish0 = 1;
%     param_Lf      = [17, 1.6]*1e-2; % fish length (m) : mean, std
%     param_Lf_Lsb  = [2.58, 0.09];   % fish length / bladder length ratio : mean, std
%     param_sbFlat  = [12.4, 0.6];    % bladder flattening : mean, std
%     param_sbTilt  = [6.8, 0.8];   % bladder tilt in � : mean, std
% %     param_sbTilt = [7, 0];
%     param_sbGamma = [6.8, 2.3];  % bladder curvature in � : mean, std
% %     param_sbGamma = [57, 0];

%     param_Lf      = [17.4, 0.5]*1e-2; % fish length (m) : mean, std
%     param_Lf_Lsb  = [2.58, 0.09];   % fish length / bladder length ratio : mean, std
%     param_sbFlat  = [22.9, 0.7];    % bladder flattening : mean, std
%     param_sbTilt  = [8.4, 1.9];   % bladder tilt in � : mean, std
% %     param_sbTilt = [7, 0];
%     param_sbGamma = [7, 1.6];  % bladder curvature in � : mean, std


%      param_Lf      = [13, 0.5]*1e-2; % fish length (m) : mean, std
%     param_Lf_Lsb  = [3.5, 0.15];   % fish length / bladder length ratio : mean, std
%     param_sbFlat  = [10.1, 0.5];    % bladder flattening : mean, std
%     param_sbTilt  = [4.2, 0.9];   % bladder tilt in � : mean, std
% %     param_sbTilt = [7, 0];
%     param_sbGamma = [0, 0];  % bladder curvature in � : mean, std
% %     param_sbGamma = [57, 0];

     param_Lf      = [5, 0]*1e-2; % fish length (m) : mean, std
    param_Lf_Lsb  = [1, 0];   % fish length / bladder length ratio : mean, std
    param_sbFlat  = [1.01, 0];    % bladder flattening : mean, std
    param_sbTilt  = [0, 0];   % bladder tilt in � : mean, std
%     param_sbTilt = [7, 0];
    param_sbGamma = [0, 0];  % bladder curvature in � : mean, std
%     param_sbGamma = [57, 0];

    fishDataSet = GenerateDataset( Nfish0, param_Lf, param_Lf_Lsb, param_sbFlat, param_sbTilt, param_sbGamma );
    fishDataSet = round(fishDataSet*1e4) ./ 1e4; % double numbers to 4 decimal
    % Create table
    table_ = array2table(fishDataSet,...
        'VariableNames',{'FishIndex', 'FishLength', 'LengthRatio', 'BladderFlattening', 'BladderTilt', 'CurvatureAngle'});
    
    writetable(table_,strcat('in/',datasetFileName{i_dataset}),'Delimiter',' ')
    
    Nfish = size(fishDataSet,1); % fish number refresh after PDF 95% truncature
    fish_index = fishDataSet(:,1); % fish index
    Ln_fish_cm = fishDataSet(:,2)*1e2; % fish length (cm)
    Ln = fishDataSet(:,3)'; % bladder length (m)
%     Ln_bc = 1.018*Ln; % bent bladder length (m)
    Ln_bc=Ln;
    bladder_tilt = fishDataSet(:,5); % bladder tilt (�), >0
    gamma_max = fishDataSet(:,6); % bladder curvature angle (�)
    Lb_distrib = 2*fishDataSet(:,4)'; % 2x bladder flattening
    as = Ln ./ Lb_distrib; % bladder radius (m)
    aplat=Lb_distrib/2;
    %on recherche l'aplatissement de l'ellipsoide de m�me surface que le
    %cylindre 
    if aplat>10
        for aplat2=5:0.01:aplat
            diff=(Ln*((1+(aplat2*asin(sqrt(1-(1/aplat2)^2))/sqrt(1-(1/aplat2)^2)))/(4*aplat2^2))-as);
            if(diff<0)
                Lb_distrib2=2*aplat2;
                break;
            end
        end
        Lb_distrib2=Lb_distrib2*ones(1,length(Lb_distrib));
        as2 = Ln./Lb_distrib2;
    else 
        Lb_distrib2=Lb_distrib;
        as2=as;
    end
    
%     surface= pi*as.*Ln;
    volume= 4/3*pi*as.^2.*Ln/2;
    aeq=(3*volume/(4*pi)).^(1/3)
    
    Lb_distrib3=Lb_distrib; % 2*flattening ellipsoid with same volume
    
%     %bulles de fond de mer
%     Nfish0=20;
%     nbulles=Nfish0;
%     mu=1.14;  %en mms
%     sigma=0.32;  %en mm
%     deltaa=0.05;  %en mm
%     tmin=0.05; %en mm
%     tmax=10;  %en mm
%     ad=(tmin:deltaa:tmax); %a en mm
%     int1=(log(ad)-mu).^2;
%     db=exp(-int1/(2*sigma^2))./(ad*sigma*sqrt(2*pi));
%     db=db/sum(db);
%     sd=cumsum(db); %somme cumul�e du nombre de bulles
%     ai=zeros(1,nbulles);
%     rng('default');
%     tir=rand(1,1,nbulles); %tirage al�atoire de nbulles bulles
%     for nt=1:nbulles
%         num=find(sd>tir(nt));
%         ai(nt)=ad(num(1)-1);
%     end
%     ais=sort(ai); %classe les tailles de bulles par ordre croissant
% %     [nb,a]=hist(ais,unique(ais)); %�limine les doublons et compte le nombre de fois o� la m�me taille appara�t
%     ap=1-(1.5/log(tmax/tmin))*log(tmin*1e-3)+(1.5/log(tmax/tmin))*log(ais*1e-3); %facteur d'applatissement
%     
%     Nfish = Nfish0; % fish number refresh after PDF 95% truncature
%     fish_index = [1:1:Nfish0]; % fish index
%     Ln_fish_cm = ais*0.1; % fish length (cm)
%     Ln = ais*10^(-3); % bladder length (m)
%     Ln_bc = Ln; % bent bladder length (m)
%     bladder_tilt = zeros(1,Nfish); % bladder tilt (�), >0
%     gamma_max = zeros(1,Nfish); % bladder curvature angle (�)
%     Lb_distrib = 2*ap; % 2x bladder flattening
%     as = Ln ./ Lb_distrib; % bladder radius (m)
%     Lb_distrib2 = Lb_distrib;
%     as2 = as;
end

%% User parameters :

% model index :
% 1 : KRM model (Clay&Horne1994)
% 2 : straight cylinder model (Stanton1988)
% 3 : bent cylinder model (Stanton1989a)
% 4 : ellipsoid model (Furusawa1988)
% 5 : deformed cylinder model (Stanton1989a)
% 4 : straight cylinder with end effect model (Ye1997)
i_model_KRM         = 1;
i_model_straightcyl = 2;
i_model_bentcyl     = 3;
i_model_ell         = 4;
i_model_deformedcyl = 5;
i_model_straightcyl_end_eff = 6;
i_model_Ye          = 7;
i_model_simplest    = 8;
model_desc = {'KRM'; 'Straight Cylinder'; 'Bent cylinder'; 'Ellipsoid'; 'Deformed cylinder'; 'Straight cylinder with end effect'; 'Ye low frequency model'; 'Medwin + sinc function model'};
mark_ = {'--','-','--','-.',':','--',':','-.'}; % LineStyle for each model : Stanton88, PSMS, KRM
facecolor_ = {'k',[0 0 1]*0.8,[1 0 0]*0.8,[0 1 0]*0.8,[1 1 1]*0.3,'c','m','g'}; % LineStyle for each model : Stanton88, PSMS, KRM
% Choice of scattering models :
% models_ON = [ i_model_straightcyl,i_model_straightcyl_end_eff];
models_ON = [i_model_bentcyl]; % i_model_KRM, i_model_straightcyl, i_model_bentcyl, i_model_ell, i_model_deformedcyl, i_model_straightcyl_end_eff
% models_ON = [  i_model_straightcyl, i_model_bentcyl, i_model_ell, i_model_deformedcyl,i_model_straightcyl_end_eff, i_model_Ye];
% models_ON = [ i_model_straightcyl,i_model_straightcyl_end_eff];
% models_ON = [ i_model_Ye];

% Normal distribution parameters : (acoustic wave incidence angle (�) in fish body frame)
Nang0  = 50; % number of fishes
% Values from Olsen1971 :
m_ta   = [0 -30]; % angle mean (�), <0 according to our orientation of reference
std_ta = [15 15]; % angle std (�)

% Frequency used for theta dependancy plot :
freq4ThetaPlot = [ 70 120 ]*1e3; % Hz
% freq4ThetaPlot = [1 5 7 10]*1e3; % Hz
% freq4ThetaPlot = [70]*1e3; % Hz
fish2plot = [1];

% Shape file choice :
ShapeGeometryList = {'real', 'straightcylinder', 'bentcylinder', 'ellipsoid'};
i_shape = 1;
ShapeGeometry = ShapeGeometryList{i_shape};
st = 1e-3; % slice thickness (m)

%% nTS computation for Tomofish data
% for each fish at different frequencies

% check if we try to use KRM model or deformed cylinder model with simulated data and real shape.
% If it is the case, we suppress that model :
if any(models_ON == i_model_KRM | models_ON == i_model_deformedcyl) && i_dataset == 7 && i_shape == 1
    models_ON(models_ON == i_model_KRM) = [];
    models_ON(models_ON == i_model_deformedcyl) = [];
end

Nmodel = length(models_ON);
Nmodel_max = length(model_desc); % total number of scattering models
nTS_dataset = zeros(Nfish,Nmodel_max,Nfreq,Ntif); % nTS for each fish according to frequency and tilt angle
fish_legend = cell(Nfish,1);
kL_distrib = zeros(Nfish,Nfreq);
kL_bc_distrib = zeros(Nfish,Nfreq);
tisb_St88_Frame = zeros(Nfish,Ntif); % fish swimbladder incidence angles (�)
tisb_Fu88_Frame = zeros(Nfish,Ntif); % fish swimbladder incidence angles (�)
tisb_St89_Frame = zeros(Nfish,Ntif); % fish swimbladder incidence angles (�)

disp('Fish currently processed :')
disp(strcat('0/',num2str(Nfish)));

for i_fish = 1:Nfish % loop on fishes
    % Legend writing :
    fishDesc_part1 = num2str(fish_index(i_fish));
    fishDesc_part2 = num2str(round(Lb_distrib(i_fish)/2*10)/10); % 1 decimal
    fishDesc_part3 = num2str(round(Lb_distrib2(i_fish)/2*10)/10); % 1 decimal
    fishDesc_part4 = num2str(round(Ln_fish_cm(i_fish)*100)/100); % 1 decimal
    fish_legend{i_fish,1} = strcat('Fish n�',fishDesc_part1,' - a/b_{cyl}=',fishDesc_part2,' - a/b_{ell}=',fishDesc_part3,' - L_f=',fishDesc_part4,'cm');
    
    % KRM file name (for real geometry and cylinder approximation) :
    KRMrealgeometry_filename = strcat(KRMFileNamePrefix{i_dataset},num2str(fish_index(i_fish)),'_KRMgeometry');
    KRMstraightcylinder_filename = strcat(KRMFileNamePrefix{i_dataset},num2str(fish_index(i_fish)),'_KRMstraightcylinder');
    KRMbentcylinder_filename = strcat(KRMFileNamePrefix{i_dataset},num2str(fish_index(i_fish)),'_KRMbentcylinder');
    KRMellipsoid_filename = strcat(KRMFileNamePrefix{i_dataset},num2str(fish_index(i_fish)),'_KRMellipsoid');
    % Building of geometric shapes :
    GenerateStraightCylinderForKRM([0 0 0], Ln(i_fish), 2*as(i_fish), bladder_tilt(i_fish), st, KRMFilePath, KRMstraightcylinder_filename);
    GenerateBentCylinderForKRM([0 0 0], Ln_bc(i_fish), as(i_fish), gamma_max(i_fish), bladder_tilt(i_fish), st, KRMFilePath, KRMbentcylinder_filename);
    GenerateEllipsoidForKRM([0 0 0], Ln_bc(i_fish)/2, as2(i_fish), bladder_tilt(i_fish), st, KRMFilePath, KRMellipsoid_filename);
    KRMFilenameList = {KRMrealgeometry_filename, KRMstraightcylinder_filename, KRMbentcylinder_filename, KRMellipsoid_filename};
    KRMFilename = KRMFilenameList{i_shape};
    
    % tif_St88_Frame(i_fish,:) = tif_MyFrame - bladder_tilt(i_fish);
    tisb_St88_Frame(i_fish,:) = tif_St88_Frame - bladder_tilt(i_fish);
    tisb_Fu88_Frame(i_fish,:) = tif_Fu88_Frame - bladder_tilt(i_fish);
    tisb_St89_Frame(i_fish,:) = tif_MyFrame + bladder_tilt(i_fish);
    
    % Cylinder models
    kL_distrib(i_fish,:) = (2*pi/c0)*Ln(i_fish)*freqs;
    kL_bc_distrib(i_fish,:) = (2*pi/c0)*Ln_bc(i_fish)*freqs;
    
    % if (0)
    % Straight fluid cylinder model : fluid cylinder of finite length without end effects (modal series Stanton88)
    % One fish : all frequencies x 1 flattening x all angles
    if any(models_ON == i_model_straightcyl)
        nsigma_St88_OneFish = cylinder_model_ka( kL_distrib(i_fish,:), Lb_distrib(i_fish), tisb_St88_Frame(i_fish,:), g, h, 1 ); % dim: Nfreq x length(L/b) x Ntif
        nTS_St88_OneFish = 10*log10(nsigma_St88_OneFish(:,1,:) ); % * Ln(i_fish)^2    dim: Nfreq x 1 x Ntif
        nTS_St88_OneFish = reshape(nTS_St88_OneFish,Nfreq,Ntif); % dim : Nfreq x Ntif
        nTS_dataset(i_fish,i_model_straightcyl,:,:) = nTS_St88_OneFish;
    end
    
    % PSMS model
    if any(models_ON == i_model_ell)
        nf_PSM_OneFish = PSM_model( freqs, Ln(i_fish)/2, as2(i_fish), 90+(0:2:50), c0, c1, g ); % dim: Nfreq x Ntif
        nTS_PSM_OneFish = 20*log10(abs(nf_PSM_OneFish));
        % nTS_PSM_OneFish = reshape(nTS_PSM_OneFish,v,Ntif); % dim : Nfreq x Ntif
        nTS_dataset(i_fish,i_model_ell,:,ceil(Ntif/2):end) = nTS_PSM_OneFish;
        nTS_dataset(i_fish,i_model_ell,:,1:ceil(Ntif/2)-1) = flip(nTS_PSM_OneFish(:,2:end),2);
%         
%         load('in/psms/nTS_PSMS.mat'); % PSMS model save
%         nTS_dataset(:,i_model_ell,:,:) = nTS_PSMS(1:8,:,:,:);
    end
    
    % Ye model low frequency
    if any(models_ON == i_model_Ye)
%         aeq= ((3/4)*Ln(i_fish)/2*(as(i_fish)^2))^(1/3); % rayon �quivalent pour une sph�re de m�me volume que le cylindre
%         aeq= (Ln(i_fish)/4*(as(i_fish)))^(1/2); % rayon �quivalent pour une sph�re de m�me surface que le cylindre
%                 aeq= (Ln(i_fish)/2*(as2(i_fish)^2))^(1/3); % rayon �quivalent pour une sph�re de m�me volume que le prolate spheroid
            aeq=(3*volume(i_fish)/(4*pi))^(1/3) % rayon �quivalent pour une sph�re de m�me volume que la vessie r�elle
            
%         aeq=as(i_fish);
        [~,nTS_Ye_OneFish,~] = gasbubbleye( kL_distrib(i_fish,:), tisb_St88_Frame(i_fish,:),freqs, aeq, D, Lb_distrib3(i_fish)/2, 1027, c0, 1.4, eta, tau ); % dim: Nfreq x Ntif
        nTS_Ye_OneFish = reshape(nTS_Ye_OneFish,Nfreq,Ntif); % dim : Nfreq x Ntif
        nTS_dataset(i_fish,i_model_Ye,:,:) = nTS_Ye_OneFish;
        
%                 [~,nTS_Ye_OneFish,~] = gasbubbleye2( freqs, aeq, 0, 1, 1027, c0, 1.4 ); % dim: Nfreq x Ntif
%         % nTS_PSM_OneFish = reshape(nTS_PSM_OneFish,v,Ntif); % dim : Nfreq x Ntif
%         nTS_dataset(i_fish,i_model_Ye,:,:) = repmat(nTS_Ye_OneFish,1,Ntif);
        
    end
    
        % Medwin+sinc model simple
    if any(models_ON == i_model_simplest)
%         aeq= ((3/4)*Ln(i_fish)/2*(as(i_fish)^2))^(1/3); % rayon �quivalent pour une sph�re de m�me volume que le cylindre
%         aeq= (Ln(i_fish)/4*(as(i_fish)))^(1/2); % rayon �quivalent pour une sph�re de m�me surface que le cylindre
%                 aeq= (Ln(i_fish)/2*(as2(i_fish)^2))^(1/3); % rayon �quivalent pour une sph�re de m�me volume que le prolate spheroid
                  aeq=(3*volume(i_fish)/(4*pi))^(1/3) % rayon �quivalent pour une sph�re de m�me volume que la vessie r�elle
%         aeq=as(i_fish);
        [~,nTS_Simplest_OneFish,~] = gasbubblesimple( kL_distrib(i_fish,:), tisb_St88_Frame(i_fish,:),freqs, aeq,D, Lb_distrib3(i_fish)/2, 1027, c0, 1.4); % dim: Nfreq x Ntif
        nTS_Simplest_OneFish = reshape(nTS_Simplest_OneFish,Nfreq,Ntif); % dim : Nfreq x Ntif
        nTS_dataset(i_fish,i_model_simplest,:,:) = nTS_Simplest_OneFish;
        
    end
    
    % KRM model
    if any(models_ON == i_model_KRM)
        sigma_KRM_OneFish = KRM_model_bladderOnly( freqs, tif_KRM_Frame, c0, c1, g, KRMFilePath, KRMFilename ); % dim: 1 x Nfreq x Ntif
        nTS_KRM_OneFish = 10*log10(sigma_KRM_OneFish ./ Ln(i_fish)^2); % dim: 1 x Nfreq x Ntif
        nTS_KRM_OneFish = reshape(nTS_KRM_OneFish,Nfreq,Ntif); % dim : Nfreq x Ntif
        nTS_dataset(i_fish,i_model_KRM,:,:) = nTS_KRM_OneFish;
    end
    
    % Bent fluid cylinder model
    if any(models_ON == i_model_bentcyl)
        nsigma_bc_OneFish = bentcylinder_model( kL_bc_distrib(i_fish,:), Lb_distrib(i_fish), tisb_St89_Frame(i_fish,:), g, h, gamma_max(i_fish) ); % dim: Nfreq x length(L/b) x Ntif
        nTS_bc_OneFish = 10*log10(nsigma_bc_OneFish(:,1,:)); %  * Ln(i_fish)^2, dim: Nfreq x 1 x Ntif
        nTS_bc_OneFish = reshape(nTS_bc_OneFish,Nfreq,Ntif); % dim : Nfreq x Ntif
        nTS_dataset(i_fish,i_model_bentcyl,:,:) = nTS_bc_OneFish;
        
         fid = fopen('Sardine.txt','wt');
         fprintf(fid,'# Diagramme de directivit� sardine � vessie gonfl�e\n');
         fprintf(fid,'# freq | phi | teta | TS\n');
         teta=0;
         for i=1:Nfreq
             for j=1:Ntif
                 for k=1:length(teta)
                     fprintf(fid,'%2.2f  %2.2f %2.2f %2.2f\n',kL_bc_distrib(i_fish,i),90+ tisb_St89_Frame(i_fish,j),teta(k),nTS_bc_OneFish(i,j));
                 end
             end
         end
         fclose(fid)
         
         figure;
h0=pcolor(tisb_St89_Frame(i_fish,:),kL_bc_distrib(i_fish,:),nTS_bc_OneFish)
% caxis([-150 0]);
 set(h0,'EdgeColor','none','FaceColor','flat')
 
    end
    
    % Deformed fluid cylinder model
    if any(models_ON == i_model_deformedcyl)
        nsigma_dc_OneFish = deformedcylinder_model( freqs, fliplr(tif_MyFrame), c0, c1, g, KRMFilePath, KRMFilename ); % dim: 1 x Nfreq x Ntif
        nTS_dc_OneFish = 10*log10(nsigma_dc_OneFish ); % * 0.0645^2   % dim: 1 x Nfreq x Ntif
        nTS_dc_OneFish = reshape(nTS_dc_OneFish,Nfreq,Ntif); % dim : Nfreq x Ntif
        nTS_dataset(i_fish,i_model_deformedcyl,:,:) = nTS_dc_OneFish;
    end
    
    % % Fluid cylinder of finite length with end effects (modal series Stanton88 + f_end Ye1997)
    if any(models_ON == i_model_straightcyl_end_eff)
        nsigma_fend_OneFish = cylinder_model_ka( kL_bc_distrib(i_fish,:), Lb_distrib(i_fish), tisb_St88_Frame(i_fish,:), g, h, 6 ); % dim: Nfreq x length(L/b) x Ntif
        nTS_fend_OneFish = 10*log10(nsigma_fend_OneFish(:,1,:)); % dim: Nfreq x 1 x Ntif
        nTS_fend_OneFish = reshape(nTS_fend_OneFish,Nfreq,Ntif); % dim : Nfreq x Ntif
        nTS_dataset(i_fish,i_model_straightcyl_end_eff,:,:) = nTS_fend_OneFish;
    end
    
    % end
    
    str_process = sprintf('%i/%i', i_fish, Nfish);
    disp(str_process);
    
end

% Load and save of simulations with ellipsoid TS computation :
% save('in/nTS_-50..0.1..50�_18..1..200kHz_Models123_sardines.mat','nTS_dataset');
% load('in/psms/nTS_-50..0.1..50�_18..1..200kHz_Models123_sardines.mat');
% save('in/psms/nTS_PSMS_38kHz.mat','nTS_dataset');
% load('in/psms/nTS_PSMS.mat'); % PSMS model save

% save('nTS_PSMS_-50.0.5.50_112_2_120','nTS_dataset');
% load('in/nTS_soft_allmodels_-50.0.5.50_18_2_120.mat');

%% (fish body tilt according to vertical axis) normal distribution

for i=1:length(m_ta)
    
anglen = m_ta(i) + std_ta(i) * randn(1, Nang0); % probality density distribution (PDF)
% Truncate at 95% the PDF :
anglen = anglen(anglen >=  m_ta(i) - 3*std_ta(i) & anglen <= m_ta(i) + 3*std_ta(i));
Nang = length(anglen);
% figure
% nbins = 100;
% histogram(anglen,nbins,'Normalization','probability');
% title('Tilt angle distribution'); xlabel('Tilt angle (�)'); ylabel('relative number of observations')

% Mean over angle distribution for each fish
nTS_tiltAveraged_dataset = zeros(Nfish,Nmodel_max,Nfreq);
for i_fish = 1:Nfish % fish selection
    for i_model = models_ON % model selection
        for i_freq = 1:Nfreq % frequency selection
            nTS_tmp = nTS_dataset(i_fish, i_model,i_freq,:); % we select one fish and one model
            nTS_tmp = squeeze(nTS_tmp);
            if i_model == 4 % PSMS can only be computed with TSmax for normal incidence
                nTS_anglen_dataset_tmp = interp1(tif_MyFrame-bladder_tilt(i_fish), nTS_tmp, anglen, 'nearest','extrap');
            else
                nTS_anglen_dataset_tmp = interp1(tif_MyFrame, nTS_tmp, anglen, 'nearest','extrap');
            end
            nTS_tiltAveraged_dataset_tmp = 10*log10(mean(10.^(nTS_anglen_dataset_tmp/10)));
            nTS_tiltAveraged_dataset(i_fish,i_model,i_freq) = nTS_tiltAveraged_dataset_tmp;
        end
    end
end

%% Plot of ka dependancy and theta dependancy for each fish


for i_freq=1:length(freq4ThetaPlot)
    ind=find(abs(freqs-freq4ThetaPlot(i_freq))<(max(freqs)-min(freqs))/Nfreq);
    ind=find(abs(freqs-freq4ThetaPlot(i_freq))<5000);
    i_freq4ThetaPlot(i_freq)=ind(1);
end
% freq4ThetaPlot_boolean = ismember(freqs, freq4ThetaPlot);
% i_freq4ThetaPlot = find(freq4ThetaPlot_boolean);

% Legend for models :
model_legend = cell(1,length(models_ON));
for ii = 1:length(models_ON)
    model_legend{ii} = strcat(model_desc{models_ON(ii)}, '   ', mark_{models_ON(ii)});
end

% each fish is associated to one color :
set(0,'DefaultAxesColorOrder',jet(Nfish))

% Theta dependancy
fig1 = figure;%('Position',scrsz);
for i_freq2plot = 1:length(i_freq4ThetaPlot)
    subplot(2,2,i_freq2plot); hold all; %grid on; %ylim([-100 -30]);
    ylim([-70 0])
    for i_model = models_ON
        for i_fish = fish2plot % 1:Nfish % loop on fishes
            nTS_tmp = nTS_dataset(i_fish, i_model,:,:);
            nTS_tmp = reshape(nTS_tmp,Nfreq,Ntif);
            if i_model == 4 % PSMS can only be computed with TSmax for normal incidence
                p_ti = plot(tif_MyFrame-bladder_tilt(i_fish), nTS_tmp(i_freq4ThetaPlot(i_freq2plot),:), 'LineStyle',mark_{i_model}, 'LineWidth', 2, 'Color',facecolor_{i_model});
            else
                p_ti = plot(tif_MyFrame, nTS_tmp(i_freq4ThetaPlot(i_freq2plot),:), 'LineStyle',mark_{i_model}, 'LineWidth', 2, 'Color',facecolor_{i_model});
            end
%             if length(fish2plot) == 1
%                 p_ti.Color = 'k';
%                 p_ti.LineWidth = 1;
%             end
        end
        % y_lim = [-100, 0];
        % plot((-bladder_tilt(i_fish))*ones(size(y_lim)), y_lim, 'k-') % plot tilt line
    end
    if length(fish2plot) == 1
        % title(strcat('Directivit� angulaire -  d''une sardine adulte - f=',num2str(freqs(i_freq4ThetaPlot(i_freq2plot))*1e-3),'kHz'));
        % xlabel('Angle d''incidence de l''onde [�]'); ylabel('index de cible TS [dB]');
        title(strcat('f=',num2str(round(freqs(i_freq4ThetaPlot(i_freq2plot))*1e-3)),'kHz',' ka_{eq}=',num2str((kL_distrib(fish2plot,i_freq4ThetaPlot(i_freq2plot))./mean(Ln))*(mean(Ln)/2*mean(as2)^2)^(1/3),2)));
        xlabel('Angle [�]'); ylabel('nTS [dB]');
%         legend('cylindre courb� modal', 'cylindre HF Urick', 'A1inslie&Ye basse fr�quence','Location','southwest')
        legend('cylindre droit modal',  'cylindre courb� modal', 'ellipso�de modal','cylindre d�form� modal', 'cylindre r�gime g�om�trique Urick', 'Location','northeast')
        axis([-50 50 -60 -5])
    else
        legend(fish_legend,'Location','south'); %display legend 1
        annotation('textbox', [0.2,0.1,0.1,0.1],...
            'String',model_legend)
    end
end
set(gca,'PlotBoxAspectRatio',[2 1 1])

% nTS_dataset(nTS_dataset<-50)=-50;
%    figure;
%     imagesc(tif_MyFrame,kL_distrib(1,:)./mean(Ln)*(mean(Ln)/2*mean(as2)^2)^(1/3),abs(10*log10(medfilt1(10.^(squeeze(nTS_dataset(1, 6,:,:)-nTS_dataset(1, 2,:,:))/10),11,Ntif,2))))
%     caxis([0 5])
%     colorbar;
%      title(strcat('Aplatissement = ',num2str(Lb_distrib/2)));
%     ylabel('ka_{eq} avec a_{eq} rayon �quivalent vessie'); xlabel('Angle [�]');
    
% ka dependancy
kaeq_distrib_plot=(mean(kL_distrib,1)./mean(Ln))*(mean(Ln)/2*mean(as2)^2)^(1/3);
figure; %hold on; %grid on; %xlim([0 30]); %ylim([-43 -39.5]);
% ylim([-23 -16]);
for i_model = models_ON
    for i_fish = fish2plot % 1:Nfish % loop on fishes
        p_ka = semilogx(kaeq_distrib_plot, squeeze(nTS_tiltAveraged_dataset(i_fish,i_model,:)), 'LineStyle',mark_{i_model}, 'LineWidth', 2, 'Color',facecolor_{i_model});
        set(gca,'YGrid','on');
         set(gca,'XGrid','on');
        if length(fish2plot) == 1
%             p_ka.Color = 'k';
%             p_ka.LineWidth = 2;
            hold on;
        end
    end
end

% Legend writing
if length(fish2plot) == 1
    title(strcat('Sardine 3 - angle d''incidence N(',num2str(m_ta(i)),'�;',num2str(std_ta(i)),'�))'));
    xlabel('ka_{eq}'); ylabel('nTS moyen [dB]');
 legend('cylindre courb� modal', 'cylindre HF Urick', 'Ainslie&Ye basse fr�quence','Location','southwest')
else
    title(strcat('tilt-averaged nTS versus ka - Tomofish sardines - Tilt distribution (m=',num2str(m_ta(i)),'�, std=',num2str(std_ta(i)),'�)'));
    xlabel('ka with a the semi-length'); ylabel('tilt-averaged length-normalised TS [dB]');
    legend(fish_legend,'Location','northeast');
    annotation('textbox', [0.2,0.1,0.1,0.1],...
        'String',model_legend)
end

axis([min(kaeq_distrib_plot) max(kaeq_distrib_plot) -50 0]);

for i_fish = 1:Nfish % fish selection
        
nTS_fish = squeeze(nTS_tiltAveraged_dataset(i_fish,:,:));

save(['in/nTS_hybrid_',num2str(round(m_ta(i))),'_',num2str(round(std_ta(i))),'_',num2str(round(12)),'_',num2str(round(D)),'.mat'],'kaeq_distrib_plot','nTS_fish');

%  save(['in/nTS_hybrid_',num2str(round(m_ta(i))),'_',num2str(round(std_ta(i))),'_',num2str(round(Lb_distrib(i_fish))),'_',num2str(round(D)),'.mat'],'kaeq_distrib_plot','nTS_fish');
end

% difference between models

kavect=reshape(kL_distrib/2,Nfish*Nfreq,1);
[kavect_tri,indexka]= sort(kavect);
N_ka_2plot = round(((max(kavect_tri)-min(kavect_tri))/0.2)*(1/mean(Ln/2))*(mean(Ln)/2*mean(as2)^2)^(1/3)); % number of ka value to plot if we want a step of 0.1
 N_ka_2plot=Nfreq/4;
pas_ka_2plot = round(length(kavect_tri)/N_ka_2plot);
ka_distrib_plot = logspace(log10(min(kavect_tri)),log10(max(kavect_tri)), N_ka_2plot);
% ka_distrib_plot = linspace((min(kavect_tri)),(max(kavect_tri)), N_ka_2plot);
nTSmean_fishAll_dataset = zeros(Nmodel_max,length(ka_distrib_plot));
nTSquantile_fishAll_dataset = zeros(Nmodel_max,length(ka_distrib_plot),2);

for i_model = models_ON % model selection
    nTS_oneModel_AllFish = nTS_tiltAveraged_dataset(:, i_model,:);
    nTS_oneModel_AllFish = reshape(nTS_oneModel_AllFish,Nfish*Nfreq,1); % dim : fish * freq
    nTS_oneModel_AllFish = nTS_oneModel_AllFish(indexka);
    for i_kL = 1:length(ka_distrib_plot)
        if i_kL == 1
            nTS_tmp = nTS_oneModel_AllFish(1:pas_ka_2plot);
        elseif i_kL == length(ka_distrib_plot)
            nTS_tmp = nTS_oneModel_AllFish(end-pas_ka_2plot:end);
        else
            mm = (i_kL-1)*pas_ka_2plot;
            nTS_tmp = nTS_oneModel_AllFish(mm-pas_ka_2plot/2:1:mm+pas_ka_2plot/2);
        end
        m_ka = 10*log10(mean(10.^(nTS_tmp/10)));
        quantile_ka = prctile(nTS_tmp,[5 95]);
        
        nTSmean_fishAll_dataset(i_model,i_kL) = m_ka;
        nTSquantile_fishAll_dataset(i_model,i_kL,:) = quantile_ka;
    end
    
    %     nTS_fishAll_dataset(i_model,:) = nTS_tmp(indexkL);
end

kaeq_distrib_plot=(ka_distrib_plot./mean(Ln/2))*(mean(Ln)/2*mean(as2)^2)^(1/3);
figure; hold on; grid on;
for i_model = models_ON
    % plot( kavect(indexka)/2, (nTS_fishAll_dataset(i_model,:)-nTS_fishAll_dataset(5,:)),'Color',facecolor_{i_model})
    errorbar(kaeq_distrib_plot, nTSmean_fishAll_dataset(i_model,:), nTSmean_fishAll_dataset(i_model,:)-nTSquantile_fishAll_dataset(i_model,:,1),nTSquantile_fishAll_dataset(i_model,:,2)-nTSmean_fishAll_dataset(i_model,:) ,'kx', 'LineStyle',mark_{i_model}, 'LineWidth', 2, 'Color',facecolor_{i_model})
    % plot( kL_distrib_plot/2, (nTS_fishAll_dataset(i_model,:)),'Color',facecolor_{i_model})
end
set(gca,'XScale','log')
title(strcat('nTS moyen avec angleangle d''incidence N(',num2str(m_ta(i)),'�;',num2str(std_ta(i)),'�))'));
xlabel('kaeq'); ylabel('nTS moyen [dB]');
legend('cylindre courb� modal',  'cylindre HF Urick', 'Ainslie&Ye basse fr�quence','Location','northeast')
% legend('cylindre droit modal',  'cylindre HF Urick')
% legend('cylindre droit',  'cylindre courb�', 'cylindre d�forme', 'Location','northeast')
legend('cylindre droit modal',  'cylindre courb� modal', 'ellipso�de modal','cylindre d�form� modal', 'cylindre r�gime g�om�trique Urick', 'Location','northeast')

% axis([0 4.5 -24 -15]);
% 
% nTSmean_fishAll_dataset_dist(i,:,1)=nTSmean_fishAll_dataset(2,:)+10*log10(mean(Ln'.^2));
% nTSmean_fishAll_dataset_dist(i,:,2)=nTSquantile_fishAll_dataset(2,:);


end

% %% Links between mean TS and bladder curvature angle
% figure('Position',scrsz); hold on; grid on;
% i_freq = i_freq4ThetaPlot;
% plot_list2 = zeros(i_model,i_fish);
% for i_model = models_ON
% for i_fish = 1:Nfish % loop on fishes
% plot_list2(i_model,i_fish) = scatter(gamma_max(i_fish),nTS_tiltAveraged_dataset(i_fish,i_model,i_freq),200,'MarkerEdgeColor','k','MarkerFaceColor',facecolor_{i_model});
% end
% end
% xlabel('Curvature angle [�]'); ylabel('mean nTS'); % legend(plot_list2(:,1),model_desc(models_ON),'Location','northwest');
% title(strcat('f=',num2str(freqs(i_freq)*1e-3),'kHz'))
%
% %% DeformedCylinder_TSmax_Position
% DefCyl_TS = nTS_dataset(:,i_model_deformedcyl,:,:); % dim : Nfish x 1 x Nfreq x Ntif
% DefCyl_TS = reshape(DefCyl_TS,Nfish,Nfreq,Ntif); % dim : Nfish x Nfreq x Ntif
%
% [DefCyl_TSmax_Value,DefCyl_TSmax_Position] = max(DefCyl_TS,[],3); % dim : Nfish x Nfreq
% % KRM_TSmax_Position : angle value for TSmax with KRM model, for every fish and frequency
%
% figure('Position',scrsz); hold on; grid on;
% for i_fish = 1:Nfish
% plot(freqs*1e-3,tif_MyFrame(DefCyl_TSmax_Position(i_fish,:)))
% end
% for i_fish = 1:Nfish
% plot(freqs*1e-3,-bladder_tilt_up(i_fish)*ones(size(freqs)),'--')
% end
% for i_fish = 1:Nfish
% plot(freqs*1e-3,-bladder_tilt_c(i_fish)*ones(size(freqs)),':')
% end
% title('Deformed cylinder TS_{max} position according to frequency');
% xlabel('Frequency [kHz]'); ylabel('TSmax position [angle �]');
% legend(fish_legend,'Location','southwest');

if (1)
    
    frequency = { [   71.9430   76.4490   80.9560   85.4620   89.9690 ...
        94.4760   98.9820  103.4890  107.9950  112.5020  118.0570  ...
        114.7550  110.2490  105.7420  101.2350   96.7290   92.2220   ...
        87.7160   83.2090 78.7020  74.1960]*1e3;
        [ 70 120 ]*1e3};
    % frequency={[];[18 38 70 120 200 ]*1e3};
    % Nbeam={0;5};
    Nbeam = {length(frequency{1}) ; length(frequency{2})};
    
    steering = {[-42.20577 -35.92659 -30.43624 -25.51571 -21.02889 ...
        -16.88417 -13.01624 -9.376566 -5.927816 -2.640473 0.495642 ...
        3.601663 6.827438 10.21141 13.78197 17.57491 21.63671 26.02967 ...
        30.84084 36.19885 42.30876] * (pi/180);
        0*ones(1,Nbeam{2})};
    % steering={[];0*ones(1,Nbeam{2})};
    
    BW = {3.5*1e3*ones(1,Nbeam{1});
        3*1e3*ones(1,Nbeam{2})};
    % BW={[];3*1e3*ones(1,Nbeam{2})};
    
    openingAlong={[5.03 4.73 4.47 4.23 4.03 3.83 3.65 3.49 3.35 3.21 3.06 3.15 3.28 3.42 3.57 3.74 3.92 4.12 4.35 4.6 4.87]*pi/180;[ 7 7]*pi/180};
    openingAthwart={[6.9 5.94 5.27 4.77 4.38 4.07 3.81 3.6 3.42 3.27 3.11 3.21 3.36 3.53 3.74 3.99 4.29 4.66 5.15 5.79 6.7]*pi/180;[  7 7]*pi/180};
    % openingAlong={[];[ 11 7 7 7 7]*pi/180};
    % openingAthwart={[];[ 11 7 7 7 7]*pi/180};
    % openingAlong={[];[ 17 11 11 11 11]*pi/180};
    % openingAthwart={[];[ 17 11 11 11 11]*pi/180};
    
    m_ta   = [0 0 0 -30 -30 -30]; % angle mean (�), <0 according to our orientation of reference
    std_ta = [5 10 15 5 10 15]; % angle std (�)
    m_ta   = [-0.5 -0.5 -0.5 -0.5]; % angle mean (�), <0 according to our orientation of reference
    std_ta = [1 5 10 15]; % angle std (�)
    m_ta   = [-0.5 -10 -20 -30]; % angle mean (�), <0 according to our orientation of reference
    std_ta = [15 15 15 15]; % angle std (�)
    m_ta   = [-30]; % angle mean (�), <0 according to our orientation of reference
    std_ta = [15 ]; % angle std (�)
    
    % m_ta   = [1 3 5 7]; % angle mean (�), <0 according to our orientation of reference
    % std_ta = [0.5 0.5 0.5 0.5]; % angle std (�)
    
    nbanglesAthmulti=60;
    nbanglesAlmulti=7;
    
    nbanglesAthmono=7;
    nbanglesAlmono=7;
    
    anglesAthwart={NaN(length(steering{1}),nbanglesAthmulti+1);NaN(length(steering{2}),nbanglesAthmono+1)};
    anglesAlong={NaN(length(steering{1}),nbanglesAlmulti+1);NaN(length(steering{2}),nbanglesAlmono+1)};
    
    
    
    
    for i_echosounder = 1:length(Nbeam)
        tetaminAth{i_echosounder}=min(steering{i_echosounder}-openingAthwart{i_echosounder}/2);
        tetamaxAth{i_echosounder}=max(steering{i_echosounder}+openingAthwart{i_echosounder}/2);
        tetaminAl{i_echosounder}=-min(openingAlong{i_echosounder}/2);
        tetamaxAl{i_echosounder}=max(openingAlong{i_echosounder}/2);
    end
    
    for i_echosounder = 1:length(Nbeam)
        for i_beam = 1:Nbeam{i_echosounder}
            for i=1:length(anglesAthwart{i_echosounder})
                angleat=asin(sin(tetaminAth{i_echosounder})+(i-1)*(sin(tetamaxAth{i_echosounder})-sin(tetaminAth{i_echosounder}))/length(anglesAthwart{i_echosounder}));
                if abs(angleat-steering{i_echosounder}(i_beam))<=openingAthwart{i_echosounder}(i_beam)/2
                    anglesAthwart{i_echosounder}(i_beam,i)=angleat;
                end
            end
            for i=1:length(anglesAlong{i_echosounder})
                angleal=asin(sin(tetaminAl{i_echosounder})+(i-1)*(sin(tetamaxAl{i_echosounder})-sin(tetaminAl{i_echosounder}))/length(anglesAlong{i_echosounder}));
                if abs(angleal)<=openingAlong{i_echosounder}(i_beam)/2
                    anglesAlong{i_echosounder}(i_beam,i)=angleal;
                end
            end
        end
    end
    
    
    for i_echosounder = 1:length(Nbeam)
        TS_dataset_dist=NaN(length(m_ta),Nmodel,Nbeam{i_echosounder},Nfish*Nang0*length(anglesAthwart{i_echosounder})*length(anglesAlong{i_echosounder}));
        TS_fishAveraged_tiltAveraged_dataset_mean=NaN(length(m_ta),Nmodel,Nbeam{i_echosounder});
        TS_fishAveraged_tiltAveraged_dataset_quantile=NaN(length(m_ta),Nmodel,Nbeam{i_echosounder},2);
        TS_tiltAveraged_dataset_mean=NaN(length(m_ta),Nfish,Nmodel,Nbeam{i_echosounder},2);
        for i_nage=1:length(m_ta)
            anglen = m_ta(i_nage) + std_ta(i_nage) * randn(1, Nang0); % probality density distribution (PDF)
            % Truncate at 95% the PDF :
            anglen = anglen(anglen >=  m_ta(i_nage) - 3*std_ta(i_nage) & anglen <= m_ta(i_nage) + 3*std_ta(i_nage));
            Nang = length(anglen);
            
            yaw=(0*rand(1,Nang));
            %         yaw=5 + 5.5*randn(1,Nang);
            % yaw=(0*ones(1,Nang));
            % yaw(1:round(Nang/2))=(normrnd(-45,std_ta,1,round(Nang/2)));
            % yaw(1+round(Nang/2):Nang)=(normrnd(45,std_ta,1,Nang-round(Nang/2)));
            % yaw(1:round(Nang/2))= -45 + std_ta*randn(1,round(Nang/2));
            % yaw(1+round(Nang/2):Nang)= 45 + std_ta*randn(1,Nang-round(Nang/2));
            
            roll=0*ones(1,Nang);
            tilt=anglen;
            
            apparentphi=zeros(Nbeam{i_echosounder},Nang*length(anglesAthwart{i_echosounder})*length(anglesAlong{i_echosounder}));
            
            nTS_dataset_apparentangle = NaN(Nfish,Nmodel,Nbeam{i_echosounder},Nang*length(anglesAthwart{i_echosounder})*length(anglesAlong{i_echosounder}));
            
            for i_beam = 1:Nbeam{i_echosounder}
                j=1;
                if i_beam==11
                    toto=0
                end
                for ath=1:length(anglesAthwart{i_echosounder})
                    for al=1:length(anglesAlong{i_echosounder})
                        for i=1:Nang
                            Rfish=CreateRotationMatrix(yaw(i)*(pi/180),(tilt(i))*(pi/180),roll(i)*(pi/180));
                            Rbeam=CreateRotationMatrix(0*pi/180,anglesAlong{i_echosounder}(i_beam,al), -anglesAthwart{i_echosounder}(i_beam,ath));
                            Vbeam=Rbeam*[0;0;1;];
                            Vfish=-Rfish*Vbeam;
                            Vfishproj=[Vfish(1);Vfish(2);0];
                            apparentphi(i_beam,j)=(acos(dot(Vbeam,Rfish*[1;0;0;])))*180/pi;
                            apparentroll(i_beam,j)=acos(dot(Vbeam,cross(Rfish*[1;0;0;],[0;0;1])))*180/pi;

                                                     apparentphi2(i_beam,j)=atand(Vfish(2,:)/Vfish(1,:));
                                                     
                                                      apparentroll2(i_beam,j)=90+atand(Vfish(3,:)/sqrt(Vfish(1,:)*Vfish(1,:)+Vfish(2,:)*Vfish(2,:)));

%                                                      if  (~isnan(apparentphi(i_beam,j)) && ~isnan(apparentphi2(i_beam,j)) && apparentphi(i_beam,j)-apparentphi2(i_beam,j)~=0)
%                                                          i_beam
%                                                          apparentphi(i_beam,j)
%                                                        apparentphi2(i_beam,j)
% %                                                          apparentroll(i_beam,j)
% %                                                          apparentroll2(i_beam,j)
%                                                      end
                            %                         
                            j=j+1;
                        end
                    end
                end
                
                
                i_freq= find(abs(freqs-frequency{i_echosounder}(i_beam))<=BW{i_echosounder}(i_beam)/2);
                for i_fish = 1:Nfish % loop on fishes
                    for i_model = models_ON
                        nTS_tmp = nTS_dataset(i_fish, i_model,i_freq(1),:); % dim : freq * angle
                        nTS_tmp = squeeze(nTS_tmp);
                        %
                        %             nTS_tmp = nTS_dataset{i_fish, i_model};
                        if i_model == 4 % PSMS can only be computed with TSmax for normal incidence
                            nTS_anglen_dataset_tmp = interp1(tif_MyFrame-bladder_tilt(i_fish), nTS_tmp, apparentphi(i_beam,:)-90, 'nearest','extrap');
                        else
                            nTS_anglen_dataset_tmp = interp1(tif_MyFrame, nTS_tmp,  apparentphi(i_beam,:)-90, 'nearest','extrap');
                        end
                        nTS_dataset_apparentangle(i_fish,i_model,i_beam,:) = nTS_anglen_dataset_tmp;
                        TS_tiltAveraged_dataset_mean(i_nage,i_fish,i_model,i_beam) = 10*log10(Ln(i_fish).^2.*nanmean(10.^(nTS_anglen_dataset_tmp./10)));
                    end
                end
            end
            
            for i_model = models_ON
                for i_beam = 1:Nbeam{i_echosounder}
                    nTS_tmp = squeeze(nTS_dataset_apparentangle(:,i_model,i_beam,:));
                    TS_dataset_dist(i_nage,i_model,i_beam,1:Nfish*Nang*length(anglesAthwart{i_echosounder})*length(anglesAlong{i_echosounder})) = reshape(10*log10(repmat(Ln'.^2,1,Nang*length(anglesAthwart{i_echosounder})*length(anglesAlong{i_echosounder})).*10.^(nTS_tmp/10)),[],1)';  % nTS to TS
                    TS_fishAveraged_tiltAveraged_dataset_mean(i_nage,i_model,i_beam) = 10*log10(nanmean(10.^(TS_dataset_dist(i_nage,i_model,i_beam,:)/10))); % average over fishes and angles
                    TS_fishAveraged_tiltAveraged_dataset_quantile(i_nage,i_model,i_beam,:) = 10*log10(prctile(10.^(TS_dataset_dist(i_nage,i_model,i_beam,:)/10), [5 95]));
                end
            end
            
        end
        
        
        c=colormap(hsv(length(m_ta)));
%         for i_model = models_ON
%             for i_beam = 1:Nbeam{i_echosounder}
%                 edge=(-65:0.5:-35);
%                 Nb_TS=zeros(length(m_ta),length(edge));
%                 for i_nage=1:length(m_ta)
%                     Nb_TS(i_nage,:)=squeeze(histc(TS_dataset_dist(i_nage,i_model,i_beam,:),edge));
%                 end
%                 
%                 figure;
%                 hold on; grid on;
%                 for i_nage=1:length(m_ta)
%                     plot(edge,Nb_TS(i_nage,:)/sum(Nb_TS(i_nage,:)),'color',c(i_nage,:), 'LineWidth', 3);
%                 end
%                 %             legend(['TSmean N(-0.5,1)   : ' ,num2str(TS_fishAveraged_tiltAveraged_dataset_mean(1,i_model,i_beam),'%.1f');'TSmean N(-0.5,5)   : ' ,num2str(TS_fishAveraged_tiltAveraged_dataset_mean(2,i_model,i_beam),'%.1f');...
%                 %                       'TSmean N(-0.5,10)  : ' ,num2str(TS_fishAveraged_tiltAveraged_dataset_mean(3,i_model,i_beam),'%.1f');'TSmean N(-0.5,15)  : ' ,num2str(TS_fishAveraged_tiltAveraged_dataset_mean(4,i_model,i_beam),'%.1f')])
%                 %
%             end
%             
%             disp('Do&Surti b20 computation method :');
%             if i_echosounder == 2
%                 figure;
%                 for i_nage=1:length(m_ta)
%                     hold on; grid on;
%                     for i_beam = 1:Nbeam{i_echosounder}
%                         for i_fish = 1:Nfish % loop on fishes
%                             b20_St88(i_beam,i_fish) = TS_tiltAveraged_dataset_mean(i_nage,i_fish,i_model,i_beam)-20*log10(mean(Ln_fish_cm(i_fish)));
%                         end
%                         str2 = sprintf('Frequency %2.0f kHz - b20 = %2.1f [dB]', freqs(i_beam)*1e-3, 10*log10(nanmean((10.^(b20_St88(i_beam,:)./10)))));
%                         disp(str2);
%                     end
%                     
%                     plot(frequency{i_echosounder}*1e-3, 10*log10(nanmean((10.^(b20_St88(:,:)./10)),2)),'x--','color',c(i_nage,:), 'LineWidth', 3);
%                 end
%                 title('b_{20} - Uniformly bent cylinder acoustic model'); xlabel('Frequency (kHz)'); ylabel('b_{20} [dB]');
%                 legend(['N(-0.5,1) ' ;'N(-0.5,5) ' ;'N(-0.5,10)' ;'N(-0.5,15)' ])
%                 axis([18 200 -70 -62]);
%                 
%             end
%             
%         end
        
        for i_model = models_ON
            figure;
            for i_nage=1:length(m_ta)
                hold on; grid on;
                if i_echosounder == 1 & length(steering{i_echosounder})>0
                    %                 plot(steering{i_echosounder}*180/pi, squeeze(TS_fishAveraged_tiltAveraged_dataset(i_nage,i_model,:)),'-*','color',c(i_nage,:), 'LineWidth', 3);%,'kx', 'LineStyle',mark_{i_model}, 'LineWidth', 2, 'Color',facecolor_{i_nage})
                    errorbar(steering{i_echosounder}*180/pi, squeeze(TS_fishAveraged_tiltAveraged_dataset_mean(i_nage,i_model,:)), -squeeze(TS_fishAveraged_tiltAveraged_dataset_quantile(i_nage,i_model,:,1))+ squeeze(TS_fishAveraged_tiltAveraged_dataset_mean(i_nage,i_model,:)), squeeze(TS_fishAveraged_tiltAveraged_dataset_quantile(i_nage,i_model,:,2))- squeeze(TS_fishAveraged_tiltAveraged_dataset_mean(i_nage,i_model,:)),'-*','color',c(i_nage,:), 'LineWidth', 3);
                    xlabel('Angle(�)');
                elseif i_echosounder == 2
                    errorbar(frequency{i_echosounder}*1e-3, squeeze(TS_fishAveraged_tiltAveraged_dataset_mean(i_nage,i_model,:)), -squeeze(TS_fishAveraged_tiltAveraged_dataset_quantile(i_nage,i_model,:,1))+ squeeze(TS_fishAveraged_tiltAveraged_dataset_mean(i_nage,i_model,:)), squeeze(TS_fishAveraged_tiltAveraged_dataset_quantile(i_nage,i_model,:,2))- squeeze(TS_fishAveraged_tiltAveraged_dataset_mean(i_nage,i_model,:)),'-*','color',c(i_nage,:), 'LineWidth', 3);
                    xlabel('Frequence (kHz)');
                end
            end
            %         title(strcat('tilt-averaged TS versus '));
            ylabel('TS moyen [dB]');
            legend('N(0,5)', 'N(0,10)', 'N(0,15)','N(-30,5)','N(-30,10)','N(-30,15)')
            
        end
        
    end

end

