
% V7.00
% updates :
% - angle convention according to a direct frame : positive from Z to X and negative from X to Z
% - allow you to work with one frequency
% - plot at fullscreen size
% V8.00
% - deformed cylinder model is validated
% - deformed cylinder model is validated

clear *; close all;
dbstop if error
addpath(genpath(pwd)) % add path for subfolder 'my_functions' and for input data (swimbladder *.stl)
cd(fileparts(mfilename('fullpath'))); % set the current Matlab folder = script directory

scrsz = get(groot,'ScreenSize');
% Change default axes fonts.
set(0,'DefaultAxesFontName', 'Arial')
set(0,'DefaultAxesFontSize', 20)
% Change default text fonts.
set(0,'DefaultTextFontname', 'Arial')
set(0,'DefaultTextFontSize', 14)
% Change default line width.
set(0,'defaultlinelinewidth', 3)


%% Prompt user for fish parameters
prompt = {'Relative mass density:', ...
  'Water soundspeed (m/s):', ...
  'Swimbladder soundspeed (m/s):', ...
  'Frequencies (kHz):', ...
  'Incidence angles (�):'};
name = 'Inputs for fish and water parameters';
numlines = 1;
defaultanswer = {'0.00129', '1483', '340', '18:1:200', '-50:1:50'};

answer = inputdlg(prompt,name,numlines,defaultanswer);

% If user pressed cancel
if isempty(answer)
  error('User pressed cancel')
end

% Parse out inputs to variables
g = str2double(answer{1});    % relative mass density
c0 = str2double(answer{2});    % water soundspeed (m/s)
c1 = str2double(answer{3});    % swimbladder soundspeed (m/s)
freqs = eval(answer{4});    % echosounder frequencies (kHz)
freqs = freqs * 1e3; % Hz
% fish body tilt according to vertical axis (acoustic wave incidence = 0�) :
tif_MyFrame = eval(answer{5});
% Transfer into local fish frame for TS computation :
tif_KRM_Frame  = 90 + tif_MyFrame;
tif_St88_Frame = 90 - tif_MyFrame;
tif_Fu88_Frame = 90 - tif_MyFrame;

h  = c1/c0;   % relative sound speed
Nfreq = length(freqs);
Ntif  = length(tif_MyFrame);


% Choice of the dataset
i_dataset = menu('Which set of data use ?','Sardines from Aquarium La Rochelle2 (13/11/2014)','Herrings from La Manche - January 2013 - depth < 30m', 'Simulated fishes');
datasetFileName = {'AqLR2_caracteristics2.txt', 'Manche2013_caracteristics.txt', 'VirtualFish_caracteristics.txt'};
KRMFileNamePrefix = {'sardine_AqLR2_','herring_Manche2013_',''};
KRMFilePath = 'in/InputDataKRM/'; % path of input geometry files for KRM script
close;

%% Load of fish dataset

if i_dataset == 0 % if the user close the multiple-choice dialog box,
    error('User pressed cancel') % the script is stopped
elseif i_dataset == 1 || i_dataset == 2
% Dataset : Tomofish sardines from La Rochelle
fishDataSet = importdata(datasetFileName{i_dataset});
fishDataSet = fishDataSet.data.';
fish_index = fishDataSet(1,:);
Ln = fishDataSet(3,:)*1e-2; % bladder length (m)
as = fishDataSet(11,:)*1e-2; % bladder radius (m) (Constant area)
as2= fishDataSet(12,:)*1e-2; % bladder radius (m) (Constant area)

bladder_tilt_edge = fishDataSet(7,:); % bladder tilt (�), >0
bladder_tilt_LS = fishDataSet(8,:); % bladder tilt (�), >0
bladder_tilt = bladder_tilt_LS; % bladder tilt (�), >0

Ln_fish_cm  = fishDataSet(2,:); % fish length (cm)
Nfish = size(fishDataSet,2); % number of fish in dataset
Lb_distrib  = Ln ./ as; % 2*flattening
Lb_distrib2 = Ln ./ as2; 

% bent bladder parameters
Ln_bc = fishDataSet(10,:)*1e-2; % bent bladder length (m)
gamma_max = abs(fishDataSet(9,:)); % 2*ones(1,Nfish)

elseif i_dataset == 3
% Dataset parameters :
Nfish0 = 1;
param_Lf      = [17, 1]*1e-2; % fish length (m) : mean, std
param_Lf_Lsb  = [2.5, 0.1];   % fish length / bladder length ratio : mean, std
param_sbFlat  = [10, 0.3];    % bladder flattening : mean, std
param_sbTilt  = [6.5, 0.5];   % bladder tilt in � : mean, std
param_sbGamma = [17.5, 1.5];  % bladder tilt in � : mean, std

fishDataSet = GenerateDataset( Nfish0, param_Lf, param_Lf_Lsb, param_sbFlat, param_sbTilt, param_sbGamma );
fishDataSet = round(fishDataSet*1e4) ./ 1e4; % double numbers to 4 decimal
% Create table
table_ = array2table(fishDataSet,...
    'VariableNames',{'FishIndex', 'FishLength', 'LengthRatio', 'BladderFlattening', 'BladderTilt', 'CurvatureAngle'});

writetable(table_,strcat('in/',datasetFileName{i_dataset}),'Delimiter',' ')

Nfish = size(fishDataSet,1); % fish number refresh after PDF 95% truncature
fish_index = fishDataSet(:,1); % fish index
Ln_fish_cm = fishDataSet(:,2)*1e2; % fish length (cm)
Ln = fishDataSet(:,3); % bladder length (m)
Ln_bc = 1.018*Ln; % bent bladder length (m)
bladder_tilt = fishDataSet(:,5); % bladder tilt (�), >0
gamma_max = fishDataSet(:,6); % bladder curvature angle (�)
Lb_distrib = 2*fishDataSet(:,4); % 2x bladder flattening
as = Ln ./ Lb_distrib; % bladder radius (m)
Lb_distrib2 = Lb_distrib;
as2 = as;

end

%% User parameters :

% model index :
% 1 : KRM model (Clay&Horne1994)
% 2 : straight cylinder model (Stanton1988)
% 3 : bent cylinder model (Stanton1989a)
% 4 : ellipsoid model (Furusawa1988)
% 5 : deformed cylinder model (Stanton1989a)
% 4 : straight cylinder with end effect model (Ye1997)
i_model_KRM         = 1;
i_model_straightcyl = 2;
i_model_bentcyl     = 3;
i_model_ell         = 4;
i_model_deformedcyl = 5;
i_model_straightcyl_end_eff = 6;
model_desc = {'KRM'; 'Straight Cylinder'; 'Bent cylinder'; 'Ellipsoid'; 'Deformed cylinder'; 'Straight cylinder with end effect'};
mark_ = {'--','--',':','-','-.','--'}; % LineStyle for each model : Stanton88, PSMS, KRM
facecolor_ = {'k','b','r','m','g','c'}; % LineStyle for each model : Stanton88, PSMS, KRM
% Choice of scattering models :
models_ON = [i_model_straightcyl,i_model_bentcyl,i_model_deformedcyl]; % i_model_KRM, i_model_straightcyl, i_model_bentcyl, i_model_ell, i_model_deformedcyl, i_model_straightcyl_end_eff

% Normal distribution parameters : (acoustic wave incidence angle (�) in fish body frame)
Nang0  = 10000; % number of fishes
% Values from Olsen1971 :
m_ta   = -0; % angle mean (�), <0 according to our orientation of reference
std_ta = 15; % angle std (�)

% Frequency used for theta dependancy plot :
freq4ThetaPlot = 38*1e3; % Hz


% Shape file choice :
ShapeGeometryList = {'real', 'straightcylinder', 'bentcylinder', 'ellipsoid'};
i_shape = 4;
ShapeGeometry = ShapeGeometryList{i_shape};
st = 1e-3; % slice thickness (m)

%% nTS computation for Tomofish data
% for each fish at different frequencies

% check if we try to use KRM model or deformed cylinder model with simulated data and real shape. 
% If it is the case, we suppress that model :
if any(models_ON == i_model_KRM | models_ON == i_model_deformedcyl) && i_dataset == 3 && i_shape == 1
    models_ON(models_ON == i_model_KRM) = [];
    models_ON(models_ON == i_model_deformedcyl) = [];
end

Nmodel = length(models_ON);
Nmodel_max = length(model_desc); % total number of scattering models
nTS_dataset = zeros(Nfish,Nmodel_max,Nfreq,Ntif); % nTS for each fish according to frequency and tilt angle
fish_legend = cell(Nfish,1);
kL_distrib = zeros(Nfish,Nfreq);
kL_bc_distrib = zeros(Nfish,Nfreq);
tisb_St88_Frame = zeros(Nfish,Ntif); % fish swimbladder incidence angles (�)
tisb_Fu88_Frame = zeros(Nfish,Ntif); % fish swimbladder incidence angles (�)
tisb_St89_Frame = zeros(Nfish,Ntif); % fish swimbladder incidence angles (�)

disp('Fish currently processed :')
disp(strcat('0/',num2str(Nfish)));

for i_fish = 1:Nfish % loop on fishes
% Legend writing :
fishDesc_part1 = num2str(fish_index(i_fish));
fishDesc_part2 = num2str(round(Lb_distrib(i_fish)/2*10)/10); % 1 decimal
fishDesc_part3 = num2str(round(Lb_distrib2(i_fish)/2*10)/10); % 1 decimal
fishDesc_part4 = num2str(round(Ln_fish_cm(i_fish)*100)/100); % 1 decimal
fish_legend{i_fish,1} = strcat('Fish n�',fishDesc_part1,' - a/b_{cyl}=',fishDesc_part2,' - a/b_{ell}=',fishDesc_part3,' - L_f=',fishDesc_part4,'cm');

% KRM file name (for real geometry and cylinder approximation) :
KRMrealgeometry_filename = strcat(KRMFileNamePrefix{i_dataset},num2str(fish_index(i_fish)),'_KRMgeometry');
KRMstraightcylinder_filename = strcat(KRMFileNamePrefix{i_dataset},num2str(fish_index(i_fish)),'_KRMstraightcylinder');
KRMbentcylinder_filename = strcat(KRMFileNamePrefix{i_dataset},num2str(fish_index(i_fish)),'_KRMbentcylinder');
KRMellipsoid_filename = strcat(KRMFileNamePrefix{i_dataset},num2str(fish_index(i_fish)),'_KRMellipsoid');
% Building of geometric shapes :
GenerateStraightCylinderForKRM([0 0 0], Ln(i_fish), 2*as(i_fish), bladder_tilt(i_fish), st, KRMFilePath, KRMstraightcylinder_filename);
GenerateBentCylinderForKRM([0 0 0], Ln_bc(i_fish), as(i_fish), gamma_max(i_fish), bladder_tilt(i_fish), st, KRMFilePath, KRMbentcylinder_filename);
GenerateEllipsoidForKRM([0 0 0], Ln_bc(i_fish)/2, as2(i_fish), bladder_tilt(i_fish), st, KRMFilePath, KRMellipsoid_filename);
KRMFilenameList = {KRMrealgeometry_filename, KRMstraightcylinder_filename, KRMbentcylinder_filename, KRMellipsoid_filename};
KRMFilename = KRMFilenameList{i_shape};

% tif_St88_Frame(i_fish,:) = tif_MyFrame - bladder_tilt(i_fish);
tisb_St88_Frame(i_fish,:) = tif_St88_Frame - bladder_tilt(i_fish);
tisb_Fu88_Frame(i_fish,:) = tif_Fu88_Frame - bladder_tilt(i_fish);
tisb_St89_Frame(i_fish,:) = tif_MyFrame + bladder_tilt(i_fish);

% Cylinder models
kL_distrib(i_fish,:) = (2*pi/c0)*Ln(i_fish)*freqs; 
kL_bc_distrib(i_fish,:) = (2*pi/c0)*Ln_bc(i_fish)*freqs; 

% if (0)
% Straight fluid cylinder model : fluid cylinder of finite length without end effects (modal series Stanton88)
% One fish : all frequencies x 1 flattening x all angles
if any(models_ON == i_model_straightcyl)
nsigma_St88_OneFish = cylinder_model_ka( kL_distrib(i_fish,:), Lb_distrib(i_fish), tisb_St88_Frame(i_fish,:), g, h, 1 ); % dim: Nfreq x length(L/b) x Ntif
nTS_St88_OneFish = 10*log10(nsigma_St88_OneFish(:,1,:)); % dim: Nfreq x 1 x Ntif
nTS_St88_OneFish = reshape(nTS_St88_OneFish,Nfreq,Ntif); % dim : Nfreq x Ntif
nTS_dataset(i_fish,i_model_straightcyl,:,:) = nTS_St88_OneFish;
end

% PSMS model
if any(models_ON == i_model_ell)
% nf_PSM_OneFish = PSM_model( freqs, Ln(i_fish)/2, as2(i_fish), tisb_Fu88_Frame(i_fish,:), c0,c1,g ); % dim: Nfreq x Ntif
% nTS_PSM_OneFish = 20*log10(abs(nf_PSM_OneFish));
% nTS_PSM_OneFish = reshape(nTS_PSM_OneFish,Nfreq,Ntif); % dim : Nfreq x Ntif
% nTS_dataset(i_fish,i_model_ell,:,:) = nTS_PSM_OneFish;

load('in/nTS_PSMS.mat'); % PSMS model save
nTS_dataset(:,i_model_ell,:,:) = nTS_PSMS(1:5,:,:,:);
end

% KRM model
if any(models_ON == i_model_KRM)
sigma_KRM_OneFish = KRM_model_bladderOnly( freqs, tif_KRM_Frame, c0, c1, g, KRMFilePath, KRMFilename ); % dim: 1 x Nfreq x Ntif
nTS_KRM_OneFish = 10*log10(sigma_KRM_OneFish ./ Ln(i_fish)^2); % dim: 1 x Nfreq x Ntif
nTS_KRM_OneFish = reshape(nTS_KRM_OneFish,Nfreq,Ntif); % dim : Nfreq x Ntif
nTS_dataset(i_fish,i_model_KRM,:,:) = nTS_KRM_OneFish;
end

% Bent fluid cylinder model
if any(models_ON == i_model_bentcyl)
nsigma_bc_OneFish = bentcylinder_model( kL_bc_distrib(i_fish,:), Lb_distrib(i_fish), tisb_St89_Frame(i_fish,:), g, h, gamma_max(i_fish) ); % dim: Nfreq x length(L/b) x Ntif
nTS_bc_OneFish = 10*log10(nsigma_bc_OneFish(:,1,:)); % dim: Nfreq x 1 x Ntif
nTS_bc_OneFish = reshape(nTS_bc_OneFish,Nfreq,Ntif); % dim : Nfreq x Ntif
nTS_dataset(i_fish,i_model_bentcyl,:,:) = nTS_bc_OneFish;
end

% Deformed fluid cylinder model
if any(models_ON == i_model_deformedcyl)
nsigma_dc_OneFish = deformedcylinder_model( freqs, fliplr(tif_MyFrame), c0, c1, g, KRMFilePath, KRMFilename ); % dim: 1 x Nfreq x Ntif
nTS_dc_OneFish = 10*log10(nsigma_dc_OneFish); % ./ Ln(i_fish)^2); % dim: 1 x Nfreq x Ntif
nTS_dc_OneFish = reshape(nTS_dc_OneFish,Nfreq,Ntif); % dim : Nfreq x Ntif
nTS_dataset(i_fish,i_model_deformedcyl,:,:) = nTS_dc_OneFish;
end

% % Fluid cylinder of finite length with end effects (modal series Stanton88 + f_end Ye1997)
if any(models_ON == i_model_straightcyl_end_eff)
nsigma_fend_OneFish = cylinder_model_ka( kL_distrib(i_fish,:), Lb_distrib(i_fish), tisb_St88_Frame(i_fish,:), g, h, 4 ); % dim: Nfreq x length(L/b) x Ntif
nTS_fend_OneFish = 10*log10(nsigma_fend_OneFish(:,1,:)); % dim: Nfreq x 1 x Ntif
nTS_fend_OneFish = reshape(nTS_fend_OneFish,Nfreq,Ntif); % dim : Nfreq x Ntif
nTS_dataset(i_fish,i_model_straightcyl_end_eff,:,:) = nTS_fend_OneFish;
end

% end

str_process = sprintf('%i/%i', i_fish, Nfish);
disp(str_process);

end

% Load and save of simulations with ellipsoid TS computation :
% save('in/nTS_-50..0.1..50�_18..1..200kHz_Models123_sardines.mat','nTS_dataset');
% load('in/nTS_-50..0.1..50�_18..1..200kHz_Models123_sardines.mat');
% save('in/psms/nTS_PSMS_38kHz.mat','nTS_dataset');
% load('in/psms/nTS_PSMS.mat'); % PSMS model save


%% (fish body tilt according to vertical axis) normal distribution

anglen = m_ta + std_ta * randn(1, Nang0); % probality density distribution (PDF)
% Truncate at 95% the PDF :
anglen = anglen(anglen >=  m_ta - 3*std_ta & anglen <= m_ta + 3*std_ta);
Nang = length(anglen);
% figure
% nbins = 100;
% histogram(anglen,nbins,'Normalization','probability'); 
% title('Tilt angle distribution'); xlabel('Tilt angle (�)'); ylabel('relative number of observations')

% Mean over angle distribution for each fish
nTS_tiltAveraged_dataset = zeros(Nfish,Nmodel_max,Nfreq);
for i_fish = 1:Nfish % fish selection
for i_model = models_ON % model selection
for i_freq = 1:Nfreq % frequency selection
    nTS_tmp = nTS_dataset(i_fish, i_model,:,:); % we select one fish and one model
    nTS_tmp = reshape(nTS_tmp,Nfreq,Ntif); % dim : freq * angle
    if i_model == 4
        nTS_anglen_dataset_tmp = interp1(tif_MyFrame-bladder_tilt(i_fish), nTS_tmp(i_freq,:), anglen, 'nearest','extrap');
    else
        nTS_anglen_dataset_tmp = interp1(tif_MyFrame, nTS_tmp(i_freq,:), anglen, 'nearest','extrap');
    end
    nTS_tiltAveraged_dataset_tmp = 10*log10(mean(10.^(nTS_anglen_dataset_tmp/10)));
    nTS_tiltAveraged_dataset(i_fish,i_model,i_freq) = nTS_tiltAveraged_dataset_tmp;
end
end
end

%% Plot of ka dependancy and theta dependancy for each fish

i_freq4ThetaPlot = find(freqs == freq4ThetaPlot);

plot_list = zeros(Nmodel,Nfish);
% Legend for models :
model_legend = cell(1,length(models_ON));
for ii = 1:length(models_ON)
    model_legend{ii} = strcat(model_desc{models_ON(ii)}, '   ', mark_{models_ON(ii)});
end

% each fish is associated to one color :
set(0,'DefaultAxesColorOrder',jet(Nfish))

% Theta dependancy
fig1 = figure('Position',scrsz); hold all; grid on; ylim([-100 0]);
for i_model = models_ON
for i_fish = 1:Nfish % loop on fishes
nTS_tmp = nTS_dataset(i_fish, i_model,:,:);
nTS_tmp = reshape(nTS_tmp,Nfreq,Ntif);
if i_model == 4
    plot_list(i_model,i_fish) = plot(tif_MyFrame-bladder_tilt(i_fish), nTS_tmp(i_freq4ThetaPlot,:),'LineStyle',mark_{i_model});
else
    plot_list(i_model,i_fish) = plot(tif_MyFrame, nTS_tmp(i_freq4ThetaPlot,:),'LineStyle',mark_{i_model});
end
end
end



title(strcat('nTS versus incidence angle - Tomofish sardines - f=',num2str(freqs(i_freq4ThetaPlot)*1e-3),'kHz'));
xlabel('fish body tilt according to vertical axis [�]'); ylabel('length-normalised TS [dB]');
legend(fish_legend,'Location','south'); %display legend 1
annotation('textbox', [0.2,0.1,0.1,0.1],...
           'String',model_legend)

% bladder tilt vertical line plot
for i_fish = 1:Nfish
    y_lim = [-100, 0];
plot((-bladder_tilt(i_fish))*ones(size(y_lim)), y_lim, '-')
end

% ka dependancy
figure('Position',scrsz); hold on; grid on;
for i_model = models_ON
for i_fish = 1:Nfish % loop on fishes
plot(kL_distrib(i_fish,:)/2, squeeze(nTS_tiltAveraged_dataset(i_fish,i_model,:)),'LineStyle',mark_{i_model})
end
end
title(strcat('tilt-averaged nTS versus ka - Tomofish sardines - Tilt distribution (m=',num2str(m_ta),'�, std=',num2str(std_ta),'�)'));
xlabel('ka with a the semi-length'); ylabel('tilt-averaged length-normalised TS [dB]');
legend(fish_legend,'Location','northeast');
annotation('textbox', [0.2,0.1,0.1,0.1],...
           'String',model_legend)
       
% difference between models
nTS_fishAll_dataset = zeros(Nmodel_max,Nfish*Nfreq);
kavect=reshape(kL_distrib,Nfish*Nfreq,1);
[~,indexka]= sort(kavect);
for i_model = models_ON % model selection
    nTS_tmp = nTS_tiltAveraged_dataset(:, i_model,:);
    nTS_tmp = reshape(nTS_tmp,Nfish*Nfreq,1); % dim : fish * freq
    nTS_fishAll_dataset(i_model,:) = nTS_tmp(indexka);
end

figure('Position',scrsz); hold on; grid on;
for i_model = models_ON
plot( kavect(indexka)/2, (nTS_fishAll_dataset(i_model,:)-nTS_fishAll_dataset(5,:)),'Color',facecolor_{i_model})
% plot( kavect(indexka)/2, (nTS_fishAll_dataset(i_model,:)),'Color',facecolor_{i_model})
end
title(strcat('nTS relative to deformed cylinder model - Tomofish sardines - Tilt distribution (m=',num2str(m_ta),'�, std=',num2str(std_ta),'�)'));
xlabel('ka with a the semi-length'); ylabel('Relative tilt-averaged length-normalised TS [dB]');
legend(model_legend,'Location','northeast');
       


%% Links between mean TS and bladder curvature angle
figure('Position',scrsz); hold on; grid on;
i_freq = i_freq4ThetaPlot;
plot_list2 = zeros(i_model,i_fish);
for i_model = models_ON
for i_fish = 1:Nfish % loop on fishes
plot_list2(i_model,i_fish) = scatter(gamma_max(i_fish),nTS_tiltAveraged_dataset(i_fish,i_model,i_freq),200,'MarkerEdgeColor','k','MarkerFaceColor',facecolor_{i_model});
end
end
xlabel('Curvature angle [�]'); ylabel('mean nTS'); % legend(plot_list2(:,1),model_desc(models_ON),'Location','northwest');
title(strcat('f=',num2str(freqs(i_freq)*1e-3),'kHz'))

%% DeformedCylinder_TSmax_Position
DefCyl_TS = nTS_dataset(:,i_model_deformedcyl,:,:); % dim : Nfish x 1 x Nfreq x Ntif
DefCyl_TS = reshape(DefCyl_TS,Nfish,Nfreq,Ntif); % dim : Nfish x Nfreq x Ntif

[DefCyl_TSmax_Value,DefCyl_TSmax_Position] = max(DefCyl_TS,[],3); % dim : Nfish x Nfreq
% KRM_TSmax_Position : angle value for TSmax with KRM model, for every fish and frequency

figure('Position',scrsz); hold on; grid on;
for i_fish = 1:Nfish 
plot(freqs*1e-3,tif_MyFrame(DefCyl_TSmax_Position(i_fish,:)))
end
for i_fish = 1:Nfish 
plot(freqs*1e-3,-bladder_tilt_LS(i_fish)*ones(size(freqs)),'--')
end
for i_fish = 1:Nfish 
plot(freqs*1e-3,-bladder_tilt_edge(i_fish)*ones(size(freqs)),':')
end
title('Deformed cylinder TS_{max} position according to frequency');
xlabel('Frequency [kHz]'); ylabel('TSmax position [angle �]');
legend(fish_legend,'Location','southwest');


