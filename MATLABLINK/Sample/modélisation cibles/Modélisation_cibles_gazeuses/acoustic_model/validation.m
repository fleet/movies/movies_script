
clear *; close all; clc;
dbstop if error
addpath(genpath(pwd)) % add path for subfolder 'my_functions' and for input data (swimbladder *.stl)
cd(fileparts(mfilename('fullpath'))); % set the current Matlab folder = script directory

%% Stanton88 modal serie cylinder + Ye97 end effect validation

% Parameters for fig. 2, 3.a and 4.a
ti = 0:0.1:90;
g = [10, 0.00129, 1.04, 1.043]; 
h = [10, 0.23, 1.04, 1.052];
L = [0.12, 0.1, 0.1, 0.0164]; % cylinder length (m)
b = [0.0235, 0.01, 0.01, L(4)/14.81]; % cylinder radius (m)
Lb = L./b; % shape flattening
kb = [40 10 1];
kL = Lb(1:3).*kb;


% Figure 2 from Ye1997 : rigid cylinder
i_par = 3;
nsigma_St88 = cylinder_model_ka( kL(i_par), Lb(i_par), ti, g(i_par), h(i_par), 1 ); % cylinder body (st88)
TS_St88 = 10*log10(nsigma_St88 * L(i_par)^2);
TS_St88 = squeeze(TS_St88);
nsigma_Ye97 = cylinder_model_ka( kL(i_par), Lb(i_par), ti, g(i_par), h(i_par), 2 ); % cylinder body (ye97)
TS_Ye97 = 10*log10(nsigma_Ye97 * L(i_par)^2);
TS_Ye97 = squeeze(TS_Ye97);
nsigma_Ye97fend = cylinder_model_ka( kL(i_par), Lb(i_par), ti, g(i_par), h(i_par), 4 ); % total scattering
TS_total = 10*log10(nsigma_Ye97fend * L(i_par)^2);
TS_total = squeeze(TS_total);

figure(1)
hold on; grid on; ylim([-120 0]);
plot(ti, TS_Ye97, 'k--'); 
plot(ti, TS_total, 'k-'); 
plot(ti, TS_St88, 'k:'); 
title(strcat('Backscattering from rigid cylinder : kb=',num2str(kb(i_par)))); xlabel('Angle (degree)'); ylabel('TS (dB)');
legend('Cylinder body only - Ye97', 'Total scattering (end effect)', 'Cylinder body only - St88')


% For getting figure 3.a and figure 4.a from Ye1997, we have to fix ti angle and modify ts angle (need to modify cylinder_model_ka script)
% i_par = 2; % i_par = 3;
% ylim([-120 -20]); ylim([-120 -40]);


% Figure 5 from Miyashita1996
kL_Miyashita1996 = 2*pi*L(4)/1500*[50, 120, 200]*1e3;
ti = 0:0.1:180;
i_par = 4;
nsigma_St88 = cylinder_model_ka( kL_Miyashita1996, Lb(i_par), ti, g(i_par), h(i_par), 1 ); % cylinder body
nTS_St88 = 10*log10(nsigma_St88);
nTS_St88 = squeeze(nTS_St88);

figure(2)
hold on; grid on; ylim([-140 -20]); xlim([-90 90])
plot(ti-90, nTS_St88(1,:), 'k-', 'LineWidth', 1); 
plot(ti-90, nTS_St88(2,:), 'k--', 'LineWidth', 1);
plot(ti-90, nTS_St88(3,:), 'k:', 'LineWidth', 1); 
title('R�tro-diffusion d''un cylindre fluide');  % title('Backscattering from weak cylinder'); 
xlabel('Angle [�]'); ylabel('nTS [dB]');
legend('50kHz', '120kHz', '200kHz')
set(gca,'FontSize',12)

%% % Validation of Do and Surti method (1990)
clear *;

% Inputs : Environment parameters + Fish data
% Environment parameters
g  = 0.00129; % relative mass density
c0 = 1483;    % sound speed (m/s) outside the cylinder
c1 = 340;     % sound speed (m/s) inside the cylinder
h  = c1/c0;   % relative sound speed

freqs = [38.1, 49.6, 68.4, 120.4]*1e3; % frequency (Hz) (Table I 1st column from Do&Surti1990)
Nfreqs = length(freqs);
tis = 15:0.1:165; % incident angle (�)

% In-situ data of 15 gadoids - Foote1985 - Table I from Do&Surti1990 :
fishDataSet = importdata('datafish1980.mat');
fishDataSet = fishDataSet.';
Ln = fishDataSet(3,:)*1e-2;    % fish bladder length (m)
as = fishDataSet(4,:)*1e-2;    % cylinder (with equivalent surface) radius (m)
Ln_fish_cm = fishDataSet(2,:); % fish body length (cm)
Nfish = size(fishDataSet,2);   % number of fish
Lb_distrib = Ln ./ as;         % fish bladder flattening
[freqs_mtx, Ln_mtx] = ndgrid(freqs, Ln); % dim : Nfreqs x Nfish
kL_distrib = 2*pi*freqs_mtx.*Ln_mtx/c0; % kL computation using frequency and bladder length
kL_distrib = reshape(kL_distrib, Nfish*Nfreqs, 1);

nsigma_Do90 = cylinder_model_ka( kL_distrib, Lb_distrib, tis, g, h, 6 ); % dim: (Nfreq x Nfish) x 1 x length(anglen)

% Tilt angle normal distribution parameters :
Nang = 10000; % number of tilt angles
% Values from Olsen1971 :
m_ta = 90 + 4.4; % tilt angle mean (�)
std_ta = 16; % tilt angle std (�)
ota_fsb = -5; % offset tilt angle of the swimbladder relative to the fish body (�)
anglen = m_ta + ota_fsb + std_ta * randn(1, Nang); % probability density distribution (PDF)
% Truncate at 95% the PDF :
anglen = anglen(anglen >=  m_ta + ota_fsb - 3*std_ta & anglen <= m_ta + ota_fsb + 3*std_ta);
Nang = length(anglen);

% mean TS for each fish
TS_Do90_AllFish = zeros(Nfish,Nfreqs);

% average over angle distribution
for i_fish = 1:Nfish % loop on fishes
nsigma_Do90_OneFish = zeros(Nfreqs,Nang);
for i_freq = 1:Nfreqs
    nsigma_Do90_OneFishOneFreq = squeeze(nsigma_Do90((i_fish - 1)*Nfreqs + i_freq,i_fish,:)); % model TS value
    % interpolation according to angle :
    nsigma_Do90_OneFish(i_freq,:) = interp1(tis, nsigma_Do90_OneFishOneFreq, anglen, 'nearest','extrap'); % dim : freq x anglen
end
nsigma_Do90_OneFish_mean = mean(nsigma_Do90_OneFish,2); % dim : freq
TS_Do90_AllFish(i_fish,:) = 10*log10(nsigma_Do90_OneFish_mean' * Ln(i_fish)^2);

end

% Validation  Do and Surti method 
% control of results (Table II from Do&Surti1990, equivalent surface)

b20_Do90 = zeros(Nfreqs,1);

for i_freq = 1:Nfreqs
b20_Do90(i_freq) = polyfit(zeros(Nfish,1),TS_Do90_AllFish(:,i_freq)-20*log10(Ln_fish_cm.'),0);

str2 = sprintf('Frequency %2.0f kHz \nb20 mod�le Do&Surti : %2.1f [dB] \n', freqs(i_freq)*1e-3, b20_Do90(i_freq));
disp(str2);
end

% We get the same results as the 4th column from table II from Do&Surti1990
figure(3)
plot(freqs*1e-3, b20_Do90,'x--'); grid on;
title('b_{20} - Do&Surti acoustic model'); xlabel('Frequency (kHz)'); ylabel('b_{20} [dB]');


%% Validation of bent cylinder (Stanton 1989)

% Figure 6 : Bent cylinder, fluid curve
kb = 0.05:0.01:13;
Lb = 10.5;
ti_normal = 180;
g = 1.043; h = 1.052;
gamma_max = 1*180/pi;
nmax = 20;
nsigma_ = bentcylinder_model( kb*Lb, Lb, ti_normal, g, h, gamma_max, nmax ); % keep the 20 first modal terms
nsigma_ = squeeze(nsigma_);
nTS_ = 10*log10(nsigma_);
figure(4)
subplot 121
semilogx(kb,nTS_,'k'); axis tight; xlim([0.01 25]); ylim([-80 10]); xlabel('kb'); ylabel('nTS [dB]'); title('Fig. 6.c')
set(gca,'XTick', [0.1 1 10])

% Figure 8 : solid line
kb = 0.1:0.01:13;
Lb = 10.5;
ti_normal = 180;
g = 1.043; h = 1.052;
gamma_max = 1*180/pi;
nmax = 6;
nsigma_ = bentcylinder_model( kb*Lb, Lb, ti_normal, g, h, gamma_max, nmax ); % keep the 6 first modal terms
nsigma_ = squeeze(nsigma_);
nTS_ = 10*log10(nsigma_);
subplot 122
semilogx(kb,nTS_,'k'); axis tight; xlim([0.1 25]); ylim([-70 -10]); xlabel('kb'); ylabel('nTS [dB]'); title('Fig. 8')
set(gca,'XTick', [0.1 1 10])

%% KRM - Reeder2004 figure 11

ti_Reeder2004 = -40:0.01:40; % incidence angle (�)
ti_Reeder2004forKRM = 90 + ti_Reeder2004;
% soft boundary conditions :
c0 = 1500;
c1 = 0;
g  = 0; % relative mass density
L_shape = 0.08; % m
b_shape = 0.0065;
kb1 = 1;
kb5 = 5;
f1 = c0*kb1/(2*pi*b_shape);
f5 = c0*kb5/(2*pi*b_shape);

krm_file_path = 'in/ImageDigitized/';
krm_file = 'shape_Reeder2004_fig11_KRMformat.mat';
krm_file2 = 'ellipsoid.mat';

sigma_KRM_kb1 = KRM_model_bladderOnly( f1, ti_Reeder2004forKRM, c0, c1, g, krm_file_path, krm_file );
nTS_KRM_Reeder2004_kb1 = 10*log10(sigma_KRM_kb1 ./ (L_shape/2)^2); % dim: Nfreq x 1 x length(ti_list)
nTS_KRM_Reeder2004_kb1 = squeeze(nTS_KRM_Reeder2004_kb1); % dim : Nfreq x length(ti_list)

sigma_KRM_kb5 = KRM_model_bladderOnly( f5, ti_Reeder2004forKRM, c0, c1, g, krm_file_path, krm_file );
nTS_KRM_Reeder2004_kb5 = 10*log10(sigma_KRM_kb5 ./ (L_shape/2)^2); % dim: Nfreq x 1 x length(ti_list)
nTS_KRM_Reeder2004_kb5 = squeeze(nTS_KRM_Reeder2004_kb5); % dim : Nfreq x length(ti_list)

figure; hold on;
plot(ti_Reeder2004,nTS_KRM_Reeder2004_kb1);
plot(ti_Reeder2004,nTS_KRM_Reeder2004_kb5);
legend(strcat('kb=',num2str(kb1)),strcat('kb=',num2str(kb5)));
xlabel('tilt angle (�)'); ylabel('nTS (dB)'); title('Shape #2 - fig. 12 from Reeder2004');

% ti_Macaulay = 90:-0.1:40;
% sigma_KRM_kb5_Macaulay2013 = KRM_model_bladderOnly( f5, ti_Macaulay, c0, c1, g, krm_file_path, krm_file2 );
% nTS_KRM_kb5_Macaulay2013 = 10*log10(sigma_KRM_kb5_Macaulay2013 ./ 0.1487^2); % dim: Nfreq x 1 x length(ti_list)
% nTS_KRM_kb5_Macaulay2013 = squeeze(nTS_KRM_kb5_Macaulay2013); % dim : Nfreq x length(ti_list)
% 
% figure; 
% plot(ti_Macaulay,nTS_KRM_kb5_Macaulay2013);

%% Angular validity field for Stanton1988, based on Ye1997 results
clear *

g  = 0.00129; % relative mass density
c0 = 1483;    % sound speed (m/s) outside the cylinder
c1 = 340;     % sound speed (m/s) inside the cylinder
h  = c1/c0;   % relative sound speed

Lsb = 6.6*1e-2; % Lfish2Lsb * Lf; % swimbladder length (m)
L_b_sb = 2 * 12.4; % ratio of the swimbladder length and the swimbladder radius
L_b_sb_ell = 2 * 9.4; 
f = 120 * 1e3; %(0.1:1:200)*1e3; % from 18kHz to 200kHz
ti_2 = (0:0.5:50)';
ti_1 = 90 - ti_2;

kL = (2*pi/c0) * Lsb * f; % wave number x swimbladder length
kb = kL / L_b_sb; % wave number x swimbladder length

% Figure 3 from Ye1997 : rigid cylinder
i_par = 1;
nsigma_St88 = cylinder_model_ka( kL, L_b_sb, ti_1, g, h, 1 ); % cylinder body (st88)
TS_St88 = 10*log10(nsigma_St88 * Lsb^2);
TS_St88 = squeeze(TS_St88);
nsigma_Ye97fend = cylinder_model_ka( kL, L_b_sb, ti_1, g, h, 4 ); % Ye97 with end effects
TS_Ye97 = 10*log10(nsigma_Ye97fend * Lsb^2);
TS_Ye97 = squeeze(TS_Ye97);

figure(1)
hold on; grid on; ylim([-100 -20]);
plot(ti_1, TS_St88, 'k-', 'LineWidth', 1); 
plot(ti_1, TS_Ye97, 'k--', 'LineWidth', 1); 

title('Effets des bords d''un cylindre fluide sur le TS'); xlabel('Angle [�]'); ylabel('TS [dB]');
legend('Contribution partie centrale - Stanton1988', 'Ajout des effets de bord - Ye1997')


