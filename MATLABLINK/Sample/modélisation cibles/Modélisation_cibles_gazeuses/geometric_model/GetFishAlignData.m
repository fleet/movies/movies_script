% Convert fish coordinates data (fish body or fish bladder) in a conventional format

% INPUT :
%     - *.mat file containing fish coordinates (also called "vertices") and
%        faces
%     - fish coordinates are in an arbitrary frame
%     - fish coordinates unit : arbitrary 
% OUTPUT :
%     - *.mat file containing fish coordinates (also called "vertices") and
%        faces
%     - fish coordinates are in a reference frame defined in figure 1 from
%        "The average three-dimensional target strength of fish
%        by spheroid model for sonar surveys", Yong Tang, Yasushi 
%        Nishimori, and Masahiko Furusawa, released in 2009 in ICES 
%        Journal of Marine Science.
%     - fish coordinates unit : meter (m) 

% filename related to the current dataset :
all_filename_in = getDatasetFilenames( path_raw_mat, d_desc{d} );
% error if all_filename_in is empty :
if isempty(all_filename_in)
    disp(strcat('No *.mat file containing fish coordinates for dataset ', ...
        d_desc{d},' is present in repertory ',path_raw_mat));
    return;
end

all_filename_obj_stl = getDatasetFilenames( path_raw_obj_stl, d_desc{d} );
filename_obj_stl = all_filename_obj_stl{1}; %  OBJ or STL filename
file_ext = filename_obj_stl(end-2:end); % file extension

% transfer matrix from Subatech frame into the reference frame
Pmtx_idx = load(strcat('transfer_matrix_',d_desc{d},'.txt')); % load the file that indicates which transfer matrix to use for each fish
P_raw_ref_list = {[0 0 1; -1 0 0; 0 -1 0], [0 0 1; 1 0 0; 0 1 0], [0 0 1; 0 -1 0; -1 0 0],[0 0 -1; 1 0 0; 0 1 0],[0 -1 0; -1 0 0; 0 0 -1],[0 0 -1; 0 1 0; 1 0 0],[0 0 -1; 0 -1 0; -1 0 0],[0 0 -1; -1 0 0; 0 1 0],[0 0 -1; -1 0 0; 0 -1 0],[0 0 1; -1 0 0; 0 1 0]};

% Voxel size file containing fish index (first column) and
% voxel size in meter (m) (second column) (only for dataset 2) :
if strcmp(file_ext, 'stl') % read STL voxel size list
    VoxelSizeList = load(strcat('in/voxel_size_',d_desc{d},'.txt'));
end

for i_file = 1:size(all_filename_in,1) % 2 first lines are not filename
    filename_in = all_filename_in{i_file}; % old frame file name
    fullfilename_in = strcat(path_raw_mat,'/',filename_in);
    load(fullfilename_in);
    V_in = fv.vertices;
    F_in = fv.faces;
    
    filename_in_split = strsplit(filename_in,{'_','.'});
        i_fish = str2double(filename_in_split{end-1}); % get fish index
    % Get the conventional unit : meter
    if strcmp(file_ext, 'obj')
        V_in = V_in * 1e-3; % mm to m
    elseif strcmp(file_ext, 'stl')
        
        voxel_size = VoxelSizeList(VoxelSizeList==i_fish,2); % get voxel size (m)
        
        V_in = V_in * voxel_size; % vertices in m
    end
    % Transfer into the conventional frame (details in the script
    % description) :
    P_raw_ref = P_raw_ref_list{Pmtx_idx(Pmtx_idx(:,1) == i_fish, 2)};
    V_out = (P_raw_ref*V_in')';
    F_out = (abs(P_raw_ref)*F_in')';

    fv = struct('faces',F_out,'vertices',V_out);
    filename_out = strrep(filename_in, '.mat', '_pr.mat'); % MAT filename
    fullfilename_out = strcat(path_processed, filename_out); % add path
    save(fullfilename_out, 'fv'); % export MAT file
    disp(strcat('The file "',filename_out, '" has been saved in "',path_processed, '" directory.'))
end


