% Script function : Convert raw data in *.stl or *obj format into *.mat format
% Data : fish body and fish swimbladder geometry

% error if all_filename_in is empty :
if isempty(MyFileNameList)
    disp(strcat('No *.stl or *.obj file for dataset ', ...
        d_desc,' is present in repertory ',path_raw_obj_stl));
    return;
end

for i_file = 1:size(MyFileNameList,1) 

    filename_obj_stl = MyFileNameList{i_file}; %  OBJ or STL filename
    file_ext = filename_obj_stl(end-2:end); % file extension

    % read data from *.stl or *obj file
    if strcmp(file_ext, 'obj') % read OBJ file
        [vertices, tessellation] = readObj(filename_obj_stl); 
    elseif strcmp(file_ext, 'stl') % read STL file
        [vertices, tessellation] = readStl(filename_obj_stl); 
    end
    
    % export data with *.mat format
    fv = struct('faces',tessellation,'vertices',vertices);
    filename_mat = strrep(filename_obj_stl, file_ext, 'mat'); % MAT filename
    fullfilename_mat = strcat(path_raw_mat, '/', filename_mat);     % add path
    save(fullfilename_mat, 'fv');                              % export MAT file
    disp(strcat('The file "',filename_mat, '" has been saved in "',path_raw_mat, '" directory.'));

end


