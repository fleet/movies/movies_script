% V1.00
clear *; close all;
dbstop if error
addpath(genpath(pwd)) % add path for subfolder 'my_functions' and for input data (swimbladder *.stl)
cd(fileparts(mfilename('fullpath'))); % set the current Matlab folder = script directory

%% Dataset choice :
d = menu('Which set of data use ?', ...
    'Sardines from Aquarium La Rochelle2 (13/11/2014)', ...
    'Herrings from La Manche - January 2013 - depth < 30m', ...
    'Sardines from Pelgas2014 (juillet 2014)', ...
    'Sardines from Kanadeven (02/10/2014)', ...
    'Sardines from Kanadeven (06/10/2014)', ...
    'Sardines from Pelgas2014 (15/06/2015)', ...
    'Anchois from Aquarium La Rochelle2 (24/02/2016)');
close;
if d == 0 % if the user close the multiple-choice dialog box,
    return % the script is stopped
end

d_desc = {'AqLRNov2014', 'Manche2013', 'Pelgas14Juillet2014','Pelgas142octobre2014','Pelgas146octobre2014','Pelgas14Juin2015','AqLR3'};

% path for input and output files :
path_raw_obj_stl = 'in/0_RawDataOBJ-STL';
path_raw_mat   = 'in/1_RawDataMAT';        
path_processed = 'in/2_RightFrameDataMAT/';
path_export_caracs = '../acoustic_model/in/';
path_export_figure = 'out/figures/';

% filename related to the current dataset :
MyFileNameList = getDatasetFilenames( path_raw_obj_stl, d_desc{d} );

MyFishList = getMyFishInfo(MyFileNameList) ; 
Nfish = length(MyFishList);

%% Get the correct data format and alignment
% Convert *.stl and *.obj files into *.mat files :
% GetMatlabFormat; 
% Transfer data into the conventional fish frame :
GetFishAlignData;

%% Variables

% Considering the swimbladder as a slender body, only parts of the
% swimbladder above p% of the equivalent radius are kept for some
% computations
p = '0';
pp = str2double(p)/100;

for i_fish = 1:Nfish % loop (fish)

% current fish parameters :
MyFish = MyFishList{i_fish};
MyFishIdx = MyFish.idx; % true fish index
% relative path
fish_desc = strcat(MyFish.specie,'_',MyFish.dataset,'_',num2str(MyFishIdx));
fileName_body = strcat(MyFish.specie,'_body_',MyFish.dataset,'_',num2str(MyFishIdx),'_pr.mat');
fileName_bladder = strcat(MyFish.specie,'_bladder_',MyFish.dataset,'_',num2str(MyFishIdx),'_pr.mat');
fileName_caracteristics = strcat(path_export_caracs,MyFish.dataset,'_caracteristics');
path_export_KRMfile   = strcat('../acoustic_model/in/InputDataKRM/',fish_desc,'_KRMgeometry');
path_export_KRMfigure = strcat('out/figures/SliceViewKRM/',fish_desc);

tic
%% Fish geometric caracteristics

% Loading of real fish bladder
% unit : meter
% axis convention : Tang2009 (details in GetFishAlignData.m script)
% format : *.mat file
file_in_sb    = load(strcat(path_processed,fileName_bladder));
sb_shape      = file_in_sb.fv;
Nv_sb = size(sb_shape.vertices,1); % number of vertices for fish bladder

% Loading of real fish body
if MyFish.hasBody % if we have the body geometry (it's the case for dataset 1)
file_in_body  = load(fileName_body);
body_shape    = file_in_body.fv;
else % we don't have the body geometry, so we say that body geometry = bladder geometry
body_shape = sb_shape;
end
Nv_b  = size(body_shape.vertices,1); % number of vertices for fish body

% Swimbladder descriptors in ~fish frame and arbitrary origin
sb = GetBladderDimensions(sb_shape, body_shape, pp); % real swimbladder descriptors
Xc_slices = mean(sb.XYZminmax(:,1:2),2);
Zc_slices = mean(sb.XYZminmax(:,5:6),2);

% Bladder tilt computation :
[ tilt_fbc, tilt_sbc, tilt_fbup, tilt_sbup ] = GetTilt(sb, MyFish);
% angle convention for a direct frame :
tilt_fbup = abs(tilt_fbup); % bladder tilt (rad)
tilt_sbup = abs(tilt_sbup); % bladder tilt (rad)
tilt_fbc = abs(tilt_fbc); % bladder tilt (rad)
tilt_sbc = abs(tilt_sbc); % bladder tilt (rad)

% Bladder corvature parameters computation :
sb.CurvatureParameters = GetCurvature(sb, MyFish);

% Shift of vertices origin : defined as swimbladder center
Csb   = GetNewOrigin(sb.XYZminmax); % get new origin : bladder center
Ns    = size(sb.XYZminmax,1); % number of slices
body_shape.vertices = body_shape.vertices - repmat(Csb,Nv_b,1); % fish body vertices (m)
sb_shape.vertices   = sb_shape.vertices - repmat(Csb,Nv_sb,1); % fish bladder vertices (m)
sb.XYZminmax        = sb.XYZminmax - repmat(Csb([1 1 2 2 3 3]),Ns,1);% bladder slices min/max (m)
sb.X0p              = sb.X0p - Csb(1); % significant bladder origin (m)

% Slice fish and swimbladder for KRM computation (~fish axis and origin=fish head) :
ExportKRMFormat(sb_shape, body_shape, path_export_KRMfile, path_export_KRMfigure);

% Shift and rotation from fish frame towards swimbladder frame :
% ~bladder main axis and origin=bladder center)
Rot_fish2sb = [cos(tilt_fbup) 0 -sin(tilt_fbup); ...
                0 1 0; ...
                sin(tilt_fbup) 0 cos(tilt_fbup)];
vertices_bladderFrame   = (Rot_fish2sb*sb_shape.vertices')'; % vertices are now in the swimbladder frame
sb_shape.vertices = vertices_bladderFrame; % upload
sb.XYZminmax(:,[1 3 5]) = (Rot_fish2sb*sb.XYZminmax(:,[1 3 5])')'; % min/max for each slice are now in the swimbladder frame
sb.XYZminmax(:,[2 4 6]) = (Rot_fish2sb*sb.XYZminmax(:,[2 4 6])')'; % min/max for each slice are now in the swimbladder frame

% %%%%%%%%%%%%%%%% PUT minmax_slice in the SB frame !
sb.tilt_fbup = (tilt_fbup); % tilt angle between the fish and its swimbladder
sb.tilt_fbc = (tilt_fbc); % tilt angle between the fish and its swimbladder
sb.tilt_sbup = (tilt_sbup); % tilt angle between the fish and its swimbladder
sb.tilt_sbc   = (tilt_sbc); % tilt angle between the fish and its swimbladder

%% Geometric shape models and figures
D = GetAllBasicGeometry(sb, sb_shape, MyFish, path_export_figure);

toc

%% Export

% fish index, fish length, bladder length, bladder surface, bladder volume,
% bladder swelling ratio, bladder tilt (1), bladder tilt (2),
% bladder curvature angle, bladder bent length,
% bladder radius (Scst, cylinder) bladder radius (Scst, ellipsoid)

% All values in cm !
m2cm = 1e2; % convert meter values into centimeter values

export_sb_carac_cm = [MyFishIdx, sb.Lf*m2cm, sb.L*m2cm, sb.S*m2cm^2, sb.V*m2cm^3, sb.swelling_ratio, sb.tilt_fbup*180/pi, sb.tilt_fbc*180/pi ... % fish parameters
    sb.CurvatureParameters.gamma_max_fbup*180/pi, sb.CurvatureParameters.Lbc_fbup*1e2, ... % bent cylinder parameters
    D.data_cyl_ScstL.yy*m2cm, D.data_ell_ScstL.yy*m2cm, sb.Rmax*m2cm, sb.Rp*m2cm]; % geometric models radius (cm)

export_sbr_carac_cm = [MyFishIdx, sb.Lf*m2cm, 2*D.data_cyl_ScstLp.xx*m2cm, sb.S*m2cm^2, sb.V*m2cm^3, sb.swelling_ratio, sb.tilt_sbup*180/pi, sb.tilt_sbc*180/pi ... % fish parameters
    sb.CurvatureParameters.gamma_max_sbup*180/pi, 1.018*2*D.data_cyl_ScstLp.xx*m2cm, ... % bent cylinder parameters
    D.data_cyl_ScstLp.yy*m2cm, D.data_ell_ScstLp.yy*m2cm]; % geometric models radius (cm)

ExportData( fileName_caracteristics, export_sb_carac_cm )
% ExportData( strcat(fileName_caracteristics,'r'), export_sbr_carac_cm )

end