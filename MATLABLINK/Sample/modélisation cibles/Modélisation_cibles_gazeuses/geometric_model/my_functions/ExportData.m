function ExportData( path_export, data )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

path_export_mat = strcat(path_export,'.mat');
path_export_txt = strcat(path_export,'.txt');
my_fish = data(1);
% Create array
if exist(path_export_mat,'file') % if the *.mat file table_DoSurti1990 exists
    old_data = importdata(path_export_mat);
    old_array = old_data.array_;
    new_array = old_array;
    if any(old_array(:,1) == my_fish) % if there is already data about the current fish
        idx_old_data = (old_array(:,1) == my_fish);
        % upload of old data
        new_array(idx_old_data,:) = data;
    else
        % new line with new data
        new_array(size(old_array,1)+1,:) = data;
    end
    % sort data according to the first column (fish index)
    new_array = sortrows(new_array,1); 
else % if the *.mat file table_DoSurti1990 does not exist
    new_array = data;
end

array_ = round(new_array*1000) ./ 1000; % double numbers to 2 decimal
% Create table
table_ = array2table(array_,...
    'VariableNames',{'FishIndex', 'FishTotalLength', 'BladderLength', 'BladderSurface', ...
    'BladderVolume', 'SwellingRatio', 'BladderTiltUpPart', 'BladderTiltCenters', ...
    'CurvatureAngleUpPart', 'BladderBentLengthUpPart', ...
    'CylinderRadius', 'EllipsoidRadius', 'MaxRadius', 'MeanRadius'});

save(path_export_mat,'array_','table_');
writetable(table_,path_export_txt,'Delimiter',' ')


end

