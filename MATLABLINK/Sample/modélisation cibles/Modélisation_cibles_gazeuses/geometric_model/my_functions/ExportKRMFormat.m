function ExportKRMFormat(sb_shape, fish_shape, path_export_data, path_export_figure)
% Slice fish body and fish bladder and export it in a
% correct format for KRM script

% Fish bladder geometry
P_sb = sb_shape.vertices;   % points coordinates (x,y,z)
% Fish body geometry
P_b  = fish_shape.vertices; % points coordinates (x,y,z)
Nv_sb = size(P_sb,1);
Nv_b = size(P_b,1);

%% Transfer from our conventional frame into KRM frame 
% KRM frame (origin+directions) is defined in fig. 3 from Clay&Horne1994

% Switch of x-axis and z-axis directions :
switch_axis = [-1,1,-1];
P_sb_KRMalign = P_sb .* repmat(switch_axis,Nv_sb,1);
P_b_KRMalign  = P_b  .* repmat(switch_axis,Nv_b,1);

% New origin for KRM frame (fig. 3 Clay&Horne1994): 
%   X = fish head
%   Y = bladder center (nothing to change)
%   Z = bladder top
% KRM frame center coordinates :
C_KRM = [min(P_b_KRMalign(:,1)), 0, max(P_sb_KRMalign(:,3))]; % fish head coordinates 
% Origin shift :
P_sb_KRMformat = P_sb_KRMalign - repmat(C_KRM,Nv_sb,1);
P_b_KRMformat  = P_b_KRMalign  - repmat(C_KRM,Nv_b,1);

%% Fish body and bladder slicing
% Note : slicing done in fish frame

% Adjust slice order according to new KRM x-axis
P_sb_KRMformat = flipud(P_sb_KRMformat);
P_b_KRMformat  = flipud(P_b_KRMformat);

L_sb = max(P_sb_KRMformat(:,1))-min(P_sb_KRMformat(:,1));       % global swimbladder length computation
L_b = max(P_b_KRMformat(:,1))-min(P_b_KRMformat(:,1));       % global fish length computation
Ns_sb = ceil(L_sb/1e-3); % we divide the swimbladder into slices of 1mm according to x-axis
Ns_b = ceil(L_b/1e-3); % we divide the fish into slices of 1mm according to x-axis
minmax_slice_sb = sb_slicing( P_sb_KRMformat, Ns_sb );
minmax_slice_b = sb_slicing( P_b_KRMformat, Ns_b );

x_sb = (minmax_slice_sb(:,1) + minmax_slice_sb(:,2)) / 2; % x coordinates of the slice
x_b  = (minmax_slice_b(:,1)  + minmax_slice_b(:,2))  / 2;
w_sb = minmax_slice_sb(:,4) - minmax_slice_sb(:,3); % thickness of the slice
w_b  = minmax_slice_b(:,4)  - minmax_slice_b(:,3);
z_sb = minmax_slice_sb(:,5:6); % z boundaries of the slice
z_b  = minmax_slice_b(:,5:6);

% Limit the number of digits:
Ndigit = 4; % 0.1mm
x_b = limit_digit( x_b, Ndigit );
w_b = limit_digit( w_b, Ndigit );
z_b = limit_digit( z_b, Ndigit );
x_sb = limit_digit( x_sb, Ndigit );
w_sb = limit_digit( w_sb, Ndigit );
z_sb = limit_digit( z_sb, Ndigit );

%% Data export
save(path_export_data, 'x_sb', 'x_b', 'w_sb', 'w_b', 'z_sb', 'z_b');

%% Figure export
figure('Visible','off')

% convert m into mm
x_b = x_b*1e3;
w_b = w_b*1e3;
z_b = z_b*1e3;
x_sb = x_sb*1e3;
w_sb = w_sb*1e3;
z_sb = z_sb*1e3;

% Plot lateral view
subplot(2,1,1)
hold on

% Plot fish body
plot(x_b, z_b(:,1), 'k', x_b, z_b(:,2), 'k')            % Upper and lower
plot([x_b(1) x_b(1)], [z_b(1,1) z_b(1,2)], 'k')         % Left end
plot([x_b(end) x_b(end)], [z_b(end,1) z_b(end,2)], 'k') % Right end

% Plot fish bladder
plot(x_sb, z_sb(:,1), 'k', x_sb, z_sb(:,2), 'k')        % Upper and lower
plot([x_sb(1) x_sb(1)], [z_sb(1,1) z_sb(1,2)], 'k')     % Left end
plot([x_sb(end) x_sb(end)], [z_sb(end,1) z_sb(end,2)], 'k') % Right end

axis equal              % make axes equal
title('Lateral view')
ylabel('Height (mm)')
xlabel('Length (mm)')

% Plot dorsal view
subplot(2,1,2)
hold on

% Plot fish body
plot(x_b, w_b/2, 'k', x_b, -w_b/2, 'k')                 % Upper and lower
plot([x_b(1) x_b(1)], [-w_b(1)/2 w_b(1)/2], 'k')        % Left end
plot([x_b(end) x_b(end)], [-w_b(end)/2 w_b(end)/2], 'k')	% Right end

% Plot fish bladder
plot(x_sb, w_sb/2, 'k', x_sb, -w_sb/2, 'k')             % Upper and lower
plot([x_sb(1) x_sb(1)], [-w_sb(1)/2 w_sb(1)/2], 'k')    % Left end
plot([x_sb(end) x_sb(end)], [-w_sb(end)/2 w_sb(end)/2], 'k')    % Right end

axis equal
title('Dorsal view')
ylabel('Width (mm)')
xlabel('Length (mm)')

saveas(gcf,strcat(path_export_figure, '.png'))

end

