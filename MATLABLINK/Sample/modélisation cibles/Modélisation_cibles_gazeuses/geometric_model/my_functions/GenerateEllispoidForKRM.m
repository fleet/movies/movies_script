clear *; close all;


% Generate ellipsoid for KRM :

f = 38*1e3; % arbitrary frequency
ka = 12;
c0 = 1479.6;
a = ka*c0 / (2*pi*f);
ti = 90:-0.1:40;%90:0.1:140;

ab = 12; % flattening
b = a/12;
Cell = [a, 0, 0]; % ellipsoid center coordinates

Nslice = 20;%133; % Macaulay2013 for in situ data
Nslice2 = round(2*a/(50*1e-6)); % Macaulay2013 for ellipsoid
x_sb = linspace(0,2*a,Nslice2).'; % x coordinates of the slice


x_b  = [];
% w_sb = 2*b*ones(size(x_sb));
w_sb = 2*b*sqrt(abs(1-(x_sb-Cell(1)).^2/a^2)); % thickness of the slice
w_b  = [];
z_sb = 0.5*[-w_sb, w_sb]; % z boundaries of the slice
% z_sb = b*ones(size(x_sb,1),2); z_sb(:,2) = -b;
z_b  = [];

Ndigit = 4; % 0.1mm
x_sb = limit_digit( x_sb, Ndigit );
w_sb = limit_digit( w_sb, Ndigit );
z_sb = limit_digit( z_sb, Ndigit );

min(x_sb)
max(x_sb)


%% Data export
save('../ellipsoid.mat', 'x_sb', 'x_b', 'w_sb', 'w_b', 'z_sb', 'z_b');
% x_sb = x_sb-10;
% save('../ellipsoid2.mat', 'x_sb', 'x_b', 'w_sb', 'w_b', 'z_sb', 'z_b');
%% Figure export
figure(1)

% Plot lateral view
subplot(2,1,1)
hold on

% Plot fish bladder
plot(x_sb, z_sb(:,1), 'k', x_sb, z_sb(:,2), 'k')        % Upper and lower
plot([x_sb(1) x_sb(1)], [z_sb(1,1) z_sb(1,2)], 'k')     % Left end
plot([x_sb(end) x_sb(end)], [z_sb(end,1) z_sb(end,2)], 'k') % Right end

axis equal              % make axes equal
title('Lateral view')
ylabel('Height (cm)')

% Plot dorsal view
subplot(2,1,2)
hold on

% Plot fish bladder
plot(x_sb, w_sb/2, 'k', x_sb, -w_sb/2, 'k')             % Upper and lower
plot([x_sb(1) x_sb(1)], [-w_sb(1)/2 w_sb(1)/2], 'k')    % Left end
plot([x_sb(end) x_sb(end)], [-w_sb(end)/2 w_sb(end)/2], 'k')    % Right end

axis equal
title('Dorsal view')
ylabel('Width (cm)')
xlabel('Length (cm)')


soft_sigma = KRM_model_bladderOnly( f, ti, c0, 0, 0, '../', 'ellipsoid.mat' );
nTS = 10*log10(squeeze(soft_sigma) ./ a^2);

% soft_sigma2 = KRM_model_bladderOnly( f, ti, c0, 0, 0, '../', 'ellipsoid2.mat' );
% nTS2 = 10*log10(squeeze(soft_sigma2) ./ a^2);

figure; grid on; hold on;
plot(ti,nTS,'-');%plot(ti,nTS2,'-');
xlabel('Angle (�)'); ylabel('nTS [dB]'); title('Validation KRM model with Macaulay2013 fig. 4')