function D = GetAllBasicGeometry( sb, sb_mesh, MyFish, figure_path )
% Real swimbladder is approximated by basic geometries (cylinder and
% ellipsoid) with respect to swimbladder descriptors

% sb : swimbladder descriptors
% sb_mesh : real swimbladder meshed

% different path possibilities :
%   - real shape figure
%   - geometric shape figure
figure_path_realShape = strcat(figure_path,'real_shape/');
figure_path_geometricShape = strcat(figure_path,'geometric_shape/');

%% Geometric shape models
X0 = sb.XYZminmax(1,1); % default X0 for cylinders

% new shape boundaries computation
[Lp, Rp, X0_p] = deal(sb.Lp, sb.Rp, sb.X0p);

% HCC (Hemisphere+Cylinder+Cone model) swimbladder global geometric shape
% Haslett approximation
data_HCC = GetBasicGeometry( sb, 'HCC', '', Rp, 0, X0 );
D.HCC_full_shape = unique([data_HCC.X, data_HCC.Y, data_HCC.Z],'rows'); % delete identical values

% Cylinder %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Cylinder with arbitrary size
% D.data_cyl_RpLp   = GetBasicGeometry( sb, 'cylinder', 'arbitrary', Rp, Lp, X0_p );      % Rp Lp
% D.data_cyl_RpL    = GetBasicGeometry( sb, 'cylinder', 'arbitrary', Rp, sb.L, X0 );      % Rp L
% D.data_cyl_RmaxLp = GetBasicGeometry( sb, 'cylinder', 'arbitrary', sb.Rmax, Lp, X0_p ); % Rmax Lp
% D.data_cyl_RmaxL  = GetBasicGeometry( sb, 'cylinder', 'arbitrary', sb.Rmax, sb.L, X0 ); % Rmax L
% Cylinder with surface conservation
D.data_cyl_ScstLp = GetBasicGeometry( sb, 'cylinder', 'surface',   0, Lp, X0_p );       % Scst Lp
D.data_cyl_ScstL  = GetBasicGeometry( sb, 'cylinder', 'surface',   0, sb.L, X0 );       % Scst L
D.data_cyl_ScstRp = GetBasicGeometry( sb, 'cylinder', 'surface',   Rp, 0, X0 );         % Scst Rp - !!!!!!!!!! red�finir X0_p
% Cylinder with volume conservation
% D.data_cyl_VcstLp = GetBasicGeometry( sb, 'cylinder', 'volume',    0, Lp, X0_p );       % Vcst Lp
% D.data_cyl_VcstL  = GetBasicGeometry( sb, 'cylinder', 'volume',    0, sb.L, X0 );       % Vcst L
% D.data_cyl_VcstRp = GetBasicGeometry( sb, 'cylinder', 'volume',    Rp, 0, X0 );         % Vcst Rp - !!!!!!!!!! red�finir X0_p
% Bent cylinder with surface conservation
D.data_bcyl_ScstL  = GetBasicGeometry( sb, 'bentcylinder', 'surface',   0, sb.CurvatureParameters.Lbc_sbc, X0 );       % Scst Lbc
% D.data_bcyl_ScstLp = GetBasicGeometry( sb, 'bentcylinder', 'surface',   0, 1.018*Lp, X0_p );       % Scst Lbc

% Ellipsoid %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Ellipsoid with arbitrary size
% D.data_ell_RmaxLp   = GetBasicGeometry( sb, 'ellipsoid', 'arbitrary', sb.Rmax, Lp/2, 0 );   % Rmax Lp
% D.data_ell_RpLp     = GetBasicGeometry( sb, 'ellipsoid', 'arbitrary', Rp, Lp/2, 0 );        % Rp Lp
% D.data_ell_RmaxL    = GetBasicGeometry( sb, 'ellipsoid', 'arbitrary', sb.Rmax, sb.L/2, 0 ); % Rmax L
% D.data_ell_RpL      = GetBasicGeometry( sb, 'ellipsoid', 'arbitrary', Rp, sb.L/2, 0 );      % Rp L
% Ellipsoid with surface conservation
D.data_ell_ScstRmax = GetBasicGeometry( sb, 'ellipsoid', 'surface',   sb.Rmax, 0, 0 );      % Scst Rmax
D.data_ell_ScstRp   = GetBasicGeometry( sb, 'ellipsoid', 'surface',   Rp, 0, 0 );           % Scst Rp
D.data_ell_ScstLp   = GetBasicGeometry( sb, 'ellipsoid', 'surface',   0, Lp/2, 0 );         % Scst Lp
D.data_ell_ScstL    = GetBasicGeometry( sb, 'ellipsoid', 'surface',   0, sb.L/2, 0 );         % Scst L
% Ellipsoid with volume conservation
% D.data_ell_VcstRmax = GetBasicGeometry( sb, 'ellipsoid', 'volume',    sb.Rmax, 0, 0 );      % Vcst Rmax
% D.data_ell_VcstRp   = GetBasicGeometry( sb, 'ellipsoid', 'volume',    sb.Rp, 0, 0 );        % Vcst Rp
% D.data_ell_VcstLp   = GetBasicGeometry( sb, 'ellipsoid', 'volume',    0, Lp/2, 0 );         % Vcst Lp
% Ellipsoid with Least Squares
% We keep only the central shape of the swimbladder for avoinding artefacts with boundaries
keepvertices_p = sb.vertices2keep;
V_LS = sb_mesh.vertices(keepvertices_p, :); % vertices used for least squares computation
[ ~, abc_p, ~, ~ ]   = ellipsoid_fit( V_LS    , 2, 'yz' );
D.data_ell_LSp = GetBasicGeometry( sb, 'ellipsoid', 'arbitrary', abc_p(2), abc_p(1), 0 );   % LS with vertices inside Lp
% Observation = all data
[ ~, abc_all, ~, ~ ] = ellipsoid_fit( sb_mesh.vertices, 2, 'yz' );
D.data_ell_LS  = GetBasicGeometry( sb, 'ellipsoid', 'arbitrary', abc_all(2), abc_all(1), 0 );   % LS with all data

%% Plotting

% Considering the swimbladder as a slender body, only parts of the
% swimbladder above p% of the equivalent radius are kept for some
% computations
p = num2str(sb.p*100);

% % Cylinder : Rp Lp
% figureExport(sb_mesh, D.data_cyl_RpLp, figure_path_geometricShape, strcat('cylinder_perso_R',p,'_L',p),strcat('Cylinder - R_{cyl}=R_{',p,'%} - L_{cyl}=L_{',p,'%}'), MyFish);
% % Cylinder : Rp L
% figureExport(sb_mesh, D.data_cyl_RpL, figure_path_geometricShape, strcat('cylinder_perso_R',p,'_L'),strcat('Cylinder - R_{cyl}=R_{',p,'%} - L_{cyl}=L'), MyFish);
% % Cylinder : Rmax Lp
% figureExport(sb_mesh, D.data_cyl_RmaxLp, figure_path_geometricShape, strcat('cylinder_perso_Rmax_L',p),strcat('Cylinder - R_{cyl}=R_{max} - L_{cyl}=L_{',p,'%}'), MyFish);
% % Cylinder : Rmax L
% figureExport(sb_mesh, D.data_cyl_RmaxL, figure_path_geometricShape, strcat('cylinder_perso_Rmax_L'),strcat('Cylinder - R_{cyl}=R_{max} - L_{cyl}=L'), MyFish);
% % Cylinder : Scst Lp
figureExport(sb_mesh, D.data_cyl_ScstLp, figure_path_geometricShape, strcat('cylinder_surface_L',p),strcat('Cylinder - S_{cst} - L_{cyl}=L_{',p,'%}'), MyFish);
% Cylinder : Scst L
figureExport(sb_mesh, D.data_cyl_ScstL, figure_path_geometricShape, 'cylinder_surface_L','Cylindre droit - S_{cst} - L_{cyl}=L', MyFish);
% % Cylinder : Vcst L
% figureExport(sb_mesh, D.data_cyl_VcstL, figure_path_geometricShape, 'cylinder_volume_L','Cylinder - V_{cst} - L_{cyl}=L', MyFish);
% % Cylinder : Vcst Lp 
% figureExport(sb_mesh, D.data_cyl_VcstLp, figure_path_geometricShape, strcat('cylinder_volume_L',p),strcat('Cylinder - V_{cst} - L_{cyl}=L_{',p,'%}'), MyFish);
% % Cylinder : Scst Rp
% figureExport(sb_mesh, D.data_cyl_ScstRp, figure_path_geometricShape, strcat('cylinder_surface_R',p),strcat('Cylinder - S_{cst} - R_{cyl}=R_{',p,'%}'), MyFish);
% % Ellipsoid : Rmax Lp
% figureExport(sb_mesh, D.data_ell_RmaxLp, figure_path_geometricShape, strcat('ellipsoid_perso_Rmax_L',p),strcat('Ellipsoid - a=L_{',p,'%}/2 - b=R_{max}'), MyFish);
% % Ellipsoid : Rp Lp
% figureExport(sb_mesh, D.data_ell_RpLp, figure_path_geometricShape, strcat('ellipsoid_perso_R',p,'_L',p),strcat('Ellipsoid - a=L_{',p,'%}/2 - b=R_{',p,'%}'), MyFish);
% % Ellipsoid : Rmax L
% figureExport(sb_mesh, D.data_ell_RmaxL, figure_path_geometricShape, 'ellipsoid_perso_Rmax_L','Ellipsoid - a=L/2 - b=R_{max}', MyFish);
% % Ellipsoid : Rp L
% figureExport(sb_mesh, D.data_ell_RpL, figure_path_geometricShape, strcat('ellipsoid_perso_R',p,'_L'),strcat('Ellipsoid - a=L/2 - b=R_{',p,'%}'), MyFish);
% % Ellipsoid : Vcst Rmax
% figureExport(sb_mesh, D.data_ell_VcstRmax, figure_path_geometricShape, 'ellipsoid_volume_Rmax','Ellipsoid - V_{cst} - b=R_{max}', MyFish);
% % Ellipsoid : Scst Rmax
% figureExport(sb_mesh, D.data_ell_ScstRmax, figure_path_geometricShape, 'ellipsoid_surface_Rmax','Ellipsoid - S_{cst} - b=R_{max}', MyFish);
% % Ellipsoid : Vcst Lp
% figureExport(sb_mesh, D.data_ell_VcstLp, figure_path_geometricShape, strcat('ellipsoid_volume_L',p),strcat('Ellipsoid - V_{cst} - a=L_{',p,'%}/2'), MyFish);
% Ellipsoid : Scst Lp
% figureExport(sb_mesh, D.data_ell_ScstLp, figure_path_geometricShape, strcat('ellipsoid_surface_L',p),strcat('Ellipsoid - S_{cst} - a=L_{',p,'%}/2'), MyFish);
% % Ellipsoid : Vcst Rp
% figureExport(sb_mesh, D.data_ell_VcstRp, figure_path_geometricShape, strcat('ellipsoid_volume_R',p),strcat('Ellipsoid - V_{cst} - b=R_{',p,'%}'), MyFish);
% % Ellipsoid : Scst Rp
% figureExport(sb_mesh, D.data_ell_ScstRp, figure_path_geometricShape, strcat('ellipsoid_surface_R',p),strcat('Ellipsoid - S_{cst} - b=R_{',p,'%}'), MyFish);
% Ellipsoid : Scst L
figureExport(sb_mesh, D.data_ell_ScstL, figure_path_geometricShape, 'ellipsoid_surface_L','Ellipsoide - S_{cst} - a=L/2', MyFish);
% % Ellipsoid : least squares fit : p=0.4
% figureExport(sb_mesh, D.data_ell_LSp, figure_path_geometricShape, strcat('ellipsoid_LS_L',p), strcat('Ellipsoid - Least squares - data inside L_{',p,'%}'), MyFish);
% % Ellipsoid : least squares fit : all data
% figureExport(sb_mesh, D.data_ell_LS, figure_path_geometricShape, 'ellipsoid_LS', 'Ellipsoid - Least squares - all data', MyFish);

% Bent Cylinder : Scst Lbc
figureExport(sb_mesh, D.data_bcyl_ScstL, figure_path_geometricShape, 'bentcylinder_surface_L', 'Cylindre courb� - S_{cst} - L_{cyl}=Lbc', MyFish);

% Real shape :
figureExport(sb_mesh, [], figure_path_realShape, 'real_shape', 'vessie natatoire r�elle', MyFish);

