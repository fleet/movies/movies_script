function data = GetBasicGeometry( swimbladder, geometry_type, sizing_way, R_in, L_in, X0 )
%shape_model 
%   Input : 
%           - swimbladder : swimbladder descriptors
%           - shape_type  : swimbladder geometric modelization shape : cylinder or ellipsoid
%           - sizing_way  : you can choose all the shape parameters or use
%                           hypothesis like volume preservation or surface are preservation
%           - R_in        : geometric shape acrossfish dimension (radius for a
%                                  cylinder and minor axis for an ellipsoid)
%           - L_in        : geometric shape alongfish dimension
%           - X0          : x-origin geometric shape (only for a cylinder, use '0' for an ellipsoid)
%
%   Output : 
%           - X, Y, Z : geometric shape 3D points
%           - S, V  : geometric shape surface area and volume
%           - xx, yz  : alongfish and acrossfish size indicators

n = 30; % points number for drawing the cylinder or the ellipsoid
dr = 1e-4; % 0.1mm, precision for computing 'a' or 'b'

switch  geometry_type
    case {'cylinder','bentcylinder'}
        L = L_in;
        R = R_in;
        switch sizing_way
            case 'arbitrary'
                
            case 'surface'
                S = swimbladder.S; % real surface area
                
                if R == 0 % L fixed, we look for R value
                    % resolution equa polynomiale 2nd degre pour trouver rayon avec surface
                    % equivalente : 2*pi.r^2+2L.pi.r-Seq=0
                    discriminant = L^2+2*S/pi;
                    R = -0.5*L+0.5*sqrt(discriminant); % radius is computed from surface area formula
%                     R = S/(2*pi*L);
                elseif L == 0 % R fixed, we look for L value
                    L = S/(2*pi*R)-R;
                end  
                    
            case 'volume'
                V = swimbladder.V; % real volume
                
                if R == 0 % L fixed, we look for R value
                    R = sqrt(V/(pi*L)); % radius is computed from volume formula
                elseif L == 0 % R fixed, we look for L value
                    L = V/(pi*R^2);
                end 
        end
        [Y,Z,X] = cylinder(R,n);       % the cylinder is generated
%         X = X(:);
%         Y = Y(:);
%         Z = Z(:);
        X       = X * L;               % adjust cylinder length

        if strcmp(geometry_type,'bentcylinder') % if the cylinder is bent
            % slicing of the cylinder according to x-axis :
            X_sampled = linspace(X(1,1),X(2,1),n);
            X = repmat(X_sampled',1,size(Z,2));
            Y = repmat(Y(1,:),n,1);
            Z = repmat(Z(1,:),n,1);
            
            rc = swimbladder.CurvatureParameters.rc; % curvature radius
            X = X - 0.5*(X(end,1)-X(1,1)); % origin offset
            gamma = asin(X./rc);
            Z_abs = rc - X./tan(gamma);% absolute value of Z coordinate
            Z = Z-Z_abs; % Z must be negative according to frame convention
            X = X + 0.5*(X(end,1)-X(1,1)); % origin offset
        end

        
        X       = X + X0;              % origin offset
        S       = 2*pi*R*L + 2*pi*R^2; % area surface computation
        V       = pi*R^2*L;            % volume computation
        xx      = L/2; yz = R;         % dimensions
        
   
    case 'ellipsoid'
        a = L_in;
        b = R_in;
        switch sizing_way
            case 'arbitrary'
                
            case 'surface'
                S = swimbladder.S; % real surface area
                
                if a == 0 % b fixed, we look for a value
                    aa = b:dr:30*b; % between b and 10*b
                    S_aa = 2*pi*b^2+(2*pi*aa.^2*b./sqrt(aa.^2-b^2)).*asin(sqrt(aa.^2-b^2)./aa); % ellipsoid surface computed with aa values
                    [useless, aa_keep] = min(abs(S_aa-S*ones(1,length(aa))));
                    a = aa(aa_keep);
                elseif b == 0 % a fixed, we look for b value
                    bb = dr:dr:a; % between 0.01 and a
                    S_bb = 2*pi*bb.^2+(2*pi*a^2*bb./sqrt(a^2-bb.^2)).*asin(sqrt(a^2-bb.^2)./a); % ellipsoid surface computed with aa values
                    [useless, bb_keep] = min(abs(S_bb-S*ones(1,length(bb))));
                    b = bb(bb_keep);
                end
            case 'volume'
                V = swimbladder.V; % real volume
                
                if a == 0 % b fixed, we look for a value
                    a = V * 3 / (4*pi*b^2);
                elseif b == 0 % a fixed, we look for b value
                    b = sqrt(3*V/(4*pi*a));
                end
        end
        [ X, Y, Z ] = ellipsoid(0,0,0,a,b,b,n);
        e           = sqrt(a^2-b^2)/a;
        S           = 2*pi*b^2+2*pi*a*b*asin(e)/e;
        V           = (4/3)*pi*a*b^2;
        xx          = a; yz = b; % dimensions
        
    
    case 'HCC' % hemisphere + cylinder + cone
        % useless to indicate a value for L_in, 
        X0_cyl = X0;
        R_HCC  = R_in;
        
        Xmax_sb = swimbladder.XYZminmax(end,2);
        Xmin_sb = swimbladder.XYZminmax(1,1);
        Lcone   = X0_cyl - Xmin_sb;
        Lcyl    = Xmax_sb - R_HCC - X0_cyl;
        Csphere = [Xmax_sb-R_HCC, 0, 0]; % (x,y) coordinates are the same for cylinder center and hemisphere center
        
        % Hemisphere
        [Xs,Ys,Zs] = sphere(n);
        Xs = R_HCC*Xs; Ys = R_HCC*Ys; Zs = R_HCC*Zs; % adjust shpere radius
        idx2keep = (Xs >= 0); % the sphere bottom part is suppressed
        Xs = Xs(idx2keep) + Csphere(1);
        Ys = Ys(idx2keep);
        Zs = Zs(idx2keep);
        
        % Cylinder
        [Ycy,Zcy,Xcy] = cylinder(R_HCC,n); % the cylinder is generated
        N   = size(Xcy,1)*size(Xcy,2);
        Xcy = reshape(Xcy,N,1); Ycy = reshape(Ycy,N,1); Zcy = reshape(Zcy,N,1); % coordinate vectors reshape
        Xcy = Xcy * Lcyl + X0_cyl; % adjust cylinder length

        % Cone
        t   = 0:R_HCC/6:R_HCC;
        [Yco,Zco,Xco] = cylinder(t);
        N   = size(Xco,1)*size(Xco,2);
        Xco = reshape(Xco,N,1); Yco = reshape(Yco,N,1); Zco = reshape(Zco,N,1); % coordinate vectors reshape
        Xco = Xco * Lcone + Xmin_sb; % adjust cylinder length
        
        % Full shape
        X = [Xs; Xcy; Xco];
        Y = [Ys; Ycy; Yco];
        Z = [Zs; Zcy; Zco];
        
        S_cylinder   = 2*pi*R_HCC*Lcyl; % lateral cylinder area surface
        S_hemisphere = 2*pi*R_HCC^2;
        S_cone       = pi*R_HCC*sqrt(R_HCC^2+Lcone^2); % lateral cone area surface
        S            = S_cylinder + S_hemisphere + S_cone;
        
        V_cylinder   = pi*R_HCC^2*Lcyl;
        V_hemisphere = (4*pi/6)*R_HCC^3;
        V_cone       = pi*R_HCC^2*Lcone/3;
        V            = V_cylinder + V_hemisphere + V_cone;
        
        xx = (Lcone+Lcyl+R_HCC)/2; yz = R_HCC; % dimensions
%     case 'box'
%         
%         % Box model descriptors
%         sb_LHW_box_model = [max(X)-min(X), max(Z)-min(Z), max(Y)-min(Y)];
%         % Box model results
%         box_model_volume = sb_LHW_box_model(1)*sb_LHW_box_model(2)*sb_LHW_box_model(3);
%         box_model_surface = 2*sb_LHW_box_model*sb_LHW_box_model([3 1 2]).';

    otherwise
        return
end

data = {};
data.X = X;
data.Y = Y;
data.Z = Z;
data.S = S;
data.V = V;
data.xx = xx;
data.yy = yz;

end

