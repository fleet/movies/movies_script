function D = GetBladderDimensions( sb_shape, fish_shape, p )
% GetBladderDimensions : computes swim bladder dimensions/descriptors
% INPUTS
%   sb_shape : fish bladder 3D geometry : position (x,y,z) of each vertex + polygon faces
%   fish_shape : fish body 3D geometry : position (x,y,z) of each vertex + polygon faces
%   p : criteria used for building bladder significant part
% OUTPUT
%   D : Matlab structure that contains all bladder geometric descriptors

%% Geometry import
% Swim bladder
Psb          = sb_shape.vertices; % position (x,y,z) of each vertex
tessellation = sb_shape.faces;    % polygon faces
% Fish body
Pf = fish_shape.vertices; % position (x,y,z) of each vertex

% Lengths
Xsb = Psb(:,1); Xf = Pf(:,1); % x-position
Lsb = max(Xsb)-min(Xsb);  % swim bladder length (m)
Lf  = max(Xf)-min(Xf);    % fish body length (m)

%% Swimblader slicing
res = 1e-4; % thickness of the slices (0,1mm)
Nslices = ceil(Lsb/res); % we divide the swimbladder into slices of resolution res according to x-axis
XYZminmax = sb_slicing(Psb, Nslices); % x,y,z min/max values for each slice (xmin, xmax, ymin, ymax, zmin, zmax)

% Dimensions of each slice :
HH = XYZminmax(:,6)-XYZminmax(:,5); % slice height (m)
WW = XYZminmax(:,4)-XYZminmax(:,3); % slice thickness (m)

% equivalent radius computation (hypothesis of constant surface area : 2.pi.R=pi.H.W)
RR = sqrt((HH./2).*(WW./2));          % slice equivalent radius (m)
Hmax = max(HH);                       % height maximum (m)
Wmax = max(WW);                       % thickness maximum (m)
Rmax = max(RR);                       % equivalent radius maximum (m)

%% Significant swim bladder part computation :
significant_slices = find(RR > p*Rmax); % equivalent radius and p criteria
Rmean            = mean(RR(significant_slices)); % mean radius value (m) from "fine slices" (slices with high radius value)
% Next computation step does not take into account the fact that there is a
% possibility of non-continuous significant slices :
x_min_C = XYZminmax(significant_slices(1),1);
x_max_C = XYZminmax(significant_slices(end),2);

vertices2keep = (sb_shape.vertices(:,1) >= x_min_C & sb_shape.vertices(:,1) <= x_max_C);
L_C = x_max_C - x_min_C; % significant bladder length (m)

%% Other bladder descriptors computation :
% Volume computation
[~, sb_real_volume] = convhull(Psb);
sb_real_volume=meshVolume(Psb, tessellation);
% Swelling ratio
[~, fish_real_volume] = convhulln(Pf);
fish_real_volume=meshVolume(fish_shape.vertices, fish_shape.faces);
swelling_ratio = sb_real_volume ./ fish_real_volume;
% Surface area computation
a = Psb(tessellation(:, 2), :) - Psb(tessellation(:, 1), :);
b = Psb(tessellation(:, 3), :) - Psb(tessellation(:, 1), :);
c = cross(a, b, 2);
sb_real_surface = 1/2 * sum(sqrt(sum(c.^2, 2)));

%% Export
D   = {};
D.p = p;
% sb_descriptors.vertices = new_vertices;
D.XYZminmax = XYZminmax;
D.L    = Lsb; % swimbladder total length (in swimbladder frame)
D.Lf   = Lf; % fish total length
D.Hmax = Hmax;
D.Wmax = Wmax;
D.R    = RR;
D.Rmax = Rmax;
D.Lp  = L_C;
D.Rp  = Rmean;
D.X0p = x_min_C; % Be careful with origin !
D.slices2keep = significant_slices; % significant part of bladder
D.vertices2keep = vertices2keep; % significant part of bladder
D.S   = sb_real_surface; % m2
D.V   = sb_real_volume; % m3
D.swelling_ratio = swelling_ratio * 100; % swelling ratio (%)

end

