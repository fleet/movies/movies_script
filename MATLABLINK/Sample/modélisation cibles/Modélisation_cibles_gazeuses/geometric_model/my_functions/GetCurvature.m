function CurvatureParameters = GetCurvature( sb, MyFish )
% This function computes bladder curvature (rad)
%   Methods available for curvature computation :
%    use least squares algorithm with :
%                     - bladder slices center
%                     - bladder slices upper part
%   You can use :
%    - the full bladder
%    - only the significant part of the bladder

% Bladder curvature is computed in (X,Z) view in ~fish frame

%% Bladder data use for tilt computation
% full bladder centers :
Xfbc = mean(sb.XYZminmax(:,1:2),2);
Zfbc = mean(sb.XYZminmax(:,5:6),2);
% full bladder upper part :
Xfbup = Xfbc;
Zfbup = sb.XYZminmax(:,5); % upper part (z-axis direction is toward bottom)
% significant bladder centers :
Xsbc = mean(sb.XYZminmax(sb.slices2keep,1:2),2);
Zsbc = mean(sb.XYZminmax(sb.slices2keep,5:6),2);
% significant bladder upper part :
Xsbup = Xsbc;
Zsbup = sb.XYZminmax(sb.slices2keep,5); % upper part (z-axis direction is toward bottom)

%% Bladder curvature computation

% LS over full bladder slices centers :
Par_fbc = PrattSVD([Xfbc, Zfbc]); % curvature radius computation
rc_fbc = Par_fbc(3); % curvature radius (m)
gamma_max_fbc = asin(0.5*sb.L/rc_fbc); % maximum curvature angle (radian)
Lbc_fbc = 2*rc_fbc*gamma_max_fbc; % bent cylinder length (m)
str_gamma_max_fbc = num2str(gamma_max_fbc*180/pi);
str_gamma_max_fbc = str_gamma_max_fbc(1:4);

% LS over significant bladder slices centers :
Par_sbc = PrattSVD([Xsbc, Zsbc]); % curvature radius computation
rc_sbc = Par_sbc(3); % curvature radius (m)
gamma_max_sbc = asin(0.5*sb.L/rc_sbc); % maximum curvature angle (radian)
Lbc_sbc = 2*rc_sbc*gamma_max_sbc; % bent cylinder length (m)
str_gamma_max_sbc = num2str(gamma_max_sbc*180/pi);
str_gamma_max_sbc = str_gamma_max_sbc(1:4);

% LS over full bladder slices upper part :
Par_fbup = PrattSVD([Xfbup, Zfbup]); % curvature radius computation
rc_fbup = Par_fbup(3); % curvature radius (m)
gamma_max_fbup = asin(0.5*sb.L/rc_fbup); % maximum curvature angle (radian)
Lbc_fbup = 2*rc_fbup*gamma_max_fbup; % bent cylinder length (m)
str_gamma_max_fbup = num2str(gamma_max_fbup*180/pi);
str_gamma_max_fbup = str_gamma_max_fbup(1:4);

% LS over significant bladder slices upper part :
Par_sbup = PrattSVD([Xsbup, Zsbup]); % curvature radius computation
rc_sbup = Par_sbup(3); % curvature radius (m)
gamma_max_sbup = asin(0.5*sb.L/rc_sbup); % maximum curvature angle (radian)
Lbc_sbup = 2*rc_sbup*gamma_max_sbup; % bent cylinder length (m)
str_gamma_max_sbup = num2str(gamma_max_sbup*180/pi);
str_gamma_max_sbup = str_gamma_max_sbup(1:4);

% %% Plot :
% m2mm = 1e3; % parameters used for having mm in plots
% figure('Visible','on'); hold on; grid on; axis equal; set(gca,'YDir','reverse');
% % slices centers plots :
% plot(Xfbc*m2mm,Zfbc*m2mm,'ob'); % full bladder centers
% plot(Xsbc*m2mm,Zsbc*m2mm,'or'); % bladder significant part centers
% plot(Xfbup*m2mm,Zfbup*m2mm,'*b'); % full bladder upper part
% plot(Xsbup*m2mm,Zsbup*m2mm,'*r'); % bladder significant upper part
% % Curvature plot :
% circle(Par_fbc(1)*m2mm, Par_fbc(2)*m2mm,rc_fbc*m2mm,  'b') % full bladder centers
% circle(Par_sbc(1)*m2mm, Par_sbc(2)*m2mm,rc_sbc*m2mm,  'r') % significant bladder centers
% circle(Par_fbup(1)*m2mm,Par_fbup(2)*m2mm,rc_fbup*m2mm,'k') % full bladder upper part
% circle(Par_sbup(1)*m2mm,Par_sbup(2)*m2mm,rc_sbup*m2mm,'g') % significant bladder upper part
% 
% xlim([min(Xfbc*m2mm) max(Xfbc*m2mm)]); ylim([min(Zfbup*m2mm) max(Zfbc*m2mm)]);
% xlabel('x-axis (mm)'); ylabel('z-axis (mm)'); title(strcat('Fish n�',num2str(MyFish.idx),' bladder curvature computation'));
% legend('full bladder centers', 'significant bladder centers', ...
%     'full bladder upper part', 'significant bladder upper part', ...
%     strcat('full bladder centers fitting with a circle (',str_gamma_max_fbc,'�)'), ...
%     strcat('significant bladder centers fitting with a circle (',str_gamma_max_sbc,'�)'), ...
%     strcat('full bladder upper part fitting with a circle (',str_gamma_max_fbup,'�)'), ...
%     strcat('significant bladder upper part fitting with a circle (',str_gamma_max_sbup,'�)'));

%% Plot :
m2mm = 1e3; % parameters used for having mm in plots
ss = 10; % sub sampling for plot
figure('Visible','on'); hold on; grid on; axis equal; set(gca,'YDir','reverse');set(gca,'PlotBoxAspectRatio',[2 1 1])

% Curvature plot :
circle(Par_fbc(1)*m2mm, Par_fbc(2)*m2mm,rc_fbc*m2mm,  'k-') % full bladder centers
circle(Par_sbc(1)*m2mm, Par_sbc(2)*m2mm,rc_sbc*m2mm,  'k--') % significant bladder centers
circle(Par_fbup(1)*m2mm,Par_fbup(2)*m2mm,rc_fbup*m2mm,'k-.') % full bladder upper part
circle(Par_sbup(1)*m2mm,Par_sbup(2)*m2mm,rc_sbup*m2mm,'k:') % significant bladder upper part
% slices centers plots :
plot(Xfbc(1:ss:end)*m2mm,Zfbc(1:ss:end)*m2mm,'ok'); % full bladder centers
plot(Xsbc(1:ss:end)*m2mm,Zsbc(1:ss:end)*m2mm,'*k'); % bladder significant part centers
plot(Xfbup(1:ss:end)*m2mm,Zfbup(1:ss:end)*m2mm,'ok'); % full bladder upper part
plot(Xsbup(1:ss:end)*m2mm,Zsbup(1:ss:end)*m2mm,'*k'); % bladder significant upper part


xlim([min(Xfbc*m2mm) max(Xfbc*m2mm)]); ylim([min(Zfbup*m2mm) max(Zfbc*m2mm)]);
xlabel('X_p (mm)'); ylabel('Z_p (mm)'); title(strcat('Sardine n�',num2str(MyFish.idx),' - calcul de courbure'));
legend(strcat('courbure sur totalit� - centre (',str_gamma_max_fbc,'�)'), ...
    strcat('courbure sur partie significative - centre (',str_gamma_max_sbc,'�)'), ...
    strcat('courbure sur totalit� - haut (',str_gamma_max_fbup,'�)'), ...
    strcat('courbure sur partie significative - haut (',str_gamma_max_sbup,'�)'));

%% Export
CurvatureParameters = struct;
% curvature radius (m)
CurvatureParameters.rc_fbc = rc_fbc;
CurvatureParameters.rc_sbc = rc_sbc;
CurvatureParameters.rc_fbup = rc_fbup;
CurvatureParameters.rc_sbup = rc_sbup;
% curvature angle (rad)
CurvatureParameters.gamma_max_fbc = gamma_max_fbc;
CurvatureParameters.gamma_max_sbc = gamma_max_sbc;
CurvatureParameters.gamma_max_fbup = gamma_max_fbup;
CurvatureParameters.gamma_max_sbup = gamma_max_sbup;
% bent cylinder length (m)
CurvatureParameters.Lbc_fbc = Lbc_fbc;
CurvatureParameters.Lbc_sbc = Lbc_sbc;
CurvatureParameters.Lbc_fbup = Lbc_fbup;
CurvatureParameters.Lbc_sbup = Lbc_sbup;

% Parameters used for export and plotting :
CurvatureParameters.rc = rc_sbup;
CurvatureParameters.gamma_max = gamma_max_sbup;
CurvatureParameters.Lbc = Lbc_sbup;


end

