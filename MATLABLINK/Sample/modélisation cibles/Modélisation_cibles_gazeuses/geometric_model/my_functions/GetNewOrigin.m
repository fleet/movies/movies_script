function C = GetNewOrigin( bladder_slices )


% Nv_b   = size(old_body_vertices,1); % number of vertices for fish body
% Nv_sb  = size(old_bladder_vertices,1); % number of vertices for fish bladder
% Ns     = size(bladder_slices,1); % number of slices

%% New origin = Fish bladder center
% X = old_bladder_vertices(:,1); % x-coordinate
% [~, head] = max(X); % fish head index
% [~, tail] = min(X); % fish tail index
% dX = X(head) - X(tail); % fish length in fish frame

% minmax_slice = sb_slicing( old_bladder_vertices, Nslice ); % edges of bladder slices
i_Csb = floor(size(bladder_slices,1)/2); % bladder x-center index
C = [mean(bladder_slices(i_Csb,1:2)), ...     % bladder center
    mean(bladder_slices(i_Csb,3:4)), ...
    mean(bladder_slices(i_Csb,5:6))];

% Shift of origin :
% new_body_vertices    = old_body_vertices    - repmat(C,Nv_b,1);
% new_bladder_vertices = old_bladder_vertices - repmat(C,Nv_sb,1);
% new_bladder_slices   = bladder_slices       - repmat(C([1 1 2 2 3 3]),Ns,1);


end

