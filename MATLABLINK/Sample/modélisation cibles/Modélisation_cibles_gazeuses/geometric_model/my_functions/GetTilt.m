function [ tilt_fbc, tilt_sbc, tilt_fbup, tilt_sbup ] = GetTilt( sb, MyFish )
% This function computes bladder tilt (rad)
%   Methods available for slope computation :
%    - use only bladder edges (head and tail)
%    - use least squares algorithm with :
%                     - bladder slices center
%                     - bladder slices upper part
%   You can use :
%    - the full bladder
%    - only the significant part of the bladder

% Bladder tilt is bladder main axis in (X,Z) view in ~fish frame

%% Bladder data use for tilt computation
% full bladder centers :
Xfbc = mean(sb.XYZminmax(:,1:2),2);
Zfbc = mean(sb.XYZminmax(:,5:6),2);
% full bladder upper part :
Xfbup = Xfbc;
Zfbup = sb.XYZminmax(:,5); % upper part (z-axis direction is toward bottom)
% % full bladder edges (head and tail) :
% Xfb_e = mean(sb.XYZminmax([1 end],1:2),2);
% Zfb_e = mean(sb.XYZminmax([1 end],5:6),2);
% significant bladder centers :
Xsbc = mean(sb.XYZminmax(sb.slices2keep,1:2),2);
Zsbc = mean(sb.XYZminmax(sb.slices2keep,5:6),2);
% significant bladder upper part :
Xsbup = Xsbc;
Zsbup = sb.XYZminmax(sb.slices2keep,5); % upper part (z-axis direction is toward bottom)


%% Bladder tilt computation
% % Edges method : full bladder
% slope_fb_e = polyfit(Xfb_e,Zfb_e,1);
% tilt_fb_e = atan2(slope_fb_e(1),1);
% str_tilt_fb_e = num2str(tilt_fb_e*180/pi);
% str_tilt_fb_e = str_tilt_fb_e(1:4);

% Least squares method : full bladder centers
slope_fbc = polyfit(Xfbc,Zfbc,1);
tilt_fbc = atan2(slope_fbc(1),1); % 
str_tilt_fbc = num2str(tilt_fbc*180/pi);
str_tilt_fbc = str_tilt_fbc(1:4);

% Least squares method : significant bladder centers
slope_sbc = polyfit(Xsbc,Zsbc,1);
tilt_sbc = atan2(slope_sbc(1),1); % 
str_tilt_sbc = num2str(tilt_sbc*180/pi);
str_tilt_sbc = str_tilt_sbc(1:4);

% Least squares method : full bladder upper part
slope_fbup = polyfit(Xfbup,Zfbup,1);
tilt_fbup = atan2(slope_fbup(1),1); % 
str_tilt_fbup = num2str(tilt_fbup*180/pi);
str_tilt_fbup = str_tilt_fbup(1:4);

% Least squares method : significant bladder upper part
slope_sbup = polyfit(Xsbup,Zsbup,1);
tilt_sbup = atan2(slope_sbup(1),1); % 
str_tilt_sbup = num2str(tilt_sbup*180/pi);
str_tilt_sbup = str_tilt_sbup(1:4);

%% Plot
% m2mm = 1e3; % parameters used for having mm in plots
% ss = 10; % sub sampling for plot
% figure('Visible','on'); hold on; axis equal; grid on; set(gca,'YDir','reverse');
% % slices centers plots :
% plot(Xfbc(1:ss:end)*m2mm,Zfbc(1:ss:end)*m2mm,'ob'); % full bladder centers
% plot(Xsbc(1:ss:end)*m2mm,Zsbc(1:ss:end)*m2mm,'or'); % significant bladder centers
% plot(Xfbup(1:ss:end)*m2mm,Zfbup(1:ss:end)*m2mm,'*b'); % full bladder upper part
% plot(Xsbup(1:ss:end)*m2mm,Zsbup(1:ss:end)*m2mm,'*r'); % significant bladder upper part
% % Tilt plot :
% plot(Xfbc(1:ss:end)*m2mm,slope_fbc(1)*Xfbc(1:ss:end)*m2mm+slope_fbc(2)*m2mm,  '-'); % full bladder centers
% plot(Xfbc(1:ss:end)*m2mm,slope_sbc(1)*Xfbc(1:ss:end)*m2mm+slope_sbc(2)*m2mm,'--'); % significant bladder centers
% plot(Xfbc(1:ss:end)*m2mm,slope_fbup(1)*Xfbc(1:ss:end)*m2mm+slope_fbup(2)*m2mm,  '-.'); % full bladder upper part
% plot(Xfbc(1:ss:end)*m2mm,slope_sbup(1)*Xfbc(1:ss:end)*m2mm+slope_sbup(2)*m2mm,  ':'); % significant bladder upper part
% 
% xlim([min(Xfbc*m2mm) max(Xfbc*m2mm)]); ylim([min(Zfbup*m2mm) max(Zfbc*m2mm)]);
% % xlabel('x-axis (mm)'); ylabel('z-axis (mm)'); title(strcat('Fish n�',num2str(MyFish.idx),' bladder tilt computation'));
% xlabel('X_p (mm)'); ylabel('Z_p (mm)'); title(strcat('Sardine n�',num2str(MyFish.idx),' - calcul d''inclinaison'));
% legend('totalit� - centre', 'partie significative - centre', ...
%     'totalit� - haut', 'partie significative - haut', ...
%     strcat('inclinaison sur totalit� - centre (',str_tilt_fbc,'�)'), ...
%     strcat('inclinaison sur partie significative - centre (',str_tilt_sbc,'�)'), ...
%     strcat('inclinaison sur totalit� - haut (',str_tilt_fbup,'�)'), ...
%     strcat('inclinaison sur partie significative - haut (',str_tilt_sbup,'�)'));
% % legend('full bladder centers', 'significant bladder centers', ...
% %     'full bladder upper part', 'significant bladder upper part', ...
% %     strcat('full bladder centers tilt computation (',str_tilt_fbc,'�)'), ...
% %     strcat('significant bladder centers tilt computation (',str_tilt_sbc,'�)'), ...
% %     strcat('full bladder upper part tilt computation (',str_tilt_fbup,'�)'), ...
% %     strcat('significant bladder upper part tilt computation (',str_tilt_sbup,'�)'));

%% Plot
m2mm = 1e3; % parameters used for having mm in plots
ss = 10; % sub sampling for plot
figure('Visible','on'); hold on; axis equal; grid on; set(gca,'YDir','reverse');set(gca,'PlotBoxAspectRatio',[2 1 1])

% Tilt plot :
plot(Xfbc(1:ss:end)*m2mm,slope_fbc(1)*Xfbc(1:ss:end)*m2mm+slope_fbc(2)*m2mm,  'k-', 'LineWidth',2); % full bladder centers
plot(Xfbc(1:ss:end)*m2mm,slope_sbc(1)*Xfbc(1:ss:end)*m2mm+slope_sbc(2)*m2mm,'k--', 'LineWidth',2); % significant bladder centers
plot(Xfbc(1:ss:end)*m2mm,slope_fbup(1)*Xfbc(1:ss:end)*m2mm+slope_fbup(2)*m2mm,  'k-.', 'LineWidth',2); % full bladder upper part
plot(Xfbc(1:ss:end)*m2mm,slope_sbup(1)*Xfbc(1:ss:end)*m2mm+slope_sbup(2)*m2mm,  'k:', 'LineWidth',2); % significant bladder upper part
% slices centers plots :
plot(Xfbc(1:ss:end)*m2mm,Zfbc(1:ss:end)*m2mm,'ok'); % full bladder centers
plot(Xsbc(1:ss:end)*m2mm,Zsbc(1:ss:end)*m2mm,'*k'); % significant bladder centers
plot(Xfbup(1:ss:end)*m2mm,Zfbup(1:ss:end)*m2mm,'ok'); % full bladder upper part
plot(Xsbup(1:ss:end)*m2mm,Zsbup(1:ss:end)*m2mm,'*k'); % significant bladder upper part

xlim([min(Xfbc*m2mm) max(Xfbc*m2mm)]); ylim([min(Zfbup*m2mm) max(Zfbc*m2mm)]);
% xlabel('x-axis (mm)'); ylabel('z-axis (mm)'); title(strcat('Fish n�',num2str(MyFish.idx),' bladder tilt computation'));
xlabel('X_p (mm)'); ylabel('Z_p (mm)'); title(strcat('Sardine n�',num2str(MyFish.idx),' - calcul d''inclinaison'));
legend(strcat('inclinaison sur totalit� - centre (',str_tilt_fbc,'�)'), ...
    strcat('inclinaison sur partie significative - centre (',str_tilt_sbc,'�)'), ...
    strcat('inclinaison sur totalit� - haut (',str_tilt_fbup,'�)'), ...
    strcat('inclinaison sur partie significative - haut (',str_tilt_sbup,'�)'));
% legend('full bladder centers', 'significant bladder centers', ...
%     'full bladder upper part', 'significant bladder upper part', ...
%     strcat('full bladder centers tilt computation (',str_tilt_fbc,'�)'), ...
%     strcat('significant bladder centers tilt computation (',str_tilt_sbc,'�)'), ...
%     strcat('full bladder upper part tilt computation (',str_tilt_fbup,'�)'), ...
%     strcat('significant bladder upper part tilt computation (',str_tilt_sbup,'�)'));


end

