function figureExport( sb, data, figure_path, figure_name, figure_title, MyFish )
%figureExport Summary of this function goes here
%   Detailed explanation goes here

% The model is rendered with a PATCH graphics object. We also add some dynamic
% lighting, and adjust the material properties to change the specular
% highlighting.

faceColor0  = [0.4 0.6 0.8]; faceAlpha0 = 0.8;
faceColor1  = [0.8 0.4 0.4]; faceAlpha1 = 0.4;
faceColor2  = [0.8 0.2 0.6]; faceAlpha2 = 0.3;
faceColor3  = [0.8 0.8 0.2]; faceAlpha3 = 0.15;
% Reality vs ellipsoid modelization
figure('Visible','on');  
subplot 211 % Top view
hold on;
% real swimbladder
sb.vertices = sb.vertices*1e3; % m to mm
patch(sb,'FaceColor',       faceColor0, ... % real swimbladder
         'FaceAlpha',       faceAlpha0, ...
         'EdgeColor',       'none',     ...
         'FaceLighting',    'gouraud',  ...
         'AmbientStrength', 0.15);

% Add a camera light, and tone down the specular highlighting
camlight('headlight'); material('dull');
% Fix the axes scaling, and set a nice view angle
axis('image'); view(0,-90); set(gca, 'YDir', 'reverse');
title(strcat('Vue du dessus - ',figure_title)), xlabel('Longueur (mm)'), ylabel('Largeur (mm)'), zlabel('Hauteur (mm)')

% Geometric model
if ~isempty(data)
X = data.X*1e3;
Y = data.Y*1e3;
Z = data.Z*1e3;
surf(X, Y, Z,'FaceColor',faceColor1,'FaceAlpha',faceAlpha1); hold off;
end

subplot 212 % Side view
hold on;
patch(sb,'FaceColor',       faceColor0, ... % real swimbladder
         'FaceAlpha',       faceAlpha0, ...
         'EdgeColor',       'none',     ...
         'FaceLighting',    'gouraud',  ...
         'AmbientStrength', 0.15);
% Add a camera light, and tone down the specular highlighting
camlight('headlight'); material('dull');
% Fix the axes scaling, and set a nice view angle
axis('image'); view(0,0); set(gca, 'ZDir', 'reverse');
title(strcat('Vue de c�t� - ',figure_title)), xlabel('Longueur (mm)'), ylabel('Largeur (mm)'), zlabel('Hauteur (mm)')

% Geometric model
if ~isempty(data)
surf(X, Y, Z,'FaceColor',faceColor1,'FaceAlpha',faceAlpha1); hold off;
end

%% Figure save
figure_name = strcat(MyFish.specie,'_',MyFish.dataset,'_',num2str(MyFish.idx),'_',figure_name);
strFilePath = strcat(figure_path,figure_name);
set(gcf,'PaperPositionMode','auto')
print(gcf,'-dpng',strFilePath)
% saveas(gcf,strcat(strFilePath,'.png'))

% % here we select which output file extension we want
bPrintOnFile_Pdf = 0; % [0 (false) 1 (true)]
bPrintOnFile_Eps = 1; % [0 (false) 1 (true)]

% we select the file path
%
% NOTE: do NOT insert extensions!


% we select the printing resolution
iResolution = 150;

% we select to crop or not the figure
bCropTheFigure = 1; % [0 (false) 1 (true)]

% ATTENTION: if PaperPositionMode is not 'auto' the saved file
% could have different dimensions from the one shown on the screen!
% set(gcf,'PaperPositionMode','auto');

% f1_pos = f1.PaperPosition; % Returns 1x4 vector [left right width height]
% f1.PaperSize = [f1_pos(3) f1_pos(4)]; %default PaperSize is [8.5 11]
% print(f1,'-dpsc2','-loose',sprintf('-r%d', iResolution),'-painters',strFilePath)

% saving on file: requires some checks
if (0)%( bPrintOnFile_Pdf || bPrintOnFile_Eps )
    %
    % NOTE: if you want a .pdf with encapsulated fonts you need to save an
    % .eps and then convert it => it is always necessary to produce the .eps
    %
    % if we want to crop the figure we do it
    if( bCropTheFigure )
        print('-depsc2', sprintf('-r%d', iResolution), strcat(strFilePath,'.eps'));
    else
        print('-depsc2','-loose', sprintf('-r%d', iResolution), strcat(strFilePath,'.eps'));
    end
    %
    % if we want the .pdf we produce it
    if( bPrintOnFile_Pdf )
        %
        % here we convert the .eps encapsulating the fonts
        system( sprintf( 'epstopdf --gsopt=-dPDFSETTINGS=/prepress --outfile=%s.pdf %s.eps', strFilePath, strFilePath));
    end
    %
    % if we do not want the .eps we remove it
    if( ~bPrintOnFile_Eps )
        delete(sprintf('%s.eps', strFilePath));
    end
    %
end % saving on file
    
%%
% % Least squares ellipsoid model
% p = patch( isosurface( Xell, Yell, Zell, Ellipsoid, 1 ) ); % ellipsoid
% set( p, 'FaceColor', 'g', 'FaceAlpha',faceAlpha2,'EdgeColor', 'none' );

% % HCC model
% DT_HCC = delaunayTriangulation(HCC_full_shape(:,1),HCC_full_shape(:,2),HCC_full_shape(:,3)); % 3-D Delaunay triangulation from the point coordinates in the column vectors, x, y, and z
% tetramesh(DT_HCC,'FaceColor',faceColor1,'FaceAlpha',faceAlpha1);


% 
% figure(8) % triangles plot
% subplot 121
% C=ones(size(HCC_full_shape(:,1))); % color
% triangles = convhull(HCC_full_shape(:,1),HCC_full_shape(:,2),HCC_full_shape(:,3));
% trisurf(triangles,HCC_full_shape(:,1),HCC_full_shape(:,2),HCC_full_shape(:,3))
% axis equal
% subplot 122
% plot3(HCC_full_shape(:,1),HCC_full_shape(:,2),HCC_full_shape(:,3))
% axis equal


% figure(9)
% subplot 211
% bar(res_volume), title('Volume computation'), ylabel('Volume (cm^3)'), grid on
% set(gca,'XTickLabel',{'reference', 'global box', 'Cone+Sphere+Cylinder r_{max}', 'Cone+Sphere+Cylinder r_{mean}'})
% subplot 212
% bar(res_surface), title('Surface area computation'), ylabel('Surface (cm^2)'), grid on
% set(gca,'XTickLabel',{'reference', 'global box', 'Cone+Sphere+Cylinder r_{max}', 'Cone+Sphere+Cylinder r_{mean}'})





end

