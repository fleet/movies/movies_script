function fileNames = getDatasetFilenames( data_path, dataset_desc )
% get filename list related to the current dataset in the path data_path

% content of repertory data_path :
data_path_content = dir(data_path);
data_path_content = data_path_content(3:end); % 2 first lines are useless
fileNames = {};
ii = 0;

for i_file = 1:size(data_path_content,1) 
    % check if the file is linked to the current dataset
    if isempty(strfind(data_path_content(i_file).name, dataset_desc));
        continue % if not, we pass to the next file
    else
        ii = ii + 1;
        fileNames{ii,1} = data_path_content(i_file).name;
    end
end

end

