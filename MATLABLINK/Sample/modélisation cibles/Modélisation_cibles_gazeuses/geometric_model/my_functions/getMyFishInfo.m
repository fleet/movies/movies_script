function MyFish = getMyFishInfo( MyDataList )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

Nfile = length(MyDataList);

MyDataSpecie = cell(Nfile,1); % sardine or herring
MyDataOrgan = cell(Nfile,1); % body or bladder
MyDataset = cell(Nfile,1); % dataset name
MyDataIdx = zeros(Nfile,1); % fish index


for i_file = 1:Nfile
filename_pattern = strsplit(MyDataList{i_file},{'_','.'}); % split filename
MyDataSpecie(i_file,1) = cellstr(filename_pattern{1,1});
MyDataOrgan(i_file,1) = cellstr(filename_pattern{1,2});
MyDataset(i_file,1) = cellstr(filename_pattern{1,3});
MyDataIdx(i_file) = str2double(filename_pattern{1,4}); % number
end

IdxList = unique(MyDataIdx); % build the index list
IdxList = sort(IdxList);
Nfish = length(IdxList);

MyFish = cell(Nfish,1);


for ii = 1:Nfish
    i_fish = find(MyDataIdx == IdxList(ii));
    if length(i_fish) > 1
        i_fish = i_fish(1);
        MyFish{i_fish}.hasBody = true;
    elseif length(i_fish) == 1
        MyFish{i_fish}.hasBody = false;
    end
    MyFish{i_fish}.idx = IdxList(ii);
    MyFish{i_fish}.specie = char(MyDataSpecie(i_fish,1));
    MyFish{i_fish}.dataset = char(MyDataset(i_fish,1));
end


end

