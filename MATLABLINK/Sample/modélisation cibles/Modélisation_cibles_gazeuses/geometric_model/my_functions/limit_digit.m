function X = limit_digit( X0, Ndigit )
% limit the number of digits of a float number

n = 10^Ndigit;
X = round(X0*n) ./ n;


end

