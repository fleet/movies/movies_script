function [ minmax_slice ] = sb_slicing( V, n )
%sb_slicing 
%   Summary : 
%           - cut a 3d object into slices according to x-axis and compute x,y,z min/max values for each slice
%
%   Input : 
%           - V : points coordinates (x,y,z)
%           - n : total number of slices
%
%   Output : 
%           - minmax_slice : (xmin, xmax, ymin, ymax, zmin, zmax) for each slice
%

X            = V(:,1);                        % x-coordinate
X_samp       = linspace(min(X), max(X), n+1); % x-coordinate sampled
minmax_slice = zeros(n,6);     % parameters for each slice : xmin, xmax, ymin, ymax, zmin, zmax


% slice parameters computation :
for ii = 1:length(X_samp)-1
    keep               = (X >= X_samp(ii) & X <= X_samp(ii+1)); % points which are inside the current slice
    V_s                = V(keep,:);                             % vertices which are in the current slice
    if ~isempty(V_s)
        minmax_slice(ii,:) = [min(V_s(:,1)) max(V_s(:,1)) ...       % xmin, xmax
                                min(V_s(:,2)) max(V_s(:,2)) ...     % ymin, ymax
                                min(V_s(:,3)) max(V_s(:,3))];       % zmin, zmax
    else % if the slice is empty, we use caracteristics from previous slice
        minmax_slice(ii,:) = minmax_slice(ii-1,:);
    end
end

end

