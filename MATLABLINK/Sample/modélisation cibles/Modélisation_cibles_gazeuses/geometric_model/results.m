%% Geometric results presentation

close all

% load dataset results
[FileName,PathName,FilterIndex] = uigetfile('*.mat','Select the geometric results', '../acoustic_model/in');
load(strcat(PathName,FileName));
data1 = array_;

[FileName,PathName,FilterIndex] = uigetfile('*.mat','Select the geometric results', '../acoustic_model/in');
load(strcat(PathName,FileName));
data2 = array_;

%% Data 1
mean_values1 = mean(data1);
std_values1 = std(data1);

idx1 = data1(:,1); % fish index
Lfish1 = data1(:,2); % fish length (cm)
Lbladder1 = data1(:,3); % bladder length (cm)
Lratio1 = Lbladder1 ./ Lfish1 * 100; % length ratio (%)
Sfish1 = data1(:,4); % fish surface (cm2)
Vfish1 = data1(:,5); % fish volume (cm3)
SwellingRatio1 = data1(:,6); % bladder swelling ratio (%)
tilt1 = data1(:,7); % bladder tilt (�)
curvature1 = data1(:,9); % bladder curvature (�)
b_cyl1 = data1(:,end-1); % bladder radius (cm) for cylinder with constant surface
b_ell1 = data1(:,end); % bladder radius (cm) for ellipsoid with constant surface

%% Data 2
mean_values2 = mean(data2);
std_values2 = std(data2);

idx2 = data2(:,1); % fish index
Lfish2 = data2(:,2); % fish length (cm)
Lbladder2 = data2(:,3); % bladder length (cm)
Lratio2 = Lbladder2 ./ Lfish2 * 100; % length ratio (%)
Sfish2 = data2(:,4); % fish surface (cm2)
Vfish2 = data2(:,5); % fish volume (cm3)
SwellingRatio2 = data2(:,6); % bladder swelling ratio (%)
tilt2 = data2(:,7); % bladder tilt (�)
curvature2 = data2(:,9); % bladder curvature (�)
b_cyl2 = data2(:,end-1); % bladder radius (cm) for cylinder with constant surface
b_ell2 = data2(:,end); % bladder radius (cm) for ellipsoid with constant surface

%% Plot

figure(1)
subplot 221
plot(Lfish1,Lbladder1,'*')
xlabel('Fish length [cm]'); ylabel('Bladder length [cm]')
subplot 222
plot(idx1,Lratio1,'*')
xlabel('Fish index'); ylabel('Length ratio [%]')
subplot 223
plot(Sfish1,Vfish1,'*')
xlabel('Bladder surface [cm^2]'); ylabel('Bladder volume [cm^3]')
subplot 224
plot(Vfish1,SwellingRatio1,'*')
xlabel('Bladder surface [cm^2]'); ylabel('Bladder swelling ratio [%]')

figure(2)
subplot 221
plot(Sfish1,b_cyl1,'k*'); hold on;
plot(Sfish1,b_ell1,'ko');
xlabel('Bladder surface [cm^2]'); ylabel('Bladder radius [cm]')
legend('cylinder','ellipsoid');
subplot 222
plot(Vfish1./Sfish1,b_cyl1,'*'); hold on;
xlabel('Bladder volume / Bladder surface [cm]'); ylabel('Bladder radius [cm]')

figure(3)
% [ax,hBar,hLine] = plotyy(idx,[Lfish*10 Lbladder*10],idx,Lratio,'bar','plot');
bar(idx1,[Lfish1*10 Lbladder1*10 Lratio1], 3); set(gca,'YGrid','on');
xlabel('indice du poisson'); title('Sardines de l''aquarium de La Rochelle')
legend('longueur du poisson (mm)', 'longueur de la vessie (mm)', 'rapport des longueurs (%)')

figure(4)
ax1 = subplot(1,2,1);
bar(idx1, [curvature1 tilt1], 4); set(ax1,'YGrid','on');
xlabel('indice du poisson'); title('Sardines de l''aquarium de La Rochelle')
ylabel('Angle [�]'); ylim([0 12]);
legend('inclinaison (�)', 'courbure (�)'); set(gca,'YGrid','on');
ax2 = subplot(1,2,2);
bar(idx2, [curvature2 tilt2], 2); set(ax2,'YGrid','on');
xlabel('indice du poisson'); title('Sardines PELGAS14')
ylabel('Angle [�]'); ylim([0 12]);
legend('inclinaison (�)', 'courbure (�)'); set(gca,'YGrid','on');
