clear all;
% close all;

%chargement du r�sultat du mod�le Furusawa 1988 gaz
load('PSM_EK_bubbles_2_vac_0p002.mat');

% TS=20*log10(abs(softL)+abs(fluidL));
TSg=20*log10(abs(softL));


i=1;
for f=1:1:length(freqs)
    for s=1:length(lengths)
        a_sb=0.05;
        ka1(i)=2*pi*(freqs(f)/c_b)*(a_sb);
        TSka1(i)=10*log10(mean(10.^(reshape(TSg(1,s,f,1)/10,[],1)))/(4*a_sb*a_sb));
        i=i+1;
    end
end

% %chargement du r�sultat du mod�le Furusawa 1988 vacant
load('PSM_EK_bubbles_5_gas_0.mat');

% TS=20*log10(abs(softL)+abs(fluidL));
TSv=20*log10(abs(softL));


i=1;
for f=1:1:length(freqs)
    for s=1:length(lengths)
        a_sb=0.05;
        ka2(i)=2*pi*(freqs(f)/c_b)*(a_sb);
        TSka2(i)=10*log10(mean(10.^(reshape(TSv(end,s,f,end)/10,[],1)))/(4*a_sb*a_sb));
        i=i+1;
    end
end

a_sb=17*1e-2;
b_sb=(17/12)*1e-2;
applatissement=a_sb/b_sb;
% applatissement=2;
depth=0;
% p_c=1.24; % densit� du gaz en kg/m3
% p_w=1030; % densit� de l'eau en kg/m3
%p_b=1071;  % densit� de l'enveloppe de la bulle en kg/m3
% c_c=345; % c�l�rit� du son dans le gaz en m/s
%   c_c=sqrt(gamma.*P0./rho_gaz_corr); % formule http://fr.wikipedia.org/wiki/Vitesse_du_son (gaz parfait)
% c_w=1495; % c�l�rit� du son dans l'eau en m/s
c_w=c_b;
p_w=p_b;

a=a_sb;
aeq=a*(1/applatissement)^(2/3); % rayon �quivalent pour une sph�re de m�me volume que la sph�re prolate
% aeq=a*(1/applatissement)^(1/3); % rayon �quivalent pour une sph�re de m�me volume que la sph�re aplatie oblate
% aeq=0.01;
% a=aeq*(1/applatissement)^(-2/3);
% Mod�les de Stanton (1989)
% p_c=p_c/(1+0.1*depth);
g=(p_c/p_w)*(1+0.1*depth);
h=c_c/c_w;

alphas=(1 - g*h^2) / (3*g*h^2) + (1 - g) / (1 + 2*g);
alphaps=(1 - g*h^2) / (2*g*h^2) + (1 - g) / (1 + g);
    R = (g*h - 1) / (g*h + 1) ;
c=c_w;
L_a=applatissement;
f=[100:5:200000];
ka3=(2*pi*f/c)*aeq;

%Stanton sph�re
F = 1 + 0.5 .* (ka3).^(-0.75) ;
G = 1 + 85 .* exp(-500000 .* ((ka3 - 0.0135).^2)) ;
TSka3=10*log10((ka3.^4*alphas^2.*G./(1+(4*ka3.^4*alphas^2./(R^2.*F))))./4);

%Stanton prolate spheroid
F=1;
G=1+6*exp(-7*100000*(ka3-0.0045).^2);
TSka4=10*log10(((1/9)*L_a^2*(ka3.^4*alphaps^2).*G./(1+((16/9)*ka3.^4*alphaps^2./(R^2.*F))))./4);

%Medwin (1998)
gamma=1.4;
P0=10^5*(1+0.1*depth);
rho=p_w;
f0=(1/(2*pi*aeq))*(3*gamma*P0/rho)^0.5;
TSka5=10*log10((1./((f0^2./f.^2-1).^2+ka3.^2))./4);

%Kloser 2002
mu1 = 1e5 ; % in Pa 
eratio = 1/applatissement ; % ratio of the minor semi-axis to the major semi-axis 
ro = p_w ; % water density in g.m-3 
Q = 5 ; % in Kloser et al. 2002 paper, delta =1/Q 
c = c_w ; 
P = (1 + 0.103*depth) * 1e5 ; % pressure at the D depth 

    Cstef0 = (1/(2*pi)) * ((3*gamma*P + 4*mu1)/ro)^(1/2) ;
    Cstefp = Cstef0 * 2^(1/2) * eratio^(-1/3) * (1-eratio^2)^(1/4) * (log((1 + (1-eratio^2)^(1/2))/(1 - (1-eratio^2)^(1/2))))^(-1/2) ;
    Cstedenom = 4;%(2/(eratio^(2/3)))^2 ;

TSka6 = 10*log10( 1./(Cstedenom * ([((Cstefp*2*pi./(c*ka3)).^2)-1].^2 + 1/Q^2))) ;
        
%Ye 1997
eratio = 1/applatissement ; % ratio of the minor semi-axis to the major semi-axis
ro = p_w ; % water density in g.m-3
epsilon=(1-eratio^2)^(1/2);
c = c_w ;
P = (1 + 0.1*depth) * 1e5 ; % pressure at the D depth

tau=10; %fish
tau=0.05; %water
s_a=tau/aeq;

if epsilon ==0
    Cstef0 = (1/(2*pi)) * ((3*gamma*P+(2*s_a)*(3*gamma-1))/ro)^(1/2);
    CsteQ = 1;
    f0 =  (1/(2*pi*aeq)) * ((3*gamma*P+(2*s_a)*(3*gamma-1))/ro)^(1/2) ;
    Q = 1/(CsteQ*((2*pi*f0/c)*aeq));
else
    Cstef0 = (1/(2*pi*eratio^(1/3))) * ((3*gamma*P+(2*s_a)*(3*gamma-1))/ro)^(1/2)* (log((1 + epsilon)/eratio)/epsilon)^(-1/2)  ;
    CsteQ = eratio^(-2/3)*((log((1 + epsilon)/eratio)/epsilon))^(-1);
    f0 = (1/(2*pi*aeq*eratio^(1/3))) * ((3*gamma*P+(2*s_a)*(3*gamma-1))/ro)^(1/2) * ((log((1 + epsilon)/eratio)/epsilon)^(-1/2)); % spherical resonant frequency at a
    Q = 1/(CsteQ*((2*pi*f0/c)*aeq));
end
Cstedenom = 4;%(2/(eratio^(2/3)))^2 ;
Dp=1.9e-5;
lth=sqrt(Dp./(2*c_w.*ka3./aeq));
X=aeq./lth;
G=gamma./(1-(((1+i).*X/2/tanh((1+i).*X/2)-1).*(6*i*(gamma-1)./X.^2)));
betath_omega=3*(P+2*s_a).*imag(G)./(2*p_w*c_w^2.*ka3.^2);
eta=2.4*1e-5*10^(247.8/(10+273.15-140));
% eta=1;
betavis_omega=2*eta./(p_w*c_w*aeq.*ka3);
beta_omega=betath_omega+betavis_omega;
% beta_omega=0;

TSka7 = 10*log10( (CsteQ/Cstedenom)  * (1./([((Cstef0*2*pi./(c*ka3)).^2)-1].^2 + ((((f0./f).^2).*CsteQ.*ka3+2*beta_omega)).^2))) ;


%Love 1978
eratio = 1/applatissement ; % ratio of the minor semi-axis to the major semi-axis 
% ro = p_w ; % water density in g.m-3
epsilon=(1-eratio^2)^(1/2);
% c = c_w ;
P = (1 + 0.1*depth) * 1e5 ; % pressure at the D depth 
s_a=10/aeq; %fish
s_a=0.05/aeq; %water
viscosity_a=50/aeq;

% hydrostatic pressure (P) for fish depth (D)
    if epsilon ==0
        Csteomega0 = ((3*gamma*P)/p_b+(2*s_a/p_b)*(3*gamma-1))^(1/2);
    else
        Csteomega0 = (eratio^(-1/3)) * ((3*gamma*P)/p_b+(2*s_a/p_b)*(3*gamma-1))^(1/2)* (log((1 + epsilon)/eratio)/epsilon)^(-1/2)  ;
    end
    
    Hrad=Csteomega0*(p_b/p_w)./(c_w*(ka3).^2);
%     Hrad=ka3.^(-1/2);
    Hvis=c_w*ka3*p_b/(2*viscosity_a);
    
    H=1./(1./Hrad+1./Hvis);
%      H=1./(1./Hrad);
    
TSka8 = 10*log10( (((((log((1 + epsilon)/eratio)/epsilon))^(-2))*(p_w/p_b)^2)./([((Csteomega0./(c_w*ka3)).^2)-1].^2 + (((Csteomega0./(c_w*ka3)).^2).*(1./H).^2).^2))./(4)) ;


%Ainslie 2011
eratio = 1/applatissement ; % ratio of the minor semi-axis to the major semi-axis 
% ro = p_w ; % water density in g.m-3
epsilon=(1-eratio^2)^(1/2);
% c = c_w ;
P = (1 + 0.1*depth) * 1e5 ; % pressure at the D depth 
s_a=10/aeq; %fish
s_a=0.05/aeq; %water
% Dp=0.0262/(p_c*1.012);
Dp=1.9e-5;
lth=sqrt(Dp./(2*c_w.*ka3./aeq));
X=aeq./lth;
G=gamma./(1-(((1+i).*X/2/tanh((1+i).*X/2)-1).*(6*i*(gamma-1)./X.^2)));
betath_omega=3*P.*imag(G)./(2*p_w*c_w^2.*ka3.^2);
eta=2.4*1e-5*10^(247.8/(10+273.15-140));
betavis_omega=2*eta./(p_w*c_w*aeq.*ka3);
beta_omega=betath_omega+betavis_omega;
% beta_omega=0;

% hydrostatic pressure (P) for fish depth (D)
    if epsilon ==0
        Csteomega0 = ((3*gamma*P)/p_w+(2*s_a/p_w)*(3*gamma-1))^(1/2);
          CsteQ = 1;
    else
        Csteomega0 = (eratio^(-1/3)) * ((3*gamma*P)/p_w+(2*s_a/p_w)*(3*gamma-1))^(1/2)* (log((1 + epsilon)/eratio)/epsilon)^(-1/2)  ;
          CsteQ = eratio^(-2/3)*((log((1 + epsilon)/eratio)/epsilon))^(-1);
    end
     Cstedenom = 4;%(2/(eratio^(2/3)))^2 ;
    
TSka9 = 10*log10( (((CsteQ/Cstedenom)) ./([((Csteomega0./(c_w*ka3)).^2)-1-2*beta_omega.*ka3].^2 + (2*beta_omega+((Csteomega0./(c_w*ka3)).^2).*(ka3)).^2))) ;


[~,index1]=sort(ka1);
[~,index2]=sort(ka2);
[~,index3]=sort(ka3);
figure;
semilogx(ka1(index1)*aeq/a,TSka1(index1)+20*log10(a/aeq),'b','linewidth',3);
hold on;
semilogx(ka2(index2)*aeq/a,TSka2(index2)+20*log10(a/aeq),'b-','linewidth',3);
semilogx(ka3(index3),TSka3(index3),'c','linewidth',3);      
% semilogx(ka3(index3),TSka4(index3),'m','linewidth',3); 
semilogx(ka3(index3),TSka5(index3),'r','linewidth',3); 
semilogx(ka3(index3),TSka6(index3),'y','linewidth',3);
semilogx(ka3(index3),TSka7(index3),'k','linewidth',3);
semilogx(ka3(index3),TSka8(index3),'g','linewidth',3);
semilogx(ka3(index3),TSka9(index3),'m','linewidth',3);
% legend( 'PSM Furusawa gas 1988','Stanton sphere 1989','Stanton prolate spheroid 1989', 'Medwin 1998','Kloser 2002','Ye 1997');
legend( 'PSM Furusawa vac 1988','PSM Furusawa gas 1988','Stanton sphere 1989', 'Medwin 1998','Kloser 2002','Ye 1997','Love 1978','Ainslie 2009');
axis([0 10 -40 30]);
grid on;
toto=xlabel('ka');
set(toto,'FontSize',16)
toto=ylabel('TS reduced (dB)');
set(toto,'FontSize',16)
title({['Reduced TS for gas bubble at depth ',num2str(depth),'m, aspect ratio ',num2str(applatissement), ' gamma for Medwin and Kloser ',num2str(gamma) ];[' sound speed in water ',num2str(c),' m/s water density ',num2str(p_w),' kg/m3 sound speed in gas ',num2str(c_c),' m/s gas density ',num2str(p_c*(1+0.1*depth)) ' kg/m3']},'FontSize',8);
% saveas(gcf,['S:\Th�se Barbara\','TSreducedversuska_',num2str(round(applatissement)),'_',num2str(depth)],'fig')
% saveas(gcf,['S:\Th�se Barbara\','TSreducedversuska_',num2str(round(applatissement)),'_',num2str(depth)],'jpg')



frequency=1e3*[18 38 70 120 200 333];
frequency=f;
for i=1:length(frequency)
    indexfrequency=min(find(abs(freqs-frequency(i))<2));
    if size(indexfrequency)>0
    TS1(i)=10*log10((2*a)^2*10^((TSka2(indexfrequency))/10));
    else
        TS1(i)=-80;
    end
end
% TS1=[10*log10((2*a)^2*10^(TSka1(indexka1)/10)) 10*log10((2*a)^2*10^(TSka1(indexka2)/10)) 10*log10((2*a)^2*10^(TSka1(indexka3)/10)) 10*log10((2*a)^2*10^(TSka1(indexka4)/10)) 10*log10((2*a)^2*10^(TSka1(indexka5)/10)) 10*log10((2*a)^2*10^(TSka1(indexka6)/10))];
% TS2=[10*log10((2*a)^2*10^(TSka2(indexka1)/10)) 10*log10((2*a)^2*10^(TSka2(indexka2)/10)) 10*log10((2*a)^2*10^(TSka2(indexka3)/10)) 10*log10((2*a)^2*10^(TSka2(indexka4)/10)) 10*log10((2*a)^2*10^(TSka2(indexka5)/10)) 10*log10((2*a)^2*10^(TSka2(indexka6)/10))];
for i=1:length(frequency)
TS3(i)=10*log10((2*aeq)^2*10^(TSka3(find(f==frequency(i)))/10)) ;
TS4(i)=10*log10((2*aeq)^2*10^(TSka4(find(f==frequency(i)))/10)) ;
TS5(i)=10*log10((2*aeq)^2*10^(TSka5(find(f==frequency(i)))/10)) ;
TS6(i)=10*log10((2*aeq)^2*10^(TSka6(find(f==frequency(i)))/10)) ;
TS7(i)=10*log10((2*aeq)^2*10^(TSka7(find(f==frequency(i)))/10)) ;
TS8(i)=10*log10((2*a)^2*10^(TSka8(find(f==frequency(i)))/10)) ;
TS9(i)=10*log10((2*aeq)^2*10^(TSka9(find(f==frequency(i)))/10)) ;
end



figure;
semilogx(frequency,TS1,'b','linewidth',3);
hold on;
% semilogx(frequency,TS2,'g','linewidth',3);     
semilogx(frequency,TS3,'c','linewidth',3);
% semilogx(frequency,TS4,'m','linewidth',3); 
semilogx(frequency,TS5,'r','linewidth',3); 
semilogx(frequency,TS6,'y','linewidth',3);
semilogx(frequency,TS7,'k','linewidth',3);
semilogx(frequency,TS8,'g','linewidth',3);
semilogx(frequency,TS9,'m','linewidth',3);
% legend( 'PSM Furusawa 1988','Stanton sphere 1989','Stanton prolate spheroid 1989', 'Medwin 1998','Kloser 2002','Ye 1997');
legend( 'PSM Furusawa 1988','Stanton sphere 1989', 'Medwin 1998','Kloser 2002','Ye 1997','Love 1978','Ainslie 2009');
% legend( 'Stanton sphere 1989','Stanton prolate spheroid 1989', 'Medwin 1998','Ye 1997');
% axis([10000 200000 -100 -40]);
grid on;
toto=xlabel('frequency (Hz)');
set(toto,'FontSize',16)
toto=ylabel('TS  (dB)');
set(toto,'FontSize',16)
title({['TS for gas bubble of size ', num2str(a*1000), ' mm',' at depth ',num2str(depth),'m, aspect ratio ',num2str(applatissement), ' gamma for Medwin and Kloser ',num2str(gamma) ];[' sound speed in water ',num2str(c_w),' m/s water density ',num2str(p_w),' kg/m3 sound speed in gas ',num2str(c_c),' m/s gas density ',num2str(p_c*(1+0.1*depth)) ' kg/m3']},'FontSize',8);
% saveas(gcf,['S:\Th�se Barbara\','TSversusfrequency_',num2str(round(applatissement)),'_',num2str(depth),'_',num2str(a*1000)],'fig')
% saveas(gcf,['S:\Th�se Barbara\','TSversusfrequency_',num2str(round(applatissement)),'_',num2str(depth),'_',num2str(a*1000)],'jpg')

% indexa=min(find(abs((lengths/2)-a)<1e-5));
% figure;
% plot(phi,squeeze(TSg(end,indexa,1,:)),'b','linewidth',3);
% hold on;
% plot(phi,squeeze(TSg(end,indexa,2,:)),'g','linewidth',3);
% plot(phi,squeeze(TSg(end,indexa,3,:)),'k','linewidth',3);
% plot(phi,squeeze(TSg(end,indexa,4,:)),'r','linewidth',3);
% plot(phi,squeeze(TSg(end,indexa,5,:)),'c','linewidth',3);
% plot(phi,squeeze(TSg(end,indexa,6,:)),'m','linewidth',3);
% 
% % plot(phi,squeeze(TSv(end,indexa,1,:)),'--b','linewidth',3);
% % plot(phi,squeeze(TSv(end,indexa,2,:)),'--g','linewidth',3);
% % plot(phi,squeeze(TSv(end,indexa,3,:)),'--k','linewidth',3);
% % plot(phi,squeeze(TSv(end,indexa,4,:)),'--r','linewidth',3);
% % plot(phi,squeeze(TSv(end,indexa,5,:)),'--c','linewidth',3);
% % plot(phi,squeeze(TSv(end,indexa,6,:)),'--m','linewidth',3);
% legend( 'PSM 18kHz','PSM 38kHz','PSM 70kHz', 'PSM 120kHz','PSM 200kHz','PSM 333kHz');
% % axis([0 100 -90 -50]);
% grid on;
% toto=xlabel('Angle (�)');
% set(toto,'FontSize',16)
% toto=ylabel('TS  (dB)');
% set(toto,'FontSize',16)
% title({['TS for gas bubble of size ', num2str(a*1000), ' mm',' at depth ',num2str(depth),'m, aspect ratio ',num2str(applatissement) ];[' sound speed in water ',num2str(c_w),' m/s water density ',num2str(p_w),' kg/m3 sound speed in gas ',num2str(c_c),' m/s gas density ',num2str(p_c*(1+0.1*depth)) ' kg/m3']},'FontSize',8);
% % saveas(gcf,['S:\Th�se Barbara\','TSversusangle_',num2str(round(applatissement)),'_',num2str(depth),'_',num2str(a*1000)],'fig')
% % saveas(gcf,['S:\Th�se Barbara\','TSversusangle_',num2str(round(applatissement)),'_',num2str(depth),'_',num2str(a*1000)],'jpg')
