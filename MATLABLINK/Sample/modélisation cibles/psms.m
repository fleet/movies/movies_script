function f_all = psms(freq, c, a, b, theta, c1, rh)
    
% A function to estimate the backscattered target strength of a pressure
% release, vacuum filled, prolate spheroid or a fluid-filled prolate
% spheroid calculated using the PSMS method of Furusawa (1988) and
% corrections in Furusawa et al. (1994)
% 
% The F/2a at the bottom of page 15 in Furusawa (1988) is returned. To get
% TS from that, multiply by 2a and take 20*log10().
%
% Calculating F/2a is a interative process - all intermediate values are
% also returned so that you can check for convergence. The last element in
% the returned vector is the best estimate.
% 
% freq is the acoustic frequency [Hz].
% 
% c is the acoustic sound speed [m/s] in the medium surrounding the prolate
% spheroid.
%
% a is the prolate spheroid major axis radius [m].
% 
% b is the prolate spheroid minor axis radius [m].
% 
% theta is the incident wave angle [degrees] in the fore/aft direction
% and corresponds to pitch. A value of 0 is end on (to the 'front' of the
% prolate spheroid) and a value of 90 is the normal direction (dorsal),
% while a value of 180 is end on (to the 'rear' of the prolate spheroid).
% 
% c1 is the acoustic sound speed [m/s] in the prolate sphere (only required 
%   for the fluid-filled model).
%
% rh = rho1/rho0, the ratio of densities inside (rho1) and outside (rho0)
%   the spheroid (only required for the fluid-filled model).
% 
% Refer to Figure 1 in Furusawa (1988) for further enlightnment.
%
% REFERENCES
%
% Furusawa, M. (1988). "Prolate spheroidal models for predicting general
%     trends of fish target strength," J. Acoust. Soc. Jpn. 9, 13-24. 
% Furusawa, M., Miyanohana, Y., Ariji, M., and Sawada, Y. (1994).
%     �Prediction of krill target strength by liquid prolate spheroid
%     model,� Fish. Sci., 60, 261�265.



if nargin == 5
    c1 = NaN;
    rh = NaN;
    type = 'void';
else
    type = 'fluid';
    hc = c1/c; % ratio of sound speed inside/outside the spheroid.
end

% The wavenumber for the surrounding fluid
k0 = 2*pi*freq/c;
% The semi-focal length of the prolate spheroid (same as for an ellipse)
q = sqrt(a*a - b*b);
% An alternative to ka is kq, the wavenumber multiplied by the focal length
h0 = k0 * q;
% Epsilon is a characteristic of the spheroid, whereby a = xi0 * q,
% hence converting this to use a and b gives:
xi0 = (1.0 - (b/a)^2)^-.5;
% Phi, the port/starboard angle is fixed for this code
phi_inc = pi; % radians, incident direction
phi_bs = 0; % radians, backscattered direction
theta = theta*pi/180;

% Approximate limits on the summations
m_max = ceil(2*k0*b);
n_max = m_max + ceil(h0/2);

% disp(['m_max = ' num2str(m_max) ', n_max = ' num2str(n_max)])
% disp(['xi0 = ' num2str(xi0)])
% disp(['ho = ' num2str(h0)])

% Terms in the summation smaller than this will cause the iteration to
% stop. Even and odd values of (m-n) are treated separately.
% The value is quite low as some of the summations start out low then rise
% quite a lot. This value will get rid of the summations that don't add
% much. 
tol = -400; % [dB] 

f = 0.0;
idx = 1;
for m = 0:m_max
    % epsilon is the Neumann factor, a rather obtuse way of saying this:
    if m == 0
        epsilon = 1;
    else
        epsilon = 2;
    end

    % reset the tolerance flags for the new m
    even_reached_tol = false;
    odd_reached_tol = false;
    
    %disp('============')
    %disp(['TS = ' num2str(20*log10(abs(2/(1j*k0)*f)))])

    for n = m:n_max
        if rem((m-n),2) == 0
            even = true;
        else
            even = false;
        end

        if (even && even_reached_tol) || (~even && odd_reached_tol)
            continue;
        end
        
        %disp(['aswfa(' num2str(m) ', ' num2str(n) ', ' num2str(h0) ', cos(' num2str(theta) '), 1)'])
        s_bs = aswfb(m, n, h0, cos(theta), 1);
        %disp(['aswfa(' num2str(m) ', ' num2str(n) ', ' num2str(h0) ', cos(' num2str(theta) '), 1) = ' num2str(s_bs)])
        
        s_inc = aswfb(m, n, h0, cos(pi - theta), 1);
        %disp(['aswfa(' num2str(m) ', ' num2str(n) ', ' num2str(h0) ', cos(' num2str(pi-theta) '), 1) = ' num2str(s_inc)])

        if strcmp(type, 'fluid')
            [r_type1A, dr_type1A, r_type2A, dr_type2A] = rswfp(m, n, h0, xi0, 3);
            [r_type1B, dr_type1B, ~, ~] = rswfp(m, n, h0/hc, xi0, 3);
            eeA = r_type1A - rh*r_type1B/dr_type1B*dr_type1A;
            eeB = eeA + 1j*(r_type2A - rh*r_type1B/dr_type1B*dr_type2A);
            aa = -eeA/eeB; %Furusawa (1988) Eq. 5 p 15
        else % 'void'
             [r_type1, ~, r_type2, ~] = rswfp(m, n, h0, xi0, 3);
             %disp(['rswfp(' num2str(m) ', ' num2str(n) ', ' num2str(h0) ', ' num2str(xi0) ', 1) = ' num2str(r_type1)])
             %disp(['rswfp(' num2str(m) ', ' num2str(n) ', ' num2str(h0) ', ' num2str(xi0) ', 2) = ' num2str(r_type2)])
             aa = -r_type1/(r_type1 + 1j*r_type2);
        end
        
        % This definition of the norm of S is in Yeh (1967), and is equation
        % 21.7.11 in Abramowitz & Stegun (10th printing) as the
        % Meixner-Sch�fke normalisation scheme. Note that the RHS of
        % 21.7.11 doesn't give correct results compared to doing the actual
        % integration. Note also that the plain Matlab quad() function
        % fails to give correct answers also in some cases, so the quadgk()
        % function us used. 
        %
        % Yeh, C. (1967). "Scattering of Acoustic Waves by a Penetrable
        %   Prolate Spheroid I Liquid Prolate Spheroid," J. Acoust. Soc.
        %   Am. 42, 518-521.
        %
        % Abramowitz, M., and Stegun, I. A. (1964). Handbook of
        %   Mathematical Functions with Formulas, Graphs, and Mathematical
        %   Tables (Dover, New York), 10th ed. 

        n_mn = quadgk(@v_aswfa2, -1, 1, 'RelTol', 1e-5);
    
        x = epsilon / n_mn * s_inc * aa * s_bs * cos(m*(phi_bs - phi_inc));
        f = f + x;

        tsx = 20*log10(abs(2/(1j*k0)*x));
        % if even
        %     disp([num2str(m) ' ' num2str(n) ' ' num2str(tsx)])
        % else
        %     disp(['                   ' num2str(m) ' ' num2str(n) ' ' num2str(tsx)])
        % end
        %
        if even && tsx < tol
            even_reached_tol = true;
        elseif ~even && tsx < tol
            odd_reached_tol = true;
        end
        f_all(idx) = f; %#ok<AGROW>
        idx = idx + 1;
    end
end

% Apply the factor that doesn't vary with m or n, and normalise to make it
% independent of the physical size of the spheroid
f_all = abs(2.0 / (1j*k0) .* f_all) / (2.0*a);
    
% A nested function to provide an appropriate vectorised interface of the
% aswfa() function. This is used by the quadgk() numerical integration
% function.
    function s2 = v_aswfa2(eta)
        % Calculates the square of S_mn for the given values of eta. Uses
        % globally avaiable values of m, n, and h0.
        s2 = ones(size(eta));
        for i = 1:length(eta)
            s2(i) = aswfa(m, n, h0, eta(i), 1);
        end
        s2 = s2 .^ 2;
    end
    
end

