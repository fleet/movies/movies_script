

close all;
clear all;

mydir  = pwd;
idcs   = strfind(mydir,'\')
pathUseful = [mydir '\Modélisation_cibles_gazeuses\acoustic_model\my_functions'];

addpath(genpath(pathUseful)) 

pathUseful = [mydir(1:idcs(end)-1) '\Calibration'];

addpath(genpath(pathUseful)) 


% taille0=2e-3;
cc_plexi=[2690,1340];
cc_WC=[6853,4171];
cc_AL=[6420,3040]
cc_verre=[5761,3437];
rho_plexi=1191;
rho_WC=14900;
rho_AL=2700;
rho_eau=1030;
rho_verre=2500;
c_eau=1500;
g_plexi=rho_plexi/rho_eau;
h_plexi=cc_plexi(1)/c_eau;
g_verre=rho_verre/rho_eau;
h_verre=cc_verre(1)/c_eau;
rho_air=1.24;
c_air=340;
aplatissement=[1];
eta=[0 0.1 1 10 100];
tau=0; 
depth=[0];
g_air=rho_air/rho_eau;
h_air=c_air/c_eau;
freq=[30 50  85 100 120];
taille0=[0.01:0.01:100]*1e-3;
% L = 2 * ((taille0^3)./((1./applatissement).^2)).^(1/3) ;
for t=1:length(taille0)
    
    %         taille=taille0*(1/(1+0.1*d))^(1/3);
    tailleeq=taille0(t);
    for f=1:1:length(freq)
        for e=1:1:length(eta)
            TS_Ye(t,f,e)=gasbubbleye2(freq(f)*1e3,tailleeq/2,depth,aplatissement,rho_eau,c_eau,1.4,eta(e),tau);
            %TS_Kloser(d,f,e)=gasbladder(freq(f)*1e3,tailleeq,depth);
        end
        
    end
end

for t=1:length(taille0)
    tailleeq=taille0(t);
    for f=1:1:length(freq)
            [TS_fluid(f,t),~,~] = ModelSpherical('rgfx',freq(f)*1e3,tailleeq/2,g_plexi,h_plexi,c_eau);
        
    end
end

for t=1:length(taille0)
    tailleeq=taille0(t);
    for f=1:1:length(freq)
            [TS_gaseous(f,t),~,~] = ModelSpherical('gaze',freq(f)*1e3,tailleeq/2,g_air,h_air,c_eau);
        
    end
end

for t=1:length(taille0)
    tailleeq=taille0(t)*1e3/2;
    for f=1:1:length(freq)
            q=2*pi*freq(f)*tailleeq/c_eau;
            x=form(rho_plexi/rho_eau,cc_plexi,c_eau,q,tailleeq);
            TS_plexi(t,f) = x(2);       
    end
end

for t=1:length(taille0)
    tailleeq=taille0(t)*1e3/2;
    for f=1:1:length(freq)
            q=2*pi*freq(f)*tailleeq/c_eau;
            x=form(rho_verre/rho_eau,cc_verre,c_eau,q,tailleeq);
            TS_verre(t,f) = x(2);       
    end
end

for t=1:length(taille0)
    tailleeq=taille0(t)*1e3/2;
    for f=1:1:length(freq)
            q=2*pi*freq(f)*tailleeq/c_eau;
            x=form(rho_WC/rho_eau,cc_WC,c_eau,q,tailleeq);
            TS_WC(t,f) = x(2);       
    end
end

for t=1:length(taille0)
    tailleeq=taille0(t)*1e3/2;
    for f=1:1:length(freq)
            q=2*pi*freq(f)*tailleeq/c_eau;
            x=form(rho_AL/rho_eau,cc_AL,c_eau,q,tailleeq);
            TS_AL(t,f) = x(2);       
    end
end


% save ("-V6","/home/mathieu/Documents/Data/TS/YeTS18.mat","TS_Ye")


% figure(1)
% imagesc(TS);
% xlabel('taile (mm)');
% ylabel('depth (m)')
% title('TS versus depth and frequency');
% % caxis([ -80 -50]);

figure;
semilogx(taille0*1e3,squeeze(TS_Ye(:,5,:))');
hold on;
semilogx(taille0*1e3,squeeze(TS_plexi(:,5))','k');
semilogx(taille0*1e3,squeeze(TS_verre(:,5))','b');
semilogx(taille0*1e3,squeeze(TS_AL(:,5))','c');
% semilogx(taille0*1e3,squeeze(TS_fluid(2,:)),'r');
semilogx(taille0*1e3,squeeze(TS_gaseous(5,:)),'g');
xlabel('Diamètre (mm)');
ylabel('TS (dB)')
title(sprintf('TS en fonction du diamètre pour des sphères de différents matériaux à 120 kHz'));
axis([0 100 -90 -10]);
grid on;
text(6,-25,'Plexiglass')
text(6,-40,'Verre sodocalcique','Color','b')
text(6,-50,'Aluminium','Color','c')
text(0.15,-50,'Sphère gazeuse avec différentes viscossités pour la paroi','Color','r')
text(1,-40,'Sphère gazeuse simplifiée','Color','green')
legend('0','0.1','1','10','100')

% 
% figure(3)
% semilogx(freq,squeeze(TS_Ye(:,:,1))');
% hold on;
% semilogx(freq,squeeze(TS_Kloser(:,:,1))','r');
% xlabel('frequency (kHz)');
% ylabel('TS (dB)')
% title(['Comparison Kloser Ye for different aspect ratio for 0.3mm ESR']);
% axis([10000 200000 -100 -40]);
% grid on;
% legend('Ye sphere','Ye aplatissement 5','Ye aplatissement 10','Kloser');

% densite=[8 11 16.5 12.4 6.1 0];
% densite=[1 1 1 1 1 1];
% figure(4)
% semilogx(freq([18 38 70 120 200]),squeeze(TS(5,[18 38 70 120 200],:))+repmat(10*log10(densite),5,1));
% xlabel('frequency (kHz)');
% ylabel('TS (dB)')
% title(['TS versus frequency for different viscosity']);
% axis([18 200 -90 -50]);