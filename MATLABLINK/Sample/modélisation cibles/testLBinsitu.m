% close all;
clear all;

mydir  = pwd;
idcs   = strfind(mydir,'\')
pathUseful = [mydir '\Modélisation_cibles_gazeuses\acoustic_model\my_functions'];

addpath(genpath(pathUseful)) 

% taille0=2e-3;
aplatissement=[1:2:5];
eta=[0.1:0.1:1];
depth=[15];
freq=[1:1:90];
taille0=0.5*1e-3;
% L = 2 * ((taille0^3)./((1./applatissement).^2)).^(1/3) ;
for d=1:length(aplatissement)
%         taille=taille0*(1/(1+0.1*d))^(1/3);
tailleeq=taille0;
    for f=1:1:length(freq)
        for e=1:1:length(eta)
        mTS_Ye(d,f,e)=gasbubbleye2(freq(f)*1e3,tailleeq,depth,aplatissement(d),1030,1500,1.4,eta(e),75*10^-3);
        %TS_Kloser(d,f,e)=gasbladder(freq(f)*1e3,tailleeq,depth);
        end
    end 
end

% % taille0=2e-3;
% aplatissement=[1:1:10];
% eta=[0:0.1:1];
% depth=[30];
% freq=[1:1:333];
% taille0=0.22*1e-3;
% % L = 2 * ((taille0^3)./((1./applatissement).^2)).^(1/3) ;
% for d=1:length(aplatissement)
% %         taille=taille0*(1/(1+0.1*d))^(1/3);
% tailleeq=taille0;
%     for f=1:1:length(freq)
%         for e=1:1:length(eta)
%         TS_Ye1(d,f,e)=gasbubbleye(freq(f)*1e3,tailleeq,depth,aplatissement(d),1030,1500,1.4,eta(e),75*10^-3);
%         %TS_Kloser(d,f,e)=gasbladder(freq(f)*1e3,tailleeq,depth);
%         end
%     end 
% end
% 
% aplatissement=[1:1:10];
% eta=[0:0.1:1];
% depth=[95];
% freq=[1:1:333];
% taille0=0.48*1e-3;
% % L = 2 * ((taille0^3)./((1./applatissement).^2)).^(1/3) ;
% for d=1:length(aplatissement)
% %         taille=taille0*(1/(1+0.1*d))^(1/3);
% tailleeq=taille0;
%     for f=1:1:length(freq)
%         for e=1:1:length(eta)
%         TS_Ye2(d,f,e)=gasbubbleye(freq(f)*1e3,tailleeq,depth,aplatissement(d),1030,1500,1.4,eta(e),75*10^-3);
%         %TS_Kloser(d,f,e)=gasbladder(freq(f)*1e3,tailleeq,depth);
%         end
%     end 
% end
% 
% aplatissement=[1:1:10];
% eta=[0:0.1:1];
% depth=[80];
% freq=[1:1:333];
% taille0=0.39*1e-3;
% % L = 2 * ((taille0^3)./((1./applatissement).^2)).^(1/3) ;
% for d=1:length(aplatissement)
% %         taille=taille0*(1/(1+0.1*d))^(1/3);
% tailleeq=taille0;
%     for f=1:1:length(freq)
%         for e=1:1:length(eta)
%         TS_Ye3(d,f,e)=gasbubbleye(freq(f)*1e3,tailleeq,depth,aplatissement(d),1030,1500,1.4,eta(e),75*10^-3);
%         %TS_Kloser(d,f,e)=gasbladder(freq(f)*1e3,tailleeq,depth);
%         end
%     end 
% end

% save ("-V6","/home/mathieu/Documents/Data/TS/mYeTS.mat","mTS_Ye")
% save ("-V6","/home/mathieu/Documents/Data/TS/YeTS1.mat","TS_Ye1")
% save ("-V6","/home/mathieu/Documents/Data/TS/YeTS2.mat","TS_Ye2")
% save ("-V6","/home/mathieu/Documents/Data/TS/YeTS3.mat","TS_Ye3")

% figure(1)
% imagesc(TS);
% xlabel('taile (mm)');
% ylabel('depth (m)')
% title('TS versus depth and frequency');
% % caxis([ -80 -50]);

% figure(3)
% plot(freq,(TS_Ye38(6,:,1))');
% max(TS_Ye38(6,:,1))
% max(max(TS_Ye38))
% TS_Ye38(6,38,1)

figure(1)
plot(freq,squeeze(mTS_Ye(:,:,1))');
hold on;
xlabel('frequency (kHz)');
ylabel('TS (dB)')
title(['TS versus frequency for different aspect ratio']);
axis([min(freq) max(freq) -70 -45]);

% figure(2)
% plot(freq,squeeze(TS_Ye(5,:,:))');
% hold on;
% xlabel('frequency (kHz)');
% ylabel('TS (dB)')
% title(['TS versus frequency for different viscosity']);
% axis([18 200 -90 -45]);
% % 
% figure(3)
% semilogx(freq,squeeze(TS_Ye(:,:,1))');
% hold on;
% semilogx(freq,squeeze(TS_Kloser(:,:,1))','r');
% xlabel('frequency (kHz)');
% ylabel('TS (dB)')
% title(['Comparison Kloser Ye for different aspect ratio for 0.3mm ESR']);
% axis([10000 200000 -100 -40]);
% grid on;
% legend('Ye sphere','Ye aplatissement 5','Ye aplatissement 10','Kloser');

% densite=[8 11 16.5 12.4 6.1 0];
% densite=[1 1 1 1 1 1];
% figure(4)
% semilogx(freq([18 38 70 120 200]),squeeze(TS(5,[18 38 70 120 200],:))+repmat(10*log10(densite),5,1));
% xlabel('frequency (kHz)');
% ylabel('TS (dB)')
% title(['TS versus frequency for different viscosity']);
% axis([18 200 -90 -50]);