function [hh,mm,ss]=read_day_time_raw(datetime)

%datetime=12899995773.49; %s
%datetime=floor(datetime*100)/100;
% 
% if strcmp(typ,'raw')
%     reftime=(datenum('2000:01:01','yyyy:mm:dd')-datenum('01-Jan-1601'))*24*3600;  %raw
% else
%     reftime=(datenum('2000:01:01','yyyy:mm:dd')-datenum('01-Jan-1970'))*24*3600;   %hac
% end

%date=datestr(floor(datetime/(24*3600))+datenum('01-Jan-1970'))%hac
date=datestr(floor(datetime/(24*3600))+datenum('01-Jan-1601'))%raw

time_day=rem(datetime,24*3600);

hh=floor(time_day/3600);
mm=floor((time_day/3600-hh)*60);
ss=((time_day/3600-hh)*60-mm)*60;
