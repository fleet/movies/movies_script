% Reading EK60 raw data file configuration header
% Simrad, Lars Nonboe Andersen, 21/12-01

function configheader = readconfigheader(fid);

configheader.surveyname = char(fread(fid,128,'char')');
configheader.transectname = char(fread(fid,128,'char')');
configheader.soundername = char(fread(fid,128,'char')');

configheader.spare = char(fread(fid,128,'char')');

configheader.transducercount = fread(fid,1,'int32');
