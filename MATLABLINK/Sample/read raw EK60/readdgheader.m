% Reading EK60 raw data file datagram header
% Simrad, Lars Nonboe Andersen, 8/5-03

function dgheader = readdgheader(fid);

dgheader.datagramtype = char(fread(fid,4,'char')');

lowdatetime = fread(fid,1,'uint32');
highdatetime = fread(fid,1,'uint32');

dgheader.datetime = highdatetime*2^32 + lowdatetime;