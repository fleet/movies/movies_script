function Attitude=readraw(fname)

% Reading SMS raw data file
% Simrad, Lars Nonboe Andersen, 12/9-05

%clear all
%close all

%fname = 'C:\SourceSafe\SMS\Tools\Matlab\Simrad Echo\BottomProfile-D20050920-T100059.raw'; % Name of file to read
%fname = 'C:\data\PELGAS08\PELGAS08-D20080518-T140236.raw'; % Name of file to read
%fname = 'K:\SMFH\EVHOE09-BATHY-141009\EVHOE09\EVHOE09-D20091014-T120933.raw'; % Name of file to read

headerlength = 12; % Bytes in datagram header

pingno = 0;
nrefbeams = 0;
pingtime=0;
lastpingtime=0;
pingrate = [];

fid = fopen(fname,'r');
if (fid==-1)
    error('Could not open file');
else
    % Read configuration datagram
    length = fread(fid,1,'int32');
    dgheader = readdgheader(fid);
    configheader = readconfigheader(fid);
    for i=1:configheader.transducercount,
        configtransducer(i) = readconfigtransducer(fid);
        if ~isempty(findstr(configtransducer(i).channelid,'Reference'))
            nrefbeams = nrefbeams+1;
        end
    end
    config = struct('header',configheader,'transducer',configtransducer);
    config.header.nrefbeams = nrefbeams;
    length = fread(fid,1,'int32');
    
    indexpos=1;
    % Read NMEA, Annotation, or Sample datagram
    while (1)
        length = fread(fid,1,'int32');
        if (feof(fid))
            break
        end
        dgheader = readdgheader(fid);

        
        
        switch (dgheader.datagramtype)
        case 'CON1' % SMS extra configuration datagram
            text = readtextdata(fid,length-headerlength);
            disp('CON1');
        case 'NME0' % NMEA datagram
            text = readtextdata(fid,length-headerlength);
            if text.text(1:6)=='$INGLL'
                lat(indexpos)=str2num(text.text(8:18));
                long(indexpos)=str2num(text.text(22:30));
                indexpos=indexpos+1;
            end
            %disp('NME0');
        case 'TAG0' % Annotation datagram
            text = readtextdata(fid,length-headerlength);
            %disp('TAG0');
        case 'RAW0' % Sample datagram
            sampledata = readsampledata(fid);
            %disp('RAW0');
           
            % WRITE YOUR OWN CODE HERE TO PROCESS AND/OR DISPLAY SAMPLE DATA
%             channel = sampledata.channel
      
%                 tvgsampledata = applytvg(config,sampledata);
%                 tvgdata(channel) = tvgsampledata;
                Attitude(1,pingno+1) = sampledata.heave;
                Attitude(2,pingno+1) = sampledata.roll;
                Attitude(3,pingno+1) = sampledata.pitch;
                Attitude(4,pingno+1) =  dgheader.datetime/ 10000000;
                pingno=pingno+1;
        
        otherwise
            %error(strcat('Unknown datagram ''',dgheader.datagramtype,''' in file'));
        end
        length = fread(fid,1,'int32');

    end
    fclose(fid);
end



disp('Finished reading file');

    
