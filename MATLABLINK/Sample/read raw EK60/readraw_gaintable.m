function [gain_table, sa_table,angle]=readraw_gaintable(fname)

% Reading SMS raw data file
% Simrad, Lars Nonboe Andersen, 12/9-05

%clear all
%close all

%fname ='L:\PELGAS10\ME70_raw\RUN003\PELGAS10-D20100428-T092407.raw'; %Name of
%file to read 

headerlength = 12; % Bytes in datagram header

pingno = 0;
pingno2 = 0;
nrefbeams = 0;
pingtime=0;
lastpingtime=0;
pingrate = [];

fid = fopen(fname,'r');
if (fid==-1)
    error('Could not open file');
else
    % Read configuration datagram
    length = fread(fid,1,'int32');
    dgheader = readdgheader(fid);
    configheader = readconfigheader(fid);
    for i=1:configheader.transducercount,
        configtransducer(i) = readconfigtransducer(fid);
        if ~isempty(findstr(configtransducer(i).channelid,'Reference'))
            nrefbeams = nrefbeams+1;
        end
    end
    config = struct('header',configheader,'transducer',configtransducer);
    config.header.nrefbeams = nrefbeams;
    length = fread(fid,1,'int32');
    
    indexpos=1;
    
    %lecture de la table de gains
    for ib=1:size(config.transducer,2)
        gain_table(ib)=config.transducer(ib).gain;
        sa_table(:,ib)=config.transducer(ib).sacorrectiontable;
        angle(ib)=config.transducer(ib).diry;
    end
    
    fclose(fid);
end



disp('Finished reading file');

    
