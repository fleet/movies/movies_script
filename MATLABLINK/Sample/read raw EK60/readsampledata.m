% Reading EK60 raw data file sample data
% Simrad, Lars Nonboe Andersen, 12/04-02

function sampledata = readsampledata(fid)

sampledata.channel = fread(fid,1,'int16');
mode_low = fread(fid,1,'int8');
mode_high = fread(fid,1,'int8');
sampledata.mode = 256*mode_high + mode_low;
sampledata.transducerdepth = fread(fid,1,'float32');
sampledata.frequency = fread(fid,1,'float32');
sampledata.transmitpower = fread(fid,1,'float32');
sampledata.pulselength = fread(fid,1,'float32');
sampledata.bandwidth = fread(fid,1,'float32');
sampledata.sampleinterval = fread(fid,1,'float32');
sampledata.soundvelocity = fread(fid,1,'float32');
sampledata.absorptioncoefficient = fread(fid,1,'float32');
sampledata.heave = fread(fid,1,'float32');
sampledata.roll = fread(fid,1,'float32');
sampledata.pitch = fread(fid,1,'float32');
sampledata.temperature = fread(fid,1,'float32');
sampledata.trawlupperdepthvalid = fread(fid,1,'int16');
sampledata.trawlopeningvalid = fread(fid,1,'int16');
sampledata.trawlupperdepth = fread(fid,1,'float32');
sampledata.trawlopening = fread(fid,1,'float32');
sampledata.offset = fread(fid,1,'int32');
sampledata.count = fread(fid,1,'int32');
%power = fread(fid,[2 sampledata.count],'int8');
%sampledata.power = (power(2,:)*10*log10(2) + power(1,:)*10*log10(2)/256)';
power = fread(fid,sampledata.count,'int16');
sampledata.power = power*10*log10(2)/256;
if (sampledata.mode>1)
    angle = fread(fid,[2 sampledata.count],'int8');
    sampledata.angle = angle(1,:) + angle(2,:)*256;
    sampledata.alongship = angle(2,:)';
    sampledata.athwartship = angle(1,:)';
end

