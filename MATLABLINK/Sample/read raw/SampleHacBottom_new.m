
clear all;
close all;

ParameterKernel= moKernelParameter();
ParameterDef=moLoadKernelParameter(ParameterKernel);

ParameterDef.m_bAutoLengthEnable=1;
ParameterDef.m_MaxRange = 150;
ParameterDef.m_bAutoDepthEnable=1;
ParameterDef.m_bIgnorePhaseData=1;

moSaveKernelParameter(ParameterKernel,ParameterDef);
clear ParameterDef;

ParameterR= moReaderParameter();
ParameterDef=moLoadReaderParameter(ParameterR);

ParameterDef.m_chunkNumberOfPingFan=50;

moSaveReaderParameter(ParameterR,ParameterDef);

chemin_ini='C:\Users\lberger\Desktop\GAZCOGNE3\RUN005\';

filelist = ls([chemin_ini,'*.hac']);  % ensemble des fichiers
nb_files = size(filelist,1);  % nombre de fichiers

crr1=1;
crr2=1;
M1 = [];
M2 = [];
T1 = [];
T2 = [];



for numfile = 1:nb_files % boucle sur l'ensemble des fichiers � traiter

    resultsname = filelist(numfile,:);
    FileName = [chemin_ini,resultsname];

    moOpenHac(FileName);


    FileStatus= moGetFileStatus;
    while FileStatus.m_StreamClosed < 1
        moReadChunk();
        FileStatus= moGetFileStatus();
    end;
    fprintf('End of File');

    VMR_ER60 = [];
    index= [];
    index2 = [];

    for index= 0:moGetNumberOfPingFan()-1
        MX= moGetPingFan(index);
        %if (MX.m_pingId >0)
        SounderDesc =  moGetFanSounder(MX.m_pingId);
        if SounderDesc.m_SounderId == 1
            M1(:,crr1)=[
                MX.navigationAttitude.pitchRad ,
                MX.navigationAttitude.sensorHeaveMeter,
                MX.navigationAttitude.rollRad ,
                MX.navigationAttitude.yawRad ,
                MX.navigationAttribute.m_headingRad,
                MX.navigationAttribute.m_speedMeter,
                MX.m_cumulatedDistance];
            T1(:,crr1)=[MX.m_meanTimeCPU,MX.m_meanTimeFraction];
             LatitudeNavire1(:,crr1) =  MX.navigationPosition.m_lattitudeDeg;
            LongitudeNavire1(:,crr1) =  MX.navigationPosition.m_longitudeDeg;
            
            tab_AmplitudeER60 =[];
            for transducerIndex=1:SounderDesc.m_numberOfTransducer
                for beamIndex=1:SounderDesc.m_transducer(1,transducerIndex).m_numberOfSoftChannel
                    BeamData=MX.beam_data(1,transducerIndex);   
                    datalist = moGetPolarMatrix(MX.m_pingId,transducerIndex-1);
                    % on prend en compte le d�lai interne au passage un
                    % �cho pour 18 et 38 kHz
%                     tab_AmplitudeER60(transducerIndex,:) = 10.^(datalist.m_Amplitude/100/20);
                    tab_AmplitudeER60(transducerIndex,:) = circshift(10.^(datalist.m_Amplitude/100/20),round(transducerIndex/3) -1);
                    if(BeamData.m_bottomWasFound==1)
                        EchoEval=   floor( BeamData.m_bottomRange/SounderDesc.m_transducer(transducerIndex).m_beamsSamplesSpacing);
                        Coord= moGetPolarToGeoCoord(MX.m_pingId,transducerIndex-1,beamIndex-1,EchoEval+2);
                        Latitude1(transducerIndex,crr1)=Coord(1);
                        Longitude1(transducerIndex,crr1)=Coord(2);
                        Depth1(transducerIndex,crr1)=Coord(3);
                        Range1(transducerIndex,crr1)=BeamData.m_bottomRange;
                    else
                        Latitude1(transducerIndex,crr1)=MX.navigationPosition.m_lattitudeDeg;
                        Longitude1(transducerIndex,crr1)=MX.navigationPosition.m_longitudeDeg;
                        Depth1(transducerIndex,crr1)=0;
                    end;
                    Speed1(transducerIndex,crr1) = M1(6,crr1);
                end;
            end; 
            VMR_ER60=10*log(var(tab_AmplitudeER60(1:end,:),1)./mean(tab_AmplitudeER60(1:end,:),1));
            VMR_ER60_util = VMR_ER60(round(10/SounderDesc.m_transducer(transducerIndex).m_beamsSamplesSpacing):end-1);
            index =find(VMR_ER60_util>-25);
            if size(index,2)~=0
                Range1_Demer(crr1) = (round(10/SounderDesc.m_transducer(transducerIndex).m_beamsSamplesSpacing)-1+index(1))*SounderDesc.m_transducer(transducerIndex).m_beamsSamplesSpacing;
                VMR(crr1)= VMR_ER60_util(index(1));
            else
                Range1_Demer(crr1) = Range1(2,crr1);
                VMR(crr1)=0;
            end
                
            
            
            crr1=crr1+1;
            
                    
                    
        elseif SounderDesc.m_SounderId == 2
            M2(:,crr2)=[
                MX.navigationAttitude.pitchRad ,
                MX.navigationAttitude.sensorHeaveMeter,
                MX.navigationAttitude.rollRad ,
                MX.navigationAttitude.yawRad ,
                MX.navigationAttribute.m_headingRad,
                MX.navigationAttribute.m_speedMeter,
                MX.m_cumulatedDistance];
            T2(:,crr2)=[MX.m_meanTimeCPU,MX.m_meanTimeFraction];
           
            LatitudeNavire(:,crr2) =  MX.navigationPosition.m_lattitudeDeg;
            LongitudeNavire(:,crr2) =  MX.navigationPosition.m_longitudeDeg;

            

            for transducerIndex=1:SounderDesc.m_numberOfTransducer
                for beamIndex=1:SounderDesc.m_transducer(1,transducerIndex).m_numberOfSoftChannel
                    BeamData=MX.beam_data(1,beamIndex);
          
                    datalist = moGetPolarMatrix(MX.m_pingId,0);
                    tab_Amplitude = datalist.m_Amplitude/100;
                    tab_phasealong = datalist.m_AlongAngle;
                    tab_phaseathwart = datalist.m_AthwartAngle;
        
                    if(BeamData.m_bottomWasFound==1)
                        EchoEval=   floor( BeamData.m_bottomRange/SounderDesc.m_transducer.m_beamsSamplesSpacing);
                        Coord= moGetPolarToGeoCoord(MX.m_pingId,0,beamIndex-1,EchoEval);
                        Latitude(beamIndex,crr2)=Coord(1);
                        Longitude(beamIndex,crr2)=Coord(2);
                        Along(beamIndex,crr2)= cos(M2(1,crr2))*SounderDesc.m_transducer(1,transducerIndex).m_pPlatform.along_ship_offset ...
                                                + sin(M2(1,crr2))*sin(M2(2,crr2))*SounderDesc.m_transducer(1,transducerIndex).m_pPlatform.athwart_ship_offset ...
                                                + sin(M2(1,crr2))*cos(M2(2,crr2))*SounderDesc.m_transducer(1,transducerIndex).m_pPlatform.depth_offset ...
                                                +BeamData.m_bottomRange*sin(SounderDesc.m_transducer(1,transducerIndex).m_SoftChannel(1,beamIndex).m_mainBeamAlongSteeringAngleRad);
                        Athwart(beamIndex,crr2)= cos(M2(2,crr2))*SounderDesc.m_transducer(1,transducerIndex).m_pPlatform.athwart_ship_offset ...
                                                - sin(M2(2,crr2))*sin(M2(2,crr2))*SounderDesc.m_transducer(1,transducerIndex).m_pPlatform.depth_offset ...
                                                + SounderDesc.m_transducer(1,transducerIndex).m_pPlatform.athwart_ship_offset + BeamData.m_bottomRange*sin(SounderDesc.m_transducer(1,transducerIndex).m_SoftChannel(1,beamIndex).m_mainBeamAthwartSteeringAngleRad);
                        Depth(beamIndex,crr2)=Coord(3);
                        Range(beamIndex,crr2)=BeamData.m_bottomRange;
                    else
                        Latitude(beamIndex,crr2)=MX.navigationPosition.m_lattitudeDeg;
                        Longitude(beamIndex,crr2)=MX.navigationPosition.m_longitudeDeg;
                        Depth(beamIndex,crr2)=0;
                    end;
                    Angle(beamIndex,crr2)=SounderDesc.m_transducer(1,transducerIndex).m_SoftChannel(1,beamIndex).m_mainBeamAthwartSteeringAngleRad*180/3.14159;
                    Speed2(beamIndex,crr2) = M2(6,crr2);
                end;
            end;
            crr2=crr2+1;
        end;
    end;
end;

figure;

plot((T1(1,:)+T1(2,:)/10000)/86400 +719529,Range1(2,:)-Range1_Demer, 'b');
hold on
plot((T1(1,:)+T1(2,:)/10000)/86400 +719529,VMR,'r');
datetickzoom('x','dd/mm HH:MM');
xlabel('Heure')

figure;
plot((T1(1,:)+T1(2,:)/10000)/86400 +719529,Range1(2,:),'b');
hold on
plot((T1(1,:)+T1(2,:)/10000)/86400 +719529,Range1_Demer,'r');
datetickzoom('x','dd/mm HH:MM');


% figure;
% subplot(2,1,1);
% plot(T1(2,:)+T1(3,:)/1000-T1(2,1),M1(2,:),'b');
% hold on
% plot(T2(2,:)+T2(3,:)/1000-T2(2,1),M2(2,:),'r');
% title('heave');
% legend('ER60','ME70');
% xlabel('Relative Time (s)'); ylabel('heave (m)')
% 
% subplot(2,1,2);
% plot(T1(2,:)+T1(3,:)/1000-T1(2,1),Depth1(4,:),'b');
% hold on
% plot(T2(2,:)+T2(3,:)/1000-T2(2,1),Depth(11,:),'r');
% title('DepthComp');
% 
% figure;
% subplot(2,1,1);
% plot(T1(2,:)+T1(3,:)/1000-T1(2,1),M1(3,:),'b');
% hold on
% plot(T2(2,:)+T2(3,:)/1000-T2(2,1),M2(3,:),'r');
% title('roll');
% legend('ER60','ME70');
% xlabel('Relative Time (s)'); ylabel('roll (�)')
% 
% subplot(2,1,2);
% plot(T1(2,:)+T1(3,:)/1000-T1(2,1),M1(1,:),'b');
% hold on
% plot(T2(2,:)+T2(3,:)/1000-T2(2,1),M2(1,:),'r');
% title('pitch');
% 
% legend('ER60','ME70');
% xlabel('Relative Time (s)'); ylabel('pitch (�)')
% 
% figure;
% 
% plot(T2(2,:)+T2(3,:)/1000-T2(2,1),M2(5,:)*180/3.14159,'r');
% title('yaw');
% 
% 
% 
% 
% 
figure;
plot(100*std(Depth(1:end,:),0,2)/mean(Depth(1:end,:),2));
hold on;
% plot(100*std(Depth1(1:end,:),0,2)/mean(Depth1(1:end,:),2));

xlabel('BeamNumber'); ylabel('STD (%WD)')


figure;
plot(reshape(Depth(:,10),1,[]));

figure;

 scatter(reshape(Longitude(:,:),1,[]),reshape(Latitude(:,:),1,[]),10,-reshape(Depth(:,:),1,[]),'filled');
 

%        FileNameAscii = [chemin_ini,'Sondes_ME70_pos_abs.csv'];
%     fid = fopen(FileNameAscii,'w');
%   
%     nbpings = size(Depth,2);
%     nbfaisceauME70 = size(Depth,1);
%     fprintf(fid,'> %d\n', nbfaisceauME70);
%     for j=1:nbpings
%         for i=2:nbfaisceauME70-1
%             if (Depth(i,j)>0)
%                 fprintf(fid,'%3.6f ',Latitude(i,j));
%                 fprintf(fid,'%3.6f ',Longitude(i,j));
%                 fprintf(fid,'%3.6f ',Depth(i,j));
%                 fprintf(fid,'%s ',datestr((T2(1,j)+T2(2,j)/1000)/86400 +719529,'dd/mm/yyyy HH:MM:SS.FFF'));
%                 fprintf(fid,'%3.2f ',Angle(i,j));
%                 fprintf(fid,'%d\n',i);
%             end;
%         end;
%     end;
%     
%        FileNameAscii = [chemin_ini,'Sondes_ME70_pos_rel.csv'];
%     fid = fopen(FileNameAscii,'w');
%   
%     nbpings = size(Depth,2);
%     nbfaisceauME70 = size(Depth,1);
%     fprintf(fid,'> %d\n', nbfaisceauME70);
%     for j=1:nbpings
%         for i=2:nbfaisceauME70-1
%             if (Depth(i,j)>0)
%                 fprintf(fid,'%3.6f ',LatitudeNavire(j));
%                 fprintf(fid,'%3.6f ',LongitudeNavire(j));
%                 fprintf(fid,'%3.6f ',Depth(i,j));
%                 fprintf(fid,'%s ',datestr((T2(1,j)+T2(2,j)/10000)/86400 +719529,'dd/mm/yyyy HH:MM:SS.FFF'));
%                 fprintf(fid,'%3.2f ',Angle(i,j));
%                 fprintf(fid,'%d ',i);
%                 fprintf(fid,'%3.2f ',Along(i,j));
%                 fprintf(fid,'%3.2f ',Athwart(i,j));
%                 fprintf(fid,'%3.2f\n',M2(5,j)*180/3.14159);
%             end;
%         end;
%     end;
%           
%     FileNameAscii = [chemin_ini,'Sondes_ER60_pos_rel.csv'];
%     fid = fopen(FileNameAscii,'w');
%   
%     nbpings = size(Depth1,2);
%     nbfaisceauER60 = size(Depth1,1);
%     fprintf(fid,'> %d\n', nbfaisceauER60);
%     for j=1:nbpings
%         for i=1:nbfaisceauER60
%             if (Depth1(i,j)>0)
%                 fprintf(fid,'%3.6f ',Latitude1(j));
%                 fprintf(fid,'%3.6f ',Longitude1(j));
%                 fprintf(fid,'%3.6f ',Depth1(i,j));
%                 fprintf(fid,'%s\n',datestr((T1(1,j)+T1(2,j)/10000)/86400 +719529,'dd/mm/yyyy HH:MM:SS.FFF'));
%             end;
%         end;
%     end;
%     
%     
%        FileNameAscii = [chemin_ini,'Sondes_ME70_pos_debug.csv'];
%     fid = fopen(FileNameAscii,'w');
%   
%     nbpings = size(Depth,2);
%     nbfaisceauME70 = size(Depth,1);
%     fprintf(fid,'> %d\n', nbfaisceauME70);
%     for j=1:nbpings
%         for i=1:nbfaisceauME70
%             if (Depth(i,j)>0)
%                 fprintf(fid,'%3.6f ',LatitudeNavire(j));
%                 fprintf(fid,'%3.6f ',LongitudeNavire(j));
%                 fprintf(fid,'%3.6f ',Range(i,j));
%                 fprintf(fid,'%3.6f ',Depth(i,j));
%                 fprintf(fid,'%s ',datestr((T2(2,j)+T2(3,j)/1000)/86400 +719529,'dd/mm/yyyy HH:MM:SS.FFF'));
%                 fprintf(fid,'%3.2f ',Angle(i,j));
%                 fprintf(fid,'%d ',i);
%                 fprintf(fid,'%3.2f ',Along(i,j));
%                 fprintf(fid,'%3.2f ',Athwart(i,j));
%                 fprintf(fid,'%3.2f\n',M2(5,j)*180/3.14159);
%             end;
%         end;
%     end;
    
% 
% MaxX=max( reshape(Longitude(4:end-3,1:400),1,[]));
% MinX=min( reshape(Longitude(4:end-3,1:400),1,[]));
% 
% MaxY=max(reshape(Latitude(4:end-3,1:400),1,[]));
% MinY=min(reshape(Latitude(4:end-3,1:400),1,[]));
% 
% nbPoints = 1000;
% Precision = max((MaxX-MinX)/nbPoints,(MaxY-MinY)/nbPoints)
% XI=MinX:Precision:MaxX;
% YI=MinY:Precision:MaxY;
%     figure;
% ZI = griddata( reshape(Longitude(4:end-3,1:400),1,[]),reshape(Latitude(4:end-3,1:400),1,[]),-reshape(Depth(4:end-3,1:400),1,[]),XI,YI');
% hold on;
% surf(XI,YI,ZI,'LineStyle','none');
% view(0,70);
% light
% lighting gouraud


% figure;
% surf(squeeze(Depth(:,:)));

% figure;
% surf(squeeze(Depth1(:,:)));
% 
% 
% figure;
% plot((Depth1(2,:)));


% plot(((T1(1,:)+T1(2,:)/10000)/86400 ),'g');
% hold on

% figure;
% plot(mean(Depth(:,:),2));

