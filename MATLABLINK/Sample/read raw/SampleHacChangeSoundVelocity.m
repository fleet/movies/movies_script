% SampleHacChangeSound Velocity is a script that modify the sound velocity
% used by M3D to compute sample intervall

clear all;

chemin_config='K:\Yannick\WBAT_38kHz_3sectors\Calib38k_lagon\HAC_Calib_38kHz_3sectors_5668pings_433samples\'
chemin_config_reverse = strrep(chemin_config, '\', '/');

moLoadConfig(chemin_config_reverse);

chemin_ini='K:\Yannick\WBAT_38kHz_3sectors\Calib38k_lagon\HAC_Calib_38kHz_3sectors_5668pings_433samples\'


save_path='K:\Yannick\WBAT_38kHz_3sectors\Calib38k_lagon\HAC_Calib_38kHz_3sectors_5668pings_433samples\'

filelist = ls([chemin_ini,'*.hac']);  % ensemble des fichiers
nb_files = size(filelist,1);  % nombre de fichiers

for l=1:1

    FileName=filelist(l,:)

    moOpenHac(strcat(chemin_ini,FileName));
    
    FileStatus= moGetFileStatus;
    while FileStatus.m_StreamClosed < 1
        moReadChunk();
        FileStatus= moGetFileStatus();
    end;
 
    SounderList=moGetSounderList();
    for k= SounderList(1,:)
        moSetSounderSoundVelocity(k.m_SounderId,1540.1);
    end
   
    % now we ask to write what's in memory
    moStartWrite(save_path, 'ER60_CORCEL_');
    % stop Writting
    moStopWrite();
end    
    

