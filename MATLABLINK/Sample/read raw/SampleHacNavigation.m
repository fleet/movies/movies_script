
clear all;
%close all;

chemin_ini='K:\Yannick\HacCWFM\';
chemin_config='K:\Yannick\HacCWFM\ConfigM3D';

 chemin_config_reverse = strrep(chemin_config, '\', '/');
 moLoadConfig(chemin_config_reverse);

filelist = ls([chemin_ini,'*.hac']);  % ensemble des fichiers
nb_files = size(filelist,1);  % nombre de fichiers





warning('off');

moOpenHac(chemin_ini);


FileStatus= moGetFileStatus;
while FileStatus.m_StreamClosed < 1
    moReadChunk();



index_sondeur=1;
list_sounder=moGetSounderList;
nb_transduc=list_sounder(index_sondeur).m_numberOfTransducer;

index= [];
crr1=ones(1,nb_transduc);
M1 = [];
M2 = [];
T1 = [];
T2 = [];

for index= 0:moGetNumberOfPingFan()-1
    MX= moGetPingFan(index);
    %if (MX.m_pingId >0)
    SounderDesc =  moGetFanSounder(MX.m_pingId);
    if SounderDesc.m_SounderId == 1
        for b=1:nb_transduc
            M1(b,:,crr1(b))=[
                MX.beam_data(b).navigationAttitude.pitchRad ,
                MX.beam_data(b).navigationAttitude.sensorHeaveMeter,
                MX.beam_data(b).navigationAttitude.rollRad ,
                MX.beam_data(b).navigationAttitude.yawRad ,
                MX.beam_data(b).navigationAttribute.m_headingRad,
                MX.beam_data(b).navigationAttribute.m_speedMeter];
            T1(b,crr1(b))=[MX.beam_data(b).navigationPosition.m_meanTimeCPU+MX.beam_data(b).navigationPosition.m_meanTimeFraction/10000];
            LatitudeNavire1(b,crr1(b)) =  MX.beam_data(b).navigationPosition.m_lattitudeDeg;
            LongitudeNavire1(b,crr1(b)) =  MX.beam_data(b).navigationPosition.m_longitudeDeg;
            
            crr1(b)=crr1(b)+1;
            
        end;
    end;
end;
FileStatus= moGetFileStatus();
end

fprintf('End of File');


% Carto.Ellipsoide.Type = 11; % WGS84
% Carto.Ellipsoide.Excentricite = 000.08181919104281098;
% Carto.Ellipsoide.DemiGrandAxe = 006378137.00000000000; % (m)
% Carto.Projection.Lambert.Type                 = 1; % Lambert 93'
% Carto.Projection.Lambert.first_paral          =  44.000000;
% Carto.Projection.Lambert.second_paral         = 49.000000;
% Carto.Projection.Lambert.long_merid_orig      = 3.000000;
% Carto.Projection.Lambert.X0                   = 700000.000000;
% Carto.Projection.Lambert.Y0                   = 6600000.000000;
% Carto.Projection.Lambert.scale_factor         = 1.000000;
%
%
% [x, y] = latlon2xy(Carto,LatitudeNavire1, LongitudeNavire1);
% [xdebut ydebut] = latlon2xy(Carto,LatitudeNavire1(1),LongitudeNavire1(1));
% zdebut=0;
% x = x-xdebut;
% y = y-ydebut;
% z = zdebut-M1(2,:);
%
%
% figure;
% scatter(x,y,2);
% text(x(1:100:end),y(1:100:end),datestr((T1(1,1:100:end)+T1(2,1:100:end)/10000)/86400 +719529,'HH:MM'),'FontSize',10);
% grid on;
% xlabel('E/W  distance (m)');
% ylabel('N/S  distance (m)');
% title(['Vessel track']);
%
% save ([chemin_ini, 'navigation'],'LatitudeNavire','LongitudeNavire') ;
% 
% vesselspeed=zeros(1,length(LatitudeNavire1));
% dist=distance(LatitudeNavire1(1:end-1),LongitudeNavire1(1:end-1),LatitudeNavire1(2:end),LongitudeNavire1(2:end),3671000);
% elapse=(T1(1,2:end)+T1(2,2:end)/10000)-(T1(1,1:end-1)+T1(2,1:end-1)/10000);
% vesselspeed(2:end)=(dist./elapse)*(3600/1852);
% 
% span = 3; % Size of the averaging window
% window = ones(span,1)/span;
% smoothed_vesselspeed = conv(vesselspeed,window,'same');
% 
% speedlimit=3;
% figure;
% geoshow(LatitudeNavire1(vesselspeed<=speedlimit),LongitudeNavire1(vesselspeed<=speedlimit),'DisplayType','point','Color', 'r', 'MarkerEdgeColor', 'auto');
% geoshow(LatitudeNavire1(vesselspeed>speedlimit),LongitudeNavire1(vesselspeed>speedlimit),'DisplayType','point','Color', 'b', 'MarkerEdgeColor', 'auto');
% % hold on
% % geoshow(X21497(:,2),X21497(:,1))
% 

figure;
hold on;
for b=1:nb_transduc
plot(T1(b,:)/86400 +719529,squeeze(M1(b,1,:))*180/pi)
end
datetickzoom;
grid on
ylim([-20 20])
