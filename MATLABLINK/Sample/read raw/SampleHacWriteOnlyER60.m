% SampleHacWrite is a sample demoonstrating how to use write algorithm
%
% Use this simple implementation to create your own .m processes
%






clear all;
chemin_ini='L:\PELGAS2014\HAChermes\RUN004\ER60_only\';

chemin_config='C:\Temp\remME70\'
chemin_config_reverse = strrep(chemin_config, '\', '/');
save_path='L:\PELGAS2014\HAChermes\RUN004\ER60_only\ER60_only'

 moLoadConfig(chemin_config_reverse);

 filelist = ls([chemin_ini,'*.HAC']);  % ensemble des fichiers
nb_files = size(filelist,1);  % nombre de fichiers

for numfile = 1:nb_files  % boucle sur l'ensemble des fichiers � traiter
    hacfilename = filelist(numfile,:);
    FileName = [chemin_ini,hacfilename];
    moOpenHac(FileName);
    clear FileName;
    
    SounderList=moGetSounderList();
%     printf sounder Id data
    for k= SounderList(1,:)
        if k.m_isMultiBeam==1
            fprintf('remove sounder %d\n',k.m_SounderId);
            moDeleteSounder(k.m_SounderId);
        end
    end
    
    
    % now we ask to write what's in memory
    moStartWrite(save_path, 'Cor_');
    
    % all pingFan already in memory are now written
    %new ones read are also written. These are written after all matlab
    %algorithm are called
    FileStatus= moGetFileStatus;
    while FileStatus.m_StreamClosed < 1
        moReadChunk();
        FileStatus= moGetFileStatus();
    end;
    
    % stop Write
    moStopWrite();
end

