function [ind_deb,time_deb,time_fin]=divise_hac_chunk(chemin_hac,hacfilename,chemin_save,chemin_config,ind_deb)
% chemin_hac : r�pertoire contenant le fichier hac � fragmenter
% hacfilename : nom du fichier hac � fragmenter
% chemin_save : r�pertoire o� sauver les fichiers hac fragment�s
% chemin_config : r�pertoire contenant la configuration movies3D � utiliser
% (un r�pertoire exemple \config_filtre\ est joint). 
% ind_deb : premier num�ro � indiquer pour l'incr�mentation des noms des
% hac


%% fichier � lire
if (0)
    chemin_hac='K:\Basin\DonneesJRoty\1iere-maree-2011\HAC\';
    chemin_save='K:\Basin\DonneesJRoty\1iere-maree-2011\HAC_filtre\';
    hacfilename='D20110120-T221336.hac';
end

FileName = [chemin_hac,hacfilename];
    
%% param�tres de lecture du hac
if (0) %config � la main
    ParameterKernel= moKernelParameter();
    ParameterDef=moLoadKernelParameter(ParameterKernel);

    ParameterDef.m_MaxRange = 200;
    ParameterDef.m_bAutoDepthEnable=0;
    ParameterDef.m_bAutoLengthEnable=1;
    ParameterDef.m_bIgnorePhaseData=1;
    ParameterDef.m_bIgnorePingsWithNoNavigation=0;
    moSaveKernelParameter(ParameterKernel,ParameterDef);
    clear ParameterDef;

    ParameterR= moReaderParameter();
    ParameterDef=moLoadReaderParameter(ParameterR);

    ParameterDef.m_chunkNumberOfPingFan=50;

    moSaveReaderParameter(ParameterR,ParameterDef);
else %charge config
    moLoadConfig(strrep(chemin_config, '\', '/'));
end

moOpenHac(FileName);

%% lecture et filtrage

FileStatus= moGetFileStatus;
ilec=0;
time_deb=[];
time_fin=[];

while FileStatus.m_StreamClosed < 1
%while (ilec < 3 & FileStatus.m_StreamClosed < 1)
    ilec=ilec+1;
    
    %% lecture %%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    disp('debut readchunk')
    moReadChunk();
    disp('fin readchunk')
    FileStatus= moGetFileStatus();
       
    %% recuperation de l'heure
    nb_pings=moGetNumberOfPingFan();
    MX= moGetPingFan(0);
    time_deb(ilec)=MX.m_meanTimeCPU+MX.m_meanTimeFraction/10000;
    MX= moGetPingFan(nb_pings-1);
    time_fin(ilec)=MX.m_meanTimeCPU+MX.m_meanTimeFraction/10000;
    
    %% ecriture hac    
    ad='0000';
    n=length(num2str(ind_deb-1+ilec));
    ad(end-n+1:end)=num2str(ind_deb-1+ilec);
    moStartWrite(chemin_save,['divis_' ad '_']);
    moStopWrite;
end

ind_deb=ind_deb+ilec;

fprintf('End of File');

disp(' ');
disp(' ');

        