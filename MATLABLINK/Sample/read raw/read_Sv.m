
clear all;
% close all;
    tic;
 chemin_ini='C:\Users\lberger\Desktop\PANDORA\';


FileName = [chemin_ini,'CMP002_001_20190908_063209.hac'];
% FileName=chemin_ini;
% FileName = [chemin_ini,'track6_PEMDEZ_1368721333_FMH_16.hac'];
MaxRange=60;
if(1)


ParameterKernel= moKernelParameter();
ParameterDef=moLoadKernelParameter(ParameterKernel);

% longueur auto pour agrandir la m�moire pour permettre de lire un fichier
% entier 1=auto
ParameterDef.m_bAutoLengthEnable=1;
ParameterDef.m_MaxRange = MaxRange;


%on utilise la d�tection ou non pour l'allocation des donn�es en range
%0=non
ParameterDef.m_bAutoDepthEnable=0;
%0 on prend en compte les phases
ParameterDef.m_bIgnorePhaseData=0;
%0 on prend en compte les pings m�me sans nav
ParameterDef.m_bIgnorePingsWithNoNavigation=0;

moSaveKernelParameter(ParameterKernel,ParameterDef);
clear ParameterDef;

ParameterR= moReaderParameter();
ParameterDef=moLoadReaderParameter(ParameterR);

taille_chunk=200;
ParameterDef.m_chunkNumberOfPingFan=taille_chunk;

moSaveReaderParameter(ParameterR,ParameterDef);
else
    chemin_config_reverse = strrep(chemin_ini, '\', '/');
    moLoadConfig(chemin_config_reverse);
end

datestart =07*3600+35*60+00+(datenum('8-Sept-2019')-datenum('01-Jan-1970'))*24*3600; %debut raw

moOpenHac(FileName);
moGoTo(datestart);
    moReadChunk(); %lit un ping suppl�mentaire
    
    %vide la m�moire des pings pr�c�dents
    nb_pings=moGetNumberOfPingFan();
    for ip=nb_pings-1:-1:1
        MX= moGetPingFan(ip-1);
        moRemovePing(MX.m_pingId);
    end
    
    ParameterDef.m_chunkNumberOfPingFan=taille_chunk;
    moSaveReaderParameter(ParameterR,ParameterDef);
    



FileStatus= moGetFileStatus;
ilec=1;
while FileStatus.m_StreamClosed < 1 
    moReadChunk();
    FileStatus= moGetFileStatus();
    ilec=ilec+1;
end;
fprintf('End of File');




list_sounder=moGetSounderList;
nb_snd=length(list_sounder);
disp(' ');
disp(['nb sondeurs = ' num2str(nb_snd)]);
for isdr = 1:nb_snd
    nb_transduc=list_sounder(isdr).m_numberOfTransducer;
    disp(['sondeur ' num2str(isdr) ':    ' 'index: ' num2str(list_sounder(isdr).m_SounderId) '   nb trans:' num2str(nb_transduc)]);
    for itr=1:nb_transduc
        nb_beams=list_sounder(isdr).m_transducer(itr).m_numberOfSoftChannel;
        for ibeam=1:nb_beams
            disp(['   trans ' num2str(itr) ':    ' 'nom: ' list_sounder(isdr).m_transducer(itr).m_transName '   freq: ' num2str(list_sounder(isdr).m_transducer(itr).m_SoftChannel(ibeam).m_acousticFrequency/1000) ' kHz']);
        end
    end
end


%� modifier
index_sondeur=1;
index_trans=1;
nb_beams=list_sounder(index_sondeur).m_transducer(index_trans).m_numberOfSoftChannel;
nb_pings=moGetNumberOfPingFan()-1;
nb_ech=floor(MaxRange/list_sounder(index_sondeur).m_transducer(index_trans).m_beamsSamplesSpacing);



u=NaN(nb_pings,nb_ech,nb_beams);
al=NaN(nb_pings,nb_ech,nb_beams);
ath=NaN(nb_pings,nb_ech,nb_beams);
% bt_sim=NaN(nb_pings,nb_beams);
heave_comp_m=NaN(nb_pings,nb_beams);
dpth_ech=NaN(nb_pings,nb_beams);
% lat=NaN(nb_pings);
% long=NaN(nb_pings);
ping_id=NaN(nb_pings);
% bt_found=NaN(nb_pings);
time_ping=NaN(nb_pings,1);
indexping=1;
for index= 1:nb_pings
        MX= moGetPingFan(index);
        SounderDesc =  moGetFanSounder(MX.m_pingId);
        if SounderDesc.m_SounderId == list_sounder(index_sondeur).m_SounderId
              SounderDesc_ref=SounderDesc;
              datalist = moGetPolarMatrix(MX.m_pingId,index_trans-1);
             
              for b=1:nb_beams
                   u(indexping,1:length(datalist.m_Amplitude),b)=datalist.m_Amplitude/100;
                    al(indexping,1:length(datalist.m_Amplitude),b)=datalist.m_AlongAngle;
                    ath(indexping,1:length(datalist.m_Amplitude),b)=datalist.m_AthwartAngle;
%                   bt_sim(indexping,b)= floor( MX.beam_data(b).m_bottomRange/SounderDesc.m_transducer(index_trans).m_beamsSamplesSpacing);
                  dpth_ech(indexping,b)=MX.beam_data(b).m_bottomRange/SounderDesc.m_transducer(index_trans).m_beamsSamplesSpacing;
%                   bt_found(indexping,b)=MX.beam_data(b).m_bottomWasFound;
%                   heave_comp_m(indexping,b)=MX.beam_data(b).m_compensateheave;
              end            
%               lat(indexping)=MX.navigationPosition.m_lattitudeDeg;
%               long(indexping)=MX.navigationPosition.m_longitudeDeg;
              ping_id(indexping)=MX.m_pingId;
              time_ping(indexping)=MX.m_meanTimeCPU+MX.m_meanTimeFraction/10000;
              indexping=indexping+1;
        end
end

steering=zeros(1,nb_beams);
for b=1:nb_beams
    steering(b)=SounderDesc_ref.m_transducer(index_trans).m_SoftChannel(b).m_mainBeamAthwartSteeringAngleRad;
    bw(b)=SounderDesc_ref.m_transducer(index_trans).m_SoftChannel(b).m_beam3dBWidthAthwartRad*180/pi;
end

 
    date = (time_ping)/86400 +719529;
      datestart=datenum(2014,4,1,10,00,00);
    dateend=datenum(2014,6,1,11,40,00);
    index=(date>datestart & date<dateend);
%           datestart=datenum(2013,4,28,11,21,20);
%     dateend=datenum(2013,4,28,11,21,25);
    
% for i=1:nb_pings
%     if (date(i)>datestart && date(i)<dateend)
%     r=(0:size(u,2)-1)*SounderDesc_ref.m_transducer(index_trans).m_beamsSamplesSpacing;
%     
%     x=(r'*sin(steering))';
%     y=-(r'*cos(steering))';
%     
%     figure;pcolor(x,y,squeeze(u(i,:,:))');caxis([-80 -20]);axis([min(min(x)) max(max(x)) -30 0]);shading('flat');
%     colormap('gray');
%           toto=xlabel('Distance transversale au navire (m)');
%         set(toto,'FontSize',16)
%         toto=ylabel('Profondeur (m)');
%         set(toto,'FontSize',16)
%     end
% end

for b=1:nb_beams

    if (abs(steering(b)*180/pi)<1)
%     heave_comp_ech=round(heave_comp_m(:,b)/SounderDesc.m_transducer(index_trans).m_beamsSamplesSpacing);
%    
%     max_ech=max(heave_comp_ech);
%     min_ech=min(heave_comp_ech);

    % on compense le pilonnement a l'affichage
%     u2=zeros(size(u(:,:,b),2)-min_ech,size(u(:,:,b),1));
%     for i= 1:size(u(:,:,b),1)
%         u2(heave_comp_ech(i)-min_ech+1:heave_comp_ech(i)-min_ech+size(u(:,:,b),2),i)=u(i,:,b);
%     end

    %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % affichage

    figure;DrawEchoGram_brut(squeeze(u(1:1000,1:end,1))',-80,0);colorbar;grid on;
    title([num2str(SounderDesc.m_transducer(index_trans).m_SoftChannel(1).m_acousticFrequency/1000) ' kHz'])
    hold on;plot(dpth_ech(:,b)+1.5,'k','LineW',2);hold off
    
     figure;imagesc(squeeze(al(1:1000,1:end,1))');colorbar;grid on;
     
          figure;imagesc(squeeze(ath(1:1000,1:end,1))');colorbar;grid on;
          
    end

end
toc;
% 
% figure;
% hist(reshape(u(1:160,3000:4000,14:50),[],1),1000);