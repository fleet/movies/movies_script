
clear all;
% close all;
    tic;
chemin_ini='C:\Temp\SBES_MBES\PELGAS16\RUN544\';


FileName = [chemin_ini,'PELGAS16_544_20171026_165340.hac'];
% FileName = [chemin_ini,'track6_PEMDEZ_1368721333_FMH_16.hac'];
MaxRange=10;
if(0)


ParameterKernel= moKernelParameter();
ParameterDef=moLoadKernelParameter(ParameterKernel);

% longueur auto pour agrandir la m�moire pour permettre de lire un fichier
% entier 1=auto
ParameterDef.m_bAutoLengthEnable=1;
ParameterDef.m_MaxRange = MaxRange;


%on utilise la d�tection ou non pour l'allocation des donn�es en range
%0=non
ParameterDef.m_bAutoDepthEnable=0;
%0 on prend en compte les phases
ParameterDef.m_bIgnorePhaseData=0;
%0 on prend en compte les pings m�me sans nav
ParameterDef.m_bIgnorePingsWithNoNavigation=0;

moSaveKernelParameter(ParameterKernel,ParameterDef);
clear ParameterDef;

ParameterR= moReaderParameter();
ParameterDef=moLoadReaderParameter(ParameterR);

ParameterDef.m_chunkNumberOfPingFan=200;

moSaveReaderParameter(ParameterR,ParameterDef);
else
    chemin_config_reverse = strrep(chemin_ini, '\', '/');
    moLoadConfig(chemin_config_reverse);
end

moOpenHac(FileName);


FileStatus= moGetFileStatus;
ilec=1;
while FileStatus.m_StreamClosed < 1 
    moReadChunk();
    FileStatus= moGetFileStatus();
    ilec=ilec+1;
end;
fprintf('End of File');




list_sounder=moGetSounderList;
nb_snd=length(list_sounder);
disp(' ');
disp(['nb sondeurs = ' num2str(nb_snd)]);
for isdr = 1:nb_snd
    nb_transduc=list_sounder(isdr).m_numberOfTransducer;
    disp(['sondeur ' num2str(isdr) ':    ' 'index: ' num2str(list_sounder(isdr).m_SounderId) '   nb trans:' num2str(nb_transduc)]);
    for itr=1:nb_transduc
        nb_beams=list_sounder(isdr).m_transducer(itr).m_numberOfSoftChannel;
        for ibeam=1:nb_beams
            disp(['   trans ' num2str(itr) ':    ' 'nom: ' list_sounder(isdr).m_transducer(itr).m_transName '   freq: ' num2str(list_sounder(isdr).m_transducer(itr).m_SoftChannel(ibeam).m_acousticFrequency/1000) ' kHz']);
        end
    end
end


%� modifier
index_sondeur=1;
index_trans=5;
nb_beams=list_sounder(index_sondeur).m_transducer(index_trans).m_numberOfSoftChannel;
nb_pings=moGetNumberOfPingFan()-1;
nb_ech=floor(MaxRange/list_sounder(index_sondeur).m_transducer(index_trans).m_beamsSamplesSpacing);



% u=NaN(nb_pings,nb_ech,nb_beams);
% bt_sim=NaN(nb_pings,nb_beams);
heave_comp_m=NaN(nb_pings,nb_beams);
dpth_ech=NaN(nb_pings,nb_beams);
% lat=NaN(nb_pings);
% long=NaN(nb_pings);
ping_id=NaN(nb_pings);
% bt_found=NaN(nb_pings);
time_ping=NaN(nb_pings,1);
indexping=1;
for index= 1:nb_pings
        MX= moGetPingFan(index);
        SounderDesc =  moGetFanSounder(MX.m_pingId);
        if SounderDesc.m_SounderId == list_sounder(index_sondeur).m_SounderId
              SounderDesc_ref=SounderDesc;
              datalist = moGetPolarMatrixFM(MX.m_pingId,index_trans-1);
             
              ping_id(indexping)=MX.m_pingId;
              time_ping(indexping)=MX.m_meanTimeCPU+MX.m_meanTimeFraction/10000;
              indexping=indexping+1;
        end
end

% 
% figure;
% hist(reshape(u(1:160,3000:4000,14:50),[],1),1000);