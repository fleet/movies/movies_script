%% Lance la fragmentation de tous les fichiers contenus dans un repertoire donne 

    %clear all;
    
    chemin_config = '.\config_divise\'; 
    chemin_hac='C:\data\test_parasites\Perou\data_Jeremie\hac\';
    chemin_save='C:\data\test_parasites\Perou\data_Jeremie\hac_divise\';
    
    
    if ~exist(chemin_save,'dir')
            mkdir(chemin_save)
    end
    
    filelist = ls([chemin_hac,'*.hac']);  % ensemble des fichiers
    nb_files = size(filelist,1);  % nombre de fichiers
    

    ind_deb=1;
    for numfile = 1:nb_files  % boucle sur l'ensemble des fichiers � traiter
    %for numfile = 1:1
        hacfilename = filelist(numfile,:);
        [ind_deb,time_deb,time_fin]=divise_hac_chunk(chemin_hac,hacfilename,chemin_save,chemin_config,ind_deb);
    end


