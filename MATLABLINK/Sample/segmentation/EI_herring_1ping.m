%% apply flat bottom

chemin_ini='V:\Data_Sondeurs\IBTS09\HAC\herring\ALL\'
fpath='/media/HAC09/IBTS09s/ALLs/'

BottomDepth=22

filelist = ls([chemin_ini,'*.hac']);  % ensemble des fichiers

nb_files = size(filelist,1);  % nombre de fichiers

for numfile = 1:nb_files  % boucle sur l'ensemble des fichiers � traiter

    hacfilename = filelist(numfile,:);

    FileName = [chemin_ini,hacfilename];

    SampleHacFlatBottom(FileName,BottomDepth)

end;

%% EI lay, ESU length = 1 ping

    %chemin_hac='V:\Data_Sondeurs\IBTS09\HAC\herring\ALL\FlatBottom\';
    %chemin_save='V:\Data_Sondeurs\IBTS09\HAC\herring\ALL\FlatBottom\lay_1p\';
    %chemin_config = 'V:\Data_Sondeurs\config M3D\IBTS09_EIlay_1ping\';

    chemin_ini='Y:\IBTS09\HAC\FlatBottom\test\';
    chemin_save='Y:\IBTS09\proc\lay1p\';
    chemin_config='Y:\IBTS09\ini\IBTS09_EIlay_1ping\';

    if ~exist(chemin_save,'dir')
           mkdir(chemin_save)
    end

    %EIlay

    taille_bloc = 2000;

    %clear functions;

    SampleEchoIntegration(chemin_ini,chemin_save,chemin_config,taille_bloc);

    clear all
