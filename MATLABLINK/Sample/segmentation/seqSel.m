%x=Sa_surfME70_mxsn;

function seqSel(x)	
	N=size(x,1);
	sn=1;
	seqn=repmat(0,N,1);
    if isnan(x(1))
        seqn(1)=0
    else
        seqn(1)=sn
    end;    
	for nesu =2:N
		if (~isnan(x(nesu)))
			if (isnan(x(nesu-1)))
                sn=sn+1
            end;    
			seqn(nesu)=sn
        end;
    end;
	seqn;
end;
