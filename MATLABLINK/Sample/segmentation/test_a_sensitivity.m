%a_range=[1:1:10];

function [Nseq_db] = test_a_sensitivity(a_range,t,tso)

    N=size(a_range,2);
    Nseq_db=[];
    
for a_nb=1:N
    
    a=a_range(:,a_nb)
    
    %if times are not in increasing order, re-order times
    [to,IX] = sort(t);
    %mean time difference between successive esus
    dt=diff(to);
    sumdt=quantile(dt,[.025 .25 .50 .75 .975]);
    
    dts=diff(tso);
    plot(dts)
    
    dts_sup=logical([1 dts>(sumdt(5)*a)]);
    tabulate(dts_sup);    
    ts_sup=[0 tso(dts_sup) max(tso)+1];
    
    %time threshold (in seconds) for sequences breaks
    sumdt(5)*a*3600*24;
    
    N1=size(ts_sup,2);
    ns_lab= num2str((1:(N1-1))');
    size(ts_sup);
    size(ns_lab);
    
    %sequence labels
    nsi= ordinal(tso,ns_lab,[],ts_sup);
    
    plot(tso,nsi)
    datetick('x',15)
    ylabel('Sequence ID')
    xlabel('Time')
    
    tnsi=tabulate(nsi);
    mnsi=mean(cell2mat(tnsi(:,2,:)));
    Nseqi=size(unique(nsi),2);
    
    Nseq_db(a_nb,1)=a;
    Nseq_db(a_nb,2)=Nseqi;
    Nseq_db(a_nb,3)=mnsi;

end;

plot(Nseq_db(:,1),Nseq_db(:,2))
xlabel('a')
ylabel('Number of sequences')

end






           
        