    %--------------------
	%function for school selection sensivity test
	%--------------------

	%low pass filter: selection of ESUs based on:
	%1. Sa series smoothed with a moving average of bandwidth bw
	%2. a Sa threshold thr applied on the smoothed series
      
    function [Nesus_db] = xsmooth_sensitivity(bw_range,thr_range,t,Sa_surfME70_mx)
    %-----------------------------------
     
    counter=1;
    
    Nbw=size(bw_range,2);
    Nthr=size(thr_range,2);
    
    Nesus_db=zeros(Nbw*Nthr, 4);
    
    for bwii=1:Nbw
        
    bwi=bw_range(bwii);
        
    Sa_surfME70_smxi = filter(ones(1,bwi)/bwi,1,Sa_surfME70_mx);
    
    %select pings with smoothed Sa higher than 'thr' 
	%---------------
        for thrii=1:Nthr
            thri=thr_range(thrii);
            shoali=(Sa_surfME70_smxi>=thri);
            tshoali=tabulate(shoali);
            rSai=sum(Sa_surfME70_mx(shoali))/sum(Sa_surfME70_mx);
            Nesus_db(counter,1)=bwi;
            Nesus_db(counter,2)=thri;
            if size(tshoali,1)>1
                Nesus_db(counter,3)=cell2mat(tshoali(2,3))/100;
            else
                Nesus_db(counter,3)=0;
            end;    
            Nesus_db(counter,4)=rSai;
            counter=counter+1
        end;    
    end;
   
    
 
 
    
    
    
   
   
 
 