clear all
close all


% <o>< ><o> <o>< ><o> <o><    Configurations    ><o> <o>< ><o> <o>< ><o> %

% Cette partie du code regroupe toute les variables du programme � d�finir
% et � v�rifier avant l'�x�cution.

%%%%%%%%%%%%%%%%%%%%%%% Mouvement bateau %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
chemin = 'C:\data\OASIS';

nbX=1;
nbY=400;

VBat=10;               %vitesse du bateau en noeud
D=150;                 %distance parcouru par le navire


RollAmplitude=0;
RollPeriod=0;
PitchAmplitude=0;
PitchPeriod=0;
RollInit=0;
PitchInit=0;
FechCentraleNav=10;
HeaveAmplitude=0;
HeavePeriod=0;
HeaveInit=0;
DepthAttitude=0;

%Latitude initial
latDeg=48 ;  %degr�s
latMin=0 ;   %minutes
latSec=0 ;   %seconde
latO=0 ;

%longitude initial
lonDeg=3 ; %degr�s
lonMin=0 ;  %minutes
lonSec=0 ;  %seconde
lonO=0 ;

%position initial des sondeurs
L=0; %position sur x
l=0; %position sur y
H=7;  %position sur z

%cap du bateau
heading=0; %en degr�s

%%%%%%%%%%%%%%%%%%%%%%% Fonf marin %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%simulation du fond marrin
AvecSansFond =1;   % 0: sans, 1: avec


%%%%%%%%%%%%%%%%%%%%%%% Bancs de poissons et biomasse %%%%%%%%%%%%%%%%%%%%%


%esp�ces de poissons : joue sur le TS
Espece=0;    % 0:sardine  1:maquereau 2: bulle

%biomasse totale du sc�nation et taille moyenne des poissons
nbpoisson=100000;                  %nombre de poissons

taillecible=20;                %taille moyenne des poissons en cm
tailleciblesd=0.05;

%�cartement entre les bancs
Xespacebanc =0;          %espace entre banc sur X (bord � bord)
Yespacebanc =0;         %espace entre banc sur Y (bord � bord)
Zespacebanc =0;          %espace entre banc sur Z (bord � bord)

%pourcentage de variation de position des poissons autour du maillage
%r�gulier
varEspac=0.2;

ProfondeurCentre=100;   % profondeur du centre des bancs
PositionCentreY=0;      %0:le navire est centr� sur le banc du centre

%%%%%%%%%%%%%%%%%%%%%%%% Propri�t�s des sondeurs %%%%%%%%%%%%%%%%%%%%%%%%%%

TetaMono=7;
%ouverture max � -3dB pour le monofaisceau (ici on prend 10�)
%Selon nos fr�quences teta=10�,7�,7�,7�,7� on prend alors l'ouverture max
%quelque soit nos fr�quences.

TetaMultiext=6;  %ouverture � -3dB pour multifaisceau
TetaMulticentre=3;

depointageMax=42; %d�pointage maximum en multifaisceau

Rmin =5;  %range minimum

%dimension dans les 3 dimensions du volume fixe
D1_couche=10;      %dimension en X
D2_couche=10;     %dimension en Y
D3_couche=5;       %dimension en Z

%forme bancs
forme=0;    % 0 ellisoide, 1 parall�lip�dique

% orientation des bancs autour des trois axes en "degr�s*pi/180=>radians"
roll=0*pi/180;      % rotation autour de X
pitch=0*pi/180;     % rotation autour de Y
yaw=0*pi/180;       % rotation autour de Z

%%%%%%position des bancs%%%%%
offsetX=60;
%l'offset en X sert � d�caler l'ensemble des bancs de poissons dans la
%direction X. Si les premier bancs sont hortogonalement sous la position
%initiale du navire ils ne seront pas vu.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% <o>< ><o> <o>< ><o> <o>< ><o> <o>< ><o> <o>< ><o> <o>< ><o> <o>< ><o> %

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Programme %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for n=1:1

    %nombre de bancs dans les 3 dimensions:
    BancsX=1;    %nombre de bancs en X
    BancsY=n;   %nombre de bancs en Y
    BancsZ=1;    %nombre de bancs en Z

    %nombre de bancs totals
    nbbanc =BancsX*BancsY*BancsZ;

    %vitesse du bateau en m/s
    vbat=(VBat*1.852)/3.6;

    % Calcul du poids (en gramme) des poisson en fonction de sa taille (en cm)
%     poids = calcul(taillecible);     %appel � la function calcul (fichier  calcul.m)

    %pour cr�er cette fonction j'ai simplement interpol� le tableau de
    %correspondance taille/poids des sardines et calcul� l'�quation de sa
    %courbe de tendance
    %fonction calcul.m
    %   function [poids] = calcul(taille)
    %   poids = 0.438*taille^2 - 8.1566*taille + 47.134;

    %nombre total de poissons
    % Masse en tonnes * 1000000 => grammes
    % poids en grammes
%     nbpoisson=Masse*1000000/poids;

    %nombre de poissons par banc
    nbpb=nbpoisson/nbbanc ;

    % Calcul de la densit� des bancs et de l'espacement entre les poissons �
    % l'int�rieur des bancs. Le calcul est diff�rent qu'il y ai 1 banc ou
    % plusieurs car les dimensions des bancs changent.

        D1=D1_couche;
        D2=D1;
        D3=D3_couche;
        if forme==0
            V=(4/3)*pi*(D1*D2*D3)/8;
        elseif forme==1
            V=D1*D2*D3;
        end
        densite=1/(V/nbpb);
        Xp=(1/densite)^(1/3);
  

    % Espace entre les centres des bancs dans toute les dimensions
    Xespacecentre=Xespacebanc+D1 ;    %espaces entre centres des bancs en X
    Yespacecentre=Yespacebanc+D2;     %espaces entre centres des bancs en Y
    Zespacecentre=Zespacebanc+D3;     %espaces entre centres des bancs en Z

    % Calcul de la position du navire par rapport aux bancs
    if PositionCentreY == 0
        PositionCentreY=0;
        % dans ce cas le navire est centr� sur le banc du centre
    elseif Yespacecentre == 1
        PositionCentreY=Yespacecentre/2;
        % dans ce cas le navire est centr� entre deux bancs
    end

    %calcul du TS en fonction de l'esp�ce choisie:
    if Espece==0
        TS = 20*log10(taillecible)-72.6;
    elseif Espece==1
        TS = 20*log10(taillecible) - 84.9;
   elseif Espece==2
        TS = -55;
    end

    % Calcul de l'ouverture max des sondeurs en fonction des TS et de la
    % densit�
    Sv=TS+10*log10(densite);
%     TetaVraiMono=0.44*(TetaMono)*(Sv+60)^0.45;
    AngleMono=TetaMono ;
%     TetaVraiMulti=0.44*(TetaMulti)*(Sv+60)^0.45;
    AngleMulti=2*(depointageMax+TetaMultiext/2);
    AngleMultiCentre=TetaMulticentre;


    %d�calage en Y et en Z afin de placer les bancs par rapport au banc du
    %centre et non par rapport au premier banc trac� par les programme (celui
    %le plus proche de l'origine (0,0,0))
    offsetY=-fix(BancsY/2)*Yespacecentre;   %d�calage en Y
    offsetZ=-fix(BancsZ/2)*Zespacecentre;   %d�calage en Z



    %Calcul de la matrice qui contient toutes les positions des bancs avec la
    %fonction meshgrid. Pour chaque direction je cr�es un vecteur d�fini par
    %ses coordonn�es initiales, le nombre de points et ses coordon�es final
    [X,Y,Z] = meshgrid(...
        offsetX+(D1/2):Xespacecentre:(BancsX*D1)+(BancsX-1)*Xespacebanc+offsetX,...
        offsetY+PositionCentreY:Yespacecentre:(BancsY-1)*Yespacecentre+offsetY+PositionCentreY,...
        offsetZ+ProfondeurCentre:Zespacecentre:(BancsZ-1)*Zespacecentre+offsetZ+ProfondeurCentre);

    %matrice de rotation pour orienter les bancs selon les angles que l'on
    %d�sire
    R=makehgtform('xrotate',roll,'yrotate',pitch,'zrotate',yaw);

    %Boucle pour calculer les coordonn�es du centre de chaque banc
    M=inf(nbX*nbY,nbbanc,3);
    p=1;
    for kx = 1:nbX
        for ky = 1:nbY
        i= nbbanc;
        while (i>0)
            PositionBancX=X(1);
            PositionBancY=Y(1)+(2*ky-nbY)*(tan(((AngleMulti*pi)/180)/2)*ProfondeurCentre/nbY);
            PositionBancZ=Z(1);
%               PositionBancX=X(1) + (2*kx-nbX)*(2/nbX);
%               PositionBancY=Y(1) + (2*ky-nbY)*(0.5/nbY);
%               PositionBancZ=Z(1);
 
                M(p,i,:)=R(1:3,1:3)*[PositionBancX PositionBancY PositionBancZ]' ;
                [x(:,:,p,i), y(:,:,p,i), z(:,:,p,i)] = ellipsoid(M(p,i,1),M(p,i,2),M(p,i,3),D1/2,D2/2,D3/2,20);
                i=i-1;
        end
        p=p+1;
        end
    end
    %recherche du banc le plus profond afin de calculer la profondeur
    %du fond
    ProfondeurFond = max(max(M(:,:,3)))+2*D3;

    % Calcul de la demie base du c�ne d'insonification et du range max pour
    % chaque sondeur
    kMono=tan(((AngleMono*pi)/180)/2)*ProfondeurFond;
    kMulti=tan(((AngleMulti*pi)/180)/2)*ProfondeurFond;
    kMultiCentre=tan(((AngleMultiCentre*pi)/180)/2)*ProfondeurFond;
    RmaxMono=sqrt(ProfondeurFond^2+kMono^2);
    RmaxMulti=sqrt(ProfondeurFond^2+kMulti^2);


    % table de r�currence des pings
    % Pronfondeur  temps interPingds
    % 20-30             0,35
    % 30-50             0,4
    % 50-70             0,45
    % 70-90             0,5
    % 90-115            0,6
    % 115-140           0,7
    % 140-165           0,8
    % 165-190           0,9
    % 190-215            1
    % 215-240           1,1
    % 240-250           1,2


    if ProfondeurCentre>=20 && ProfondeurCentre<30
        InterPing=0.35;
    elseif ProfondeurCentre>=30 && ProfondeurCentre<50
        InterPing=0.4;
    elseif ProfondeurCentre>=50 && ProfondeurCentre<70
        InterPing=0.45;
    elseif ProfondeurCentre>=70 && ProfondeurCentre<90
        InterPing=0.5;
    elseif ProfondeurCentre>=90 && ProfondeurCentre<115
        InterPing=0.6;
    elseif ProfondeurCentre>=115 && ProfondeurCentre<140
        InterPing=0.7;
    elseif ProfondeurCentre>=140 && ProfondeurCentre<165
        InterPing=0.8;
    elseif ProfondeurCentre>=165 && ProfondeurCentre<190
        InterPing=0.9;
    elseif ProfondeurCentre>=190 && ProfondeurCentre<215
        InterPing=1;
    elseif ProfondeurCentre>=215 && ProfondeurCentre<240
        InterPing=1.1;
    elseif ProfondeurCentre>=240 && ProfondeurCentre<250
        InterPing=1.2;
    end

    % Calculde la distance entre deux pings et du nombre de pings
    dinterPing=vbat*InterPing; %distance entre deux pings en m�tre
    NbPing=round(D/dinterPing);%nombre total de ping

    dirScene=sprintf('scene_%g_%g_%03g_%g_%g_%g',nbpoisson,nbbanc, round(ProfondeurCentre), D1, nbX*nbY, roll*180/pi);
    mkdir([chemin,'\', dirScene]);

    %%%%%%%%%%%%%%%Ecriture du fichier bancsfin_mono.xml%%%%%%%%%%%%%%%%%

    %Nom du fichier XML qui va �tre g�n�r�
    for k=1:nbX*nbY

        if Espece==0
            Oasisfilenamemono = sprintf('scene_mono_sardine_%g_%g_%03g_%g_%03g_%g.oasis',nbpoisson,nbbanc, round(ProfondeurCentre), D1, k, roll*180/pi);
        elseif Espece==1
            Oasisfilenamemono = sprintf('scene_mono_maquereau_%g_%g_%03g_%g_%03g_%g.oasis',nbpoisson,nbbanc, round(ProfondeurCentre), D1, k, roll*180/pi);
        elseif Espece==2
            Oasisfilenamemono = sprintf('scene_mono_bulle_%g_%g_%03g_%g_%03g_%g.oasis',nbpoisson,nbbanc, round(ProfondeurCentre), D1, k, roll*180/pi);
        end

        fid = fopen([chemin,'\', dirScene  '\' Oasisfilenamemono], 'w+');
        fprintf(fid,'<?xml version="1.0" encoding="UTF-8" standalone="no" ?>');
        fprintf(fid,'\n<APPLICATION APPNAME="OASIS" APPVERSION_MAJ="3" APPVERSION_MIN="01">');
        fprintf(fid,'\n <Configuration TYPE="Scenario">');

        %param�tres concernant l'environnement:
        fprintf(fid,'\n  <Configuration TYPE="ConfigEnv">');
        fprintf(fid,'\n   <PARAMETER NAME="SeaTemperature" VALUE="10" />');
        fprintf(fid,'\n   <PARAMETER NAME="Salinity" VALUE="35" />');
        fprintf(fid,'\n   <PARAMETER NAME="PH" VALUE="8" />');
        fprintf(fid,'\n   <PARAMETER NAME="WindSpeed" VALUE="15" />');
        fprintf(fid,'\n   <PARAMETER NAME="SelfNoiseLevel1Hz" VALUE="173" />');
        fprintf(fid,'\n   <PARAMETER NAME="PropellerPosition" VALUE-X="-40" VALUE-Y="0" VALUE-Z="0" />');
        fprintf(fid,'\n  </Configuration>');

        %param�tres concernant la sc�ne:
        fprintf(fid,'\n  <Configuration TYPE="ConfigScene">');
        fprintf(fid,'\n   <PARAMETER NAME="HasSeaFloor" VALUE="%d" />',AvecSansFond);
        fprintf(fid,'\n   <PARAMETER NAME="SeaFloorName" VALUE="Seafloor Name" />');
        fprintf(fid,'\n   <PARAMETER NAME="H" VALUE="%d" />',ProfondeurFond);
        fprintf(fid,'\n   <PARAMETER NAME="PhiBottom" VALUE="0" />');
        fprintf(fid,'\n   <PARAMETER NAME="PsiBottom" VALUE="0" />');
        fprintf(fid,'\n   <PARAMETER NAME="m10loga1" VALUE="-5" />');
        fprintf(fid,'\n   <PARAMETER NAME="a2" VALUE="180" />');
        fprintf(fid,'\n   <PARAMETER NAME="m10loga3" VALUE="-25" />');
        fprintf(fid,'\n   <PARAMETER NAME="Prof" VALUE="10" />');
        fprintf(fid,'\n   <PARAMETER NAME="BottomAngleSampling" VALUE="10" />');
        fprintf(fid,'\n   <PARAMETER NAME="NivMinIntercor" VALUE="-60" />');
        fprintf(fid,'\n   <PARAMETER NAME="NivMinGabarit" VALUE="-60" />');
        fprintf(fid,'\n   <PARAMETER NAME="Seed" VALUE="1" />');

        %param�tre concernant les bancs:
        fprintf(fid,'\n  <Configuration TYPE="Schools">');

        %gestion des options des figure
        figure(round(ProfondeurCentre));
        set(round(ProfondeurCentre),'Units','Normalized','Position',[0 0 1 1]);
        set(round(ProfondeurCentre),'Name', ['monofaisceau/multifaisceaux, ',num2str(ProfondeurCentre),' m ', num2str(NbPing),'Pings ',num2str(D),'m'], 'NumberTitle','off');
        subplot(2,1,1);
        title(['monofaisceau: ',num2str(round(nbpb)),' poissons par bancs ', num2str(densite),' poisson/m^3'])
        set(gca,'ydir','reverse');
        set(gca,'zdir','reverse');
        axis equal;
        xlabel('x');
        ylabel('y');
        zlabel('z');

        % Coordonn�es des points des plans du c�ne d'insonification
        p1=[L l H];
        p2=[L l+kMono H+ProfondeurFond];
        p3=[L l-kMono H+ProfondeurFond];
        p4=[L+D l H];
        p5=[L+D l+kMono H+ProfondeurFond];
        p6=[L+D l-kMono H+ProfondeurFond];



        % Trac� des plans du c�ne d'insonification
        S7=[p1;p4;p2];
        patch(S7(:,1),S7(:,2),S7(:,3),'b');
        S8=[p2;p4;p5];
        patch(S8(:,1),S8(:,2),S8(:,3),'b');
        S9=[p1;p4;p3];
        patch(S9(:,1),S9(:,2),S9(:,3),'b');
        S10=[p3;p4;p6];
        patch(S10(:,1),S10(:,2),S10(:,3),'b');

        % Calcul des �quations des plans du c�ne d'insonification:
        dd=2;                    %on fixe le param�tre d (diff�rent de 0)

        P1=[p1;p4;p2];           %matrice 3*3 contenant les coefficients
        P2=[p1;p4;p3];



        matd=[-dd;-dd;-dd];             %vecteur colonne 3*1 rempli par le coeff d


        EE1=inv(P1)*matd;
        EE2=inv(P2)*matd;
        ee11=EE1(1);
        ee12=EE1(2);
        ee13=EE1(3);
        ee21=EE2(1);
        ee22=EE2(2);
        ee23=EE2(3);



        NbBancsVusMono=0;


        for i = 1:nbbanc;

            eqq1=ee11*M(k,i,1)+ee12*M(k,i,2)+ee13*M(k,i,3)+dd;
            eqq2=ee21*M(k,i,1)+ee22*M(k,i,2)+ee23*M(k,i,3)+dd;


%             if (D1/2)>H*tan((AngleMono*pi/180)/2),
%                 if  (eqq1>0 && eqq2>0) ,
%                     NbBancsVusMono=NbBancsVusMono+1;
%                     hh=surface(x(:,:,k,i),y(:,:,k,i),z(:,:,k,i),'FaceColor','red','edgecolor','none');
%                     tt = hgtransform;
%                     set(hh,'Parent',tt)
%                     Rx = makehgtform('xrotate',roll);
%                     Ry = makehgtform('yrotate',pitch);
%                     Rz = makehgtform('zrotate',yaw);
%                     Tx1 = makehgtform('translate',[-M(k,i,1) -M(k,i,2) -M(k,i,3)]);
%                     Tx2 = makehgtform('translate',[M(k,i,1) M(k,i,2) M(k,i,3)]);
%                     set(tt,'Matrix',Tx2*Rx*Ry*Rz*Tx1)
%                     hold on;
% 
%                     fprintf(fid,'\n  <School NAME="a%d">',i);
%                     fprintf(fid,'\n   <PARAMETER NAME="TSMode" VALUE="0" />');
%                     fprintf(fid,'\n   <PARAMETER NAME="TS" VALUE="%f" /> ',TS);
%                     fprintf(fid,'\n   <PARAMETER NAME="FreqList" VALUE="18000;38000;70000;120000;200000;" /> ');
%                     fprintf(fid,'\n   <PARAMETER NAME="TSList" VALUE="%f" />',TS);
%                     fprintf(fid,'\n   <PARAMETER NAME="Position" VALUE-X="%f" VALUE-Y="%f" VALUE-Z="%f"/>',M(k,i,1),M(k,i,2),M(k,i,3));
%                     fprintf(fid,'\n   <PARAMETER NAME="Orientation" VALUE-X="%f" VALUE-Y="%f" VALUE-Z="%f"/>',roll*180/pi,pitch*180/pi,yaw*180/pi);
%                     fprintf(fid,'\n   <PARAMETER NAME="Dimension" VALUE-X="%f" VALUE-Y="%f" VALUE-Z="%f"/>',D1,D2,D3);
%                     fprintf(fid,'\n   <PARAMETER NAME="Spacing" VALUE-X="%f" VALUE-Y="%f" VALUE-Z="%f"/>',Xp,Xp,Xp);
%                     fprintf(fid,'\n   <PARAMETER NAME="Deviation" VALUE-X="%f" VALUE-Y="%f" VALUE-Z="%f"/>',varEspac*Xp,varEspac*Xp,varEspac*Xp);
%                     fprintf(fid,'\n   <PARAMETER NAME="Seed" VALUE="1"/>');
%                     fprintf(fid,'\n   <PARAMETER NAME="SchoolType" VALUE="1"/>');
%                     fprintf(fid,'\n  </School>');
% 
%                 else
%                     hh=surface(x(:,:,k,i),y(:,:,k,i),z(:,:,k,i),'FaceColor','green','edgecolor','none');
%                     tt = hgtransform;
%                     set(hh,'Parent',tt)
%                     Rx = makehgtform('xrotate',roll);
%                     Ry = makehgtform('yrotate',pitch);
%                     Rz = makehgtform('zrotate',yaw);
%                     Tx1 = makehgtform('translate',[-M(k,i,1) -M(k,i,2) -M(k,i,3)]);
%                     Tx2 = makehgtform('translate',[M(k,i,1) M(k,i,2) M(k,i,3)]);
%                     set(tt,'Matrix',Tx2*Rx*Ry*Rz*Tx1)
%                     hold on;
% 
%                     %� commenter si on ne veut simuler que les bancs situ�
%                     %dans le volume insonifi�
%                     fprintf(fid,'\n  <School NAME="a%d">',i);
%                     fprintf(fid,'\n   <PARAMETER NAME="TSMode" VALUE="0" />');
%                     fprintf(fid,'\n   <PARAMETER NAME="TS" VALUE="%f" /> ',TS);
%                     fprintf(fid,'\n   <PARAMETER NAME="FreqList" VALUE="18000;38000;70000;120000;200000;" /> ');
%                     fprintf(fid,'\n   <PARAMETER NAME="TSList" VALUE="%f" />',TS);
%                     fprintf(fid,'\n   <PARAMETER NAME="Position" VALUE-X="%f" VALUE-Y="%f" VALUE-Z="%f"/>',M(k,i,1),M(k,i,2),M(k,i,3));
%                     fprintf(fid,'\n   <PARAMETER NAME="Orientation" VALUE-X="%f" VALUE-Y="%f" VALUE-Z="%f"/>',roll*180/pi,pitch*180/pi,yaw*180/pi);
%                     fprintf(fid,'\n   <PARAMETER NAME="Dimension" VALUE-X="%f" VALUE-Y="%f" VALUE-Z="%f"/>',D1,D2,D3);
%                     fprintf(fid,'\n   <PARAMETER NAME="Spacing" VALUE-X="%f" VALUE-Y="%f" VALUE-Z="%f"/>',Xp,Xp,Xp);
%                     fprintf(fid,'\n   <PARAMETER NAME="Deviation" VALUE-X="%f" VALUE-Y="%f" VALUE-Z="%f"/>',varEspac*Xp,varEspac*Xp,varEspac*Xp);
%                     fprintf(fid,'\n   <PARAMETER NAME="Seed" VALUE="1"/>');
%                     fprintf(fid,'\n   <PARAMETER NAME="SchoolType" VALUE="1"/>');
%                     fprintf(fid,'\n  </School>');
% 
% 
%                 end
% 
%             elseif (D1/2)<abs(H*tan((AngleMono*pi/180)/2)),
                if  (eqq1<0 && eqq2<0) ,
                    NbBancsVusMono=NbBancsVusMono+1;

                    hh=surface(x(:,:,k,i),y(:,:,k,i),z(:,:,k,i),'FaceColor','red','edgecolor','none');
                    tt = hgtransform;
                    set(hh,'Parent',tt)
                    Rx = makehgtform('xrotate',roll);
                    Ry = makehgtform('yrotate',pitch);
                    Rz = makehgtform('zrotate',yaw);
                    Tx1 = makehgtform('translate',[-M(k,i,1) -M(k,i,2) -M(k,i,3)]);
                    Tx2 = makehgtform('translate',[M(k,i,1) M(k,i,2) M(k,i,3)]);
                    set(tt,'Matrix',Tx2*Rx*Ry*Rz*Tx1)
                    hold on;

                  fprintf(fid,'\n  <School NAME="a%d">',i);
                    fprintf(fid,'\n   <PARAMETER NAME="TSMode" VALUE="3" />');
                    fprintf(fid,'\n   <PARAMETER NAME="TS" VALUE="%f" /> ',TS);
                    fprintf(fid,'\n   <PARAMETER NAME="FreqList" VALUE="1;" /> ');
                    fprintf(fid,'\n   <PARAMETER NAME="TSList" VALUE="%f" />',TS);
                    fprintf(fid,'\n   <PARAMETER NAME="Position" VALUE-X="%f" VALUE-Y="%f" VALUE-Z="%f"/>',M(k,i,1),M(k,i,2),M(k,i,3));
                    fprintf(fid,'\n   <PARAMETER NAME="Orientation" VALUE-X="%f" VALUE-Y="%f" VALUE-Z="%f"/>',roll*180/pi,pitch*180/pi,yaw*180/pi);
                    fprintf(fid,'\n   <PARAMETER NAME="Dimension" VALUE-X="%f" VALUE-Y="%f" VALUE-Z="%f"/>',D1,D2,D3);
                    fprintf(fid,'\n   <PARAMETER NAME="Spacing" VALUE-X="%f" VALUE-Y="%f" VALUE-Z="%f"/>',Xp,Xp,Xp);
                    fprintf(fid,'\n   <PARAMETER NAME="Deviation" VALUE-X="0.1" VALUE-Y="0.1" VALUE-Z="0.1"/>');
                    fprintf(fid,'\n   <PARAMETER NAME="Seed" VALUE="1"/>');
                    fprintf(fid,'\n   <PARAMETER NAME="OrientationDeviation" VALUE-X="2" VALUE-Y="2" VALUE-Z="2"/>');
                    fprintf(fid,'\n   <PARAMETER NAME="OrientationSeed" VALUE="1"/>');
                    fprintf(fid,'\n   <PARAMETER NAME="FishLength" VALUE="%f"/>',taillecible/100);
                    fprintf(fid,'\n   <PARAMETER NAME="FishLengthSeed" VALUE="1"/>');
                    fprintf(fid,'\n   <PARAMETER NAME="FishLengthDeviation" VALUE="%f"/>',tailleciblesd/100);
                     fprintf(fid,'\n   <PARAMETER NAME="SchoolType" VALUE="%d"/>',forme);
                    fprintf(fid,'\n   <PARAMETER NAME="DiffuserModelIndex" VALUE="7"/>');
                    fprintf(fid,'\n  </School>');
                    
                else
                    hh=surface(x(:,:,k,i),y(:,:,k,i),z(:,:,k,i),'FaceColor','green','edgecolor','none');
                    tt = hgtransform;
                    set(hh,'Parent',tt)
                    Rx = makehgtform('xrotate',roll);
                    Ry = makehgtform('yrotate',pitch);
                    Rz = makehgtform('zrotate',yaw);
                    Tx1 = makehgtform('translate',[-M(k,i,1) -M(k,i,2) -M(k,i,3)]);
                    Tx2 = makehgtform('translate',[M(k,i,1) M(k,i,2) M(k,i,3)]);
                    set(tt,'Matrix',Tx2*Rx*Ry*Rz*Tx1)
                    hold on;
                    
                    %� commenter si on ne veut simuler que les bancs situ�
                    %dans le volume insonifi�
                    fprintf(fid,'\n  <School NAME="a%d">',i);
                    fprintf(fid,'\n   <PARAMETER NAME="TSMode" VALUE="3" />');
                    fprintf(fid,'\n   <PARAMETER NAME="TS" VALUE="%f" /> ',TS);
                    fprintf(fid,'\n   <PARAMETER NAME="FreqList" VALUE="1;" /> ');
                    fprintf(fid,'\n   <PARAMETER NAME="TSList" VALUE="%f" />',TS);
                    fprintf(fid,'\n   <PARAMETER NAME="Position" VALUE-X="%f" VALUE-Y="%f" VALUE-Z="%f"/>',M(k,i,1),M(k,i,2),M(k,i,3));
                    fprintf(fid,'\n   <PARAMETER NAME="Orientation" VALUE-X="%f" VALUE-Y="%f" VALUE-Z="%f"/>',roll*180/pi,pitch*180/pi,yaw*180/pi);
                    fprintf(fid,'\n   <PARAMETER NAME="Dimension" VALUE-X="%f" VALUE-Y="%f" VALUE-Z="%f"/>',D1,D2,D3);
                    fprintf(fid,'\n   <PARAMETER NAME="Spacing" VALUE-X="%f" VALUE-Y="%f" VALUE-Z="%f"/>',Xp,Xp,Xp);
                    fprintf(fid,'\n   <PARAMETER NAME="Deviation" VALUE-X="0.1" VALUE-Y="0.1" VALUE-Z="0.1"/>');
                    fprintf(fid,'\n   <PARAMETER NAME="Seed" VALUE="1"/>');
                    fprintf(fid,'\n   <PARAMETER NAME="OrientationDeviation" VALUE-X="2" VALUE-Y="2" VALUE-Z="2"/>');
                    fprintf(fid,'\n   <PARAMETER NAME="OrientationSeed" VALUE="1"/>');
                 fprintf(fid,'\n   <PARAMETER NAME="FishLength" VALUE="%f"/>',taillecible/100);
                    fprintf(fid,'\n   <PARAMETER NAME="FishLengthSeed" VALUE="1"/>');
                    fprintf(fid,'\n   <PARAMETER NAME="FishLengthDeviation" VALUE="%f"/>',tailleciblesd/100);
                    fprintf(fid,'\n   <PARAMETER NAME="SchoolType" VALUE="%d"/>',forme);
                    fprintf(fid,'\n   <PARAMETER NAME="DiffuserModelIndex" VALUE="7"/>');
                    fprintf(fid,'\n  </School>');
                    
                    
                end

%             end
        end

        NbBancsVusMono;
        NbPoissonVusMono(k)=NbBancsVusMono*nbpb;
    


        fprintf(fid,'\n  </Configuration>');
        
        fprintf(fid,'\n   <Configuration TYPE="DiffuserModels">');
        fprintf(fid,'\n   <DiffuserModel NAME="TFS Holliday">');
        fprintf(fid,'\n   <PARAMETER NAME="g" VALUE="1.12"/>');
        fprintf(fid,'\n   <PARAMETER NAME="h" VALUE="1.09"/>');
        fprintf(fid,'\n   </DiffuserModel>');
        fprintf(fid,'\n   <DiffuserModel NAME="Fluid Sphere Stanton">');
        fprintf(fid,'\n   <PARAMETER NAME="g" VALUE="1.043"/>');
        fprintf(fid,'\n   <PARAMETER NAME="h" VALUE="1.05"/>');
        fprintf(fid,'\n   </DiffuserModel>');
        fprintf(fid,'\n   <DiffuserModel NAME="Fluid Prolate Spheroid Stanton">');
        fprintf(fid,'\n   <PARAMETER NAME="g" VALUE="1.043"/>');
        fprintf(fid,'\n   <PARAMETER NAME="h" VALUE="1.05"/>');
        fprintf(fid,'\n   <PARAMETER NAME="La" VALUE="5"/>');
        fprintf(fid,'\n   </DiffuserModel>');
        fprintf(fid,'\n   <DiffuserModel NAME="Fluid Bent Cylinder 93">');
        fprintf(fid,'\n   <PARAMETER NAME="g" VALUE="1.0357"/>');
        fprintf(fid,'\n   <PARAMETER NAME="h" VALUE="1.0279"/>');
        fprintf(fid,'\n   <PARAMETER NAME="La" VALUE="16"/>');
        fprintf(fid,'\n   <PARAMETER NAME="phoL" VALUE="3"/>');
        fprintf(fid,'\n   <PARAMETER NAME="s" VALUE="0.06"/>');
        fprintf(fid,'\n   <PARAMETER NAME="Incident Angle" VALUE="20"/>');
        fprintf(fid,'\n   <PARAMETER NAME="Angle Std Dev" VALUE="20"/>');
        fprintf(fid,'\n   </DiffuserModel>');
        fprintf(fid,'\n   <DiffuserModel NAME="Fluid Bent Cylinder 94">');
        fprintf(fid,'\n   <PARAMETER NAME="s" VALUE="0.06"/>');
        fprintf(fid,'\n   <PARAMETER NAME="R" VALUE="0.058"/>');
        fprintf(fid,'\n   <PARAMETER NAME="La" VALUE="16"/>');
        fprintf(fid,'\n   </DiffuserModel>');
        fprintf(fid,'\n   <DiffuserModel NAME="Elastic Sphere">');
        fprintf(fid,'\n   <PARAMETER NAME="R" VALUE="0.5"/>');
        fprintf(fid,'\n   </DiffuserModel>');
        fprintf(fid,'\n   <DiffuserModel NAME="Gaseous Prolate Spheroid">');
        fprintf(fid,'\n   <PARAMETER NAME="gsurf" VALUE="0.0012"/>');
        fprintf(fid,'\n   <PARAMETER NAME="h" VALUE="0.22"/>');
        fprintf(fid,'\n   <PARAMETER NAME="La" VALUE="5"/>');
        fprintf(fid,'\n   </DiffuserModel>');
        fprintf(fid,'\n   <DiffuserModel NAME="Gaseous Sphere">');
        fprintf(fid,'\n   <PARAMETER NAME="gsurf" VALUE="0.0012"/>');
        fprintf(fid,'\n   <PARAMETER NAME="h" VALUE="0.22"/>');
        fprintf(fid,'\n   </DiffuserModel>');
        fprintf(fid,'\n   </Configuration>');
      
        fprintf(fid,'\n   </Configuration>');

        fprintf(fid,'\n  <Configuration TYPE="ConfigSignals">');
        fprintf(fid,'\n   <PARAMETER NAME="PulseLength" VALUE="0.001" />');
        fprintf(fid,'\n   <PARAMETER NAME="ModulationType" VALUE="1" /> ');
        fprintf(fid,'\n   <PARAMETER NAME="BetaFactor" VALUE="-20" /> ');
        fprintf(fid,'\n   <PARAMETER NAME="Fdemod" VALUE="109000" /> ');
        fprintf(fid,'\n   <PARAMETER NAME="Fech" VALUE="10000" /> ');
        fprintf(fid,'\n   <PARAMETER NAME="TypeSelection" VALUE="0" />');
        fprintf(fid,'\n   <PARAMETER NAME="Tsig" VALUE="0.003" /> ');
        fprintf(fid,'\n   <PARAMETER NAME="EchoesWindowLength" VALUE="0.005" />');
        fprintf(fid,'\n  </Configuration>');

        fprintf(fid,'\n  <Configuration TYPE="ConfigSounder">');
        fprintf(fid,'\n   <PARAMETER NAME="Nping" VALUE="%d" />',NbPing);
        fprintf(fid,'\n   <PARAMETER NAME="Trec" VALUE="%d" />',InterPing);
        fprintf(fid,'\n   <PARAMETER NAME="Rmin" VALUE="%d" />',Rmin);
        fprintf(fid,'\n   <PARAMETER NAME="Rmax" VALUE="%d" />',RmaxMono);
        fprintf(fid,'\n   <PARAMETER NAME="SL" VALUE="230" />');
        fprintf(fid,'\n   <PARAMETER NAME="TVG" VALUE="0" />');
        fprintf(fid,'\n   <PARAMETER NAME="TransducerType" VALUE="1">');
        fprintf(fid,'\n    <Transducer TYPE="Mono">');
        fprintf(fid,'\n     <PARAMETER NAME="TransducerGeometry" VALUE="0" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="Frequency" VALUE="18000" />');
        fprintf(fid,'\n     <PARAMETER NAME="Psi" VALUE="0" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="Phi" VALUE="0" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="Position" VALUE-X="27.7" VALUE-Y="-0.4" VALUE-Z="7.11" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="TypeBeam" VALUE="1" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="BeamPsiFile" VALUE="" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="BeamPhiFile" VALUE="" />');
        fprintf(fid,'\n     <PARAMETER NAME="BeamWidthPhi3dB" VALUE="10" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="BeamWidthPsi3dB" VALUE="10" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="BeamWidth3dB" VALUE="10" />');
        fprintf(fid,'\n     <PARAMETER NAME="NumberAngleSamples" VALUE="361" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="MaxAngleBeam" VALUE="90" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="MinAngleBeam" VALUE="-90" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="BeamAngleSampling" VALUE="0.5" /> ');
        fprintf(fid,'\n    </Transducer>');
        fprintf(fid,'\n    <Transducer TYPE="Mono">');
        fprintf(fid,'\n     <PARAMETER NAME="TransducerGeometry" VALUE="0" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="Frequency" VALUE="38000" />');
        fprintf(fid,'\n     <PARAMETER NAME="Psi" VALUE="0" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="Phi" VALUE="0" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="Position" VALUE-X="27.69" VALUE-Y="0.4" VALUE-Z="7.11" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="TypeBeam" VALUE="1" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="BeamPsiFile" VALUE="" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="BeamPhiFile" VALUE="" />');
        fprintf(fid,'\n     <PARAMETER NAME="BeamWidthPhi3dB" VALUE="7" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="BeamWidthPsi3dB" VALUE="7" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="BeamWidth3dB" VALUE="7" />');
        fprintf(fid,'\n     <PARAMETER NAME="NumberAngleSamples" VALUE="361" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="MaxAngleBeam" VALUE="90" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="MinAngleBeam" VALUE="-90" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="BeamAngleSampling" VALUE="0.5" /> ');
        fprintf(fid,'\n    </Transducer>');
        fprintf(fid,'\n    <Transducer TYPE="Mono">');
        fprintf(fid,'\n     <PARAMETER NAME="TransducerGeometry" VALUE="0" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="Frequency" VALUE="70000" />');
        fprintf(fid,'\n     <PARAMETER NAME="Psi" VALUE="0" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="Phi" VALUE="0" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="Position" VALUE-X="27.02" VALUE-Y="-0.56" VALUE-Z="7.11" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="TypeBeam" VALUE="1" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="BeamPsiFile" VALUE="" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="BeamPhiFile" VALUE="" />');
        fprintf(fid,'\n     <PARAMETER NAME="BeamWidthPhi3dB" VALUE="7" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="BeamWidthPsi3dB" VALUE="7" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="BeamWidth3dB" VALUE="7" />');
        fprintf(fid,'\n     <PARAMETER NAME="NumberAngleSamples" VALUE="361" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="MaxAngleBeam" VALUE="90" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="MinAngleBeam" VALUE="-90" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="BeamAngleSampling" VALUE="0.5" /> ');
        fprintf(fid,'\n    </Transducer>');
        fprintf(fid,'\n    <Transducer TYPE="Mono">');
        fprintf(fid,'\n     <PARAMETER NAME="TransducerGeometry" VALUE="0" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="Frequency" VALUE="120000" />');
        fprintf(fid,'\n     <PARAMETER NAME="Psi" VALUE="0" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="Phi" VALUE="0" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="Position" VALUE-X="26.52" VALUE-Y="0.52" VALUE-Z="7.11" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="TypeBeam" VALUE="1" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="BeamPsiFile" VALUE="" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="BeamPhiFile" VALUE="" />');
        fprintf(fid,'\n     <PARAMETER NAME="BeamWidthPhi3dB" VALUE="7" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="BeamWidthPsi3dB" VALUE="7" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="BeamWidth3dB" VALUE="7" />');
        fprintf(fid,'\n     <PARAMETER NAME="NumberAngleSamples" VALUE="361" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="MaxAngleBeam" VALUE="90" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="MinAngleBeam" VALUE="-90" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="BeamAngleSampling" VALUE="0.5" /> ');
        fprintf(fid,'\n    </Transducer>');
        fprintf(fid,'\n     <Transducer TYPE="Mono">');
        fprintf(fid,'\n     <PARAMETER NAME="TransducerGeometry" VALUE="0" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="Frequency" VALUE="200000" />');
        fprintf(fid,'\n     <PARAMETER NAME="Psi" VALUE="0" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="Phi" VALUE="0" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="Position" VALUE-X="27.07" VALUE-Y="0.49" VALUE-Z="7.11" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="TypeBeam" VALUE="1" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="BeamPsiFile" VALUE="" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="BeamPhiFile" VALUE="" />');
        fprintf(fid,'\n     <PARAMETER NAME="BeamWidthPhi3dB" VALUE="7" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="BeamWidthPsi3dB" VALUE="7" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="BeamWidth3dB" VALUE="7" />');
        fprintf(fid,'\n     <PARAMETER NAME="NumberAngleSamples" VALUE="361" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="MaxAngleBeam" VALUE="90" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="MinAngleBeam" VALUE="-90" /> ');
        fprintf(fid,'\n     <PARAMETER NAME="BeamAngleSampling" VALUE="0.5" /> ');
        fprintf(fid,'\n    </Transducer>');

        fprintf(fid,'\n   </PARAMETER>');
        fprintf(fid,'\n  </Configuration>');

        if Espece ==0
            filenamemono = sprintf('scene_mono_sardine_%g_%g_%03g_%g_%03g_%g_%g',nbpoisson ,nbbanc, round(ProfondeurCentre), D1, k, roll*180/pi,NbBancsVusMono );
            FileOasisResMono=[chemin,'\','simulation\sardine\', dirScene,'\mono\',filenamemono];
        elseif Espece ==1
            filenamemono = sprintf('scene_mono_maquereau_%g_%g_%03g_%g_%03g_%g_%g',nbpoisson ,nbbanc, round(ProfondeurCentre), D1, k, roll*180/pi,NbBancsVusMono );
            FileOasisResMono=[chemin,'\','simulation\maquereau\', dirScene,'\mono\',filenamemono];
        elseif Espece ==2
            filenamemono = sprintf('scene_mono_bulle_%g_%g_%03g_%g_%03g_%g_%g',nbpoisson ,nbbanc, round(ProfondeurCentre), D1, k, roll*180/pi,NbBancsVusMono );
            FileOasisResMono=[chemin,'\','simulation\bulle\', dirScene,'\mono\',filenamemono];
        end

        fprintf(fid,'\n  <Configuration TYPE="ConfigStorage">');
        fprintf(fid,'\n   <PARAMETER NAME="ResultFileName" VALUE="%s" />',FileOasisResMono);
        fprintf(fid,'\n   <PARAMETER NAME="FileTypeRESON" VALUE="0" />');
        fprintf(fid,'\n   <PARAMETER NAME="FileTypeSIMRAD" VALUE="0" /> ');
        fprintf(fid,'\n   <PARAMETER NAME="FileTypeMATLAB" VALUE="0" /> ');
        fprintf(fid,'\n   <PARAMETER NAME="FileTypeHAC" VALUE="1" /> ');
        fprintf(fid,'\n   <PARAMETER NAME="DecimationRatio" VALUE="1" /> ');
        fprintf(fid,'\n   <PARAMETER NAME="StorageThreshold" VALUE="-100" /> ');
        fprintf(fid,'\n  </Configuration>');




        fprintf(fid,'\n  <Configuration TYPE="ConfigVesselMotion">');
        fprintf(fid,'\n   <PARAMETER NAME="Vbat" VALUE="%d"/>',VBat );
        fprintf(fid,'\n   <PARAMETER NAME="RollAmplitude" VALUE="%d" />',RollAmplitude);
        fprintf(fid,'\n   <PARAMETER NAME="RollPeriod" VALUE="%d" />',RollPeriod);
        fprintf(fid,'\n   <PARAMETER NAME="PitchAmplitude" VALUE="%d" />',PitchAmplitude);
        fprintf(fid,'\n   <PARAMETER NAME="PitchPeriod" VALUE="%d" />',PitchPeriod);
        fprintf(fid,'\n   <PARAMETER NAME="RollInit" VALUE="%d" />',RollInit);
        fprintf(fid,'\n   <PARAMETER NAME="PitchInit" VALUE="%d" />',PitchInit);
        fprintf(fid,'\n   <PARAMETER NAME="FechCentraleNav" VALUE="%d" />',FechCentraleNav);
        fprintf(fid,'\n   <PARAMETER NAME="InitialLatitude" VALUE-Deg="%d" VALUE-Min="%d" VALUE-Sec="%d" VALUE-Orientation="%d" />',latDeg,latMin,latSec,latO);
        fprintf(fid,'\n   <PARAMETER NAME="InitialLongitude" VALUE-Deg="%d" VALUE-Min="%d" VALUE-Sec="%d" VALUE-Orientation="%d" />',lonDeg,lonMin,lonSec,lonO);
        fprintf(fid,'\n   <PARAMETER NAME="Heading" VALUE="%d" />',heading);
        fprintf(fid,'\n   <PARAMETER NAME="HeaveAmplitude" VALUE="%d" />',HeaveAmplitude);
        fprintf(fid,'\n   <PARAMETER NAME="HeavePeriod" VALUE="%d" />',HeavePeriod);
        fprintf(fid,'\n   <PARAMETER NAME="HeaveInit" VALUE="%d" />',HeaveInit);
        fprintf(fid,'\n   <PARAMETER NAME="DepthAttitude" VALUE="%d" />',DepthAttitude);
        fprintf(fid,'\n  </Configuration>');

        fprintf(fid,'\n </Configuration>');
        fprintf(fid,'\n</APPLICATION>');

        fclose(fid);
D2_couche=2*(tan(((AngleMulti*pi)/180)/2)*ProfondeurCentre);
        line([0 0],[-D2_couche/2 D2_couche/2], [ProfondeurCentre ProfondeurCentre],'LineStyle','-','LineWidth',2,'Color','r');
        line([0 D],[-D2_couche/2 -D2_couche/2], [ProfondeurCentre ProfondeurCentre],'LineStyle','-','LineWidth',2,'Color','r');
        line([D D],[-D2_couche/2 D2_couche/2], [ProfondeurCentre ProfondeurCentre],'LineStyle','-','LineWidth',2,'Color','r');
        line([D 0],[D2_couche/2 D2_couche/2], [ProfondeurCentre ProfondeurCentre],'LineStyle','-','LineWidth',2,'Color','r');
        axis([0 D -D2_couche/2 D2_couche/2 0 ProfondeurCentre+50]);

        %%%%%%%%%%%%%Ecriture du fichier bancsfin_multi.xml%%%%%%%%%%%%%%%%% %%




        ArrayPsiFile='C:\temp\stageSMFH2009\developpement\weighting_athwart_HR21.txt';
        ArrayPhiFile='C:\temp\stageSMFH2009\developpement\weighting_along_HR21.txt';
        FanBeamDistributionFile='C:\temp\stageSMFH2009\developpement\distrib_spatio_freq.txt';



        if Espece==0
            Oasisfilenamemulti = sprintf('scene_multi_sardine_%g_%g_%03g_%g_%g_%g.oasis',nbpoisson,nbbanc, round(ProfondeurCentre), D1,k, roll*180/pi);
        elseif Espece==1
            Oasisfilenamemulti = sprintf('scene_multi_maquereau_%g_%g_%03g_%g_%g_%g.oasis',nbpoisson,nbbanc, round(ProfondeurCentre), D1,k, roll*180/pi);
        elseif Espece==2
            Oasisfilenamemulti = sprintf('scene_multi_bulle_%g_%g_%03g_%g_%g_%g.oasis',nbpoisson,nbbanc, round(ProfondeurCentre), D1,k, roll*180/pi);
        end

        fid = fopen([chemin,'\', dirScene '\' Oasisfilenamemulti], 'wt');
        fprintf(fid,'<?xml version="1.0" encoding="UTF-8" standalone="no" ?>');
        fprintf(fid,'\n<APPLICATION APPNAME="OASIS" APPVERSION_MAJ="2" APPVERSION_MIN="9">');
        fprintf(fid,'\n');
        fprintf(fid,'\n <Configuration TYPE="Scenario">');
        fprintf(fid,'\n  <Configuration TYPE="ConfigEnv">');
        fprintf(fid,'\n   <PARAMETER NAME="SeaTemperature" VALUE="10" />');
        fprintf(fid,'\n   <PARAMETER NAME="Salinity" VALUE="35" />');
        fprintf(fid,'\n   <PARAMETER NAME="PH" VALUE="8" />');
        fprintf(fid,'\n   <PARAMETER NAME="WindSpeed" VALUE="15" />');
        fprintf(fid,'\n   <PARAMETER NAME="SelfNoiseLevel1Hz" VALUE="173" />');
        fprintf(fid,'\n   <PARAMETER NAME="PropellerPosition" VALUE-X="-40" VALUE-Y="0" VALUE-Z="0" />');
        fprintf(fid,'\n  </Configuration>');

        fprintf(fid,'\n  <Configuration TYPE="ConfigScene">');
        fprintf(fid,'\n   <PARAMETER NAME="HasSeaFloor" VALUE="%d" />',AvecSansFond);
        fprintf(fid,'\n   <PARAMETER NAME="SeaFloorName" VALUE="Seafloor Name" />');
        fprintf(fid,'\n   <PARAMETER NAME="H" VALUE="%d" />',2*ProfondeurFond);
        fprintf(fid,'\n   <PARAMETER NAME="PhiBottom" VALUE="0" />');
        fprintf(fid,'\n   <PARAMETER NAME="PsiBottom" VALUE="0" />');
        fprintf(fid,'\n   <PARAMETER NAME="m10loga1" VALUE="-5" />');
        fprintf(fid,'\n   <PARAMETER NAME="a2" VALUE="180" />');
        fprintf(fid,'\n   <PARAMETER NAME="m10loga3" VALUE="-25" />');
        fprintf(fid,'\n   <PARAMETER NAME="Prof" VALUE="10" />');
        fprintf(fid,'\n   <PARAMETER NAME="BottomAngleSampling" VALUE="10" />');
        fprintf(fid,'\n   <PARAMETER NAME="NivMinIntercor" VALUE="-60" />');
        fprintf(fid,'\n   <PARAMETER NAME="NivMinGabarit" VALUE="-60" />');
        fprintf(fid,'\n   <PARAMETER NAME="Seed" VALUE="1" />');
        fprintf(fid,'\n   <Configuration TYPE="Schools">');

        figure(round(ProfondeurCentre));
        set(round(ProfondeurCentre),'Units','Normalized','Position',[0 0 1 1]);
        set(round(ProfondeurCentre),'Name', ['monofaisceau/multifaisceaux, ', num2str(ProfondeurCentre),'m ', num2str(NbPing),'Pings ',num2str(D),'m' ], 'NumberTitle','off');
        subplot(2,1,2);
        title(['multifaisceau: ',num2str(round(nbpb)),' poissons par bancs ', num2str(densite),' poisson/m^3'])
        set(gca,'ydir','reverse');
        set(gca,'zdir','reverse');
        xlabel('x');
        ylabel('y');
        zlabel('z');
        axis equal;
        % % Repr�sentations des plans monofaisceau
        % % p1=[L l H];
        % % p2=[L l+kMulti H+ProfondeurFond];
        % % p3=[L l-kMulti H+ProfondeurFond];
        % % p4=[L+D*cos((heading*pi)/180) (sin((heading*pi)/180))*D+l H];
        % % p5=[L+D*cos((heading*pi)/180) (sin((heading*pi)/180))*D+l+kMulti H+ProfondeurFond];
        % % p6=[L+D*cos((heading*pi)/180) (sin((heading*pi)/180))*D+l-kMulti H+ProfondeurFond];

        p1=[L l H];
        p2=[L l+kMulti H+ProfondeurFond];
        p3=[L l-kMulti H+ProfondeurFond];
        p4=[L+D l H];
        p5=[L+D l+kMulti H+ProfondeurFond];
        p6=[L+D l-kMulti H+ProfondeurFond];

      
        S7=[p1;p4;p2];
        patch(S7(:,1),S7(:,2),S7(:,3),'b');
        S8=[p2;p4;p5];
        patch(S8(:,1),S8(:,2),S8(:,3),'b');
        S9=[p1;p4;p3];
        patch(S9(:,1),S9(:,2),S9(:,3),'b');
        S10=[p3;p4;p6];
        patch(S10(:,1),S10(:,2),S10(:,3),'b');


        dd=2;                      %on fixe le param�tre d (diff�rent de 0)

        P1=[p1;p4;p2];   %matrice 3*3 contenant les coefficients
        P2=[p1;p4;p3];

        matd=[-dd;-dd;-dd];             %vecteur colonne 3*1 rempli par le coeff d

        EE1=inv(P1)*matd;
        EE2=inv(P2)*matd;
        ee11=EE1(1);
        ee12=EE1(2);
        ee13=EE1(3);
        ee21=EE2(1);
        ee22=EE2(2);
        ee23=EE2(3);

        p1c=[L l H];
        p2c=[L l+kMultiCentre H+ProfondeurFond];
        p3c=[L l-kMultiCentre H+ProfondeurFond];
        p4c=[L+D l H];
        p5c=[L+D l+kMultiCentre H+ProfondeurFond];
        p6c=[L+D l-kMultiCentre H+ProfondeurFond];


        P1c=[p1c;p4c;p2c];   %matrice 3*3 contenant les coefficients
        P2c=[p1c;p4c;p3c];

        EE1c=inv(P1c)*matd;
        EE2c=inv(P2c)*matd;
        ee11c=EE1c(1);
        ee12c=EE1c(2);
        ee13c=EE1c(3);
        ee21c=EE2c(1);
        ee22c=EE2c(2);
        ee23c=EE2c(3);

     

        NbBancsVusMulti=0;
        NbBancsVusMultiCentre=0;

        for i = 1:nbbanc;

           

            eqq1=ee11*M(k,i,1)+ee12*M(k,i,2)+ee13*M(k,i,3)+dd;
            eqq2=ee21*M(k,i,1)+ee22*M(k,i,2)+ee23*M(k,i,3)+dd;
            
          

            eqq1c=ee11c*M(k,i,1)+ee12c*M(k,i,2)+ee13c*M(k,i,3)+dd;
            eqq2c=ee21c*M(k,i,1)+ee22c*M(k,i,2)+ee23c*M(k,i,3)+dd;

%             if (D1/2)>H*tan((AngleMulti*pi/180)/2),
% 
%                 if(eqq1>0 && eqq2>0) ,
%                     NbBancsVusMulti=NbBancsVusMulti+1;
% 
%                     hh=surface(x(:,:,k,i),y(:,:,k,i),z(:,:,k,i),'FaceColor','red','edgecolor','none');
%                     tt = hgtransform;
%                     set(hh,'Parent',tt)
%                     Rx = makehgtform('xrotate',roll);
%                     Ry = makehgtform('yrotate',pitch);
%                     Rz = makehgtform('zrotate',yaw);
%                     Tx1 = makehgtform('translate',[-M(k,i,1) -M(k,i,2) -M(k,i,3)]);
%                     Tx2 = makehgtform('translate',[M(k,i,1) M(k,i,2) M(k,i,3)]);
%                     set(tt,'Matrix',Tx2*Rx*Ry*Rz*Tx1)
% 
% 
%                     hold on;
%                     fprintf(fid,'\n  <School NAME="a%d">',i);
%                     fprintf(fid,'\n   <PARAMETER NAME="TSMode" VALUE="0" />');
%                     fprintf(fid,'\n   <PARAMETER NAME="TS" VALUE="%f" /> ',TS);
%                     fprintf(fid,'\n   <PARAMETER NAME="FreqList" VALUE="18000;38000;70000;120000;200000;" /> ');
%                     fprintf(fid,'\n   <PARAMETER NAME="TSList" VALUE="%f" />',TS);
%                     fprintf(fid,'\n   <PARAMETER NAME="Position" VALUE-X="%f" VALUE-Y="%f" VALUE-Z="%f"/>',M(k,i,1),M(k,i,2),M(k,i,3));
%                     fprintf(fid,'\n   <PARAMETER NAME="Orientation" VALUE-X="%f" VALUE-Y="%f" VALUE-Z="%f"/>',roll*180/pi,pitch*180/pi,yaw*180/pi);
%                     fprintf(fid,'\n   <PARAMETER NAME="Dimension" VALUE-X="%f" VALUE-Y="%f" VALUE-Z="%f"/>',D1,D2,D3);
%                     fprintf(fid,'\n   <PARAMETER NAME="Spacing" VALUE-X="%f" VALUE-Y="%f" VALUE-Z="%f"/>',Xp,Xp,Xp);
%                     fprintf(fid,'\n   <PARAMETER NAME="Deviation" VALUE-X="0.1" VALUE-Y="0.1" VALUE-Z="0.1"/>');
%                     fprintf(fid,'\n   <PARAMETER NAME="Seed" VALUE="1"/>');
%                      fprintf(fid,'\n   <PARAMETER NAME="SchoolType" VALUE="%d"/>',forme);
%                     fprintf(fid,'\n  </School>');
% 
%                 else
%                     hh=surface(x(:,:,k,i),y(:,:,k,i),z(:,:,k,i),'FaceColor','green','edgecolor','none');
%                     tt = hgtransform;
%                     set(hh,'Parent',tt)
%                     Rx = makehgtform('xrotate',roll);
%                     Ry = makehgtform('yrotate',pitch);
%                     Rz = makehgtform('zrotate',yaw);
%                     Tx1 = makehgtform('translate',[-M(k,i,1) -M(k,i,2) -M(k,i,3)]);
%                     Tx2 = makehgtform('translate',[M(k,i,1) M(k,i,2) M(k,i,3)]);
%                     set(tt,'Matrix',Tx2*Rx*Ry*Rz*Tx1)
%                     hold on;
% 
%                     %� commenter si on ne veut simuler que les bancs situ�
%                     %dans le volume insonifi�
%                     fprintf(fid,'\n  <School NAME="a%d">',i);
%                     fprintf(fid,'\n   <PARAMETER NAME="TSMode" VALUE="0" />');
%                     fprintf(fid,'\n   <PARAMETER NAME="TS" VALUE="%f" /> ',TS);
%                     fprintf(fid,'\n   <PARAMETER NAME="FreqList" VALUE="18000;38000;70000;120000;200000;" /> ');
%                     fprintf(fid,'\n   <PARAMETER NAME="TSList" VALUE="%f" />',TS);
%                     fprintf(fid,'\n   <PARAMETER NAME="Position" VALUE-X="%f" VALUE-Y="%f" VALUE-Z="%f"/>',M(k,i,1),M(k,i,2),M(k,i,3));
%                     fprintf(fid,'\n   <PARAMETER NAME="Orientation" VALUE-X="%f" VALUE-Y="%f" VALUE-Z="%f"/>',roll*180/pi,pitch*180/pi,yaw*180/pi);
%                     fprintf(fid,'\n   <PARAMETER NAME="Dimension" VALUE-X="%f" VALUE-Y="%f" VALUE-Z="%f"/>',D1,D2,D3);
%                     fprintf(fid,'\n   <PARAMETER NAME="Spacing" VALUE-X="%f" VALUE-Y="%f" VALUE-Z="%f"/>',Xp,Xp,Xp);
%                     fprintf(fid,'\n   <PARAMETER NAME="Deviation" VALUE-X="0.1" VALUE-Y="0.1" VALUE-Z="0.1"/>');
%                     fprintf(fid,'\n   <PARAMETER NAME="Seed" VALUE="1"/>');
%                     fprintf(fid,'\n   <PARAMETER NAME="SchoolType" VALUE="1"/>');
%                     fprintf(fid,'\n  </School>');
% 
%                 end
% 
%             elseif (D1/2)<abs(H*tan((AngleMulti*pi/180)/2)),

                if(eqq1<0 && eqq2<0) ,
                    NbBancsVusMulti=NbBancsVusMulti+1;

                    hh=surface(x(:,:,k,i),y(:,:,k,i),z(:,:,k,i),'FaceColor','red','edgecolor','none');
                    tt = hgtransform;
                    set(hh,'Parent',tt)
                    Rx = makehgtform('xrotate',roll);
                    Ry = makehgtform('yrotate',pitch);
                    Rz = makehgtform('zrotate',yaw);
                    Tx1 = makehgtform('translate',[-M(k,i,1) -M(k,i,2) -M(k,i,3)]);
                    Tx2 = makehgtform('translate',[M(k,i,1) M(k,i,2) M(k,i,3)]);
                    set(tt,'Matrix',Tx2*Rx*Ry*Rz*Tx1)


                    hold on;
                    fprintf(fid,'\n  <School NAME="a%d">',i);
                    fprintf(fid,'\n   <PARAMETER NAME="TSMode" VALUE="3" />');
                    fprintf(fid,'\n   <PARAMETER NAME="TS" VALUE="%f" /> ',TS);
                    fprintf(fid,'\n   <PARAMETER NAME="FreqList" VALUE="1;" /> ');
                    fprintf(fid,'\n   <PARAMETER NAME="TSList" VALUE="%f" />',TS);
                    fprintf(fid,'\n   <PARAMETER NAME="Position" VALUE-X="%f" VALUE-Y="%f" VALUE-Z="%f"/>',M(k,i,1),M(k,i,2),M(k,i,3));
                    fprintf(fid,'\n   <PARAMETER NAME="Orientation" VALUE-X="%f" VALUE-Y="%f" VALUE-Z="%f"/>',roll*180/pi,pitch*180/pi,yaw*180/pi);
                    fprintf(fid,'\n   <PARAMETER NAME="Dimension" VALUE-X="%f" VALUE-Y="%f" VALUE-Z="%f"/>',D1,D2,D3);
                    fprintf(fid,'\n   <PARAMETER NAME="Spacing" VALUE-X="%f" VALUE-Y="%f" VALUE-Z="%f"/>',Xp,Xp,Xp);
                    fprintf(fid,'\n   <PARAMETER NAME="Deviation" VALUE-X="0.1" VALUE-Y="0.1" VALUE-Z="0.1"/>');
                    fprintf(fid,'\n   <PARAMETER NAME="Seed" VALUE="1"/>');
                    fprintf(fid,'\n   <PARAMETER NAME="OrientationDeviation" VALUE-X="2" VALUE-Y="2" VALUE-Z="2"/>');
                    fprintf(fid,'\n   <PARAMETER NAME="OrientationSeed" VALUE="1"/>');
                   fprintf(fid,'\n   <PARAMETER NAME="FishLength" VALUE="%f"/>',taillecible/100);
                    fprintf(fid,'\n   <PARAMETER NAME="FishLengthSeed" VALUE="1"/>');
                    fprintf(fid,'\n   <PARAMETER NAME="FishLengthDeviation" VALUE="%f"/>',tailleciblesd/100);
                    fprintf(fid,'\n   <PARAMETER NAME="SchoolType" VALUE="0"/>');
                    fprintf(fid,'\n   <PARAMETER NAME="DiffuserModelIndex" VALUE="7"/>');
                    fprintf(fid,'\n  </School>');

                else
                    hh=surface(x(:,:,k,i),y(:,:,k,i),z(:,:,k,i),'FaceColor','green','edgecolor','none');
                    tt = hgtransform;
                    set(hh,'Parent',tt)
                    Rx = makehgtform('xrotate',roll);
                    Ry = makehgtform('yrotate',pitch);
                    Rz = makehgtform('zrotate',yaw);
                    Tx1 = makehgtform('translate',[-M(k,i,1) -M(k,i,2) -M(k,i,3)]);
                    Tx2 = makehgtform('translate',[M(k,i,1) M(k,i,2) M(k,i,3)]);
                    set(tt,'Matrix',Tx2*Rx*Ry*Rz*Tx1)
                    hold on;

                    %� commenter si on ne veut simuler que les bancs situ�
                    %dans le volume insonifi�
                    fprintf(fid,'\n  <School NAME="a%d">',i);
                    fprintf(fid,'\n   <PARAMETER NAME="TSMode" VALUE="3" />');
                    fprintf(fid,'\n   <PARAMETER NAME="TS" VALUE="%f" /> ',TS);
                    fprintf(fid,'\n   <PARAMETER NAME="FreqList" VALUE="1;" /> ');
                    fprintf(fid,'\n   <PARAMETER NAME="TSList" VALUE="%f" />',TS);
                    fprintf(fid,'\n   <PARAMETER NAME="Position" VALUE-X="%f" VALUE-Y="%f" VALUE-Z="%f"/>',M(k,i,1),M(k,i,2),M(k,i,3));
                    fprintf(fid,'\n   <PARAMETER NAME="Orientation" VALUE-X="%f" VALUE-Y="%f" VALUE-Z="%f"/>',roll*180/pi,pitch*180/pi,yaw*180/pi);
                    fprintf(fid,'\n   <PARAMETER NAME="Dimension" VALUE-X="%f" VALUE-Y="%f" VALUE-Z="%f"/>',D1,D2,D3);
                    fprintf(fid,'\n   <PARAMETER NAME="Spacing" VALUE-X="%f" VALUE-Y="%f" VALUE-Z="%f"/>',Xp,Xp,Xp);
                    fprintf(fid,'\n   <PARAMETER NAME="Deviation" VALUE-X="0.1" VALUE-Y="0.1" VALUE-Z="0.1"/>');
                    fprintf(fid,'\n   <PARAMETER NAME="Seed" VALUE="1"/>');
                    fprintf(fid,'\n   <PARAMETER NAME="OrientationDeviation" VALUE-X="2" VALUE-Y="2" VALUE-Z="2"/>');
                    fprintf(fid,'\n   <PARAMETER NAME="OrientationSeed" VALUE="1"/>');
               fprintf(fid,'\n   <PARAMETER NAME="FishLength" VALUE="%f"/>',taillecible/100);
                    fprintf(fid,'\n   <PARAMETER NAME="FishLengthSeed" VALUE="1"/>');
                    fprintf(fid,'\n   <PARAMETER NAME="FishLengthDeviation" VALUE="%f"/>',tailleciblesd/100);
                     fprintf(fid,'\n   <PARAMETER NAME="SchoolType" VALUE="%d"/>',forme);
                    fprintf(fid,'\n   <PARAMETER NAME="DiffuserModelIndex" VALUE="7"/>');
                    fprintf(fid,'\n  </School>');
                end
                if(eqq1c<0 && eqq2c<0) ,
                    NbBancsVusMultiCentre=NbBancsVusMultiCentre+1;
                end;
%             end
        end
        fprintf(fid,'\n   </Configuration>');
        fprintf(fid,'\n  </Configuration>');


        NbPoissonVusMulti(k)=NbBancsVusMulti*nbpb;
        NbPoissonVusMultiCentre(k)=NbBancsVusMultiCentre*nbpb;
   

        
        

        fprintf(fid,'\n  <Configuration TYPE="ConfigSignals">');
        fprintf(fid,'\n   <PARAMETER NAME="PulseLength" VALUE="0.001" />');
        fprintf(fid,'\n   <PARAMETER NAME="ModulationType" VALUE="1" /> ');
        fprintf(fid,'\n   <PARAMETER NAME="BetaFactor" VALUE="-20" /> ');
        fprintf(fid,'\n   <PARAMETER NAME="Fdemod" VALUE="95000" /> ');
        fprintf(fid,'\n   <PARAMETER NAME="Fech" VALUE="62500" /> ');
        fprintf(fid,'\n   <PARAMETER NAME="TypeSelection" VALUE="0" />');
        fprintf(fid,'\n   <PARAMETER NAME="Tsig" VALUE="0.003" /> ');
        fprintf(fid,'\n   <PARAMETER NAME="EchoesWindowLength" VALUE="0.005" />');
        fprintf(fid,'\n  </Configuration>');

        fprintf(fid,'\n  <Configuration TYPE="ConfigSounder">');
        fprintf(fid,'\n   <PARAMETER NAME="Nping" VALUE="%d" />',NbPing);
        fprintf(fid,'\n   <PARAMETER NAME="Trec" VALUE="%d" />',InterPing);
        fprintf(fid,'\n   <PARAMETER NAME="Rmin" VALUE="%d" />',Rmin);
        fprintf(fid,'\n   <PARAMETER NAME="Rmax" VALUE="%d" />',RmaxMulti);
        fprintf(fid,'\n   <PARAMETER NAME="SL" VALUE="230" />');
        fprintf(fid,'\n   <PARAMETER NAME="TVG" VALUE="0" />');
        fprintf(fid,'\n   <PARAMETER NAME="TransducerType" VALUE="0">');
        fprintf(fid,'\n    <PARAMETER NAME="TransmitterStabilisation" VALUE="3" />');
        fprintf(fid,'\n    <PARAMETER NAME="ReceiverStabilisation" VALUE="3" />');
        fprintf(fid,'\n    <PARAMETER NAME="Position" VALUE-X="26.73" VALUE-Y="0" VALUE-Z="7.12" />');
        fprintf(fid,'\n    <PARAMETER NAME="SensorLength" VALUE="14.2" />');
        fprintf(fid,'\n    <PARAMETER NAME="SensorWidth" VALUE="6.7" />');
        fprintf(fid,'\n    <PARAMETER NAME="LongitudinalSampling" VALUE="15" />');
        fprintf(fid,'\n    <PARAMETER NAME="AthwartshipSampling" VALUE="7.5" />');
        fprintf(fid,'\n    <PARAMETER NAME="NbAlongShipSensors" VALUE="20" />');
        fprintf(fid,'\n    <PARAMETER NAME="NbAthwartShipSensors" VALUE="40" />');
        fprintf(fid,'\n    <PARAMETER NAME="ArrayPsiFile" VALUE="%s" />',ArrayPsiFile);
        fprintf(fid,'\n    <PARAMETER NAME="ArrayPhiFile" VALUE="%s" />',ArrayPhiFile);
        fprintf(fid,'\n    <PARAMETER NAME="FanBeamDistributionFile" VALUE="%s" />',FanBeamDistributionFile);
        fprintf(fid,'\n    <PARAMETER NAME="AngDPsiMax" VALUE="10" />');
        fprintf(fid,'\n    <PARAMETER NAME="AngDPhiMax" VALUE="5" />');
        fprintf(fid,'\n    <PARAMETER NAME="DepNpsi" VALUE="21" />');
        fprintf(fid,'\n    <PARAMETER NAME="DepNphi" VALUE="11" />');
        fprintf(fid,'\n    <PARAMETER NAME="TypeBeam" VALUE="1" /> ');
        fprintf(fid,'\n    <PARAMETER NAME="NumberAngleSamples" VALUE="361" />');
        fprintf(fid,'\n    <PARAMETER NAME="MaxAngleBeam" VALUE="90" />');
        fprintf(fid,'\n    <PARAMETER NAME="MinAngleBeam" VALUE="-90" />');
        fprintf(fid,'\n    <PARAMETER NAME="BeamPsiFile" VALUE="" />');
        fprintf(fid,'\n    <PARAMETER NAME="BeamPhiFile" VALUE="" />');
        fprintf(fid,'\n   </PARAMETER>');
        fprintf(fid,'\n  </Configuration>');

        if Espece == 0
            filenamemulti = sprintf('scene_multi_sardine_%g_%g_%03g_%g_%03g_%g_%g',nbpoisson,nbbanc, round(ProfondeurCentre), D1, k, roll*180/pi,NbBancsVusMulti);
            FileOasisResMulti=[chemin,'\','simulation\sardine\', dirScene,'\multi\',filenamemulti];
        elseif Espece ==1
            filenamemulti = sprintf('scene_multi_maquereau_%g_%g_%03g_%g_%03g_%g_%g',nbpoisson,nbbanc, round(ProfondeurCentre), D1, k, roll*180/pi,NbBancsVusMulti);
            FileOasisResMulti=[chemin,'\','simulation\maquereau\', dirScene,'\multi\',filenamemulti];
        elseif Espece ==2
            filenamemulti = sprintf('scene_multi_bulle_%g_%g_%03g_%g_%03g_%g_%g',nbpoisson,nbbanc, round(ProfondeurCentre), D1, k, roll*180/pi,NbBancsVusMulti);
            FileOasisResMulti=[chemin,'\','simulation\bulle\', dirScene,'\multi\',filenamemulti];
        end


        fprintf(fid,'\n  <Configuration TYPE="ConfigStorage">');
        fprintf(fid,'\n   <PARAMETER NAME="ResultFileName" VALUE="%s" />',FileOasisResMulti);
        fprintf(fid,'\n   <PARAMETER NAME="FileTypeRESON" VALUE="0" />');
        fprintf(fid,'\n   <PARAMETER NAME="FileTypeSIMRAD" VALUE="0" /> ');
        fprintf(fid,'\n   <PARAMETER NAME="FileTypeMATLAB" VALUE="0" /> ');
        fprintf(fid,'\n   <PARAMETER NAME="FileTypeHAC" VALUE="1" /> ');
        fprintf(fid,'\n   <PARAMETER NAME="DecimationRatio" VALUE="1" /> ');
        fprintf(fid,'\n   <PARAMETER NAME="StorageThreshold" VALUE="-120" /> ');
        fprintf(fid,'\n  </Configuration>');




        fprintf(fid,'\n  <Configuration TYPE="ConfigVesselMotion">');
        fprintf(fid,'\n   <PARAMETER NAME="Vbat" VALUE="%d"/>',VBat );
        fprintf(fid,'\n   <PARAMETER NAME="RollAmplitude" VALUE="%d" />',RollAmplitude);
        fprintf(fid,'\n   <PARAMETER NAME="RollPeriod" VALUE="%d" />',RollPeriod);
        fprintf(fid,'\n   <PARAMETER NAME="PitchAmplitude" VALUE="%d" />',PitchAmplitude);
        fprintf(fid,'\n   <PARAMETER NAME="PitchPeriod" VALUE="%d" />',PitchPeriod);
        fprintf(fid,'\n   <PARAMETER NAME="RollInit" VALUE="%d" />',RollInit);
        fprintf(fid,'\n   <PARAMETER NAME="PitchInit" VALUE="%d" />',PitchInit);
        fprintf(fid,'\n   <PARAMETER NAME="FechCentraleNav" VALUE="%d" />',FechCentraleNav);
        fprintf(fid,'\n   <PARAMETER NAME="InitialLatitude" VALUE-Deg="%d" VALUE-Min="%d" VALUE-Sec="%d" VALUE-Orientation="%d" />',latDeg,latMin,latSec,latO);
        fprintf(fid,'\n   <PARAMETER NAME="InitialLongitude" VALUE-Deg="%d" VALUE-Min="%d" VALUE-Sec="%d" VALUE-Orientation="%d" />',lonDeg,lonMin,lonSec,lonO);
        fprintf(fid,'\n   <PARAMETER NAME="Heading" VALUE="%d" />',heading);
        fprintf(fid,'\n   <PARAMETER NAME="HeaveAmplitude" VALUE="%d" />',HeaveAmplitude);
        fprintf(fid,'\n   <PARAMETER NAME="HeavePeriod" VALUE="%d" />',HeavePeriod);
        fprintf(fid,'\n   <PARAMETER NAME="HeaveInit" VALUE="%d" />',HeaveInit);
        fprintf(fid,'\n   <PARAMETER NAME="DepthAttitude" VALUE="%d" />',DepthAttitude);
        fprintf(fid,'\n  </Configuration>');

        fprintf(fid,'\n </Configuration>');
        fprintf(fid,'\n</APPLICATION>');

        fclose(fid);

        
        line([0 0],[-D2_couche/2 D2_couche/2], [ProfondeurCentre ProfondeurCentre],'LineStyle','-','LineWidth',2,'Color','r');
        line([0 D],[-D2_couche/2 -D2_couche/2], [ProfondeurCentre ProfondeurCentre],'LineStyle','-','LineWidth',2,'Color','r');
        line([D D],[-D2_couche/2 D2_couche/2], [ProfondeurCentre ProfondeurCentre],'LineStyle','-','LineWidth',2,'Color','r');
        line([D 0],[D2_couche/2 D2_couche/2], [ProfondeurCentre ProfondeurCentre],'LineStyle','-','LineWidth',2,'Color','r');
        axis([0 D -D2_couche/2 D2_couche/2 0 ProfondeurCentre+50]);
%         mkdir(['simulation\figures\scenes\' dirScene]);
%         saveas(gcf,['simulation\figures\scenes\', dirScene,'\',sprintf('scene_sardine_%g_%g_%g_%g_%g_%g.png',nbpoisson,nbbanc, round(ProfondeurCentre), D1, k ,roll*180/pi)],'png')
        
        if k==10
            toto=0;
        end
%         close;

    end
    
    save([chemin,'\', dirScene  '\nbpoissonsvu.mat'],'NbPoissonVusMono','NbPoissonVusMulti','NbPoissonVusMultiCentre');

end
