close all
clear all


chemin ='C:\data\PELGAS09\RUN032\';



%%Echo-int�gration (peut �tre comment� si elle a d�j� �t� ex�cut�e)

% SampleEchoIntegration(chemin);
% clear functions;

% chargement des r�sultats

depth_bottom_ER60=[];
Sa_surface_ER60 = [];
depth_bottom_ME70=[];
Sa_surface_ME70 = [];

seuil=0;


filelist = ls([chemin,'*_EI.mat']);  % ensemble des fichiers
nb_files = size(filelist,1);  % nombre de fichiers
sort(filelist);

nblME70=0;
nblER60=0;
for numfile = 1:nb_files
  
    matfilename = filelist(numfile,:)
    
   
    FileName = [chemin,matfilename(1:findstr('.',matfilename)-1),'.mat'];
    load(FileName);

   
       if  size(Depth_botME70,1)>0

        timeME70(nblME70+1:nblME70+size(Depth_botME70,1)) = time_ME70;
        timeER60(nblER60+1:nblER60+size(Depth_botER60,1)) = time_ER60;
        
        depth_bottom_ME70(nblME70+1:nblME70+size(Depth_botME70,1),:,:) = Depth_botME70;
        depth_bottom_ER60(nblER60+1:nblER60+size(Depth_botER60,1),:,:) = Depth_botER60;

        Sa_bottom_ME70(nblME70+1:nblME70+size(Sa_botME70,1),:,:) = Sa_botME70;
        Sa_bottom_ER60(nblER60+1:nblER60+size(Sa_botER60,1),:,:) = Sa_botER60;

        Sa_surface_ME70(nblME70+1:nblME70+size(Sa_surfME70,1),:,:) = Sa_surfME70;
        Sa_surface_ER60(nblER60+1:nblER60+size(Sa_surfER60,1),:,:) = Sa_surfER60;
        
   

        
        Sa_surface_ME70_mean(nblME70+1:nblME70+size(Sa_surfME70,1),:)=(sum((Volume_surfME70(:,:,:)).*Sa_surfME70(:,:,:),3))./sum((Volume_surfME70(:,:,:)),3);
        Sa_surface_ME70_centre(nblME70+1:nblME70+size(Sa_surfME70,1),:) = Sa_surfME70(:,:,11);
        Sa_surface_ME70_mean_centre(nblME70+1:nblME70+size(Sa_surfME70,1),:) = (sum(((Volume_surfME70(:,:,6:16)).*Sa_surfME70(:,:,6:16)),3)./sum((Volume_surfME70(:,:,6:16)),3));
        

        nblME70=size(depth_bottom_ME70,1);
        nblER60=size(depth_bottom_ER60,1);
    end
    

                
    
  
    
    clear  Depth_botER60  Sa_botER60 Sa_surfER60;
    
            clear  Depth_botME70  Sa_botME70 Sa_surfME70 Volume_surfME70;
        
    
end

    
    Nmesuremono1(:,:)=Sa_surface_ER60(:,:,1);
    Nmesuremono2(:,:)=Sa_surface_ER60(:,:,2);
    Nmesuremono3(:,:)=Sa_surface_ER60(:,:,3);
    Nmesuremono4(:,:)=Sa_surface_ER60(:,:,4);
    Nmesuremono5(:,:)=Sa_surface_ER60(:,:,5);
    
      
    Nmesuremulti(:,:) = (Sa_surface_ME70_mean);
%     NmesuremultiSeuil(:) = (moyenne_Sa_surface_ME70(:,1)/sigma)*((D1*D2)/1852^2);
    Nmesuremulti11(:,:) = (Sa_surface_ME70_centre);
    Nmesuremultimoyennecentre(:,:) = (Sa_surface_ME70_mean_centre);
   

Profondeur = round(depth_bottom_ER60(:,1));

%v:un vecteur par fichier contenant les valeurs de SA avec seuil sur les valeurs des SA
% for jj=1:nb_files
%     v=reshape(Sa_surface_ME70(jj,:),1,21);
%     v=v(v>=seuil);
%     moyenne_Sa_surface_ME70(jj,1)=mean(v);
% end
%     
%graphiques
%cr�ation de vecteurs pour le label de l'axe X
% for i=1:1:max(nb_files)
%     a(i)=depth_bottom_ER60(i,1);
% end

map = colormap(hsv(100));

timeER60ml = timeER60;
timeME70ml = timeME70;


figure;
% plot( Nmesuremono1(:),'b', 'linewidth',2)

plot( timeER60ml,Nmesuremono4(:,1),'r')
hold on
% plot( Nmesuremono3(:),'y', 'linewidth',2)
% plot( Nmesuremono4(:),'g', 'linewidth',2)
% plot( Nmesuremono5(:),'c', 'linewidth',2)
plot(timeME70ml, Nmesuremulti(:,1), 'm' )
plot (timeME70ml, Nmesuremulti11(:,1), 'k' )
% plot(timeME70ml,Nmesuremultimoyennecentre(:,1),'+-','color',map(80,:))

nbESUmono=size(Nmesuremono2,1);
nbESUmulti=size(Nmesuremulti,1);

% plot ((1:1:nb_files),mean(Nmesuremono1(1:numel))*ones(1,nb_files),'--b')
plot (timeER60ml,mean(Nmesuremono4(:,1))*ones(1,nbESUmono),'--r','linewidth',2)
% plot ((1::),mean(Nmesuremono3(1:numel))*ones(1,nb_files),'--y')
% plot ((1::),mean(Nmesuremono4(1:numel))*ones(1,nb_files),'--g')
% plot ((1::),mean(Nmesuremono5(1:numel))*ones(1,nb_files),'--c')
plot (timeME70ml,mean(Nmesuremulti(:,1))*ones(1,nbESUmulti),'--m','linewidth',2)
plot (timeME70ml,mean(Nmesuremulti11(:,1))*ones(1,nbESUmulti),'--k','linewidth',2)
% plot(timeME70ml,mean(Nmesuremultimoyennecentre(:,1))*ones(1,nbESUmulti),'--','color',map(80,:),'linewidth',2)




% legend('mono 18KHz','moyenne mono 18KHz','mono 120KHz',' moyenne mono 120KHz','mono 70KHz','moyenne mono 70KHz','mono 120KHz','moyenne mono 120KHz','mono 200 KHz','moyenne mono 200 KHz','moyenne des 21 faisceaux multi','moyenne des sc�nes multi','faisceau central','moyenne des sc�nes faisceau central','3 faisceaux du centre','moyenne des sc�nes 3 faisceaux centraux','biomasse totale inject�e')
% legend('mono 18KHz','mono 120KHz','mono 70KHz','mono 120KHz','mono 200 KHz','moyenne des 21 faisceaux multi','faisceau central','3 faisceaux du centre','biomasse totale inject�e')
legend('mono 120KHz','mean multi','center multi')
title(['Abundance estimates: ', sprintf('15 to 30 m')])
datetickzoom('x','HH:MM');
ylabel('NASC (m2/mille2)')
xlabel('Heure')
% set(gca,'xtick',1:max(nbESUmono,nbESUmulti))
% set(gca,'xticklabel',time)


xticklabel_rotate

saveas(gcf,[chemin,sprintf('comparaison_sardine_mean')],'fig')

beams=[72 76 81 85 90 94 99 103 108 113 118 115 110 106 101 97 92 88 83 79 74];


figure;
surf([1:1:length(beams)],[1:1:length(timeME70ml)],squeeze(Sa_surface_ME70(:,1,:)))
set(gca,'Ytick',[1:25:length(timeME70ml)]);
set(gca,'YTickLabel',datestr(timeME70ml(1:25:end),'HH:MM'))
set(gca,'Xtick',[1:3:length(beams)]);
set(gca,'XTickLabel',num2str(beams(1:3:end)'))
% datetick('y','HH:MM');
xlabel('Frequency (kHz)');
ylabel('Time (HH:MM)');
zlabel('NASC (m2/mille2)');
saveas(gcf,[chemin,sprintf('comparaison_reel_NASCmulti_15to30m')],'fig')
saveas(gcf,[chemin,sprintf('comparaison_reel_NASCmulti _15to30m')],'jpg')


t11=Nmesuremono4(find(Nmesuremono4(1:min(nbESUmono,nbESUmulti),1)>seuil),1);
t21=Nmesuremulti(find(Nmesuremulti(1:min(nbESUmono,nbESUmulti),1)>seuil),1);
t31= Nmesuremulti11(find(Nmesuremulti11(1:min(nbESUmono,nbESUmulti),1)>seuil),1);

label(1:size(t11))={'mono 120KHz'};
label(size(t11)+1:size(t11)+1+size(t21))={'mean multi'};
label(size(t11)+1+size(t21)+1:size(t11)+1+size(t21)+1+size(t31))={'center multi'};
x = cat(1,t11,t21,t31);
anova1(x,label);
hold on
plot([mean(t11) mean(t21) mean(t31)],'r');
title(['Abundance estimates: ', sprintf('15 to 30 m')])
ylabel('NASC (m2/mille2)')
axis([0 4 0 2000])
saveas(gcf,[chemin,sprintf('comparaison_reel_15to30m')],'fig')
saveas(gcf,[chemin,sprintf('comparaison_reel_15to30m')],'jpg')



figure;
% plot( Nmesuremono1(:),'b', 'linewidth',2)

plot( timeER60ml,Nmesuremono4(:,2),'r')
hold on
% plot( Nmesuremono3(:),'y', 'linewidth',2)
% plot( Nmesuremono4(:),'g', 'linewidth',2)
% plot( Nmesuremono5(:),'c', 'linewidth',2)
plot(timeME70ml, Nmesuremulti(:,2), 'm' )
plot (timeME70ml, Nmesuremulti11(:,2), 'k' )
% plot(timeME70ml,Nmesuremultimoyennecentre(:,2),'+-','color',map(80,:))

nbESUmono=size(Nmesuremono2,1);
nbESUmulti=size(Nmesuremulti,1);

% plot ((1:1:nb_files),mean(Nmesuremono1(1:numel))*ones(1,nb_files),'--b')
plot (timeER60ml,mean(Nmesuremono4(:,2))*ones(1,nbESUmono),'--r','linewidth',2)
% plot ((1::),mean(Nmesuremono3(1:numel))*ones(1,nb_files),'--y')
% plot ((1::),mean(Nmesuremono4(1:numel))*ones(1,nb_files),'--g')
% plot ((1::),mean(Nmesuremono5(1:numel))*ones(1,nb_files),'--c')
plot (timeME70ml,mean(Nmesuremulti(:,2))*ones(1,nbESUmulti),'--m','linewidth',2)
plot (timeME70ml,mean(Nmesuremulti11(:,2))*ones(1,nbESUmulti),'--k','linewidth',2)
% plot(timeME70ml,mean(Nmesuremultimoyennecentre(:,2))*ones(1,nbESUmulti),'--','color',map(80,:),'linewidth',2)




% legend('mono 18KHz','moyenne mono 18KHz','mono 120KHz',' moyenne mono 120KHz','mono 70KHz','moyenne mono 70KHz','mono 120KHz','moyenne mono 120KHz','mono 200 KHz','moyenne mono 200 KHz','moyenne des 21 faisceaux multi','moyenne des sc�nes multi','faisceau central','moyenne des sc�nes faisceau central','3 faisceaux du centre','moyenne des sc�nes 3 faisceaux centraux','biomasse totale inject�e')
% legend('mono 18KHz','mono 120KHz','mono 70KHz','mono 120KHz','mono 200 KHz','moyenne des 21 faisceaux multi','faisceau central','3 faisceaux du centre','biomasse totale inject�e')
legend('mono 120KHz','mean multi','center multi')
title(['Abundance estimates: ', sprintf('30 to 90 m')])
datetickzoom('x','HH:MM');
ylabel('NASC (m2/mille2)')
xlabel('Heure')
% set(gca,'xtick',1:max(nbESUmono,nbESUmulti))
% set(gca,'xticklabel',time)


xticklabel_rotate

saveas(gcf,[chemin,sprintf('comparaison_sardine_mean')],'fig')

beams=[72 76 81 85 90 94 99 103 108 113 118 115 110 106 101 97 92 88 83 79 74];


figure;
surf([1:1:length(beams)],[1:1:length(timeME70ml)],squeeze(Sa_surface_ME70(:,2,:)))
set(gca,'Ytick',[1:25:length(timeME70ml)]);
set(gca,'YTickLabel',datestr(timeME70ml(1:25:end),'HH:MM'))
set(gca,'Xtick',[1:3:length(beams)]);
set(gca,'XTickLabel',num2str(beams(1:3:end)'))
% datetick('y','HH:MM');
xlabel('Frequency (kHz)');
ylabel('Time (HH:MM)');
zlabel('NASC (m2/mille2)');
saveas(gcf,[chemin,sprintf('comparaison_reel_NASCmulti_30to90m')],'fig')
saveas(gcf,[chemin,sprintf('comparaison_reel_NASCmulti _30to90m')],'jpg')


t11=Nmesuremono4(find(Nmesuremono4(1:min(nbESUmono,nbESUmulti),2)>seuil),2);
t21=Nmesuremulti(find(Nmesuremulti(1:min(nbESUmono,nbESUmulti),2)>seuil),2);
t31= Nmesuremulti11(find(Nmesuremulti11(1:min(nbESUmono,nbESUmulti),2)>seuil),2);

label(1:size(t11))={'mono 120KHz'};
label(size(t11)+1:size(t11)+1+size(t21))={'mean multi'};
label(size(t11)+1+size(t21)+1:size(t11)+1+size(t21)+1+size(t31))={'center multi'};
x = cat(1,t11,t21,t31);
anova1(x,label);
hold on
plot([mean(t11) mean(t21) mean(t31)],'r');
title(['Abundance estimates: ', sprintf('30 to 90 m')])
ylabel('NASC (m2/mille2)')
axis([0 4 0 1000])
saveas(gcf,[chemin,sprintf('comparaison_reel_30to90m')],'fig')
saveas(gcf,[chemin,sprintf('comparaison_reel_30to90m')],'jpg')



figure;
% plot( Nmesuremono1(:),'b', 'linewidth',2)

plot( timeER60ml,Nmesuremono4(:,3),'r')
hold on
% plot( Nmesuremono3(:),'y', 'linewidth',2)
% plot( Nmesuremono4(:),'g', 'linewidth',2)
% plot( Nmesuremono5(:),'c', 'linewidth',2)
plot(timeME70ml, Nmesuremultimoyennecentre(:,3), 'm' )
plot (timeME70ml, Nmesuremulti11(:,3), 'k' )
% plot(timeME70ml,Nmesuremultimoyennecentre(:,3),'+-','color',map(80,:))

nbESUmono=size(Nmesuremono2,1);
nbESUmulti=size(Nmesuremulti,1);

% plot ((1:1:nb_files),mean(Nmesuremono1(1:numel))*ones(1,nb_files),'--b')
plot (timeER60ml,mean(Nmesuremono4(:,3))*ones(1,nbESUmono),'--r','linewidth',2)
% plot ((1::),mean(Nmesuremono3(1:numel))*ones(1,nb_files),'--y')
% plot ((1::),mean(Nmesuremono4(1:numel))*ones(1,nb_files),'--g')
% plot ((1::),mean(Nmesuremono5(1:numel))*ones(1,nb_files),'--c')
plot (timeME70ml,mean(Nmesuremultimoyennecentre(:,3))*ones(1,nbESUmulti),'--m','linewidth',2)
plot (timeME70ml,mean(Nmesuremulti11(:,3))*ones(1,nbESUmulti),'--k','linewidth',2)
% plot(timeME70ml,mean(Nmesuremultimoyennecentre(:,3))*ones(1,nbESUmulti),'--','color',map(80,:),'linewidth',2)




% legend('mono 18KHz','moyenne mono 18KHz','mono 120KHz',' moyenne mono 120KHz','mono 70KHz','moyenne mono 70KHz','mono 120KHz','moyenne mono 120KHz','mono 200 KHz','moyenne mono 200 KHz','moyenne des 21 faisceaux multi','moyenne des sc�nes multi','faisceau central','moyenne des sc�nes faisceau central','3 faisceaux du centre','moyenne des sc�nes 3 faisceaux centraux','biomasse totale inject�e')
% legend('mono 18KHz','mono 120KHz','mono 70KHz','mono 120KHz','mono 200 KHz','moyenne des 21 faisceaux multi','faisceau central','3 faisceaux du centre','biomasse totale inject�e')
legend('mono 120KHz','mean multi','center multi')
title(['Abundance estimates: ', sprintf('90 to 130 m')])
datetickzoom('x','HH:MM');
ylabel('NASC (m2/mille2)')
xlabel('Heure')
% set(gca,'xtick',3:max(nbESUmono,nbESUmulti))
% set(gca,'xticklabel',time)


xticklabel_rotate

saveas(gcf,[chemin,sprintf('comparaison_sardine_mean')],'fig')

beams=[72 76 81 85 90 94 99 103 108 113 118 115 110 106 101 97 92 88 83 79 74];


figure;
surf([1:1:length(beams)],[1:1:length(timeME70ml)],squeeze(Sa_surface_ME70(:,3,:)))
set(gca,'Ytick',[1:25:length(timeME70ml)]);
set(gca,'YTickLabel',datestr(timeME70ml(1:25:end),'HH:MM'))
set(gca,'Xtick',[1:3:length(beams)]);
set(gca,'XTickLabel',num2str(beams(1:3:end)'))
% datetick('y','HH:MM');
xlabel('Frequency (kHz)');
ylabel('Time (HH:MM)');
zlabel('NASC (m2/mille2)');
saveas(gcf,[chemin,sprintf('comparaison_reel_NASCmulti_90to130m')],'fig')
saveas(gcf,[chemin,sprintf('comparaison_reel_NASCmulti _90to130m')],'jpg')


t11=Nmesuremono4(find(Nmesuremono4(1:min(nbESUmono,nbESUmulti),3)>seuil),3);
t21=Nmesuremulti(find(Nmesuremulti(1:min(nbESUmono,nbESUmulti),3)>seuil),3);
t31= Nmesuremulti11(find(Nmesuremultimoyennecentre(1:min(nbESUmono,nbESUmulti),3)>seuil),3);

label(1:size(t11))={'mono 120KHz'};
label(size(t11)+1:size(t11)+1+size(t21))={'mean multi'};
label(size(t11)+1+size(t21)+1:size(t11)+1+size(t21)+1+size(t31))={'center multi'};
x = cat(1,t11,t21,t31);
anova1(x,label);
hold on
plot([mean(t11) mean(t21) mean(t31)],'r');
title(['Abundance estimates: ', sprintf('90 to 130 m')])
ylabel('NASC (m2/mille2)')
axis([0 4 0 1000])
saveas(gcf,[chemin,sprintf('comparaison_reel_90to130m')],'fig')
saveas(gcf,[chemin,sprintf('comparaison_reel_90to130m')],'jpg')