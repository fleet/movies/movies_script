close all
clear all

chemin ='C:\simulation\evaluation biomasse mono multi\simulation\sardine\scene_2_1_030_20_1600_0\';
chemin2 ='C:\simulation\evaluation biomasse mono multi\simulation\sardine\scene_2_1_030_20_40_0\';

%Echo-int�gration (peut �tre comment� si elle a d�j� �t� ex�cut�e
% SampleEchoIntegration([chemin2,'\mono\']);
% clear functions;
% SampleEchoIntegration([chemin2,'\multi\']);
% clear functions;

% chargement des variables du programme generate_bancs.m

nbX=40;
nbY=40;

nbbancs = str2num(chemin(size(chemin,2)-15));
Masse=str2num(chemin(size(chemin,2)-17));                  %biomasse en tonnes
taille=20 ;                %taille du poisson en cm
Espece=0;    % 0:sardine  1:maquereau
if Espece==0
    TS = 20*log10(taille)-72.6;
elseif Espece==1
    TS = 20*log10(taille) - 84.9;
end
poids = calcul(taille);     %appel � la function calcul (fichier  calcul.m)
nbpoisson=Masse*1000000/poids;          %nombre total de poissons
offsetX=60;
VBat=(10*1.852)/3.6; %vitesse du bateau en m/s


AngleMono18 = 10;
AngleMono = 7;
AngleMulti = 90;
AngleMultiCentre = 3;
AngleMultiCentre3 = 9.5;
AngleMultiMax=6;

Profondeur = str2num(chemin(size(chemin,2)-13:size(chemin,2)-11));

if min(Profondeur)>=20 && min(Profondeur)<30
    distanceInterPing=0.35*VBat;
elseif min(Profondeur)>=30 && min(Profondeur)<50
    distanceInterPing=0.4*VBat;
elseif min(Profondeur)>=50 && min(Profondeur)<70
    distanceInterPing=0.45*VBat;
elseif min(Profondeur)>=70 && min(Profondeur)<90
    distanceInterPing=0.5*VBat;
elseif min(Profondeur)>=90 && min(Profondeur)<115
    distanceInterPing=0.6*VBat;
elseif min(Profondeur)>=115 && min(Profondeur)<140
    distanceInterPing=0.7*VBat;
elseif min(Profondeur)>=140 && min(Profondeur)<165
    distanceInterPing=0.8*VBat;
elseif min(Profondeur)>=165 && min(Profondeur)<190
    distanceInterPing=0.9*VBat;
elseif min(Profondeur)>=190 && min(Profondeur)<215
    distanceInterPing=1*VBat;
elseif min(Profondeur)>=215 && min(Profondeur)<240
    distanceInterPing=1.1*VBat;
elseif min(Profondeur)>=240 && min(Profondeur)<250
    distanceInterPing=1.2*VBat;
end

%�valuation biomas
sigma=4*pi*10^(TS/10);

D1=str2num(chemin(size(chemin,2)-9:size(chemin,2)-8));
nbpoisson=nbpoisson*(4/3*pi*D1/2*D1/2*2.5)/(D1*D1*5);

densite = nbpoisson/(4/3*pi*D1/2*D1/2*2.5);

% chargement des r�sultats

depth_bottom_ER60=[];
Sa_surface_ER60 = [];
depth_bottom_ME70=[];
Sa_surface_ME70 = [];

seuil=0;


filelist = searchfile(chemin,'scene_mono*.mat',1);  % ensemble des fichiers
nb_files_mono = size(filelist,2);  % nombre de fichiers
% sort(filelist);

nblER60=0;
for numfile = 1:nb_files_mono

    fullname = filelist(numfile).fullpath;
    load(fullname);
    matfilename = filelist(numfile).name;
    
    numsimu = str2num(strtok(matfilename(30:34),'_'));
    

    if size(Sa_surfER60,1)>0
        Sa_surface_ER60(nblER60+1:nblER60+size(Sa_surfER60,1),:) = Sa_surfER60(:,1,:);

        kx = 1+floor((numsimu-1)/nbY);
        ky = numsimu - (kx-1)*nbY;
        centrexER60(nblER60+1:nblER60+size(Sa_surfER60,1)) = offsetX + D1/2+ (2*kx-nbX)*(D1/nbX);
        centreyER60(nblER60+1:nblER60+size(Sa_surfER60,1)) =   (2*ky-nbY)*(D1/nbY);

        latER60(nblER60+1:nblER60+size(Sa_surfER60,1),:) = Lat_surfER60(:,1,:);
        longER60(nblER60+1:nblER60+size(Sa_surfER60,1),:) = Long_surfER60(:,1,:);

        nblER60=size(Sa_surface_ER60,1);
    end

    clear  Sa_surfER60;


end
% 
filelist = searchfile(chemin2,'scene_mono*.mat',1);  % ensemble des fichiers
nb_files_mono = size(filelist,2);  % nombre de fichiers
% sort(filelist);
nbX=10;
nbY=4;
for numfile = 1:nb_files_mono

    fullname = filelist(numfile).fullpath;
    load(fullname);
    matfilename = filelist(numfile).name;
    
    numsimu = str2num(strtok(matfilename(30:34),'_'));
    

    if size(Sa_surfER60,1)>0
        Sa_surface_ER60(nblER60+1:nblER60+size(Sa_surfER60,1),:) = Sa_surfER60(:,1,:);

        kx = 1+floor((numsimu-1)/nbY);
        ky = numsimu - (kx-1)*nbY;
        centrexER60(nblER60+1:nblER60+size(Sa_surfER60,1)) = offsetX + D1/2+ (2*kx-nbX)*(2/nbX);
        centreyER60(nblER60+1:nblER60+size(Sa_surfER60,1)) =   (2*ky-nbY)*(0.5/nbY);

        latER60(nblER60+1:nblER60+size(Sa_surfER60,1),:) = Lat_surfER60(:,1,:);
        longER60(nblER60+1:nblER60+size(Sa_surfER60,1),:) = Long_surfER60(:,1,:);

        nblER60=size(Sa_surface_ER60,1);
    end

    clear  Sa_surfER60;


end


Samesuremono=Sa_surface_ER60(:,4);

filelist = searchfile(chemin,'scene_multi*.mat',1);  % ensemble des fichiers
nb_files_multi = size(filelist,2);  % nombre de fichiers
% sort(filelist);

nbX=40;
nbY=40;

nblME70=0;
for numfile = 1:nb_files_multi  % boucle sur

    fullname = filelist(numfile).fullpath;
    load(fullname);
    matfilename = filelist(numfile).name;
    
    numsimu = str2num(strtok(matfilename(31:35),'_'));

    if size(Sa_surfME70,1)>0
        Sa_surface_ME70(nblME70+1:nblME70+size(Sa_surfME70,1),:) = Sa_surfME70(:,1,:);
        Samesuremulti(nblME70+1:nblME70+size(Sa_surfME70,1),:) = (sum(Sa_surface_ME70(nblME70+1:nblME70+size(Sa_surfME70,1),:)*Volume_surfME70(1,:)',2))/sum(Volume_surfME70(1,:));
        Samesuremulti11(nblME70+1:nblME70+size(Sa_surfME70,1),:) = Sa_surface_ME70(nblME70+1:nblME70+size(Sa_surfME70,1),11);
        Samesuremultimoyennecentre(nblME70+1:nblME70+size(Sa_surfME70,1),:,:) = (sum(Sa_surface_ME70(nblME70+1:nblME70+size(Sa_surfME70,1),10:12)*Volume_surfME70(1,10:12)',2))/sum(Volume_surfME70(1,10:12));

        kx = 1+floor((numsimu-1)/nbY);
        ky = numsimu - (kx-1)*nbY;
        centrexME70(nblME70+1:nblME70+size(Sa_surfME70,1)) = offsetX + D1/2+ (2*kx-nbX)*(D1/nbX);
        centreyME70(nblME70+1:nblME70+size(Sa_surfME70,1)) =  (2*ky-nbY)*(D1/nbY);


        latME70(nblME70+1:nblME70+size(Sa_surfME70,1),:) = Lat_surfME70(:,1,:);
        longME70(nblME70+1:nblME70+size(Sa_surfME70,1),:) = Long_surfME70(:,1,:);
        

        nblME70=size(Sa_surface_ME70,1);
    end

    clear  Depth_botME70  Sa_botME70 Sa_surfME70 Volume_surfME70;
end


filelist = searchfile(chemin2,'scene_multi*.mat',1);  % ensemble des fichiers
nb_files_multi = size(filelist,2);  % nombre de fichiers
% sort(filelist);

nbX=10;
nbY=4;
for numfile = 1:nb_files_multi  % boucle sur

    fullname = filelist(numfile).fullpath;
    load(fullname);
    matfilename = filelist(numfile).name;
    
    numsimu = str2num(strtok(matfilename(31:35),'_'));

    if size(Sa_surfME70,1)>0
        Sa_surface_ME70(nblME70+1:nblME70+size(Sa_surfME70,1),:) = Sa_surfME70(:,1,:);
        Samesuremulti(nblME70+1:nblME70+size(Sa_surfME70,1),:) = (sum(Sa_surface_ME70(nblME70+1:nblME70+size(Sa_surfME70,1),:)*Volume_surfME70(1,:)',2))/sum(Volume_surfME70(1,:));
        Samesuremulti11(nblME70+1:nblME70+size(Sa_surfME70,1),:) = Sa_surface_ME70(nblME70+1:nblME70+size(Sa_surfME70,1),11);
        Samesuremultimoyennecentre(nblME70+1:nblME70+size(Sa_surfME70,1),:,:) = (sum(Sa_surface_ME70(nblME70+1:nblME70+size(Sa_surfME70,1),10:12)*Volume_surfME70(1,10:12)',2))/sum(Volume_surfME70(1,10:12));

        kx = 1+floor((numsimu-1)/nbY);
        ky = numsimu - (kx-1)*nbY;
        centrexME70(nblME70+1:nblME70+size(Sa_surfME70,1)) = offsetX + D1/2+ (2*kx-nbX)*(2/nbX);
        centreyME70(nblME70+1:nblME70+size(Sa_surfME70,1)) =  (2*ky-nbY)*(0.5/nbY);


        latME70(nblME70+1:nblME70+size(Sa_surfME70,1),:) = Lat_surfME70(:,1,:);
        longME70(nblME70+1:nblME70+size(Sa_surfME70,1),:) = Long_surfME70(:,1,:);
        

        nblME70=size(Sa_surface_ME70,1);
    end

    clear  Depth_botME70  Sa_botME70 Sa_surfME70 Volume_surfME70;
end

xER60=(latER60(:,4)-latER60(1,4))*60.0*1852.0+25;
yER60=(longER60(:,4)-longER60(1,4))*60.0*1852.0;

xER60=xER60'-centrexER60;
yER60= yER60'-centreyER60;

xME70=(latME70(:,11)-latME70(1,4))*60.0*1852.0+25;
yME70=(longME70(:,11)-longME70(1,11))*60.0*1852.0+Profondeur*tan(0.5*pi/180);

xME70=xME70'-centrexME70;
yME70= yME70'-centreyME70;


axismin=min(min(min(Samesuremono),min(min(Samesuremulti11))));
axismax=max(max(max(Samesuremono),max(max(Samesuremulti11))));

figure
plot(Sa_surface_ER60(:,4),'r');
hold on
plot(Sa_surface_ME70(:,11),'b');
caxis([axismin axismax])



precision=0.5;


indexER60=find(abs(xER60)<D1 & abs(yER60) < D1 & Samesuremono'>0);
distanceER60=(xER60(indexER60).^2+yER60(indexER60).^2).^0.5;
SaER60=Samesuremono(indexER60);


SaD_ER60=zeros(D1/precision+1,3);
Satruemono=zeros(D1/precision+1,1);
i=0;
for d=0:precision:D1
    SaD_ER60(D1/precision+1+i,:)=prctile(SaER60(abs(distanceER60-d)<precision/2),[5 50 95]);
    SaD_ER60(D1/precision+1-i,:)=prctile(SaER60(abs(distanceER60-d)<precision/2),[5 50 95]);
    if (d<D1/2)
        Satruemono(D1/precision+1+i)=1852*1852*nbpoisson*sigma/(pi*D1*D1/4);
        Satruemono(D1/precision+1-i)=1852*1852*nbpoisson*sigma/(pi*D1*D1/4);
    else
        Satruemono(D1/precision+1+i)=0;
        Satruemono(D1/precision+1-i)=0;
    end
    i=i+1;
end

SaD_ER60(isnan(SaD_ER60))=0;

indexME70=find(abs(xME70)<D1 & abs(yME70) < D1 & Samesuremulti11'>0);
distanceME70=(xME70(indexME70').^2+yME70(indexME70').^2).^0.5;
SaME70=Samesuremulti11(indexME70);


SaD_ME70=zeros(D1/precision+1,3);
i=0;
for d=0:precision:D1
    SaD_ME70(D1/precision+1+i,:)=prctile(SaME70(abs(distanceME70-d)<precision/2),[5 50 95]);
    SaD_ME70(D1/precision+1-i,:)=prctile(SaME70(abs(distanceME70-d)<precision/2),[5 50 95]);
    i=i+1;
end

SaD_ME70(isnan(SaD_ME70))=0;

% SaD_ER60=convolve(SaD_ER60);
% SaD_ME70=convolve(SaD_ME70);


figure
plot([-D1:precision:D1],SaD_ER60,'r-')
hold on
plot([-D1:precision:D1],SaD_ME70,'b-')
plot([-D1:precision:D1],Satruemono,'k','LineWidth',2);
caxis([axismin axismax])
xlabel('Distance to the center of the school (m)');
ylabel('Sa (m2/mille2)');
title(['Theoretical and simulated Sa, one school ',sprintf('of %g m at depth of %g m, %g sardines/m3',D1,round(Profondeur),densite)])
legend('ER60 5% percentile','ER60 50% percentile','ER60 95% percentile','ME70 5% percentile','ME70 50% percentile','ME70 95% percentile')
saveas(gcf,[chemin(1:size(chemin,2)-23),chemin(size(chemin,2)-22:end-1),'_','Sa'],'fig')
saveas(gcf,[chemin(1:size(chemin,2)-23),chemin(size(chemin,2)-22:end-1),'_','Sa'],'jpg')

SaER60_area=trapz(SaD_ER60);
SaME70_area=trapz(SaD_ME70);
Satrue_area=trapz(Satruemono);

(SaER60_area(2)-Satrue_area)/Satrue_area
(SaME70_area(2)-Satrue_area)/Satrue_area




precisionER60x=1;
precisionER60y=1;



XER60 = -D1:precisionER60x:D1;
YER60 = -D1:precisionER60y:D1;

SaiER60=zeros(size(XER60,2),size(YER60,2),nbX);
XiER60=zeros(size(XER60,2),size(YER60,2),nbX);
YiER60=zeros(size(XER60,2),size(YER60,2),nbX);

pER60=ones(size(XER60,2),size(YER60,2));

for l=1:size(yER60,2)
    if(abs(xER60(l))<D1 && abs(yER60(l)) < D1 && Samesuremono(l)>0)
        m=find(XER60>xER60(l)-precisionER60x/2 & XER60<=xER60(l)+precisionER60x/2);
        n=find(YER60>yER60(l)-precisionER60y/2 & YER60<=yER60(l)+precisionER60y/2);
        SaiER60(m,n,pER60(m,n)) = Samesuremono(l);
        XiER60(m,n,pER60(m,n)) = xER60(l);
        YiER60(m,n,pER60(m,n)) = yER60(l);
        pER60(m,n)=pER60(m,n)+1;
    end
end


SaER60=zeros(size(XER60,2),size(YER60,2));
nbER60=zeros(size(XER60,2),size(YER60,2));
for m = 1:size(XER60,2)
    for n = 1:size(YER60,2)
        indexER60=find(SaiER60(m,n,:)>0);
        if indexER60
            SaER60(m,n)=mean(squeeze(SaiER60(m,n,indexER60)));
            nbER60(m,n)=size(SaiER60(m,n,:),1);
        end
    end
end





precisionME70x=1;
precisionME70y=1;

%  centreyME70 = centreyME70 -mean(centreyME70);



XME70 = -D1:precisionME70x:D1;
YME70 = -D1:precisionME70y:D1;

SaiME70=zeros(size(XME70,2),size(YME70,2),nbX);
XiME70=zeros(size(XME70,2),size(YME70,2),nbX);
YiME70=zeros(size(XME70,2),size(YME70,2),nbX);

pME70=ones(size(XME70,2),size(YME70,2));

for l=1:size(yME70,1)
    for b=11:11
        if(abs(xME70(l,b))<D1 && abs(yME70(l,b)) < D1 && Sa_surface_ME70(l,b)>0)
            m=find(XME70>xME70(l,b)-precisionME70x/2 & XME70<=xME70(l,b)+precisionME70x/2);
            n=find(YME70>yME70(l,b)-precisionME70y/2 & YME70<=yME70(l,b)+precisionME70y/2);
            SaiME70(m,n,pME70(m,n)) = Sa_surface_ME70(l,b);
            XiME70(m,n,pME70(m,n)) = xME70(l,b);
            YiME70(m,n,pME70(m,n)) = yME70(l,b);
            pME70(m,n)=pME70(m,n)+1;
        end
    end
end

SaME70=zeros(size(XME70,2),size(YME70,2));
nbME70=zeros(size(XME70,2),size(YME70,2));
for m = 1:size(XME70,2)
    for n = 1:size(YME70,2)
        indexME70=find(SaiME70(m,n,:)>0);
        if indexME70
            SaME70(m,n)=mean(squeeze(SaiME70(m,n,indexME70)));
            nbME70(m,n)=size(indexME70,1);
        end
    end
end

