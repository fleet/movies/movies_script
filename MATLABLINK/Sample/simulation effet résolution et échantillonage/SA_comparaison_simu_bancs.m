close all
clear all

chemin ='C:\simulation\evaluation biomasse mono multi\simulation\sardine\scene_8_1_100_20_400_0\';

%Echo-int�gration (peut �tre comment� si elle a d�j� �t� ex�cut�e
% SampleEchoIntegration([chemin,'\mono\']);
% clear functions;
% SampleEchoIntegration([chemin,'\multi\']);
% clear functions;

% chargement des variables du programme generate_bancs.m

nbX=1;
nbY=400;

nbbancs = str2num(chemin(size(chemin,2)-14));
Masse=str2num(chemin(size(chemin,2)-16));                  %biomasse en tonnes
taille=20 ;                %taille du poisson en cm
Espece=0;    % 0:sardine  1:maquereau
if Espece==0
    TS = 20*log10(taille)-72.6;
elseif Espece==1
    TS = 20*log10(taille) - 84.9;
end
poids = calcul(taille);     %appel � la function calcul (fichier  calcul.m)
nbpoisson=Masse*1000000/poids;          %nombre total de poissons
offsetX=60;
VBat=(10*1.852)/3.6; %vitesse du bateau en m/s

AngleMono18 = 10;
AngleMono = 7;
AngleMulti = 90;
AngleMultiCentre = 3;
AngleMultiCentre3 = 9.5;
AngleMultiMax=6;

Profondeur = str2num(chemin(size(chemin,2)-12:size(chemin,2)-10));

if min(Profondeur)>=20 && min(Profondeur)<30
    distanceInterPing=0.35*VBat;
elseif min(Profondeur)>=30 && min(Profondeur)<50
    distanceInterPing=0.4*VBat;
elseif min(Profondeur)>=50 && min(Profondeur)<70
    distanceInterPing=0.45*VBat;
elseif min(Profondeur)>=70 && min(Profondeur)<90
    distanceInterPing=0.5*VBat;
elseif min(Profondeur)>=90 && min(Profondeur)<115
    distanceInterPing=0.6*VBat;
elseif min(Profondeur)>=115 && min(Profondeur)<140
    distanceInterPing=0.7*VBat;
elseif min(Profondeur)>=140 && min(Profondeur)<165
    distanceInterPing=0.8*VBat;
elseif min(Profondeur)>=165 && min(Profondeur)<190
    distanceInterPing=0.9*VBat;
elseif min(Profondeur)>=190 && min(Profondeur)<215
    distanceInterPing=1*VBat;
elseif min(Profondeur)>=215 && min(Profondeur)<240
    distanceInterPing=1.1*VBat;
elseif min(Profondeur)>=240 && min(Profondeur)<250
    distanceInterPing=1.2*VBat;
end

%�valuation biomas
sigma=4*pi*10^(TS/10);

D1=str2num(chemin(size(chemin,2)-8:size(chemin,2)-7));

nbpoisson=nbpoisson*(4/3*pi*D1/2*D1/2*2.5)/(D1*D1*5);

% chargement des r�sultats

depth_bottom_ER60=[];
Sa_surface_ER60 = [];
depth_bottom_ME70=[];
Sa_surface_ME70 = [];

seuil=0;


filelist = searchfile(chemin,'scene_mono*.mat',1);  % ensemble des fichiers
nb_files_mono = size(filelist,2);  % nombre de fichiers
% sort(filelist);

nblER60=1;
for numfile = 1:nb_files_mono

    fullname = filelist(numfile).fullpath;
    load(fullname);
    matfilename = filelist(numfile).name;
  
    if size(Sa_surfER60,1)>0
       Samesuremono(nblER60) = mean(Sa_surfER60(:,1,4));  
       nblER60=nblER60+1;
    end

    clear  Sa_surfER60;


end


filelist = searchfile(chemin,'scene_multi*.mat',1);  % ensemble des fichiers
nb_files_multi = size(filelist,2);  % nombre de fichiers
% sort(filelist);


nblME70=1;
for numfile = 1:nb_files_multi  % boucle sur

    fullname = filelist(numfile).fullpath;
    load(fullname);
    matfilename = filelist(numfile).name;
  
    if size(Sa_surfME70,1)>0

        Samesuremulti(nblME70) = mean((sum(squeeze(Sa_surfME70(:,1,:))*Volume_surfME70(1,:)',2))/sum(Volume_surfME70(1,:)));
%     Samesuremulti(nblME70)=mean(mean(Sa_surfME70(:,1,:)));
        nblME70=nblME70+1;
    end

    clear  Depth_botME70  Sa_botME70 Sa_surfME70 Volume_surfME70;
end


area_prosp=((150)*(2*tan(((AngleMulti*pi)/180)/2)*Profondeur));
% area=(133)^2;

SaTrue=(1852*1852*nbpoisson*sigma/(area_prosp));

bER60=(Samesuremono-SaTrue)/SaTrue;
bME70=(Samesuremulti-SaTrue)/SaTrue;

% bER60=Sa_surface_ER60(:,4);
% bME70=Samesuremulti;

% bER60=convolve(bER60);
% bME70=convolve(bME70);

figure
plot(bER60,'r');
hold on
plot(bME70,'b');
legend('ER60','ME70');
% caxis([axismin axismax])

figure;
[a,b]=hist(bER60,20);
bar(b,a/trapz(b,a),'r');hold on;
[a,b]=hist(bME70,20);
bar(b,a/trapz(b,a),'b')
legend('ER60','ME70');

bstarER60=bootrsp(bER60,1000);
bstarME70=bootrsp(bME70,1000);

muER60=mean(bstarER60);
muME70=mean(bstarME70);

sER60=std(bstarER60);
sME70=std(bstarME70);


figure;
[a,b]=hist(muER60,20);
bar(b,a/trapz(b,a),'r');hold on;
[a,b]=hist(muME70,20);
bar(b,a/trapz(b,a),'b')
legend('ER60','ME70');
% plot(-1:0.01:1,normpdf(-1:0.01:1,0,1/sqrt(20)),'r');hold off        
xlabel('Mean')
ylabel('Density Function')

figure;
[a,b]=hist(sER60,20);
bar(b,a/trapz(b,a),'r');hold on;
[a,b]=hist(sME70,20);
bar(b,a/trapz(b,a),'b')
legend('ER60','ME70');
% plot(-1:0.01:1,normpdf(-1:0.01:1,0,1/sqrt(20)),'r');hold off        
xlabel('Std')
ylabel('Density Function')


for i=1:1000
    for k=1:nbY
            bER60_mean(i,k)=mean(bER60(ceil(nbY.*rand(k,1))));
            bME70_mean(i,k)=mean(bME70(ceil(nbY.*rand(k,1))));
    end
end

figure
plot(mean(bER60_mean),'r')
hold on;
plot(mean(bME70_mean),'b')
xlabel('Nb schools');

figure
area(-2*std(bER60_mean)'+mean(bER60_mean(:,400)),'FaceColor','r')
hold on;
area(2*std(bER60_mean)'-mean(bER60_mean(:,400)),'FaceColor','r')
area(-2*std(bME70_mean)'+mean(bME70_mean(:,400)),'FaceColor','b')
area(2*std(bME70_mean)'-mean(bME70_mean(:,400)),'FaceColor','b')
plot(mean(bER60_mean),'k','LineWidth',2)
plot(mean(bME70_mean),'k','LineWidth',2)
axis([1 400 -1 1])
grid on;
% legend('ER60','','ME70');
xlabel('Nb schools');
ylabel('Mean nomalised bias in fish density estimates')
% plot(std(bME70_mean),'b')
saveas(gcf,[chemin(1:size(chemin,2)-23),chemin(size(chemin,2)-22:end-1),'_','Bias'],'fig')
saveas(gcf,[chemin(1:size(chemin,2)-23),chemin(size(chemin,2)-22:end-1),'_','Bias'],'jpg')



