function SampleEchoIntegration(chemin_ini)




% Chargement de la bonne configuration
% Attention : a charger avant le OpenHAC
% sinon on a une config qui change en cours de
% route
chemin_ini_reverse = strrep(chemin_ini, '\', '/');
moLoadConfig(chemin_ini_reverse);
%  moLoadConfig('C:/simulation/evaluation biomasse mono multi/simulation/sardine');
% 
% % Taille des blocs
% taille_bloc = 200;
% 
% ParameterKernel= moKernelParameter();
% ParameterDef=moLoadKernelParameter(ParameterKernel);
% 
% ParameterDef.m_nbPingFanMax=taille_bloc;
% ParameterDef.m_bAutoLengthEnable=0;
% ParameterDef.m_bAutoDepthEnable=1;
% ParameterDef.m_MaxRange = 200;
% ParameterDef.m_bIgnorePhaseData=1;
% 
% moSaveKernelParameter(ParameterKernel,ParameterDef);
% clear ParameterKernel;
% clear ParameterDef;
% 
% ParameterR= moReaderParameter();
% ParameterDef=moLoadReaderParameter(ParameterR);
% 
% ParameterDef.m_chunkNumberOfPingFan=taille_bloc;
% 
% moSaveReaderParameter(ParameterR,ParameterDef);
% clear ParameterR;
% clear ParameterDef;


filelist = ls([chemin_ini,'*.hac']);  % ensemble des fichiers
nb_files = size(filelist,1);  % nombre de fichiers


for numfile = 1:nb_files  % boucle sur l'ensemble des fichiers � traiter

    hacfilename = filelist(numfile,:);
    FileName = [chemin_ini,hacfilename];

        EchoIntegrModule = moEchoIntegration();
    moSetEchoIntegrationEnabled(EchoIntegrModule);
        
    moOpenHac(FileName);

    num_bloc=1;

    FileStatus= moGetFileStatus;
    while FileStatus.m_StreamClosed < 1
        hacfilename
        display(['bloc numero ',num2str(num_bloc)])

        % init matrices
        time_ER60 = [];
        time_ME70 = [];
        Sa_surfME70 =[];
        Sa_botME70=[];
        Sa_surfER60=[];
        Sa_botER60=[];
        Sv_surfME70=[];
        Sv_botME70=[];
        Sv_surfER60=[];
        Sv_botER60=[];
        Lat_surfME70=[];
        Long_surfME70=[];
        Depth_surfME70=[];
        Lat_surfER60=[];
        Long_surfER60=[];
        Depth_surfER60=[];
        Lat_botME70=[];
        Long_botME70=[];
        Depth_botME70=[];
        Lat_botER60=[];
        Long_botER60=[];
        Depth_botER60=[];
        Volume_surfME70=[];
        Volume_surfER60=[];
        Volume_botME70=[];
        Volume_botER60=[];
        AllData=[];



        moReadChunk();


        AllData=moGetOutPutList(EchoIntegrModule);

        % r�cup�ration de la d�finition des ESU
        ESUParam = moGetESUDefinition();

        % r�cup�ration de la d�finition des couches
        LayersDef = moGetLayersDefinition(EchoIntegrModule);
        
        % r�cup�ration des volumes de chaque couche (pour 1 ping)
        LayersVolumes = moGetLayersVolumes(EchoIntegrModule);
        sounderNb = size(LayersVolumes(1).Sounders,2);
        indexSounderME70=0;
        indexSounderER60=0;
        for indexSounder=1:sounderNb
            if size(LayersVolumes(1).Sounders(indexSounder).Channels,2) > 6
                indexSounderME70=indexSounder;
            else
                indexSounderER60=indexSounder;
            end
        end
        
   


        indexESUME70 = 1;
        indexESUER60 = 1;
        nbResults = size(AllData,2);
        for indexResults=1:nbResults
            if size(AllData(1,indexResults).m_Sa,2)>6

                time_ME70(indexESUME70) = AllData(1,indexResults).m_time/86400 +719529;
                nbLayer= size(AllData(1,indexResults).m_Sa,1);
                indexLayerSurface = 1;
                indexLayerBottom = 1;

                for indexLayer=1:nbLayer
                    if LayersDef.LayerType(indexLayer)==0
                        Sa_surfME70(indexESUME70,indexLayerSurface,:)=max(0,AllData(1,indexResults).m_Sa(indexLayer,:));
                        Sv_surfME70(indexESUME70,indexLayerSurface,:)=max(-100,AllData(1,indexResults).m_Sv(indexLayer,:));
                        Lat_surfME70(indexESUME70,indexLayerSurface,:)=AllData(1,indexResults).m_latitude(indexLayer,:);
                        Long_surfME70(indexESUME70,indexLayerSurface,:)=AllData(1,indexResults).m_longitude(indexLayer,:);
                        Depth_surfME70(indexESUME70,indexLayerSurface,:)=AllData(1,indexResults).m_Depth(indexLayer,:);
                        Volume_surfME70(indexESUME70,indexLayerSurface,:)=LayersVolumes(indexLayer).Sounders(indexSounderME70).Channels(:);
                        indexLayerSurface = indexLayerSurface+1;
                    else
                        Sa_botME70(indexESUME70,indexLayerBottom,:)=max(0,AllData(1,indexResults).m_Sa(indexLayer,:));
                        Sv_botME70(indexESUME70,indexLayerBottom,:)=max(-100,AllData(1,indexResults).m_Sv(indexLayer,:));
                        Lat_botME70(indexESUME70,indexLayerBottom,:)=AllData(1,indexResults).m_latitude(indexLayer,:);
                        Long_botME70(indexESUME70,indexLayerBottom,:)=AllData(1,indexResults).m_longitude(indexLayer,:);
                        Depth_botME70(indexESUME70,indexLayerBottom,:)=AllData(1,indexResults).m_Depth(indexLayer,:);
                        Volume_botME70(indexESUME70,indexLayerBottom,:)=LayersVolumes(indexLayer).Sounders(indexSounderME70).Channels(:);
                        indexLayerBottom = indexLayerBottom+1;
                    end;

                end;


                indexESUME70=indexESUME70+1;
            elseif size(AllData(1,indexResults).m_Sa,2)==5
                time_ER60(indexESUER60) = AllData(1,indexResults).m_time/86400 +719529;
                
                nbLayer= size(AllData(1,indexResults).m_Sa,1);
                indexLayerSurface = 1;
                indexLayerBottom = 1;

                for indexLayer=1:nbLayer
                    if LayersDef.LayerType(indexLayer)==0
                        Sa_surfER60(indexESUER60,indexLayerSurface,:)=max(0,AllData(1,indexResults).m_Sa(indexLayer,:));
                        Sv_surfER60(indexESUER60,indexLayerSurface,:)=max(-100,AllData(1,indexResults).m_Sv(indexLayer,:));
                        Lat_surfER60(indexESUER60,indexLayerSurface,:)=AllData(1,indexResults).m_latitude(indexLayer,:);
                        Long_surfER60(indexESUER60,indexLayerSurface,:)=AllData(1,indexResults).m_longitude(indexLayer,:);
                        Depth_surfER60(indexESUER60,indexLayerSurface,:)=AllData(1,indexResults).m_Depth(indexLayer,:);
                        Volume_surfER60(indexESUER60,indexLayerSurface,:)=LayersVolumes(indexLayer).Sounders(indexSounderER60).Channels(:);
                        indexLayerSurface = indexLayerSurface+1;
                    else
                        Sa_botER60(indexESUER60,indexLayerBottom,:)=max(0,AllData(1,indexResults).m_Sa(indexLayer,:));
                        Sv_botER60(indexESUER60,indexLayerBottom,:)=max(-100,AllData(1,indexResults).m_Sv(indexLayer,:));
                        Lat_botER60(indexESUER60,indexLayerBottom,:)=AllData(1,indexResults).m_latitude(indexLayer,:);
                        Long_botER60(indexESUER60,indexLayerBottom,:)=AllData(1,indexResults).m_longitude(indexLayer,:);
                        Depth_botER60(indexESUER60,indexLayerBottom,:)=AllData(1,indexResults).m_Depth(indexLayer,:);
                        Volume_botER60(indexESUER60,indexLayerBottom,:)=LayersVolumes(indexLayer).Sounders(indexSounderER60).Channels(:);
                        indexLayerBottom = indexLayerBottom+1;
                    end;
                end;


                indexESUER60=indexESUER60+1;

            end;


        end

        save ([chemin_ini, hacfilename(1:findstr('.',hacfilename)-1),'_', num2str(num_bloc),'_EI'],'time_ER60','time_ME70','Sa_surfME70', 'Sa_botME70', 'Sa_surfER60', 'Sa_botER60', 'Sv_surfME70', 'Sv_botME70', 'Sv_surfER60', 'Sv_botER60','Lat_surfME70', 'Long_surfME70', 'Depth_surfME70','Lat_surfER60','Long_surfER60','Depth_surfER60','Lat_botME70', 'Long_botME70', 'Depth_botME70','Lat_botER60','Long_botER60','Depth_botER60','Volume_surfER60','Volume_botER60','Volume_surfME70','Volume_botME70') ;
        clear time_ER60 time_ME70 Sa_surfME70 Sa_botME70 Sa_surfER60 Sa_botER60 Sv_surfME70 Sv_botME70 Sv_surfER60 Sv_botER60 Lat_surfME70 Long_surfME70 Depth_surfME70 Lat_surfER60 Long_surfER60 Depth_surfER60 Lat_botME70 Long_botME70 Depth_botME70 Lat_botER60 Long_botER60 Depth_botER60
        clear AllData;
        clear Volume_surfER60 Volume_botER60 Volume_surfME70 Volume_botME70;

        num_bloc=num_bloc+1;
 
        FileStatus= moGetFileStatus();
    end
    clear EchoIntegrModule;

end;
