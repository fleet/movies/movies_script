function [poids] = calcul(taille) %interpolation de la taille du poisson en fonction de sa masse (cas:sardine)
poids = 0.438*taille^2 - 8.1566*taille + 47.134;