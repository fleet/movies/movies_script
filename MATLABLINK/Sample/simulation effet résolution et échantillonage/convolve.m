%-------------------------------------------------------------------------
%-------------------------------------------------------------------------
%-------------------------------------------------------------------------
function Output = convolve(Input)

Kernel=[2 4 5 4 2;4 9 12 9 4;5 12 15 12 5;4 9 12 9 4;2 4 5 4 2];

XSizeFilter = fix(size(Kernel,2)/2);
YSizeFilter = fix(size(Kernel,1)/2);

temp([1:size(Input,1)+YSizeFilter*2],[1:size(Input,2)+XSizeFilter*2]) = mean(mean(Input));

for j=YSizeFilter+1:size(Input,1)+YSizeFilter
    for i = XSizeFilter+1:size(Input,2)+XSizeFilter
        temp(j,i) = Input(j-YSizeFilter,i-XSizeFilter);
    end
end

for i=XSizeFilter+1:size(temp,2)-XSizeFilter
    for j=YSizeFilter+1:size(temp,1)-YSizeFilter
        Output(j-YSizeFilter,i-XSizeFilter) = (sum(sum(temp([j-YSizeFilter:j+YSizeFilter],[i-XSizeFilter:i+XSizeFilter]).*Kernel))/sum(sum(Kernel)));
    end
end

