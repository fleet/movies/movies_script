function [x, y, flag] = latlon2xy(Carto, lat, lon)

flag = 1;
E  = double(Carto.Ellipsoide.Excentricite);
E2 = E ^ 2;
A  = double(Carto.Ellipsoide.DemiGrandAxe);
deg2rad = pi / 180;

x = NaN(size(lon), 'double');
y = NaN(size(lon), 'double');

L1   = double(Carto.Projection.Lambert.first_paral)     * deg2rad;
L2   = double(Carto.Projection.Lambert.second_paral)    * deg2rad;
G0   = double(Carto.Projection.Lambert.long_merid_orig) * deg2rad;
X0   = double(Carto.Projection.Lambert.X0);
Y0   = double(Carto.Projection.Lambert.Y0);
RK   = double(Carto.Projection.Lambert.scale_factor);
N1   = A / sqrt(1 - (E2 * sin(L1) ^ 2));
N2   = A / sqrt(1 - (E2 * sin(L2) ^ 2));
ISO1 = tan(L1/2+pi/4)*((1-E*sin(L1))/(1+E*sin(L1)))^(E/2);
ISO2 = tan(L2/2+pi/4)*((1-E*sin(L2))/(1+E*sin(L2)))^(E/2);
L12  = (L1 + L2) / 2;
R0   = A / sqrt(1 - E2*sin(L12)^2) / sin(L12) * cos(L12) * RK;
N    = log((N2*cos(L2)) / (N1 * cos(L1))) / log(ISO1/ISO2);
C    = N1 * cos(L1) / N * ISO1^N;

for i=1:size(lon,1)
    G = lon(i,:) * deg2rad;
    L = lat(i,:) * deg2rad;
    ISO  = tan(pi/4 + L/2) * ((1-E*sin(L)) / (1+E*sin(L))) ^ (E/2);

    IsoN   = ISO .^ N;
    NG_G0  = N .* (G - G0);
    x(i,:) =      C .* sin(NG_G0) ./ IsoN + X0;
    y(i,:) = R0 - C .* cos(NG_G0) ./ IsoN + Y0;
end
