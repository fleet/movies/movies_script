
%% PCA on ME70 shoal descriptors w/o extreme shoal

%Xs=multishl60s(time_ME70_60sf~=time_ME70_60sf(317),:);
Xs=multishl60(1:50,:);

Xsstd=(Xs-repmat(mean(Xs,1),size(Xs,1),1))./repmat(std(Xs,1),size(Xs,1),1);

[coefs2,scores2,variances2,t2] = princomp(Xsstd);

percent_explained2 = 100*variances2/sum(variances2)
pareto(percent_explained2)
xlabel('Principal Component')
ylabel('Variance Explained (%)')

biplot(coefs2(:,1:3), 'scores',scores2(:,1:3),'varlabels',nvar)

Xpc=scores2(:,1:3);

% kmeans clustering

[cidx2,cmeans2] = kmeans(Xpc,4,'dist','sqeuclidean');
[silh2,h] = silhouette(Xpc,cidx2,'sqeuclidean');

[cidx3,cmeans3,sumd3] = kmeans(Xpc,2,'replicates',5,'display','final');
sum(sumd3)

% HAC clustering

eucD = pdist(Xpc,'euclidean');
clustTreeEuc = linkage(eucD,'average');
cophenet(clustTreeEuc,eucD)
[h,nodes] = dendrogram(clustTreeEuc,0);

% Biplots

vmult=10;
ptsymb = {'bs','r^','md','go','c+'};

%subplot(2,2,1)
for i = 1:size(unique(cidx2),1)
    clust = find(cidx2==i);
    plot(Xpc(clust,1),Xpc(clust,2),ptsymb{i});
    hold on
end
hold on
text(coefs2(:,1)*vmult,coefs2(:,2)*vmult,nvar)
h=compass(coefs2(:,1)*(vmult-1),coefs2(:,2)*(vmult-1))
set(h,'Color',[.8 .8 .8],'MarkerSize',6);
line([min(scores2(:,1))-1 max(scores2(:,1))+1],[0 0],'Color',[.8 .8 .8])
line([0 0],[min(scores2(:,2))-1 max(scores2(:,2))+1],'Color',[.8 .8 .8])
title({'PCA biplot + kmeans clustering of ME70 shoal descriptors at -60dB'});
legend('Cluster1','Cluster2');
xlabel('PC1');
ylabel('PC2');