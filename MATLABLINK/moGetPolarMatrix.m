% moGetPolarMatrix get a pingFan data in polar coordinates as a matrix 
%   moGetPolarMatrix(s1,s2) 
%       s1 fan id 
%       s2 the transducer index
%       
%
% Ifremer Cponcelet 07/11/2007
% $Revision: 0.1 $
