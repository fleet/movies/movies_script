% moSetSounderTimeCorrection Set a time shift offset for the given sounder Id
%   moSetSounderTimeCorrection(s1,s2) 
%       s1 the sounder id
%       s2 the offset value
%
% Ifremer Cponcelet 03/06/2008
% $Revision: 0.1 $
