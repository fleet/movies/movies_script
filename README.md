# Welcome to the MOVIES3D Matlab code repository

MOVIES3D is a software designed by [Ifremer](https://wwz.ifremer.fr/en) to [display and process underwater active acoustic data](http://flotte.ifremer.fr/content/download/6032/129677/file/MOVIES3D_general.pdf) produced by [sonars](https://en.wikipedia.org/wiki/Sonar) and [echosounders](https://en.wikipedia.org/wiki/Echo_sounding).

This repository has been set up to share codes making use of MOVIES3D APIs available for Matlab programming languages.

Please proceed to [the wiki](https://gitlab.ifremer.fr/fleet/movies/movies_script/-/wikis/home) for more information on how to get and use MOVIES3D and its APIs.

For each method available in MOVIES3D software a mex file is linked to the MOVIES3D C++ library and allows to have access to the raw and processed data in Matlab possibly modify them and writing them back in HAC files using C++ standrad library.

All methods are available in MATLABLINK folder in the installtion path of MOVIES3D software.

In order to use MOVIES3D in Matlab:
- the path for the software (default C:\Program Files\Ifremer\Movies3d) needs to be added in the Windows path environment variable (in windows command line type set PATH=%PATH% C:\Program Files\Ifremer\Movies3d)
- the MATLABLINK folder (default C:\Program Files\Ifremer\Movies3d\MATLABLINK) needs to be added in Matlab path

Main scripts are listed [here](https://gitlab.ifremer.fr/fleet/movies/movies_script/-/blob/master/MATLABLINK/Sample/script_description.ods)